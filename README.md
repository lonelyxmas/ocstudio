### OneCode低代码引擎
随着低代码概念的火热，相关的技术及产品也是层出不穷，不管是老牌行业软件厂商还是开放平台厂商，不论是互联网行业企业SAAS软件新动向还是新兴的低代码创新产品服务,都在第一时间打出了低代码这张牌。各个平台虽然各有优势，但大多又是自成体系，真正在企业方面进行选择时却一时难以抉择。对于低代码平台的功能评价，以及各平台组件间的互联互通则成为了市场上迫切需求。

### 产品白皮书
[OneCode开源低代码引擎白皮书](https://www.toutiao.com/article/7195806447953904128/?log_from=117e4a6cca10e_1677482293750)

### 如何下载编译OneCode版本 
[如何下载编译OneCode版本](https://www.zhihu.com/zvideo/1630651575810011136)

### 视频介绍
[OneCode开源低代码引擎视频介绍](http://www.ixigua.com/7205505880618598927)

在刚刚过去的2022年，在平台互联互通的方面，阿里在第二季度推出开源引擎 “LowCodeEngin”，国家队信通院也应市场需求在第四季度推出了《低代码开发平台通用技术要求》，针对低代码相关概念以及功能点新型了进一步的规范和梳理。

在全面开放的大背景下，CodeBee团队，推出了基于开源LGPL协议 低代码引擎（LowCodeEngine）。

![输入图片说明](https://foruda.gitee.com/images/1677484365415989085/c0152cdf_8526535.png "屏幕截图")

### 产品组成

低代码引擎，由界面设计器、OneCode通码框架以及，DSM领域建模工具 三部分支撑体系相互支撑的部分来组成，通过开放标准的组件协议完成相互继承支持。
![输入图片说明](https://foruda.gitee.com/images/1677484419428814634/9d2fd8b6_8526535.png "屏幕截图")

### (1) 视图设计器引擎

设计器，采用的是拖拽引擎+插件的构造模型，用户可以通过开放的低代码协议编写插件。支持JS和JAVA两种扩展语言。样式构建提供了标准CSS3编辑器，支持事件动作以及函数动态扩展。支持自定义函数库扩展，支持阿里字体图片等资源库。

![输入图片说明](https://foruda.gitee.com/images/1677484450797462866/34af449a_8526535.png "屏幕截图")
### （2）OneCode通码编辑器：

OneCode,是一款为低代码语言定制的统一语法体系，采用Java语言作为原生语言，运行在JVM环境中，用户可以通过Java语言与低代码应用进行交互，也可以通过Java语言完成引擎插件，调用代码引擎完成编译部署应用。
![输入图片说明](https://foruda.gitee.com/images/1677484498028647779/68f60265_8526535.png "屏幕截图")

### （3）DSMEngine领域建模
![输入图片说明](https://foruda.gitee.com/images/1677484512876418738/7cd9c270_8526535.png "屏幕截图")
DSM模型支持三种建模模式：

（1）CodeFrist 代码优先模式

通过Java语言 OneCode 模式原生撰写。

（2）ViewFrist 视图优先

通过视图引擎拖拽完成前期的交互模型，反向完成DSM模型。

（3）ModuleFrist 模型优先

通过数据库,微服务接口等模式，构建基础模型。

DSM逆向转换

通过不同方式完成的DSM模型，可以通过OneCode 在视图、Code 、以及Module 三种方式之间自由切换，利用相关工具完成仿真调试以及部署运行。

DSM第三方语言转换

DSM出码模块采用了独立的模板架构，除了可以以OneCode形式存在，还可以支持独立的出码模块定制独立的第三方语言模型输
出
### 工作原理
 OneCode 本身基于JAVA语言体系，是在Java Spring 注解基础上的一套扩展子集，混合编译引擎器通过扩展注解构建完整的Domain模型，通过读取标准Spring 注解完成普通Web数据交付及调度过程，通过Domin域模型动态渲染JS文件输出为JSON交付给前端引擎构建页面。
![输入图片说明](https://foruda.gitee.com/images/1677481475543882307/b790657a_8526535.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1677481604694633769/4b0d1d24_8526535.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1677481491781322366/c7e80cb6_8526535.png "屏幕截图")

### OneCode 建模过程
    用户通过，拖拽完成页面建模序列化为按标准协议序列化JSON文件,后端OneCode服务支撑系统解析JSON文件并混合DSM建模信息以及后端服务逻辑后，通过混合编译，通过代码工厂指定出码模板，完成前后端一体的编译文件。
![输入图片说明](https://foruda.gitee.com/images/1677481807952628963/e7b75bd9_8526535.png "屏幕截图")

### 拖拽建模

    用户由前端低代码引擎提供拖拽支持，将用户需求转换为相应的组件组合，完成建模后根据标准的低代码组件序列化为JSON文件。
![输入图片说明](https://foruda.gitee.com/images/1677481725850909630/9f5e0854_8526535.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1677481733594640386/df356402_8526535.png "屏幕截图")
### 后端融合

            后端融合系统，通过读取标准JSON组件，完成组件在后端的模型转换。用户可以通过，DSM模型工具或者通过OneCode提供的建模通讯SDK，针对前端需要的服务调用逻辑以及业务数据逻辑，进行建模干预。
###   混合编译出码

           完成后端建模的融合后，通过代码工厂通过出码配置输出为OneCode源码。

### （1）标准化能力

OneCode平台在设计上将前端组件的设计上即采用了开放式设计模型及及存储通讯标准。这将在很大程度上为各家低代码平台组件互联互通提供便利，在设计上实现通用通行。避免形成应用孤岛。解决用户被绑定在特定平台的忧虑。
### （2）混合编译赋能
OneCode除了在前端实现了标准化组件定义外，还额外提供了后端建模的工具DSM，并通过领域模型将二者打通。这样在前端组件建模时便可以直接调用后端服务模型完成数据部分API构建。而DSM模型工具也可以在后端建模时直接读取前端组件属性，打通前端动作与后端服务的通讯能力。
![输入图片说明](https://foruda.gitee.com/images/1677481896368006704/eb2a85cd_8526535.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1677481901887009963/af10cfca_8526535.png "屏幕截图")
二者模型的打通大幅降低了低代码平台的劳动强度，同时通过针对模型的建模干预API，在模型建立重构的方面可以从更深的层次上建立业务模型的构建能力。
### （3）混编检测（预编译）
传统低代码平台基本上都是完全建立在JS的模型下，在初期建模时结构还算清晰但经过稍有点复杂的逻辑，构建时代码的冗余度以及结构就会变得混乱，特别是页跨页面操作或者完成前后台数据交互时。由于其脚本语言的特点无法完成实时校验，只能运行期测试才能发现问题。采用低代码构建的页面往往只是由于页面中做了一些简单的组件增删或者属性样式就该就会造成不可预期的结果，这大大降低了代码的可维护度。OneCode所构建的领域模型则很好的解决了这一问题，在前后端任意模型发生变化时即可调用混合编译，将页面间的连接关系以及前后台的数据关系进行校验通知。在预编译中提升整体的编译能力。
### （4）传统低代码服务赋能
在低代码平台有几个领域是有很不错的实施效果的我们以最常见的表单流程模式来谈一下OneCode赋能

### 编译文件下载
[http://cluster.raddev.cn:9080/ocstudio.rar](http://http://cluster.raddev.cn:9080/ocstudio.rar)
### 在线演示地址

[http://demo.raddev.cn:9080/RAD/DSMdsm/projectManager](http://demo.raddev.cn:9080/RAD/DSMdsm/projectManager)

### 产品白皮书
[OneCode开源低代码引擎白皮书](https://www.toutiao.com/article/7195806447953904128/?log_from=117e4a6cca10e_1677482293750)



### OneCode 集成工具介绍

     OneCode 集成工具（以下简称ESD），是OneCode 代码的集成开发环境。ESD面向的用户是专业的程序员，如果您不是专业的编程工作者需要先行了解一些JAVA语言相关基础知识以及其编译基础以便于方便自行下载编译。
### 一，为什么要做ESD？

     低代码不是一个新兴的概念，但将低代码作为一个独立的编程语言体系，独立的开发方法却是最近几年才被大家所接受的理念。但如大多新技术一样都会有一个成熟的过程，这个过程中除了技术本身的发展之外，其相关辅助的工具集，开发群体生态也是非常重要的一个部分。ESD最初的版本只是为了方便团队协作将OneCode （RAD）设计器以门户的方式开放给业务及技术开发者方便大家协同工作。随着项目及团队的技术演进，ESD也从单一的门户逐步完成了后端代码的编译，独立服务器的管理及部署。直到今天经过几轮的重构将其开放出来作为一个开源的项目。
### 二，ESD能做什么？

       首先，ESD是一组工具集，通过ESD开发者可以以最快最简介的方式快速的上手OneCode,将设计器完成的设计文件转换为真正的代码工程发布部署。
       在完成设计向代码的转换后，ESD将作为代码与设计之间的桥接工具，将页面设计器（RAD），领域模型工具（DSM），OneCode 真实代码(RealCode) 三者之间有机的组合起来，实现三者之间的转换应用。
       ESD也是作为OneCode 项目的管理工具，提供工程版本支持，团队协作支持，以及部署应用等功能支持。
### 三，ESD开源结构说明

（1）开源协议声明
开源地址：https://gitee.com/wenzhang77/ocstudio
​


 ESD本身采用，GPL3.0协议。该协议允许用户商业使用，但如果直接针对改程序进行改造则必须遵循GPL协议。
下载源码从pom编译依赖来看,ESD主要依赖的为OneCode基础包以及相关插件是采用的是更为宽泛的MIT协议。


```
<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-common-client</artifactId>
    <version>1.0.2</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-vfs-web</artifactId>
    <version>1.0.2</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-server</artifactId>
    <version>1.0.2</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-org-web</artifactId>
    <version>1.0.2</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-index-web</artifactId>
    <version>1.0.2</version>
</dependency>


<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-iot-webclient</artifactId>
    <version>1.0.2</version>
</dependency>


<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-database</artifactId>
    <version>1.0.2</version>
</dependency>


<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-esdstudio</artifactId>
    <version>1.0.2</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-plugins-db</artifactId>
    <version>1.0.2</version>
</dependency>
<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-plugins-org</artifactId>
    <version>1.0.2</version>
</dependency>


<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-plugins-formula</artifactId>
    <version>1.0.2</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-plugins-bpd</artifactId>
    <version>1.0.2</version>
</dependency>
<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-plugins-right</artifactId>
    <version>1.0.2</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-plugins-sysmanager</artifactId>
    <version>1.0.2</version>
</dependency>
<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-plugins-nav</artifactId>
    <version>1.0.3</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-bpm-web</artifactId>
    <version>1.0.2</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-plugins-dsm</artifactId>
    <version>1.0.2</version>
</dependency>


<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-plugins-bpm</artifactId>
    <version>1.0.2</version>
</dependency>

```



    总结一下：
（1）直接使用ESD免费，可以“魔改"ESD,但不能将魔改的版本作为商业版本二次销售。
（2）ESD依赖的OneCode基础包以及其插件，仍然遵循MIT协议。如果二次开发者在有一定的工具开发能力的话，可以在参考ESD的基础上重新构建自有的独立知识产权的OneCode 开发工具。

（2）如何编译ESD源码
如何编译ESD源码，可以参照OneCode编译视频介绍。

### （3）ESD服务工程依赖

      ESD是面向开发者的Studio工具 ，ESD运行需要依赖一些集成环境来支持，OneCode也为这些提供了一些默认的微服务实现。包括：开发代码协同管理的 onecode-vfs 虚拟目录服务，onecode-org用户认证，onecode-cluster集群节点管理，以及其他应用类服务如：onecode-bpm流程服务,onecode-iot物联网应用支持，onecode-jmq 消息服务，onecode-index检索服务， 从依赖的jar包来看，每一组服务，onecode也都提供了独立的SDK支持方便集成调用。分别对应的基于MIT协议的驱动服务包。

```
<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-vfs-web</artifactId>
    <version>1.0.2</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-org-web</artifactId>
    <version>1.0.2</version>
</dependency>

<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-index-web</artifactId>
    <version>1.0.2</version>
</dependency>


<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-iot-webclient</artifactId>
    <version>1.0.2</version>
</dependency>


<dependency>
    <groupId>cn.raddev</groupId>
    <artifactId>onecode-bpm-web</artifactId>
    <version>1.0.2</version>
</dependency>

```


### （4）ESD服务配置

    ESD的配置文件比较多，由于篇幅关系就不再一一做解说了，今天着重描述一下，集群相关的服务配置。/resource/useresbbean_config.xml
​
```

<actionContextClass>
    com.ds.context.MinServerActionContextImpl
</actionContextClass>

<configid>esb</configid>
<esb>
    <cnname>本地服务</cnname>
    <path>/../lib/:^onecode.*\.jar;./lib/:^onecode.*\.jar;</path>
</esb>


<configid>local</configid>
<local>
    <templetname>检索本地Class</templetname>
    <path>*com.ds</path>
</local>

<configid>remoteService</configid>
<remoteService>
    <path>remoteService</path>
    <cnname>集群订阅服务</cnname>
    <expressionTemManager>com.ds.web.client.RemoteTempXMLProxy</expressionTemManager>
</remoteService>

<configid>bpmservice</configid>
<bpmservice>
    <cnname>工作流服务</cnname>
    <path>bpm_tempbean_config.xml</path>
    <tokenType>user</tokenType>
    <serverUrl>http://bpm.raddev.cn:9080</serverUrl>
</bpmservice>

<configid>vfsnamenode</configid>
<vfsnamenode>
    <cnname>虚拟目录</cnname>
    <path>vfs_tempbean_config.xml</path>
    <tokenType>admin</tokenType>
    <serverUrl>http://data-vfsnamenode.raddev.cn:9080</serverUrl>
</vfsnamenode>

<configid>VFSStoreService</configid>
<VFSStoreService>
    <cnname>实体文件存储</cnname>
    <tokenType>user</tokenType>
    <path>vfsstore_tempbean_config.xml</path>
    <serverUrl>http://vfsstore.raddev.cn:9080</serverUrl>
</VFSStoreService>

<configid>orgservice</configid>
<orgservice>
    <path>org_tempbean_config.xml</path>
    <cnname>组织机构服务</cnname>
    <tokenType>admin</tokenType>
    <serverUrl>http://org.raddev.cn:9080</serverUrl>
</orgservice>

<configid>msgService</configid>
<msgService>
    <path>msg_tempbean_config.xml</path>
    <cnname>消息分发服务</cnname>
    <tokenType>admin</tokenType>
    <serverUrl>http://msg.raddev.cn:9080</serverUrl>
</msgService>

<configid>repeateventservice</configid>
<repeateventservice>
    <cnname>集群事件注册</cnname>
    <path>repeatmsg_config.xml</path>
</repeateventservice>

<configid>localservice</configid>
<localservice>
    <cnname>本地注册服务</cnname>
    <path>local_tempbean_config.xml</path>
</localservice>
<configid>iot</configid>
<iot>
    <type>BEAN</type>
    <cnname>总线内部方法</cnname>
    <desc>总线内部方法</desc>
    <serverUrl>http://iotserver.raddev.cn:9080</serverUrl>
    <path>iot_tempbean_config.xml</path>
    <expressionTemManager>
        com.ds.esb.config.xml.ExpressionTempXmlProxy
    </expressionTemManager>
</iot>


从配置文件中，我们可以获取两块配置，
（1）本地服务装载
  前面的博文介绍过，onecode本身是一组基于Spring的扩展注解。装载onecode模型需要配置本地的检索路径方便检索装载。
//本地服务装载AR
<configid>esb</configid>
<esb>
    <cnname>本地服务</cnname>
    <path>/../lib/:^onecode.*\.jar;./lib/:^onecode.*\.jar;</path>
</esb>

//检索本地Class装载服务
<configid>local</configid>2
<local>
    <templetname>检索本地Class</templetname>
    <path>*com.ds</path>
</local>

（2）远程服务装载
<configid>bpmservice</configid>
<bpmservice>
    <cnname>工作流服务</cnname>
    <path>bpm_tempbean_config.xml</path>
    <tokenType>user</tokenType>
    <serverUrl>http://bpm.raddev.cn:9080</serverUrl>
</bpmservice>
```


 ***篇幅关系具体的配置细节就不做累述了，如果您希望进一步了解配置细节或者具体实现可以下载 onecode-common-client 源码自行阅读，或在GITEE留言。** 


### 四，OneCode集群版开放计划

OneCode集群版设计的内容以及技术难度也会更大，但OneCode 依然会采用开放开源的策略。
预计，开放时间表

| 服务名称  | 服务介绍  |预计开放时间   |   开放协议|
|---|---|---|---|
|  onecode-iot | onecode物联网套件  |2023年4月份（已开放）   | MIT  |
|  onecode-vfs | onecode 文件代码管理服务  | 2023年第二季度  |   MIT|
| onecode-bpm  | onecode 流程管理服务  | 2023年第二季度  | GPL  |
|onecode-jmq   |  onecode 消息服务 |  2023年第二，三季度 |  GPL |
 **
如果您有兴趣可以，联系codebee团队，提前获取源码包开展内测。** 

