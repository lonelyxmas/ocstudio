//

// [[Page Appearance
xui.ini.$PageAppearance = {
    "theme":"default"
};
// ]]Page Appearance

// [[Font Icons CDN

// ]]Font Icons CDN

// [[Develop Env Setting
xui.ini.$DevEnv = {
    "designViewConf":{
        "width":1280,
        "height":1024
    }
};
// ]]Develop Env Setting
// [[Global Functions
xui.$cache.functions ={"reloadpage":{"desc":"远程装载页面", "params":[{"id":"uri", "type":"String", "desc":"远程VFS地址"}, {"id":"target", "type":"String", "desc":"目标框架地址"}, {"id":"childname", "type":"String", "desc":"子框架名称"}, {"id":"params", "type":"Hash", "desc":"初始化参数"}, {"id":"data", "type":"Hash", "desc":"设置数据到控件"}], "actions":[function(uri, target, childname, params,data){
                try{  xui.getModule(xui.getClassName(uri)).destroy(); }catch(e){ }
                var onEnd=function(url, remoteclass, test){
                    var clazz=xui.ModuleFactory.getModule(remoteclass,function(){
                        var currpanel ;
                        try{
                            currpanel=xui.getObjectByAlias(target)[xui.getObjectByAlias(target).length-1]
                                 currpanel.boxing().dumpContainer(childname);
                        }catch(e){ }
                         
                        try{
                            xui.merge(this.properties,params)
                        }catch(e){ }
                        
                        try{
                            if (data){
                                this.setData(data)
                            }
                          
                        }catch(e){ }
                      
                        this.show(null,currpanel,childname)
                    } ,false,params)
                }
                var onFail=function(msg){
                    xui.alert(msg)
                }
                xui.fetchClass(uri,onEnd,onFail,null,false)
            }]}};
// ]]Global Functions
