
xui.Class('RAD.db.project.TableSelectTree', 'xui.Module',{
    Instance:{
        initialize : function(){ },
        Dependencies:[],
        Required:[],
        properties : {
            "path":"form/myspace/versionspace/projectManager/0/RAD/db/project/TableSelectTree.cls",
            "projectName":"projectManager"
        },
        events:{},
        functions:{},
        iniComponents : function(){
            // [[Code created by JDSEasy RAD Studio
            var host=this, children=[], properties={}, append=function(child){children.push(child.get(0));};
            xui.merge(properties, this.properties);

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"addTableNames")
                    .setRequestDataSource([
                        {
                            "type":"treeview",
                            "name":"xui_ui_treeview21",
                            "path":""
                        },
                        {
                            "type":"form",
                            "name":"xui_ui_block216",
                            "path":""
                        }
                    ])
                    .setResponseDataTarget([ ])
                    .setResponseCallback([ ])
                    .setQueryURL("/admin/addTableNames")
                    .setQueryMethod("POST")
                    .onData([
                        {
                            "args":[ ],
                            "desc":"动作 1",
                            "method":"destroy",
                            "target":"xui_ui_dialog35",
                            "type":"control"
                        }
                    ])
            );

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"getAllTableTrees")
                    .setName("getAllTableTrees")
                    .setAutoRun(true)
                    .setQueryURL("/admin/fdt/magager/getAllTableTrees")
                    .setQueryMethod("POST")
                    .setRequestDataSource([
                        {
                            "name":"xui_ui_dialog35",
                            "path":"",
                            "type":"form"
                        }
                    ])
                    .setResponseDataTarget([
                        {
                            "name":"xui_ui_treeview21",
                            "path":"data",
                            "type":"treeview"
                        }
                    ])
                    .setResponseCallback([ ])
                    .onData([
                        {
                            "args":[
                                "{page.xui_ui_treeview21.setUIValue()}",
                                null,
                                null,
                                "{page.tableNames.getValue()}"
                            ],
                            "desc":"动作 1",
                            "method":"setUIValue",
                            "redirection":"other:callback:call",
                            "target":"xui_ui_treeview21",
                            "type":"control"
                        }
                    ])
            );

            append(
                xui.create("xui.UI.Dialog")
                    .setHost(host,"xui_ui_dialog35")
                    .setLeft("15.833333333333334em")
                    .setTop("2.5em")
                    .setWidth("28.333333333333332em")
                    .setHeight("41.666666666666664em")
                    .setCaption("所有库表")
                    .setImageClass("xui-icon-dialog")
            );

            host.xui_ui_dialog35.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block216")
                    .setDock("fill")
                    .setLeft("1.6666666666666667em")
                    .setTop("10.833333333333334em")
            );

            host.xui_ui_block216.append(
                xui.create("xui.UI.HiddenInput")
                    .setHost(host,"tableNames")
                    .setName("tableNames")
                    .setValue("")
            );

            host.xui_ui_block216.append(
                xui.create("xui.UI.HiddenInput")
                    .setHost(host,"configKey")
                    .setName("configKey")
                    .setValue("")
            );

            host.xui_ui_block216.append(
                xui.create("xui.UI.HiddenInput")
                    .setHost(host,"projectName")
                    .setName("projectName")
                    .setValue("")
            );

            host.xui_ui_block216.append(
                xui.create("xui.UI.TreeView")
                    .setHost(host,"xui_ui_treeview21")
                    .setItems([
                        {
                            "caption":"所有库表",
                            "hidden":false,
                            "id":"all",
                            "iniFold":false
                        }
                    ])
                    .setLeft("0em")
                    .setTop("0em")
                    .setSelMode("multibycheckbox")
                    .setValue("")
                    .onGetContent([
                        {
                            "args":[
                                "{page.loadapi.invoke()}",
                                null,
                                null,
                                null,
                                null,
                                null,
                                "{args[2]}"
                            ],
                            "desc":"动作 1",
                            "method":"invoke",
                            "redirection":"other:callback:call",
                            "target":"loadapi",
                            "type":"control"
                        }
                    ])
            );

            host.xui_ui_block216.append(
                xui.create("xui.UI.ComboInput")
                    .setHost(host,"xui_ui_comboinput346")
                    .setName("pattern")
                    .setDock("top")
                    .setLeft("3.6666666666666665em")
                    .setTop("2em")
                    .setWidth("18em")
                    .setLabelSize("6em")
                    .setLabelCaption("数据库表")
                    .setType("helpinput")
                    .setImageClass("xui-icon-code")
                    .onChange([
                        {
                            "args":[
                                "{page.getAllTableTrees.invoke()}"
                            ],
                            "desc":"动作 1",
                            "method":"invoke",
                            "redirection":"other:callback:call",
                            "target":"getAllTableTrees",
                            "type":"control"
                        }
                    ])
            );

            host.xui_ui_block216.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block217")
                    .setDock("bottom")
                    .setLeft("9.166666666666666em")
                    .setTop("30.833333333333332em")
                    .setHeight("3.5em")
            );

            host.xui_ui_block217.append(
                xui.create("xui.UI.Button")
                    .setHost(host,"xui_ui_button42")
                    .setLeft("6.583333333333333em")
                    .setTop("0.75em")
                    .setCaption("确定")
                    .setImageClass("fa fa-check-square")
                    .onClick([
                        {
                            "args":[
                                "{page.addTableNames.invoke()}"
                            ],
                            "desc":"动作 3",
                            "method":"invoke",
                            "redirection":"other:callback:call",
                            "target":"addTableNames",
                            "type":"control"
                        }
                    ])
            );

            host.xui_ui_block217.append(
                xui.create("xui.UI.Button")
                    .setHost(host,"xui_ui_button43")
                    .setLeft("14.916666666666666em")
                    .setTop("0.8333333333333334em")
                    .setCaption("关闭")
                    .setImageClass("fa fa-close")
                    .onClick([
                        {
                            "args":[ ],
                            "desc":"动作 1",
                            "method":"destroy",
                            "target":"RAD.db.project.TableSelectTree",
                            "type":"otherModuleCall"
                        }
                    ])
            );

            return children;
            // ]]Code created by JDSEasy RAD Studio
        },

        customAppend : function(parent, subId, left, top){
            return false;
        }  } ,
    Static:{
        "designViewConf":{
            "height":1024,
            "mobileFrame":false,
            "width":1280
        }
    }



});