
xui.Class('RAD.db.TableList', 'xui.Module',{
    Instance:{
        initialize : function(){ },
        Dependencies:[],
        Required:[
            "RAD.db.TableInfo"
        ],
        properties : {
            "autoDestroy":true,
            "path":"form/myspace/versionspace/projectManager/0/RAD/db/TableList.cls",
            "personId":"devdev",
            "personName":"系统管理员",
            "projectName":"projectManager"
        },
        events:{},
        functions:{},
        iniComponents : function(){
            // [[Code created by JDSEasy RAD Studio
            var host=this, children=[], properties={}, append=function(child){children.push(child.get(0));};
            xui.merge(properties, this.properties);

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"GetAllTableByNameAjax")
                    .setName("GetAllTableByNameAjax")
                    .setAutoRun(true)
                    .setQueryURL("/admin/fdt/magager/GetAllTableByName")
                    .setQueryMethod("POST")
                    .setRequestDataSource([
                        {
                            "name":"xui_ui_pagebar8",
                            "path":"",
                            "type":"pagebar"
                        },
                        {
                            "name":"xui_ui_block46",
                            "path":"",
                            "type":"form"
                        }
                    ])
                    .setResponseDataTarget([
                        {
                            "name":"addTable",
                            "path":"data",
                            "type":"treegrid"
                        },
                        {
                            "name":"xui_ui_pagebar8",
                            "path":"size",
                            "type":"pagebar"
                        }
                    ])
                    .setResponseCallback([ ])
                    .setProxyType("AJAX")
            );

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"delete")
                    .setName("delete")
                    .setQueryURL("/admin/fdt/magager/DropTable")
                    .setQueryMethod("POST")
                    .setRequestDataSource([
                        {
                            "name":"addTable",
                            "path":"",
                            "type":"treegrid"
                        },
                        {
                            "name":"xui_ui_block46",
                            "path":"",
                            "type":"form"
                        }
                    ])
                    .setResponseDataTarget([ ])
                    .setResponseCallback([ ])
                    .onData([
                        {
                            "args":[
                                "{page.GetAllTableByNameAjax.invoke()}"
                            ],
                            "conditions":[
                                {
                                    "symbol":"=",
                                    "right":"{0}",
                                    "left":"{args[1].requestStatus}"
                                }
                            ],
                            "desc":"动作 1",
                            "koFlag":"_DI_fail",
                            "method":"invoke",
                            "okFlag":"_DI_succeed",
                            "redirection":"other:callback:call",
                            "target":"GetAllTableByNameAjax",
                            "type":"control"
                        },
                        {
                            "args":[
                                "{args[1].errDes}"
                            ],
                            "conditions":[
                                {
                                    "symbol":"=",
                                    "right":"{-1}",
                                    "left":"{args[1].requestStatus}"
                                }
                            ],
                            "desc":"动作 2",
                            "method":"pop",
                            "target":"msg",
                            "type":"other"
                        }
                    ])
                    .onError([
                        {
                            "args":[
                                null,
                                "{args[1]}",
                                200,
                                5000
                            ],
                            "desc":"动作 1",
                            "koFlag":"_confirm_no",
                            "method":"message",
                            "okFlag":"_confirm_yes",
                            "target":"msg",
                            "type":"other"
                        }
                    ])
            );

            append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_panel24")
                    .setDock("fill")
            );

            host.xui_ui_panel24.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block46")
                    .setDock("fill")
                    .setLeft("5.833333333333333em")
                    .setTop("5em")
                    .setWidth("46.666666666666664em")
                    .setHeight("35.833333333333336em")
            );

            host.xui_ui_block46.append(
                xui.create("xui.UI.HiddenInput")
                    .setHost(host,"configKey")
                    .setName("configKey")
                    .setValue("")
            );

            host.xui_ui_block46.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block47")
                    .setDock("bottom")
                    .setLeft("19.166666666666668em")
                    .setTop("45em")
                    .setHeight("3.25em")
            );

            host.xui_ui_block47.append(
                xui.create("xui.UI.PageBar")
                    .setHost(host,"xui_ui_pagebar8")
                    .setLeft("1em")
                    .setTop("1em")
                    .setCaption("翻页")
            );

            host.xui_ui_block46.append(
                xui.create("xui.UI.ToolBar")
                    .setHost(host,"xui_ui_toolbar10")
                    .setItems([
                        {
                            "caption":"toolbar",
                            "hidden":false,
                            "id":"toolbar",
                            "sub":[
                                {
                                    "caption":"增加",
                                    "hidden":false,
                                    "iconFontSize":"",
                                    "id":"add",
                                    "imageClass":"fa fa-calendar-plus-o",
                                    "position":"absolute"
                                },
                                {
                                    "caption":"修改",
                                    "hidden":true,
                                    "iconFontSize":"",
                                    "id":"edit",
                                    "imageClass":"fa fa-lg fa-edit",
                                    "position":"absolute"
                                },
                                {
                                    "caption":"删除",
                                    "hidden":false,
                                    "iconFontSize":"",
                                    "id":"delete",
                                    "imageClass":"fa fa-lg fa-remove",
                                    "position":"absolute"
                                },
                                {
                                    "caption":"刷新",
                                    "hidden":false,
                                    "iconFontSize":"",
                                    "id":"reload",
                                    "imageClass":"xui-refresh",
                                    "position":"absolute"
                                },
                                {
                                    "id":"createModule",
                                    "caption":"批量生成模型",
                                    "imageClass":"fa  fa-building",
                                    "hidden":false,
                                    "position":"absolute",
                                    "iconFontSize":""
                                }
                            ]
                        }
                    ])
                    .onClick([
                        {
                            "args":[
                                "{page.show2()}",
                                null,
                                null,
                                null,
                                null,
                                null,
                                "{page.getData()}",
                                "{page}"
                            ],
                            "className":"RAD.db.TableInfo",
                            "conditions":[
                                {
                                    "symbol":"=",
                                    "right":"add",
                                    "left":"{args[5]}"
                                }
                            ],
                            "desc":"动作 5",
                            "method":"show2",
                            "redirection":"page",
                            "return":false,
                            "target":"RAD.db.TableInfo",
                            "type":"page"
                        },
                        {
                            "args":[
                                "{page.delete.invoke()}"
                            ],
                            "conditions":[
                                {
                                    "symbol":"=",
                                    "right":"delete",
                                    "left":"{args[1].id}"
                                }
                            ],
                            "desc":"动作 3",
                            "koFlag":"_DI_fail",
                            "method":"invoke",
                            "okFlag":"_DI_succeed",
                            "redirection":"other:callback:call",
                            "return":false,
                            "target":"delete",
                            "type":"control"
                        },
                        {
                            "args":[
                                "{page.GetAllTableByNameAjax.invoke()}"
                            ],
                            "conditions":[
                                {
                                    "symbol":"=",
                                    "right":"reload",
                                    "left":"{args[1].id}"
                                }
                            ],
                            "desc":"动作 4",
                            "koFlag":"_DI_fail",
                            "method":"invoke",
                            "okFlag":"_DI_succeed",
                            "redirection":"other:callback:call",
                            "return":false,
                            "target":"GetAllTableByNameAjax",
                            "type":"control"
                        },
                        {
                            "desc":"动作 6",
                            "type":"page",
                            "target":"RAD.db.ModuleConfig",
                            "args":[
                                "{page.show2()}",
                                undefined,
                                undefined,
                                undefined,
                                undefined,
                                undefined,
                                "{page.addTable.getValue()}",
                                "{page}"
                            ],
                            "method":"show2",
                            "conditions":[
                                {
                                    "left":"{args[1].id}",
                                    "symbol":"=",
                                    "right":"createModule"
                                }
                            ],
                            "className":"RAD.db.ModuleConfig",
                            "redirection":"page"
                        }
                    ])
            );

            host.xui_ui_block46.append(
                xui.create("xui.UI.TreeGrid")
                    .setHost(host,"addTable")
                    .setName("addTable")
                    .setLeft("0em")
                    .setTop("0em")
                    .setSelMode("multibycheckbox")
                    .setRowNumbered(true)
                    .setHeader([
                        {
                            "caption":"表名",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"name",
                            "type":"label",
                            "width":"16em"
                        },
                        {
                            "caption":"主键名称",
                            "colResizer":false,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"pkName",
                            "type":"label",
                            "width":"12em"
                        },
                        {
                            "caption":"数据库标识",
                            "colResizer":false,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"configKey",
                            "type":"label",
                            "width":"12em"
                        },
                        {
                            "caption":"中文注解",
                            "colResizer":false,
                            "editable":false,
                            "flexSize":true,
                            "hidden":false,
                            "id":"cnname",
                            "type":"label",
                            "width":"12em"
                        }
                    ])
                    .setUidColumn("name")
                    .setColOptions({
                        "colResizer":true,
                        "editable":false,
                        "flexSize":false,
                        "hidden":false,
                        "type":"label"
                    })
                    .setValue("")
                    .onRender([ ])
                    .onDblclickRow([
                        {
                            "args":[
                                "{page.show2()}",
                                null,
                                null,
                                "",
                                null,
                                null,
                                "{args[1]}",
                                "{page}"
                            ],
                            "className":"RAD.db.TableInfo",
                            "desc":"动作 1",
                            "method":"show2",
                            "redirection":"page",
                            "target":"RAD.db.TableInfo",
                            "type":"page"
                        }
                    ])
            );

            return children;
            // ]]Code created by JDSEasy RAD Studio
        },

        customAppend : function(parent, subId, left, top){
            return false;
        }  } ,
    Static:{
        "designViewConf":{
            "height":1024,
            "mobileFrame":false,
            "width":1280
        }
    }



});