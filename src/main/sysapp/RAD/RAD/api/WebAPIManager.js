xui.Class('RAD.api.WebAPIManager', 'xui.Module', {
    Instance: {
        initialize: function () {
        },
        Dependencies: [],

        Required: [
            "RAD.api.URLConfig"
        ],
        properties: {
            "path": "form/myspace/versionspace/projectManager/0/RAD/api/WebAPIManager.cls",
            "projectName": "projectManager"
        },
        events: {
            "onRender": {
                "actions": [
                    {
                        "args": [
                            "{page.show2()}",
                            null,
                            null,
                            "resourcelayout",
                            "main",
                            null,
                            null,
                            "{page}"
                        ],
                        "className": "RAD.api.URLConfig",
                        "desc": "动作 1",
                        "method": "show2",
                        "redirection": "page",
                        "target": "RAD.api.URLConfig",
                        "type": "page"
                    }
                ]
            }
        },
        properties: {},
        functions: {},
        iniComponents: function () {
            // [[Code created by JDSEasy RAD Studio
            var host = this, children = [], properties = {}, append = function (child) {
                children.push(child.get(0));
            };
            xui.merge(properties, this.properties);
            append(
                xui.create("xui.UI.Dialog")
                    .setHost(host, "xui_ui_dialog9")
                    .setLeft("1.6666666666666667em")
                    .setTop("3.3333333333333335em")
                    .setWidth("84.16666666666667em")
                    .setHeight("50.833333333333336em")
                    .setCaption("API管理")
                    .setImageClass("spafont spa-icon-rpc")
            );

            host.xui_ui_dialog9.append(
                xui.create("xui.UI.HiddenInput")
                    .setHost(host, "className")
                    .setName("className")
                    .setValue("")
            );

            host.xui_ui_dialog9.append(
                xui.create("xui.UI.Block")
                    .setHost(host, "resourcemain")
                    .setName("resourcemain")
                    .setDock("fill")
                    .setLeft("0em")
                    .setTop("0em")
            );

            host.resourcemain.append(
                xui.create("xui.UI.Layout")
                    .setHost(host, "resourcelayout")
                    .setName("resourcelayout")
                    .setItems([
                        {
                            "cmd": true,
                            "folded": false,
                            "id": "left",
                            "locked": false,
                            "min": 10,
                            "pos": "before",
                            "size": 200,
                            "hidden": false
                        },
                        {
                            "id": "main",
                            "min": 10,
                            "size": 80
                        }
                    ])
                    .setLeft("0em")
                    .setTop("0em")
                    .setType("horizontal")
            );

            host.resourcelayout.append(
                xui.create("xui.UI.Block")
                    .setHost(host, "xui_ui_block60")
                    .setDock("fill")
                    .setLeft("0em")
                    .setTop("0em"),
                "left"
            );

            host.xui_ui_block60.append(
                xui.create("xui.UI.HiddenInput")
                    .setHost(host, "projectName")
                    .setName("projectName")
                    .setValue("")
            );


            host.xui_ui_block60.append(
                (new xui.UI.TreeBar())
                    .setHost(host, "treebarCom")
                    .setSelMode("none")
                    .onItemSelected("_xui_ui_treeview19_onitemselected")
                    .onGetContent("_treebarcom_ongetcontent")
                    .onChange("_xui_ui_treeview19_onchange")
                    .setCustomStyle({
                        "KEY": {
                            "background-color": "#FFFFFF"
                        }
                    })
            );


            host.xui_ui_block60.append(
                (new xui.UI.ToolBar())
                    .setHost(host, "servicetool")
                    .setHandler(false)
                    .onClick("_toolbar_onclick")
                    .setCustomStyle({
                        "ITEMS": "border-width:0 0 1px 0;"
                    })
            );



            var items = [{
                id: 'any', sub: [
                    {id: 'refresh', imageClass: 'xui-refresh', caption: '', tips: '$RAD.tool2.refresh'},
                    {id: 'import', imageClass: 'xui-uicmd-add', caption: '', tips: '$RAD.toolbox.importapi'}
                ]
            }];

            host.servicetool.setItems(items);
            host.xui_ui_block60.append(
                (new xui.UI.ComboInput())
                    .setHost(host, "pattern")
                    .setName("pattern")
                    .setDock("top")
                    .setLeft("3.6666666666666665em")
                    .setTop("2em")
                    .setWidth("15em")
                    .setLabelSize("3em")
                    .setLabelCaption("$RAD.toolbox.search")
                    .setType("helpinput")
                    .onChange("_reload")
            );


            host.resourcelayout.append(
                xui.create("xui.UI.Block")
                    .setHost(host, "content")
                    .setName("content")
                    .setDock("fill")
                    .setLeft("15.833333333333334em")
                    .setTop("13.333333333333334em"),
                "main"
            );

            return children;
            // ]]Code created by JDSEasy RAD Studio
        },
        _buildItems: function (items) {
            var nitems = [], ns = this;
            xui.each(items, function (item) {
                if (item.key && CONF.mapWidgets[item.key]) {
                    var p = CONF.mapWidgets[item.key];
                    item.imageClass = CONF.mapWidgets[item.key].imageClass;
                }
                item.children = item.sub;
                item.properties = item.iniProp;
                if (item.sub) {
                    item.sub = ns._buildItems(item.sub);
                };

                item.getProperties=function () {
                    return this.properties;
                }


            })
            return items;
        },

        topNodes: [

            {
                id: 'pageService',
                key: 'jds.getPageServiceToolBoxService',
                caption: '$RAD.toolbox.pageService',
                group: true,
                iniFold: false,
                imageClass: 'spafont spa-icon-action1',
                sub: true
            },
            {
                id: 'projectService',
                key: 'jds.getProjectServiceToolBoxService',
                caption: '$RAD.toolbox.projectService',
                group: true,

                iniFold: false,
                imageClass: 'fa fa-cubes',
                sub: true
            },
            {
                id: 'localService',
                key: 'jds.localService',
                caption: '$RAD.toolbox.cls',
                group: true,
                iniFold: true,
                imageClass: 'spafont spa-icon-options',
                sub: true
            }

        ],

        _reload: function (profile, item) {
            ns = this;
            ns.treebarCom.setItems(ns._buildItems(this.topNodes));
            ns.treebarCom.fireItemClickEvent('pageService');

        },

        _toolbar_onclick: function (profile, item, group, e, src) {
            var ns = this;
            switch (item.id) {
                case 'refresh':
                    ns.treebarCom.setItems(ns._buildItems(this.topNodes));
                    break;

                case 'import':
                    xui.getModule("RAD.api.APITree", function () {
                        this.setData({projectName: SPA.curProjectName});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;

            }
        },

        _treebarcom_ongetcontent: function (profile, item, callback) {
            ns = this;
            if (item.key == "jds.localService") {
                xui.Ajax(CONF.getLocalServiceToolBoxService, {
                    projectName: SPA.curProjectName,
                    pattern: ns.pattern.getUIValue()
                }, function (txt) {
                    var obj = txt;
                    if (!obj || obj.error) {
                        xui.message("No module in this project!");
                        callback(false);
                    } else {

                        callback(ns._buildItems(obj.data));
                    }
                }, function (txt) {
                    callback(false);
                }, null, {method: 'post'}).start();
            } else if (item.key == "jds.getPageServiceToolBoxService") {
                xui.Ajax(CONF.getPageServiceToolBoxService, {
                    projectName: SPA.curProjectName,
                    className: SPA.currentClassName,
                    pattern: ns.pattern.getUIValue()
                }, function (txt) {
                    var obj = txt;
                    if (!obj || obj.error) {
                        xui.message("No module in this project!");
                        callback(false);
                    } else {
                        callback(ns._buildItems(obj.data));
                    }
                }, function (txt) {
                    callback(false);
                }, null, {method: 'post'}).start();
            } else if (item.key == "jds.getProjectServiceToolBoxService") {
                xui.Ajax(CONF.getProjectServiceToolBoxService, {
                    projectName: SPA.curProjectName,
                    pattern: ns.pattern.getUIValue()
                }, function (txt) {
                    var obj = txt;
                    if (!obj || obj.error) {
                        xui.message("No module in this project!");
                        callback(false);
                    } else {
                        var items = [];

                        callback(ns._buildItems(obj.data));
                        //callback(obj.data);
                    }
                }, function (txt) {
                    callback(false);
                }, null, {method: 'post'}).start();
            } else if (item.key == "jds.Server") {
                xui.Ajax(CONF.getRemoteServiceByKeyService, {
                    projectName: SPA.curProjectName,
                    pattern: ns.pattern.getUIValue(),
                    serverId: item.id
                }, function (txt) {

                    var obj = txt;
                    if (!obj || obj.error) {
                        xui.message("No service in this project!");
                        callback(false);
                    } else {
                        var items = [];
                        callback(obj.data);
                    }
                }, function (txt) {
                    callback(false);
                }, null, {method: 'post'}).start();
            }


        },

        _xui_ui_treeview19_onchange:function (profile, oldValue, newValue, force, tag) {
            var ns = this, uictrl = profile.boxing();
            xui.echo( xui.getModule("RAD.api.URLConfig").getDataFromEditor());
        } ,
        _xui_ui_treeview19_onitemselected: function (profile, item, e, src, type) {
            var ns = this, uictrl = profile.boxing(),
                prop = ns.properties, callback = function () { };
            xui.getModule("RAD.api.URLConfig", function () {
               this.setDataToEditor(item, ns.$host, ns.$cls,ns.$designer);
                this.setEvents({
                    "onchange": callback
                });

                //this.show();
            });
        },

        setDataToEditor: function (host, cls,designer) {
            this.$cls = cls;
            this.$host = host;
            this.$designer=designer;
            this.treebarCom.setItems(this._buildItems(this.topNodes));

        },

        customAppend: function (parent, subId, left, top) {
            return false;
        }
    },
    Static: {
        "designViewConf": {
            "height": 1024,
            "mobileFrame": false,
            "width": 1280
        }
    }


});