
xui.Class('RAD.api.APITree', 'xui.Module',{
    Instance:{
        initialize : function(){
        },
        Dependencies:[],
        Required:[],
        properties : {
            "path":"form/myspace/versionspace/projectManager/0/App/projectmanager/api/APITree.cls",
            "projectName":"projectManager"
        },
        events:{},
        functions:{},
        iniComponents : function(){
            // [[Code created by JDSEasy RAD Studio
            var host=this, children=[], properties={}, append=function(child){children.push(child.get(0));};
            xui.merge(properties, this.properties);

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"loadapi")
                    .setName("loadapi")
                    .setAutoRun(true)
                    .setQueryURL("/admin/getAllService")
                    .setQueryMethod("POST")
                    .setRequestDataSource([
                        {
                            "name":"xui_ui_treeview19",
                            "path":"",
                            "type":"form"
                        },
                        {
                            "name":"xui_ui_dialog13",
                            "path":"",
                            "type":"form"
                        }
                    ])
                    .setResponseDataTarget([
                        {
                            "name":"xui_ui_treeview19",
                            "path":"data",
                            "type":"treeview"
                        }
                    ])
                    .setResponseCallback([ ])
                    .onData([
                        {
                            "args":[
                                {
                                    "imageClass":"xui-icon-code"
                                },
                                { }
                            ],
                            "desc":"动作 1",
                            "method":"setProperties",
                            "target":"pattern",
                            "type":"control"
                        }
                    ])
                    .beforeData([
                        {
                            "args":[
                                {
                                    "imageClass":"xui-icon-loading"
                                },
                                { }
                            ],
                            "desc":"动作 1",
                            "method":"setProperties",
                            "target":"pattern",
                            "type":"control"
                        }
                    ])
            );

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"addApi")
                    .setName("addApi")
                    .setQueryURL("/admin/addAPI")
                    .setQueryMethod("POST")
                    .setRequestDataSource([
                        {
                            "name":"xui_ui_block55",
                            "path":"",
                            "type":"form"
                        }
                    ])
                    .setResponseDataTarget([ ])
                    .setResponseCallback([ ])
            );

            append(
                xui.create("xui.UI.Dialog")
                    .setHost(host,"xui_ui_dialog13")
                    .setLeft("15.833333333333334em")
                    .setTop("3.3333333333333335em")
                    .setWidth("28.333333333333332em")
                    .setHeight("41.666666666666664em")
                    .setCaption("所有API")
                    .setImageClass("xui-icon-bullet")
            );

            host.xui_ui_dialog13.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block55")
                    .setDock("fill")
                    .setLeft("1.6666666666666667em")
                    .setTop("10.833333333333334em")
            );

            host.xui_ui_block55.append(
                xui.create("xui.UI.HiddenInput")
                    .setHost(host,"projectName")
                    .setName("projectName")
                    .setValue("")
            );

            host.xui_ui_block55.append(
                xui.create("xui.UI.TreeView")
                    .setHost(host,"xui_ui_treeview19")

                    .setItems([
                        {
                            "caption":"所有API",
                            "hidden":false,
                            "id":"all",
                            "imageClass":"xui-uicmd-cmdbox",
                            "iniFold":false,
                            "sub":[
                                {
                                    "caption":"管理API",
                                    "hidden":false,
                                    "id":"admin",
                                    "imageClass":"xui-icon-xui"
                                }
                            ]
                        }
                    ])
                    .setLeft("0em")
                    .setTop("0em")
                    .setSelMode("multibycheckbox")
                    .setValue("")
                    .onGetContent([
                        {
                            "args":[
                                "{page.loadapi.invoke()}",
                                null,
                                null,
                                null,
                                null,
                                null,
                                "{args[2]}"
                            ],
                            "desc":"动作 1",
                            "method":"invoke",
                            "redirection":"other:callback:call",
                            "target":"loadapi",
                            "type":"control"
                        }
                    ])
            );

            host.xui_ui_block55.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block59")
                    .setDock("bottom")
                    .setLeft("9.166666666666666em")
                    .setTop("30.833333333333332em")
                    .setHeight("3.5em")
            );

            host.xui_ui_block59.append(
                xui.create("xui.UI.Button")
                    .setHost(host,"xui_ui_button17")
                    .setLeft("6.583333333333333em")
                    .setTop("0.75em")
                    .setCaption("确定")
                    .setImageClass("fa fa-check-square")
                    .onClick([
                        {
                            "args":[
                                "{page.addApi.setQueryData()}",
                                null,
                                null,
                                "{page.xui_ui_treeview19.getUIValue()}",
                                "id"
                            ],
                            "desc":"动作 2",
                            "method":"setQueryData",
                            "redirection":"other:callback:call",
                            "target":"addApi",
                            "type":"control"
                        },
                        {
                            "args":[ ],
                            "desc":"动作 1",
                            "koFlag":"_DI_fail",
                            "method":"invoke",
                            "okFlag":"_DI_succeed",
                            "target":"addApi",
                            "type":"control"
                        },
                        {
                            "args":[ ],
                            "desc":"动作 3",
                            "method":"destroy",
                            "target":"xui_ui_dialog13",
                            "type":"control"
                        }
                    ])
            );

            host.xui_ui_block59.append(
                xui.create("xui.UI.Button")
                    .setHost(host,"xui_ui_button18")
                    .setLeft("14.916666666666666em")
                    .setTop("0.8333333333333334em")
                    .setCaption("关闭")
                    .setImageClass("fa fa-close")
                    .onClick([
                        {
                            "args":[ ],
                            "desc":"动作 1",
                            "method":"destroy",
                            "target":"RAD.api.APITree",
                            "type":"page"
                        }
                    ])
            );

            host.xui_ui_block55.append(
                xui.create("xui.UI.ComboInput")
                    .setHost(host,"pattern")
                    .setName("pattern")
                    .setDock("top")
                    .setLeft("3.6666666666666665em")
                    .setTop("2em")
                    .setWidth("18em")
                    .setLabelSize("6em")
                    .setLabelCaption("API-URL:")
                    .setType("helpinput")
                    .setImageClass("xui-icon-code")
                    .onChange([
                        {
                            "args":[
                                "{page.loadapi.setQueryData()}",
                                null,
                                null,
                                "{args[2]}",
                                "pattern"
                            ],
                            "desc":"动作 1",
                            "method":"setQueryData",
                            "redirection":"other:callback:call",
                            "target":"loadapi",
                            "type":"control"
                        },
                        {
                            "args":[
                                "{page.loadapi.invoke()}",
                                null,
                                null,
                                null,
                                null,
                                ""
                            ],
                            "desc":"动作 2",
                            "koFlag":"_DI_fail",
                            "method":"invoke",
                            "okFlag":"_DI_succeed",
                            "redirection":"other:callback:call",
                            "target":"loadapi",
                            "type":"control"
                        }
                    ])
            );

            return children;
            // ]]Code created by JDSEasy RAD Studio
        },

        customAppend : function(parent, subId, left, top){
            return false;
        }
    } ,
    Static:{}
});