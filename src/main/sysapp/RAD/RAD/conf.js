new function () {
    if (!window.CONF) window.CONF = {};

    xui.builtinFontIcon = {
        "fa fa-lg fa-calendar-plus-o": "fa fa-lg fa-calendar-plus-o",
        "fa fa-lg fa-close": "fa fa-lg fa-close",
        "fa fa-lg fa-circle-o-notch": "fa fa-lg fa-circle-o-notch",
        'spafont spa-icon-data': '&#xe601',
        'spafont spa-icon-debug': '&#xe602',
        'spafont spa-icon-debug1': '&#xe603',
        'spafont spa-icon-c-video': '&#xe604',
        'spafont spa-icon-c-webapi': '&#xe605',
        'spafont spa-icon-cut': '&#xe606',
        'spafont spa-icon-delete': '&#xe607',
        'spafont spa-icon-designview': '&#xe608',
        'spafont spa-icon-domnode': '&#xe609',
        'spafont spa-icon-deletepath': '&#xe60a',
        'spafont spa-icon-edit': '&#xe60b',
        'spafont spa-icon-deletefile': '&#xe60c',
        'spafont spa-icon-event': '&#xe60d',
        'spafont spa-icon-editpath': '&#xe60e',
        'spafont spa-icon-empty': '&#xe60f',
        'spafont spa-icon-file': '&#xe610',
        'spafont spa-icon-explore': '&#xe611',
        'spafont spa-icon-fillcell': '&#xe612',
        'spafont spa-icon-filexml': '&#xe613',
        'spafont spa-icon-formatjs': '&#xe614',
        'spafont spa-icon-formatjson': '&#xe615',
        'spafont spa-icon-frame': '&#xe616',
        'spafont spa-icon-hmirror': '&#xe617',
        'spafont spa-icon-hash': '&#xe618',
        'spafont spa-icon-function': '&#xe619',
        'spafont spa-icon-gallery': '&#xe61a',
        'spafont spa-icon-html': '&#xe61b',
        'spafont spa-icon-icon-arrowtop': '&#xe61c',
        'spafont spa-icon-icon-bigup': '&#xe61d',
        'spafont spa-icon-indent': '&#xe61e',
        'spafont spa-icon-js': '&#xe61f',
        'spafont spa-icon-jumpto': '&#xe620',
        'spafont spa-icon-language': '&#xe621',
        'spafont spa-icon-json-file': '&#xe622',
        'spafont spa-icon-manual': '&#xe623',
        'spafont spa-icon-links': '&#xe624',
        'spafont spa-icon-login': '&#xe625',
        'spafont spa-icon-movebackward': '&#xe626',
        'spafont spa-icon-module': '&#xe627',
        'spafont spa-icon-move-down': '&#xe628',
        'spafont spa-icon-moveforward': '&#xe629',
        'spafont spa-icon-move-up': '&#xe62a',
        'spafont spa-icon-newfile': '&#xe62c',
        'spafont spa-icon-newprj': '&#xe62d',
        'spafont spa-icon-ok': '&#xe62e',
        'spafont spa-icon-openprj': '&#xe62f',
        'spafont spa-icon-package': '&#xe630',
        'spafont spa-icon-options': '&#xe631',
        'spafont spa-icon-page': '&#xe632',
        'spafont spa-icon-phonegap': '&#xe633',
        'spafont spa-icon-paste': '&#xe634',
        'spafont spa-icon-pic': '&#xe635',
        'spafont spa-icon-previewpage': '&#xe636',
        'spafont spa-icon-preview': '&#xe637',
        'spafont spa-icon-property': '&#xe638',
        'spafont spa-icon-rename': '&#xe639',
        'spafont spa-icon-project': '&#xe63a',
        'spafont spa-icon-rendermode': '&#xe63b',
        'spafont spa-icon-resetpath': '&#xe63c',
        'spafont spa-icon-rotateleft': '&#xe63d',
        'spafont spa-icon-rotateright': '&#xe63e',
        'spafont spa-icon-rpc': '&#xe63f',
        'spafont spa-icon-save': '&#xe640',
        'spafont spa-icon-saveall': '&#xe641',
        'spafont spa-icon-searchreplace': '&#xe642',
        'spafont spa-icon-select': '&#xe643',
        'spafont spa-icon-select1': '&#xe644',
        'spafont spa-icon-setup': '&#xe645',
        'spafont spa-icon-shouhou': '&#xe646',
        'spafont spa-icon-star': '&#xe647',
        'spafont spa-icon-settingprj': '&#xe648',
        'spafont spa-icon-theme': '&#xe649',
        'spafont spa-icon-tool': '&#xe64a',
        'spafont spa-icon-unkown': '&#xe64b',
        'spafont spa-icon-tools': '&#xe64c',
        'spafont spa-icon-values': '&#xe64d',
        'spafont spa-icon-video': '&#xe64e',
        'spafont spa-icon-var': '&#xe64f',
        'spafont spa-icon-vmirror': '&#xe650',
        'spafont spa-icon-shukongjian': '&#xe651',
        'spafont spa-icon-viewpoint': '&#xe652',
        'spafont spa-icon-action': '&#xe653',
        'spafont spa-icon-x': '&#xe654',
        'spafont spa-icon-wbdiconsoftswf': '&#xe655',
        'spafont spa-icon-action1': '&#xe656',
        'spafont spa-icon-addpath': '&#xe657',
        'spafont spa-icon-alignh': '&#xe658',
        'spafont spa-icon-alignl': '&#xe659',
        'spafont spa-icon-alignc': '&#xe65a',
        'spafont spa-icon-alignm': '&#xe65b',
        'spafont spa-icon-aligncell': '&#xe65c',
        'spafont spa-icon-alignt': '&#xe65d',
        'spafont spa-icon-alignwh': '&#xe65e',
        'spafont spa-icon-alingb': '&#xe65f',
        'spafont spa-icon-alignw': '&#xe660',
        'spafont spa-icon-array': '&#xe661',
        'spafont spa-icon-app': '&#xe662',
        'spafont spa-icon-arrow': '&#xe663',
        'spafont spa-icon-audio': '&#xe664',
        'spafont spa-icon-breakpath': '&#xe665',
        'spafont spa-icon-astext': '&#xe666',
        'spafont spa-icon-cancel': '&#xe667',
        'spafont spa-icon-c-audio': '&#xe668',
        'spafont spa-icon-c-buttonviews': '&#xe669',
        'spafont spa-icon-c-button': '&#xe66a',
        'spafont spa-icon-c-block': '&#xe66b',
        'spafont spa-icon-c-buttonview': '&#xe66c',
        'spafont spa-icon-c-checkbox': '&#xe66d',
        'spafont spa-icon-c-colorpicker': '&#xe66e',
        'spafont spa-icon-c-comboinput': '&#xe66f',
        'spafont spa-icon-c-counter': '&#xe670',
        'spafont spa-icon-c-cssbox': '&#xe671',
        'spafont spa-icon-c-databinder': '&#xe672',
        'spafont spa-icon-c-currencyinput': '&#xe673',
        'spafont spa-icon-c-dialog': '&#xe674',
        'spafont spa-icon-c-div': '&#xe675',
        'spafont spa-icon-c-dateinput': '&#xe676',
        'spafont spa-icon-c-datetime': '&#xe677',
        'spafont spa-icon-c-droplist': '&#xe678',
        'spafont spa-icon-c-dropbutton': '&#xe679',
        'spafont spa-icon-c-fileinput': '&#xe67a',
        'spafont spa-icon-c-element': '&#xe67b',
        'spafont spa-icon-c-foldinglist': '&#xe67c',
        'spafont spa-icon-c-foldingtabs': '&#xe67d',
        'spafont spa-icon-c-flash': '&#xe67e',
        'spafont spa-icon-c-gallery': '&#xe67f',
        'spafont spa-icon-c-grid': '&#xe680',
        'spafont spa-icon-c-group': '&#xe681',
        'spafont spa-icon-c-iconslist': '&#xe682',
        'spafont spa-icon-c-hiddeninput': '&#xe683',
        'spafont spa-icon-c-imagebutton': '&#xe684',
        'spafont spa-icon-c-image': '&#xe685',
        'spafont spa-icon-c-label': '&#xe686',
        'spafont spa-icon-c-layout': '&#xe687',
        'spafont spa-icon-c-input': '&#xe688',
        'spafont spa-icon-clear': '&#xe689',
        'spafont spa-icon-class': '&#xe68a',
        'spafont spa-icon-click': '&#xe68b',
        'spafont spa-icon-c-list': '&#xe68c',
        'spafont spa-icon-closepath': '&#xe68d',
        'spafont spa-icon-clone': '&#xe68e',
        'spafont spa-icon-c-menu': '&#xe68f',
        'spafont spa-icon-c-menubar': '&#xe690',
        'spafont spa-icon-c-nativebutton': '&#xe691',
        'spafont spa-icon-codeview': '&#xe692',
        'spafont spa-icon-code': '&#xe693',
        'spafont spa-icon-c-numberinput': '&#xe694',
        'spafont spa-icon-coin': '&#xe695',
        'spafont spa-icon-com': '&#xe696',
        'spafont spa-icon-conf': '&#xe697',
        'spafont spa-icon-conf1': '&#xe698',
        'spafont spa-icon-coms': '&#xe699',
        'spafont spa-icon-console': '&#xe69a',
        'spafont spa-icon-config': '&#xe69b',
        'spafont spa-icon-c-pager': '&#xe69c',
        'spafont spa-icon-c-panel': '&#xe69d',
        'spafont spa-icon-copy': '&#xe69e',
        'spafont spa-icon-c-progressbar': '&#xe69f',
        'spafont spa-icon-c-richeditor': '&#xe6a0',
        'spafont spa-icon-c-radiobox': '&#xe6a1',
        'spafont spa-icon-c-slider': '&#xe6a2',
        'spafont spa-icon-c-password': '&#xe6a3',
        'spafont spa-icon-c-span': '&#xe6a4',
        'spafont spa-icon-c-stacks': '&#xe6a5',
        'spafont spa-icon-css': '&#xe6a6',
        'spafont spa-icon-c-statusbtns': '&#xe6a7',
        'spafont spa-icon-c-spinner': '&#xe6a8',
        'spafont spa-icon-c-tabs': '&#xe6a9',
        'spafont spa-icon-c-statusbutton': '&#xe6aa',
        'spafont spa-icon-c-textarea': '&#xe6ab',
        'spafont spa-icon-c-timeinput': '&#xe6ac',
        'spafont spa-icon-c-toolbar': '&#xe6ad',
        'spafont spa-icon-c-timer': '&#xe6ae',
        'spafont spa-icon-c-treebar': '&#xe6af',
        'spafont spa-icon-c-treeview': '&#xe6b0',
        'spafont spa-icon-c-url': '&#xe6b1',
        'spafont spa-icon-custom': '&#xe6b2',
        'spafont spa-icon-com2': '&#xe6b3',
        'spafont spa-icon-com3': '&#xe6b4',
        'spafont spa-icon-mqtt': '&#xe70c',
        'xui-icon-xui': '&#xe6a7;',
        'xui-icon-placeholder': '&#xe668;',
        'xui-icon-empty': '&#xe668;',
        'xui-icon-loading': '&#xe678;',
        'xui-icon-star': '&#xe69c;',
        'xui-icon-smill': '&#xe69a;',
        'xui-icon-trash': '&#xe6a1;',
        'xui-icon-error': '&#xe666;',
        'xui-icon-back': '&#xe64e;',
        'xui-icon-bold': '&#xe650;',
        'xui-icon-bgcolor': '&#xe64f;',
        'xui-icon-breaklink': '&#xe652;',
        'xui-icon-clock': '&#xe657;',
        'xui-icon-circle': '&#xe651;',
        'xui-icon-bigup': '&#xe658;',
        'xui-icon-code': '&#xe659;',
        'xui-icon-function': '&#xe619',
        'xui-icon-dialog': '&#xe65a;',
        'xui-icon-bullet': '&#xe65b;',
        'xui-icon-date': '&#xe65c;',
        'xui-icon-dragadd': '&#xe661;',
        'xui-icon-dragmove': '&#xe662;',
        'xui-icon-dragcopy': '&#xe663;',
        'xui-icon-dropdown': '&#xe664;',
        'xui-icon-dragstop': '&#xe665;',
        'xui-icon-file': '&#xe667;',
        'xui-icon-file-expand': '&#xe669;',
        'xui-icon-file-fold': '&#xe66a;',
        'xui-icon-filter': '&#xe66b;',
        'xui-icon-font': '&#xe66c;',
        'xui-icon-forecolor': '&#xe66e;',
        'xui-icon-formatbrush': '&#xe66f;',
        'xui-icon-formatclear': '&#xe670;',
        'xui-icon-indent': '&#xe671;',
        'xui-icon-html': '&#xe672;',
        'xui-icon-italic': '&#xe673;',
        'xui-icon-link': '&#xe675;',
        'xui-icon-inserthr': '&#xe676;',
        'xui-icon-menu': '&#xe677;',
        'xui-icon-minus': '&#xe679;',
        'xui-icon-menu-checked': '&#xe67a;',
        'xui-icon-mobile': '&#xe67b;',
        'xui-icon-mouse': '&#xe67c;',
        'xui-icon-outdent': '&#xe689;',
        'xui-icon-picture': '&#xe68a;',
        'xui-icon-print': '&#xe68c;',
        'xui-icon-numbers': '&#xe68d;',
        'xui-icon-right': '&#xe68e;',
        'xui-icon-search': '&#xe68f;',
        'xui-icon-question': '&#xe690;',
        'xui-icon-remove': '&#xe693;',
        'xui-icon-sort': '&#xe699;',
        'xui-icon-sort-checked': '&#xe69b;',
        'xui-icon-strikethrough': '&#xe69d;',
        'xui-icon-sub': '&#xe69e;',
        'xui-icon-super': '&#xe69f;',
        'xui-icon-transparent': '&#xe6a0;',
        'xui-icon-triangle-down': '&#xe6a2;',
        'xui-icon-triangle-left': '&#xe6a3;',
        'xui-icon-triangle-right': '&#xe6a4;',
        'xui-icon-triangle-up': '&#xe6a5;',
        'xui-icon-undo': '&#xe6a6;',
        'xui-icon-redo': '&#xe691;',
        'xui-icon-upload': '&#xe6a8;',
        'xui-icon-zoomin': '&#xe6a9;',
        'xui-icon-zoomout': '&#xe6ac;',
        'xui-icon-underline': '&#xe6aa;',
        'xui-load-error': '&#xe6ab;',
        'xui-refresh': '&#xe6ae;',
        'xui-uicmd-arrowdrop': '&#xe6af;',
        'xui-uicmd-check': '&#xe6b0;',
        'xui-uicmd-check-checked': '&#xe6b1;',
        'xui-uicmd-add': '&#xe6ad;',
        'xui-uicmd-close': '&#xe6b2;',
        'xui-uicmd-cmdbox': '&#xe6b3;',
        'xui-uicmd-date': '&#xe6b4;',
        'xui-uicmd-color': '&#xe6b5;',
        'xui-uicmd-delete': '&#xe6b6;',
        'xui-uicmd-dotted': '&#xe6b7;',
        'xui-uicmd-datetime': '&#xe6b8;',
        'xui-uicmd-helpinput': '&#xe6b9;',
        'xui-uicmd-getter': '&#xe6ba;',
        'xui-uicmd-file': '&#xe6bb;',
        'xui-uicmd-info': '&#xe6bc;',
        'xui-uicmd-land': '&#xe6bd;',
        'xui-uicmd-max': '&#xe6be;',
        'xui-uicmd-min': '&#xe6bf;',
        'xui-uicmd-location': '&#xe6c0;',
        'xui-uicmd-opt': '&#xe6c1;',
        'xui-uicmd-pop': '&#xe6c2;',
        'xui-uicmd-pin': '&#xe6c3;',
        'xui-uicmd-radio': '&#xe6c4;',
        'xui-uicmd-radio-checked': '&#xe6c5;',
        'xui-uicmd-popbox': '&#xe6c6;',
        'xui-uicmd-refresh': '&#xe6c7;',
        'xui-uicmd-restore': '&#xe6c8;',
        'xui-uicmd-remove': '&#xe6c9;',
        'xui-uicmd-time': '&#xe6ca;',
        'xui-uicmd-select': '&#xe6cb;',
        'xui-uicmd-save': '&#xe6cc;',
        'xui-uicmd-toggle': '&#xe6cd;',
        'xui-uicmd-toggle-checked': '&#xe6ce;',
        'xui-icon-alignleft': '&#xe6d2;',
        'xui-icon-aligncenter': '&#xe6d0;',
        'xui-icon-alignjustify': '&#xe6d1;',
        'xui-icon-alignright': '&#xe6cf;',
        'xui-icon-singleleft': '&#xe694;',
        'xui-icon-singleright': '&#xe695;',
        'xui-icon-singleup': '&#xe696;',
        'xui-icon-singledown': '&#xe692;',
        'xui-icon-smalldown': '&#xe697;',
        'xui-icon-smallup': '&#xe698;',
        'xui-icon-arrowleft': '&#xe64c;',
        'xui-icon-arrowright': '&#xe64b;',
        'xui-icon-arrowtop': '&#xe64d;',
        'xui-icon-arrowbottom': '&#xe6d3;',
        'xui-icon-circleleft': '&#xe654;',
        'xui-icon-circleright': '&#xe655;',
        'xui-icon-circleup': '&#xe656;',
        'xui-icon-circledown': '&#xe653;',
        'xui-icon-doubleleft': '&#xe65e;',
        'xui-icon-doubleright': '&#xe660;',
        'xui-icon-doubleup': '&#xe65f;',
        'xui-icon-doubledown': '&#xe65d;',
        'xui-icon-first': '&#xe66d;',
        'xui-icon-prev': '&#xe68b;',
        'xui-icon-next': '&#xe67d;',
        'xui-icon-last': '&#xe674;',
        'xui-icon-number': '&#xe67e;',
        'xui-icon-number0': '&#xe680;',
        'xui-icon-number1': '&#xe67f;',
        'xui-icon-number2': '&#xe681;',
        'xui-icon-number3': '&#xe682;',
        'xui-icon-number4': '&#xe683;',
        'xui-icon-number5': '&#xe686;',
        'xui-icon-number6': '&#xe687;',
        'xui-icon-number7': '&#xe684;',
        'xui-icon-number8': '&#xe685;',
        'xui-icon-number9': '&#xe688;'
    };


    xui.merge(window.CONF, {
        enable_codeEdtor: 1,
        enable_openAPI: 1,
        enable_preview: 1,
        enable_editSandboxTheme: 1,
        enable_fontAwesome: 1,

        productName: "JDSEasy Front-End Builder",
        canvas_svg: false,
        DefaultXUIClassCode: "xui.Class('App','xui.Module',{\n    Instance:{\n    }\n});",

//        ignoreAnnouncement:true,
        version: '0.9',
        versionTag: 'Release',
        dftLang: 'cn',
        serviceType: "remote",
        openProjectService: xui.ini.appPath + 'openProject',
        remoteService: xui.ini.appPath + 'request',
        saveFile: xui.ini.appPath + 'saveContent',
        saveAsJson: xui.ini.appPath + 'saveAsJson',
        getFileContent: xui.ini.appPath + 'getFileContent',
        getProjectListService: '/admin/getProjectList',
        getProjectVersionListService: '/admin/getProjectVersionList',
        getProjectTemp: '/admin/getTempProjectList',
        getProjectTempType: '/admin/getTempType',

        openActionsService: xui.ini.appPath + 'openActions',
        openFolderService: xui.ini.appPath + 'openFolder',
        importFileService: xui.ini.appPath + 'importFile',
        //  getProjectAll:xui.ini.appPath + 'getProjectAll',


        /*************** tools API******************************/

        getLocalServiceToolBoxService: xui.ini.appPath + "api/getLocalServiceToolBox",
        getPageServiceToolBoxService: xui.ini.appPath + "api/getPageServiceToolBox",

        getProjectServiceToolBoxService: xui.ini.appPath + "api/getProjectServiceToolBox",


        getAllServerToolBoxService: xui.ini.appPath + "api/getAllServerToolBox",
        getRemoteServiceByKeyService: xui.ini.appPath + "api/getRemoteServiceByKey",
        getAPIService: xui.ini.appPath + "api/getAPIService",

        /*************** tools API******************************/

        getAllComponentsByPath: xui.ini.appPath + "plugs/getAllComponentsByPath",
        GetAllPackages: xui.ini.appPath + "plugs/getAllPackages",
        getAllProjectTreeSrvice: xui.ini.appPath + "plugs/getAllProjectTree",

        getAllClasses: xui.ini.appPath + "plugs/getAllClasses",

        getAllComponentsByClass: xui.ini.appPath + "plugs/getAllComponentsByClass",

        GetTableComponentsService: xui.ini.appPath + 'plugs/GetTableComponents',
        GetAllProjectTreeService: xui.ini.appPath + 'plugs/getAllProjectTree',
        GetExtComProjectTreeService: xui.ini.appPath + 'plugs/getExtComProjectTree',
        GetExtModuleProjectTreeService: xui.ini.appPath + 'plugs/getExtModuleProjectTree',


        getSelFontServcie: xui.ini.appPath + "plugs/getSelFont",
        getSelImgServcie: xui.ini.appPath + "plugs/getSelImg",

        getInnerImgServcie: xui.ini.appPath + "plugs/getInnerImg",
        getTableTreesService: xui.ini.appPath + 'plugs/fdt/GetTableTrees',
        GetAllTableByNameServcie: xui.ini.appPath + "plugs/fdt/GetAllTableByName",
        getSelImgByPathServcie: xui.ini.appPath + "plugs/getSelImgByPath",
        /*************** tools API******************************/


        /*************** DataBase API******************************/
        getDbTreesService: xui.ini.appPath + 'fdt/GetDbTrees',
        getColTreeService: xui.ini.appPath + 'fdt/GetColTree',
        AddColServcie: xui.ini.appPath + "fdt/AddCol",
        CreateViewServcie: xui.ini.appPath + "fdt/CreateView",
        CreateModuleServcie: xui.ini.appPath + "fdt/CreateModule",
        GetFolderTreeServcie: xui.ini.appPath + "fdt/GetFolderTree",

        GetAllCollByTableNameServcie: xui.ini.appPath + "fdt/GetAllCollByTableName",

        DropTableServcie: xui.ini.appPath + "fdt/DropTable",
        AddTableServcie: xui.ini.appPath + "fdt/AddTable",
        /*************** DataBase API******************************/


        getAllFileVersionService: xui.ini.appPath + "getAllFileVersion",
        delFileVersionService: xui.ini.appPath + "delFileVersion",
        getFileVersionService: xui.ini.appPath + "getFileVersionService",


        addFolder: xui.ini.appPath + 'addFolder',
        getAllClass: xui.ini.appPath + 'plugs/getAllClass',
        //getProjectConfigService: xui.ini.appPath + 'getProjectConfig',
        getProjectConfigService: 'xuiconf.js',

        addTextFile: xui.ini.appPath + 'addTextFile',
        addClass: xui.ini.appPath + 'addClass',
        addFromTpl: xui.ini.appPath + 'addFromTpl',
        addModule: xui.ini.appPath + 'addModule',
        addFile: xui.ini.appPath + 'addFile',
        delFile: xui.ini.appPath + 'delFile',
        upload: xui.ini.appPath + 'uploadFiles',
        copy: xui.ini.appPath + 'copy',
        copyClass: xui.ini.appPath + 'copyClass',
        copyFolder: xui.ini.appPath + 'copyFolder',
        copyFile: xui.ini.appPath + 'copyFolder',
        paste: xui.ini.appPath + 'paste',
        getTemp: xui.ini.appPath + 'paste',
        reName: xui.ini.appPath + 'reName',

        newProject: '/admin/createProject',
        uploadPath: "/plugins/fileupload/uploadgrid.html",
        openOtherFolderService: 'openFolder',
        testPath: xui.ini.appPath + 'debug.jsp',

        clientMode: 'project',

        getClientMode: function () {
            return 'project'
        },//ONF.clientMode},

        nonASCII: new RegExp("[\\u00DF\\u0590-\\u05F4\\u0600-\\u06FF\\u3040-\\u309F\\u30A0-\\u30FF\\u3400-\\u4DB5\\u4E00-\\u9FCC\\uAC00-\\uD7AF]"),

        fileNames: /^[^\\\/:*?"<>|\r\n]+$/im,
        fileNames2: /^[a-z][\w. ]*[0-9a-z]$/im,
        sizeFormat: /^(([1-9]\d*\.\d+)|([1-9]\d*)|(\.\d\d*))\s*(px|em)?$/,
        //defaultWS:'XUIWorkSpace',
        //wsPath:'XUIWorkSpace',

        defaultWS: 'form/userspace',
        wsPath: 'form/userspace',

        requestKey: 'RAD',
        requestKey2: 'RAD2',

        xui_page_tpl_tag: "xui_page_tpl",
        xui_project_tpl_tag: "xui_project_tpl",
        xui_module_tpl_tag: "xui_module_tpl",

        localGalleryPath: 'template/gallery',
        localTempalteLibPath: 'template/example/pages/',
        localProjectLibPath: 'template/example/projects/',
        localModlueLibPath: "template/modules",

        localWidgetLibPath: "RAD/widgets",

        myTempalteLibPath: 'template/example/pages/',
        myProjectLibPath: 'template/example/projects/',
        myModuleLibPath: "assets_mine/modules",

// can be csutomized
        onlineGalleryPath: 'http://www.itjds.net/services/assets_online/',
        onlineTempalteLibPath: 'http://www.itjds.net/services/assets_online/ver2/', // for 2 and upper version

        versionCheckingURL: 'http://www.itjds.net/services/latestversion.js',
        systemMessageURL: 'http://www.itjds.net/services/message.js',

        path_website: "http://www.itjds.net",
        path_forum: 'http://www.itjds.net/Forum/',
        path_download: 'http://www.itjds.net/download.html',
        path_licence: 'http://www.itjds.net/license.html',
        path_video: "http://www.itjds.net/videos/index.html",
        path_codesnipurl: "http://www.itjds.net/CodeSnip/Classes/",

        fileExts: /\.(jpg|png|gif|bmp|css|txt|swf|js|json|txt|html|htm|woff2|eot|svg|woff|ttf)$/,
        fileAccept: ".jpg,.png,.gif,.bmp,.css,.txt,.swf,.js,.json,.txt,.html,.htm,.woff2,.eot,.svg,.woff,.ttf",
        picExts: /\.(jpg|png|gif|bmp)$/,
        picAccept: ".jpg,.png,.gif,.bmp",

        img_widgets: xui.getPath('/RAD/img/', 'widgets.png'),

        img_dft: "{xui.ini.img_pic}",
        img_class: "xui-icon-xui",
        svg_dft: "http://www.itjds.net/RAD/img/svg_demo.svg",
        img_progress: xui.getPath('/RAD/img/', 'progress.gif'),

    });

    xui.merge(window.CONF, {
        localeItems: [{"id": "en", "caption": "$RAD.en"}, {"id": "cn", "caption": "$RAD.cn"}, {
            "id": "tw",
            "caption": "$RAD.tw"
        }, {"id": "ja", "caption": "$RAD.ja"}, {"id": "ru", "caption": "$RAD.ru"}],
        designer_list_class: ['xui-uisyle-mobile', 'xui-cursor-touch', 'xui-uiborder-circle', 'xui-uigradient', 'xui-ui-shadow', 'xui-css-noscroll'],
        designer_list_imageclass: (function () {
            var items = [];
            xui.arr.each(xui.toArr(xui.builtinFontIcon, true), function (i) {
                items.push({id: i, imageClass: i})
            });
            return items;
        })(),
        designer_list_imageclass2: (function () {
            var items = [];
            xui.arr.each(xui.toArr(xui.builtinFontIcon, true), function (i) {
                items.push({id: 'xuicon ' + i, imageClass: 'xuicon ' + i})
            });
            return items;
        })(),
        designer_data_fontfamily: [
            {
                caption: "<span style='font-family:arial,helvetica,sans-serif'>Arial</span>",
                id: "arial,helvetica,sans-serif"
            },
            {
                caption: "<span style='font-family:arial black,avant garde'>Arial Black</span>",
                id: "arial black,avant garde"
            },
            {
                caption: "<span style='font-family:comic sans ms,cursive'>Comic Sans MS</span>",
                id: "comic sans ms,cursive"
            },
            {
                caption: "<span style='font-family:courier new,courier,monospace'>Courier New</span>",
                id: "courier new,courier,monospace"
            },
            {caption: "<span style='font-family:georgia,serif'>Georgia</span>", id: "georgia,serif"},
            {caption: "<span style='font-family:impact,chicago'>impact</span>", id: "impact,chicago"},
            {
                caption: "<span style='font-family:lucida sans unicode,lucida grande,sans-serif'>Lucida Sans Unicode</span>",
                id: "lucida sans unicode,lucida grande,sans-serif"
            },
            {
                caption: "<span style='font-family:tahoma,geneva,sans-serif'>Tahoma</span>",
                id: "tahoma,geneva,sans-serif"
            },
            {
                caption: "<span style='font-family:times new roman,times,serif'>Times New Roman</span>",
                id: "times new roman,times,serif"
            },
            {
                caption: "<span style='font-family:trebuchet ms,helvetica,sans-serif'>Trebuchet MS</span>",
                id: "trebuchet ms,helvetica,sans-serif"
            },
            {
                caption: "<span style='font-family:verdana,geneva,sans-serif'>Verdana</span>",
                id: "verdana,geneva,sans-serif"
            },
            {caption: "<span style='font-family:wingdings'>WingDings</span>", id: "wingdings"}
        ],
        designer_zoom: ["", "width", "height", "contain", "cover", "width-resize", "height-resize", "contain-resize", "cover-resize", "1", "2", "3", "4", "0.75", "0.5", "0.25"],
        designer_themes: ["", "default", "army", "darkblue", "electricity", "lightblue", "moonify", "orange", "pink", "red", "vista"],
        designer_themes2: ["", "default", "army", "classic", "darkblue", "electricity", "lightblue", "moonify", "orange", "pink", "red", "vista", "webflat"],
        designer_background_position: ["", "top left", "top center", "top right", "center left", "center center", "center right", "bottom left", "bottom center", "bottom right", "0% 0%", "-0px -0px"],
        designer_background_repeat: ["", "repeat", "repeat-x", "repeat-y", "no-repeat"],
        designer_background_attachment: ["", "scroll", "fixed"],
        designer_background_size: ["", "cover", "contain", "100% 100%", "50%", "0px 0px", "0px"],
        designer_background_origin: ["", "padding-box", "border-box", "content-box"],
        designer_background_clip: ["", "border-box", "padding-box", "content-box"],
        designer_data_fontsize: ["", "12px", "14px", "16px", "20px", "28px"],
        designer_data_fontweight: ["", "normal", "bolder", "bold", "lighter", "100", "200", "300", "400", "500", "600", "700", "800", "900"],
        designer_data_fontstyle: ["", "normal", "italic", "oblique"],
        designer_data_textdecoration: ["", "none", "underline", "overline", "line-through", "blink"],
        designer_data_textalign: ["", "left", "right", "center", "justify"],
        designer_data_textanchor: ["", "start", "middle", "end"],
        designer_data_target: ["", "_blank"],
        designer_data_strokelinecap: ["", "butt", "square", "round"],
        designer_data_strokelinejoin: ["", "bevel", "round", "miter"],
        designer_data_strokedasharray: ["", "-", ".", "-.", "-..", ". ", "- ", "--", "- .", "--.", "--.."],
        designer_data_cursor: ["", "default", "text", "pointer", "move", "crosshair", "wait", "help", "e-resize", "ne-resize", "nw-resize", "n-resize", "se-resize", "sw-resize", "s-resize", "w-resize"],
        designer_data_overflow: ['', 'visible', 'hidden', 'scroll', 'auto', 'overflow-x:hidden;overflow-y:auto', 'overflow-x:auto;overflow-y:hidden']
    });
    var tagVar = {
            id: 'tagVar',
            type: "button",
            cellRenderer: function () {
                return xui.adjustRes("[ $RAD.designer.conf.Object ]")
            },
            event: function (profile, cell, pro) {
                var ns = profile.host,
                    node = profile.getSubNode('CELL', cell._serialId),
                    text = cell.obj || {},
                    _cb = function (obj) {
                        cell.obj = typeof(obj) == 'string' ? xui.unserialize(obj) : obj;
                        cell.needCollect = 1;
                        ns._dirty = 1;
                        node.focus();
                    };

                if (window.SPA && false === SPA.fe("beforeObjectEditorPop", [
                    "jsonEditor", "tagVar", text, _cb,
                    "conf", xui.stringify(text), node.get(0), profile.boxing(), pro && pro.boxing()])) return;

                xui.ModuleFactory.getCom('jsonEditor', function () {
                    this.setProperties({
                        caption: 'tagVar',
                        imageClass: 'spafont spa-icon-hash',
                        text: text,
                        onOK: function (obj, txt) {
                            _cb(xui.clone(obj.properties.object));
                        }
                    });
                    this.show();
                });
            }
        }, tagCmds = {
            id: 'tagCmds',
            type: "button",
            cellRenderer: function () {
                return xui.adjustRes("[ $RAD.designer.conf.Object ]")
            },
            event: function (profile, cell, editorprf) {
                var ns = profile.host,
                    node = profile.getSubNode('CELL', cell._serialId),
                    obj = cell.obj || [];
                RAD.Designer.prototype._showMixedEditor(profile._targetProfile, "xui.UI.List", profile._targetPropKey + "." + cell._col.id, "tagCmds", obj, node, function (items) {
                    cell.obj = xui.clone(items, true);
                    cell.needCollect = 1;
                    ns._dirty = 1;
                    node.focus();
                }, true);
            }
        }
    ;

    xui.merge(window.CONF, {
        adjustRelativePath: function (path) {
            if (!path) return '';
            var t;
            path = path.replace(/\\/g, "/");

            if (t = window['/']) {
                t = t.replace(/\\/g, "/") + "/";
                if (xui.str.startWith(path, t))
                    path = path.replace(t, "{/}");
            }

            if (t = SPA.curProjectPath) {
                t = t.replace(/\\/g, "/") + "/";
                if (xui.str.startWith(path, t))
                    path = path.replace(t, "{/}");
            }
            return path;
        },
        getAPIPath: function () {
            return "API/";
        },
        propBinder: false,
        PropBinderBlackList: null,
        PropBinderWhiteList: null,
        widgets_xprops: {
            'xui.Module': ["onReady", "onRender", "onModulePropChange", "onMessage", "onFragmentChanged"],
            'xui.APICaller': ["name", "queryURL", "requestType", "responseType", "queryMethod", "queryOptions", "queryArgs", "responseCallback", "beforeData", "onError", "onData", "proxyInvoker"],
            'xui.MQTT': ["server", "useSSL", "port", "path", "clientId", "userName", "password", "subscribers", "onMsgArrived"],
            'xui.Timer': ['interval', 'autoStart', 'onTime'],
            'xui.AnimBinder': ['type', 'frames'],
            'xui.MessageService': ['msgType', 'asynReceive', 'onMessageReceived'],
            'xui.UI.CSSBox': ['html', 'className', 'customCss', 'sandbox', 'normalStatus', 'hoverStatus', 'activeStatus'],
            'xui.UI.Element': ['html', 'nodeName', 'position', 'className', 'attributes'],
            'xui.UI.Icon': ['imageClass', 'dock', 'iconFontSize', 'iconColor', 'onClick'],
            'xui.UI.Span': ['html', 'position', 'className', 'dock'],
            'xui.UI.Label': ['caption', 'clock', 'fontSize', 'fontWeight', 'fontColor', 'fontFamily', 'imageClass', 'iconFontCode', 'hAlign', 'className', 'dock', 'onClick'],
            'xui.UI.Link': ['caption', 'dock', 'target', 'onClick'],
            'xui.UI.HTMLButton': ['caption', 'fontSize', 'visibility', 'fontWeight', 'fontColor', 'fontFamily', 'position', 'className', 'attributes', 'onClick'],
            'xui.UI.Button': ['type', 'value', 'caption', 'visibility', 'fontSize', 'fontWeight', 'fontColor', 'fontFamily', 'imageClass', 'iconFontCode', 'dirtyMark', 'onClick', 'onChange'],
            'xui.UI.CheckBox': ['value', 'caption', 'readonly', 'disabled', 'imageClass', 'iconFontCode', 'dock', 'dirtyMark', 'onChange'],
            'xui.UI.Input': ['value', 'readonly', 'disabled', 'type', 'valueFormat', 'labelPos', 'labelSize', 'labelCaption', 'dock', 'dirtyMark', 'onChange'],
            'xui.UI.HiddenInput': ['value'],
            'xui.UI.RichEditor': ['value', 'dock', 'readonly', 'disabled', 'dirtyMark', 'onChange'],
            'xui.UI.List': ['items', 'value', 'readonly', 'disabled', 'dock', 'dirtyMark', 'onItemSelected', 'onChange'],
            'xui.UI.ComboInput': ['type', 'imageClass', 'readonly', 'disabled', 'iconFontCode', 'commandBtn', 'value', 'labelSize', 'labelCaption', 'dock', 'dirtyMark', 'items', 'onClick', 'onCommand', 'onChange', 'beforeComboPop', 'beforePopShow'],
            'xui.UI.ProgressBar': ['value', 'captionTpl', 'type', 'fillBG', 'dock', 'dirtyMark', 'onChange'],
            'xui.UI.Slider': ['value', 'dock', 'dirtyMark', 'onChange'],
            'xui.UI.RadioBox': ['items', 'value', 'readonly', 'disabled', 'dock', 'dirtyMark', 'onItemSelected', 'onChange'],
            'xui.UI.DatePicker': ['value', 'dock', 'readonly', 'disabled', 'dirtyMark', 'onChange'],
            'xui.UI.TimePicker': ['value', 'dock', 'dirtyMark', 'onChange'],
            'xui.UI.ColorPicker': ['value', 'dock', 'dirtyMark', 'onChange'],
            'xui.UI.StatusButtons': ['items', 'value', 'onItemSelected'],
            'xui.UI.FoldingList': ['items', 'activeLast', 'onItemSelected'],
            'xui.UI.Gallery': ['items', 'value', 'iconList', 'dock', 'onItemSelected'],
            'xui.UI.Opinion': ['items', 'value', 'iconGalleryList', 'dock', 'onItemSelected'],
            'xui.UI.PageBar': ['caption', 'value', 'textTpl', 'prevMark', 'nextMark', 'onPageSet'],
            'xui.UI.PopMenu': ['items', 'onMenuSelected', 'onClick'],
            'xui.UI.MenuBar': ['items', 'dock', 'valueSeparator', 'onMenuSelected', 'onMenuBtnClick', 'onClick'],

            'xui.UI.SystemMenu': ['items', 'value', 'dock', 'onItemSelected', 'onChange'],
            'xui.UI.TreeBar': ['items', 'value', 'dock', 'onItemSelected', 'onChange'],

            'xui.UI.ToolBar': ['items', 'dock', 'onClick'],

            'xui.UI.TreeView': ['items', 'value', 'dock', 'onItemSelected', 'onChange'],
            'xui.UI.TreeGrid': ['header', 'uidColumn', 'rows', 'value', 'expression', 'dock', 'altRowsBg', 'headerHeight', 'selMode', 'rowNumbered', 'onClickRow', 'onDblclickRow', 'onRowSelected', 'afterRowActive'],

            'xui.UI.Image': ['src', 'items', 'activeItem', 'cursor', 'dock', 'onClick'],
            'xui.UI.Flash': ['src', 'parameters', 'flashvars', 'dock'],
            'xui.UI.FormLayout': ['floatHandler', 'dock', 'solidGridlines', 'stretchH', 'stretchHeight', 'layoutData'],
            'xui.UI.Audio': ['src', 'onMediaEvent'],


            'xui.UI.FileUpload': ['params', 'prepareFormData', 'dock', 'uploadUrl', 'uploadfail', 'uploadcomplete'],
            'xui.UI.Video': ['src', 'onMediaEvent'],
            'xui.UI.ECharts': ['chartOption', 'dataset', 'chartRenderer', 'expression', 'chartTheme', 'onMouseEvent', 'onChartEvent'],
            'xui.UI.FusionChartsXT': ['chartType', 'JSONData', 'expression', 'configure', 'onDataClick', 'onLabelClick', 'onAnnotationClick'],
// container
            'xui.UI.Div': ['html', 'dropKeys', 'conLayoutColumns', 'dock', 'overflow', 'dataBinder', 'sandboxTheme'],
            'xui.UI.Block': ['html', 'dropKeys', 'borderType', 'background', 'conLayoutColumns', 'dock', 'overflow', 'dataBinder', 'sandboxTheme'],
            'xui.UI.Group': ['caption', 'html', 'dropKeys', 'conLayoutColumns', 'dock', 'overflow', 'dataBinder', 'sandboxTheme', 'beforeExpand', 'afterFold', 'onIniPanelView'],
            'xui.UI.Panel': ['caption', 'html', 'imageClass', 'dropKeys', 'conLayoutColumns', 'dock', 'overflow', 'dataBinder', 'sandboxTheme', 'beforeExpand', 'afterFold', 'onIniPanelView'],
            'xui.UI.Tabs': ['items', 'value', 'conLayoutColumns', 'dock', 'overflow', 'dataBinder', 'sandboxTheme', 'onItemSelected', 'onIniPanelView'],
            'xui.UI.Stacks': ['items', 'value', 'conLayoutColumns', 'dock', 'overflow', 'dataBinder', 'sandboxTheme', 'onItemSelected', 'onIniPanelView'],
            'xui.UI.ButtonViews': ['items', 'value', 'conLayoutColumns', 'dock', 'overflow', 'dataBinder', 'sandboxTheme', 'barLocation', 'barHAlign', 'barVAlign', 'barSize', 'sideBarStatus', 'onItemSelected', 'onIniPanelView'],
            'xui.UI.FoldingTabs': ['items', 'value', 'conLayoutColumns', 'dock', 'overflow', 'dataBinder', 'sandboxTheme', 'onItemSelected', 'onIniPanelView'],
            'xui.UI.Layout': ['type', 'items', 'conLayoutColumns', 'dock', 'overflow', 'dataBinder', 'sandboxTheme', 'onGetContent'],

            'xui.UI.Dialog': ['caption', 'modal', 'minBtn', 'imageClass', 'maxBtn', 'dock', 'html', 'overflow', 'sandboxTheme', 'beforeClose'],
            'xui.UI.SVGPaper': ['conLayoutColumns', 'dock', 'dropKeys', 'overflow', 'dataBinder', 'sandboxTheme'],
//svg
            'xui.svg.circle': ['attr', 'animDraw', 'offsetFlow', 'onClick'],
            'xui.svg.ellipse': ['attr', 'animDraw', 'offsetFlow', 'onClick'],
            'xui.svg.rect': ['attr', 'animDraw', 'offsetFlow', 'onClick'],
            'xui.svg.image': ['attr', 'onClick'],
            'xui.svg.text': ['attr', 'onClick'],
            'xui.svg.path': ['attr', 'animDraw', 'offsetFlow', 'onClick'],
            'xui.svg.rectComb': ['attr', 'animDraw', 'offsetFlow', 'hAlign', 'vAlign', 'onClick'],
            'xui.svg.circleComb': ['attr', 'animDraw', 'offsetFlow', 'hAlign', 'vAlign', 'onClick'],
            'xui.svg.ellipseComb': ['attr', 'animDraw', 'offsetFlow', 'hAlign', 'vAlign', 'onClick'],
            'xui.svg.pathComb': ['attr', 'animDraw', 'offsetFlow', 'hAlign', 'vAlign', 'onClick'],
            'xui.svg.imageComb': ['attr', 'hAlign', 'vAlign', 'onClick'],
            'xui.svg.connector': ['attr', 'animDraw', 'offsetFlow', 'hAlign', 'vAlign', 'onClick']
        },
        widgets_prop_cat: [
            {
                id: "Data & Form",
                items: ['value', 'attributes', 'readonly', 'disabled', 'tag', 'tagVar', 'formMethod', 'formTarget', 'formAction', 'formEnctype', 'formDataPath']
            },
            {
                id: "Visibility, Size and Dimensions",
                items: ['visibility', 'position', 'zIndex', 'width', 'height', 'align', 'hAlign', 'vAlign']
            },

            {
                id: 'Caption & Tips',
                items: ['caption', 'fontSize', 'fontWeight', 'fontColor', 'fontFamily', 'imageClass', 'labelCaption', 'labelHAlign']
            },

            {
                id: "Container",
                items: ['sandboxTheme', 'overflow', 'panelBgClr', 'panelBgImg', 'iframeAutoLoad']
            },
            {
                id: "Items",
                items: ['listKey', 'items', 'header', 'rows', 'grpCols', 'rawData', 'tagCmds', 'tagCmdsAlign']
            },
            {
                id: "Other Prop",
                items: ['className', 'selectable', 'defaultFocus']
            }
        ],
        widgets_events_cat: [
            {
                id: "Layout",
                items: ['beforeRender', 'onRender']
            },

            {
                id: "Keyboard",
                items: ['beforeKeypress']
            },

            {
                id: "swipe",
                items: [
                    'swipe', 'swipeleft', 'swiperight', 'swipeup', 'swipedown'
                ]
            },
            {
                id: "pan",
                items: [
                    'pan', 'panstart','panmove','panend','pancancel','panleft','panright','panup','pandown'
                ]
            },
            {
                id: "pinch",
                items: [
                    'pinch', 'pinchstart','pinchmove', 'pinchend', 'pinchcancel', 'pinchin', 'pinchout'
                ]
              },
            {
                id: "press",
                items: [
                    'press', 'pressup'
                ]
            },
            {
                id: "rotate",
                items: [
                    'rotate','rotatestart', 'rotatemove','rotateend', 'rotatecancel',
                ]
            },


            {
                id: "Mouse & Touch",
                items: ['onClick', 'onDblclick', 'onCmd', 'onContextmenu','touchstart','touchmove','touchend','touchcancel',
                    'pan', 'panstart','panmove','panend','pancancel','panleft','panright','panup','pandown',
                    'pinch', 'pinchstart','pinchmove', 'pinchend', 'pinchcancel', 'pinchin', 'pinchout',
                    'press', 'pressup',
                    'rotate','rotatestart', 'rotatemove','rotateend', 'rotatecancel',
                    'swipe', 'swipeleft', 'swiperight', 'swipeup', 'swipedown'
                ]
                //

            },
            {

                id: 'Editor',
                items: ['onEditorClick', 'onEndEdit']
            },
            {
                id: "Items",
                items: ['onIniPanelView', 'onGetContent', 'beforeFold', 'afterFold', 'beforeExpand', 'afterExpand']
            },
            {
                id: "XUI Module Events",
                items: ['onReady', 'onFragmentChanged', 'onMessage', 'afterIniComponents', 'afterShow']
            },
            {
                id: "Other Events",
                items: ['onBlur', 'onFocus', 'afterPropertyChanged', 'onShow']
            }
        ],
        lock_hidden_prop: {
            zIndex: 1,
            tabindex: 1,
            position: 1,
            left: 1,
            top: 1,
            bottom: 1,
            right: 1,
            width: 1,
            height: 1,
            rotate: 1,
            CC: 1,
            CS: 1,
            dock: 1,
            dockMargin: 1,
            dockOrder: 1,
            dockMinW: 1,
            dockMinH: 1,
            dockMaxW: 1,
            dockMaxH: 1,
            dockFloat: 1,
            dockIgnore: 1,
            dockIgnoreFlexFill: 1,
            dockStretch: 1,
            conDockPadding: 1,
            conDockSpacing: 1,
            conDockFlexFill: 1,
            conDockStretch: 1,
            conDockRelative: 1,
            conLayoutColumns: 1,
            visibility: 1,
            display: 1,
            overflow: 1
        },

        widgets_itemsProp: {
            items_conf: {
                id: {type: "input"},
                path: {type: "input"},
                target: {type: "input"},
                childname: {type: "childname"},
                caption: {type: "input"},
                title: {type: "input"},
                text: {type: "textarea"},
                url: {type: "input"},
                height: {type: "number"},

                creatorName: {type: "input"},
                createDateStr: {type: "input"},

                capDisplay: {type: "checkbox"},
                add: {type: "input"},
                comment: {type: "input"},
                type: {type: "listbox", editorListItems: ["", "split", "button", "checkbox", "radiobox"]},
                value: {type: "checkbox"},
                handler: {type: "checkbox"},
                size: {type: "spin", precision: 0, increment: 1, min: 0, max: 10000},
                maxlength: {id: "maxlength", type: "spin", precision: 0, increment: 1, min: 0, max: 10000},
                min: {type: "spin", precision: 0, increment: 1, min: 0, max: 10000},
                max: {type: "spin", precision: 0, increment: 1, min: 0, max: 10000},
                locked: {type: "checkbox"},
                folded: {type: "checkbox"},
                cmd: {type: "checkbox"},
                renderer: {type: "input"},
                imageClass: {
                    type: "combobox",
                    editorCommandBtn: 'pop',
                    editorDropListWidth: '18em',
                    editorListItems: CONF.designer_list_imageclass,
                    tagVar: {
                        prop: 'imageClass'
                    }
                },
                iconFontCode: {type: 'input'},
                iconFontSize: {type: "combobox", editorListItems: CONF.designer_data_fontsize},
                iconStyle: {type: 'input'},
                itemClass: {type: "input"},
                itemStyle: {type: "input"},
                image: {
                    type: CONF.getClientMode() == 'singlepage' ? "input" : "popbox",
                    event: function (profile, cell, editorprf) {
                        var node = profile.getSubNode('CELL', cell._serialId),
                            _cb = function (path) {
                                editorprf.boxing().setUIValue(CONF.adjustRelativePath(path));
                                node.focus();
                            };
                        var t = profile._targetProfile,
                            k = profile._targetPropKey;
                        if (window.SPA && false === SPA.fe("beforeURISelectorPop", [
                            k + ':item:image', cell.value, _cb,
                            'conf', t && t['xui.absProfile'] ? t.boxing() : t, node.get(0), cell, profile.boxing(), editorprf && editorprf.boxing()
                        ])) return;

                        if (CONF.getClientMode() != 'singlepage') {
                            xui.ModuleFactory.getCom('RAD.ImageSelector', function () {
                                this.setProperties({
                                    fromRegion: node.cssRegion(true),
                                    onOK: function (obj, path) {
                                        _cb(path);
                                    }
                                });
                                this.show();
                            });
                        }
                    }
                },
                imagePos: {type: "combobox", editorListItems: CONF.designer_background_repeat},
                imageBgSize: {type: "combobox", editorListItems: CONF.designer_background_size},
                imageRepeat: {type: "combobox", editorListItems: CONF.designer_background_repeat},
                itemMargin: {type: "input"},
                itemWidth: {
                    type: "input",
                    editorHAlign: 'right',
                    cellStyle: "text-align:right",
                    editorFormat: CONF.sizeFormat
                },
                itemHeight: {
                    type: "input",
                    editorHAlign: 'right',
                    cellStyle: "text-align:right",
                    editorFormat: CONF.sizeFormat
                },
                itemAlign: {type: "listbox", editorListItems: ["", "left", "center", "right"]},
                group: {type: "checkbox"},
                closeBtn: {type: "checkbox"},
                optBtn: {type: "checkbox"},
                popBtn: {type: "checkbox"},
                panelBgClr: {type: "color"},
                panelBgImg: {
                    type: CONF.getClientMode() == 'singlepage' ? "input" : "popbox",
                    event: function (profile, cell, editorprf) {
                        var node = profile.getSubNode('CELL', cell._serialId),
                            _cb = function (path) {
                                editorprf.boxing().setUIValue(CONF.adjustRelativePath(path));
                                node.focus();
                            };
                        var t = profile._targetProfile,
                            k = profile._targetPropKey;
                        if (window.SPA && false === SPA.fe("beforeURISelectorPop", [
                            k + ':item:panelBgImg', cell.value, _cb,
                            'conf', t && t['xui.absProfile'] ? t.boxing() : t, node.get(0), cell, profile.boxing(), editorprf && editorprf.boxing()
                        ])) return;

                        if (CONF.getClientMode() != 'singlepage') {
                            xui.ModuleFactory.getCom('RAD.ImageSelector', function () {
                                this.setProperties({
                                    fromRegion: node.cssRegion(true),
                                    onOK: function (obj, path) {
                                        _cb(path);
                                    }
                                });
                                this.show();
                            });
                        }
                    }
                },
                panelBgImgPos: {type: "combobox", editorListItems: CONF.designer_background_position},
                panelBgImgRepeat: {type: "combobox", editorListItems: CONF.designer_background_repeat},
                panelBgImgAttachment: {type: "combobox", editorListItems: CONF.designer_background_attachment},
                overflow: {type: "combobox", editorListItems: CONF.designer_data_overflow},
                iconFontSize: {type: "combobox", editorListItems: CONF.designer_data_fontsize},
                fontSize: {type: "combobox", editorListItems: CONF.designer_data_fontsize},
                fontWeight: {type: "combobox", editorListItems: CONF.designer_data_fontweight},
                html: {type: "textarea"},
                iframeAutoLoad: {type: "input"},//{url:/*String*/, query:/*String or Oject*/, method/*"get" or "set"*/, enctype:/*String*/},
                ajaxAutoLoad: {type: "input"},//{url:/*String*/, query:/*String or Oject*/,options:/*Obejct*/, refer to xui.Ajax function arguments},
                disabled: {type: "checkbox"},
                readonly: {type: "checkbox"},
                hidden: {type: "checkbox"},
                iniFold: {type: "checkbox", dftValue: true},
                showMark: {type: "checkbox"},
                noIcon: {type: "checkbox"},
                tag: {type: "input"},
                tagCmdsAlign: {id: "tagCmdsAlign", type: "combobox", editorListItems: ["left", "right", "floatright"]},
                buttonType: {id: "buttonType", type: "combobox", editorListItems: ["button", "image", "text", "split"]},
                tagVar: tagVar,
                tagCmds: tagCmds
            },

            items: {
                "xui.UI.List": {
                    header: ["id", {
                        id: 'type',
                        type: "listbox",
                        editorListItems: ["", "split"]
                    }, "caption", "imageClass", "disabled", "hidden", "tagCmds", "tips", "tag", "tagVar"],
                    uniqueKey: "id"
                },
                "xui.UI.RadioBox": {
                    header: ["id", "caption", "itemClass", "iconStyle", "disabled", "hidden", "tips", "tag", "tagVar"],
                    uniqueKey: "id"
                },
                "xui.UI.FoldingList": {
                    header: ["id", "caption", 'className', "title", "text", "imageClass", "disabled", "hidden", "tag", "tagVar", "tagCmds", "tips"],
                    uniqueKey: "id"
                },
                "xui.UI.ComboInput": {
                    header: ["id", "caption", "imageClass", "iconFontSize", "disabled", "hidden", "tagCmds", "tips", "tag", "tagVar"],
                    uniqueKey: "id"
                },
                "xui.UI.StatusButtons": {
                    header: ["id", "caption", "renderer", {
                        id: 'itemType',
                        type: "listbox",
                        editorListItems: ["text", "button", "dropButton"]
                    }, "imageClass", "disabled", "hidden", "tag", "tagVar", "tips"],
                    uniqueKey: "id"
                },
                "xui.UI.Tabs": {
                    header: ["id", "caption", 'className', "url", "imageClass", "closeBtn", "optBtn", "popBtn", "panelBgClr", "panelBgImg", "overflow", "html", "iframeAutoLoad", "ajaxAutoLoad", "disabled", "hidden", "tag", "tagVar", "tagCmds", "tips"],
                    uniqueKey: "id"
                },
                "xui.UI.Stacks": {
                    header: ["id", "caption", 'className', "imageClass", "iconFontCode", "closeBtn", "optBtn", "popBtn", "panelBgClr", "panelBgImg", "overflow", "html", "iframeAutoLoad", "ajaxAutoLoad", "disabled", "hidden", "tag", "tagVar", "tagCmds", "tips"],
                    uniqueKey: "id"
                },
                "xui.UI.ButtonViews": {
                    header: ["id", "caption", 'className', "imageClass", "iconFontCode", "closeBtn", "optBtn", "popBtn", "panelBgClr", "panelBgImg", "overflow", "html", "iframeAutoLoad", "ajaxAutoLoad", "disabled", "hidden", "tag", "tagVar", "tagCmds", "tips"],
                    uniqueKey: "id"
                },
                "xui.UI.FoldingTabs": {
                    header: ["id", "caption", 'className', "height", "imageBgSize", "imageRepeat", "imageClass", "iconFontCode", "iconFontSize", "iconStyle", "closeBtn", "optBtn", "popBtn", "panelBgClr", "panelBgImg", "overflow", "html", "iframeAutoLoad", "ajaxAutoLoad", "disabled", "hidden", "tag", "tagVar", "tagCmds", "tips"],
                    uniqueKey: "id"
                },
                "xui.UI.Gallery": {
                    header: ["id", "caption", "className", "capDisplay", "renderer", "comment", "itemClass", "itemStyle", "image", "imageClass", "iconFontCode", "iconFontSize", "iconStyle", "disabled", "hidden", "itemWidth", "itemHeight", "itemPadding", "itemMargin", "flagText", "flagClass", "flagStyle", "tag", "tagVar", "tips"],
                    uniqueKey: "id"
                },
                "xui.UI.Opinion": {
                    header: ["id", "caption", "creatorName", "createDateStr", "capDisplay", "renderer", "comment", "itemClass", "itemStyle", "image", "imageClass", "iconFontCode", "iconFontSize", "iconStyle", "disabled", "hidden", "itemWidth", "itemHeight", "itemPadding", "itemMargin", "flagText", "flagClass", "flagStyle", "tag", "tagVar", "tips"],
                    uniqueKey: "id"
                },
                "xui.UI.Image": {
                    header: ["id", "image", "alt", "tips"],
                    uniqueKey: "id"
                },
                "xui.UI.TreeBar": {
                    header: ["id", {
                        id: 'type',
                        type: "listbox",
                        editorListItems: ["", "split"]
                    }, "caption", 'className', "imageClass", "group", "disabled", "hidden", "tag", "tagVar", "iniFold", "tips"],
                    uniqueKey: "id",
                    hasSub: true
                },
                "xui.UI.SystemMenu": {
                    header: ["id", "caption", "path", "target", "childname", "imageClass", "group", "disabled", "hidden", "tag", "iniFold", "tips"],
                    uniqueKey: "id",
                    hasSub: true
                },
                "xui.UI.TreeView": {
                    header: ["id", {
                        id: 'type',
                        type: "listbox",
                        editorListItems: ["", "split"]
                    }, "caption", "className", "imageClass", "group", "disabled", "hidden", "tag", "tagVar", "tagCmds", "iniFold", "tips"],
                    uniqueKey: "id",
                    hasSub: true
                },
                "xui.UI.PopMenu": {
                    header: ["id", "caption", 'className', "group", "type", "value", "add", "imageClass", "disabled", "hidden", "tag", "tagVar", "tips"],
                    uniqueKey: "id",
                    hasSub: 1
                },
                "xui.UI.MenuBar": {
                    header: ["id", "caption", 'className', "group", "type", "value", "add", "imageClass", "iconFontCode", "disabled", "hidden", "tag", "tagVar", "tips"],
                    uniqueKey: "id",
                    hasSub: 1
                },
                "xui.UI.ToolBar": {
                    header: ["id", "hidden", "handler"],
                    subheader: ["id", "caption", "label", {
                        id: 'type',
                        type: "listbox",
                        editorListItems: ["", "button", "dropButton", "statusButton", "profile", "split"]
                    }, "value", "renderer", "imageClass", "iconFontCode", "disabled", "hidden", "tag", "tagVar", "tips"],
                    uniqueKey: "id",
                    subFixedRows: false,
                    innerKey: 'sub'
                },
                "xui.UI.Layout": {
                    header: ["id", "size", "min", "max", "locked", "folded", "hidden", "cmd", "panelBgClr", "panelBgImg", "panelBgImgPos", "overflow", "html", "itemClass", "itemStyle", "tag"],
                    rows: {
//                        'before':[{type:'label'},{type:'label'}],
                        'main': {
                            tag: 'candel',
                            cells: [{type: 'label'}, {}, {}, {type: 'label', value: "-"}, {
                                type: 'label',
                                value: "-"
                            }, {type: 'label', value: "-"}, {type: 'label', value: "-"}, {type: 'label', value: "-"}]
                        }
//                        'after':[{type:'label'},{type:'label'}]
                    }
                }
            },
            header: {
                "xui.UI.TreeGrid": {
                    header: [
                        {id: "id", type: 'input'},
                        {id: "caption", type: 'input'},
                        {
                            id: 'type',
                            type: "listbox",
                            editorListItems: ',label,input,textarea,combobox,listbox,file,getter,helpinput,button,dropbutton,cmdbox,popbox,date,time,datetime,color,spin,counter,currency,number,checkbox,progress,cmd'.split(',')
                        },
                        {
                            id: "formula", type: 'input', editorEvents: {
                                _onChange: function (p, ov, v) {
                                    if (false === xui.ExcelFormula.validate(v)) {
                                        xui.alert("It's not a valid formula");
                                    }
                                }
                            }
                        },
                        {id: "defaultValue", type: "input"},
                        {id: "flexSize", type: "checkbox"},
                        {
                            id: "width",
                            type: "input",
                            editorHAlign: 'right',
                            cellStyle: "text-align:right",
                            editorFormat: CONF.sizeFormat
                        },
                        {
                            id: "minWidth",
                            type: "input",
                            editorHAlign: 'right',
                            cellStyle: "text-align:right",
                            editorFormat: CONF.sizeFormat
                        },
                        {
                            id: "maxWidth",
                            type: "input",
                            editorHAlign: 'right',
                            cellStyle: "text-align:right",
                            editorFormat: CONF.sizeFormat0
                        },
                        {id: "colResizer", type: "checkbox"},
                        {id: "headerStyle", type: 'input'},
                        {id: "headerClass", type: 'input'},
                        {id: "colRenderer", type: "input"},
                        {id: "hidden", type: 'checkbox'},
//sortby : Function(x,y){return -1:0:1;}, custom sortby function
                        {id: "cellRenderer", type: "input"},
                        {id: "cellCapTpl", type: 'input'},
                        {id: 'cellClass', type: 'input'},
                        {id: 'cellStyle', type: 'input'},
                        {id: "disabled", type: "checkbox"},
                        {id: "readonly", type: "checkbox"},
                        {id: "editable", type: "checkbox"},
                        {id: "editMode", type: "listbox", editorListItems: ["focus", "sharp", "hover", "inline"]},
                        {
                            id: "editorListItems",
                            cellRenderer: function () {
                                return xui.adjustRes("[ $RAD.designer.conf.Object ]")
                            },
                            type: "button",
                            event: function (profile, cell, editorprf) {
                                var ns = profile.host,
                                    node = profile.getSubNode('CELL', cell._serialId),
                                    obj = cell.obj || [];
                                RAD.Designer.prototype._showMixedEditor(profile._targetProfile, "xui.UI.List", "header." + cell._col.id, "items", obj, node, function (items) {
                                    cell.obj = xui.clone(items, true);
                                    cell.needCollect = 1;
                                    ns._dirty = 1;
                                    node.focus();
                                }, true);
                            }
                        },
                        {
                            id: "editorDropListWidth",
                            type: "input",
                            editorHAlign: 'right',
                            cellStyle: "text-align:right",
                            editorFormat: CONF.sizeFormat
                        },
                        {
                            id: "editorDropListHeight",
                            type: "input",
                            editorHAlign: 'right',
                            cellStyle: "text-align:right",
                            editorFormat: CONF.sizeFormat
                        },
                        {id: "precision", type: "spin", precision: 0, increment: 1, min: 0, max: 100},
                        {id: "increment", type: "number", precision: 2, increment: 0.01},
                        {id: "min", type: "number", precision: 2, increment: 0.01},
                        {id: "max", type: "number", precision: 2, increment: 0.01},
                        {id: "maxlength", type: "spin", precision: 0, increment: 1, min: 0, max: 10000},
                        {id: "groupingSeparator", type: "input", maxlength: 1},
                        {id: "decimalSeparator", type: "input", maxlength: 1},
                        {id: "forceFillZero", type: "checkbox"},
                        {id: "numberTpl", type: "input"},
                        {id: "currencyTpl", type: "input"},
                        {id: "dateEditorTpl", type: "input"},
                        {id: "tips", type: "input"},
                        {id: "tag", type: "input"},
                        tagVar
                    ],
                    uniqueKey: "id"
                }
            },
            grpCols: {
                "xui.UI.TreeGrid": {
                    header: [
                        {id: "id", type: 'input'},
                        {id: "caption", type: 'input'},
                        {id: "from", type: "spin", precision: 0, increment: 1, min: 0},
                        {id: "to", type: "spin", precision: 0, increment: 1, min: 0},
                        {id: "colResizer", type: "checkbox"},
                        {id: "headerStyle", type: 'input'},
                        {id: "headerClass", type: 'input'},
                        {id: "tips", type: "input"},
                        {id: "tag", type: "input"},
                        tagVar
                    ],
                    uniqueKey: "id"
                }
            },
            rows: {
                "xui.UI.TreeGrid": {
                    header: [

                        //cells : Array, row's cells data
                        //sub : Array or [true], sub rows data

                        {id: "id", type: 'input'},
                        {id: "caption", type: 'input'},
                        {
                            id: 'type',
                            type: "listbox",
                            editorListItems: ',label,input,textarea,combobox,listbox,file,getter,helpinput,button,dropbutton,cmdbox,popbox,date,time,datetime,color,spin,counter,currency,number,checkbox,progress,cmd'.split(',')
                        },
                        {
                            id: "formula", type: 'input', editorEvents: {
                                _onChange: function (p, ov, v) {
                                    if (false === xui.ExcelFormula.validate(v)) {
                                        xui.alert("It's not a valid formula");
                                    }
                                }
                            }
                        },
                        {
                            id: "height",
                            type: "input",
                            editorHAlign: 'right',
                            cellStyle: "text-align:right",
                            editorFormat: CONF.sizeFormat
                        },
                        {id: "rowStyle", type: 'input'},
                        {id: "rowClass", type: 'input'},
                        {id: "firstCellStyle", type: 'input'},
                        {id: "firstCellClass", type: 'input'},
                        {id: "group", type: "checkbox"},
                        {id: "hidden", type: 'checkbox'},
                        {id: "rowResizer", type: "checkbox"},
                        {id: "rowRenderer", type: "input"},
//sortby : Function(x,y){return -1:0:1;}, custom sortby function
                        {id: "cellRenderer", type: "input"},
                        {id: "cellCapTpl", type: 'input'},
                        {id: 'cellClass', type: 'input'},
                        {id: 'cellStyle', type: 'input'},
                        {id: "disabled", type: "checkbox"},
                        {id: "readonly", type: "checkbox"},
                        {id: "editable", type: "checkbox"},
                        {id: "editMode", type: "listbox", editorListItems: ["focus", "sharp", "hover", "inline"]},
                        {
                            id: "editorListItems",
                            cellRenderer: function () {
                                return xui.adjustRes("[ $RAD.designer.conf.Object ]")
                            },
                            type: "button",
                            event: function (profile, cell, editorprf) {
                                var ns = profile.host,
                                    node = profile.getSubNode('CELL', cell._serialId),
                                    obj = cell.obj || [];
                                RAD.Designer.prototype._showMixedEditor(profile._targetProfile, "xui.UI.List", "rows." + cell._col.id, "items", obj, node, function (items) {
                                    cell.obj = xui.clone(items, true);
                                    cell.needCollect = 1;
                                    ns._dirty = 1;
                                    node.focus();
                                }, true);
                            }
                        },
                        {
                            id: "editorDropListWidth",
                            type: "input",
                            editorHAlign: 'right',
                            cellStyle: "text-align:right",
                            editorFormat: CONF.sizeFormat
                        },
                        {
                            id: "editorDropListHeight",
                            type: "input",
                            editorHAlign: 'right',
                            cellStyle: "text-align:right",
                            editorFormat: CONF.sizeFormat
                        },
                        {id: "precision", type: "spin", precision: 0, increment: 1, min: 0, max: 100},
                        {id: "increment", type: "number"},
                        {id: "min", type: "number"},
                        {id: "max", type: "number"},
                        {id: "maxlength", type: "spin", precision: 0, increment: 1, min: 0, max: 10000},
                        {id: "groupingSeparator", type: "input", maxlength: 1},
                        {id: "decimalSeparator", type: "input", maxlength: 1},
                        {id: "forceFillZero", type: "checkbox"},
                        {id: "numberTpl", type: "input"},
                        {id: "currencyTpl", type: "input"},
                        {id: "dateEditorTpl", type: "input"},
                        {id: "iniFold", type: "checkbox"},
                        {id: "tips", type: "input"},
                        {id: "tag", type: "input"},
                        tagVar,
                        tagCmds
                    ],
                    subheader: [
//                        {id:"id",type:'input'},
                        {id: "value", type: 'input'},
                        {id: "caption", type: 'input'},
                        {id: "cellRenderer", type: "input"},
                        {id: "cellCapTpl", type: 'input'},
                        {
                            id: 'type',
                            type: "listbox",
                            editorListItems: ',label,input,textarea,combobox,listbox,file,getter,helpinput,button,dropbutton,cmdbox,popbox,date,time,datetime,color,spin,counter,currency,number,checkbox,progress,cmd'.split(',')
                        },
                        {
                            id: "formula", type: 'input', editorEvents: {
                                _onChange: function (p, ov, v) {
                                    if (false === xui.ExcelFormula.validate(v)) {
                                        xui.alert("It's not a valid formula");
                                    }
                                }
                            }
                        },
                        {id: "tag", type: 'input'},
                        tagVar,
                        {id: 'cellClass', type: 'input'},
                        {id: 'cellStyle', type: 'input'},
                        {id: "disabled", type: "checkbox"},
                        {id: "readonly", type: "checkbox"},
                        {id: "editable", type: "checkbox"},
                        {id: "editMode", type: "listbox", editorListItems: ["focus", "sharp", "hover", "inline"]},
                        {
                            id: "editorListItems",
                            cellRenderer: function () {
                                return xui.adjustRes("[ $RAD.designer.conf.Object ]")
                            },
                            type: "button",
                            event: function (profile, cell, editorprf) {
                                var ns = profile.host,
                                    node = profile.getSubNode('CELL', cell._serialId),
                                    obj = cell.obj || [];
                                RAD.Designer.prototype._showMixedEditor(profile._targetProfile, "xui.UI.List", "rows.cell." + cell._col.id, "items", obj, node, function (items) {
                                    cell.obj = xui.clone(items, true);
                                    cell.needCollect = 1;
                                    ns._dirty = 1;
                                    node.focus();
                                }, true);
                            }
                        },
                        {
                            id: "editorDropListWidth",
                            type: "input",
                            editorHAlign: 'right',
                            cellStyle: "text-align:right",
                            editorFormat: CONF.sizeFormat
                        },
                        {
                            id: "editorDropListHeight",
                            type: "input",
                            editorHAlign: 'right',
                            cellStyle: "text-align:right",
                            editorFormat: CONF.sizeFormat
                        },
                        {id: "precision", type: "spin", precision: 0, increment: 1, min: 0, max: 100},
                        {id: "increment", type: "number"},
                        {id: "min", type: "number"},
                        {id: "max", type: "number"},
                        {id: "maxlength", type: "spin", precision: 0, increment: 1, min: 0, max: 10000},
                        {id: "groupingSeparator", type: "input", maxlength: 1},
                        {id: "decimalSeparator", type: "input", maxlength: 1},
                        {id: "forceFillZero", type: "checkbox"},
                        {id: "numberTpl", type: "input"},
                        {id: "currencyTpl", type: "input"},
                        {id: "dateEditorTpl", type: "input"},
                        {id: "tips", type: "input"},
                        tagCmds
                    ],
                    // dont set it , id will be auto set
                    uniqueKey: null,
                    hasSub: 1,
                    innerKey: 'cells',
                    subrowfixed: 1
                }
            },
            tagCmds: {
                "all": {
                    header: [
                        {id: "id", type: "input"},
                        {id: "tagCmdsAlign", type: "combobox", editorListItems: ["left", "right", "floatright"]},
                        {id: "buttonType", type: "combobox", editorListItems: ["button", "image", "text", "split"]},
                        {id: "pos", type: "combobox", editorListItems: ["", "row", "header"]},
                        {id: "caption", type: "input"},
                        {
                            id: "itemClass",
                            type: "combobox",
                            editorCommandBtn: 'pop',
                            editorDropListWidth: '18em',
                            editorListItems: CONF.designer_list_imageclass2,
                            tagVar: {
                                prop: 'imageClass'
                            }
                        },
                        {id: "itemStyle", type: "input"},
                        {
                            id: "image",
                            type: CONF.getClientMode() == 'singlepage' ? "input" : "popbox",
                            event: function (profile, cell, editorprf) {
                                var node = profile.getSubNode('CELL', cell._serialId),
                                    _cb = function (path) {
                                        editorprf.boxing().setUIValue(CONF.adjustRelativePath(path));
                                        node.focus();
                                    };
                                var t = profile._targetProfile,
                                    k = profile._targetPropKey;
                                if (window.SPA && false === SPA.fe("beforeURISelectorPop", [
                                    k + ':item:image', cell.value, _cb,
                                    'conf', t && t['xui.absProfile'] ? t.boxing() : t, node.get(0), cell, profile.boxing(), editorprf && editorprf.boxing()
                                ])) return;

                                if (CONF.getClientMode() != 'singlepage') {
                                    xui.ModuleFactory.getCom('RAD.ImageSelector', function () {
                                        this.setProperties({
                                            fromRegion: node.cssRegion(true),
                                            onOK: function (obj, path) {
                                                _cb();
                                            }
                                        });
                                        this.show();
                                    });
                                }
                            }
                        },
                        {id: "tips", type: "input"},
                        tagVar
                    ]
                }
            }
        },
        widgets_mapProp: {
            dockMargin: {
                all: {
                    cells: [
                        {id: "left", type: "spin", precision: 0, increment: 1},
                        {id: "top", type: "spin", precision: 0, increment: 1},
                        {id: "right", type: "spin", precision: 0, increment: 1},
                        {id: "bottom", type: "spin", precision: 0, increment: 1}
                    ]
                }
            },
            conDockPadding: {
                all: {
                    cells: [
                        {id: "left", type: "spin", precision: 0, increment: 1},
                        {id: "top", type: "spin", precision: 0, increment: 1},
                        {id: "right", type: "spin", precision: 0, increment: 1},
                        {id: "bottom", type: "spin", precision: 0, increment: 1}
                    ]
                }
            },
            conDockSpacing: {
                all: {
                    cells: [
                        {id: "width", type: "spin", precision: 0, increment: 1},
                        {id: "height", type: "spin", precision: 0, increment: 1}
                    ]
                }
            },
            rowOptions: {
                "xui.UI.TreeGrid": {
                    cells: [
                        {
                            id: 'type',
                            type: "listbox",
                            editorListItems: ',label,input,textarea,combobox,listbox,file,getter,helpinput,button,dropbutton,cmdbox,popbox,date,time,datetime,color,spin,counter,currency,number,checkbox,progress,cmd'.split(',')
                        },
                        {
                            id: "formula", type: 'input', editorEvents: {
                                _onChange: function (p, ov, v) {
                                    if (false === xui.ExcelFormula.validate(v)) {
                                        xui.alert("It's not a valid formula");
                                    }
                                }
                            }
                        },
                        {id: "rowRenderer", type: "input"},
                        {id: "cellRenderer", type: "input"},
                        {id: 'cellClass', type: 'input'},
                        {id: 'cellStyle', type: 'input'},
                        {id: "disabled", type: "checkbox"},
                        {id: "readonly", type: "checkbox"},
                        {id: "editable", type: "checkbox"},
                        {id: "editMode", type: "listbox", editorListItems: ["focus", "sharp", "hover", "inline"]},
                        {id: "precision", type: "spin", precision: 0, increment: 1, min: 0, max: 100},
                        {id: "increment", type: "number"},
                        {id: "min", type: "number"},
                        {id: "max", type: "number"},
                        {id: "maxlength", type: "spin", precision: 0, increment: 1, min: 0, max: 10000},
                        {id: "groupingSeparator", type: "input", maxlength: 1},
                        {id: "decimalSeparator", type: "input", maxlength: 1},
                        {id: "forceFillZero", type: "checkbox"},
                        {id: "numberTpl", type: "input"},
                        {id: "currencyTpl", type: "input"},
                        {id: "dateEditorTpl", type: "input"}
                    ]
                }
            },
            colOptions: {
                "xui.UI.TreeGrid": {
                    cells: [
                        {
                            id: 'type',
                            type: "listbox",
                            editorListItems: ',label,input,textarea,combobox,listbox,file,getter,helpinput,button,dropbutton,cmdbox,popbox,date,time,datetime,color,spin,counter,currency,number,checkbox,progress,cmd'.split(',')
                        },
                        {
                            id: "formula", type: 'input', editorEvents: {
                                _onChange: function (p, ov, v) {
                                    if (false === xui.ExcelFormula.validate(v)) {
                                        xui.alert("It's not a valid formula");
                                    }
                                }
                            }
                        },
                        {id: "colRenderer", type: "input"},
                        {id: "cellRenderer", type: "input"},
                        {id: 'cellClass', type: 'input'},
                        {id: 'cellStyle', type: 'input'},
                        {id: "disabled", type: "checkbox"},
                        {id: "readonly", type: "checkbox"},
                        {id: "editable", type: "checkbox"},
                        {id: "editMode", type: "listbox", editorListItems: ["focus", "sharp", "hover", "inline"]},
                        {id: "precision", type: "spin", precision: 0, increment: 1, min: 0, max: 100},
                        {id: "increment", type: "number"},
                        {id: "min", type: "number"},
                        {id: "max", type: "number"},
                        {id: "maxlength", type: "spin", precision: 0, increment: 1, min: 0, max: 10000},
                        {id: "groupingSeparator", type: "input", maxlength: 1},
                        {id: "decimalSeparator", type: "input", maxlength: 1},
                        {id: "forceFillZero", type: "checkbox"},
                        {id: "numberTpl", type: "input"},
                        {id: "currencyTpl", type: "input"},
                        {id: "dateEditorTpl", type: "input"}
                    ]
                }
            }
        },
        widgets_attrProp: {
            dockMargin: {
                all: {
                    header: ["key", "value"],
                    rows: [
                        [{value: 'left'}],
                        [{value: 'top'}],
                        [{value: 'right'}],
                        [{value: 'bottom'}]
                    ],
                    fix: 1
                }
            }
        },
        widgets_customCellOptions: {
            'xui.UI.Input': {
                maxlength: {
                    precision: 0
                }
            },
            'xui.UI.ComboInput': {
                maxlength: {
                    precision: 0
                },
                min: {
                    precision: 2
                },
                max: {
                    precision: 2
                },
                increment: {
                    precision: 2
                }
            },
            'xui.UI.ColorPicker': {
                value: {
                    type: 'color'
                }
            },
            'xui.UI.Audio': {
                volume: {type: "spin", precision: 2, min: 0, max: 1}
            },
            'xui.UI.Video': {
                volume: {type: "spin", precision: 2, min: 0, max: 1}
            }
        },
        ModuleFactoryProfile: {
            about: {
                cls: 'RAD.About'
            },
            delFile: {
                cls: 'RAD.DelFile'
            },
            prjPro: {
                cls: 'RAD.ProjectPro'
            },
            prjSel: {
                cls: 'RAD.ProjectSelector'
            },
            prjVersionSel: {
                cls: 'RAD.ProjectVersionSelector'
            },
            funEditor: {
                cls: 'RAD.FunEditor'
            },
            jsonEditor: {
                cls: 'RAD.JSONEditor'
            },
            HTMLEditor: {
                cls: 'RAD.HTMLEditor'
            },
            FileSelector: {
                cls: 'RAD.FileSelector'
            },
            MobileInstruction: {
                cls: 'RAD.MobileInstruction'
            },
            RegisterForm: {
                cls: 'RAD.RegisterForm'
            }
        },
        inplaceedit: {
            "xui.UI.Link": {
                KEY: [0, 'getCaption', 'setCaption']
            },
            "xui.UI.Label": {
                KEY: [0, 'getCaption', 'setCaption']
            },
            "xui.UI.CheckBox": {
                CAPTION: [0, 'getCaption', 'setCaption']
            },
            "xui.UI.Span": {
                KEY: [0, 'getHtml', 'setHtml']
            },
            /*"xui.UI.Div":{
                KEY:[0,'getHtml','setHtml']
            },*/
            "xui.UI.HTMLButton": {
                KEY: [0, 'getCaption', 'setCaption']
            },
            "xui.UI.Button": {
                KEY: [0, 'getCaption', 'setCaption']
            },
            "xui.UI.Group": {
                CAPTION: [0, 'getCaption', 'setCaption']
            },
            "xui.UI.Input": {
                INPUT: [0, 'getValue', 'setValue'],
                LABEL: [0, 'getLabelCaption', 'setLabelCaption']
            },
            "xui.UI.ComboInput": {
                INPUT: [0, 'getValue', 'setValue'],
                LABEL: [0, 'getLabelCaption', 'setLabelCaption']
            },
            "xui.UI.RichEditor": {
                LABEL: [0, 'getLabelCaption', 'setLabelCaption']
            },
            "xui.UI.Slider": {
                LABEL: [0, 'getLabelCaption', 'setLabelCaption']
            },
            "xui.UI.PageBar": {
                LABEL: [0, 'getCaption', 'setCaption']
            },
            "xui.UI.Dialog": {
                CAPTION: [0, 'getCaption', 'setCaption']
            },
            "xui.UI.Panel": {
                CAPTION: [0, 'getCaption', 'setCaption']
            },
            "xui.UI.Slider": {
                LABEL: [0, 'getLabelCaption', 'setLabelCaption']
            },

            "xui.UI.List": {
                ITEM: [1, 'getItemByDom', 'updateItem'],
                LABEL: [0, 'getLabelCaption', 'setLabelCaption']
            },
            "xui.UI.RadioBox": {
                CAPTION: [1, 'getItemByDom', 'updateItem'],
                LABEL: [0, 'getLabelCaption', 'setLabelCaption']
            },
            "xui.UI.StatusButtons": {
                CAPTION: [1, 'getItemByDom', 'updateItem'],
                LABEL: [0, 'getLabelCaption', 'setLabelCaption']
            },
            "xui.UI.Gallery": {
                CAPTION: [1, 'getItemByDom', 'updateItem'],
                COMMENT: [1, 'getItemByDom', 'updateItem', 'comment']
            },
            "xui.UI.Tabs": {
                CAPTION: [1, 'getItemByDom', 'updateItem']
            },
            "xui.UI.Stacks": {
                CAPTION: [1, 'getItemByDom', function (prf, item, itemKey, nv) {
                    prf.boxing().updateItem(item.id, {caption: nv});
                    prf.boxing().reLayout(true);
                }]
            },
            "xui.UI.ButtonViews": {
                CAPTION: [1, 'getItemByDom', 'updateItem']
            },
            "xui.UI.MenuBar": {
                ITEM: [1, 'getItemByDom', 'updateItem']
            },
            "xui.UI.TreeBar": {
                ITEMCAPTION: [1, 'getItemByDom', 'updateItem']
            },
            "xui.UI.TreeView": {
                ITEMCAPTION: [1, 'getItemByDom', 'updateItem']
            },
            "xui.UI.ToolBar": {
                LABEL: [1, function (prf, node) {
                    return prf.SubSerialIdMapItem[prf.getSubId(node.id)];
                }, 'updateItem', 'label'],
                BTN: [1, function (prf, node) {
                    return prf.SubSerialIdMapItem[prf.getSubId(node.id)];
                }, 'updateItem']
            },
            "xui.UI.TreeGrid": {
                CELLA: [1, function (prf, node) {
                    var subId = prf.getSubId(node.id), o, item = {};
                    if (subId.charAt(0) == '-' && subId.charAt(1) == 'r') {
                        o = prf.rowMap[subId];
                        item.id = o.id;
                        item.caption = o.caption;
                        item.isRow = true;
                    } else {
                        o = prf.cellMap[subId];
                        item.id = o.id;
                        item.caption = o.value;
                    }
                    return item;
                }, function (prf, item, itemKey, nv) {
                    if (item.isRow)
                        prf.boxing().updateRow(item.id, {caption: nv});
                    else
                        prf.boxing().updateCell(item.id, {value: nv});
                }],
                HCELLA: [1, function (prf, node) {
                    var subId = prf.getSubId(node.id);
                    return subId ? prf.colMap[subId] : {
                        id: ' X ',
                        caption: prf.boxing().getGridHandlerCaption()
                    };
                }, function (prf, item, itemKey, nv) {
                    if (item.id != ' X ')
                        prf.boxing().updateHeader(item.id, {caption: nv});
                    else
                        prf.boxing().setGridHandlerCaption(nv);
                }]
            },
            "xui.UI.FormLayout": {
                ITEM: [1, null, null, null, null, null, function (page, prf, node) {
                    if (prf.$htable) {
                        var cellMeta = prf.getItemByDom(node.id());
                        prf.$htable.selectCell(cellMeta.row, cellMeta.col);
                    }
                }]
            },
            "xui.UI.FoldingTabs": {
                CAPTION: [1, 'getItemByDom', 'updateItem'],
                MESSAGE: [1, 'getItemByDom', 'updateItem', 'message']
            },
            "xui.svg.text": {
                KEY: [0, function (prf) {
                    return prf.properties.attr.text;
                }, function (prf, nv) {
                    prf.boxing().setAttr({text: nv}, null, false);
                }, 'KEY', true]
            },
            "xui.svg.rectComb": {
                TEXT: [0, function (prf) {
                    return xui.get(prf.properties.attr, ["TEXT", "text"]);
                }, function (prf, nv) {
                    prf.boxing().setAttr("TEXT", {text: nv}, false);
                }, 'KEY', true]
            },
            "xui.svg.circleComb": {
                TEXT: [0, function (prf) {
                    return xui.get(prf.properties.attr, ["TEXT", "text"]);
                }, function (prf, nv) {
                    prf.boxing().setAttr("TEXT", {text: nv}, false);
                }, 'KEY', true]
            },
            "xui.svg.ellipseComb": {
                TEXT: [0, function (prf) {
                    return xui.get(prf.properties.attr, ["TEXT", "text"]);
                }, function (prf, nv) {
                    prf.boxing().setAttr("TEXT", {text: nv}, false);
                }, 'KEY', true]
            },
            "xui.svg.pathComb": {
                TEXT: [0, function (prf) {
                    return xui.get(prf.properties.attr, ["TEXT", "text"]);
                }, function (prf, nv) {
                    prf.boxing().setAttr("TEXT", {text: nv}, false);
                }, 'KEY', true]
            },
            "xui.svg.imageComb": {
                TEXT: [0, function (prf) {
                    return xui.get(prf.properties.attr, ["TEXT", "text"]);
                }, function (prf, nv) {
                    prf.boxing().setAttr("TEXT", {text: nv}, false);
                }, 'KEY', true]
            },
            "xui.svg.connector": {
                TEXT: [0, function (prf) {
                    return xui.get(prf.properties.attr, ["TEXT", "text"]);
                }, function (prf, nv) {
                    prf.boxing().setAttr("TEXT", {text: nv}, false);
                }, 'KEY', true]
            }
        }
    });
    xui.setAppLangKey("RAD");

// special setting for xui.UI.FormLayout
    CONF._$initForDesign = {
        'xui.UI.FormLayout': function (page, prf) {
            var capMap = {
                'row_above': '$(RAD.FormLayout.Insert row above)',
                'row_below': '$(RAD.FormLayout.Insert row below)',
                'col_left': '$(RAD.FormLayout.Insert column left)',
                'col_right': '$(RAD.FormLayout.Insert column right)',
                'clear_column': '$(RAD.FormLayout.Clear selected column)',
                'remove_row': '$(RAD.FormLayout.Remove row)',
                'remove_col': '$(RAD.FormLayout.Remove column)',
                'alignment': '$(RAD.FormLayout.Alignment)',
                'alignment:left': '$(RAD.FormLayout.Left)',
                'alignment:top': '$(RAD.FormLayout.Top)',
                'alignment:right': '$(RAD.FormLayout.Right)',
                'alignment:bottom': '$(RAD.FormLayout.Bottom)',
                'alignment:middle': '$(RAD.FormLayout.Middle)',
                'alignment:center': '$(RAD.FormLayout.Center)',
                'alignment:justify': '$(RAD.FormLayout.Justify)',
                'mergeCells': '$(RAD.FormLayout.Merge cells)',
                'color': '$(RAD.FormLayout.Foreground color)',
                'bgcolor': '$(RAD.FormLayout.Background color)',
                'border': '$(RAD.FormLayout.Border)',
                'font': '$(RAD.FormLayout.Font)',
                'font:font-family': '$(RAD.FormLayout.Font family)',
                'font:font-size': '$(RAD.FormLayout.Font size)',
                'font:font-weight': '$(RAD.FormLayout.Font weight)',
                'font:font-style': '$(RAD.FormLayout.Font style)',
                'font:text-decoration': '$(RAD.FormLayout.Text decoration)'
            };
            prf.$onLayoutChanged = function (profile, odata, ndata) {
                var redo;
                var alias = profile.alias;

                odata = xui.clone(odata);
                ndata = xui.clone(ndata);

                page.markDirty(function () {
                    page.getByAlias(alias).setLayoutData(odata, true);
                }, redo = function () {
                    page.getByAlias(alias).setLayoutData(ndata, true);
                }, 'set properties');
                //prf.properties.layoutData = xui.clone(ndata);
            };
            prf.$onClick=function(prf, xuimenu, _menu){

            };
            prf.$onPopMenu = function (prf, xuimenu, _menu) {
                var ignoreMap = {cut: 1, copy: 1, make_read_only: 1, undo: 1, redo: 1};
                var htable = _menu.hot;
                var mapArr = function (arr, key) {
                    var mapArr = [];
                    xui.arr.each(arr, function (item) {
                        var t = {}
                        if (xui.isHash(item)) {
                            t = xui.copy(item);
                        } else if (item) {
                            t = {};
                            t.id = t.value = item;
                        } else {
                            return;
                        }
                        t.cat = key;
                        mapArr.push(t);
                    });
                    return mapArr;
                };
                if (xuimenu) {
                    xui.each(_menu.menuItems, function (item) {
                        if (item.name != '---------') {
                            xuimenu.updateItem(item.key, {disabled: item.disabled ? item.disabled.call(htable) : false});
                        }
                    });
                } else {
                    xuimenu = CONF._FormLayout_PopMenu;
                    if (!xuimenu || xuimenu.isDestroyed()) {
                        xuimenu = new xui.UI.PopMenu();
                        var mapMenu = function (items) {
                                var nitems = [], lastSplit;
                                xui.each(items, function (item) {
                                    if (ignoreMap[item.key]) return;
                                    if (item.name == '---------') {
                                        if (!lastSplit) {
                                            lastSplit = 1;
                                            nitems.push({type: 'split'});
                                        }
                                    } else {
                                        lastSplit = 0;
                                        nitems.push({
                                            id: item.key,
                                            disabled: item.disabled ? item.disabled.call(htable) : false,
                                            caption: capMap && capMap[item.key] || item.name.call(htable),
                                            sub: item.submenu ? mapMenu(item.submenu.items) : null,
                                            _cb: item.callback
                                        });
                                    }
                                });
                                return nitems;
                            },
                            normalizeSelection = function (selRanges) {
                                var arr = [];
                                for (var i = 0, l = selRanges.length; i < l; i++) {
                                    arr.push({
                                        start: selRanges[i].getTopLeftCorner(),
                                        end: selRanges[i].getBottomRightCorner()
                                    });
                                }
                                return arr;
                            },
                            nitems = mapMenu(_menu.menuItems);
                        nitems.push({type: 'split'});
                        nitems.push({
                            id: 'color',
                            caption: capMap && capMap.color || 'color',
                            sub: true
                        });
                        nitems.push({
                            id: 'bgcolor',
                            caption: capMap && capMap.bgcolor || 'bgcolor',
                            sub: true
                        });
                        nitems.push({
                            id: 'font',
                            caption: capMap && capMap.font || 'font',
                            sub: [
                                {
                                    id: 'font:font-family',
                                    caption: capMap && capMap['font:font-family'] || 'font-family',
                                    sub: mapArr(CONF.designer_data_fontfamily, 'family')
                                },
                                {
                                    id: 'font:font-size',
                                    caption: capMap && capMap['font:font-size'] || 'font-size',
                                    sub: mapArr(CONF.designer_data_fontsize, 'size')
                                },
                                {
                                    id: 'font:font-weight',
                                    caption: capMap && capMap['font:font-weight'] || 'font-weight',
                                    sub: mapArr(CONF.designer_data_fontweight, 'weight')
                                },
                                {
                                    id: 'font:font-style',
                                    caption: capMap && capMap['font:font-style'] || 'font-style',
                                    sub: mapArr(CONF.designer_data_fontstyle, 'style')
                                },
                                {
                                    id: 'font:text-decoration',
                                    caption: capMap && capMap['font:text-decoration'] || 'text-decoration',
                                    sub: mapArr(CONF.designer_data_textdecoration, 'decoration')
                                }
                            ]
                        });
                        nitems.push({
                            id: 'border',
                            caption: capMap && capMap.border || 'border',
                            sub: true
                        })
                        xuimenu.setItems(nitems);

                        xuimenu.onMenuSelected(function (p, item) {
                            var setStyle = function (key, value) {
                                xui.each(htable.getSelectedRange(), function (selection) {
                                    var fromRow = Math.min(selection.from.row, selection.to.row),
                                        toRow = Math.max(selection.from.row, selection.to.row),
                                        fromCol = Math.min(selection.from.col, selection.to.col),
                                        toCol = Math.max(selection.from.col, selection.to.col);

                                    for (var row = fromRow; row <= toRow; row++) {
                                        for (var col = fromCol; col <= toCol; col++) {
                                            var cellMeta = htable.getCellMeta(row, col),
                                                TD = htable.getCell(row, col);

                                            TD.style[key] = (cellMeta.style || (cellMeta.style = {}))[key] = value;


                                        }
                                    }
                                });
                                prf.box._layoutChanged(prf);
                            };
                            switch (item.id) {
                                case 'bgcolor':
                                    setStyle('backgroundColor', "#" + item.value);
                                    break;
                                case 'color':
                                    setStyle('color', "#" + item.value);
                                    break;
                                case 'alignment:left':
                                    setStyle('textAlign', "left");
                                    break;
                                case 'alignment:center':
                                    setStyle('textAlign', "center");
                                    break;
                                case 'alignment:right':
                                    setStyle('textAlign', "right");
                                    break;
                                case 'alignment:justify':
                                    setStyle('textAlign', "justify");
                                    break;
                                case 'alignment:top':
                                    setStyle('verticalAlign', "top");
                                    break;
                                case 'alignment:middle':
                                    setStyle('verticalAlign', "middle");
                                    break;
                                case 'alignment:bottom':
                                    setStyle('verticalAlign', "bottom");
                                    break;
                                default:
                                    if (item._cb) {
                                        item._cb.call(htable, item.id, normalizeSelection(htable.getSelectedRange()));
                                    } else {
                                        switch (item.cat) {
                                            case "family":
                                                setStyle('fontFamily', item.id);
                                                break;
                                            case "weight":
                                                setStyle('fontWeight', item.id);
                                                break;
                                            case "size":
                                                setStyle('fontSize', item.id);
                                                break;
                                            case "decoration":
                                                setStyle('textDecoration', item.id);
                                                break;
                                            case "style":
                                                setStyle('fontStyle', item.id);
                                                break;
                                        }
                                    }
                            }
                        })
                            .onShowSubMenu(function (profile, item, src) {
                                var menubar = profile.boxing(), obj;
                                switch (item.id) {
                                    case 'color':
                                    case 'bgcolor':
                                        obj = (new xui.UI.ColorPicker).render(true);
                                        obj.setCloseBtn(false);
                                        obj.afterUIValueSet(function (p, old, n) {
                                            menubar.hide(false);
                                            menubar.onMenuSelected(profile, {
                                                id: item.id,
                                                value: n
                                            });
                                        });
                                        obj.get(0).$parentPopMenu = profile;
                                        break;
                                    case 'border':
                                        var module = (new RAD.GridBorder()).render(true);
                                        module.fillBtns();
                                        module.setEvents("onApplyBorder", function (type, style, width, color) {
                                            var value = (style || 'solid') + ' ' + (width ? (width + 'px') : '') + ' ' + (color || '');
                                            value = xui.str.trim(value).replace(/ +/g, ' ');

                                            var customBordersPlugin = htable.getPlugin('customBorders');
                                            var customBordersT = [], customBordersL = [];
                                            xui.each(htable.getSelectedRange(), function (selection) {
                                                var fromRow = Math.min(selection.from.row, selection.to.row),
                                                    toRow = Math.max(selection.from.row, selection.to.row),
                                                    fromCol = Math.min(selection.from.col, selection.to.col),
                                                    toCol = Math.max(selection.from.col, selection.to.col);

                                                var l = 'borderLeft', r = 'borderRight', t = 'borderTop',
                                                    b = 'borderBottom';
                                                // clear custom
                                                customBordersPlugin.clearBorders([[fromRow, fromCol, toRow + 1, toCol + 1]]);
                                                // clear td border
                                                for (var row = fromRow; row <= toRow; row++) {
                                                    for (var col = fromCol; col <= toCol; col++) {
                                                        var cellMeta = htable.getCellMeta(row, col),
                                                            TD = htable.getCell(row, col),
                                                            prevCellMeta, prevTD;
                                                        cellMeta.style = cellMeta.style || {};
                                                        delete cellMeta.style[l];
                                                        delete cellMeta.style[r];
                                                        delete cellMeta.style[t];
                                                        delete cellMeta.style[b];
                                                        xui(TD).css('border', '');
                                                        if (TD.previousElementSibling && TD.previousElementSibling.tagName.toUpperCase() == 'TD') {
                                                            // find the prev one
                                                            prevCellMeta = htable.getCellMeta(row, col - 1);
                                                            prevTD = htable.getCell(row, col - 1);
                                                            if (!prevCellMeta.hidden) {
                                                                delete prevCellMeta.style[r];
                                                                xui(prevTD).css('borderRight', '');
                                                            }
                                                        }
                                                        if (TD.parentElement.previousElementSibling && TD.parentElement.previousElementSibling.tagName.toUpperCase() == 'TR') {
                                                            // find the prev one
                                                            prevCellMeta = htable.getCellMeta(row - 1, col);
                                                            prevTD = htable.getCell(row - 1, col);
                                                            if (!prevCellMeta.hidden) {
                                                                delete prevCellMeta.style[b];
                                                                xui(prevTD).css('borderBottom', '');
                                                            }
                                                        }
                                                    }
                                                }
                                                // set one by one
                                                var isLeft, isRight, isTop, isBottom, isInner, isOuter;
                                                for (var row = fromRow; row <= toRow; row++) {
                                                    for (var col = fromCol; col <= toCol; col++) {
                                                        var cellMeta = htable.getCellMeta(row, col),
                                                            TD = htable.getCell(row, col),
                                                            prevCellMeta, prevTD;
                                                        isLeft = col == fromCol;
                                                        isRight = col == toCol;
                                                        isTop = row == fromRow;
                                                        isBottom = row == toRow;
                                                        isOuter = isLeft || isRight || isTop || isBottom;
                                                        isInner = !isOuter;

                                                        if (cellMeta.spanned) {
                                                            isOuter = isLeft = isRight = isTop = isBottom = true;
                                                            isInner = !isOuter;
                                                        }

                                                        if (type == 'all' || type == 'inner') {
                                                            if (isInner) {
                                                                TD.style[r] = cellMeta.style[r] = value;
                                                                TD.style[b] = cellMeta.style[b] = value;
                                                            } else {
                                                                if (isLeft || isRight) {
                                                                    if (isLeft) {
                                                                        TD.style[r] = cellMeta.style[r] = value;
                                                                    }
                                                                    if (!isBottom) {
                                                                        TD.style[b] = cellMeta.style[b] = value;
                                                                    }
                                                                }
                                                                if (isTop || isBottom) {
                                                                    if (isTop) {
                                                                        TD.style[b] = cellMeta.style[b] = value;
                                                                    }
                                                                    if (!isRight) {
                                                                        TD.style[r] = cellMeta.style[r] = value;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (isRight && (type == 'right' || type == 'outer' || type == 'all')) {
                                                            TD.style[r] = cellMeta.style[r] = value;
                                                        }
                                                        if (isLeft && (type == 'left' || type == 'outer' || type == 'all')) {
                                                            // first
                                                            if (!TD.previousElementSibling || TD.previousElementSibling.tagName.toUpperCase() != 'TD') {
                                                                TD.style[l] = cellMeta.style[l] = value;
                                                            } else {
                                                                // find the prev one
                                                                prevCellMeta = htable.getCellMeta(row, col - 1);
                                                                prevTD = htable.getCell(row, col - 1);
                                                                if (!prevCellMeta.hidden) {
                                                                    prevTD.style[r] = prevCellMeta.style[r] = value;
                                                                } else {
                                                                    //var first = prf.getItemByDom(prevTD);
                                                                    //if(!xui.get(first,['style','borderR'])){
                                                                    //    TD.style[l] = cellMeta.style[l] =  value;
                                                                    //}
                                                                    // draw customBorders
                                                                    customBordersL.push([row, col, row, col]);
                                                                }
                                                            }
                                                        }
                                                        if (isBottom && (type == 'bottom' || type == 'outer' || type == 'all')) {
                                                            TD.style[b] = cellMeta.style[b] = value;
                                                        }
                                                        if (isTop && (type == 'top' || type == 'outer' || type == 'all')) {
                                                            // first
                                                            if (!TD.parentElement.previousElementSibling || TD.parentElement.previousElementSibling.tagName.toUpperCase() != 'TR') {
                                                                TD.style[t] = cellMeta.style[t] = value;
                                                            } else {
                                                                // find the prev one
                                                                prevCellMeta = htable.getCellMeta(row - 1, col);
                                                                prevTD = htable.getCell(row - 1, col);
                                                                if (!prevCellMeta.hidden) {
                                                                    prevTD.style[b] = prevCellMeta.style[b] = value;
                                                                } else {
                                                                    //var first = prf.getItemByDom(prevTD);
                                                                    //if(!xui.get(first,['style','borderB'])){
                                                                    //   TD.style[t] = cellMeta.style[t] =  value;
                                                                    //}
                                                                    // draw customBorders
                                                                    customBordersT.push([row, col, row, col]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            });
                                            // Use customBorders plugin for patch merged cell's small sibling's left/top border
                                            if (customBordersT.length) customBordersPlugin.setBorders(customBordersT, {
                                                top: {
                                                    color: color,
                                                    width: width
                                                }
                                            });
                                            if (customBordersL.length) customBordersPlugin.setBorders(customBordersL, {
                                                left: {
                                                    color: color,
                                                    width: width
                                                }
                                            });

                                            prf.box._layoutChanged(prf);
                                            profile.boxing().hide();
                                        });
                                        obj = module.getRoot(true).boxing();
                                        module.getAllComponents().each(function (prf) {
                                            prf.$parentPopMenu = profile;
                                        });
                                        break;
                                }
                                return obj;
                            });

                        CONF._FormLayout_PopMenu = xuimenu;
                    }
                }
                xuimenu.pop(prf.$lastMousePos);
                return xuimenu;
            };
            prf.$handleCustomVars = function (d) {
                if (d) {
                    for (var i in d) if (d[i]) this[i] = d[i];
                } else {
                    return {
                        $onLayoutChanged: prf.$onLayoutChanged,
                        $onPopMenu: prf.$onPopMenu
                    }
                }
            }
        }
    };

    CONF.openProject = function (projectName, onOK1, onOK2, callback, host, openMain) {
        var openRoot = function (projectName, onOK1, onOK2, callback, host, openMain) {
                var action = 'open';
                if (!onOK2) {
                    action = 'refresh'
                }
                xui.Ajax(CONF.openProjectService,
                    {
                        hashCode: xui.id(),
                        projectName: projectName

                    }, function (txt) {
                        var obj = txt;
                        if (!obj || obj.error) {
                            switch (obj.error.id) {
                                case 3005:
                                    xui.alert(xui.adjustRes('$(RAD.msg.$0 dones not exit-' + projectName + ')'));
                                    break;
                                default:
                                    xui.message(xui.get(obj, ['error', 'message']) || obj || 'no response!');
                            }
                        } else {
                            openCls(projectName, onOK1, onOK2, callback, host, openMain, obj.data.files || obj.data);
                        }
                    }).start();
            },
            openCls = function (projectName, onOK1, onOK2, callback, host, openMain, ok1data) {
                var finalFun = function (projectName, projectRootClass, onOK1, onOK2, callback, host, ok1data) {
                    if (onOK1) {
                        xui.tryF(onOK1, [projectName, ok1data, openMain, projectRootClass], host);
                    }
                    if (onOK2) {
                        getXUIClasses(projectName, projectRootClass, onOK2, openMain, callback, host);
                    } else {
                        xui.tryF(callback);
                    }
                };
                finalFun(projectName, "App", onOK1, onOK2, callback, host, ok1data);
            },
            getXUIClasses = function (projectName, rootCls, onOK2, openMain, callback, host) {
                if (!rootCls) rootCls = "App";

                xui.Ajax(CONF.getAllClass, {projectName: projectName}, function (txt) {
                    var obj = txt;
                    if (!obj || obj.error)
                        xui.message(xui.get(obj, ['error', 'errdes']) || 'no response!');
                    else {
                        xui.tryF(onOK2, [projectName, obj.data.files || obj.data, openMain, rootCls], host);
                    }
                    xui.tryF(callback);
                }).start();
            };

        if (onOK1) openRoot(projectName, onOK1, onOK2, callback, host, openMain);
        else if (onOK2) openCls(projectName, null, onOK2, callback, host, openMain);
    };
    CONF.showToolBox = function (show) {
        if (show) {
            if (CONF._toolbox) {
                CONF._toolbox.show();
            } else {
                CONF._toolbox = xui.showCom("RAD.ToolBox");
            }
        } else {
            if (CONF._toolbo) CONF._toolbox.hide();
        }
    };
    CONF.callService = function (query, onSuccess, onFail, threadid, options, observable) {
        var request;
        request = function () {
            var args = xui.toArr(arguments);

            args.unshift(CONF.remoteService);
            return xui.request.apply(xui, args);
        };

        if (!options) options = {};
        var token = xui.Cookies.get('access_token') || '';
        if (token) {
            options.exData = {access_token: token};
        }
        options.beforeSuccess = function (response, rspType) {
            if (response && response.error && (response.error.code + "") == "-100") {
                window.onbeforeunload = null;
                window.location = CONF.loginPage;
            }
        };

        if (!observable)
            request(query, onSuccess, onFail, threadid, options);
        else
            xui.observableRun(function (threadid) {
                request(query, onSuccess, onFail, threadid, options);
            }, null, threadid);
    };
    CONF.mapServerPath = function (path) {
        return path;
    };


    CONF.getLocalFile = function (path, onSuccess, onFail, sync) {

        xui.Ajax(path, '', onSuccess, onFail, null, {rspType: "text", asy: !sync}).start();
    };
    CONF.downloadFile = function (params, onSuccess, onFail, threadid, options, observable) {
        request = function () {
            var args = xui.toArr(arguments);
            args.unshift(CONF.remoteService);
            args.curProjectPath = SPA.curProjectPath;
            return xui.XDMI.apply(xui.IAjax, args).start();
        };

        if (!observable)
            request(query, onSuccess, onFail, threadid, options);
        else
            xui.observableRun(function (threadid) {
                request(query, onSuccess, onFail, threadid, options);
            }, null, threadid);

    };

    CONF.debugApp = function (appPath, debugFile, data, newinstance, method, uselocalfile) {
        data = data || {};
        data.dir = appPath;
        data.debug = 1;

        var url = appPath + debugFile;

        if (method == 'post')
            xui.Dom.submit(url, data, method);
        else {
            var hash = '';
            if (data) hash = '#' + xui.urlEncode(data);
            xui.Dom.submit(url + hash);
        }
    };
    CONF.openAPI = function (para) {
        xui.Dom.submit(CONF.getAPIPath() + '#!' + (para || ""));

    };
    CONF.openJSONEditor = function () {
        CONF.openURL("http://www.itjds.net/JSONEditor");
    };
    CONF.previewPage2 = function (SPA, path) {
        var code = SPA.getJSONCode(SPA.getAllWidgets()), size = SPA.getViewSize();
        code = code.replace(/^xui\.create\(/, '');
        code = code.replace(/\)\.get\(\);$/, '');
        window.__classCode = code;
        if (window.__preview) {
            window.__preview.opener = null;
            window.__preview.close();
            window.__preview = void(0);
        }
        window.__preview = window.open((path || "preview.html") + "#" + xui.urlEncode({
            rand: xui.rand(),
            theme: SPA.$curTheme || 'default',
            lang: xui.getLang()
        }), "preview", "height=" + size.height + ",width=" + size.width + ",toolbar=no,menubar=no,scrollbars=yes, resizable=yes,location=no,status=no,center=yes");
    }
    CONF.openURL = function (url) {
        xui.Dom.submit(url);
    };
    CONF.openFile = function (url) {
        xui.Dom.submit(url);
    };


    CONF.saveAS = function (path, hash, method) {
        var ifrid = 'ifr_for_download';
        if (!xui.Dom.byId(ifrid))
            xui('body').append(xui.create('<iframe id="' + ifrid + '" name="' + ifrid + '" style="display:none;"/>'));
        xui.Dom.submit(path, hash, method, ifrid);

    };


    CONF.saveFiles = function (collections) {
        var hash = {};
        xui.each(collections, function (o) {
            xui.Ajax(CONF.saveFile, o.paras, o.onSuccess, o.onFail,
                null, {
                    rspType: 'json',
                    method: 'POST'
                }).start();
        });
    };

    // for local file in browser
    CONF.adjustProtocol = function (path) {
        return path;
    };
    CONF.adjustFileAbsPath = function (path) {
        return "/" + SPA.curProjectName + "/" + path;
    }

    CONF.getPrjPath = function (path, encode) {
        var exepath = xui.ini.appPath;

        if (encode) {
            path = encodeURI(path).replace(/\(/g, escape("(")).replace(/\)/g, escape(")"))
            exepath = encodeURI(exepath).replace(/\(/g, escape("(")).replace(/\)/g, escape(")"));
        }
        if (xui.str.startWith(path, exepath)) {
            return path;
        } else {
            return exepath + path;
        }
    };
    CONF.isZipURL = function (url) {
        return (new RegExp(
            '^((https|http|ftp|rtsp|mms)?://)'
            + '?(([0-9a-z_!~*\'().&=+$%-]+: )?[0-9a-z_!~*\'().&=+$%-]+@)?' //ftp user@
            + '(([0-9]{1,3}.){3}[0-9]{1,3}' // IP URL- 199.194.52.184
            + '|' //  IP or DOMAIN
            + '([0-9a-z_!~*\'()-]+.)*' // - www.
            + '([0-9a-z][0-9a-z-]{0,61})?[0-9a-z].' // sub domain
            + '[a-z]{2,6})' // first level domain- .com or .museum
            + '(:[0-9]{1,4})?' // port- :80
            + '((/?)|' // a slash isn't required if there is no file name
            + '(/[0-9a-z_\\[\\]!~*\'().;?:@&=+$,%#-]+)+/?)$'
        )).test(url.toLowerCase()) && /\.zip$/i.test(url);
    };
    CONF.isURL = function (url) {
        return /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(url.toLowerCase());
    };

    CONF.saveStatus = function (obj, filename) {
        filename = CONF.mapServerPath(filename || "status.json");
        xui.Cookies.set(filename, xui.serialize(obj), 100);
    };
    CONF.getStatus = function (filename) {
        filename = CONF.mapServerPath(filename || "status.json");
        var str = xui.Cookies.get(filename);
        return xui.unserialize(str) || {};
    };
    CONF.setWidgetDftTheme = function (cls, theme, all) {
        var clsObj = xui.SC(cls);
        if (clsObj) {
            CONF.WidgetDftTheme = CONF.WidgetDftTheme || {};
            CONF.WidgetDftTheme[cls] = theme;
            var nodes = [];
            clsObj.getAll().each(function (prf) {
                if (prf.$inDesign && (all || !prf.theme)) {
                    nodes.push(prf);
                }
            });
            if (nodes.length)
                clsObj.pack(nodes).setTheme(theme);
        }
    };
    (function () {
        // codemirror mode
        var codemirror_map = {
            'APL': "as3 asf",
            'Asterisk': "agi",
            'C': "c",
            'C++': "cpp cp cxx c__",
            'Cobol': "cbl cob cobol cpy",
            'Java': "j jav java mash",
            'C#': "c# cs csx",
            'Scala': "scala sca scb",
            'Clojure': "clj",
            'CoffeeScript': "coffee",
            'Common Lisp': "cl gc3 gcl kcl",
            'Cypher': "cyp",
            'CSS': "css styl",
            'D': "d",
            'diff': "diff dif",
            'DTD': "dtd",
            'Dylan': "Dylan",
            'ECL': "ecl",
            'Eiffel': "e",
            'Erlang': "erl",
            'Fortran': "f f77 f90 f95 for fpp ftn hpf mod lng",
            'F#': "mli fsscript fsx",
            'Gas': "gas",
            'Gherkin': "feature",
            'GitHub Flavored Markdown': "gfm",
            'Go': "go",
            'Groovy': "Groovy",
            'HAML': "haml",
            'Haskell': "hs has lhs gs lit",
            'Haxe': "hx",
            'ASP.NET': "aspx asp+ asax armx",
            'Embedded Javascript': "ejs",
            'JavaServer Pages': "jsp",
            'HTML': "hhs hlm ht3 htm html html5 htmls uhtml adiumhtmllog nclk hhc xhtm",
            'HTTP': "cgx",
            'Jade': "Jade",
            'JavaScript': 'js jsfl jsxinc hbs vv module cls',
            'JSON': "json jsonp",
            'TypeScript': "ts",
            'Julia': "julia",
            'Julia2': "julia2",
            'LESS': "less",
            'LiveScript': "ls",
            'Lua': "lua luca",
            'Markdown (GitHub-flavour)': "md markdown",
            'mIRC': "mrc",
            "Mathematica": "m nb",
            "Modelica": "mo",
            "MUMPS": "mps",
            "mbox": "mbox",
            'Nginx': "nginx",
            'NTriples': "nt",
            'OCaml': "ml",
            'Octave': "m",
            'Pascal': "dfm dpk dpr p pp tpx vpi",
            'PEG.js': "pegjs",
            'Perl': "cgi Perl ph ipb m2r pdl pl pls plx pm prl",
            'PHP': "ctp phl mod php php1 php2 php3 php4 php5 phps phpt phs inc",
            'PHP(HTML)': "phtml",
            'Pig': "pig",
            'Plain Text': "$00 $01 $02 $04 $05 $o1 $ol 001 12da 1st 7 82t 92t abl ac adiumlog adl adt adw aiml alx aml android annot ans ansi application aprj aqt ass awd awh awp axt ba1 bad bas bbs bbxt bcr bdp bdr bea bel bep big big5 bk blm bln blw bmtp bna bnx bog box box bpdx brf bsdl bss bt bzw cag cas cascii cc cd2 charset cho chord cif cil ckn clg cli clix cmd cmtx cof conf coo crash crd cs csassembly csmanifest csv ctd ctf ctl ctl ctx dat dcd dce ddd ddt de dectest des desc dfe dii diskdefines diz dk dkz dmr dne dok dp dpv dqy drp dsc dsml dtd dwl ecsv edml edt efm eia emulecollection en enc enf eng err es esw etf etx euc ext extra faq fff ffp fin first flr fmr fnx fon fr fra frm fsa full gbf gdt gen ger gnu gpl gs gthr gtx guide hdr hlx hp8 hs hsk htx hvc hwl hz id31 id32 idc idt idx iem igv igy iif ill inc inc inc ini inuse ion ion ipr iqy isr it ivp ja jad jam jeb jis jp1 jss jtx kahl kar kch kix klg kor la label las lay lin linux linx lisp lnc log lo_ lrc lst ltr ltt ltx lue luf lwd lyr lyt man manifest map mar markdown mathml maxFR mcw md mdl mdle mdown mdtext mdtxt mdwn me mez mf mib mit mkd mkdn mno mnu modd mpsub mss mtx mtx mtxt mvg mw nbr ncm new nfo nlc nmbd nokogiri not notes now npdt nt nwctxt ocr odc oh ojp omn oogl oot opc openbsd opml ort osi p3x panic pbd pc5 pcl pd pdu pfs pgw pjs pla pla plf plg plk pln pml pmo pod prc prn pro prr ps psb psi2 pt3 pts pvj pvw q&a qdt qud rbdf rdf rea readme reg rel rep resp rest rff ris rml rqy rst rt rtf rtl rtx ru rus rzk rzn s19 s2k sami sbv sct sdnf sen seq set sfb sgp sha1 sha512 skcard skv sls smali smf smi sms snw soap soundscript spa spec spg spn spx srt srt ssa ssf st1 stf stq strings sub syn t t2t tab tbd tbl tbx tce tcm tdf ted text text text textclipping tfw tgf thml thp tlb tle tlx tlx tm tml tmprtf tmx tnef tph tpl trn trt tsv tt ttbl tte ttf ttpl ttxt tx8 tx? txa txa txd txe txh txt txt txt u3i uax uk unauth uni unx us usa user usf usg utf8 utx utxt ver vet vfk vhd vhdl vis vkp vmg vna vsmproj vw vw3 wer wir wkf wn wrd wrl wsc wsf wsh wst wtf wtl wtx x20 x60 x70 x80 x90 xb0 xc0 xct xd0 xff xht xlf xsr xwp xwp xy: xy3 xyp xyw xyz zanebug zed zhp zib zw zxe _me",
            'Properties files': "properties",
            'Python': "py pyw rpy pyx",
            'Puppet': "Puppet",
            'R': "r",
            'reStructuredText': "rst rest",
            'Ruby': "irbrc rb rbw rbx",
            'Rust': "rust",
            'Sass': "Sass",
            'Scheme': "fasl lap scm smd ss",
            'SCSS': "scss",
            'Shell': "bash bsh csh sh ksh ucsh psc1 psc2 psm1",
            'Sieve': "sieve",
            'Smalltalk': "st",
            'Solr': "solr",
            'SPARQL': "srx",
            'SQL': 'db2 eql sql',
            'sTeX': "stex",
            'SystemVerilog': "verilog",
            'Tcl': "glf tcl itcl rvt",
            'TiddlyWiki': "tiddlywiki",
            'Tiki wiki': "tiki",
            'TOML': "toml",
            'Turtle': "turtle",
            'VB.NET': "vb vbx aspx",
            'VBScript': "vba dvb vbi vbs vbscript",
            'Verilog': "v",
            XML: "apx arxml bxml ccxml cxe cxs dxl mlsxml vxml lxfml plist vxml xdl xdp xfd xml xsd xsl xslt yml wxs ptxml pxml txml",
            'XQuery': "xq xquery",
            'YAML': "yaml",
            'Z80': "Z80"
        };
        var codemirror_hash;
        CONF.getRenderMode = function (key) {
            if (!codemirror_hash) {
                codemirror_hash = {};
                xui.each(codemirror_map, function (o, i) {
                    var index = xui.arr.subIndexOf(CodeMirror.modeInfo, "name", i);
                    if (index != -1) {
                        var mode = CodeMirror.modeInfo[index];
                        xui.arr.each(o.split(' '), function (k) {
                            codemirror_hash[k.toLowerCase()] = mode;
                        });
                    }
                });
            }
            return CodeMirror.findModeByExtension(key) || codemirror_hash[key];
        };
        CONF.getImageTag = function (ext, xui) {
            var imagePos, mime;
            if (ext.indexOf(".") != -1) {
                var filearr = ext.split(".");
                ext = filearr[filearr.length - 1];
            }
            ext = ext.toLowerCase();

            if (xui) {
                imagePos = "spafont spa-icon-page";
            } else {
                switch (ext) {
                    case 'html':
                    case 'htm':
                        imagePos = 'spafont spa-icon-html';
                        break;
                    case 'css':
                        imagePos = 'spafont spa-icon-css';
                        break;
                    case 'js':
                        imagePos = 'spafont spa-icon-js';
                        break;
                    case 'cls':
                        imagePos = 'spafont spa-icon-page';
                        break;
                    case 'json':
                        imagePos = 'spafont spa-icon-formatjson';
                        break;
                    case 'xml':
                        imagePos = 'spafont spa-icon-xml';
                        break;
                    case 'swf':
                    case 'fla':
                    case 'flv':
                        imagePos = 'spafont spa-icon-swf';
                        break;
                        break;
                    case 'jpg':
                    case 'png':
                    case 'gif':
                    case 'bmp':
                        imagePos = 'spafont spa-icon-pic';
                        break;
                    default:
                        if (mime) {
                            if (mime == "audio") {
                                imagePos = 'spafont spa-icon-audio';
                            } else if (mime == "video") {
                                imagePos = 'spafont spa-icon-video';
                            } else if (mime == "image") {
                                imagePos = 'spafont spa-icon-pic';
                            } else if (mime == "application") {
                                imagePos = 'spafont spa-icon-app';
                            }
                        }
                        if (!imagePos) {
                            var mode = CONF.getRenderMode(ext);
                            if (mode) {
                                imagePos = mode.mode == "null" ? 'spafont spa-icon-empty' : "spafont spa-icon-file";
                            } else
                                imagePos = 'spafont spa-icon-unkown';
                        }
                }
            }
            return imagePos;
        };
    }());

    (function () {
        var m = CONF.mixed_prop = {},
            n = CONF.widgets_itemsProp,
            t = n.items,
            k, h;
        for (var i in t) {
            h = {};
            k = t[i].header;
            for (var j = 0, l = k.length; j < l; j++)
                h[k[j].id || k[j]] = "";
            m[i.replace(/\./g, "_") + "_item"] = h;
        }
        k = n.header['xui.UI.TreeGrid'].header;
        h = {};
        for (var j = 0, l = k.length; j < l; j++)
            h[k[j].id] = "";
        m["xui_UI_TreeGrid_col"] = h;

        k = n.rows['xui.UI.TreeGrid'].header;
        h = {};
        for (var j = 0, l = k.length; j < l; j++)
            h[k[j].id] = "";
        m["xui_UI_TreeGrid_row"] = h;

        k = n.rows['xui.UI.TreeGrid'].subheader;
        h = {};
        for (var j = 0, l = k.length; j < l; j++)
            h[k[j].id] = "";
        m["xui_UI_TreeGrid_cell"] = h;
    }());


    // load snapshot
    (function () {

    }());


}
;