
xui.Class('RAD.expression.ExpressionEditor', 'xui.Module',{
    Instance:{
        initialize : function(){ },
        Dependencies:[],
        Required:[],
        properties : {
            "path":"form/myspace/versionspace/projectManager/0/RAD/expression/ExpressionEditor.cls",
            "personId":"devdev",
            "personName":"devdev",
            "projectName":"projectManager"
        },
        events:{
            "onRender":{
                "actions":[
                    {
                        "args":[
                            "{xui.showModule2()}",
                            null,
                            null,
                            "RAD.PageEditor",
                            "editor"
                        ],
                        "desc":"动作 1",
                        "method":"showModule2",
                        "redirection":"other:callback:call",
                        "target":"url",
                        "type":"other"
                    }
                ]
            }
        },
        functions:{},
        iniComponents : function(){
            // [[Code created by JDSEasy RAD Studio
            var host=this, children=[], properties={}, append=function(child){children.push(child.get(0));};
            xui.merge(properties, this.properties);

            append(
                xui.create("xui.UI.Dialog")
                    .setHost(host,"xui_ui_dialog14")
                    .setLeft("0.8333333333333334em")
                    .setTop("3.3333333333333335em")
                    .setWidth("75em")
                    .setHeight("60em")
                    .setCaption("公式编辑器")
                    .setImageClass("spafont spa-icon-function")
            );

            host.xui_ui_dialog14.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block65")
                    .setDock("fill")
                    .setLeft("17.5em")
                    .setTop("9.166666666666666em")
            );

            host.xui_ui_block65.append(
                xui.create("xui.UI.ButtonViews")
                    .setHost(host,"xui_ui_buttonviews9")
                    .setItems([
                        {
                            "caption":"手工编写",
                            "hidden":false,
                            "id":"custom",
                            "imageClass":"spafont spa-icon-action1"
                        },
                        {
                            "caption":"常用模板库",
                            "hidden":false,
                            "id":"temp",
                            "imageClass":"spafont spa-icon-designview"
                        },
                        {
                            "caption":"常用函数查询",
                            "hidden":false,
                            "id":"fun",
                            "imageClass":"spafont spa-icon-function"
                        }
                    ])
                    .setLeft("0em")
                    .setTop("0em")
                    .setBarLocation("left")
                    .setBarSize("11.666666666666666em")
                    .setValue("custom")
            );

            host.xui_ui_buttonviews9.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block67")
                    .setDock("fill")
                    .setLeft("13.333333333333334em")
                    .setTop("10em"),
                "custom"
            );

            host.xui_ui_block67.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block69")
                    .setDock("fill")
                    .setLeft("7.5em")
                    .setTop("28.333333333333332em")
            );

            host.xui_ui_block69.append(
                xui.create("xui.UI.Tabs")
                    .setHost(host,"centerdown")
                    .setName("centerdown")
                    .setItems([
                        {
                            "caption":"数据",
                            "hidden":false,
                            "id":"data",
                            "imageClass":"spafont spa-icon-c-dateinput"
                        },
                        {
                            "caption":"JSON",
                            "hidden":false,
                            "id":"json",
                            "imageClass":"spafont spa-icon-c-treeview"
                        },
                        {
                            "caption":"VIEW",
                            "hidden":false,
                            "id":"veiw",
                            "imageClass":"spafont spa-icon-designview"
                        },
                        {
                            "caption":"日志",
                            "hidden":false,
                            "id":"log",
                            "imageClass":"spafont spa-icon-coin"
                        }
                    ])
                    .setLeft("0em")
                    .setTop("0em")
                    .setValue("data")
                    .onItemSelected([
                        {
                            "args":[
                                "{xui.showModule2()}",
                                null,
                                null,
                                "RAD.PageEditor",
                                "centerdown",
                                "{args[1].id}"
                            ],
                            "conditions":[
                                {
                                    "symbol":"=",
                                    "right":"json",
                                    "left":"{args[1].id}"
                                }
                            ],
                            "desc":"动作 1",
                            "method":"showModule2",
                            "redirection":"other:callback:call",
                            "target":"url",
                            "type":"other"
                        }
                    ])
            );

            host.xui_ui_block67.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block68")
                    .setDock("top")
                    .setLeft("1.6666666666666667em")
                    .setTop("20em")
                    .setHeight("26.083333333333332em")
            );

            host.xui_ui_block68.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"editor")
                    .setName("editor")
                    .setDock("fill")
                    .setLeft("1.6666666666666667em")
                    .setTop("11.666666666666666em")
            );

            host.xui_ui_block68.append(
                xui.create("xui.UI.MenuBar")
                    .setHost(host,"xui_ui_menubar2")
                    .setItems([
                        {
                            "caption":"文件",
                            "hidden":false,
                            "id":"file"
                        },
                        {
                            "caption":"函数",
                            "hidden":false,
                            "id":"fun",
                            "sub":[
                                {
                                    "caption":"取整",
                                    "hidden":false,
                                    "id":"abs"
                                }
                            ]
                        },
                        {
                            "caption":"集合",
                            "hidden":false,
                            "id":"arr"
                        },
                        {
                            "caption":"调试",
                            "hidden":false,
                            "id":"debug"
                        }
                    ])
            );

            host.xui_ui_dialog14.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block64")
                    .setName("buttongroup")
                    .setDock("bottom")
                    .setLeft("0em")
                    .setTop("29.166666666666668em")
                    .setHeight("3.5em")
            );

            host.xui_ui_block64.append(
                xui.create("xui.UI.Button")
                    .setHost(host,"savebutton")
                    .setName("savebutton")
                    .setLeft("28.666666666666668em")
                    .setTop("0.6666666666666666em")
                    .setCaption("$RAD.widgets.esd.buttonsave")
                    .setImageClass("xui-icon-right")
            );

            host.xui_ui_block64.append(
                xui.create("xui.UI.Button")
                    .setHost(host,"closebutton")
                    .setName("closebutton")
                    .setLeft("39.5em")
                    .setTop("0.6666666666666666em")
                    .setCaption("$RAD.widgets.esd.buttonclose")
                    .setImageClass("fa fa-lg fa-close")
            );

            return children;
            // ]]Code created by JDSEasy RAD Studio
        },

        customAppend : function(parent, subId, left, top){
            return false;
        }  } ,
    Static:{
        "designViewConf":{
            "height":1024,
            "mobileFrame":false,
            "width":1280
        }
    }



});