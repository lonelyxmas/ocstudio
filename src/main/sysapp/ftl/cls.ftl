<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta name="viewport"
          content="user-scalable=no, initial-scale=0.5, minimum-scale=0.5, maximum-scale=2.0,width=device-width, height=device-height"/>
    <meta http-equiv="no-cache">
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="content-type" content="no-cache, must-revalidate"/>
    <meta http-equiv="expires" content="-1"/>
    <link rel="stylesheet" type="text/css" href="/RAD/css/default.css"/>
    <link rel="stylesheet" type="text/css" href="/xui/iconfont/iconfont.css"/>
    <link rel="stylesheet" type="text/css" href="/plugins/formlayout/handsontable.full.min.css"/>
    <link rel="stylesheet" type="text/css" href="/xui/bpm/bpmfont.css"/>

    <link type="text/css" href="/plugins/fontawesome/font-awesome.min.css" rel="stylesheet">
    <link type="text/css" href="/xui/appearance/default/theme.css" rel="stylesheet">
    <#list $CssFills as item>
        <link rel="stylesheet" type="text/css" href="/root/${item.path}"/>
    </#list>
    <script type="text/javascript" src="/RAD/xui.js"></script>

    <script type="text/javascript" src="./xuiconf.js"></script>
    <script type="text/javascript" src="/xui/Locale/cn.js"></script>
    <#list $CssFills as item>
        <link rel="stylesheet" type="text/css" href="/root/${item.path}"/>
    </#list>

    <#list cssList as item>
        <link rel="stylesheet" type="text/css" href="${item.path}"/>
    </#list>

    <title>${projectName}调试界面</title>
</head>


<body>
<div id="loading" style="position:fixed;width:100%;text-align:center;">
    <img id="loadingimg" alt="Loading..." title="Loading..." src="/RAD/img/loading.gif"/>
</div>
</body>
</html>

<script type="text/javascript">
    if (/#.*touch\=(1|true)/.test(location.href)) {
        window.xui_ini = {fakeTouch: 1};
        document.body.className += " xui-cursor-touch";
    }
</script>
<script type="text/javascript">
    var args = xui.getUrlParams(),
            onEnd = function () {
                xui('loading').remove();
                this.initData();
                this._fireEvent('afterShow');

            };
    xui.launch('${className}', onEnd, 'cn', args && args.theme || 'default');


    $E = xui.execExpression;
    $BPD = {
        open: function () {
            $E('$BPD.open()')
        },


        close: function () {
            $E('$BPD.close()')
        }
    }

    $ESD = {
        open: function (url, params) {
            var paramArr = params | {};
            if (url) {
                paramArr.url = url;
            }
            $E('$ESD.open()', paramArr)
        },
        screen: function () {
            var paramArr = {projectName: "${projectName}"};
            paramArr.url = window.location.href;
            $E('$ESD.screen()', paramArr)
        },
        reload: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            paramArr.handleId = window.handleId;
            if (url) {
                paramArr.url = url;

            }
            $E('$ESD.reload()', paramArr)
        },

        quit: function () {
            $E('$BPD.quit()')
        },
        logout: function () {
            $E('$BPD.logout()')
        }
    }

</script>