<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta name="viewport"
          content="user-scalable=no, initial-scale=0.5, minimum-scale=0.5, maximum-scale=2.0,width=device-width, height=device-height"/>
    <meta http-equiv="no-cache">
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="content-type" content="no-cache, must-revalidate"/>
    <meta http-equiv="expires" content="-1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" href="/xui/fontawesome/font-awesome.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="/RAD/css/default.css"/>
    <link rel="stylesheet" type="text/css" href="/RAD/iconfont/iconfont.css"/>
    <link rel="stylesheet" type="text/css" href="/RAD/css/control-themes.css"/>
    <link rel="stylesheet" type="text/css" href="/plugins/formlayout/handsontable.full.min.css"/>
    <link rel="stylesheet" type="text/css" href="/xui/bpm/bpmfont.css"/>
    <link type="text/css" href="/xui/fontawesome/font-awesome.min.css" rel="stylesheet">
    <link type="text/css" href="/xui/appearance/webflat/theme.css" rel="stylesheet">
    <#list $CssFills as item>
        <link rel="stylesheet" type="text/css" href="/root/${item.path}"/>
    </#list>

    <script type="text/javascript" src="/xui/xui-all.js"></script>
    <script type="text/javascript" src="xuiconf.js"></script>
    <script type="text/javascript" src="/xui/Locale/cn.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/lib/codemirror.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/mode/meta.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/keymap/sublime.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/mode/loadmode.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/lint/jshint.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/lint/jsonlint.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/lint/csslint.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/lint/htmlhint.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/lint/lint.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/lint/javascript-lint.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/lint/json-lint.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/lint/css-lint.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/lint/html-lint.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/fold/foldcode.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/fold/foldgutter.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/fold/brace-fold.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/fold/xml-fold.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/fold/comment-fold.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/selection/active-line.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/search/searchcursor.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/search/match-highlighter.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/edit/matchbrackets.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/edit/closebrackets.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/comment/comment.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/addon/comment/continuecomment.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/mode/javascript/javascript.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/mode/xml/xml.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/mode/css/css.js"></script>
    <script type="text/javascript" src="/plugins/codemirror5/mode/htmlmixed/htmlmixed.js"></script>
    <script type="text/javascript" src="/RAD/RAD/conf.js"></script>
    <script type="text/javascript" src="/RAD/RAD/CodeEditor.js"></script>
    <script type="text/javascript" src="/RAD/RAD/ClassTool.js"></script>
    <script type="text/javascript" src="/RAD/RAD/JSEditor.js"></script>
    <script type="text/javascript" src="/RAD/RAD/expression/CodeEditor.js"></script>
    <script type="text/javascript" src="/RAD/RAD/expression/JavaEditor.js"></script>



    <title>${projectName}调试界面</title>
</head>


<body>
<div id="loading" style="position:fixed;width:100%;text-align:center;">
    <img id="loadingimg" alt="Loading..." title="Loading..." src="/RAD/img/loading.gif"/>
</div>
</body>


<script type="text/javascript">
    if (/#.*touch\=(1|true)/.test(location.href)) {
        window.xui_ini = {fakeTouch: 1};
        document.body.className += " xui-cursor-touch";
    }
</script>

<script type="text/javascript">

    var args = xui.getUrlParams(),
            onEnd = function () {
                this.setData(args);
                xui('loading').remove();
                this.initData();
                this._fireEvent('afterShow');
            };
    xui.launch('${className}', onEnd, 'cn', args && args.theme || 'default');


    $E = xui.execExpression;
    $BPD = {
        open: function () {
            $E('$BPD.open()')
        },
        close: function () {
            $E('$BPD.close()')
        }
    }
    $ESD = {
        export: function (url, params) {
            var paramArr = params | {};
            if (url) {
                paramArr.url = url;
            }
            $E('$ESD.export()', paramArr)
        },
        reload: function (packageName, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (packageName) {
                paramArr.packageName = packageName;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.reload()', paramArr)
        },
        open: function (url, params) {
            var paramArr = params | {};
            if (url) {
                paramArr.url = url;
            }
            $E('$ESD.open()', paramArr)
        },
        quit: function () {
            $E('$BPD.quit()')
        },
        logout: function () {
            $E('$BPD.logout()')
        }
    }
</script>
</html>