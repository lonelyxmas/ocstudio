<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/RAD/css/default.css"/>
    <link rel="stylesheet" type="text/css" href="/RAD/iconfont/iconfont.css"/>
    <link type="text/css" href="/xui/fontawesome/font-awesome.min.css" rel="stylesheet">

    <title>欢迎使用 OneCode 工具1.0</title>
</head>
<body spellcheck="false">

<div id='loading'>
    <img src="/RAD/img/loading.gif" alt="Loading..."/>
</div>
</body>
<script type="text/javascript" src="/RAD/xui.js"></script>
<script type="text/javascript" src="/xui/Module.js"></script>
<script type="text/javascript" src="/xui/XML.js"></script>
<script type="text/javascript" src="/xui/XMLRPC.js"></script>
<script type="text/javascript" src="/xui/SOAP.js"></script>
<script type="text/javascript" src="/xui/ModuleFactory.js"></script>
<script type="text/javascript" src="/xui/Debugger.js"></script>
<script type="text/javascript" src="/xui/Date.js"></script>
<script type="text/javascript" src="/xui/UI.js"></script>
<script type="text/javascript" src="/xui/UI/Image.js"></script>
<script type="text/javascript" src="/xui/UI/Flash.js"></script>
<script type="text/javascript" src="/xui/UI/Audio.js"></script>
<script type="text/javascript" src="/xui/UI/FileUpload.js"></script>
<script type="text/javascript" src="/xui/UI/Video.js"></script>
<script type="text/javascript" src="/xui/UI/Resizer.js"></script>
<script type="text/javascript" src="/xui/UI/Block.js"></script>
<script type="text/javascript" src="/xui/UI/Label.js"></script>
<script type="text/javascript" src="/xui/UI/ProgressBar.js"></script>
<script type="text/javascript" src="/xui/UI/Slider.js"></script>
<script type="text/javascript" src="/xui/UI/Input.js"></script>
<script type="text/javascript" src="/xui/UI/CheckBox.js"></script>
<script type="text/javascript" src="/xui/UI/HiddenInput.js"></script>
<script type="text/javascript" src="/xui/UI/RichEditor.js"></script>
<script type="text/javascript" src="/xui/UI/ComboInput.js"></script>
<script type="text/javascript" src="/xui/UI/ColorPicker.js"></script>
<script type="text/javascript" src="/xui/UI/DatePicker.js"></script>
<script type="text/javascript" src="/xui/UI/TimePicker.js"></script>
<script type="text/javascript" src="/xui/UI/List.js"></script>
<script type="text/javascript" src="/xui/UI/Gallery.js"></script>
<script type="text/javascript" src="/xui/UI/Panel.js"></script>
<script type="text/javascript" src="/xui/UI/Group.js"></script>
<script type="text/javascript" src="/xui/UI/PageBar.js"></script>
<script type="text/javascript" src="/xui/UI/Tabs.js"></script>
<script type="text/javascript" src="/xui/UI/Stacks.js"></script>
<script type="text/javascript" src="/xui/UI/ButtonViews.js"></script>
<script type="text/javascript" src="/xui/UI/RadioBox.js"></script>
<script type="text/javascript" src="/xui/UI/StatusButtons.js"></script>
<script type="text/javascript" src="/xui/UI/TreeBar.js"></script>
<script type="text/javascript" src="/xui/UI/TreeView.js"></script>
<script type="text/javascript" src="/xui/UI/PopMenu.js"></script>
<script type="text/javascript" src="/xui/UI/MenuBar.js"></script>
<script type="text/javascript" src="/xui/UI/ToolBar.js"></script>
<script type="text/javascript" src="/xui/UI/Layout.js"></script>
<script type="text/javascript" src="/xui/UI/TreeGrid.js"></script>
<script type="text/javascript" src="/xui/UI/Dialog.js"></script>
<script type="text/javascript" src="/xui/UI/FormLayout.js"></script>
<script type="text/javascript" src="/xui/UI/FoldingTabs.js"></script>
<script type="text/javascript" src="/xui/UI/FoldingList.js"></script>
<script type="text/javascript" src="/xui/UI/Opinion.js"></script>
<script type="text/javascript" src="/xui/ThirdParty/raphael.js"></script>
<script type="text/javascript" src="/xui/svg.js"></script>
<script type="text/javascript" src="/xui/UI/SVGPaper.js"></script>
<script type="text/javascript" src="/xui/UI/FusionChartsXT.js"></script>
<script type="text/javascript" src="/xui/UI/ECharts.js"></script>
<script type="text/javascript" src="/xui/Coder.js"></script>
<script type="text/javascript" src="/xui/Module/JSONEditor.js"></script>


<script type="text/javascript">
    var lang = 'cn';

    xui.include("xui.Locale." + lang + '.doc', "/RAD/Locale/" + lang + ".js", function () {
        xui.Module.load('RAD', function () {
            SPA = this;
        }, '');
    });
    xui.setLang('cn');


</script>

<script type="text/javascript" src="/RAD/index.js"></script>
<script type="text/javascript">
    CONF.clientType = 'ESDClient';
    currProjectName = "${projectName}";
    $E = xui.execExpression;
    $BPD = {
        open: function () {
            $E('$BPD.open()')
        },
        close: function () {
            $E('$BPD.close()')
        },
        newprocess: function () {
            var paramArr = {projectName: "${projectName}"};
            paramArr.handleId = window.handleId;
            $E('$ESD.newprocess()', paramArr);
        },
    }


    $ESD = {
        export: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (url) {
                paramArr.url = url;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.export()', paramArr);
            xui.busy();
        },


        publicRemote: function () {
            var paramArr = {
                projectName: "${projectName}",
                className: SPA.currentClassName
            };
            paramArr.url = window.location.href;
            $E('$ESD.publicRemote()', paramArr)
        },
        publicLocal: function () {
            var paramArr = {
                projectName: "${projectName}",
                className: SPA.currentClassName
            };
            paramArr.url = window.location.href;
            $E('$ESD.publicLocal()', paramArr)
        },


        exportRemoteServer: function () {
            var paramArr = {projectName: "${projectName}"};
            paramArr.url = window.location.href;
            $E('$ESD.exportRemoteServer()', paramArr)
        },

        screen: function () {
            var paramArr = {projectName: "${projectName}"};
            paramArr.url = window.location.href;
            $E('$ESD.screen()', paramArr)
        },

        buildCustomModule: function (url, params) {
            var paramArr = {projectName: "${projectName}"};
            paramArr.handleId = window.handleId;
            $E('$ESD.rebuildCustomModule()', paramArr)
        },

        download: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (url) {
                paramArr.url = url;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.download()', paramArr);
            xui.busy();
        },


        startDebugServer: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (url) {
                paramArr.url = url;
                paramArr.handleId = window.handleId;
            }

            $E('$ESD.startDebugServer()', paramArr)
        },
        stopDebugServer: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (url) {
                paramArr.url = url;
                paramArr.handleId = window.handleId;
            }

            $E('$ESD.stopDebugServer()', paramArr)
        },

        exportLocalServer: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (url) {
                paramArr.url = url;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.exportLocalServer()', paramArr)
        },


        pull: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (url) {
                paramArr.url = url;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.pull()', paramArr)
        },

        clearAll: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (url) {
                paramArr.url = url;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.clearAll()', paramArr)
        },


        push: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (url) {
                paramArr.url = url;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.push()', paramArr)
        },


        clearAll: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (url) {
                paramArr.url = url;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.clearAll()', paramArr)
        },

        open: function (url, params) {
            var paramArr = params | {};
            if (url) {
                paramArr.url = url;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.open()', paramArr)
        },

        reload: function (packageName, params) {
            var paramArr = params || {projectName: "${projectName}"};
            paramArr.handleId = window.handleId;
            if (packageName) {
                paramArr.packageName = packageName;
            }
            $E('$ESD.reload()', paramArr)
        },

        customDebug: function (params) {
            var paramArr = params || {projectName: "${projectName}"};
            paramArr.handleId = window.handleId;
            $E('$ESD.customDebug()', paramArr)
        },

        clear: function (spaceName, params) {
            var paramArr = params || {projectName: "${projectName}"};
            paramArr.handleId = window.handleId;
            if (spaceName) {
                paramArr.spaceName = spaceName;
            }
            $E('$ESD.clear()', paramArr)
        },

        createDBModule: function (url, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (url) {
                paramArr.url = url;

            }
            paramArr.handleId = window.handleId;
            $E('$ESD.createDBModule()', paramArr)
        },

        openProject: function (newProjectName, params) {
            var paramArr = params || {};
            if (newProjectName) {
                paramArr.newProjectName = newProjectName;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.openProject()', paramArr)
        },
        quit: function () {
            $E('$ESD.quit()')
        },
        logout: function () {
            $E('$ESD.logout()')
        },
        clear: function () {
            $E('$ESD.clear()', {projectName: SPA.curProjectName})
        }
    }
</script>

</html>
