var MGallery = {
{
    tempConfig:{

        items: {
            ITEM: {
                tabindex: '{_tabindex}',
                    className: 'xui-uitembg xui-uiborder-radius xui-showfocus {itemClass} {disabled} {readonly}',
                    style: 'padding:{itemPadding};margin:{itemMargin};{_itemSize};{itemStyle})',

                    ITEMFRAME: {
                    style: '{_inneritemSize};{_bgimg}; {_position}}',
                        COMMENT: {
                        tagName: 'div',
                            className: 'xui-ui-ellipsis',
                            text: '{comment}',
                            style: '{commentDisplay};{_color}',
                            $order: 2
                    },
                    CONTENT: {
                        tagName: 'div',
                            $order: 1,
                            className: '{contentClass}',
                            style: '{_loadbg}',
                            ICON: {
                            className: 'xuifont {_imageClass}',
                                style: "{_fontSize};{_color};{_icon};{_position};}",
                                text: '{iconFontCode}'
                        },
                        IMAGE: {
                            tagName: 'img',
                                src: xui.ini.img_bg,
                                title: '{image}',
                                style: '{_innerimgSize};{imgStyle}'
                        }
                    },
                    CAPTION: {
                        tagName: 'div',
                            className: 'xui-ui-ellipsis',
                            style: '{capDisplay}',
                            text: '{caption}',
                            $order: 0
                    }

                },
                FLAG: {
                    $order: 20,
                        className: 'xui-display-none {flagClass}',
                        style: '{_flagStyle};{flagStyle}',
                        text: '{flagText}'
                },
                EXTRA: {
                    text: '{ext}',
                        $order: 30
                }
            }
        }

    }
}
