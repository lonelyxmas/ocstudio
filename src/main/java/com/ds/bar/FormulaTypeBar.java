package com.ds.bar;

import com.ds.bar.service.AttributetypeService;
import com.ds.bar.service.EngineService;
import com.ds.bar.service.FormulaTypeService;
import com.ds.bar.service.ParticipantSelectService;
import com.ds.bpm.formula.ParticipantSelect;
import com.ds.enums.attribute.Attributetype;
import com.ds.esb.config.formula.EngineType;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TreeAnnotation
public class FormulaTypeBar extends TreeListItem {


    @Pid
    FormulaType formulaType;
    @Pid
    String participantSelectId;
    @Pid
    Attributetype baseType;


    @TreeItemAnnotation(caption = "公式类型")
    public FormulaTypeBar() {
        this.caption = "公式类型";
        this.setId("allFormulaType");
    }

    @TreeItemAnnotation(bindService = AttributetypeService.class, autoHidden = false)
    public FormulaTypeBar(Attributetype baseType) {
        this.caption = baseType.getName();
        this.id = baseType.getType();
        this.baseType = baseType;
        this.imageClass = baseType.getImageClass();

    }

    @TreeItemAnnotation(closeBtn = true, bindService = FormulaTypeService.class, autoHidden = false)
    public FormulaTypeBar(FormulaType formulaType) {
        this.caption = formulaType.getName();
        this.formulaType = formulaType;
        this.setImageClass(formulaType.getImageClass());
        this.setId(formulaType.getType());

    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-function", bindService = ParticipantSelectService.class, autoHidden = false)
    public FormulaTypeBar(ParticipantSelect select) {
        this.caption = select.getSelectName();
        this.id = select.getParticipantSelectId();
        this.tips = select.getFormula();
        this.formulaType = select.getFormulaType();
        this.participantSelectId = select.getParticipantSelectId();
    }

    @TreeItemAnnotation(bindService = EngineService.class, autoHidden = false)
    public FormulaTypeBar(EngineType engineType) {
        this.caption = engineType.getName();
        this.setImageClass(engineType.getImageClass());
        this.setId(engineType.getType());
        this.setIniFold(false);

    }

    public Attributetype getBaseType() {
        return baseType;
    }

    public void setBaseType(Attributetype baseType) {
        this.baseType = baseType;
    }

    public FormulaType getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }

    public String getParticipantSelectId() {
        return participantSelectId;
    }

    public void setParticipantSelectId(String participantSelectId) {
        this.participantSelectId = participantSelectId;
    }
}
