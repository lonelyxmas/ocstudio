package com.ds.bar.tempbar;


import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.dsm.manager.temp.view.ViewTempGrid;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.annotation.ViewType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/action/spatoolbar/temp/view/")
@MethodChinaName(cname = "视图管理", imageClass = "spafont spa-icon-conf")
public class ViewTempService {


    @RequestMapping(method = RequestMethod.POST, value = "TempGrid")
    @GridViewAnnotation
    @DialogAnnotation
    @ModuleAnnotation(caption = "TempGrid")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<ViewTempGrid>> getViewTempGrid(ViewType viewType) {
        ListResultModel<List<ViewTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> beans = new ArrayList<>();
            if (viewType != null) {
                beans = DSMFactory.getInstance().getTempManager().getViewTemps(viewType);
            } else {
                beans = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(DSMType.view);
            }
            resultModel = PageUtil.getDefaultPageList(beans, ViewTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
