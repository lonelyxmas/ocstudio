package com.ds.bar.tempbar;

import com.ds.bar.JavaTempBar;
import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempGrid;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.enums.Enumstype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.ViewGroupType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = {"/action/spatoolbar/temp/admin/"})
@MethodChinaName(cname = "模板管理", imageClass = "spafont spa-icon-c-gallery")
public class AdminTempService {


    @RequestMapping(method = RequestMethod.POST, value = "AllTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AllTemp")
    @DialogAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<JavaTempGrid>> getAllTempGrid(DSMType dsmType) {
        ListResultModel<List<JavaTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
            resultModel = PageUtil.getDefaultPageList(temps, JavaTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "loadChildDSM")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public TreeListResultModel<List<JavaTempBar>> loadChildDSM(DSMType dsmType) {
        TreeListResultModel<List<JavaTempBar>> result = new TreeListResultModel<List<JavaTempBar>>();
        List<Enumstype> dsmtypes = new ArrayList<>();
        switch (dsmType) {
            case view:
                ViewGroupType[] viewTypes = ViewGroupType.values();
                dsmtypes.addAll(Arrays.asList(viewTypes));
                break;
            case repository:
                RepositoryType[] repositoryTypes = RepositoryType.values();
                dsmtypes.addAll(Arrays.asList(repositoryTypes));
                break;
            case aggregation:
                AggregationType[] aggregationTypes = AggregationType.values();
                dsmtypes.addAll(Arrays.asList(aggregationTypes));
                break;
            case customDomain:
                CustomDomainType[] domainTypes = CustomDomainType.values();
                dsmtypes.addAll(Arrays.asList(domainTypes));
                break;

        }
        result = TreePageUtil.getTreeList(dsmtypes, JavaTempBar.class);
        return result;
    }

}
