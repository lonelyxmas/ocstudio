package com.ds.bar.tempbar;


import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.admin.temp.JavaTempGallery;
import com.ds.dsm.admin.temp.repository.RepositoryAdminTempGrid;
import com.ds.dsm.admin.temp.repository.RepositoryTempForm;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/action/spatoolbar/temp/repository/")
@MethodChinaName(cname = "资源库模板", imageClass = "spafont spa-icon-conf")
public class RepositoryAdminService {


    @RequestMapping(method = RequestMethod.POST, value = "TempGrid")
    @GridViewAnnotation
    @ModuleAnnotation( caption = "TempGrid")
    @DialogAnnotation
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<RepositoryAdminTempGrid>> getTempGrid(RepositoryType repositoryType) {
        ListResultModel<List<RepositoryAdminTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> beans = new ArrayList<>();
            if (repositoryType != null) {
                beans = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(repositoryType);
            } else {
                beans = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(DSMType.repository);
            }
            resultModel = PageUtil.getDefaultPageList(beans, RepositoryAdminTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
