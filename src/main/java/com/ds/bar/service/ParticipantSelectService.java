package com.ds.bar.service;

import com.ds.bpm.formula.FormulaService;
import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.manager.formula.view.ParticipantSelectFormView;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.right.component.view.ModuleFormulaInstNav;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin/formula/nav/")
@MethodChinaName(cname = "编辑表达式信息", imageClass = "bpmfont bpmgongzuoliuzuhuzicaidan")
public class ParticipantSelectService {



//    @RequestMapping(method = RequestMethod.POST, value = "FormulaInstInfo")
//    @NavGroupViewAnnotation()
//    @DialogAnnotation
//    @ModuleAnnotation(caption = "添加参数", imageClass = "spafont spa-icon-function", dynLoad = true)
//    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
//    public @ResponseBody
//    ResultModel<PageFormulaInstNav> getFormulaInstInfo(String formulaInstId, String projectName) {
//        ResultModel<PageFormulaInstNav> model = new ResultModel<PageFormulaInstNav>();
//        return model;
//    }
//

    @RequestMapping(method = RequestMethod.POST, value = "ParticipantInfo")
    @FormViewAnnotation()
    @ModuleAnnotation(caption = "编辑表达式信息")
    @DialogAnnotation
    @APIEventAnnotation(bindMenu = {CustomMenuItem.treeNodeEditor},autoRun = true)
    public @ResponseBody
    ResultModel<ParticipantSelectFormView> getParticipantSelect(String participantSelectId) {
        ResultModel<ParticipantSelectFormView> model = new ResultModel<ParticipantSelectFormView>();
        ParticipantSelect select = null;
        try {
            select = getService().getParticipantSelect(participantSelectId).get();
            select = new ParticipantSelectFormView(select);
            model.setData((ParticipantSelectFormView) select);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return model;
    }

    @RequestMapping(method = RequestMethod.POST, value = "FormulaInstInfo")
    @NavGroupViewAnnotation
    @DialogAnnotation
    @ModuleAnnotation(caption = "添加参数", imageClass = "spafont spa-icon-function")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
    public @ResponseBody
    ResultModel<ModuleFormulaInstNav> getFormulaInstInfo(String formulaInstId, String projectName) {
        ResultModel<ModuleFormulaInstNav> model = new ResultModel<ModuleFormulaInstNav>();
        return model;
    }

    public FormulaService getService() {
        FormulaService service = (FormulaService) EsbUtil.parExpression("$FormulaService");
        return service;
    }
}



