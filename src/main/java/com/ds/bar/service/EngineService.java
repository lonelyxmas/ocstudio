package com.ds.bar.service;

import com.ds.bar.FormulaTypeBar;
import com.ds.config.TreeListResultModel;
import com.ds.enums.attribute.Attributetype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.EngineType;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin/formula/nav/")
@MethodChinaName(cname = "引擎列表", imageClass = "bpmfont bpmgongzuoliuzuhuzicaidan")
public class EngineService {




    @RequestMapping("FormulaList")
    @ResponseBody
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public TreeListResultModel<List<FormulaTypeBar>> getChild(EngineType engineType) {
        TreeListResultModel<List<FormulaTypeBar>> listResultModel = new TreeListResultModel();
        List<Attributetype> attributetypeList = new ArrayList<>();
        Attributetype[] attributetypes = Attributetype.values();
        for (Attributetype attributetype : attributetypes) {
            if (attributetype.getEngineType().equals(engineType)) {
                attributetypeList.add(attributetype);
            }
        }
        listResultModel = TreePageUtil.getDefaultTreeList(attributetypeList, FormulaTypeBar.class);
        return listResultModel;
    }

}



