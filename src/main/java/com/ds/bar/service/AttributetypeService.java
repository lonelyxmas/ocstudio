package com.ds.bar.service;

import com.ds.bar.FormulaTypeBar;
import com.ds.bpm.formula.FormulaService;
import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.attribute.Attributetype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.manager.formula.view.ParticipantSelectGridView;
import com.ds.esd.util.TreePageUtil;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin/formula/nav/")
@MethodChinaName(cname = "引擎列表", imageClass = "bpmfont bpmgongzuoliuzuhuzicaidan")
public class AttributetypeService {

    @RequestMapping(method = RequestMethod.POST, value = "AttributeSelectList")
    @MethodChinaName(cname = "获取表达式信息")
    @GridViewAnnotation()
    @ModuleAnnotation()
    @DialogAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor, autoRun = true)
    public @ResponseBody
    ListResultModel<List<ParticipantSelectGridView>> getAttributeSelectList(Attributetype baseType) {
        List<FormulaType> formulaTypeList = new ArrayList<>();
        FormulaType[] formulaTypes = FormulaType.values();
        for (FormulaType formulaType : formulaTypes) {
            if (formulaType.getBaseType().equals(baseType)) {
                formulaTypeList.add(formulaType);
            }
        }
        List<ParticipantSelect> selectGridViewList = new ArrayList<>();
        for (FormulaType formulaType : formulaTypeList) {
            try {
                List<ParticipantSelect> selects = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulas(formulaType);
                selectGridViewList.addAll(selects);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }

        return PageUtil.getDefaultPageList(selectGridViewList, ParticipantSelectGridView.class);
    }


    @RequestMapping("FormulaTypeList")
    @ResponseBody
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public TreeListResultModel<List<FormulaTypeBar>> getFormulaTypeList(Attributetype baseType) {
        List<FormulaType> formulaTypeList = new ArrayList<>();
        TreeListResultModel<List<FormulaTypeBar>> listResultModel = new TreeListResultModel();
        FormulaType[] formulaTypes = FormulaType.values();
        for (FormulaType formulaType : formulaTypes) {
            if (formulaType.getBaseType().equals(baseType)) {
                formulaTypeList.add(formulaType);

            }
        }
        listResultModel = TreePageUtil.getDefaultTreeList(formulaTypeList, FormulaTypeBar.class);
        return listResultModel;
    }

    private FormulaService getService() {
        FormulaService service = (FormulaService) EsbUtil.parExpression("$FormulaService");
        return service;
    }
}



