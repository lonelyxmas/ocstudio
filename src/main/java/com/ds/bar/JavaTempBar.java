package com.ds.bar;


import com.ds.bar.tempbar.*;
import com.ds.enums.IconEnumstype;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.ViewGroupType;
import com.ds.web.annotation.ViewType;

@TabsAnnotation(singleOpen = true, closeBtn = true)
@TreeAnnotation(heplBar = true, caption = "JAVA模板导航页")
public class JavaTempBar extends TreeListItem {


    @Pid
    DSMType dsmType;
    @Pid
    CustomDomainType customDomainType;

    @Pid
    AggregationType aggregationType;

    @Pid
    RepositoryType repositoryType;

    @Pid
    IconEnumstype itemDomainType;

    @Pid
    ViewType viewType;

    @Pid
    ViewGroupType viewGroupType;


    public JavaTempBar(String id, String caption, String imageClass) {
        this.id = id;
        this.caption = caption;
        this.imageClass = imageClass;
    }

    @TreeItemAnnotation(bindService = AdminTempService.class)
    public JavaTempBar(DSMType dsmType) {
        this.caption = dsmType.getName();
        this.id = dsmType.getType();
        this.imageClass = dsmType.getImageClass();
        this.dsmType = dsmType;
    }

    @TreeItemAnnotation(bindService = AggregationService.class)
    public JavaTempBar(AggregationType aggregationType) {
        this.caption = aggregationType.getName();
        this.imageClass = aggregationType.getImageClass();
        this.id = DSMType.aggregation.getType() + "_" + aggregationType.getType();
        this.aggregationType = aggregationType;
        this.dsmType = DSMType.aggregation;

    }

    @TreeItemAnnotation(bindService = RepositoryAdminService.class)
    public JavaTempBar(RepositoryType repositoryType) {
        this.caption = repositoryType.getName();
        this.imageClass = repositoryType.getImageClass();
        this.id = DSMType.repository.getType() + "_" + repositoryType.getType();
        this.repositoryType = repositoryType;
        this.dsmType = DSMType.repository;
    }


    @TreeItemAnnotation(bindService = ViewTempService.class)
    public JavaTempBar(ViewType viewType) {
        this.caption = viewType.getName();
        this.imageClass = viewType.getImageClass();
        this.id = DSMType.view.getType() + "_" + viewType.getType();
        this.viewType = viewType;

    }


    @TreeItemAnnotation(bindService = ViewGroupTreeService.class)
    public JavaTempBar(ViewGroupType viewGroupType, DSMType dsmType) {
        this.caption = viewGroupType.getName();
        this.imageClass = viewGroupType.getImageClass();
        this.viewGroupType = viewGroupType;
        this.dsmType = dsmType;
        this.id = dsmType + "_" + viewGroupType.getType();
    }

    public ViewGroupType getViewGroupType() {
        return viewGroupType;
    }

    public void setViewGroupType(ViewGroupType viewGroupType) {
        this.viewGroupType = viewGroupType;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public CustomDomainType getCustomDomainType() {
        return customDomainType;
    }

    public void setCustomDomainType(CustomDomainType customDomainType) {
        this.customDomainType = customDomainType;
    }

    public AggregationType getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(AggregationType aggregationType) {
        this.aggregationType = aggregationType;
    }

    public RepositoryType getRepositoryType() {
        return repositoryType;
    }

    public void setRepositoryType(RepositoryType repositoryType) {
        this.repositoryType = repositoryType;
    }

    public IconEnumstype getItemDomainType() {
        return itemDomainType;
    }

    public void setItemDomainType(IconEnumstype itemDomainType) {
        this.itemDomainType = itemDomainType;
    }

    public ViewType getViewType() {
        return viewType;
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }
}
