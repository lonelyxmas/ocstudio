package com.ds;

import com.ds.config.JDSConfig;
import com.ds.config.UserBean;
import com.ds.esb.config.manager.EsbBeanFactory;
import com.ds.esb.config.manager.ExpressionTempBean;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.esb.util.EsbFactory;
import com.ds.server.httpproxy.core.Server;
import java.io.*;
import java.util.*;
public class JDSInit {

    private static final long serialVersionUID = -7181186633660301865L;
    public static final String PRODUCT_NAME = "BSI-JDS VFSTools";


    public static final String PRODUCT_VERSION = "V0.5b";

    public static final String PRODUCT_COPYRIGHT = "Copyright(c)2012 - 2020 itjds.net, All Rights Reserved";


    public static int main(String[] args, InputStream in, PrintStream out) throws IOException {


        try {
            //  System.setProperty(JDSConfig.JDSHomeName, PathUtil.getJdsRealPath());
            System.out.println("************************************************");
            System.out.println("----- 欢迎使用 BSI-JDS VFSTools 管理工具");

            System.out.println(PRODUCT_NAME + " - " + PRODUCT_VERSION);
            System.out.println(PRODUCT_COPYRIGHT);
            System.out.println("************************************************");
            System.out.println("-------- VFSTools Initialization ---------");
            System.out.println("************************************************");
            System.out.println("- Start Connect Server - " + UserBean.getInstance().getServerUrl() + "*");
            System.out.println("- Connent VFSServer RUL    [" + UserBean.getInstance().getServerUrl() + "]*");

            System.out.println("- Connent VFSServer UserName    [" + UserBean.getInstance().getUsername() + "]*");
//            EsbFactory.initBus();
//            List<? extends ServiceBean> list = EsbBeanFactory.getInstance().loadAllServiceBean();
//            for (int k = 0; k < list.size(); k++) {
//                if (!(list.get(k) instanceof ExpressionTempBean)) {
//                    continue;
//                }
//                ExpressionTempBean bean = (ExpressionTempBean) list.get(k);
//                // System.out.println(bean.getName() + bean.getExpressionArr());
//            }
//
//            Properties props = new Properties();
//
//            System.out.println("------------" + JDSConfig.getAbsolutePath("/") + "server.properties");
//
//            props.load(new FileInputStream(JDSConfig.getAbsolutePath("") + "server.properties"));
//            ChainableProperties config = new ChainableProperties(props);
//
//            Server server = new Server(config);
//            try {
//                server.start();
//
//                synchronized (server) {
//                    server.wait();
//                }
//            } catch (InterruptedException e) {
//                // log.error("Server Interupted.");
//            }
//
//            System.out.println("************************************************");
        } catch (Exception e1) {
            return -1;
        }
        return 0;

    }


}
