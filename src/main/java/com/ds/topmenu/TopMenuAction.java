package com.ds.topmenu;


import com.ds.bar.FormulaTypeBar;
import com.ds.bar.JavaTempBar;
import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.enums.attribute.Attributetype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.EngineType;
import com.ds.esd.custom.annotation.TreeViewAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.dynbar.CustomToolsBar;
import com.ds.esd.custom.toolbar.dynbar.DynBar;
import com.ds.esd.custom.toolbar.dynbar.RADTopToolsBar;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.editor.extmenu.PluginsFactory;
import com.ds.esd.tool.ui.component.UIItem;
import com.ds.esd.tool.ui.component.data.APICallerComponent;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.component.event.Event;
import com.ds.esd.tool.ui.component.navigation.ToolBarProperties;
import com.ds.esd.tool.ui.enums.event.enums.ToolBarEventEnum;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Controller
@RequestMapping(value = {"/action/spatoolbar/"})
@MethodChinaName(cname = "RAD菜单")
public class TopMenuAction {
    @MethodChinaName(cname = "宏公式")
    @RequestMapping(method = RequestMethod.POST, value = "Formula")
    @ModuleAnnotation(dynLoad = true, imageClass = "bpmfont bpmgongzuoliu")
    @TreeViewAnnotation
    @ResponseBody
    public TreeListResultModel<List<FormulaTypeBar>> getFormulaMenu(EngineType engineType) {
        TreeListResultModel<List<FormulaTypeBar>> listResultModel = new TreeListResultModel();
        FormulaTypeBar root = new FormulaTypeBar();
        Attributetype[] attributetype = new Attributetype[]{
                Attributetype.PAGE,
                Attributetype.PAGERIGHT,
                Attributetype.TASK
        };

        root.setSub(TreePageUtil.fillObjs(Arrays.asList(attributetype), FormulaTypeBar.class));
        listResultModel.setData(Arrays.asList(root));

        return listResultModel;

    }

    @MethodChinaName(cname = "出码方案")
    @RequestMapping(method = RequestMethod.POST, value = "GenCode")
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-formatjs")
    @TreeViewAnnotation
    @ResponseBody
    public TreeListResultModel<List<JavaTempBar>> getGenCodeType(String id) {
        TreeListResultModel<List<JavaTempBar>> resultModel = new TreeListResultModel<List<JavaTempBar>>();
        JavaTempBar root = new JavaTempBar("GenCode", "出码模板", "spafont spa-icon-formatjs");
        DSMType[] types = new DSMType[]{
                DSMType.repository,
                DSMType.aggregation,
                DSMType.view
        };
        root.setSub(TreePageUtil.fillObjs(Arrays.asList(types), JavaTempBar.class));
        resultModel.setData(Arrays.asList(root));
        return resultModel;
    }

    @MethodChinaName(cname = "领域模型")
    @RequestMapping(method = RequestMethod.POST, value = "Domain")
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-formatjs")
    @TreeViewAnnotation
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> getDomain(String id) {
        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel<List<JavaTempNavTree>>();
        resultModel = TreePageUtil.getDefaultTreeList(Arrays.asList(DSMType.customDomain), JavaTempNavTree.class);
        return resultModel;
    }

    @ResponseBody
    @RequestMapping(value = {"getPluginBar"}, method = {RequestMethod.POST, RequestMethod.GET})
    public ResultModel<RADTopToolsBar> getPluginBar(CustomMenuType menuType, String pattern, String domainId, String projectName) {
        ResultModel<RADTopToolsBar> resultModel = new ResultModel<RADTopToolsBar>();
        RADTopToolsBar<ToolBarProperties, ToolBarEventEnum> toolsBar = new RADTopToolsBar("TopBar");
        try {
            List<DynBar> viewBars = PluginsFactory.getInstance().getAllViewBar(menuType, domainId);

            for (DynBar bar : viewBars) {
                if (bar instanceof CustomToolsBar) {
                    CustomToolsBar<ToolBarProperties, ToolBarEventEnum> childBar = PluginsFactory.getInstance().getViewBarById(bar.getId(), null, true);
                    Set<Map.Entry<ToolBarEventEnum, Event>> entrySet = childBar.getEvents().entrySet();
                    for (Map.Entry<ToolBarEventEnum, Event> entry : entrySet) {
                        for (Action action : entry.getValue().getActions()) {
                            toolsBar.addEvent((ToolBarEventEnum) entry.getValue().eventKey, action);
                        }
                    }
                    List<UIItem> items = childBar.getProperties().getItems();
                    for (UIItem item : items) {
                        toolsBar.getProperties().addItem(item);
                    }
                    List<APICallerComponent> apiCallerComponents = childBar.getApis();
                    for (APICallerComponent api : apiCallerComponents) {
                        toolsBar.getApis().add(api);
                    }
                }
            }
            resultModel.setData(toolsBar);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
