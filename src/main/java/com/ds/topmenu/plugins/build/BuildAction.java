package com.ds.topmenu.plugins.build;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.RemoteConnectionManager;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = {"/action/build/"})
@Aggregation(type = AggregationType.menu)
public class BuildAction {


    @RequestMapping(value = {"rebuildCustomModule"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 0, caption = "编译", imageClass = "spafont spa-icon-coin")
    @ResponseBody
    public ResultModel<Boolean> rebuildCustomModule(String projectName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        ProjectVersion version = null;
        try {
            version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            Map map = new HashMap();
            map.put("projectId", version.getProject().getId());
            DSMFactory.getInstance().compileProject(projectName, true);
            ESDFacrory.getESDClient().buildCustomModule(projectName, null, null, map, chrome);


        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(value = {"javaBuild"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 1, caption = "混合编译", imageClass = "spafont spa-icon-moveforward")
    @ResponseBody
    public ResultModel<Boolean> javaBuild(String projectName, String packageName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        try {

            if (packageName != null && !packageName.equals("")) {
                List names = Arrays.asList(new String[]{packageName.toString()});
                ESDFacrory.getESDClient().delFile(names, projectName.toString());
            }
            DSMFactory.getInstance().compileProject(projectName, true);
            ESDFacrory.getESDClient().buildCustomModule(projectName, packageName, null, null, chrome);
            ESDFacrory.getInstance().reload();


        } catch (Exception e) {
            e.printStackTrace();
            chrome.printError(e.getMessage());
            ((ErrorResultModel) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }


    @RequestMapping(value = {"split2"})
    @Split
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ResultModel<Boolean> split2() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }

    private void exportProject(String projectName, ESDChrome chrome, boolean deploy, boolean download) {
        try {
            ESDFacrory.getESDClient().exportProject(projectName, chrome, deploy, download);
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            chrome.execScript("xui.free('export')");
        }

    }

    @RequestMapping(value = {"export"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 3, caption = "打包发布", imageClass = "spafont spa-icon-package")
    @ResponseBody
    public ResultModel<Boolean> export(String projectName) {
        ESDChrome defaultChrome = getCurrChromeDriver();
        if (projectName != null) {
            RemoteConnectionManager.getConntctionService(projectName).execute(new Runnable() {
                @Override
                public void run() {
                    exportProject(projectName, defaultChrome, false, false);
                }
            });
        }
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(value = {"download"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 3, caption = "打包下载", imageClass = "spafont spa-icon-select1")
    @ResponseBody
    public ResultModel<Boolean> download(String projectName) {
        ESDChrome defaultChrome = getCurrChromeDriver();
        if (projectName != null) {
            RemoteConnectionManager.getConntctionService(projectName).execute(new Runnable() {
                @Override
                public void run() {
                    exportProject(projectName, defaultChrome, false, true);
                }
            });
        }
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 4)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "Public")
    @CustomAnnotation(index = 5)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "发布", imageClass = "spafont spa-icon-newprj")
    public PublicAction getPublicAction() {
        return new PublicAction();
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
