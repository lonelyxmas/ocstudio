package com.ds.topmenu.plugins.resource;

import com.ds.config.DSMResultModel;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = {"/action/resource/"})
@MenuBarMenu(menuType = CustomMenuType.menubar, caption = "资源配置", index = 3)
@Aggregation(type = AggregationType.menu)
public class ResourcesAction {


    @RequestMapping(value = {"FontList"}, method = {RequestMethod.POST})
    @ModuleAnnotation(caption = "引入字体",  imageClass = "spafont spa-icon-c-label")
    @DynLoadAnnotation(refClassName = "RAD.resource.FontList", projectName = "DSMdsm")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 0)
    @ResponseBody
    @DialogAnnotation(width = "800", height = "550")
    public ResultModel<Boolean> getFontList(String projectManager) {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(value = {"ImageConfigList"}, method = {RequestMethod.POST})
    @DialogAnnotation(width = "800", height = "550")
    @ModuleAnnotation(caption = "导入图片",  imageClass = "spafont spa-icon-html")
    @DynLoadAnnotation(refClassName = "RAD.resource.ImageConfigList", projectName = "DSMdsm")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 3)
    @ResponseBody
    public DSMResultModel<Boolean> getImageConfigList(String projectManager) {
        DSMResultModel resultModel = new DSMResultModel();
        return resultModel;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
