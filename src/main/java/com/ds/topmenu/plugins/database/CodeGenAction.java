package com.ds.topmenu.plugins.database;

import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.dsm.manager.repository.RepositoryNav;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.checkerframework.checker.units.qual.C;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/action/database/codegen/"})
@Aggregation(type = AggregationType.menu)
public class CodeGenAction {


    @RequestMapping(method = RequestMethod.POST, value = "DBInstList")
    @DialogAnnotation
    @MethodChinaName(cname = "资源库")
    @NavGroupViewAnnotation
    @CustomAnnotation(index = 0)
    @ModuleAnnotation(imageClass = "iconfont iconchucun", caption = "资源库")
    @ResponseBody
    public ResultModel<RepositoryNav> getTempNav(String projectVersionName) {
        ResultModel<RepositoryNav> result = new ResultModel<RepositoryNav>();
        return result;
    }


    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 1)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "CodeTemps")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @CustomAnnotation(index = 2)
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", dynLoad = true, caption = "模板管理")
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> getTempManager(String id) {
        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel<List<JavaTempNavTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(DSMType.repository), JavaTempNavTree.class);
        return resultModel;

    }


}
