package com.ds.topmenu.plugins.team;

import com.ds.common.JDSException;
import com.ds.config.DSMResultModel;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.web.RemoteConnectionManager;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = {"/action/team/configconfig"})
@Aggregation(type = AggregationType.menu)
public class TeamConfigAction {
    public TeamConfigAction() {

    }

    @RequestMapping(value = {"pull"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 0, caption = "拉取代码", imageClass = "spafont spa-icon-move-down")
    @ResponseBody
    public ResultModel<Boolean> startDebugServer(String projectName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        chrome.printLog("正在拉取，请稍后！", true);
        if (projectName == null) {
            chrome.printError(projectName + "must be null!");
        }
        if (projectName != null) {
            RemoteConnectionManager.getConntctionService(projectName.toString()).execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        ESDFacrory.getESDClient().reLoadProject(projectName.toString());
                    } catch (JDSException e) {
                        e.printStackTrace();
                    }
                    chrome.getChrome().navigate().refresh();
                    ESDFacrory.getInstance().dumpCache();
                }
            });

        }

        return resultModel;
    }


    @RequestMapping(value = {"push"}, method = {RequestMethod.POST})
    @CustomAnnotation(index = 1, caption = "提交代码", imageClass = "spafont spa-icon-move-up")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @ResponseBody
    public ResultModel<Boolean> push(String projectName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        chrome.printLog("正在提交，请稍后关闭浏览器！", true);
        if (projectName != null) {
            RemoteConnectionManager.getConntctionService(projectName).execute(new Runnable() {
                @Override
                public void run() {
                    ESDFacrory.getInstance().dumpCache();
                }
            });
        }

        return resultModel;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }

    @RequestMapping(value = {"ProjectVersionSelector"}, method = {RequestMethod.POST})
    @DialogAnnotation(width = "300", height = "380")
    @ModuleAnnotation(caption = "切换分支")
    @DynLoadAnnotation(refClassName = "RAD.ProjectVersionSelector", projectName = "DSMdsm")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 3, imageClass = "spafont spa-icon-cancel")
    @ResponseBody
    public DSMResultModel<Boolean> getRemoteServerList(String projectName) {
        DSMResultModel resultModel = new DSMResultModel();
        return resultModel;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
