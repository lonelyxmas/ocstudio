package com.ds.topmenu.plugins.team;

import com.ds.config.DSMResultModel;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.RemoteConnectionManager;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = {"/action/team/"})
@MethodChinaName(cname = "协同")
@MenuBarMenu(menuType = CustomMenuType.menubar, caption = "协同", index = 4)
@Aggregation(type = AggregationType.menu)
public class TeamAction {

    @RequestMapping(method = RequestMethod.POST, value = "CodeConfig")
    @CustomAnnotation(index = 0)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "代码分支", imageClass = "spafont spa-icon-shukongjian")
    public TeamConfigAction getCodeConfig() {
        return new TeamConfigAction();
    }


    @RequestMapping(value = {"split2"})
    @Split
    @CustomAnnotation(index = 1)
    @ResponseBody
    public ResultModel<Boolean> split2() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(value = {"PersonList"}, method = {RequestMethod.POST})
    @ModuleAnnotation(caption = "团队授权")
    @DynLoadAnnotation(refClassName = "RAD.org.PersonList", projectName = "DSMdsm")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 2, imageClass = "spafont spa-icon-login")
    @ResponseBody
    @DialogAnnotation( width = "800", height = "550")
    public DSMResultModel<Boolean> getPersonList(String projectManager) {
        DSMResultModel resultModel = new DSMResultModel();
        return resultModel;
    }


    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 3)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(value = {"clearAll"}, method = {RequestMethod.POST})
    @CustomAnnotation(index = 4, caption = "强制清空", imageClass = "spafont spa-icon-options")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @ResponseBody
    public ResultModel<Boolean> clearAll(String projectName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        chrome.printLog("正在提交，请稍后关闭浏览器！", true);
        chrome.printLog("清空后会重启刷新浏览器", true);
        if (projectName != null) {
            RemoteConnectionManager.getConntctionService(projectName.toString()).execute(new Runnable() {
                @Override
                public void run() {
                    ESDFacrory.getInstance().clearCache();
                    chrome.getChrome().navigate().refresh();
                }
            });
        }

        return resultModel;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
