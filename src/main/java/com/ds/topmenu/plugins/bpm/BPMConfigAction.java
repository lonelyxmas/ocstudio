package com.ds.topmenu.plugins.bpm;

import com.ds.bpm.plugins.bpd.PluginTypeTree;
import com.ds.bpm.plugins.bpd.PluginsItems;
import com.ds.bpm.plugins.listener.ListenerItems;
import com.ds.bpm.plugins.nav.ListenerTypeTree;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/action/bpm/config/"})
@Aggregation(type = AggregationType.menu)
public class BPMConfigAction {

    @RequestMapping(method = RequestMethod.POST, value = "PluginNavTree")
    @ModuleAnnotation(dynLoad = true, caption = "工作流插件", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @NavTreeViewAnnotation
    @ResponseBody
    public TreeListResultModel<List<PluginTypeTree>> getPlugins(String id, String projectVersionName) {
        TreeListResultModel<List<PluginTypeTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(PluginsItems.values()), PluginTypeTree.class);
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ListenerNavTree")
    @NavTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-conf1", caption = "监听器")
    @ResponseBody
    public TreeListResultModel<List<ListenerTypeTree>> getListeners(String id, String projectVersionName) {
        TreeListResultModel<List<ListenerTypeTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(ListenerItems.values()), ListenerTypeTree.class);

        return result;
    }

}
