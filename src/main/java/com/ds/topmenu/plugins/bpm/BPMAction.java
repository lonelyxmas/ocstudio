package com.ds.topmenu.plugins.bpm;

import com.ds.bpm.bpd.BPD;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esb.config.formula.EngineType;
import com.ds.esd.bpm.worklist.WorkListService;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.formula.manager.formula.item.FormulaTypeItem;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/action/bpm/"})
@MenuBarMenu(menuType = CustomMenuType.menubar, caption = "流程", index = 5)
@Aggregation(type=AggregationType.menu)
public class BPMAction {


    @RequestMapping(method = RequestMethod.POST, value = "BPD")
    @CustomAnnotation(index = 0, caption = "绘制流程", imageClass = "spafont spa-icon-designview")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @ResponseBody
    public ResultModel<Boolean> openBPD() {
        ResultModel resultModel = new ResultModel();
        BPD.getInstance().init(true);
        BPD.getInstance().autoLogin(ESDEditor.getInstance().getUser());
        return resultModel;
    }


    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation( index = 1)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "BPMTest")
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-action", caption = "仿真测试")
    @CustomAnnotation(index = 2)
    @DialogAnnotation
    @APIEventAnnotation()
    @ResponseBody
    public ResultModel<WorkListService> getBPMTest() {
        return new ResultModel<WorkListService>();
    }


    @RequestMapping(value = {"split4"})
    @Split
    @CustomAnnotation( index = 3)
    @ResponseBody
    public ResultModel<Boolean> split4() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ExpressionConfig")
    @NavTreeViewAnnotation
    @CustomAnnotation(index = 4)
    @DialogAnnotation
    @ModuleAnnotation( imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "通用配置")
    @ResponseBody
    public TreeListResultModel<List<FormulaTypeItem>> getExpressionConfig(String id) {
        TreeListResultModel<List<FormulaTypeItem>> resultModel = new TreeListResultModel<List<FormulaTypeItem>>();
        List<FormulaTypeItem> items = new ArrayList<>();
        FormulaTypeItem root = new FormulaTypeItem(EngineType.CUSTOM);
        items.add(root);
        if (id != null && !id.equals("")) {
            String[] orgIdArr = StringUtility.split(id, ";");
            resultModel.setIds(Arrays.asList(orgIdArr));
        } else {
            resultModel.setIds(Arrays.asList(new String[]{root.getFristClassItem(root).getId()}));
        }
        resultModel.setData(items);
        return resultModel;

    }


    @RequestMapping(method = RequestMethod.POST, value = "BPMConfig")
    @CustomAnnotation(index = 5)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "流程配置", imageClass = "spafont spa-icon-conf1")
    public BPMConfigAction getBPMConfigAction() {
        BPMConfigAction resultModel = new BPMConfigAction();
        return resultModel;
    }

    @RequestMapping(value = {"split6"})
    @Split
    @CustomAnnotation( index = 6)
    @ResponseBody
    public ResultModel<Boolean> split5() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "BPMRight")
    @CustomAnnotation(index = 7)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "流程权限", imageClass = "spafont spa-icon-phonegap")
    public BPMRightAction getBPMRightAction() {
        return new BPMRightAction();
    }


    @RequestMapping(value = {"split7"})
    @Split
    @CustomAnnotation(index = 8)
    @ResponseBody
    public ResultModel<Boolean> split3() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "BPMDev")
    @CustomAnnotation(index = 9)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "表单开发", imageClass = "spafont spa-icon-conf1")
    public BPMDevAction getBPMDevAction() {
        BPMDevAction resultModel = new BPMDevAction();
        return resultModel;
    }


    public ESDClient getClient() throws JDSException {
        ESDClient client = ESDFacrory.getESDClient();
        return client;
    }

    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
