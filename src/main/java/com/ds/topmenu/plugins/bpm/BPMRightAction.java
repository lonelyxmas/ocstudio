package com.ds.topmenu.plugins.bpm;

import com.ds.admin.iorg.IOrgNav;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.attribute.Attributetype;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavFoldingTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.manager.formula.item.FormulaTypeItem;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/action/bpm/config/"})
@Aggregation(type = AggregationType.menu)
public class BPMRightAction {

    @RequestMapping(method = RequestMethod.POST, value = "OrgNav")
    @NavFoldingTreeViewAnnotation
    @CustomAnnotation(index = 1)
    @DialogAnnotation
    @ModuleAnnotation(imageClass = "bpmfont bpmgongzuoliu", cache = false, caption = "组织机构")
    @ResponseBody
    public ResultModel<IOrgNav> getOrgManager() {
        return new ResultModel<IOrgNav>();
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "BpmRight")
    @NavTreeViewAnnotation
    @CustomAnnotation(index = 3)
    @DialogAnnotation(width = "600", height = "450")
    @ModuleAnnotation(dynLoad = true, imageClass = "bpmfont bpmgongzuoliu", caption = "办理权限")
    @ResponseBody
    public TreeListResultModel<List<FormulaTypeItem>> getFormulaManager(String id) {
        TreeListResultModel<List<FormulaTypeItem>> resultModel = new TreeListResultModel<List<FormulaTypeItem>>();
        List<FormulaTypeItem> items = new ArrayList<>();
        FormulaTypeItem root = new FormulaTypeItem(Attributetype.RIGHT);
        items.add(root);
        if (id != null && !id.equals("")) {
            String[] orgIdArr = StringUtility.split(id, ";");
            resultModel.setIds(Arrays.asList(orgIdArr));
        } else {
            resultModel.setIds(Arrays.asList(new String[]{root.getFristClassItem(root).getId()}));
        }
        resultModel.setData(items);
        return resultModel;

    }


}
