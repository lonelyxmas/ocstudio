package com.ds.topmenu.plugins;

import com.ds.admin.db.nav.DBNavTreeView;
import com.ds.common.JDSException;
import com.ds.common.database.metadata.ProviderConfig;
import com.ds.config.DSMResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping(value = {"/action/database/"})
@MenuBarMenu(menuType = CustomMenuType.top, caption = "数据库", index = 2)
@Aggregation(type = AggregationType.menu)
public class DataBaseAction {

    @RequestMapping(value = {"ProviderList"}, method = {RequestMethod.POST})
    @ModuleAnnotation(caption = "数据源")
    @DialogAnnotation
    @CustomAnnotation(index = 0, imageClass = "iconfont iconchucun")
    @DynLoadAnnotation(refClassName = "RAD.db.DBProviderList", projectName = "DSMdsm")
    @APIEventAnnotation(autoRun = true)
    public @ResponseBody
    ResultModel<Boolean> getProviderList() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 1)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "DbManager")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @CustomAnnotation(index = 2, imageClass = "spafont spa-icon-c-grid")
    @ModuleAnnotation(caption = "库表管理", dynLoad = true)
    @DialogAnnotation(width = "850")
    @ResponseBody
    public TreeListResultModel<List<DBNavTreeView>> getTabelManager(String id) {
        TreeListResultModel<List<DBNavTreeView>> resultModel = new TreeListResultModel<List<DBNavTreeView>>();
        try {
            List<ProviderConfig> configs = ESDFacrory.getESDClient().getAllDbConfig();
            resultModel = TreePageUtil.getTreeList(configs, DBNavTreeView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(value = {"importTable"}, method = {RequestMethod.POST})
    @ModuleAnnotation(caption = "导入库表")
    @DynLoadAnnotation(refClassName = "RAD.db.project.DataBaseConfigList", projectName = "DSMdsm")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 3, imageClass = "fa fa-database")
    @DialogAnnotation(width = "800", height = "550")
    @ResponseBody
    public DSMResultModel<Boolean> importTable(String projectName) {
        DSMResultModel resultModel = new DSMResultModel();
        return resultModel;
    }


    @RequestMapping(value = {"split2"})
    @Split
    @CustomAnnotation(index = 4)
    @ResponseBody
    public ResultModel<Boolean> split2() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(value = {"SqlConsole"}, method = {RequestMethod.POST})
    @ModuleAnnotation(caption = "运行监控")
    @DynLoadAnnotation(refClassName = "RAD.db.console.SqlConsole", projectName = "DSMdsm")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 5, imageClass = "spafont spa-icon-c-video")
    @ResponseBody
    @DialogAnnotation(width = "800", height = "550")
    public DSMResultModel<Boolean> sqlConsole(String projectName) {
        DSMResultModel resultModel = new DSMResultModel();
        return resultModel;
    }

    public ESDClient getClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }


}
