package com.ds.topmenu.plugins;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.config.AggDsmConfigTree;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.CustomViewFactory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.util.TreePageUtil;
import com.ds.server.JDSServer;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URL;
import java.util.List;


//@Controller
//@RequestMapping(value = {"/action/tool/otherproxy/"})
//@MethodChinaName(cname = "切换")
//
//@MenuBarMenu(menuType = CustomMenuType.top, caption = "切换", index = 6, imageClass = "spafont spa-icon-alignw")
//@Aggregation(type = AggregationType.menu)
public class OtherProxyAction {


    @RequestMapping(value = {"debugProxy"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 1, caption = "预览工程", imageClass = "spafont spa-icon-previewpage")
    @ResponseBody
    public void debugProxy(String sessionId, String projectName) {
        ESDChrome chrome = getCurrChromeDriver();
        try {
            if (sessionId == null) {
                sessionId = JDSServer.getInstance().getAdminUser().getSessionId();
            }
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            URL rUrl = new URL(version.getProject().getPublicServerUrl());
            ESDEditor.getInstance().openNOProxyWin(rUrl, "/RAD/" + projectName + "/Debug.ModuleTree.view?domainId=" + version.getProject().getProjectName(), projectName, sessionId);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }

    @RequestMapping(value = {"dsmDesigner"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 3, caption = "DSM编辑", imageClass = "spafont spa-icon-conf")
    @ResponseBody
    public void dsmDesigner(String sessionId, String projectName) {
        ESDChrome chrome = getCurrChromeDriver();
        try {
            if (sessionId == null) {
                sessionId = JDSServer.getInstance().getAdminUser().getSessionId();
            }
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            URL rUrl = new URL(version.getProject().getPublicServerUrl());
            ESDEditor.getInstance().openNOProxyWin(rUrl, "/RAD/DSMdsm/projectManager", projectName, sessionId);

            //ESDEditor.getInstance().openNewWin(rUrl, sessionId);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @RequestMapping(value = {"bpmtest"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 3, caption = "流程仿真", imageClass = "spafont spa-icon-action")
    @ResponseBody
    public void bpmtest(String sessionId, String projectName) {
        ESDChrome chrome = getCurrChromeDriver();
        try {
            if (sessionId == null) {
                sessionId = JDSServer.getInstance().getAdminUser().getSessionId();
            }
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            URL rUrl = new URL(version.getProject().getPublicServerUrl());
            ESDEditor.getInstance().openNOProxyWin(rUrl, "/RAD/" + projectName + "/admin.Index.view", projectName, sessionId);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @RequestMapping(method = RequestMethod.POST, value = "DomainConfig")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "950", height = "680")
    @CustomAnnotation(imageClass = "spafont spa-icon-c-cssbox", index = 4)
    @ModuleAnnotation(dynLoad = true, caption = "领域配置")
    @ResponseBody
    public TreeListResultModel<List<AggDsmConfigTree>> loadDomainList(String projectName) {
        TreeListResultModel<List<AggDsmConfigTree>> result = new TreeListResultModel<>();
        try {
            List<DomainInst> domainInstList = DSMFactory.getInstance().getAggregationManager().getDomainInstList(projectName);
            domainInstList.add(DSMFactory.getInstance().getAggregationManager().getDomainInstById(CustomViewFactory.DSMdsm));
            result = TreePageUtil.getTreeList(domainInstList, AggDsmConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;

    }


    private ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
