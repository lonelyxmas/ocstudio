package com.ds.topmenu.plugins.temp;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/action/form/"})
@MethodChinaName(cname = "表单")
@MenuBarMenu(menuType = CustomMenuType.toolbar, caption = "表单", index = 5)
@Aggregation(type = AggregationType.menu)
public class FormAction {

    @MethodChinaName(cname = "代码模板")
    @RequestMapping(method = RequestMethod.POST, value = "CodeTemps")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @CustomAnnotation(index = 2)
    @DialogAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", dynLoad = true, caption = "代码模板")
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> getTempManager(String id) {
        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel<List<JavaTempNavTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(DSMType.values()), JavaTempNavTree.class);
        return resultModel;

    }

    @RequestMapping(method = RequestMethod.POST, value = "ViewTemps")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @CustomAnnotation(index = 3)
    @DialogAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-config", dynLoad = true, caption = "视图配置")
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> getViewTemps(String id) {
        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel<List<JavaTempNavTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(DSMType.aggregation), JavaTempNavTree.class);

        return resultModel;

    }

}
