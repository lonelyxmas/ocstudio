package com.ds.topmenu.plugins.temp;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/action/action/"})
@MethodChinaName(cname = "模板")
@MenuBarMenu(menuType = CustomMenuType.top, caption = "模板", index = 4, imageClass = "spafont spa-icon-settingprj")
@Aggregation(type = AggregationType.menu)
public class ActionAction {

    @MethodChinaName(cname = "代码模板")
    @RequestMapping(method = RequestMethod.POST, value = "CodeTemps")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @CustomAnnotation(imageClass = "spafont spa-icon-settingprj", index = 2)
    @DialogAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "代码模板")
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> getTempManager(String id) {
        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel<List<JavaTempNavTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(DSMType.values()), JavaTempNavTree.class);
        return resultModel;

    }

    @RequestMapping(method = RequestMethod.POST, value = "ViewTemps")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @CustomAnnotation(imageClass = "spafont spa-icon-config", index = 3)
    @DialogAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "视图配置")
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> getViewTemps(String id) {
        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel<List<JavaTempNavTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(DSMType.aggregation), JavaTempNavTree.class);
        return resultModel;

    }

}
