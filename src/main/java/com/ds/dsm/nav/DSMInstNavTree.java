package com.ds.dsm.nav;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.manager.aggregation.AggregationGridView;
import com.ds.dsm.manager.repository.RepositoryNav;
import com.ds.dsm.manager.view.ViewGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

//@Controller
//@RequestMapping("/dsm/manager/")
//@MethodChinaName(cname = "领域模型")
//@TabsAnnotation(singleOpen = true)
public class DSMInstNavTree {


    @CustomAnnotation(hidden = true, pid = true)
    public String projectVersionName;


    public DSMInstNavTree() {

    }

    public DSMInstNavTree(String projectVersionName) {
        this.projectVersionName = projectVersionName;

    }

//    @MethodChinaName(cname = "资源库")
//    @RequestMapping(method = RequestMethod.POST, value = "DSMInstNav")
//    @NavGroupViewAnnotation
//    @ModuleAnnotation(imageClass = "iconfont iconchucun", caption = "仓储库")
//    @CustomAnnotation(index = 1)
//    @TreeItemAnnotation
//    @ResponseBody
//    public ResultModel<RepositoryNav> getTempNav(String projectVersionName) {
//        ResultModel<RepositoryNav> result = new ResultModel<RepositoryNav>();
//        return result;
//    }
//
//
//    @RequestMapping(method = RequestMethod.POST, value = "AggregationList")
//    @GridViewAnnotation
//    @ModuleAnnotation(imageClass = "spafont spa-icon-module", caption = "领域模型")
//    @CustomAnnotation(index = 2)
//    @ResponseBody
//    public ListResultModel<List<AggregationGridView>> getAggregationList(String projectVersionName) {
//        ListResultModel<List<AggregationGridView>> result = new ListResultModel();
//        try {
//            if (projectVersionName == null || projectVersionName.equals("")) {
//                throw new JDSException("projectVersionName is null");
//            }
//            List<DomainInst> tempBeans = DSMFactory.getInstance().getAggregationManager().getDomainInstList(projectVersionName, true);
//            result = PageUtil.getDefaultPageList(tempBeans, AggregationGridView.class);
//        } catch (JDSException e) {
//            e.printStackTrace();
//            result = new ErrorListResultModel<>();
//            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
//            ((ErrorListResultModel) result).setErrdes(e.getMessage());
//        }
//        return result;
//    }
//
//    @MethodChinaName(cname = "视图工厂")
//    @RequestMapping(method = RequestMethod.POST, value = "ViewInstList")
//    @GridViewAnnotation
//    @ModuleAnnotation(imageClass = "spafont spa-icon-conf", caption = "视图工厂")
//    @CustomAnnotation(index = 3)
//    @ResponseBody
//    public ListResultModel<List<ViewGridView>> getViewInstList(String projectVersionName) {
//        ListResultModel<List<ViewGridView>> result = new ListResultModel();
//        try {
//            if (projectVersionName == null || projectVersionName.equals("")) {
//                throw new JDSException("projectVersionName is null");
//            }
//            List<ViewInst> tempBeans = DSMFactory.getInstance().getViewManager().getViewList(projectVersionName, true);
//            result = PageUtil.getDefaultPageList(tempBeans, ViewGridView.class);
//        } catch (JDSException e) {
//            e.printStackTrace();
//            result = new ErrorListResultModel<>();
//            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
//            ((ErrorListResultModel) result).setErrdes(e.getMessage());
//        }
//        return result;
//    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
