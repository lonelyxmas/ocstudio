package com.ds.dsm.nav.agg;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.aggregation.AggregationFormView;
import com.ds.dsm.manager.aggregation.AggregationNav;
import com.ds.dsm.nav.DSMNavTree;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@RequestMapping("/dsm/manager/agg/inst/")
@Controller
public class AggInstService {


//    @RequestMapping(method = RequestMethod.POST, value = "AggregationInfo")
//    @FormViewAnnotation()
//    @ModuleAnnotation(caption = "实体信息")
//    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
//    @ResponseBody
//    public ResultModel<AggregationFormView> getAggregationInfo(String domainId, String projectVersionName) {
//        ResultModel<AggregationFormView> result = new ResultModel<AggregationFormView>();
//        DomainInst domainInst = null;
//        try {
//            domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
//        } catch (JDSException e) {
//            e.printStackTrace();
//        }
//        result.setData(new AggregationFormView(domainInst));
//        return result;
//    }

    @RequestMapping(method = RequestMethod.POST, value = "AggregationInfo")
    @NavGroupViewAnnotation()
    @ModuleAnnotation(caption = "聚合信息")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treegridrow, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<AggregationNav> getAggregationInfo(String domainId) {
        ResultModel<AggregationNav> result = new ResultModel<AggregationNav>();
        return result;
    }



    @RequestMapping(method = RequestMethod.POST, value = "AggregationNav")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<DSMNavTree>> getAggregationNav(String projectVersionName, String domainId) {
        TreeListResultModel<List<DSMNavTree>> result = TreePageUtil.getDefaultTreeList(Arrays.asList(AggInstNavItems.values()), DSMNavTree.class);
        return result;
    }
}
