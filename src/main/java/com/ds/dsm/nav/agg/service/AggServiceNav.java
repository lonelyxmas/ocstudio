package com.ds.dsm.nav.agg.service;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.nav.DSMNavTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@RequestMapping("/dsm/manager/agg/")
@Controller
public class AggServiceNav {


    @RequestMapping(method = RequestMethod.POST, value = "AggregationService")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<DSMNavTree>> getAggregationNav(String domainId, String projectVersionName) {
        TreeListResultModel<List<DSMNavTree>> result = TreePageUtil.getDefaultTreeList(Arrays.asList(AggServiceItems.values()), DSMNavTree.class);
        return result;
    }

}
