package com.ds.dsm.nav.agg.view;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.view.entity.ViewEntityGridView;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/manager/agg/view/")
@Controller
public class AggViewEntityService {
    @RequestMapping(method = RequestMethod.POST, value = "ESDClassList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-webapp", caption = "实体列表")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<ViewEntityGridView>> getESDClassList(String viewInstId) {
        ListResultModel<List<ViewEntityGridView>> result = new ListResultModel();
        try {
            ViewInst bean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            List<ESDClass> esdClassList = new ArrayList<>();
            if (bean != null) {
                esdClassList = bean.getAggList();
                result = PageUtil.getDefaultPageList(esdClassList, ViewEntityGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

}
