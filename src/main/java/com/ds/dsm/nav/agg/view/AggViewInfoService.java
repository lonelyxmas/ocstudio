package com.ds.dsm.nav.agg.view;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.manager.view.ViewFormView;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/dsm/manager/agg/view/")
@Controller
public class AggViewInfoService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewInfo")
    @FormViewAnnotation()
    @UIAnnotation(height = "150")
    @ModuleAnnotation(caption = "实体信息", dock = Dock.top)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<ViewFormView> getViewInfo(String viewInstId, String projectVersionName) {
        ResultModel<ViewFormView> result = new ResultModel<ViewFormView>();
        ViewInst viewInst = null;
        try {
            viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            ViewFormView formView = new ViewFormView(viewInst);
            result.setData(formView);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


}
