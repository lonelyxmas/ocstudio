package com.ds.dsm.nav.agg.view;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.view.ViewTempNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.ViewGroupType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@RequestMapping("/dsm/manager/agg/")
@Controller
public class AggViewTempsService {


    @MethodChinaName(cname = "代码工厂")
    @RequestMapping(method = RequestMethod.POST, value = "ViewTemps")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", dynLoad = true, caption = "代码工厂")
    @CustomAnnotation(index = 3)
    @ResponseBody
    public TreeListResultModel<List<ViewTempNavTree>> getViewTemps(String id, String viewInstId) {
        TreeListResultModel<List<ViewTempNavTree>> resultModel = new TreeListResultModel<List<ViewTempNavTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(ViewGroupType.values()), ViewTempNavTree.class);
        return resultModel;
    }

}
