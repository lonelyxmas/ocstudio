package com.ds.dsm.nav.agg;

import com.ds.dsm.aggregation.config.domain.AggDomainService;
import com.ds.dsm.nav.agg.entity.AggEntityService;
import com.ds.dsm.nav.agg.java.AggJavaService;
import com.ds.dsm.nav.agg.java.AggSourceService;
import com.ds.dsm.nav.agg.service.AggServiceNav;
import com.ds.dsm.nav.agg.temp.AggTempsService;
import com.ds.dsm.nav.agg.view.AggViewNavService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum AggInstNavItems implements TreeItem {
    AggSoruceList("实体源", "spafont spa-icon-c-gallery", AggSourceService.class, false, false, false),
    AggDomainList("通用域", "spafont spa-icon-conf", AggDomainService.class, false, false, false),
    AggregationService("领域服务", "spafont spa-icon-c-menu", AggServiceNav.class, false, false, false),
    ViewService("通用视图","spafont spa-icon-c-gallery", AggViewNavService.class, false, false, false),
    AggEntityNav("聚合实体", "spafont spa-icon-conf", AggEntityService.class, false, false, false);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    AggInstNavItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
