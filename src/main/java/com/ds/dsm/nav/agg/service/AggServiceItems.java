package com.ds.dsm.nav.agg.service;

import com.ds.dsm.aggregation.config.domain.AggDomainService;
import com.ds.dsm.nav.agg.entity.AggEntityService;
import com.ds.dsm.nav.agg.entity.AggRootService;
import com.ds.dsm.nav.agg.java.AggJavaService;
import com.ds.dsm.nav.agg.java.AggSourceService;
import com.ds.dsm.nav.agg.temp.AggTempsService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum AggServiceItems implements TreeItem {
    AggMenuList("菜单动作", "spafont spa-icon-c-menu", AggMenuService.class, false, false, false),
    AggAPIList("服务接口", "spafont spa-icon-c-webapi", AggAPIService.class, false, false, false);
      private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    AggServiceItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
