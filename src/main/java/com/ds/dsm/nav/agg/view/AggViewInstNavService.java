package com.ds.dsm.nav.agg.view;

import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.view.ViewMetaView;
import com.ds.dsm.nav.DSMNavTree;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@RequestMapping("/dsm/manager/view/inst/")
@Controller
public class AggViewInstNavService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewInstNav")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<DSMNavTree>> getViewNav(String projectVersionName, String viewInstId, String domainId) {
        TreeListResultModel<List<DSMNavTree>> result = TreePageUtil.getDefaultTreeList(Arrays.asList(AggViewItems.values()), DSMNavTree.class);
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ViewMetaView")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "关联信息", imageClass = "spafont spa-icon-c-databinder")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ViewMetaView> getViewMetaView(String viewInstId, String projectVersionName, String className) {
        ResultModel<ViewMetaView> result = new ResultModel<ViewMetaView>();
        return result;

    }

}
