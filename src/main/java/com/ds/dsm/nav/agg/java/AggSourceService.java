package com.ds.dsm.nav.agg.java;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.aggregation.config.source.AggSoruceGridView;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/manager/agg/")
@Controller
public class AggSourceService {
    @RequestMapping(method = RequestMethod.POST, value = "AggSoruceList")
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-gallery", caption = "实体源")
    @CustomAnnotation(index = 0)
    @ResponseBody
    public ListResultModel<List<AggSoruceGridView>> getSourceList(String domainId) {
        ListResultModel<List<AggSoruceGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAllServiceClass();
                result = PageUtil.getDefaultPageList(esdClassList, AggSoruceGridView.class);
            }

        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

}
