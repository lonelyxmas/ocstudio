package com.ds.dsm.nav.agg.view;

import com.ds.dsm.aggregation.config.domain.AggDomainService;
import com.ds.dsm.nav.agg.entity.AggEntityService;
import com.ds.dsm.nav.agg.java.AggJavaService;
import com.ds.dsm.nav.agg.java.AggSourceService;
import com.ds.dsm.nav.agg.service.AggServiceNav;
import com.ds.dsm.nav.agg.temp.AggTempsService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum AggViewItems implements TreeItem {
    ViewList("视图列表","spafont spa-icon-c-webapp", AggViewService.class, false, false, false),
    JavaList("Java源码", "spafont spa-icon-json-file", AggViewJavaService.class, false, false, false),
    ESDClassList("聚合实体", "spafont spa-icon-conf", AggViewEntityService.class, false, false, false),
    AggregationTemps("代码工厂", "spafont spa-icon-settingprj", AggViewTempsService.class, false, false, false);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    AggViewItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
