package com.ds.dsm.nav.agg.temp;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.manager.aggregation.AggregationTempNavTree;
import com.ds.dsm.manager.temp.agg.AggTempGrid;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.GridPageUtil;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/dsm/manager/agg/")
@Controller
public class AggTempsService {


    @RequestMapping(method = RequestMethod.POST, value = "AggTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AggTempGrid")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<AggTempGrid>> getAggTempGrid(String domainId, String projectName, AggregationType aggregationType) {
        ListResultModel<List<AggTempGrid>> resultModel = new ListResultModel();
        DomainInst bean = null;
        try {
            if (domainId == null || domainId.equals("")) {
                if (projectName != null && !projectName.equals("")) {
                    Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
                    domainId = project.getProjectName();
                }
            }
            bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null) {
                        if (aggregationType == null || (javaTemp.getAggregationType() != null && javaTemp.getAggregationType().equals(aggregationType))) {
                            temps.add(javaTemp);
                        }
                    }
                }
                resultModel = GridPageUtil.getDefaultPageList(temps, AggTempGrid.class);
            }
        } catch (JDSException e) {
            resultModel = new ErrorListResultModel<>();
            ((ErrorListResultModel) resultModel).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) resultModel).setErrdes(e.getMessage());
        }

        return resultModel;
    }


    @MethodChinaName(cname = "代码工厂")
    @RequestMapping(method = RequestMethod.POST, value = "AggregationTemps")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggregationTempNavTree>> getAggregationTemps(String id, String projectName, String domainId) {
        TreeListResultModel<List<AggregationTempNavTree>> result = new TreeListResultModel<>();
        if (domainId == null || domainId.equals("")) {
            if (projectName != null && !projectName.equals("")) {
                Project project = null;
                try {
                    project = ESDFacrory.getESDClient().getProjectByName(projectName);
                } catch (JDSException e) {
                    e.printStackTrace();
                }
                domainId = project.getProjectName();
                JDSActionContext.getActionContext().getContext().put("domainId", domainId);
            }
        }
        result = TreePageUtil.getTreeList(Arrays.asList(AggregationType.values()), AggregationTempNavTree.class);
        return result;
    }

}
