package com.ds.dsm.nav.agg.view;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.view.ViewGridView;
import com.ds.dsm.nav.DSMNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@RequestMapping("/dsm/manager/view/")
@Controller
public class AggViewNavService {


    @MethodChinaName(cname = "视图工厂")
    @RequestMapping(method = RequestMethod.POST, value = "ViewInstList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf", caption = "视图工厂")
    @CustomAnnotation(index = 3)
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<ViewGridView>> getViewInstList(String projectVersionName, String domainId) {
        ListResultModel<List<ViewGridView>> result = new ListResultModel();
        try {
            if (projectVersionName == null || projectVersionName.equals("")) {
                throw new JDSException("projectVersionName is null");
            }
            List<ViewInst> tempBeans = DSMFactory.getInstance().getViewManager().getViewInstList(projectVersionName, domainId);
            result = PageUtil.getDefaultPageList(tempBeans, ViewGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "AggViewNav")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<DSMNavTree>> getAggViewNav(String projectVersionName, String domainId) {

        TreeListResultModel<List<DSMNavTree>> result = new TreeListResultModel();
        try {
            if (projectVersionName == null || projectVersionName.equals("")) {
                throw new JDSException("projectVersionName is null");
            }
            List<ViewInst> tempBeans = DSMFactory.getInstance().getViewManager().getViewInstList(projectVersionName, domainId);
            result = TreePageUtil.getDefaultTreeList(tempBeans, DSMNavTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }
}
