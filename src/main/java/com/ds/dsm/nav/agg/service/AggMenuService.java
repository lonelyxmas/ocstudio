package com.ds.dsm.nav.agg.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.aggregation.config.menu.AggMenuGridView;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/manager/agg/")
@Controller
public class AggMenuService {



    @RequestMapping(method = RequestMethod.POST, value = "AggMenuList")
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-menu", caption = "菜单动作")
    @CustomAnnotation( index = 3)
    @ResponseBody
    public ListResultModel<List<AggMenuGridView>> getAggMenuList(String domainId) {
        ListResultModel<List<AggMenuGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAggMenus();
                result = PageUtil.getDefaultPageList(esdClassList, AggMenuGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


}
