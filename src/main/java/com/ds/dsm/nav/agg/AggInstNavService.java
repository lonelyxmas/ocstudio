package com.ds.dsm.nav.agg;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.nav.DSMNavTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/manager/agg/")
@Controller
public class AggInstNavService {

//
//    @RequestMapping(method = RequestMethod.POST, value = "AggregationList")
//    @GridViewAnnotation
//    @ModuleAnnotation(imageClass = "spafont spa-icon-module", caption = "领域模型")
//    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
//    @ResponseBody
//    public ListResultModel<List<AggregationGridView>> getAggregationList(String projectVersionName) {
//        ListResultModel<List<AggregationGridView>> result = new ListResultModel();
//        try {
//            if (projectVersionName == null || projectVersionName.equals("")) {
//                throw new JDSException("projectVersionName is null");
//            }
//            List<DomainInst> tempBeans = DSMFactory.getInstance().getAggregationManager().getDomainInstList(projectVersionName, true);
//            result = PageUtil.getDefaultPageList(tempBeans, AggregationGridView.class);
//        } catch (JDSException e) {
//            e.printStackTrace();
//            result = new ErrorListResultModel<>();
//            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
//            ((ErrorListResultModel) result).setErrdes(e.getMessage());
//        }
//        return result;
//    }
//


    @RequestMapping(method = RequestMethod.POST, value = "AggregationInstNav")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<DSMNavTree>> getAggregationNav(String projectVersionName) {
        List<DomainInst> tempBeans = new ArrayList<>();
        try {
            tempBeans = DSMFactory.getInstance().getAggregationManager().getDomainInstList(projectVersionName);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        TreeListResultModel<List<DSMNavTree>> result = TreePageUtil.getDefaultTreeList(tempBeans, DSMNavTree.class);
        return result;
    }
}
