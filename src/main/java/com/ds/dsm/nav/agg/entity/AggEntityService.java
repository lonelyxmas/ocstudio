package com.ds.dsm.nav.agg.entity;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.aggregation.config.entity.info.AggEntityGridView;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/manager/agg/")
@Controller
public class AggEntityService {

    @RequestMapping(method = RequestMethod.POST, value = "AggEntityList")
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf", caption = "聚合实体")
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ListResultModel<List<AggEntityGridView>> getAggEntityList(String domainId) {
        ListResultModel<List<AggEntityGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAggEntities();
                result = PageUtil.getDefaultPageList(esdClassList, AggEntityGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

}
