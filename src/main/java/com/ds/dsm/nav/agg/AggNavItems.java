package com.ds.dsm.nav.agg;

import com.ds.dsm.nav.agg.java.AggJavaService;
import com.ds.dsm.nav.agg.temp.AggTempsService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum AggNavItems implements TreeItem {
    AggregationInst("领域实例", "spafont spa-icon-c-menu", AggInstNavService.class, false, true, true),
    JavaList("Java源码", "spafont spa-icon-json-file", AggJavaService.class, false, false, false),
    AggregationTemps("模版配置", "spafont spa-icon-settingprj", AggTempsService.class, true, false, false);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    AggNavItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
