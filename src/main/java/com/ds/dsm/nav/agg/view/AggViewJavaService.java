package com.ds.dsm.nav.agg.view;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.manager.aggregation.javasrc.JavaAggSrcGridView;
import com.ds.dsm.manager.view.javasrc.JavaSrcGridView;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/dsm/manager/agg/view/")
@Controller
public class AggViewJavaService {


    @RequestMapping(method = RequestMethod.POST, value = "JavaList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-json-file", caption = "Java源码")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<JavaSrcGridView>> getJavaList(String viewInstId) {
        ListResultModel<List<JavaSrcGridView>> result = new ListResultModel();
        try {
            ViewInst bean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            if (bean != null) {
                result = PageUtil.getDefaultPageList(bean.getJavaSrcBeans(), JavaSrcGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

}
