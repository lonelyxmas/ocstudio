package com.ds.dsm.nav.agg.java;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.manager.aggregation.javasrc.JavaAggSrcGridView;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/dsm/manager/agg/")
@Controller
public class AggJavaService {


    @RequestMapping(method = RequestMethod.POST, value = "JavaList")
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(imageClass = "spafont spa-icon-json-file", caption = "Java源码")
    @CustomAnnotation( index = 5)
    @ResponseBody
    public ListResultModel<List<JavaAggSrcGridView>> getJavaList(String domainId,String projectName) {
        ListResultModel<List<JavaAggSrcGridView>> result = new ListResultModel();
        try {
            if (domainId==null || domainId.equals("")){
                if (projectName!=null && !projectName.equals("")){
                    Project project=ESDFacrory.getESDClient().getProjectByName(projectName);
                    domainId=project.getProjectName();
                }
            }
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                result = PageUtil.getDefaultPageList(bean.getJavaSrcBeans(), JavaAggSrcGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

}
