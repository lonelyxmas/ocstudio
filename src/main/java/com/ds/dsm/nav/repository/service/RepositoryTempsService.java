package com.ds.dsm.nav.repository.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.repository.RepositoryTempNavTree;
import com.ds.dsm.manager.temp.repository.RepositoryTempGrid;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/dsm/manager/repository/")
@Controller
public class RepositoryTempsService {

    @MethodChinaName(cname = "代码模板")
    @RequestMapping(method = RequestMethod.POST, value = "RepositoryCodeTemps")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<RepositoryTempNavTree>> getRepositoryCodeTemps(String id, String projectVersionName) {
        TreeListResultModel<List<RepositoryTempNavTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(RepositoryType.values()), RepositoryTempNavTree.class);
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "RepositoryTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "RepositoryTempGrid")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<RepositoryTempGrid>> getAggTempGrid(String projectVersionName, RepositoryType repositoryType) {
        ListResultModel<List<RepositoryTempGrid>> resultModel = new ListResultModel();
        RepositoryInst bean = null;
        try {
            bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null) {
                        if (javaTemp.getRepositoryType() == null || repositoryType == null || (javaTemp.getRepositoryType() != null && javaTemp.getRepositoryType().equals(repositoryType))) {
                            temps.add(javaTemp);
                        }
                    }
                }
                resultModel = PageUtil.getDefaultPageList(temps, RepositoryTempGrid.class);
            }
        } catch (JDSException e) {
            resultModel = new ErrorListResultModel<>();
            ((ErrorListResultModel) resultModel).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) resultModel).setErrdes(e.getMessage());
        }

        return resultModel;
    }


}
