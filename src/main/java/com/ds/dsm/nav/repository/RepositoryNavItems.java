package com.ds.dsm.nav.repository;

import com.ds.dsm.nav.repository.service.RepositoryEntityService;
import com.ds.dsm.nav.repository.service.RepositorySourceService;
import com.ds.dsm.nav.repository.service.RepositoryTableService;
import com.ds.dsm.nav.repository.service.RepositoryTempsService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum RepositoryNavItems implements TreeItem {
    TableList("数据库表", "spafont spa-icon-c-grid", RepositoryTableService.class, false, false, false),
    EntityList("实体信息", "spafont spa-icon-c-buttonview", RepositoryEntityService.class, false, false, false),
    JavaList("Java源码", "spafont spa-icon-json-file", RepositorySourceService.class, false, false, false),
    AggregationTemps("代码模版", "spafont spa-icon-settingprj", RepositoryTempsService.class, true, false, false);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    RepositoryNavItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
