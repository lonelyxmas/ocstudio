package com.ds.dsm.nav.repository.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.repository.entity.EntityGridView;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/manager/repository/")
@Controller
public class RepositoryEntityService {


    @RequestMapping(method = RequestMethod.POST, value = "EntityList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-buttonview", caption = "实体信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<EntityGridView>> getEntityList(String projectVersionName) {
        ListResultModel<List<EntityGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            if (bean != null) {
                for (String className : bean.getEntityNames()) {
                    ESDClass esdClass = DSMFactory.getInstance().getClassManager().getRepositoryClass(className, true);
                    if (esdClass != null) {
                        esdClassList.add(esdClass);
                    }
                }
                result = PageUtil.getDefaultPageList(esdClassList, EntityGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

}
