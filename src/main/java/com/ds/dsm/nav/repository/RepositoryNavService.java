package com.ds.dsm.nav.repository;

import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.repository.RepositoryNav;
import com.ds.dsm.nav.DSMNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/manager/repository/")
public class RepositoryNavService {
    @MethodChinaName(cname = "资源库")
    @RequestMapping(method = RequestMethod.POST, value = "RepositoryList")
    @NavGroupViewAnnotation
    @ModuleAnnotation(imageClass = "iconfont iconchucun", caption = "仓储库")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<RepositoryNav> getRepositoryList(String projectVersionName) {
        ResultModel<RepositoryNav> result = new ResultModel<RepositoryNav>();
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "RepositoryNav")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<DSMNavTree>> getRepositoryNav(String projectVersionName) {
        TreeListResultModel<List<DSMNavTree>> result = TreePageUtil.getDefaultTreeList(Arrays.asList(RepositoryNavItems.values()), DSMNavTree.class);
        return result;
    }
}
