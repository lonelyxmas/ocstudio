package com.ds.dsm.nav.repository.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.manager.repository.javasrc.JavaRepositorySrcGridView;
import com.ds.dsm.repository.entity.EntityGridView;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/manager/repository/")
@Controller
public class RepositorySourceService {



    @RequestMapping(method = RequestMethod.POST, value = "JavaList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-json-file", caption = "Java源码")
    @APIEventAnnotation( bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<JavaRepositorySrcGridView>> getJavaList(String projectVersionName) {
        ListResultModel<List<JavaRepositorySrcGridView>> result = new ListResultModel();
        try {
            RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            if (bean != null) {
                result = PageUtil.getDefaultPageList(bean.getJavaSrcBeans(), JavaRepositorySrcGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

}
