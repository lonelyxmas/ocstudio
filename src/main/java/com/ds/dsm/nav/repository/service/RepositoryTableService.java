package com.ds.dsm.nav.repository.service;

import com.ds.common.JDSException;
import com.ds.common.database.metadata.TableInfo;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.repository.db.table.TableRefGridView;
import com.ds.dsm.repository.entity.EntityGridView;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.dsm.repository.database.FDTFactory;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/manager/repository/")
@Controller
public class RepositoryTableService {

    @RequestMapping(method = RequestMethod.POST, value = "TableList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-grid", caption = "数据库表")
    @APIEventAnnotation( bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<TableRefGridView>> getTableList(String projectVersionName) {
        ListResultModel<List<TableRefGridView>> result = new ListResultModel();
        try {
            List<TableRefGridView> tableList = new ArrayList<>();
            RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            if (bean != null) {
                for (String tableName : bean.getTableNames()) {
                    TableInfo tableInfo = FDTFactory.getInstance().getTableInfoByFullName(tableName);
                    if (tableInfo != null) {
                        tableList.add(new TableRefGridView(tableInfo, projectVersionName));
                    }
                }
                result = PageUtil.getDefaultPageList(tableList);
            }
        } catch (Exception e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;

    }

}
