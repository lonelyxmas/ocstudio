package com.ds.dsm.nav;

import com.ds.dsm.nav.agg.AggInstNavItems;
import com.ds.dsm.nav.agg.AggInstService;
import com.ds.dsm.nav.agg.AggNavItems;
import com.ds.dsm.nav.agg.service.AggServiceItems;
import com.ds.dsm.nav.agg.view.AggViewInstNavService;
import com.ds.dsm.nav.agg.view.AggViewItems;
import com.ds.dsm.nav.repository.RepositoryNavItems;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TreeAnnotation(heplBar = true)
@TabsAnnotation(singleOpen = true)
public class DSMNavTree extends TreeListItem {


    @Pid
    String projectVersionName;

    @Pid
    String domainId;

    @Pid
    String viewInstId;

    @TreeItemAnnotation(customItems = DSMNavItems.class)
    public DSMNavTree(DSMNavItems dsmType, String projectVersionName) {
        this.caption = dsmType.getName();
        this.imageClass = dsmType.getImageClass();
        this.projectVersionName = projectVersionName;
        this.id = dsmType.getType();

    }

    @TreeItemAnnotation(customItems = AggNavItems.class)
    public DSMNavTree(AggNavItems dsmType, String domainId, String projectVersionName) {
        this.caption = dsmType.getName();
        this.domainId = domainId;
        this.imageClass = dsmType.getImageClass();
        this.projectVersionName = projectVersionName;
        if (domainId == null) {
            this.id = DSMType.aggregation.getType() + "_" + dsmType.getType();
        }
        this.id = domainId + "_" + dsmType.getType();

    }

    @TreeItemAnnotation(customItems = AggServiceItems.class)
    public DSMNavTree(AggServiceItems dsmType, String domainId, String projectVersionName) {
        this.caption = dsmType.getName();
        this.domainId = domainId;
        this.imageClass = dsmType.getImageClass();
        this.projectVersionName = projectVersionName;
        this.id = domainId + "_" + dsmType.getType();

    }

    @TreeItemAnnotation(customItems = RepositoryNavItems.class)
    public DSMNavTree(RepositoryNavItems dsmType, String domainId, String projectVersionName) {
        this.caption = dsmType.getName();
        this.domainId = domainId;
        this.imageClass = dsmType.getImageClass();
        this.projectVersionName = projectVersionName;
        this.id = dsmType.getType();

    }


    @TreeItemAnnotation(imageClass = "spafont spa-icon-module", iniFold = true, dynDestory = true, lazyLoad = true, bindService = AggInstService.class)
    public DSMNavTree(DomainInst domainInst, String projectVersionName) {
        this.caption = domainInst.getName();
        this.domainId = domainInst.getDomainId();
        this.projectVersionName = projectVersionName;
        this.id = domainInst.getDsmType().getType() + "_" + domainId;

    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-grid", iniFold = true, dynDestory = true, lazyLoad = true, bindService = AggViewInstNavService.class)
    public DSMNavTree(ViewInst viewInst, String projectVersionName) {
        this.caption = viewInst.getName();
        this.domainId = viewInst.getDomainId();
        this.viewInstId = viewInst.getViewInstId();
        this.projectVersionName = projectVersionName;
        this.id = viewInst.getDsmType().getType() + "_" + domainId;

    }

    @TreeItemAnnotation(customItems = AggInstNavItems.class)
    public DSMNavTree(AggInstNavItems dsmType, String domainId, String viewInstId, String projectVersionName) {
        this.caption = dsmType.getName();
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.imageClass = dsmType.getImageClass();
        this.projectVersionName = projectVersionName;
        this.id = domainId + "_" + dsmType.getType();

    }


    @TreeItemAnnotation(customItems = AggViewItems.class)
    public DSMNavTree(AggViewItems dsmType, String domainId, String viewInstId, String projectVersionName) {
        this.caption = dsmType.getName();
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.imageClass = dsmType.getImageClass();
        this.projectVersionName = projectVersionName;
        this.id = domainId + "_" + dsmType.getType();

    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
