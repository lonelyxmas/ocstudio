package com.ds.dsm.nav;

import com.ds.dsm.nav.agg.AggNavService;
import com.ds.dsm.nav.repository.RepositoryNavService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum DSMNavItems implements TreeItem {
    DSMInstNav("仓储库", "iconfont iconchucun", RepositoryNavService.class, false, false, false),
    Aggregation("领域模型", "spafont spa-icon-module", AggNavService.class, false, false, false);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    DSMNavItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
