package com.ds.dsm.manager.view;


import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.web.annotation.Required;


@FormAnnotation(stretchHeight = StretchType.last, customService = {ViewService.class})
public class ViewFormView {
    @FieldAnnotation(colWidth = "16em")
    @CustomAnnotation(uid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    public String domainId;

    @FieldAnnotation(colSpan = -1, required = true)
    @CustomAnnotation(caption = "标识")
    private String name;

    @Required
    @CustomAnnotation(caption = "命名空间")
    public String space = "dsm";
    @Required
    @CustomAnnotation(caption = "包名")
    private String packageName = "com.ds.dsm";

    @CustomAnnotation(readonly = true, hidden = true)
    private DSMType dsmType = DSMType.view;

    @FieldAnnotation( colSpan = -1, rowHeight = "50", required = true)
    @CustomAnnotation(caption = "描述")
    private String desc;


    @CustomAnnotation(hidden = true)
    private String projectVersionName;


    public ViewFormView() {

    }

    public ViewFormView(ViewInst bean) {
        this.name = bean.getName();
        this.space = bean.getSpace();
        this.viewInstId = bean.getViewInstId();
        this.desc = bean.getDesc();
        this.projectVersionName = bean.getProjectVersionName();
        this.dsmType = bean.getDsmType();
        if (desc == null || desc.equals("")) {
            this.desc = bean.getName();
        }
        this.packageName = bean.getPackageName();
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }


    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


}
