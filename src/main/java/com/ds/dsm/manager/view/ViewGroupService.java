package com.ds.dsm.manager.view;


import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempGrid;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.ViewGroupType;
import com.ds.web.annotation.ViewType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/temp/view/group/")
@MethodChinaName(cname = "视图分类", imageClass = "spafont spa-icon-conf")
public class ViewGroupService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewGroupGrid")
    @GridViewAnnotation
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<JavaTempGrid>> getViewTempGrid(ViewGroupType groupType) {
        ListResultModel<List<JavaTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> beans = new ArrayList<>();
            for (ViewType type : ViewType.getGroupView(groupType)) {
                if (type != null) {
                    List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getViewTemps(type);
                    beans.addAll(temps);
                }
            }
            resultModel = PageUtil.getDefaultPageList(beans, JavaTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadGroupType")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewTempNavTree>> loadChildViewType(ViewGroupType viewGroupType, String dsmTempId) {
        TreeListResultModel<List<ViewTempNavTree>> resultModel = new TreeListResultModel();
        List<ViewType> viewTypes = new ArrayList<>();
        for (ViewType type : ViewType.getGroupView(viewGroupType)) {
            viewTypes.add(type);
        }

        resultModel = TreePageUtil.getTreeList(viewTypes, ViewTempNavTree.class);
        return resultModel;
    }


}
