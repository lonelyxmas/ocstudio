package com.ds.dsm.manager.view;

import com.ds.dsm.manager.temp.view.ViewJavaTempService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.ViewGroupType;
import com.ds.web.annotation.ViewType;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, caption = "JAVA模板")
public class ViewTempNavTree<T extends ViewTempNavTree> extends TreeListItem<T> {

    @Pid
    ViewType viewType;
    @Pid
    String viewInstId;
    @Pid
    ViewGroupType viewGroupType;

    @TreeItemAnnotation(imageClass = "spafont spa-icon-settingprj", bindService = ViewTreeService.class)
    public ViewTempNavTree(String viewInstId) {
        this.caption = "模板分类";
        this.id = "ViewRoot";
        this.iniFold = false;
        this.viewInstId = viewInstId;
    }


    @TreeItemAnnotation(bindService = ViewGroupService.class, dynDestory = true, iniFold = true, lazyLoad = true)
    public ViewTempNavTree(ViewGroupType viewGroupType, String viewInstId) {
        this.caption = viewGroupType.getName();
        this.imageClass = viewGroupType.getImageClass();
        this.viewGroupType = viewGroupType;
        this.viewInstId = viewInstId;
        this.id = viewGroupType.getType();
    }

    @TreeItemAnnotation(bindService = ViewJavaTempService.class)
    public ViewTempNavTree(ViewType viewType, String viewInstId) {
        this.caption = viewType.getName();
        this.imageClass = viewType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + viewType.getType();
        this.iniFold = false;
        this.viewType = viewType;
        this.viewInstId = viewInstId;

    }

    public ViewGroupType getViewGroupType() {
        return viewGroupType;
    }

    public void setViewGroupType(ViewGroupType viewGroupType) {
        this.viewGroupType = viewGroupType;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public ViewType getViewType() {
        return viewType;
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }
}
