package com.ds.dsm.manager.view;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.view.ViewInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashSet;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/manager/view/action/row/")
public class ViewRowCMDAction {


    @MethodChinaName(cname = "编译")
    @RequestMapping(method = RequestMethod.POST, value = "dsmBuildView")
    @CustomAnnotation(imageClass = "xuicon spafont spa-icon-debug1")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> dsmBuildView(String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (viewInstId != null) {
                String[] dsmIds = StringUtility.split(viewInstId, ";");
                Set<DomainInst> beans = new LinkedHashSet<>();
                for (String id : dsmIds) {
                    ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(id);
                    DSMFactory.getInstance().getViewManager().buildCustomView(viewInst, getCurrChromeDriver());
                }
                ESDFacrory.getInstance().dyReload(null);
                getCurrChromeDriver().printLog("编译完成！", true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = new ErrorResultModel<>();

            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "删除")
    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "fa fa-lg fa-close")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> delete(String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            DSMFactory.getInstance().getViewManager().deleteViewInst(viewInstId, true);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    ResultModel<Boolean> reload() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ESDChrome chrome = getCurrChromeDriver();
        chrome.printLog("正在装载编译，请稍后！", true);
        try {
            ESDFacrory.getInstance().dyReload(null);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;

    }


    public ESDChrome getCurrChromeDriver() {
        ESDChrome chrome = JDSActionContext.getActionContext().Par("$currChromeDriver", ESDChrome.class);
        return chrome;
    }


}

