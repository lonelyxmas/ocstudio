package com.ds.dsm.manager.view;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.temp.view.ViewTempGrid;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.dsm.view.ViewManager;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.ViewType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/temp/view/nav/")
@MethodChinaName(cname = "聚合模型", imageClass = "spafont spa-icon-conf")

public class ViewTreeService {


    @MethodChinaName(cname = "代码工厂")
    @RequestMapping(method = RequestMethod.POST, value = "ViewTemps")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewTempNavTree>> getViewTemps(String id, String viewInstId) {
        TreeListResultModel<List<ViewTempNavTree>> result = new TreeListResultModel<>();
        ViewType[] viewTypes = ViewType.values();
        result = TreePageUtil.getTreeList(Arrays.asList(viewTypes), ViewTempNavTree.class);
        return result;

    }

    @MethodChinaName(cname = "All模板")
    @RequestMapping(method = RequestMethod.POST, value = "AllViewTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "All模板")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<ViewTempGrid>> getAllViewTempGrid(String viewInstId) {
        ListResultModel<List<ViewTempGrid>> result = new ListResultModel();
        try {
            ViewManager viewManager = DSMFactory.getInstance().getViewManager();
            Set<String> javaTempIds = viewManager.getViewInstById(viewInstId).getJavaTempIds();
            List<JavaTemp> temps = new ArrayList<>();
            for (String javaid : javaTempIds) {
                JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                temps.add(javaTemp);
            }

            result = PageUtil.getDefaultPageList(temps, ViewTempGrid.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }


}
