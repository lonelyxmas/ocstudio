package com.ds.dsm.manager.view;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/manager/view/")
@MethodChinaName(cname = "聚合模型", imageClass = "spafont spa-icon-conf")

public class ViewService {

    public ViewService() {

    }

    @MethodChinaName(cname = "更新领域信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateView")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateView(@RequestBody ViewFormView tempInfo) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewInst bean = DSMFactory.getInstance().getViewManager().getViewInstById(tempInfo.getViewInstId());
            bean.setDesc(tempInfo.getDesc());
            bean.setName(tempInfo.getName());
            bean.setSpace(tempInfo.getSpace());
            bean.setPackageName(tempInfo.getPackageName());
            bean.setProjectVersionName(tempInfo.getProjectVersionName());
            bean.setDsmType(tempInfo.getDsmType());
            DSMFactory.getInstance().getViewManager().updateViewInst(bean, true);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "刪除领域")
    @RequestMapping(method = RequestMethod.POST, value = "delView")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> delView(String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (viewInstId == null) {
                throw new JDSException("viewInstId is bull");
            }
            DSMFactory.getInstance().getViewManager().deleteViewInst(viewInstId, true);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }

//
//    @RequestMapping(value = {"CreateView"}, method = {RequestMethod.GET, RequestMethod.POST})
//    @FormViewAnnotation
//    @ModuleAnnotation( caption = "新建领域模型")
//    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
//    @DialogAnnotation(height = "260", width = "380")
//    @ResponseBody
//    public ResultModel<ViewCreateView> createView(String projectVersionName) {
//        ResultModel<ViewCreateView> result = new ResultModel<ViewCreateView>();
//        try {
//            ViewInst dsmInst = DSMFactory.getInstance().getViewManager().createDefaultView(projectVersionName, projectVersionName);
//            result.setData(new ViewCreateView(dsmInst));
//        } catch (JDSException e) {
//            e.printStackTrace();
//            result = new ErrorResultModel<>();
//            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
//            ((ErrorResultModel) result).setErrdes(e.getMessage());
//        }
//        return result;
//    }


    @RequestMapping(method = RequestMethod.POST, value = "ViewNav")
    @NavGroupViewAnnotation()
    @ModuleAnnotation( caption = "模板详细信息")
    @DialogAnnotation(width = "900")
    @APIEventAnnotation(bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<ViewNav> getViewNav(String viewInstId) {
        ResultModel<ViewNav> result = new ResultModel<ViewNav>();
        return result;
    }


}
