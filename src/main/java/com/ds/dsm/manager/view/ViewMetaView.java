package com.ds.dsm.manager.view;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.view.javasrc.JavaSrcGridView;
import com.ds.dsm.view.entity.ViewEntityGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/dsm/manager/view/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "视图管理", imageClass = "spafont spa-icon-conf")
public class ViewMetaView {

    @CustomAnnotation(uid = true, hidden = true)
    private String viewInstId;


    @RequestMapping(method = RequestMethod.POST, value = "ViewList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-webapp", caption = "视图列表")
    @CustomAnnotation( index = 0)
    @ResponseBody
    public ListResultModel<List<ViewEntityGridView>> getViewList(String viewInstId) {
        ListResultModel<List<ViewEntityGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            ViewInst bean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            if (bean != null) {
                esdClassList = bean.getViewProxyClasses();
                result = PageUtil.getDefaultPageList(esdClassList, ViewEntityGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "ESDClassList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-webapp", caption = "实体列表")
    @CustomAnnotation( index = 1)
    @ResponseBody
    public ListResultModel<List<ViewEntityGridView>> getESDClassList(String viewInstId) {
        ListResultModel<List<ViewEntityGridView>> result = new ListResultModel();
        try {
            ViewInst bean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            List<ESDClass> esdClassList = new ArrayList<>();
            if (bean != null) {
                esdClassList = bean.getAggList();
                result = PageUtil.getDefaultPageList(esdClassList, ViewEntityGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "JavaList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-json-file", caption = "Java源码")
    @CustomAnnotation( index = 2)
    @ResponseBody
    public ListResultModel<List<JavaSrcGridView>> getJavaList(String viewInstId) {
        ListResultModel<List<JavaSrcGridView>> result = new ListResultModel();
        try {
            ViewInst bean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            if (bean != null) {
                result = PageUtil.getDefaultPageList(bean.getJavaSrcBeans(), JavaSrcGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "代码工厂")
    @RequestMapping(method = RequestMethod.POST, value = "ViewTemps")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", dynLoad = true, caption = "代码工厂")
    @CustomAnnotation( index = 3)
    @ResponseBody
    public TreeListResultModel<List<ViewTempNavTree>> getViewTemps(String id, String viewInstId) {
        TreeListResultModel<List<ViewTempNavTree>> resultModel = new TreeListResultModel<List<ViewTempNavTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(viewInstId), ViewTempNavTree.class);
        return resultModel;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}
