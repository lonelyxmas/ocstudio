package com.ds.dsm.manager.view;


import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;
import com.ds.web.annotation.Required;

@PageBar
@RowHead(rowNumbered = false,
        selMode = SelModeType.none, rowHandlerWidth = "6em")
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left,menuClass = {ViewRowCMDAction.class})
@GridAnnotation(
        customMenu = {GridMenu.Reload, GridMenu.Add},
        customService = {ViewService.class},
        event = {CustomGridEvent.editor})
public class ViewGridView {
    @FieldAnnotation( colWidth = "16em")
    @CustomAnnotation(uid = true, hidden = true)
    String viewInstId;

    @FieldAnnotation( colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "名称")
    private String desc;

    @Required
    @CustomAnnotation(caption = "标识")
    private String name;
    @Required
    @CustomAnnotation(caption = "命名空间")
    public String space = "dsm";
    @Required
    @CustomAnnotation(caption = "包名")
    private String packageName = "com.ds.dsm";

    @CustomAnnotation(hidden = true, pid = true)
    public String projectVersionName;


    @CustomAnnotation(hidden = true, pid = true)
    public DSMType dsmType = DSMType.view;


    public ViewGridView() {

    }

    public ViewGridView(ViewInst bean) {
        this.name = bean.getName();
        this.space = bean.getSpace();
        this.viewInstId = bean.getViewInstId();
        this.desc = bean.getDesc();
        if (desc == null || desc.equals("")) {
            this.desc = bean.getName();
        }
        this.projectVersionName = bean.getProjectVersionName();
        this.packageName = bean.getPackageName();
        this.dsmType = bean.getDsmType();
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }


    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }
}
