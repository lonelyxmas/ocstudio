package com.ds.dsm.manager.view.javasrc;


import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.view.ViewInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = {"/dsm/manager/view/java/"})
public class JavaSrcService {

    @RequestMapping(value = "delete")
    @CustomAnnotation(imageClass = "xuicon xui-icon-minus", index = 4, caption = "删除")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = CustomCallBack.Reload)
    public @ResponseBody
    ResultModel<Boolean> delete(String viewInstId, String javaClassName, String javaTempId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {

            DSMFactory dsmFactory = DSMFactory.getInstance();
            ViewInst viewInst = dsmFactory.getViewManager().getViewInstById(viewInstId);
            String[] javaClassNames = StringUtility.split(javaClassName, ";");
            for (String className : javaClassNames) {
                JavaSrcBean javaSrcBean = viewInst.getJavaSrcByClassName(className);
                if (javaSrcBean != null) {
                    dsmFactory.getTempManager().deleteJavaFile(javaSrcBean);
                    viewInst.removeJavaSrc(javaSrcBean);
                }
            }
            dsmFactory.getViewManager().updateViewInst(viewInst, true);

        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


}
