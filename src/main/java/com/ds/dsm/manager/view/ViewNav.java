package com.ds.dsm.manager.view;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/manager/view/")
@BottomBarMenu(menuClass = BuildViewMenu.class)
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save}, customService = {ViewService.class})
@MethodChinaName(cname = "视图信息", imageClass = "spafont spa-icon-c-gallery")
public class ViewNav {


    @CustomAnnotation(pid = true, hidden = true)
    public String viewInstId;


    @CustomAnnotation(uid = true, hidden = true)
    public String projectVersionName;


    @RequestMapping(method = RequestMethod.POST, value = "ViewInfo")
    @FormViewAnnotation()
    @UIAnnotation(height = "150")
    @ModuleAnnotation(caption = "实体信息", dock = Dock.top)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<ViewFormView> getViewInfo(String viewInstId, String projectVersionName, String className) {
        ResultModel<ViewFormView> result = new ResultModel<ViewFormView>();
        ViewInst viewInst = null;
        try {
            viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            ViewFormView formView = new ViewFormView(viewInst);
            result.setData(formView);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "ViewMetaView")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "关联信息", imageClass = "spafont spa-icon-c-databinder")
    @ResponseBody
    public ResultModel<ViewMetaView> getViewMetaView(String viewInstId, String projectVersionName, String className) {
        ResultModel<ViewMetaView> result = new ResultModel<ViewMetaView>();
        return result;

    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
