package com.ds.dsm.manager.repository;


import com.ds.common.JDSException;
import com.ds.common.database.dao.DAOException;
import com.ds.common.database.metadata.TableInfo;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.repository.javasrc.JavaRepositorySrcGridView;
import com.ds.dsm.repository.db.table.TableRefGridView;
import com.ds.dsm.repository.entity.EntityGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.dsm.repository.database.FDTFactory;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/dsm/manager/repository/meta/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "数据库实例", imageClass = "spafont spa-icon-conf")
public class RepositoryMetaView {


    @CustomAnnotation(hidden = true, pid = true)
    public String projectVersionName;

    @RequestMapping(method = RequestMethod.POST, value = "TableList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-grid", caption = "数据库表")
    @CustomAnnotation(index = 0)
    @ResponseBody
    public ListResultModel<List<TableRefGridView>> getTableList(String projectVersionName) {
        ListResultModel<List<TableRefGridView>> result = new ListResultModel();
        try {
            List<TableRefGridView> tableList = new ArrayList<>();
            RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            if (bean != null) {
                for (String tableName : bean.getTableNames()) {
                    TableInfo tableInfo =null;
                    try {
                        tableInfo = FDTFactory.getInstance().getTableInfoByFullName(tableName);
                    }catch (DAOException d){

                    }

                    if (tableInfo != null) {
                        tableList.add(new TableRefGridView(tableInfo, projectVersionName));
                    }
                }
                result = PageUtil.getDefaultPageList(tableList);
            }
        } catch (Exception e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;

    }

    @RequestMapping(method = RequestMethod.POST, value = "EntityList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-buttonview", caption = "实体信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @CustomAnnotation(index = 1)
    @ResponseBody
    public ListResultModel<List<EntityGridView>> getEntityList(String projectVersionName) {
        ListResultModel<List<EntityGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            if (bean != null) {
                for (String className : bean.getEntityNames()) {
                    ESDClass esdClass = DSMFactory.getInstance().getClassManager().getRepositoryClass(className, true);
                    if (esdClass != null) {
                        esdClassList.add(esdClass);
                    }
                }
                result = PageUtil.getDefaultPageList(esdClassList, EntityGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

//
//    @RequestMapping(method = RequestMethod.POST, value = "UrlList")
//    @GridViewAnnotation
//    @ModuleAnnotation(imageClass = "spafont spa-icon-alignm", caption = "外部服务")
//    @CustomAnnotation(index = 2)
//    @ResponseBody
//    public ListResultModel<List<TableRefGridView>> getUrlList(String projectVersionName) {
//        ListResultModel<List<TableRefGridView>> result = new ListResultModel();
//        try {
//            List<TableRefGridView> tableList = new ArrayList<>();
//            RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
//            if (bean != null) {
//                for (String tableName : bean.getTableNames()) {
//                    TableInfo tableInfo = FDTFactory.getInstance().getTableInfoByFullName(tableName);
//                    if (tableInfo != null) {
//                        tableList.add(new TableRefGridView(tableInfo, projectVersionName));
//                    }
//                }
//                result = PageUtil.getDefaultPageList(tableList);
//            }
//        } catch (Exception e) {
//            result = new ErrorListResultModel<>();
//
//            ((ErrorListResultModel) result).setErrdes(e.getMessage());
//        }
//        return result;
//
//    }

    @RequestMapping(method = RequestMethod.POST, value = "JavaList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-json-file", caption = "Java源码")
    @CustomAnnotation(index = 3)
    @ResponseBody
    public ListResultModel<List<JavaRepositorySrcGridView>> getJavaList(String projectVersionName) {
        ListResultModel<List<JavaRepositorySrcGridView>> result = new ListResultModel();
        try {
            RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            if (bean != null) {
                result = PageUtil.getDefaultPageList(bean.getJavaSrcBeans(), JavaRepositorySrcGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "代码模板")
    @RequestMapping(method = RequestMethod.POST, value = "RepositoryCodeTemps")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", dynLoad = true, caption = "代码模板")
    @CustomAnnotation(index = 4)
    @ResponseBody
    public TreeListResultModel<List<RepositoryTempNavTree>> getRepositoryCodeTemps(String id, String projectVersionName) {
        TreeListResultModel<List<RepositoryTempNavTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(projectVersionName), RepositoryTempNavTree.class);
        return result;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

}
