package com.ds.dsm.manager.repository;

import com.ds.common.database.metadata.ProviderConfig;
import com.ds.common.database.metadata.TableInfo;
import com.ds.dsm.manager.repository.service.ProviderTreeService;
import com.ds.dsm.manager.repository.service.TableTreeService;
import com.ds.dsm.repository.db.service.DSMTableService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox,
        lazyLoad = true,
        customService = DSMTableService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class SelectTableTopTree extends TreeListItem {

    @Pid
    String tablename;
    @Pid
    String configKey;
    @Pid
    String projectVersionName;


    @TreeItemAnnotation(imageClass = "spafont spa-icon-coin", iniFold = false, bindService = ProviderTreeService.class)
    public SelectTableTopTree(String projectVersionName) {
        this.caption = "数据库";
        this.id = "all";
        this.projectVersionName = projectVersionName;

    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-grid", iniFold = false)
    public SelectTableTopTree(TableInfo tableInfo, String projectVersionName) {
        this.caption = tableInfo.getName() + "(" + tableInfo.getCnname() + ")";
        this.tablename = tableInfo.getName();
        this.configKey = tableInfo.getConfigKey();
        this.projectVersionName = projectVersionName;
        this.id = tableInfo.getName();
    }

    @TreeItemAnnotation(imageClass = "iconfont iconchucun", iniFold = false, bindService = TableTreeService.class)
    public SelectTableTopTree(ProviderConfig config, String projectVersionName) {
        this.caption = config.getConfigName() == null ? config.getConfigKey() : config.getConfigName();
        this.configKey = config.getConfigKey();
        this.projectVersionName = projectVersionName;
        this.id = config.getConfigKey() == null ? config.getConfigName() : config.getConfigKey();
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
