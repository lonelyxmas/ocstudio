package com.ds.dsm.manager.repository;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.temp.repository.RepositoryTempGrid;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/temp/repository/nav/")
@MethodChinaName(cname = "聚合模型", imageClass = "spafont spa-icon-conf")

public class RepositoryTreeService {


    @MethodChinaName(cname = "代码工厂")
    @RequestMapping(method = RequestMethod.POST, value = "RepositoryTemps")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<RepositoryTempNavTree>> getRepositoryTemps(String id, String projectVersionName) {
        TreeListResultModel<List<RepositoryTempNavTree>> result = new TreeListResultModel<>();
        RepositoryType[] repositoryTypes = RepositoryType.values();
        result = TreePageUtil.getTreeList(Arrays.asList(repositoryTypes), RepositoryTempNavTree.class);
        return result;

    }

    @MethodChinaName(cname = "All模板")
    @RequestMapping(method = RequestMethod.POST, value = "AllRepositoryTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "All模板")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<RepositoryTempGrid>> getAllRepositoryTempGrid(String projectVersionName) {
        ListResultModel<List<RepositoryTempGrid>> result = new ListResultModel();
        try {

            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            Set<String> javaTempIds = repositoryInst.getJavaTempIds();
            List<JavaTemp> temps = new ArrayList<>();
            for (String javaid : javaTempIds) {
                JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                temps.add(javaTemp);
            }

            result = PageUtil.getDefaultPageList(temps, RepositoryTempGrid.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }


}
