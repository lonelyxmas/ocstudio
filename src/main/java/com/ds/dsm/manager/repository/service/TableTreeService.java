package com.ds.dsm.manager.repository.service;

import com.ds.common.database.metadata.TableInfo;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.repository.SelectTableTopTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/repository/")
@MethodChinaName(cname = "选择数据库表", imageClass = "spafont spa-icon-conf")
public class TableTreeService {

    @MethodChinaName(cname = "获取所有数据源")
    @RequestMapping(value = {"getAllTables"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public @ResponseBody
    TreeListResultModel<List<SelectTableTopTree>> getAllTables(String configKey) {
        TreeListResultModel listResultModel = new TreeListResultModel();
        try {
            List<TableInfo> tableInfos = ESDFacrory.getESDClient().getDbFactory(configKey).getTableInfos(null);
            listResultModel = TreePageUtil.getTreeList(tableInfos, SelectTableTopTree.class);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return listResultModel;
    }
}
