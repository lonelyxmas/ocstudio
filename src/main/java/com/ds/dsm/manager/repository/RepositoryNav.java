package com.ds.dsm.manager.repository;


import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.repository.db.action.DBRowCMDAction;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/manager/repository/group/")
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {RepositoryService.class})
@BottomBarMenu(menuClass = DBRowCMDAction.class)
@MethodChinaName(cname = "数据库模型", imageClass = "spafont spa-icon-conf")
public class RepositoryNav {


    @CustomAnnotation(hidden = true, pid = true)
    public String projectVersionName;


    @MethodChinaName(cname = "模型信息")
    @RequestMapping(method = RequestMethod.POST, value = "RepositoryInfo")
    @FormViewAnnotation
    @UIAnnotation(dock = Dock.top, height = "160")
    @ModuleAnnotation(caption = "模型信息")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<RepositoryInfoView> getRepositoryInfo(String projectVersionName) {
        ResultModel<RepositoryInfoView> result = new ResultModel<RepositoryInfoView>();
        RepositoryInst bean = null;
        try {
            bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            RepositoryInfoView tempView = new RepositoryInfoView(bean);
            result.setData(tempView);
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return result;
    }


    @MethodChinaName(cname = "详细信息")
    @RequestMapping(method = RequestMethod.POST, value = "RepositoryMetaView")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "详细信息")
    @ResponseBody
    public ResultModel<RepositoryMetaView> getRepositoryMetaView(String projectVersionName) {
        ResultModel<RepositoryMetaView> result = new ResultModel<RepositoryMetaView>();
        return result;

    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
