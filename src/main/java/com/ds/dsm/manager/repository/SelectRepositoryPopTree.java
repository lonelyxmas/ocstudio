package com.ds.dsm.manager.repository;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

import java.util.HashMap;
import java.util.List;

@TabsAnnotation(singleOpen = true)
@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, caption = "JAVA模板", bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class SelectRepositoryPopTree<T extends SelectRepositoryPopTree> extends TreeListItem {


    @Pid
    DSMType dsmType;
    @Pid
    RepositoryType repositoryType;

    @Pid
    String projectVersionName;

    @Pid
    String javaTempId;



    public SelectRepositoryPopTree(RepositoryType repositoryType, String className, String projectVersionName, boolean hasTemp) throws JDSException {
        this.caption = repositoryType.getName();
        this.imageClass = repositoryType.getImageClass();
        this.euClassName = className;
        this.id = repositoryType.getType();
        this.iniFold = false;
        this.tagVar = new HashMap<>();
        tagVar.put("dsmType", DSMType.repository.getType());
        tagVar.put("repositoryType", repositoryType.getType());
        tagVar.put("projectVersionName", projectVersionName);
        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(repositoryType);
            for (JavaTemp javaTemp : javaTemps) {
                this.addChild(new SelectRepositoryPopTree(javaTemp, repositoryType, className, projectVersionName));
            }
        }


    }

    public SelectRepositoryPopTree(JavaTemp javaTemp, RepositoryType repositoryType, String className, String projectVersionName) {
        this.caption = javaTemp.getName();
        this.euClassName = className;
        this.imageClass = "xui-icon-code";
        this.id = javaTemp.getJavaTempId();
        this.tagVar = new HashMap<>();
        tagVar.put("dsmType", DSMType.repository.getType());
        tagVar.put("repositoryType", repositoryType.getType());
        tagVar.put("projectVersionName", projectVersionName);
        tagVar.put("javaTempId", javaTemp.getJavaTempId());
    }

    public SelectRepositoryPopTree(JavaTemp javaTemp, DSMType dsmType, String className, String projectVersionName) {
        this.caption = javaTemp.getName();
        this.imageClass = "xui-icon-code";
        this.euClassName = className;
        this.id = javaTemp.getJavaTempId();
        this.tagVar = new HashMap<>();
        tagVar.put("dsmType", dsmType.getType());
        tagVar.put("projectVersionName", projectVersionName);
        tagVar.put("javaTempId", javaTemp.getJavaTempId());
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public RepositoryType getRepositoryType() {
        return repositoryType;
    }

    public void setRepositoryType(RepositoryType repositoryType) {
        this.repositoryType = repositoryType;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }
}
