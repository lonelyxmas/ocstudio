package com.ds.dsm.manager.repository.service;

import com.ds.common.JDSException;
import com.ds.common.database.metadata.ProviderConfig;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.repository.SelectTableTopTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/repository/")
@MethodChinaName(cname = "选择数据库表", imageClass = "spafont spa-icon-conf")
public class ProviderTreeService {

    @MethodChinaName(cname = "获取所有数据源")
    @RequestMapping(value = {"getAllProvider"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    TreeListResultModel<List<SelectTableTopTree>> getAllProvider(String projectVersionName) {
        TreeListResultModel listResultModel = new TreeListResultModel();
        try {
            List<ProviderConfig> configs = ESDFacrory.getESDClient().getAllDbConfig();
            listResultModel = TreePageUtil.getTreeList(configs, SelectTableTopTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return listResultModel;
    }
}
