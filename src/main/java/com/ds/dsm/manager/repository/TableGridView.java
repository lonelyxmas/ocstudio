package com.ds.dsm.manager.repository;

import com.ds.common.database.metadata.TableInfo;
import com.ds.dsm.repository.db.service.DSMTableService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.web.annotation.Required;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {DSMTableService.class}, event = CustomGridEvent.editor)
public class TableGridView {

    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;


    @CustomAnnotation(hidden = true, pid = true)
    public String projectVersionName;


    @CustomAnnotation(uid = true, hidden = true)
    public String tablename;

    @Required
    @CustomAnnotation(caption = "表名")
    public String name;

    @Required
    @CustomAnnotation(caption = "注解")
    public String cnname;

    @Required
    @CustomAnnotation(caption = "数据库标识")
    public String configKey;

    @Required
    @CustomAnnotation(caption = "主键")
    private String pkName;

    @CustomAnnotation(caption = "连接串", hidden = true)
    private String url;

    public TableGridView(TableInfo info) {
        this.name = info.getName();
        this.cnname = info.getCnname();
        this.pkName = info.getPkName();
        this.configKey = info.getConfigKey();
        this.url = info.getUrl();
        this.tablename = info.getName();
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getPkName() {
        return pkName;
    }

    public void setPkName(String pkName) {
        this.pkName = pkName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

}
