package com.ds.dsm.manager.repository;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.repository.RepositoryInst;


@FormAnnotation(customService = {RepositoryService.class})
public class RepositoryInfoView {

    @FieldAnnotation( colSpan = -1, required = true)
    @CustomAnnotation(caption = "名称")
    private String desc;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "标识")
    private String name;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "命名空间")
    public String space = "dsm";

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "数据库配置KEY")
    private String schema = "fdt";


    @FieldAnnotation( required = true)
    @CustomAnnotation(caption = "包名")
    private String packageName = "com.ds.dsm";

    @CustomAnnotation(caption = "基础路径", hidden = true)
    public String basepath;

    @CustomAnnotation(hidden = true)
    public String projectVersionName;

    @CustomAnnotation(hidden = true)
    public DSMType dsmType;


    public RepositoryInfoView() {

    }

    public RepositoryInfoView(RepositoryInst bean) {
        this.name = bean.getName();
        this.basepath = bean.getBasepath();
        this.space = bean.getSpace();

        this.desc = bean.getDesc();
        if (desc == null || desc.equals("")) {
            this.desc = bean.getName();
        }
        this.projectVersionName = bean.getProjectVersionName();
        this.packageName = bean.getPackageName();
        this.schema = bean.getSchema();
        this.dsmType = bean.getDsmType();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getBasepath() {
        return basepath;
    }

    public void setBasepath(String basepath) {
        this.basepath = basepath;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
