package com.ds.dsm.manager.repository;

import com.ds.common.JDSException;
import com.ds.dsm.manager.temp.repository.RepositoryJavaTempService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, caption = "JAVA模板")
public class RepositoryTempNavTree<T extends RepositoryTempNavTree> extends TreeListItem<T> {

    @Pid
    RepositoryType repositoryType;
    @Pid
    String projectVersionName;

    @TreeItemAnnotation(imageClass = "spafont spa-icon-settingprj", bindService = RepositoryTreeService.class)
    public RepositoryTempNavTree(String projectVersionName) throws JDSException {
        this.caption = "模板分类";
        this.id = "RepositoryRoot";
        this.iniFold = false;
        this.projectVersionName = projectVersionName;
    }

    @TreeItemAnnotation(bindService = RepositoryJavaTempService.class)
    public RepositoryTempNavTree(RepositoryType repositoryType, String projectVersionName) throws JDSException {
        this.caption = repositoryType.getName();
        this.imageClass = repositoryType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + repositoryType.getType();
        this.iniFold = false;
        this.repositoryType = repositoryType;
        this.projectVersionName = projectVersionName;

    }

    public RepositoryType getRepositoryType() {
        return repositoryType;
    }

    public void setRepositoryType(RepositoryType repositoryType) {
        this.repositoryType = repositoryType;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
