package com.ds.dsm.manager.repository;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/manager/repository/")
@MethodChinaName(cname = "资源库模型", imageClass = "spafont spa-icon-c-grid")

public class RepositoryService {

    public RepositoryService() {

    }

    @MethodChinaName(cname = "更新领域信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateRepository")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateRepository(@RequestBody RepositoryInfoView tempInfo) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(tempInfo.getProjectVersionName());
            bean.setBasepath(tempInfo.getBasepath());
            bean.setDesc(tempInfo.getDesc());
            bean.setName(tempInfo.getName());
            bean.setDsmType(tempInfo.getDsmType());
            bean.setProjectVersionName(tempInfo.getProjectVersionName());
            bean.setSchema(tempInfo.getSchema());
            bean.setSpace(tempInfo.getSpace());
            bean.setPackageName(tempInfo.getPackageName());
            DSMFactory.getInstance().getRepositoryManager().updateRepositoryInst(bean, true);

        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "模型信息")
    @RequestMapping(method = RequestMethod.POST, value = "RepositoryInfo")
    @FormViewAnnotation
    @ModuleAnnotation(caption = "模型信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<RepositoryInfoView> getRepositoryInfo(String projectVersionName) {
        ResultModel<RepositoryInfoView> result = new ResultModel<RepositoryInfoView>();
        try {
            if (projectVersionName == null) {
                RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                RepositoryInfoView tempView = new RepositoryInfoView(bean);
                result.setData(tempView);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
