package com.ds.dsm.manager.build;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.web.annotation.Entity;
import com.ds.web.annotation.Required;

import java.util.LinkedHashSet;
import java.util.Set;

public class BuildConfig {


    @Required
    @CustomAnnotation(caption = "领域模型名称")
    private String name;

    @Required
    @CustomAnnotation(caption = "包名")
    private String packageName = "com.ds.test";

    @Required
    @CustomAnnotation(caption = "访问路径")
    public String space = "dsm";

    @CustomAnnotation(caption = "库表")
    public Set<String> tableNames = new LinkedHashSet<>();
    @CustomAnnotation(caption = "关联关系")
    public Set<String> refIds = new LinkedHashSet<>();

    @CustomAnnotation(caption = "关联关系")
    public Set<String> dsmRefIds = new LinkedHashSet<>();

    @CustomAnnotation(caption = "更新模板")
    public Set<String> javaTempIds = new LinkedHashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }

    public Set<String> getTableNames() {
        return tableNames;
    }

    public void setTableNames(Set<String> tableNames) {
        this.tableNames = tableNames;
    }

    public Set<String> getRefIds() {
        return refIds;
    }

    public void setRefIds(Set<String> refIds) {
        this.refIds = refIds;
    }

    public Set<String> getDsmRefIds() {
        return dsmRefIds;
    }

    public void setDsmRefIds(Set<String> dsmRefIds) {
        this.dsmRefIds = dsmRefIds;
    }

    public Set<String> getJavaTempIds() {
        return javaTempIds;
    }

    public void setJavaTempIds(Set<String> javaTempIds) {
        this.javaTempIds = javaTempIds;
    }
}
