package com.ds.dsm.manager.aggregation.javasrc;


import com.ds.esd.custom.annotation.ComboDateAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridColItemAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

@PageBar
@RowHead(selMode = SelModeType.multibycheckbox)
@GridAnnotation(
        customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete},
        customService = JavaAggSrcService.class
)
public class JavaAggSrcGridView {

    @Pid
    public DSMType dsmType = DSMType.view;
    @Pid
    String dsmId;
    @Pid
    String sourceClassName;
    @CustomAnnotation(caption = "文件名")
    String name;
    @Pid
    String methodName;

    @CustomAnnotation(caption = "类名", uid = true)
    String javaClassName;


    @CustomAnnotation(caption = "路径")
    String filePath;

    @CustomAnnotation(caption = "生成日期")
    @GridColItemAnnotation(inputType = ComboInputType.datetime)
    Long createDate;


    public JavaAggSrcGridView() {

    }

    public JavaAggSrcGridView(JavaSrcBean bean) {
        this.name = bean.getName();
        this.filePath = bean.getPath();
        this.sourceClassName = bean.getSourceClassName();
        this.javaClassName = bean.getClassName();
        this.methodName = bean.getMethodName();
        this.dsmId = bean.getDsmId();
        this.dsmType = bean.getDsmType();
        this.createDate = bean.getDate() == null ? System.currentTimeMillis() : bean.getDate();
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public void setJavaClassName(String javaClassName) {
        this.javaClassName = javaClassName;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public String getDsmId() {
        return dsmId;
    }

    public void setDsmId(String dsmId) {
        this.dsmId = dsmId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }
}
