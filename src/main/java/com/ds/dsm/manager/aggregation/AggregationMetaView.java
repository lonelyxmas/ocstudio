package com.ds.dsm.manager.aggregation;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.aggregation.config.entity.info.AggEntityGridView;
import com.ds.dsm.aggregation.config.root.AggRootGridView;
import com.ds.dsm.aggregation.config.source.AggSoruceGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/manager/agg/meta/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "实例管理", imageClass = "spafont spa-icon-conf")
public class AggregationMetaView {

    @CustomAnnotation(uid = true, hidden = true)
    private String domainId;


    @RequestMapping(method = RequestMethod.POST, value = "AggSoruceList")
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-gallery", caption = "实体源")
    @CustomAnnotation(index = 0)
    @ResponseBody
    public ListResultModel<List<AggSoruceGridView>> getSourceList(String domainId) {
        ListResultModel<List<AggSoruceGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAllServiceClass();
                result = PageUtil.getDefaultPageList(esdClassList, AggSoruceGridView.class);
            }

        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AggRootList")
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-gallery", caption = "聚合根")
    @CustomAnnotation(index = 1)
    @ResponseBody
    public ListResultModel<List<AggRootGridView>> getAggRootList(String domainId) {
        ListResultModel<List<AggRootGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAggRoots();
                result = PageUtil.getDefaultPageList(esdClassList, AggRootGridView.class);
            }

        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "AggEntityList")
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf", caption = "聚合实体")
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ListResultModel<List<AggEntityGridView>> getAggEntityList(String domainId) {
        ListResultModel<List<AggEntityGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAggEntities();
                result = PageUtil.getDefaultPageList(esdClassList, AggEntityGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


//    @RequestMapping(method = RequestMethod.POST, value = "AggMenuList")
//    @GridViewAnnotation
//    @APIEventAnnotation(autoRun = true)
//    @ModuleAnnotation(imageClass = "spafont spa-icon-c-menu", caption = "菜单动作")
//    @CustomAnnotation( index = 3)
//    @ResponseBody
//    public ListResultModel<List<AggMenuGridView>> getAggMenuList(String domainId) {
//        ListResultModel<List<AggMenuGridView>> result = new ListResultModel();
//        try {
//            List<ESDClass> esdClassList = new ArrayList<>();
//            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
//            if (bean != null) {
//                esdClassList = bean.getAggMenus();
//                result = PageUtil.getDefaultPageList(esdClassList, AggMenuGridView.class);
//            }
//        } catch (JDSException e) {
//            result = new ErrorListResultModel<>();
//            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
//            ((ErrorListResultModel) result).setErrdes(e.getMessage());
//        }
//        return result;
//    }
//
//    @RequestMapping(method = RequestMethod.POST, value = "AggAPIList")
//    @GridViewAnnotation
//    @APIEventAnnotation(autoRun = true)
//    @ModuleAnnotation(imageClass = "spafont spa-icon-c-webapi", caption = "服务接口")
//    @CustomAnnotation( index = 3)
//    @ResponseBody
//    public ListResultModel<List<AggAPIGridView>> getAggAPIList(String domainId) {
//        ListResultModel<List<AggAPIGridView>> result = new ListResultModel();
//        try {
//            List<ESDClass> esdClassList = new ArrayList<>();
//            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
//            if (bean != null) {
//                esdClassList = bean.getAggAPIs();
//                result = PageUtil.getDefaultPageList(esdClassList, AggAPIGridView.class);
//            }
//        } catch (JDSException e) {
//            result = new ErrorListResultModel<>();
//            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
//            ((ErrorListResultModel) result).setErrdes(e.getMessage());
//        }
//        return result;
//    }

//    @RequestMapping(method = RequestMethod.POST, value = "AggDomainList")
//    @GridViewAnnotation
//    @APIEventAnnotation(autoRun = true)
//    @ModuleAnnotation(imageClass = "spafont spa-icon-conf", caption = "通用域")
//    @CustomAnnotation( index = 4)
//    @ResponseBody
//    public ListResultModel<List<AggDomainGridView>> getAggDomainList(String domainId) {
//        ListResultModel<List<AggDomainGridView>> result = new ListResultModel();
//        try {
//            List<ESDClass> esdClassList = new ArrayList<>();
//            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
//            if (bean != null) {
//                esdClassList = bean.getAggDomains();
//                result = PageUtil.getDefaultPageList(esdClassList, AggDomainGridView.class);
//            }
//        } catch (JDSException e) {
//            result = new ErrorListResultModel<>();
//            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
//            ((ErrorListResultModel) result).setErrdes(e.getMessage());
//        }
//        return result;
//    }

//    @RequestMapping(method = RequestMethod.POST, value = "JavaList")
//    @GridViewAnnotation
//    @ModuleAnnotation(imageClass = "spafont spa-icon-json-file", caption = "Java源码")
//    @CustomAnnotation( index = 5)
//    @ResponseBody
//    public ListResultModel<List<JavaAggSrcGridView>> getJavaList(String domainId) {
//        ListResultModel<List<JavaAggSrcGridView>> result = new ListResultModel();
//        try {
//            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
//            if (bean != null) {
//                result = PageUtil.getDefaultPageList(bean.getJavaSrcBeans(), JavaAggSrcGridView.class);
//            }
//        } catch (JDSException e) {
//            result = new ErrorListResultModel<>();
//            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
//            ((ErrorListResultModel) result).setErrdes(e.getMessage());
//        }
//        return result;
//    }


//    @MethodChinaName(cname = "代码工厂")
//    @RequestMapping(method = RequestMethod.POST, value = "AggregationTemps")
//    @APIEventAnnotation(autoRun = true)
//    @NavTreeViewAnnotation
//    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", dynLoad = true, caption = "代码工厂")
//    @CustomAnnotation( index = 6)
//    @ResponseBody
//    public TreeListResultModel<List<AggregationTempNavTree>> getAggregationTemps(String id, String domainId) {
//        TreeListResultModel<List<AggregationTempNavTree>> result = new TreeListResultModel<>();
//        result = TreePageUtil.getTreeList(Arrays.asList(domainId), AggregationTempNavTree.class);
//        return result;
//
//    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
