package com.ds.dsm.manager.aggregation;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.action.ESDMenuAction;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.BlockAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.tool.ui.enums.Dock;
import org.checkerframework.checker.guieffect.qual.UI;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/manager/agg/")
@BottomBarMenu(menuClass = ESDMenuAction.class)
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {AggregationService.class})
@MethodChinaName(cname = "实体信息", imageClass = "spafont spa-icon-c-gallery")
public class AggregationNav {


    @CustomAnnotation(pid = true, hidden = true)
    public String domainId;


    @CustomAnnotation(uid = true, hidden = true)
    public String projectVersionName;


    @RequestMapping(method = RequestMethod.POST, value = "AggregationInfo")
    @FormViewAnnotation()
    @ModuleAnnotation(caption = "实体信息")
    @UIAnnotation( dock = Dock.top, height = "150")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<AggregationFormView> getAggregationInfo(String domainId, String projectVersionName, String className) {
        ResultModel<AggregationFormView> result = new ResultModel<AggregationFormView>();
        DomainInst domainInst = null;
        try {
            domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        result.setData(new AggregationFormView(domainInst));
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "AggregationMetaInfo")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "关联信息", imageClass = "spafont spa-icon-c-databinder")
    @ResponseBody
    public ResultModel<AggregationMetaView> getAggregationMetaInfo(String domainId, String projectVersionName, String className) {
        ResultModel<AggregationMetaView> result = new ResultModel<AggregationMetaView>();
        return result;

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
