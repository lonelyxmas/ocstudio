package com.ds.dsm.manager.aggregation;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/manager/agg/")
@MethodChinaName(cname = "聚合模型", imageClass = "spafont spa-icon-conf")

public class AggregationService {

    public AggregationService() {

    }

    @MethodChinaName(cname = "更新领域信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateDomain")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateDomain(@RequestBody AggregationFormView tempInfo) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(tempInfo.getDomainId());
            if (bean == null) {
                bean = DSMFactory.getInstance().createDomain(tempInfo.getProjectVersionName());
            }
            bean.setDesc(tempInfo.getDesc());
            bean.setDomainId(tempInfo.getDomainId());
            bean.setName(tempInfo.getName());
            bean.setSpace(tempInfo.getSpace());
            bean.setPackageName(tempInfo.getPackageName());
            bean.setProjectVersionName(tempInfo.getProjectVersionName());
            bean.setDsmType(tempInfo.getDsmType());
            DSMFactory.getInstance().getAggregationManager().updateDomainInst(bean, true);

        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "刪除领域")
    @RequestMapping(method = RequestMethod.POST, value = "delDomain")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> delDomain(String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (domainId == null) {
                throw new JDSException("domainId is bull");
            }
            DSMFactory.getInstance().deleteDomainInst(domainId);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }


    @RequestMapping(value = {"CreateDomain"}, method = {RequestMethod.GET, RequestMethod.POST})
    @FormViewAnnotation
    @ModuleAnnotation(caption = "新建领域模型")
    @DialogAnnotation(height = "360", width = "480")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public ResultModel<AggregationCreateView> createDomain(String projectVersionName) {
        ResultModel<AggregationCreateView> result = new ResultModel<AggregationCreateView>();
        try {
            DomainInst dsmInst = DSMFactory.getInstance().createDomain(projectVersionName);
            result.setData(new AggregationCreateView(dsmInst));
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AggregationNav")
    @NavGroupViewAnnotation()
    @ModuleAnnotation(caption = "模板详细信息")
    @DialogAnnotation(width = "900")
    @APIEventAnnotation(bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<AggregationNav> getTempNav(String domainId) {
        ResultModel<AggregationNav> result = new ResultModel<AggregationNav>();
        return result;
    }


}
