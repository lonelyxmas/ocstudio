package com.ds.dsm.manager.aggregation;

import com.ds.common.JDSException;
import com.ds.dsm.manager.temp.agg.AggJavaTempService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, caption = "JAVA模板")
public class AggregationTempNavTree<T extends AggregationTempNavTree> extends TreeListItem<T> {

    @Pid
    AggregationType aggregationType;
    @Pid
    String domainId;

    @TreeItemAnnotation(imageClass = "spafont spa-icon-settingprj", bindService = AggregationTreeService.class)
    public AggregationTempNavTree(String domainId) throws JDSException {
        this.caption = "模板分类";
        this.id = "AggregationRoot";
        this.iniFold = false;
        this.domainId = domainId;
    }

    @TreeItemAnnotation(bindService = AggJavaTempService.class)
    public AggregationTempNavTree(AggregationType aggregationType, String domainId) throws JDSException {
        this.caption = aggregationType.getName();
        this.imageClass = aggregationType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + aggregationType.getType();
        this.iniFold = false;
        this.aggregationType = aggregationType;
        this.domainId = domainId;

    }

    public AggregationType getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(AggregationType aggregationType) {
        this.aggregationType = aggregationType;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
