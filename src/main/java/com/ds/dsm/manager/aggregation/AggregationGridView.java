package com.ds.dsm.manager.aggregation;

import com.ds.dsm.aggregation.action.ESDRowCMDAction;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;
import com.ds.web.annotation.Required;

@PageBar
@RowHead(rowNumbered = false,
        selMode = SelModeType.none, rowHandlerWidth = "6em")
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, menuClass = {ESDRowCMDAction.class})
@GridAnnotation(
        customMenu = {GridMenu.Reload, GridMenu.Add},
        customService = {AggregationService.class},
        event = {CustomGridEvent.editor})
public class AggregationGridView {
    @FieldAnnotation(colWidth = "16em")
    @CustomAnnotation(uid = true, hidden = true)
    String domainId;

    @FieldAnnotation(colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "名称")
    private String desc;

    @Required
    @CustomAnnotation(caption = "标识")
    private String name;

    @Required
    @CustomAnnotation(caption = "命名空间")
    public String space = "dsm";

    @Required
    @CustomAnnotation(caption = "包名")
    private String packageName = "com.ds." + space;

    @CustomAnnotation(hidden = true, pid = true)
    public String projectVersionName;


    @CustomAnnotation(hidden = true, pid = true)
    public DSMType dsmType = DSMType.aggregation;


    public AggregationGridView() {

    }

    public AggregationGridView(DomainInst bean) {
        this.name = bean.getName();
        this.space = bean.getSpace();
        this.domainId = bean.getDomainId();
        this.desc = bean.getDesc();
        if (desc == null || desc.equals("")) {
            this.desc = bean.getName();
        }
        this.projectVersionName = bean.getProjectVersionName();
        this.packageName = bean.getPackageName();
        this.dsmType = bean.getDsmType();
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }


    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }
}
