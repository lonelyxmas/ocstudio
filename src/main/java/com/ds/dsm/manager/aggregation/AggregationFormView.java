package com.ds.dsm.manager.aggregation;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.web.annotation.Required;


@FormAnnotation(stretchHeight = StretchType.last, customService = {AggregationService.class})
public class AggregationFormView {
    @FieldAnnotation( colWidth = "16em")
    @CustomAnnotation(uid = true, hidden = true)
    String domainId;

    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "标识")
    private String name;

    @Required
    @CustomAnnotation(caption = "命名空间")
    public String space = "dsm";

    @Required
    @CustomAnnotation(caption = "包名")
    private String packageName = "com.ds.dsm";

    @CustomAnnotation(readonly = true, hidden = true)
    private DSMType dsmType;

    @FieldAnnotation(required = true, colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "描述")
    private String desc;


    @CustomAnnotation(hidden = true)
    private String projectVersionName;


    public AggregationFormView() {

    }

    public AggregationFormView(DomainInst bean) {
        this.name = bean.getName();
        this.space = bean.getSpace();
        this.domainId = bean.getDomainId();
        this.desc = bean.getDesc();
        this.projectVersionName = bean.getProjectVersionName();
        this.dsmType = bean.getDsmType();
        if (desc == null || desc.equals("")) {
            this.desc = bean.getName();
        }
        this.packageName = bean.getPackageName();
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }


    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


}
