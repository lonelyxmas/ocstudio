package com.ds.dsm.manager.aggregation;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.temp.agg.AggTempGrid;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggregationManager;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/temp/agg/nav/")
@MethodChinaName(cname = "聚合模型", imageClass = "spafont spa-icon-conf")

public class AggregationTreeService {


    @MethodChinaName(cname = "代码工厂")
    @RequestMapping(method = RequestMethod.POST, value = "AggregationTemps")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggregationTempNavTree>> getAggregationTemps(String id, String domainId) {
        TreeListResultModel<List<AggregationTempNavTree>> result = new TreeListResultModel<>();
        AggregationType[] aggregationTypes = AggregationType.values();
        result = TreePageUtil.getTreeList(Arrays.asList(aggregationTypes), AggregationTempNavTree.class);
        return result;

    }

    @MethodChinaName(cname = "All模板")
    @RequestMapping(method = RequestMethod.POST, value = "AllAggTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "All模板")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<AggTempGrid>> getCustomTempGrid(String domainId) {
        ListResultModel<List<AggTempGrid>> result = new ListResultModel();
        try {
            AggregationManager aggManager = DSMFactory.getInstance().getAggregationManager();
            Set<String> javaTempIds = aggManager.getDomainInstById(domainId).getJavaTempIds();
            List<JavaTemp> temps = new ArrayList<>();
            for (String javaid : javaTempIds) {
                JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                temps.add(javaTemp);
            }

            result = PageUtil.getDefaultPageList(temps, AggTempGrid.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }


}
