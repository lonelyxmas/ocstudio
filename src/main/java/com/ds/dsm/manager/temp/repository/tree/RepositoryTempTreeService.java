package com.ds.dsm.manager.temp.repository.tree;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.temp.agg.tree.AggTempPopTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/repository/temp/tree/")
@MethodChinaName(cname = "模板选择")

public class RepositoryTempTreeService {

    @RequestMapping(method = RequestMethod.POST, value = "loadChild")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<RepositoryTempPopTree>> loadChild(RepositoryType repositoryType, String domainId) {
        TreeListResultModel<List<RepositoryTempPopTree>> result = new TreeListResultModel<>();
        try {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(repositoryType);
            result = TreePageUtil.getTreeList(javaTemps, RepositoryTempPopTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }
}
