package com.ds.dsm.manager.temp.view;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.admin.temp.JavaTempForm;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.web.annotation.ViewType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/temp/")
@MethodChinaName(cname = "领域模板", imageClass = "spafont spa-icon-c-cssbox")

public class ViewJavaTempService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "ViewTempGrid")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<ViewTempGrid>> getAggTempGrid(String viewInstId, ViewType viewType) {
        ListResultModel<List<ViewTempGrid>> resultModel = new ListResultModel();
        ViewInst bean = null;
        try {
            bean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null) {
                        if (javaTemp.getViewType() != null && javaTemp.getViewType().equals(viewType)) {
                            temps.add(javaTemp);
                        }
                    }
                }
                resultModel = PageUtil.getDefaultPageList(temps, ViewTempGrid.class);
            }
        } catch (JDSException e) {
            resultModel = new ErrorListResultModel<>();
            ((ErrorListResultModel) resultModel).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) resultModel).setErrdes(e.getMessage());
        }

        return resultModel;
    }


    @MethodChinaName(cname = "移除JAVA模板")
    @RequestMapping(method = RequestMethod.POST, value = "removeJavaTemp")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> removeJavaTemp(String javaTempId, String id, String viewInstId, String domainId, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (javaTempId == null && id == null) {
                throw new JDSException("javaTempId is bull");
            }
            if (javaTempId == null) {
                javaTempId = id;
            }
            String[] tempIds = StringUtility.split(javaTempId, ";");
            ViewInst ViewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);

            if (sourceClassName != null && !sourceClassName.equals("")) {
                ViewEntityConfig entityConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
                for (String tempId : tempIds) {
                    entityConfig.getJavaTempIds().remove(tempId);
                }
                DSMFactory.getInstance().getViewManager().updateViewEntityConfig(entityConfig);
            } else {
                for (String tempId : tempIds) {
                    ViewInst.getJavaTempIds().remove(tempId);
                }
                DSMFactory.getInstance().getViewManager().updateViewInst(ViewInst, true);
            }


        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "TempFileInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @ModuleAnnotation(caption = "模板信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    public @ResponseBody
    ResultModel<JavaTempForm> getJavaTempInfo(String javaTempId) {
        ResultModel<JavaTempForm> result = new ResultModel<JavaTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaTempId);
            result.setData(new JavaTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<JavaTempForm> errorResult = new ErrorResultModel<JavaTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }


        return result;
    }

}
