package com.ds.dsm.manager.temp.agg.website;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.temp.agg.tree.AggTempPopTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/agg/temp/website/")
@Aggregation(type = AggregationType.menu,rootClass =AggWebSiteImport.class )
public class AggWebSiteImport {


    @MethodChinaName(cname = "业务模板")
    @RequestMapping(method = RequestMethod.POST, value = "LocalWebSiteNav")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "900",height = "620")
    @ModuleAnnotation( dynLoad = true,
            caption = "业务模板", imageClass = "spafont spa-icon-c-menubar")
    @ResponseBody
    public TreeListResultModel<List<AggWebSiteSelectTree>> getLocalWebSiteNav(String domainId) {
        TreeListResultModel<List<AggWebSiteSelectTree>> resultModel = new TreeListResultModel<List<AggWebSiteSelectTree>>();
        resultModel = TreePageUtil.getDefaultTreeList(Arrays.asList(domainId), AggWebSiteSelectTree.class);
        return resultModel;

    }


    @MethodChinaName(cname = "添加JAVA模板")
    @RequestMapping(value = "AddTempFromJaveTemp")
    @PopTreeViewAnnotation()
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-c-flash", caption = "添加JAVA模板")
    @DialogAnnotation(width = "300",
            height = "450")
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<AggTempPopTree>> addTempFromJaveTemp(String domainId, AggregationType aggregationType) {
        TreeListResultModel<List<AggTempPopTree>> result = new TreeListResultModel<List<AggTempPopTree>>();
        try {
            DomainInst tempBean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            Set<String> javaTempIds = tempBean.getJavaTempIds();
            List<String> ids = new ArrayList<>(Arrays.asList(javaTempIds.toArray(new String[]{})));
            if (aggregationType != null) {
                result = TreePageUtil.getTreeList(Arrays.asList(aggregationType), AggTempPopTree.class, ids);
            } else {
                AggregationType[] aggregationTypes = AggregationType.values();
                result = TreePageUtil.getTreeList(Arrays.asList(aggregationTypes), AggTempPopTree.class, ids);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

}

