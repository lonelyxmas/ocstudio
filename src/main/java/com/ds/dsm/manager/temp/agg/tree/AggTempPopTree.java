package com.ds.dsm.manager.temp.agg.tree;

import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Pid;

import java.util.HashMap;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, iniFold = false, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = AggTempSelectService.class)
@PopTreeAnnotation(caption = "JAVA模板")
public class AggTempPopTree<T extends AggTempPopTree> extends TreeListItem {


    @Pid
    String domainId;
    @Pid
    AggregationType aggregationType;
    @Pid
    String javaTempId;

    public AggTempPopTree(String domainId) {
        this.caption = DSMType.aggregation.getName();
        this.imageClass = DSMType.aggregation.getImageClass();
        this.iniFold = false;
        this.id = DSMType.aggregation.getType();
        this.domainId = domainId;

    }

    @TreeItemAnnotation(bindService = AggTempTreeService.class, iniFold = false)
    public AggTempPopTree(AggregationType aggregationType, String domainId) {
        this.caption = aggregationType.getName();
        this.imageClass = aggregationType.getImageClass();
        this.id = aggregationType.getType();
        this.tagVar = new HashMap<>();
        this.domainId = domainId;
        this.aggregationType = aggregationType;

    }

    @TreeItemAnnotation()
    public AggTempPopTree(JavaTemp javaTemp, String domainId) {
        this.caption = javaTemp.getName();
        this.imageClass = "spafont spa-icon-page";
        this.id = javaTemp.getJavaTempId();
        this.domainId = domainId;
        this.javaTempId = javaTemp.getJavaTempId();
        this.tagVar = new HashMap<>();
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public AggregationType getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(AggregationType aggregationType) {
        this.aggregationType = aggregationType;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }
}
