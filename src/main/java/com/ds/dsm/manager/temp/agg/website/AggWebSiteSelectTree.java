package com.ds.dsm.manager.temp.agg.website;

import com.ds.esd.custom.annotation.NavTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(caption = "JAVA模板")
@NavTreeAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = AggWebSiteSelectService.class)
@BottomBarMenu
public class AggWebSiteSelectTree<T extends AggWebSiteSelectTree> extends TreeListItem<T> {

    @Pid
    String domainId;

    @Pid
    DSMTempType dsmTempType;

    @TreeItemAnnotation(bindService = AllAggWebSiteService.class)
    public AggWebSiteSelectTree(String domainId) {
        this.caption = "所有模型";
        this.id = "DSMTempRoot";
        this.domainId = domainId;

    }

    @TreeItemAnnotation(bindService = AggWebSiteSelectService.class)
    public AggWebSiteSelectTree(DSMTempType dsmTempType, String domainId) {
        this.domainId = domainId;
        this.caption = dsmTempType.getName();
        this.id = dsmTempType.getType();
        this.dsmTempType = dsmTempType;
        this.imageClass = dsmTempType.getImageClass();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public DSMTempType getDsmTempType() {
        return dsmTempType;
    }

    public void setDsmTempType(DSMTempType dsmTempType) {
        this.dsmTempType = dsmTempType;
    }
}
