package com.ds.dsm.manager.temp.agg;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.GridPageUtil;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/temp/")
@MethodChinaName(cname = "领域模板", imageClass = "spafont spa-icon-c-cssbox")
public class AggJavaTempService {


    @RequestMapping(method = RequestMethod.POST, value = "AggTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AggTempGrid")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<AggTempGrid>> getAggTempGrid(String domainId, AggregationType aggregationType) {
        ListResultModel<List<AggTempGrid>> resultModel = new ListResultModel();
        DomainInst bean = null;
        try {
            bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null) {
                        if (javaTemp.getAggregationType() != null && javaTemp.getAggregationType().equals(aggregationType)) {
                            temps.add(javaTemp);
                        }
                    }
                }
                resultModel = GridPageUtil.getDefaultPageList(temps, AggTempGrid.class);
            }
        } catch (JDSException e) {
            resultModel = new ErrorListResultModel<>();
            ((ErrorListResultModel) resultModel).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) resultModel).setErrdes(e.getMessage());
        }

        return resultModel;
    }


    @MethodChinaName(cname = "移除JAVA模板")
    @RequestMapping(method = RequestMethod.POST, value = "removeAggJavaTemp")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> removeJavaTemp(String javaTempId, String id, String domainId, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (javaTempId == null && id == null) {
                throw new JDSException("javaTempId is bull");
            }
            if (javaTempId == null) {
                javaTempId = id;
            }
            String[] tempIds = StringUtility.split(javaTempId, ";");
            DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);

            if (sourceClassName != null && !sourceClassName.equals("")) {
                AggEntityConfig aggEntityConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
                for (String tempId : tempIds) {
                    aggEntityConfig.getJavaTempIds().remove(tempId);
                }
                DSMFactory.getInstance().getAggregationManager().updateAggEntityConfig(aggEntityConfig);
            } else {
                for (String tempId : tempIds) {
                    domainInst.getJavaTempIds().remove(tempId);
                }
                DSMFactory.getInstance().getAggregationManager().updateDomainInst(domainInst, true);
            }


        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AggTempFileInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @ModuleAnnotation(caption = "模板信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    public @ResponseBody
    ResultModel<AggJavaTempForm> getJavaTempInfo(String javaTempId) {
        ResultModel<AggJavaTempForm> result = new ResultModel<AggJavaTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaTempId);
            result.setData(new AggJavaTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<AggJavaTempForm> errorResult = new ErrorResultModel<AggJavaTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }


        return result;
    }

}
