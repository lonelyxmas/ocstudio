package com.ds.dsm.manager.temp.view.website;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.temp.view.tree.ViewTempPopTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.ViewType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/view/temp/website/")
@Aggregation(type = AggregationType.menu,rootClass =ViewWebSiteImport.class )
public class ViewWebSiteImport {


    @MethodChinaName(cname = "业务模板")
    @RequestMapping(method = RequestMethod.POST, value = "LocalViewWebSiteNav")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "900",height = "620" )
    @ModuleAnnotation(dynLoad = true,
             caption = "业务模板", imageClass = "spafont spa-icon-c-menubar")
    @ResponseBody
    public TreeListResultModel<List<ViewWebSiteSelectTree>> getLocalViewWebSiteNav(String domainId) {
        TreeListResultModel<List<ViewWebSiteSelectTree>> resultModel = new TreeListResultModel<List<ViewWebSiteSelectTree>>();
        resultModel = TreePageUtil.getDefaultTreeList(Arrays.asList(domainId), ViewWebSiteSelectTree.class);
        return resultModel;

    }


    @MethodChinaName(cname = "添加JAVA模板")
    @RequestMapping(value = "AddViewTempFromJaveTemp")
    @PopTreeViewAnnotation()
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-c-flash", caption = "添加JAVA模板")
    @DialogAnnotation(width = "300",
            height = "450")
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<ViewTempPopTree>> addViewTempFromJaveTemp(String viewInstId, ViewType viewType) {
        TreeListResultModel<List<ViewTempPopTree>> result = new TreeListResultModel<List<ViewTempPopTree>>();
        try {
            ViewInst tempBean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            Set<String> javaTempIds = tempBean.getJavaTempIds();
            List<String> ids = new ArrayList<>(Arrays.asList(javaTempIds.toArray(new String[]{})));
            if (viewType != null) {
                result = TreePageUtil.getTreeList(Arrays.asList(viewType), ViewTempPopTree.class, ids);
            } else {
                ViewType[] viewTypes = ViewType.values();
                result = TreePageUtil.getTreeList(Arrays.asList(viewTypes), ViewTempPopTree.class, ids);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

}

