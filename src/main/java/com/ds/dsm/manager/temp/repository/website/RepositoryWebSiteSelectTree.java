package com.ds.dsm.manager.temp.repository.website;

import com.ds.esd.custom.annotation.NavTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(caption = "JAVA模板")
@NavTreeAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = RepositoryWebSiteSelectService.class)
@BottomBarMenu
public class RepositoryWebSiteSelectTree<T extends RepositoryWebSiteSelectTree> extends TreeListItem<T> {

    @Pid
    String projectVersionName;

    @Pid
    DSMTempType dsmTempType;

    @TreeItemAnnotation(bindService = AllRepositoryWebSiteService.class)
    public RepositoryWebSiteSelectTree(String projectVersionName) {
        this.caption = "所有模型";
        this.id = "DSMTempRoot";
        this.projectVersionName = projectVersionName;

    }

    @TreeItemAnnotation(bindService = RepositoryWebSiteSelectService.class)
    public RepositoryWebSiteSelectTree(DSMTempType dsmTempType, String projectVersionName) {
        this.projectVersionName = projectVersionName;
        this.caption = dsmTempType.getName();
        this.id = dsmTempType.getType();
        this.dsmTempType = dsmTempType;
        this.imageClass = dsmTempType.getImageClass();

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public DSMTempType getDsmTempType() {
        return dsmTempType;
    }

    public void setDsmTempType(DSMTempType dsmTempType) {
        this.dsmTempType = dsmTempType;
    }
}
