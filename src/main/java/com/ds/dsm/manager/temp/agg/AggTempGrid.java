package com.ds.dsm.manager.temp.agg;


import com.ds.dsm.manager.temp.agg.website.AggWebSiteImport;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.annotation.AggregationType;


@PageBar
@MenuBarMenu(menuClasses = {AggWebSiteImport.class})
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Delete}, customService = {AggJavaTempService.class}, event = CustomGridEvent.editor)
public class AggTempGrid {

    @CustomAnnotation(uid = true, hidden = true)
    String javaTempId;

    @CustomAnnotation(pid = true, hidden = true)
    String fileId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;


    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String name;

    @CustomAnnotation(caption = "聚合类型", pid = true)
    AggregationType aggregationType = AggregationType.customapi;

    @CustomAnnotation(caption = "模板类型", pid = true)
    DSMType dsmType = DSMType.aggregation;


    @CustomAnnotation(caption = "模板说明")
    String desc;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "包名规则")
    String packagePostfix;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "名称规则")
    String namePostfix;

    public AggTempGrid() {

    }

    public AggTempGrid(JavaTemp temp) {
        this.name = temp.getName();
        this.desc = getDesc();
        this.aggregationType = temp.getAggregationType();
        this.fileId = temp.getFileId();
        this.javaTempId = temp.getJavaTempId();
        this.dsmType = temp.getDsmType();
        this.namePostfix = temp.getNamePostfix();
        this.packagePostfix = temp.getPackagePostfix();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }


    public String getPackagePostfix() {
        return packagePostfix;
    }

    public void setPackagePostfix(String packagePostfix) {
        this.packagePostfix = packagePostfix;
    }


    public String getNamePostfix() {
        return namePostfix;
    }

    public void setNamePostfix(String namePostfix) {
        this.namePostfix = namePostfix;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public AggregationType getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(AggregationType aggregationType) {
        this.aggregationType = aggregationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
