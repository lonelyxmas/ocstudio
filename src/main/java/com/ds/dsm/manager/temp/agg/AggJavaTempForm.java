package com.ds.dsm.manager.temp.agg;

import com.ds.dsm.admin.temp.AdminTempService;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.toolbar.CodeEditorTools;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RangeType;
import com.ds.esd.dsm.enums.ThumbnailType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(stretchHeight = StretchType.last, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {AdminTempService.class})
public class AggJavaTempForm<T> {

    @CustomAnnotation(uid = true, hidden = true)
    String javaTempId;

    @CustomAnnotation(pid = true, hidden = true)
    String fileId;

    @CustomAnnotation(pid = true, hidden = true)
    ThumbnailType thumbnailType;

    @InputAnnotation(multiLines = true)
    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String name;

    @CustomAnnotation(caption = "领域类型", hidden = true)
    DSMType dsmType;
    @Required
    @CustomAnnotation(caption = "聚合类型")
    AggregationType aggregationType;

    @CustomAnnotation(caption = "模板域")
    RangeType rangeType;
    @Required
    @CustomAnnotation(caption = "包名规则")
    String packagePostfix;

    @CustomAnnotation(caption = "模板文件", hidden = true)
    String path;
    @Required
    @CustomAnnotation(caption = "名称规则")
    String namePostfix;
    @FieldAnnotation(rowHeight = "100", componentType = ComponentType.Image)
    @CustomAnnotation( caption = "略缩图")
    String image = "/RAD/img/project.png";

    @FileUploadAnnotation(src = "/custom/dsm/AttachUPLoad?thumbnailType=javaTemp")
    @FieldAnnotation(rowHeight = "100", componentType = ComponentType.FileUpload)
    @CustomAnnotation( caption = "上传略缩图")
    String thumbnailFile;


    @JavaEditorAnnotation
    @ToolBarMenu(menuClasses = CodeEditorTools.class)
    @FieldAnnotation( haslabel = false, colSpan = -1, rowHeight = "350")
    @CustomAnnotation(caption = "模板内容")
    String content;

    public AggJavaTempForm() {

    }

    public AggJavaTempForm(JavaTemp temp) {
        this.name = temp.getName();
        this.image = temp.getImage();
        this.aggregationType = temp.getAggregationType();
        this.content = temp.getContent();
        this.fileId = temp.getFileId();
        this.javaTempId = temp.getJavaTempId();
        this.path = temp.getPath();
        this.dsmType = temp.getDsmType();
        this.rangeType = temp.getRangeType();
        this.namePostfix = temp.getNamePostfix();
        this.packagePostfix = temp.getPackagePostfix();
        this.thumbnailType = ThumbnailType.javaTemp;
    }

    public String getThumbnailFile() {
        return thumbnailFile;
    }

    public void setThumbnailFile(String thumbnailFile) {
        this.thumbnailFile = thumbnailFile;
    }

    public ThumbnailType getThumbnailType() {
        return thumbnailType;
    }

    public void setThumbnailType(ThumbnailType thumbnailType) {
        this.thumbnailType = thumbnailType;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }


    public String getPackagePostfix() {
        return packagePostfix;
    }

    public void setPackagePostfix(String packagePostfix) {
        this.packagePostfix = packagePostfix;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getNamePostfix() {
        return namePostfix;
    }

    public void setNamePostfix(String namePostfix) {
        this.namePostfix = namePostfix;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public RangeType getRangeType() {
        return rangeType;
    }

    public void setRangeType(RangeType rangeType) {
        this.rangeType = rangeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
