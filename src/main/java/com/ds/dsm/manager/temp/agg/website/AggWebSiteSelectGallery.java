package com.ds.dsm.manager.temp.agg.website;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.gallery.annotation.GalleryAnnotation;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.tool.ui.enums.ComponentType;

@BottomBarMenu()
@GalleryAnnotation(customService = {AggWebSiteSelectService.class})
public class AggWebSiteSelectGallery {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation()
    String caption;

    @CustomAnnotation(uid = true, hidden = true)
    String id;

    @CustomAnnotation(caption = "模板名称", captionField = true)
    String comment;

    @FieldAnnotation( componentType = ComponentType.Image)
    @CustomAnnotation(caption = "图片", captionField = true)
    String image = "/RAD/img/project.png";


    public AggWebSiteSelectGallery() {

    }

    public AggWebSiteSelectGallery(DSMBean temp) {
        this.comment = temp.getName();
        if (temp.getImage() != null && !temp.getImage().equals("")) {
            this.image = temp.getImage();
        }
        this.id = temp.getDsmTempId();
        this.caption = "";
        this.dsmTempId = temp.getDsmTempId();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

}
