package com.ds.dsm.manager.temp.view.website;

import com.ds.esd.custom.annotation.NavTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(caption = "JAVA模板")
@NavTreeAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = ViewWebSiteSelectService.class)
@BottomBarMenu
public class ViewWebSiteSelectTree<T extends ViewWebSiteSelectTree> extends TreeListItem<T> {

    @Pid
    String viewInstId;

    @Pid
    DSMTempType dsmTempType;

    @TreeItemAnnotation(bindService = AllViewWebSiteService.class)
    public ViewWebSiteSelectTree(String viewInstId) {
        this.caption = "所有模型";
        this.id = "DSMTempRoot";
        this.viewInstId = viewInstId;

    }

    @TreeItemAnnotation(bindService = ViewWebSiteSelectService.class)
    public ViewWebSiteSelectTree(DSMTempType dsmTempType, String viewInstId) {
        this.viewInstId = viewInstId;
        this.caption = dsmTempType.getName();
        this.id = dsmTempType.getType();
        this.dsmTempType = dsmTempType;
        this.imageClass = dsmTempType.getImageClass();

    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public DSMTempType getDsmTempType() {
        return dsmTempType;
    }

    public void setDsmTempType(DSMTempType dsmTempType) {
        this.dsmTempType = dsmTempType;
    }
}
