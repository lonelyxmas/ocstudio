package com.ds.dsm.manager.temp.view.website;


import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.temp.agg.website.AggWebSiteSelectService;
import com.ds.dsm.manager.temp.agg.website.AggWebSiteSelectTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/temp/select/")
@TabsAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {ViewWebSiteSelectService.class}, singleOpen = true)
public class ViewWebSiteSelectNav {


    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewInstId;


    @MethodChinaName(cname = "站内模板")
    @RequestMapping(method = RequestMethod.POST, value = "LocalWebSiteNav")
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "站内模板")
    @ResponseBody
    public TreeListResultModel<List<ViewWebSiteSelectTree>> getLocalWebSiteNav(String viewInstId) {
        TreeListResultModel<List<ViewWebSiteSelectTree>> resultModel = new TreeListResultModel<List<ViewWebSiteSelectTree>>();
        resultModel = TreePageUtil.getDefaultTreeList(Arrays.asList(viewInstId), ViewWebSiteSelectTree.class);
        return resultModel;

    }


//    @MethodChinaName(cname = "模板市场")
//    @RequestMapping(method = RequestMethod.POST, value = "NETWebSiteNav")
//    @NavTreeViewAnnotation
//    @ModuleAnnotation(imageClass = "spafont spa-icon-links", dynLoad = true, caption = "模板市场")
//    @ResponseBody
//    public TreeListResultModel<List<RepositoryWebSiteSelectTree>> getNETWebSiteNav(String repositoryInstId) {
//        TreeListResultModel<List<RepositoryWebSiteSelectTree>> resultModel = new TreeListResultModel<List<RepositoryWebSiteSelectTree>>();
//        resultModel= TreePageUtil.getDefaultTreeList(Arrays.asList(repositoryInstId),RepositoryWebSiteSelectTree.class);
//        return resultModel;
//
//    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
