package com.ds.dsm.manager.temp.view.tree;

import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.ViewType;

import java.util.HashMap;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, iniFold = false, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = ViewTempSelectService.class)
@PopTreeAnnotation(caption = "JAVA模板")
public class ViewTempPopTree<T extends ViewTempPopTree> extends TreeListItem {


    @Pid
    String viewInstId;
    @Pid
    ViewType viewType;
    @Pid
    String javaTempId;

    public ViewTempPopTree(String viewInstId) {
        this.caption = DSMType.view.getName();
        this.imageClass = DSMType.view.getImageClass();
        this.iniFold = false;
        this.id = DSMType.view.getType();
        this.viewInstId = viewInstId;

    }

    @TreeItemAnnotation(bindService = ViewTempTreeService.class, iniFold = false)
    public ViewTempPopTree(ViewType viewType, String viewInstId) {
        this.caption = viewType.getName();
        this.imageClass = viewType.getImageClass();
        this.id = viewType.getType();
        this.tagVar = new HashMap<>();
        this.viewInstId = viewInstId;
        this.viewType = viewType;

    }

    @TreeItemAnnotation()
    public ViewTempPopTree(JavaTemp javaTemp, String viewInstId) {
        this.caption = javaTemp.getName();
        this.imageClass = "spafont spa-icon-page";
        this.id = javaTemp.getJavaTempId();
        this.viewInstId = viewInstId;
        this.javaTempId = javaTemp.getJavaTempId();
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public ViewType getViewType() {
        return viewType;
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }
}
