package com.ds.dsm.manager.temp.view.website;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GalleryViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(path = "/dsm/view/temp/select/")
@MethodChinaName(cname = "模板选择")

public class ViewWebSiteSelectService {


    @RequestMapping(method = RequestMethod.POST, value = "viewLoadTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeReload)
    @ResponseBody
    public TreeListResultModel<List<ViewWebSiteSelectTree>> getViewLoadTree(String domainId) {
        DSMTempType[] dsmTypes = DSMTempType.values();
        TreeListResultModel treeListResultModel = TreePageUtil.getTreeList(Arrays.asList(dsmTypes), ViewWebSiteSelectTree.class);
        return treeListResultModel;
    }


    @MethodChinaName(cname = "远程模板管理")
    @RequestMapping(method = RequestMethod.POST, value = "ViewNETWebSiteList")
    @GalleryViewAnnotation
    @ModuleAnnotation(caption = "远程模板管理")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<ViewWebSiteSelectGallery>> getViewNETWebSiteList(String viewInstId, String sourceClassName, String dsmTempType) {
        ListResultModel<List<ViewWebSiteSelectGallery>> result = new ListResultModel();
        List<DSMBean> dsmBeans = new ArrayList<>();
        try {
            List<DSMBean> tempBeans = DSMFactory.getInstance().getTempManager().getDSMBeanList();
            for (DSMBean bean : tempBeans) {
                if (bean.getType() == null || bean.getType().equals(dsmTempType)) {
                    dsmBeans.add(bean);
                }
            }
            result = PageUtil.getDefaultPageList(dsmBeans, ViewWebSiteSelectGallery.class);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "本地模板管理")
    @RequestMapping(method = RequestMethod.POST, value = "ViewLocalWebSiteList")
    @ModuleAnnotation(caption = "本地模板管理")
    @GalleryViewAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<ViewWebSiteSelectGallery>> getViewLocalWebSiteList(String viewInstId, DSMTempType dsmTempType) {
        ListResultModel<List<ViewWebSiteSelectGallery>> result = new ListResultModel();
        List<DSMBean> dsmBeans = new ArrayList<>();
        try {
            List<DSMBean> tempBeans = DSMFactory.getInstance().getTempManager().getDSMBeanList();
            for (DSMBean bean : tempBeans) {
                if (bean.getType() == null || bean.getType().equals(dsmTempType)) {
                    dsmBeans.add(bean);
                }
            }
            result = PageUtil.getDefaultPageList(dsmBeans, ViewWebSiteSelectGallery.class);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "从模板添加")
    @RequestMapping(method = RequestMethod.POST, value = "addTempFromWebSite")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadTopParent, CustomCallBack.CloseTop}, bindMenu = CustomMenuItem.gridSave)
    public @ResponseBody
    ResultModel<Boolean> addTempFromWebSite(String LocalWebSiteListGallery, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (LocalWebSiteListGallery == null) {
                throw new JDSException("javaTempId is bull");
            }
            if (LocalWebSiteListGallery != null) {
                String[] tempIds = StringUtility.split(LocalWebSiteListGallery, ";");
                Set<String> javaTempIds = new HashSet<>();
                for (String id : tempIds) {
                    DSMBean tempBean = DSMFactory.getInstance().getTempManager().getDSMBeanById(id);
                    if (tempBean != null) {
                        javaTempIds.addAll(tempBean.getJavaTempIds());
                    }
                }
                ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
                viewInst.getJavaTempIds().clear();
                viewInst.getJavaTempIds().addAll(javaTempIds);
                DSMFactory.getInstance().getViewManager().updateViewInst(viewInst, true);

            }
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "选中模板")
    @RequestMapping(method = RequestMethod.POST, value = "selectTemp")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadTopParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save})
    public @ResponseBody
    ResultModel<Boolean> selectTemp(String ViewNETWebSiteList, String ViewLocalWebSiteList, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {

            if (ViewNETWebSiteList == null && ViewLocalWebSiteList == null) {
                throw new JDSException("javaTempId is bull");
            }
            String[] tempIds = new String[]{};
            if (ViewLocalWebSiteList != null && !ViewLocalWebSiteList.equals("")) {
                tempIds = StringUtility.split(ViewLocalWebSiteList, ";");
            } else if (ViewNETWebSiteList != null && !ViewNETWebSiteList.equals("")) {
                tempIds = StringUtility.split(ViewNETWebSiteList, ";");

            }
            Set<String> javaTempIds = new HashSet<>();
            for (String id : tempIds) {
                DSMBean tempBean = DSMFactory.getInstance().getTempManager().getDSMBeanById(id);
                if (tempBean != null) {
                    javaTempIds.addAll(tempBean.getJavaTempIds());
                }
            }


            ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            viewInst.getJavaTempIds().clear();
            viewInst.getJavaTempIds().addAll(javaTempIds);
            DSMFactory.getInstance().getViewManager().updateViewInst(viewInst, true);

        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

}
