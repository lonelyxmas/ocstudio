package com.ds.dsm.manager.temp.repository.website;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.temp.repository.tree.RepositoryTempPopTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/repository/temp/website/")
@Aggregation(type = AggregationType.menu,rootClass =RepositoryWebSiteImport.class )
public class RepositoryWebSiteImport {


    @MethodChinaName(cname = "业务模板")
    @RequestMapping(method = RequestMethod.POST, value = "RepositoryLocalWebSiteNav")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "900",height = "620")
    @ModuleAnnotation( dynLoad = true,
             caption = "业务模板", imageClass = "spafont spa-icon-c-menubar")
    @ResponseBody
    public TreeListResultModel<List<RepositoryWebSiteSelectTree>> getLocalWebSiteNav(String projectVersionName) {
        TreeListResultModel<List<RepositoryWebSiteSelectTree>> resultModel = new TreeListResultModel<List<RepositoryWebSiteSelectTree>>();
        resultModel = TreePageUtil.getDefaultTreeList(Arrays.asList(projectVersionName), RepositoryWebSiteSelectTree.class);
        return resultModel;

    }


    @MethodChinaName(cname = "添加JAVA模板")
    @RequestMapping(value = "AddTempFromJaveTemp")
    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-c-flash", caption = "添加JAVA模板")
    @DialogAnnotation(width = "300",
            height = "450")
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<RepositoryTempPopTree>> addTempFromJaveTemp(String projectVersionName, RepositoryType repositoryType) {

        TreeListResultModel<List<RepositoryTempPopTree>> result = new TreeListResultModel<List<RepositoryTempPopTree>>();
        try {
            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            Set<String> javaTempIds = repositoryInst.getJavaTempIds();
            List<String> ids = new ArrayList<>(Arrays.asList(javaTempIds.toArray(new String[]{})));
            if (repositoryType != null) {
                result = TreePageUtil.getTreeList(Arrays.asList(repositoryType), RepositoryTempPopTree.class, ids);
            } else {
                RepositoryType[] repositoryTypes = RepositoryType.values();
                result = TreePageUtil.getTreeList(Arrays.asList(repositoryTypes), RepositoryTempPopTree.class, ids);
            }


        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


}

