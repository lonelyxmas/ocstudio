package com.ds.dsm.manager.temp.tree;

import com.ds.common.JDSException;
import com.ds.enums.IconEnumstype;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.*;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.ViewType;

import java.util.List;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, caption = "JAVA模板")
public class DomainTempTree<T extends DomainTempTree> extends TreeListItem<T> {
    String javaPackage = "dsm.manager.temp.";

    public DomainTempTree(String domainId, boolean hasTemp) throws JDSException {
        this.caption = "模板分类";
        this.id = "ViewRoot";
        this.euClassName = javaPackage + "CustomTempGrid";
        this.iniFold = false;
        this.imageClass = "spafont spa-icon-settingprj";
        DSMType[] dsmTypes = DSMType.values();
        for (DSMType dsmType : dsmTypes) {
            if (hasTemp && !dsmType.equals(DSMType.aggregation)) {
                List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                if (javaTemps.size() > 0) {
                    DomainTempTree dsmInstItem = new DomainTempTree(dsmType, domainId, hasTemp);
                    this.addChild((T) dsmInstItem);
                }
            } else {
                this.addChild((T) new DomainTempTree(dsmType, domainId, hasTemp));
            }
        }
    }

    public DomainTempTree(DSMTempType dsmTempType, String domainId, boolean hasTemp) throws JDSException {
        this.caption = dsmTempType.getName();
        this.id = dsmTempType.getType();
        this.iniFold = false;
        this.euClassName = javaPackage + "CustomTempGrid";

        this.addTagVar("dsmTempType", dsmTempType.getType());
        this.addTagVar("domainId", domainId);

        this.setImageClass(dsmTempType.getImageClass());
        DSMType[] dsmTypes = DSMType.values();
        for (DSMType dsmType : dsmTypes) {
            if (hasTemp && !dsmType.equals(DSMType.aggregation)) {
                List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                if (javaTemps.size() > 0) {
                    DomainTempTree dsmInstItem = new DomainTempTree(dsmType, domainId, hasTemp);
                    dsmInstItem.setIniFold(true);
                    this.addChild((T) dsmInstItem);
                }
            } else {
                this.addChild((T) new DomainTempTree(dsmType, domainId, hasTemp));
            }
        }
    }


    public DomainTempTree(ViewType viewType, String viewInstId, String domainId, boolean hasTemp) throws JDSException {
        this.caption = viewType.getName();
        this.imageClass = viewType.getImageClass();
        this.id = DSMType.aggregation.getType() + "_" + viewType.getType();

        this.euClassName = javaPackage + "ViewTempGallery";
        this.addTagVar("dsmType", DSMType.view.getType());
        this.addTagVar("viewType", viewType.getType());
        this.addTagVar("domainId", domainId);
        this.addTagVar("viewInstId", viewInstId);
        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getViewTemps(viewType);
            for (JavaTemp javaTemp : javaTemps) {
                this.addChild((T) new DomainTempTree(javaTemp, domainId));
            }
        }


    }

    public DomainTempTree(RepositoryType repositoryType, String projectVersionName, boolean hasTemp) throws JDSException {
        this.caption = repositoryType.getName();
        this.imageClass = repositoryType.getImageClass();
        this.id = DSMType.repository.getType() + "_" + repositoryType.getType();
        this.euClassName = javaPackage + "RepositoryTempGrid";
        this.addTagVar("dsmType", DSMType.repository.getType());
        this.addTagVar("repositoryType", repositoryType.getType());
        this.addTagVar("projectVersionName", projectVersionName);
        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(repositoryType);
            for (JavaTemp javaTemp : javaTemps) {
                this.addChild((T) new DomainTempTree(javaTemp, projectVersionName));
            }
        }


    }

    public DomainTempTree(DSMType dsmType, String domainId, String projectVersionName, boolean hasTemp) throws JDSException {
        this.caption = dsmType.getName();
        this.imageClass = dsmType.getImageClass();
        this.euClassName = javaPackage + "CustomTempGrid";
        this.iniFold = false;
        this.id = dsmType.getType();
        this.addTagVar("dsmType", dsmType.getType());
        this.addTagVar("domainId", domainId);

        switch (dsmType) {
            case aggregation:
                AggregationType[] aggregationTypes = AggregationType.values();
                for (AggregationType type : aggregationTypes) {
                    DomainTempTree dsmInstItem = new DomainTempTree(type, domainId, hasTemp);
                    this.addChild((T) dsmInstItem);
                    dsmInstItem.setIniFold(true);
                }
                break;
            case repository:
                RepositoryType[] repositoryTypes = RepositoryType.values();
                for (RepositoryType type : repositoryTypes) {
                    if (hasTemp) {
                        List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(type);
                        if (javaTemps.size() > 0) {
                            DomainTempTree dsmInstItem = new DomainTempTree(type, projectVersionName, hasTemp);
                            dsmInstItem.setIniFold(true);
                            this.addChild((T) dsmInstItem);
                        }
                    } else {
                        this.addChild((T) new DomainTempTree(type, projectVersionName, hasTemp));
                    }
                }
                break;

            case customDomain:
                CustomDomainType[] customDomainTypes = CustomDomainType.values();
                for (CustomDomainType customDomainType : customDomainTypes) {
                    if (hasTemp) {
                        List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(customDomainType);
                        if (javaTemps.size() > 0) {
                            DomainTempTree dsmInstItem = new DomainTempTree(customDomainType, domainId, hasTemp);
                            //dsmInstItem.setIniFold(true);
                            this.addChild((T) dsmInstItem);
                        }
                    } else {
                        this.addChild((T) new DomainTempTree(customDomainType, domainId, hasTemp));
                    }
                }
                break;
            default:
                if (hasTemp) {
                    List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                    for (JavaTemp javaTemp : javaTemps) {
                        this.addChild((T) new DomainTempTree(javaTemp, domainId));
                    }
                }

        }


    }

    public DomainTempTree(CustomDomainType customDomainType, String domainId, boolean hasTemp) throws JDSException {

        this.caption = customDomainType.getName();
        this.imageClass = customDomainType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + customDomainType.getType();
        this.euClassName = "dsm.admin.temp.DaoTempGallery";
        this.addTagVar("dsmType", DSMType.customDomain.getType());
        this.addTagVar("customDomainType", customDomainType.getType());

        switch (customDomainType) {
            case bpm:
                BpmDomainType[] types = BpmDomainType.values();
                for (BpmDomainType type : types) {
                    this.addChild((T) new DomainTempTree(type, domainId, hasTemp));
                }
                break;
            case msg:
                MsgDomainType[] msgtypes = MsgDomainType.values();
                for (MsgDomainType type : msgtypes) {
                    this.addChild((T) new DomainTempTree(type, domainId, hasTemp));
                }
                break;
            case org:
                OrgDomainType[] orgtypes = OrgDomainType.values();
                for (OrgDomainType type : orgtypes) {
                    this.addChild((T) new DomainTempTree(type, domainId, hasTemp));
                }
                break;
            case nav:
                NavDomainType[] navtypes = NavDomainType.values();
                for (NavDomainType type : navtypes) {
                    this.addChild((T) new DomainTempTree(type, domainId, hasTemp));
                }
                break;
        }

    }


    public DomainTempTree(AggregationType aggregationType, String domainId, boolean hasTemp) throws JDSException {
        this.caption = aggregationType.getName();
        this.imageClass = aggregationType.getImageClass();
        this.id = DSMType.aggregation.getType() + "_" + aggregationType.getType();
        this.euClassName = javaPackage + "AggTempGrid";
        this.iniFold = false;
        this.addTagVar("dsmType", DSMType.aggregation.getType());
        this.addTagVar("aggregationType", aggregationType.getType());
    }


    public DomainTempTree(IconEnumstype customDomainType, String dsmId, boolean hasTemp) throws JDSException {
        this.caption = customDomainType.getName();
        this.imageClass = customDomainType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + customDomainType.getType();
        this.euClassName = javaPackage + "AggTempGrid";
        this.addTagVar("dsmType", DSMType.customDomain.getType());
        this.addTagVar("customDomainType", customDomainType.getType());
    }


    public DomainTempTree(JavaTemp javaTemp, String domainId) {
        this.caption = javaTemp.getName();
        this.imageClass = "spafont spa-icon-page";
        this.id = javaTemp.getJavaTempId();
        if (javaTemp.getViewType() != null) {
            this.addTagVar("viewType", javaTemp.getViewType().getType());
        }
        if (javaTemp.getAggregationType() != null) {
            this.addTagVar("aggregationType", javaTemp.getAggregationType().getType());
        }

        if (javaTemp.getRepositoryType() != null) {
            this.addTagVar("repositoryType", javaTemp.getRepositoryType().getType());
        }
        if (javaTemp.getCustomDomainType() != null) {
            this.addTagVar("customDomainType", javaTemp.getCustomDomainType().getType());
        }
        if (javaTemp.getDsmType() != null) {
            this.addTagVar("dsmType", javaTemp.getDsmType().getType());
        }

        this.addTagVar("domainId", domainId);
        this.addTagVar("javaTempId", javaTemp.getJavaTempId());
    }

}
