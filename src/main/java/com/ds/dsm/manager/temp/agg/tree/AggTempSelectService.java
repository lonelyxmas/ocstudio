package com.ds.dsm.manager.temp.agg.tree;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/agg/temp/tree/")
@MethodChinaName(cname = "模板选择")

public class AggTempSelectService {


    @RequestMapping(method = RequestMethod.POST, value = "loadTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeReload)
    @ResponseBody
    public TreeListResultModel<List<AggTempPopTree>> loadTree(String projectName, String id, String euPackageName, String parentId) {
        TreeListResultModel<List<AggTempPopTree>> result = new TreeListResultModel<>();
        AggregationType[] aggregationTypes = AggregationType.values();
        result = TreePageUtil.getTreeList(Arrays.asList(aggregationTypes), AggTempPopTree.class);
        return result;
    }


    @MethodChinaName(cname = "从模板添加")
    @RequestMapping(method = RequestMethod.POST, value = "addTempFromJava")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.treeSave)
    public @ResponseBody
    ResultModel<Boolean> addTempFromJava(String AddTempFromJaveTempTree, String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (AddTempFromJaveTempTree == null) {
                throw new JDSException("javaTempId is bull");
            }
            if (AddTempFromJaveTempTree != null) {
                String[] tempIds = StringUtility.split(AddTempFromJaveTempTree, ";");
                if (tempIds.length > 0) {
                    Set<String> javaTempIds = new HashSet<>();
                    for (String id : tempIds) {
                        JavaTemp tempBean = DSMFactory.getInstance().getTempManager().getJavaTempById(id);
                        if (tempBean != null) {
                            javaTempIds.add(id);
                        }
                    }
                    DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                    domainInst.setJavaTempIds(javaTempIds);
                    DSMFactory.getInstance().getAggregationManager().updateDomainInst(domainInst, true);
                }
            }

        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }
}
