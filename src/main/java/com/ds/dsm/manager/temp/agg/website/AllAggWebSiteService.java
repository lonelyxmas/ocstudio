package com.ds.dsm.manager.temp.agg.website;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GalleryViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(path = "/dsm/agg/temp/website/")
@MethodChinaName(cname = "模板选择")
public class AllAggWebSiteService<T extends AllAggWebSiteService> extends TreeListItem<T> {


    @MethodChinaName(cname = "本地模板管理")
    @RequestMapping(method = RequestMethod.POST, value = "AllLocalWebSiteList")
    @ModuleAnnotation(caption = "本地模板管理")
    @GalleryViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggWebSiteSelectTree>> getAllLocalWebSiteList(String domainId) {
        DSMTempType[] dsmTypes = DSMTempType.values();
        TreeListResultModel treeListResultModel = TreePageUtil.getTreeList(Arrays.asList(dsmTypes), AggWebSiteSelectTree.class);
        return treeListResultModel;
    }




    @MethodChinaName(cname = "本地模板管理")
    @RequestMapping(method = RequestMethod.POST, value = "AllWebSiteGallery")
    @ModuleAnnotation(caption = "本地模板管理")
    @GalleryViewAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<AggWebSiteSelectGallery>> getLocalWebSiteList(String domainId) {
        ListResultModel<List<AggWebSiteSelectGallery>> result = new ListResultModel();
        List<DSMBean> dsmBeans = new ArrayList<>();
        try {
            List<DSMBean> tempBeans = DSMFactory.getInstance().getTempManager().getDSMBeanList();
            result = PageUtil.getDefaultPageList(tempBeans, AggWebSiteSelectGallery.class);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


}
