package com.ds.dsm.manager.temp.repository.tree;

import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

import java.util.HashMap;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, iniFold = false, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = RepositoryTempSelectService.class)
@PopTreeAnnotation(caption = "JAVA模板")
public class RepositoryTempPopTree<T extends RepositoryTempPopTree> extends TreeListItem {


    @Pid
    String projectVersionName;
    @Pid
    RepositoryType repositoryType;
    @Pid
    String javaTempId;

    public RepositoryTempPopTree(String projectVersionName) {
        this.caption = DSMType.repository.getName();
        this.imageClass = DSMType.repository.getImageClass();
        this.iniFold = false;
        this.id = DSMType.repository.getType();
        this.projectVersionName = projectVersionName;

    }

    @TreeItemAnnotation(bindService = RepositoryTempTreeService.class, iniFold = false)
    public RepositoryTempPopTree(RepositoryType repositoryType, String projectVersionName) {
        this.caption = repositoryType.getName();
        this.imageClass = repositoryType.getImageClass();
        this.id = repositoryType.getType();
        this.tagVar = new HashMap<>();
        this.projectVersionName = projectVersionName;
        this.repositoryType = repositoryType;

    }

    @TreeItemAnnotation()
    public RepositoryTempPopTree(JavaTemp javaTemp, String projectVersionName) {
        this.caption = javaTemp.getName();
        this.imageClass = "spafont spa-icon-page";
        this.id = javaTemp.getJavaTempId();
        this.projectVersionName = projectVersionName;
        this.javaTempId = javaTemp.getJavaTempId();
        this.tagVar = new HashMap<>();
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public RepositoryType getRepositoryType() {
        return repositoryType;
    }

    public void setRepositoryType(RepositoryType repositoryType) {
        this.repositoryType = repositoryType;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }
}
