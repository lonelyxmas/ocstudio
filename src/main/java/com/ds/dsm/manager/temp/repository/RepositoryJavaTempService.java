package com.ds.dsm.manager.temp.repository;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.dsm.repository.config.EntityConfig;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/repository/temp/")
@MethodChinaName(cname = "领域模板", imageClass = "spafont spa-icon-c-cssbox")

public class RepositoryJavaTempService {

    @RequestMapping(method = RequestMethod.POST, value = "RepositoryTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "RepositoryTempGrid")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<RepositoryTempGrid>> getAggTempGrid(String projectVersionName, RepositoryType repositoryType) {
        ListResultModel<List<RepositoryTempGrid>> resultModel = new ListResultModel();
        RepositoryInst bean = null;
        try {
            bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null) {
                        if (javaTemp.getRepositoryType() != null && javaTemp.getRepositoryType().equals(repositoryType)) {
                            temps.add(javaTemp);
                        }
                    }
                }
                resultModel = PageUtil.getDefaultPageList(temps, RepositoryTempGrid.class);
            }
        } catch (JDSException e) {
            resultModel = new ErrorListResultModel<>();
            ((ErrorListResultModel) resultModel).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) resultModel).setErrdes(e.getMessage());
        }

        return resultModel;
    }


    @MethodChinaName(cname = "移除JAVA模板")
    @RequestMapping(method = RequestMethod.POST, value = "removeJavaTemp")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> removeJavaTemp(String javaTempId, String id, String projectVersionName, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (javaTempId == null && id == null) {
                throw new JDSException("javaTempId is bull");
            }
            if (javaTempId == null) {
                javaTempId = id;
            }
            String[] tempIds = StringUtility.split(javaTempId, ";");
            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            if (sourceClassName != null && !sourceClassName.equals("")) {
                EntityConfig entityConfig = DSMFactory.getInstance().getRepositoryManager().getEntityConfig(sourceClassName,  true);
                for (String tempId : tempIds) {
                    entityConfig.getJavaTempIds().remove(tempId);
                }
                DSMFactory.getInstance().getRepositoryManager().updateEntityConfig(entityConfig);
            } else {
                for (String tempId : tempIds) {
                    repositoryInst.getJavaTempIds().remove(tempId);
                }
                DSMFactory.getInstance().getRepositoryManager().updateRepositoryInst(repositoryInst, true);
            }


        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "RepositoryTempInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @ModuleAnnotation( caption = "仓库模板信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    public @ResponseBody
    ResultModel<RepositoryJavaTempForm> getJavaTempInfo(String javaTempId) {
        ResultModel<RepositoryJavaTempForm> result = new ResultModel<RepositoryJavaTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaTempId);
            result.setData(new RepositoryJavaTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<RepositoryJavaTempForm> errorResult = new ErrorResultModel<RepositoryJavaTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }


        return result;
    }

}
