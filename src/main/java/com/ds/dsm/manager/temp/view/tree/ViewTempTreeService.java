package com.ds.dsm.manager.temp.view.tree;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.ViewType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/temp/tree/")
@MethodChinaName(cname = "模板选择")

public class ViewTempTreeService {

    @RequestMapping(method = RequestMethod.POST, value = "loadChild")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewTempPopTree>> loadChild(ViewType viewType, String domainId) {
        TreeListResultModel<List<ViewTempPopTree>> result = new TreeListResultModel<>();
        try {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getViewTemps(viewType);
            result = TreePageUtil.getTreeList(javaTemps, ViewTempPopTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }
}
