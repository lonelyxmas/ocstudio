package com.ds.dsm.manager.temp.tree;

import com.ds.common.JDSException;
import com.ds.dsm.aggregation.AggregationTree;
import com.ds.dsm.aggregation.CustomDomainTree;
import com.ds.dsm.manager.repository.SelectRepositoryPopTree;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.ViewType;

import java.util.HashMap;
import java.util.List;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, iniFold = false, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@PopTreeAnnotation(caption = "JAVA模板")
public class DSMInstPopTree<T extends DSMInstPopTree> extends TreeListItem {


    public DSMInstPopTree(String className, boolean hasTemp) throws JDSException {
        this.caption = "模板分类";
        this.id = "ViewRoot";
        //  this.iniFold = false;
        this.imageClass = "spafont spa-icon-settingprj";
        DSMType[] dsmTypes = DSMType.values();

        for (DSMType dsmType : dsmTypes) {
            if (hasTemp) {
                List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                if (javaTemps.size() > 0) {
                    DSMInstPopTree dsmInstItem = new DSMInstPopTree(dsmType, className, null, null, null, hasTemp);
                    dsmInstItem.setIniFold(true);
                    this.addChild(dsmInstItem);
                }
            } else {
                this.addChild(new DSMInstPopTree(dsmType, className, null, null, null, hasTemp));
            }
        }
    }

    public DSMInstPopTree(String className, String projectVersionName, String domainId, String viewInstId, boolean hasTemp) throws JDSException {
        this.caption = "模板分类";
        this.id = "ViewRoot";
        //  this.iniFold = false;
        this.imageClass = "spafont spa-icon-settingprj";
        DSMType[] dsmTypes = DSMType.values();

        for (DSMType dsmType : dsmTypes) {
            if (hasTemp) {
                List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                if (javaTemps.size() > 0) {
                    DSMInstPopTree dsmInstItem = new DSMInstPopTree(dsmType, className, projectVersionName, domainId, viewInstId, hasTemp);
                    dsmInstItem.setIniFold(true);
                    this.addChild(dsmInstItem);
                }
            } else {
                this.addChild(new DSMInstPopTree(dsmType, className, projectVersionName, domainId, viewInstId, hasTemp));
            }
        }
    }

    public DSMInstPopTree(DSMTempType dsmTempType, String className, String projectVersionName, String domainId, String viewInstId, boolean hasTemp) throws JDSException {
        this.caption = dsmTempType.getName();
        this.id = dsmTempType.getType();
        // this.iniFold = false;
        this.setImageClass(dsmTempType.getImageClass());
        DSMType[] dsmTypes = DSMType.values();
        for (DSMType dsmType : dsmTypes) {
            if (hasTemp) {
                List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                if (javaTemps.size() > 0) {
                    DSMInstPopTree dsmInstItem = new DSMInstPopTree(dsmType, className, projectVersionName, domainId, viewInstId, hasTemp);
                    dsmInstItem.setIniFold(true);
                    this.addChild(dsmInstItem);
                }
            } else {
                this.addChild(new DSMInstPopTree(dsmType, className, projectVersionName, domainId, viewInstId, hasTemp));
            }
        }
    }


    public DSMInstPopTree(AggregationType aggregationType, String euClassName, String domainId, boolean hasTemp) throws JDSException {
        this.caption = aggregationType.getName();
        this.imageClass = aggregationType.getImageClass();
        this.euClassName = euClassName;
        this.id = aggregationType.getType();
        this.tagVar = new HashMap<>();
        tagVar.put("dsmType", DSMType.aggregation.getType());
        tagVar.put("aggregationType", aggregationType.getType());
        tagVar.put("domainId", domainId);
        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getAggregationTemps(aggregationType);
            for (JavaTemp javaTemp : javaTemps) {
                this.addChild(new DSMInstPopTree(javaTemp, euClassName, null, domainId, null));
            }
        }
    }


    public DSMInstPopTree(ViewType viewType, String euClassName, String domainId, String viewInstId, boolean hasTemp) throws JDSException {
        this.caption = viewType.getName();
        this.imageClass = viewType.getImageClass();
        this.euClassName = euClassName;
        this.id = viewType.getType();

        this.tagVar = new HashMap<>();
        tagVar.put("dsmType", DSMType.view.getType());
        tagVar.put("viewType", viewType.getType());
        tagVar.put("viewInstId", viewInstId);
        tagVar.put("domainId", domainId);
        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getViewTemps(viewType);
            for (JavaTemp javaTemp : javaTemps) {
                this.addChild(new DSMInstPopTree(javaTemp, euClassName, null, domainId, viewInstId));
            }
        }
    }

    public DSMInstPopTree(RepositoryType repositoryType, String euClassName, String projectVersionName, boolean hasTemp) throws JDSException {


        this.caption = repositoryType.getName();
        this.imageClass = repositoryType.getImageClass();
        this.euClassName = euClassName;
        this.id = repositoryType.getType();

        this.tagVar = new HashMap<>();
        tagVar.put("dsmType", DSMType.repository.getType());
        tagVar.put("repositoryType", repositoryType.getType());
        tagVar.put("projectVersionName", projectVersionName);
        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(repositoryType);
            for (JavaTemp javaTemp : javaTemps) {
                this.addChild(new DSMInstPopTree(javaTemp, euClassName, projectVersionName, null, null));
            }
        }


    }


    public DSMInstPopTree(DSMType dsmType, String projectVersionName, String domainId, String viewInstId, String euClassName, boolean hasTemp) throws JDSException {
        this.caption = dsmType.getName();
        this.imageClass = dsmType.getImageClass();
        this.euClassName = euClassName;
        this.id = dsmType.getType();
        this.tagVar = new HashMap<>();
        tagVar.put("dsmType", dsmType.getType());
        tagVar.put("projectVersionName", projectVersionName);
        tagVar.put("domainId", domainId);
        tagVar.put("viewInstId", viewInstId);

        switch (dsmType) {
            case repository:
                RepositoryType[] repositoryTypes = RepositoryType.values();
                for (RepositoryType type : repositoryTypes) {
                    if (hasTemp) {
                        List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(type);
                        if (javaTemps.size() > 0) {
                            SelectRepositoryPopTree dsmInstItem = new SelectRepositoryPopTree(type, projectVersionName, euClassName, hasTemp);
                            dsmInstItem.setIniFold(true);
                            this.addChild(dsmInstItem);
                        }
                    } else {
                        this.addChild(new SelectRepositoryPopTree(type, projectVersionName, euClassName, hasTemp));
                    }
                }
                break;
            case aggregation:
                AggregationType[] aggregationTypes = AggregationType.values();
                for (AggregationType type : aggregationTypes) {
                    if (hasTemp) {
                        List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getAggregationTemps(type);
                        if (javaTemps.size() > 0) {
                            AggregationTree dsmInstItem = new AggregationTree(type, domainId, euClassName, hasTemp);
                            dsmInstItem.setIniFold(true);
                            this.addChild(dsmInstItem);
                        }
                    } else {
                        this.addChild(new AggregationTree(type, domainId, euClassName, hasTemp));
                    }
                }
                break;
            case customDomain:
                CustomDomainType[] customDomainTypes = CustomDomainType.values();
                for (CustomDomainType customDomainType : customDomainTypes) {
                    if (hasTemp) {
                        List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(customDomainType);
                        if (javaTemps.size() > 0) {
                            CustomDomainTree dsmInstItem = new CustomDomainTree(customDomainType, domainId, euClassName, hasTemp);
                            dsmInstItem.setIniFold(true);
                            this.addChild(dsmInstItem);
                        }
                    } else {
                        this.addChild(new CustomDomainTree(customDomainType, domainId, euClassName, hasTemp));
                    }
                }
                break;

            default:
                if (hasTemp) {
                    List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                    for (JavaTemp javaTemp : javaTemps) {
                        this.addChild(new DSMInstPopTree(javaTemp, euClassName, projectVersionName, domainId, viewInstId));
                    }
                }
        }

    }


    public DSMInstPopTree(JavaTemp javaTemp, String euClassName, String projectVersionName, String domainId, String viewInstId) {
        this.caption = javaTemp.getName();
        this.euClassName = euClassName;
        this.imageClass = "spafont spa-icon-page";
        this.id = javaTemp.getJavaTempId();
        this.tagVar = new HashMap<>();
        if (javaTemp.getViewType() != null) {
            tagVar.put("viewType", javaTemp.getViewType().getType());
        }

        if (javaTemp.getRepositoryType() != null) {
            tagVar.put("repositoryType", javaTemp.getRepositoryType());
        }

        if (javaTemp.getDsmType() == null) {
            tagVar.put("dsmType", javaTemp.getDsmType().getType());
        }

        tagVar.put("projectVersionName", projectVersionName);
        tagVar.put("domainId", domainId);
        tagVar.put("viewInstId", viewInstId);
        tagVar.put("javaTempId", javaTemp.getJavaTempId());
    }

}
