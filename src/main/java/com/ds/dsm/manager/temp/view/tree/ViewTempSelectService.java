package com.ds.dsm.manager.temp.view.tree;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.ViewType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/view/temp/tree/")
@MethodChinaName(cname = "模板选择")

public class ViewTempSelectService {


    @RequestMapping(method = RequestMethod.POST, value = "loadTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeReload)
    @ResponseBody
    public TreeListResultModel<List<ViewTempPopTree>> loadTree(String id, String viewInstId) {
        TreeListResultModel<List<ViewTempPopTree>> result = new TreeListResultModel<>();
        ViewType[] viewTypes = ViewType.values();
        result = TreePageUtil.getTreeList(Arrays.asList(viewTypes), ViewTempPopTree.class);
        return result;
    }


    @MethodChinaName(cname = "从模板添加")
    @RequestMapping(method = RequestMethod.POST, value = "addViewTempFromJava")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.treeSave)
    public @ResponseBody
    ResultModel<Boolean> addAggTempFromJava(String AddViewTempFromJaveTempTree, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (AddViewTempFromJaveTempTree == null) {
                throw new JDSException("javaTempId is bull");
            }
            if (AddViewTempFromJaveTempTree != null) {
                String[] tempIds = StringUtility.split(AddViewTempFromJaveTempTree, ";");
                if (tempIds.length > 0) {
                    Set<String> javaTempIds = new HashSet<>();
                    for (String id : tempIds) {
                        JavaTemp tempBean = DSMFactory.getInstance().getTempManager().getJavaTempById(id);
                        if (tempBean != null) {
                            javaTempIds.add(id);
                        }
                    }
                    ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
                    viewInst.getJavaTempIds().clear();
                    viewInst.getJavaTempIds().addAll(javaTempIds);
                    DSMFactory.getInstance().getViewManager().updateViewInst(viewInst, true);
                }
            }

        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }
}
