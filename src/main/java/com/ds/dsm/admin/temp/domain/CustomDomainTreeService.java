package com.ds.dsm.admin.temp.domain;


import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.enums.IconEnumstype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/admin/temp/customdomain/")
@MethodChinaName(cname = "资源库模板", imageClass = "spafont spa-icon-conf")
public class CustomDomainTreeService {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;


    @RequestMapping(method = RequestMethod.POST, value = "AllCustomDomainTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AllRepositoryTempGrid")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<DomainTempGrid>> getAllCustomDomainTempGrid(IconEnumstype itemDomainType) {
        ListResultModel<List<DomainTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(itemDomainType);
            resultModel = PageUtil.getDefaultPageList(temps, DomainTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }
}
