package com.ds.dsm.admin.temp.domain;


import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.enums.IconEnumstype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.*;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/admin/temp/domain/")
@MethodChinaName(cname = "通用领域", imageClass = "spafont spa-icon-conf")

public class DomainService {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;


    @RequestMapping(method = RequestMethod.POST, value = "AllTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AllTemp")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<DomainTempGrid>> getAllTempGrid() {
        ListResultModel<List<DomainTempGrid>> resultModel = new ListResultModel();
        try {

            List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(DSMType.customDomain);
            resultModel = PageUtil.getDefaultPageList(temps, DomainTempGrid.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "TempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "TempGrid")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<DomainTempGrid>> getTempGrid(CustomDomainType domainType) {
        ListResultModel<List<DomainTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> beans = new ArrayList<>();
            if (domainType != null) {
                beans = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(domainType);
            }
            resultModel = PageUtil.getDefaultPageList(beans, DomainTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "编辑模板信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateJavaTemp")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateJavaTemp(@RequestBody JavaTemp tempInfo) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            DSMFactory.getInstance().getTempManager().updateJavaTemp(tempInfo);

        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "编辑模板信息")
    @RequestMapping(method = RequestMethod.POST, value = "deleteJavaTemp")
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent}, bindMenu = {CustomMenuItem.delete})
    public @ResponseBody
    ResultModel<Boolean> deleteJavaTemp(String javaTempId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (javaTempId == null) {
                throw new JDSException("javaTempId is bull");
            }
            DSMFactory.getInstance().getTempManager().deleteJavaTemp(javaTempId);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "新建JAVA模板")
    @RequestMapping(value = {"CreateJavaTemp"}, method = {RequestMethod.GET, RequestMethod.POST})
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @ModuleAnnotation( caption = "新建JAVA模板")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public ResultModel<DomainTempForm> createJavaTemp(String javaTempId) {
        ResultModel<DomainTempForm> result = new ResultModel<DomainTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().createJavaTemp();
            result.setData(new DomainTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<DomainTempForm> errorResult = new ErrorResultModel<DomainTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    ;


    @RequestMapping(method = RequestMethod.POST, value = "TempFileInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @ModuleAnnotation(caption = "模板信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    public @ResponseBody
    ResultModel<DomainTempForm> getJavaTempInfo(String javaTempId) {
        ResultModel<DomainTempForm> result = new ResultModel<DomainTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaTempId);
            result.setData(new DomainTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<DomainTempForm> errorResult = new ErrorResultModel<DomainTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }


        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadChildCustomDomain")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> loadChildCustomDomain(CustomDomainType customDomainType) {

        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel();
        List<IconEnumstype> domainTypes = new ArrayList<>();
        switch (customDomainType) {
            case bpm:
                BpmDomainType[] types = BpmDomainType.values();
                domainTypes.addAll(Arrays.asList(types));
                break;
            case msg:
                MsgDomainType[] msgtypes = MsgDomainType.values();
                domainTypes.addAll(Arrays.asList(msgtypes));
                break;
            case org:
                OrgDomainType[] orgtypes = OrgDomainType.values();
                domainTypes.addAll(Arrays.asList(orgtypes));
                break;
            case nav:
                NavDomainType[] navtypes = NavDomainType.values();
                domainTypes.addAll(Arrays.asList(navtypes));
                break;
            case vfs:
                VfsDomainType[] vfstypes = VfsDomainType.values();
                domainTypes.addAll(Arrays.asList(vfstypes));
                break;
        }

        resultModel = TreePageUtil.getTreeList(domainTypes, JavaTempNavTree.class);
        return resultModel;
    }

    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }
}
