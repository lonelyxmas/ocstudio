package com.ds.dsm.admin.temp;

import com.ds.dsm.admin.temp.aggreagtion.AggregationService;
import com.ds.dsm.admin.temp.domain.CustomDomainTreeService;
import com.ds.dsm.admin.temp.domain.DomainService;
import com.ds.dsm.admin.temp.repository.RepositoryAdminService;
import com.ds.dsm.admin.temp.view.ViewGroupTreeService;
import com.ds.dsm.admin.temp.view.ViewTempService;
import com.ds.enums.IconEnumstype;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.domain.enums.*;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.ViewGroupType;
import com.ds.web.annotation.ViewType;

@TabsAnnotation(singleOpen = true, closeBtn = true)
@TreeAnnotation(heplBar = true, caption = "JAVA模板导航页")
public class JavaTempNavTree extends TreeListItem {


    @Pid
    DSMType dsmType;
    @Pid
    CustomDomainType customDomainType;

    @Pid
    AggregationType aggregationType;

    @Pid
    RepositoryType repositoryType;

    @Pid
    IconEnumstype itemDomainType;

    @Pid
    ViewType viewType;

    @Pid
    ViewGroupType viewGroupType;


    public JavaTempNavTree(String id, String caption, String imageClass) {
        this.id = id;
        this.caption = caption;
        this.imageClass = imageClass;
    }

    @TreeItemAnnotation(bindService = AdminTempService.class)
    public JavaTempNavTree(DSMType dsmType) {
        this.caption = dsmType.getName();
        this.id = dsmType.getType();
        this.imageClass = dsmType.getImageClass();
        this.dsmType = dsmType;
    }

    @TreeItemAnnotation(bindService = AggregationService.class)
    public JavaTempNavTree(AggregationType aggregationType) {
        this.caption = aggregationType.getName();
        this.imageClass = aggregationType.getImageClass();
        this.id = DSMType.aggregation.getType() + "_" + aggregationType.getType();
        this.aggregationType = aggregationType;
        this.dsmType = DSMType.aggregation;

    }

    @TreeItemAnnotation(bindService = RepositoryAdminService.class)
    public JavaTempNavTree(RepositoryType repositoryType) {
        this.caption = repositoryType.getName();
        this.imageClass = repositoryType.getImageClass();
        this.id = DSMType.repository.getType() + "_" + repositoryType.getType();
        this.repositoryType = repositoryType;
        this.dsmType = DSMType.repository;
    }

    @TreeItemAnnotation(bindService = DomainService.class)
    public JavaTempNavTree(CustomDomainType customDomainType) {
        this.caption = customDomainType.getName();
        this.imageClass = customDomainType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + customDomainType.getType();
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = ViewTempService.class)
    public JavaTempNavTree(ViewType viewType, ViewGroupType viewGroupType) {
        this.caption = viewType.getName();
        this.imageClass = viewType.getImageClass();
        if (viewGroupType != null) {
            this.id = DSMType.view.getType() + "_" + viewGroupType + "_" + viewType.getType();
        } else {
            this.id = DSMType.view.getType() + "_" + viewType.getType();
        }

        this.viewType = viewType;

    }

    @TreeItemAnnotation(bindService = CustomDomainTreeService.class, dynDestory = true, lazyLoad = true)
    public JavaTempNavTree(NavDomainType itemDomainType, CustomDomainType customDomainType) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType;
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = CustomDomainTreeService.class, dynDestory = true, lazyLoad = true)
    public JavaTempNavTree(BpmDomainType itemDomainType, CustomDomainType customDomainType) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType;
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = CustomDomainTreeService.class, dynDestory = true, lazyLoad = true)
    public JavaTempNavTree(OrgDomainType itemDomainType, CustomDomainType customDomainType) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType;
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = CustomDomainTreeService.class, dynDestory = true, lazyLoad = true)
    public JavaTempNavTree(MsgDomainType itemDomainType, CustomDomainType customDomainType) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType;
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = CustomDomainTreeService.class, dynDestory = true, lazyLoad = true)
    public JavaTempNavTree(VfsDomainType itemDomainType, CustomDomainType customDomainType) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType;
        this.customDomainType = customDomainType;

    }


    @TreeItemAnnotation(bindService = ViewGroupTreeService.class, dynDestory = true, iniFold = true, lazyLoad = true)
    public JavaTempNavTree(ViewGroupType viewGroupType, DSMType dsmType) {
        this.caption = viewGroupType.getName();
        this.imageClass = viewGroupType.getImageClass();
        this.viewGroupType = viewGroupType;
        this.dsmType = dsmType;
        this.id = dsmType + "_" + viewGroupType.getType();
    }

    public ViewGroupType getViewGroupType() {
        return viewGroupType;
    }

    public void setViewGroupType(ViewGroupType viewGroupType) {
        this.viewGroupType = viewGroupType;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public CustomDomainType getCustomDomainType() {
        return customDomainType;
    }

    public void setCustomDomainType(CustomDomainType customDomainType) {
        this.customDomainType = customDomainType;
    }

    public AggregationType getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(AggregationType aggregationType) {
        this.aggregationType = aggregationType;
    }

    public RepositoryType getRepositoryType() {
        return repositoryType;
    }

    public void setRepositoryType(RepositoryType repositoryType) {
        this.repositoryType = repositoryType;
    }

    public IconEnumstype getItemDomainType() {
        return itemDomainType;
    }

    public void setItemDomainType(IconEnumstype itemDomainType) {
        this.itemDomainType = itemDomainType;
    }

    public ViewType getViewType() {
        return viewType;
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }
}
