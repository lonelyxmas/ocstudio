package com.ds.dsm.admin.temp.view;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.ImageAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.annotation.Required;
import com.ds.web.annotation.ViewType;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {ViewTempService.class}, event = CustomGridEvent.editor)
public class ViewTempGrid {

    @CustomAnnotation(uid = true, hidden = true)
    String javaTempId;

    @CustomAnnotation(pid = true, hidden = true)
    String fileId;

    @Required
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String name;

    @CustomAnnotation(caption = "视图类型")
    ViewType viewType;

    @CustomAnnotation(caption = "模板类型", pid = true, hidden = true)
    DSMType dsmType = DSMType.view;

    @CustomAnnotation(caption = "模板说明")
    String desc;

    @CustomAnnotation(caption = "略缩图")
    @ImageAnnotation
    @FieldAnnotation(rowHeight = "100")
    String image = "/RAD/img/project.png";
    @Required
    @CustomAnnotation(caption = "包名规则")
    String packagePostfix;

    @Required
    @CustomAnnotation(caption = "名称规则")
    String namePostfix;

    public ViewTempGrid() {

    }

    public ViewTempGrid(JavaTemp temp) {
        this.viewType = temp.getViewType();
        this.name = temp.getName();
        this.desc = getDesc();
        if (temp.getImage() != null) {
            this.image = temp.getImage();
        }

        this.fileId = temp.getFileId();
        this.javaTempId = temp.getJavaTempId();
        this.dsmType = temp.getDsmType();
        this.namePostfix = temp.getNamePostfix();
        this.packagePostfix = temp.getPackagePostfix();

    }


    public ViewType getViewType() {
        return viewType;
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }


    public String getPackagePostfix() {
        return packagePostfix;
    }

    public void setPackagePostfix(String packagePostfix) {
        this.packagePostfix = packagePostfix;
    }


    public String getNamePostfix() {
        return namePostfix;
    }

    public void setNamePostfix(String namePostfix) {
        this.namePostfix = namePostfix;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
