package com.ds.dsm.admin.temp.domain.nav;


import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempGallery;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.domain.enums.NavDomainType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/admin/temp/customdomain/nav/")
@MethodChinaName(cname = "视图模板", imageClass = "spafont spa-icon-conf")
public class NavTreeService {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;


    @RequestMapping(method = RequestMethod.POST, value = "AllNavTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<JavaTempGallery>> getAllNavTempGrid() {
        ListResultModel<List<JavaTempGallery>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(CustomDomainType.nav);
            resultModel = PageUtil.getDefaultPageList(temps, JavaTempGallery.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadChildNav")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> loadChildNav(String dsmTempId) {
        this.dsmTempId = dsmTempId;
        NavDomainType[] msgDomainTypes = NavDomainType.values();
        TreeListResultModel<List<JavaTempNavTree>> resultModel = TreePageUtil.getTreeList(Arrays.asList(msgDomainTypes), JavaTempNavTree.class);
        return resultModel;
    }


    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }
}
