package com.ds.dsm.admin.temp;


import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RangeType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.annotation.RefType;
import com.ds.web.annotation.ViewType;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {AdminTempService.class}, event = CustomGridEvent.editor)
public class JavaTempGrid {

    @CustomAnnotation(uid = true, hidden = true)
    String javaTempId;

    @CustomAnnotation(pid = true, hidden = true)
    String fileId;

    @FieldAnnotation( required = true)
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String name;

    @CustomAnnotation(caption = "视图类型")
    ViewType viewType;

    @CustomAnnotation(caption = "模板类型")
    DSMType dsmType;

    @FieldAnnotation( required = true)
    @CustomAnnotation(caption = "实体关系")
    RefType refType;

    @FieldAnnotation( required = true)
    @CustomAnnotation(caption = "模板域")
    RangeType rangeType;

    @CustomAnnotation(caption = "模板说明")
    String desc;

    @CustomAnnotation(caption = "略缩图")
    String image = "/RAD/img/project.png";

    @FieldAnnotation( required = true)
    @CustomAnnotation(caption = "包名规则")
    String packagePostfix;

    @FieldAnnotation( required = true)
    @CustomAnnotation(caption = "名称规则")
    String namePostfix;

    public JavaTempGrid() {

    }

    public JavaTempGrid(JavaTemp temp) {
        this.viewType = temp.getViewType();
        this.name = temp.getName();
        this.desc = getDesc();
        if (temp.getImage() != null) {
            this.image = temp.getImage();
        }

        this.fileId = temp.getFileId();
        this.javaTempId = temp.getJavaTempId();

        this.dsmType = temp.getDsmType();
        this.refType = temp.getRefType();
        this.rangeType = temp.getRangeType();
        this.namePostfix = temp.getNamePostfix();
        this.packagePostfix = temp.getPackagePostfix();

    }

    public ViewType getViewType() {
        return viewType;
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }


    public String getPackagePostfix() {
        return packagePostfix;
    }

    public void setPackagePostfix(String packagePostfix) {
        this.packagePostfix = packagePostfix;
    }


    public RefType getRefType() {
        return refType;
    }

    public void setRefType(RefType refType) {
        this.refType = refType;
    }

    public String getNamePostfix() {
        return namePostfix;
    }

    public void setNamePostfix(String namePostfix) {
        this.namePostfix = namePostfix;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public RangeType getRangeType() {
        return rangeType;
    }

    public void setRangeType(RangeType rangeType) {
        this.rangeType = rangeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
