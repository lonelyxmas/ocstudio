package com.ds.dsm.admin.temp;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.Enumstype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.ViewGroupType;
import com.ds.web.annotation.ViewType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = {"/dsm/admin/temp/"})
@MethodChinaName(cname = "模板管理", imageClass = "spafont spa-icon-c-gallery")
public class AdminTempService {
    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;

    @RequestMapping(method = RequestMethod.POST, value = "AllTempGallery")
    @GalleryViewAnnotation
    @ModuleAnnotation(caption = "所有Gallery")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<JavaTempGallery>> getAllTempGallery() {
        ListResultModel<List<JavaTempGallery>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getAllJavaTemps();
            resultModel = PageUtil.getDefaultPageList(temps, JavaTempGallery.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "ViewTempGallery")
    @GalleryViewAnnotation
    @ModuleAnnotation(caption = "视图Gallery")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<JavaTempGallery>> getViewTempGallery(ViewType viewType) {
        ListResultModel<List<JavaTempGallery>> resultModel = new ListResultModel();
        try {
            if (viewType != null) {
                List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getViewTemps(viewType);
                resultModel = PageUtil.getDefaultPageList(temps, JavaTempGallery.class);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "DaoTempGallery")
    @GalleryViewAnnotation
    @ModuleAnnotation(caption = "DAOGallery")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<JavaTempGallery>> getDaoTempGallery(RepositoryType repositoryType) {
        ListResultModel<List<JavaTempGallery>> resultModel = new ListResultModel();
        try {
            if (repositoryType != null) {
                List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(repositoryType);
                resultModel = PageUtil.getDefaultPageList(temps, JavaTempGallery.class);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "CodeTempGallery")
    @GalleryViewAnnotation
    @ModuleAnnotation(caption = "CodeGallery")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<JavaTempGallery>> getCodeTempGallery(DSMType dsmType) {
        ListResultModel<List<JavaTempGallery>> resultModel = new ListResultModel();
        try {
            if (dsmType != null) {
                List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                resultModel = PageUtil.getDefaultPageList(temps, JavaTempGallery.class);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "AllTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AllTemp")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<JavaTempGrid>> getAllTempGrid(DSMType dsmType) {
        ListResultModel<List<JavaTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
            resultModel = PageUtil.getDefaultPageList(temps, JavaTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "CodeTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "CodeTempGrid")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<JavaTempGrid>> getCodeTempGrid(DSMType dsmType) {
        ListResultModel<List<JavaTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> beans = new ArrayList<>();
            if (dsmType != null) {
                beans = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
            }
            resultModel = PageUtil.getDefaultPageList(beans, JavaTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "AggregationTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AggregationTempGrid")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<JavaTempGrid>> getAggregationTempGrid(AggregationType aggregationType) {
        ListResultModel<List<JavaTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> beans = new ArrayList<>();
            if (aggregationType != null) {
                beans = DSMFactory.getInstance().getTempManager().getAggregationTemps(aggregationType);
            }
            resultModel = PageUtil.getDefaultPageList(beans, JavaTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ViewTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<JavaTempGrid>> getViewTempGrid(ViewType viewType) {
        ListResultModel<List<JavaTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> beans = new ArrayList<>();
            if (viewType != null) {
                beans = DSMFactory.getInstance().getTempManager().getViewTemps(viewType);
            }
            resultModel = PageUtil.getDefaultPageList(beans, JavaTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "DaoTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "DaoTempGrid")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<JavaTempGrid>> getDaoTempGrid(RepositoryType repositoryType) {
        ListResultModel<List<JavaTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> beans = new ArrayList<>();
            if (repositoryType != null) {
                beans = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(repositoryType);
            }
            resultModel = PageUtil.getDefaultPageList(beans, JavaTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "编辑模板信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateJavaTemp")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateJavaTemp(@RequestBody JavaTemp tempInfo) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            DSMFactory.getInstance().getTempManager().updateJavaTemp(tempInfo);

        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "编辑模板信息")
    @RequestMapping(method = RequestMethod.POST, value = "deleteJavaTemp")
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent}, bindMenu = {CustomMenuItem.delete})
    public @ResponseBody
    ResultModel<Boolean> deleteJavaTemp(String javaTempId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (javaTempId == null) {
                throw new JDSException("javaTempId is bull");
            }
            DSMFactory.getInstance().getTempManager().deleteJavaTemp(javaTempId);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "新建JAVA模板")
    @RequestMapping(value = {"CreateJavaTemp"}, method = {RequestMethod.GET, RequestMethod.POST})
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @ModuleAnnotation(caption = "新建JAVA模板")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public ResultModel<JavaTempForm> createJavaTemp(String javaTempId) {
        ResultModel<JavaTempForm> result = new ResultModel<JavaTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().createJavaTemp();
            result.setData(new JavaTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<JavaTempForm> errorResult = new ErrorResultModel<JavaTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    ;


    @RequestMapping(method = RequestMethod.POST, value = "TempFileInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @ModuleAnnotation(caption = "模板信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    public @ResponseBody
    ResultModel<JavaTempForm> getJavaTempInfo(String javaTempId) {
        ResultModel<JavaTempForm> result = new ResultModel<JavaTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaTempId);
            result.setData(new JavaTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<JavaTempForm> errorResult = new ErrorResultModel<JavaTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "loadChildDSM")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public TreeListResultModel<List<JavaTempNavTree>> loadChildDSM(DSMType dsmType) {
        TreeListResultModel<List<JavaTempNavTree>> result = new TreeListResultModel<List<JavaTempNavTree>>();

        List<Enumstype> dsmtypes = new ArrayList<>();
        switch (dsmType) {
            case view:
                ViewGroupType[] viewTypes = ViewGroupType.values();
                dsmtypes.addAll(Arrays.asList(viewTypes));
                break;
            case repository:
                RepositoryType[] repositoryTypes = RepositoryType.values();
                dsmtypes.addAll(Arrays.asList(repositoryTypes));
                break;
            case aggregation:
                AggregationType[] aggregationTypes = AggregationType.values();
                dsmtypes.addAll(Arrays.asList(aggregationTypes));
                break;
            case customDomain:
                CustomDomainType[] domainTypes = CustomDomainType.values();
                dsmtypes.addAll(Arrays.asList(domainTypes));
                break;

        }

        result = TreePageUtil.getTreeList(dsmtypes, JavaTempNavTree.class);


        return result;

    }

    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }

}
