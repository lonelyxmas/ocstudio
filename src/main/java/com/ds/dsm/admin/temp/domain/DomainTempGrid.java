package com.ds.dsm.admin.temp.domain;


import com.ds.dsm.admin.temp.domain.bpm.BpmDomainService;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {BpmDomainService.class}, event = CustomGridEvent.editor)
public class DomainTempGrid {

    @CustomAnnotation(uid = true, hidden = true)
    String javaTempId;

    @CustomAnnotation(pid = true, hidden = true)
    String fileId;

    @FieldAnnotation(  required = true)
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String name;

    @CustomAnnotation(caption = "领域",pid = true)
    DSMType dsmType;

    @FieldAnnotation(  required = true)
    @CustomAnnotation(caption = "通用域")
    CustomDomainType customDomainType = CustomDomainType.msg;

    @CustomAnnotation(caption = "模板说明")
    String desc;

    @FieldAnnotation(  required = true)
    @CustomAnnotation(caption = "包名规则")
    String packagePostfix;

    @FieldAnnotation(  required = true)
    @CustomAnnotation(caption = "名称规则")
    String namePostfix;

    public DomainTempGrid() {

    }

    public DomainTempGrid(JavaTemp temp) {
        this.name = temp.getName();
        this.desc = getDesc();
        this.fileId = temp.getFileId();
        this.javaTempId = temp.getJavaTempId();
        this.dsmType = temp.getDsmType();
        this.customDomainType = temp.getCustomDomainType();
        this.namePostfix = temp.getNamePostfix();
        this.packagePostfix = temp.getPackagePostfix();

    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }

    public CustomDomainType getCustomDomainType() {
        return customDomainType;
    }

    public void setCustomDomainType(CustomDomainType customDomainType) {
        this.customDomainType = customDomainType;
    }

    public String getPackagePostfix() {
        return packagePostfix;
    }

    public void setPackagePostfix(String packagePostfix) {
        this.packagePostfix = packagePostfix;
    }


    public String getNamePostfix() {
        return namePostfix;
    }

    public void setNamePostfix(String namePostfix) {
        this.namePostfix = namePostfix;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
