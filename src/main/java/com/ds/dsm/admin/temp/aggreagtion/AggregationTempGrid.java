package com.ds.dsm.admin.temp.aggreagtion;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Required;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {AggregationService.class}, event = CustomGridEvent.editor)
public class AggregationTempGrid {

    @CustomAnnotation(uid = true, hidden = true)
    String javaTempId;

    @CustomAnnotation(pid = true, hidden = true)
    String fileId;

    @Required
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String name;

    @CustomAnnotation(caption = "领域", pid = true)
    DSMType dsmType;

    @Required
    @CustomAnnotation(caption = "聚合类型")
    AggregationType aggregationType = AggregationType.aggregationEntity;

    @CustomAnnotation(caption = "模板说明")
    String desc;

    @CustomAnnotation(caption = "略缩图", hidden = true)
    String image = "/RAD/img/project.png";

    @Required
    @CustomAnnotation(caption = "包名规则")
    String packagePostfix;


    @Required
    @CustomAnnotation(caption = "名称规则")
    String namePostfix;


    public AggregationTempGrid() {

    }

    public AggregationTempGrid(JavaTemp temp) {

        this.name = temp.getName();
        this.desc = getDesc();
        if (temp.getImage() != null) {
            this.image = temp.getImage();
        }
        this.aggregationType = temp.getAggregationType();
        this.fileId = temp.getFileId();
        this.javaTempId = temp.getJavaTempId();
        this.dsmType = temp.getDsmType();
        this.namePostfix = temp.getNamePostfix();
        this.packagePostfix = temp.getPackagePostfix();

    }

    public AggregationType getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(AggregationType aggregationType) {
        this.aggregationType = aggregationType;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }


    public String getPackagePostfix() {
        return packagePostfix;
    }

    public void setPackagePostfix(String packagePostfix) {
        this.packagePostfix = packagePostfix;
    }

    public String getNamePostfix() {
        return namePostfix;
    }

    public void setNamePostfix(String namePostfix) {
        this.namePostfix = namePostfix;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
