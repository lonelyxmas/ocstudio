package com.ds.dsm.admin.temp.view;


import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.ViewType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/admin/temp/view/")
@MethodChinaName(cname = "视图模板", imageClass = "spafont spa-icon-conf")
public class ViewTreeService {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;


    @RequestMapping(method = RequestMethod.POST, value = "AllViewTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AllViewTempGrid")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<ViewTempGrid>> getAllViewTempGrid() {
        ListResultModel<List<ViewTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(DSMType.view);
            resultModel = PageUtil.getDefaultPageList(temps, ViewTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadChildView")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> loadChildView(String dsmTempId) {
        this.dsmTempId = dsmTempId;
        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel();
        ViewType[] viewTypes = ViewType.values();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(viewTypes), JavaTempNavTree.class);
        return resultModel;
    }


    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }
}
