package com.ds.dsm.admin.temp.aggreagtion;


import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.admin.temp.JavaTempGallery;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/admin/temp/agg/")
@MethodChinaName(cname = "聚合模板", imageClass = "spafont spa-icon-conf")
public class AggregationService {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;


    @RequestMapping(method = RequestMethod.POST, value = "TempGallery")
    @GalleryViewAnnotation
    @ModuleAnnotation( caption = "Gallery")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<JavaTempGallery>> getTempGallery(RepositoryType repositoryType) {
        ListResultModel<List<JavaTempGallery>> resultModel = new ListResultModel();
        try {
            if (repositoryType != null) {
                List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(repositoryType);
                resultModel = PageUtil.getDefaultPageList(temps, JavaTempGallery.class);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AllTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation( caption = "AllTemp")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<AggregationTempGrid>> getAllTempGrid() {
        ListResultModel<List<AggregationTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> temps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(DSMType.aggregation);
            resultModel = PageUtil.getDefaultPageList(temps, AggregationTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "TempGrid")
    @GridViewAnnotation
    @ModuleAnnotation( caption = "TempGrid")
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<AggregationTempGrid>> getTempGrid(AggregationType aggregationType) {
        ListResultModel<List<AggregationTempGrid>> resultModel = new ListResultModel();
        try {
            List<JavaTemp> beans = new ArrayList<>();
            if (aggregationType != null) {
                beans = DSMFactory.getInstance().getTempManager().getAggregationTemps(aggregationType);
            } else {
                beans = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(DSMType.aggregation);
            }
            resultModel = PageUtil.getDefaultPageList(beans, AggregationTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "编辑模板信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateJavaTemp")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateJavaTemp(@RequestBody JavaTemp tempInfo) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            DSMFactory.getInstance().getTempManager().updateJavaTemp(tempInfo);

        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "编辑模板信息")
    @RequestMapping(method = RequestMethod.POST, value = "deleteJavaTemp")
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent}, bindMenu = {CustomMenuItem.delete})
    public @ResponseBody
    ResultModel<Boolean> deleteJavaTemp(String javaTempId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (javaTempId == null) {
                throw new JDSException("javaTempId is bull");
            }
            DSMFactory.getInstance().getTempManager().deleteJavaTemp(javaTempId);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "新建JAVA模板")
    @RequestMapping(value = {"CreateJavaTemp"}, method = {RequestMethod.GET, RequestMethod.POST})
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @ModuleAnnotation( caption = "新建JAVA模板")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public ResultModel<AggregationTempForm> createJavaTemp(String javaTempId) {
        ResultModel<AggregationTempForm> result = new ResultModel<AggregationTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().createJavaTemp();
            result.setData(new AggregationTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<AggregationTempForm> errorResult = new ErrorResultModel<AggregationTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    ;


    @RequestMapping(method = RequestMethod.POST, value = "TempFileInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @ModuleAnnotation( caption = "模板信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    public @ResponseBody
    ResultModel<AggregationTempForm> getJavaTempInfo(String javaTempId) {
        ResultModel<AggregationTempForm> result = new ResultModel<AggregationTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaTempId);
            result.setData(new AggregationTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<AggregationTempForm> errorResult = new ErrorResultModel<AggregationTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }


        return result;
    }

    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }
}
