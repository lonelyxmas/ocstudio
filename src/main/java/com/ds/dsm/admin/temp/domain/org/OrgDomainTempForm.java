package com.ds.dsm.admin.temp.domain.org;


import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.toolbar.CodeEditorTools;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.domain.enums.OrgDomainType;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.ThumbnailType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(stretchHeight = StretchType.last, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {OrgDomainService.class})
public class OrgDomainTempForm {

    @CustomAnnotation(uid = true, hidden = true)
    String javaTempId;

    @CustomAnnotation(pid = true, hidden = true)
    String fileId;

    @CustomAnnotation(pid = true, hidden = true)
    ThumbnailType thumbnailType;

@FieldAnnotation(required = true, colSpan = -1)
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String name;


    @CustomAnnotation(caption = "领域类型", readonly = true, hidden = true)
    DSMType dsmType = DSMType.customDomain;


    @CustomAnnotation(caption = "通用域", readonly = true)
    CustomDomainType customDomainType = CustomDomainType.org;

@Required
    @CustomAnnotation(caption = "模板")
    OrgDomainType orgDomainType;

    @Required
    @CustomAnnotation(caption = "包名规则")
    String packagePostfix;

    @CustomAnnotation(caption = "模板文件", hidden = true)
    String path;

    @CustomAnnotation(caption = "名称规则")
    String namePostfix;


    @FieldAnnotation(rowHeight = "100",  componentType = ComponentType.Image)
    @CustomAnnotation(caption = "略缩图", captionField = true)
    String image = "/RAD/img/project.png";


    @FileUploadAnnotation(src = "/custom/dsm/AttachUPLoad?thumbnailType=javaTemp")
    @FieldAnnotation(rowHeight = "100", componentType = ComponentType.FileUpload)
    @CustomAnnotation( caption = "上传略缩图")
    String thumbnailFile;

    @JavaEditorAnnotation
    @ToolBarMenu(menuClasses = CodeEditorTools.class)
    @FieldAnnotation(componentType = ComponentType.JavaEditor, haslabel = false, colSpan = -1, rowHeight = "350")
    @CustomAnnotation(caption = "模板内容")
    String content;

    public OrgDomainTempForm() {

    }

    public OrgDomainTempForm(JavaTemp temp) {
        this.name = temp.getName();
        this.image = temp.getImage();
        this.content = temp.getContent();
        this.fileId = temp.getFileId();
        this.javaTempId = temp.getJavaTempId();
        this.path = temp.getPath();
        this.customDomainType = temp.getCustomDomainType();
        this.orgDomainType = temp.getOrgDomainType();
        this.namePostfix = temp.getNamePostfix();
        this.packagePostfix = temp.getPackagePostfix();
        this.thumbnailType = ThumbnailType.javaTemp;

    }

    public OrgDomainType getOrgDomainType() {
        return orgDomainType;
    }

    public void setOrgDomainType(OrgDomainType orgDomainType) {
        this.orgDomainType = orgDomainType;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public CustomDomainType getCustomDomainType() {
        return customDomainType;
    }

    public void setCustomDomainType(CustomDomainType customDomainType) {
        this.customDomainType = customDomainType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getThumbnailFile() {
        return thumbnailFile;
    }

    public void setThumbnailFile(String thumbnailFile) {
        this.thumbnailFile = thumbnailFile;
    }

    public ThumbnailType getThumbnailType() {
        return thumbnailType;
    }

    public void setThumbnailType(ThumbnailType thumbnailType) {
        this.thumbnailType = thumbnailType;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }


    public String getPackagePostfix() {
        return packagePostfix;
    }

    public void setPackagePostfix(String packagePostfix) {
        this.packagePostfix = packagePostfix;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public String getNamePostfix() {
        return namePostfix;
    }

    public void setNamePostfix(String namePostfix) {
        this.namePostfix = namePostfix;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
