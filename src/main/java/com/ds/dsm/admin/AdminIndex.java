package com.ds.dsm.admin;


import com.ds.enums.db.MethodChinaName;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/dsm/admin/")
@MethodChinaName(cname = "控制管理台", imageClass = "spafont spa-icon-settingprj")
public class AdminIndex {


}
