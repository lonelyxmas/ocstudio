package com.ds.dsm.website.select;


import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/website/select/")
@TabsAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {WebSiteSelectService.class}, singleOpen = true)
public class WebSiteSelectNav {


    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;

    @CustomAnnotation(hidden = true, pid = true)
    public String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String projectVersionName;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewInstId;

    @MethodChinaName(cname = "站内模板")
    @RequestMapping(method = RequestMethod.POST, value = "LocalWebSiteNav")
        @NavTreeViewAnnotation
        @ModuleAnnotation( imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "站内模板")
        @ResponseBody
        public TreeListResultModel<List<WebSiteSelectTree>> getLocalWebSiteNav(String projectVersionName, String domainId, String viewInstId) {
            TreeListResultModel<List<WebSiteSelectTree>> resultModel = new TreeListResultModel<List<WebSiteSelectTree>>();
            WebSiteSelectTree<WebSiteSelectTree> viewItem = new WebSiteSelectTree("dsm.website.select.LocalWebSiteList", domainId);
            List<WebSiteSelectTree> items = new ArrayList<>();
            items.add(viewItem);
            resultModel.setIds(Arrays.asList(new String[]{viewItem.getSub().get(0).getId()}));
            resultModel.setData(items);
            return resultModel;

    }


    @MethodChinaName(cname = "模板市场")
    @RequestMapping(method = RequestMethod.POST, value = "NETWebSiteNav")
    @NavTreeViewAnnotation
    @ModuleAnnotation( imageClass = "spafont spa-icon-links", dynLoad = true, caption = "模板市场")
    @ResponseBody
    public TreeListResultModel<List<WebSiteSelectTree>> getNETWebSiteNav(String projectVersionName, String domainId, String viewInstId) {
        TreeListResultModel<List<WebSiteSelectTree>> resultModel = new TreeListResultModel<List<WebSiteSelectTree>>();
        WebSiteSelectTree<WebSiteSelectTree> viewItem = new WebSiteSelectTree("dsm.website.select.NETWebSiteList", domainId);
        List<WebSiteSelectTree> items = new ArrayList<>();
        items.add(viewItem);
        if (viewItem.getSub().size() > 0) {
            resultModel.setIds(Arrays.asList(new String[]{viewItem.getSub().get(0).getId()}));
        }
        resultModel.setData(items);
        return resultModel;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
