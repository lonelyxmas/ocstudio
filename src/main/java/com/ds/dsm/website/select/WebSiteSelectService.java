package com.ds.dsm.website.select;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GalleryViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/website/select/")
@MethodChinaName(cname = "模板选择")

public class WebSiteSelectService {


    @MethodChinaName(cname = "远程模板管理")
    @RequestMapping(method = RequestMethod.POST, value = "NETWebSiteList")
    @GalleryViewAnnotation
    @ModuleAnnotation(caption = "远程模板管理")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<WebSiteSelectGallery>> getNETTempList(String dsmId, String sourceClassName, String dsmTempType) {
        ListResultModel<List<WebSiteSelectGallery>> result = new ListResultModel();
        List<DSMBean> dsmBeans = new ArrayList<>();
        try {
            List<DSMBean> tempBeans = DSMFactory.getInstance().getTempManager().getDSMBeanList();
            for (DSMBean bean : tempBeans) {
                if (bean.getType() == null || bean.getType().getType().equals(dsmTempType)) {
                    dsmBeans.add(bean);
                }
            }
            result = PageUtil.getDefaultPageList(dsmBeans, WebSiteSelectGallery.class);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "本地模板管理")
    @RequestMapping(method = RequestMethod.POST, value = "LocalWebSiteList")
    @ModuleAnnotation(caption = "本地模板管理")
    @GalleryViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<WebSiteSelectGallery>> getLocalWebSiteList(String dsmId, String sourceClassName, String dsmTempType) {
        ListResultModel<List<WebSiteSelectGallery>> result = new ListResultModel();
        List<DSMBean> dsmBeans = new ArrayList<>();
        try {
            List<DSMBean> tempBeans = DSMFactory.getInstance().getTempManager().getDSMBeanList();
            for (DSMBean bean : tempBeans) {
                if (bean.getType() == null || bean.getType().getType().equals(dsmTempType)) {
                    dsmBeans.add(bean);
                }
            }
            result = PageUtil.getDefaultPageList(dsmBeans, WebSiteSelectGallery.class);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "从模板添加")
    @RequestMapping(method = RequestMethod.POST, value = "addTempFromJava")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> addTempFromJava(String AddTempFromJaveTempTree, String sourceClassName, String methodName, String projectVersionName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (AddTempFromJaveTempTree == null) {
                throw new JDSException("javaTempId is bull");
            }
            if (AddTempFromJaveTempTree != null) {
                String[] tempIds = StringUtility.split(AddTempFromJaveTempTree, ";");
                if (tempIds.length > 0) {
                    Set<String> javaTempIds = new HashSet<>();
                    for (String id : tempIds) {
                        JavaTemp tempBean = DSMFactory.getInstance().getTempManager().getJavaTempById(id);
                        if (tempBean != null) {
                            javaTempIds.add(id);
                        }
                    }
                    DSMType dsmType = null;
                    if (DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName) != null) {
                        dsmType = DSMType.repository;
                    } else if (DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId) != null) {
                        dsmType = DSMType.aggregation;
                    } else if (DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId) != null) {
                        dsmType = DSMType.view;
                    } else {
                        dsmType = DSMType.customDomain;
                    }

                    if (dsmType == null) {
                        RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                        if (repositoryInst != null) {
                            dsmType = DSMType.repository;
                        } else if (DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId) != null) {
                            dsmType = DSMType.aggregation;
                        } else if (DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId) != null) {
                            dsmType = DSMType.view;
                        } else {
                            dsmType = DSMType.customDomain;
                        }
                    }


                    switch (dsmType) {
                        case view:
                            ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
                            if (sourceClassName != null && !sourceClassName.equals("")) {
                                ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId);
                                if (methodName != null && !methodName.equals("")) {
                                    MethodConfig apiBean = classConfig.getSourceConfig().getMethodByName(methodName);
                                    if (apiBean != null) {
                                        apiBean.getJavaTempIds().addAll(javaTempIds);
                                    } else {
                                        classConfig.getJavaTempIds().addAll(javaTempIds);
                                    }
                                } else {
                                    classConfig.getJavaTempIds().addAll(javaTempIds);
                                }
                                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
                            } else {
                                viewInst.getJavaTempIds().clear();
                                viewInst.getJavaTempIds().addAll(javaTempIds);
                                DSMFactory.getInstance().getViewManager().updateViewInst(viewInst, true);
                            }
                            break;
                        case aggregation:
                            DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                            if (javaTempIds.size() > 0) {

                                if (sourceClassName != null && !sourceClassName.equals("")) {
                                    AggEntityConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
                                    esdClassConfig.getJavaTempIds().clear();
                                    esdClassConfig.getJavaTempIds().addAll(javaTempIds);
                                    DSMFactory.getInstance().getAggregationManager().updateAggEntityConfig(esdClassConfig);
                                } else {
                                    domainInst.getJavaTempIds().clear();
                                    domainInst.getJavaTempIds().addAll(javaTempIds);
                                    DSMFactory.getInstance().getAggregationManager().updateDomainInst(domainInst, true);
                                }


                            }
                            break;
                        case repository:
                            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                            if (repositoryInst != null) {
                                repositoryInst.getJavaTempIds().clear();
                                repositoryInst.getJavaTempIds().addAll(javaTempIds);
                                DSMFactory.getInstance().getRepositoryManager().updateRepositoryInst(repositoryInst, true);
                            }
                            break;

                    }


                }
            }

        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "从模板添加")
    @RequestMapping(method = RequestMethod.POST, value = "addTempFromWebSite")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadTopParent, CustomCallBack.CloseTop}, bindMenu = CustomMenuItem.gridSave)
    public @ResponseBody
    ResultModel<Boolean> addTempFromWebSite(String LocalWebSiteListGallery, String sourceClassName, String methodName, String projectVersionName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (LocalWebSiteListGallery == null) {
                throw new JDSException("javaTempId is bull");
            }
            if (LocalWebSiteListGallery != null) {
                String[] tempIds = StringUtility.split(LocalWebSiteListGallery, ";");
                Set<String> javaTempIds = new HashSet<>();
                for (String id : tempIds) {
                    DSMBean tempBean = DSMFactory.getInstance().getTempManager().getDSMBeanById(id);
                    if (tempBean != null) {
                        javaTempIds.addAll(tempBean.getJavaTempIds());
                    }
                }

                if (tempIds.length > 0) {
                    DSMType dsmType = null;
                    if (DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName) != null) {
                        dsmType = DSMType.repository;
                    } else if (DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId) != null) {
                        dsmType = DSMType.aggregation;
                    } else if (DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId) != null) {
                        dsmType = DSMType.view;
                    } else {
                        dsmType = DSMType.customDomain;
                    }


                    switch (dsmType) {
                        case view:
                            ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
                            if (sourceClassName != null && !sourceClassName.equals("")) {
                                ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
                                if (methodName != null && !methodName.equals("")) {
                                    MethodConfig apiBean = classConfig.getSourceConfig().getMethodByName(methodName);
                                    if (apiBean != null) {
                                        apiBean.getJavaTempIds().addAll(javaTempIds);
                                    } else {
                                        classConfig.getJavaTempIds().addAll(javaTempIds);
                                    }
                                } else {
                                    classConfig.getJavaTempIds().addAll(javaTempIds);
                                }
                                DSMFactory.getInstance().getViewManager().updateViewEntityConfig(classConfig);
                            } else {
                                viewInst.getJavaTempIds().clear();
                                viewInst.getJavaTempIds().addAll(javaTempIds);
                                DSMFactory.getInstance().getViewManager().updateViewInst(viewInst, true);
                            }
                            break;
                        case aggregation:
                            DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                            if (javaTempIds.size() > 0) {
                                domainInst.getJavaTempIds().clear();
                                domainInst.getJavaTempIds().addAll(javaTempIds);
                                DSMFactory.getInstance().getAggregationManager().updateDomainInst(domainInst, true);
                            }
                            break;
                        case repository:
                            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                            if (repositoryInst != null) {
                                repositoryInst.getJavaTempIds().clear();
                                repositoryInst.getJavaTempIds().addAll(javaTempIds);
                                DSMFactory.getInstance().getRepositoryManager().updateRepositoryInst(repositoryInst, true);
                            }
                            break;
                    }
                }
            }
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "选中模板")
    @RequestMapping(method = RequestMethod.POST, value = "selectTemp")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadTopParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save})
    public @ResponseBody
    ResultModel<Boolean> selectTemp(String NETWebSiteListGallery, String LocalWebSiteListGallery, String sourceClassName, String projectVersionName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {

            if (NETWebSiteListGallery == null && LocalWebSiteListGallery == null) {
                throw new JDSException("javaTempId is bull");
            }
            String[] tempIds = new String[]{};
            if (LocalWebSiteListGallery != null && !LocalWebSiteListGallery.equals("")) {
                tempIds = StringUtility.split(LocalWebSiteListGallery, ";");
            } else if (NETWebSiteListGallery != null && !NETWebSiteListGallery.equals("")) {
                tempIds = StringUtility.split(NETWebSiteListGallery, ";");

            }
            Set<String> javaTempIds = new HashSet<>();
            for (String id : tempIds) {
                DSMBean tempBean = DSMFactory.getInstance().getTempManager().getDSMBeanById(id);
                if (tempBean != null) {
                    javaTempIds.addAll(tempBean.getJavaTempIds());
                }
            }
            DSMType dsmType = null;
            if (DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName) != null) {
                dsmType = DSMType.repository;
            } else if (DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId) != null) {
                dsmType = DSMType.aggregation;
            } else if (DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId) != null) {
                dsmType = DSMType.view;
            } else {
                dsmType = DSMType.customDomain;
            }

            switch (dsmType) {
                case view:
                    ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
                    if (sourceClassName != null && !sourceClassName.equals("")) {
                        ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
                        Set<String> oTempIds = classConfig.getJavaTempIds();
                        oTempIds.clear();
                        oTempIds.addAll(javaTempIds);
                        DSMFactory.getInstance().getViewManager().updateViewEntityConfig(classConfig);
                    } else {
                        viewInst.getJavaTempIds().clear();
                        viewInst.getJavaTempIds().addAll(javaTempIds);
                        DSMFactory.getInstance().getViewManager().updateViewInst(viewInst, true);
                    }
                    break;
                case aggregation:
                    DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                    if (javaTempIds.size() > 0) {
                        domainInst.getJavaTempIds().clear();
                        domainInst.getJavaTempIds().addAll(javaTempIds);
                        DSMFactory.getInstance().getAggregationManager().updateDomainInst(domainInst, true);
                    }
                    break;
                case repository:
                    RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                    if (repositoryInst != null) {
                        repositoryInst.getJavaTempIds().clear();
                        repositoryInst.getJavaTempIds().addAll(javaTempIds);
                        DSMFactory.getInstance().getRepositoryManager().updateRepositoryInst(repositoryInst, true);
                    }
                    break;
            }

        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

}
