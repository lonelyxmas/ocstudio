package com.ds.dsm.website.select;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.tool.ui.component.list.TreeListItem;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(caption = "JAVA模板")
public class WebSiteSelectTree<T extends WebSiteSelectTree> extends TreeListItem<T> {
    public WebSiteSelectTree(String euClassName, String domainId) {
        this.caption = "所有模型";
        this.setId("DSMTempRoot");
        this.setIniFold(false);
        this.addTagVar("domainId", domainId);
        DSMTempType[] dsmTypes = DSMTempType.values();

        for (DSMTempType type : dsmTypes) {
            this.addChild((T) new WebSiteSelectTree(type, euClassName, domainId));

        }
    }

    public WebSiteSelectTree(DSMTempType dsmTempType, String euClassName, String domainId) {
        this.caption = dsmTempType.getName();
        this.id = dsmTempType.getType();
        this.iniFold = false;
        this.euClassName = euClassName;
        this.addTagVar("dsmTempType", dsmTempType.getType());
        this.addTagVar("domainId", domainId);
        this.setImageClass(dsmTempType.getImageClass());

    }


}
