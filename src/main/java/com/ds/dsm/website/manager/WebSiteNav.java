package com.ds.dsm.website.manager;


import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.website.temp.tree.WebSiteTreeView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.BlockAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/website/")
@MethodChinaName(cname = "模板站点", imageClass = "xui-icon-upload")
@BottomBarMenu
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {WebSiteService.class})
public class WebSiteNav {


    @CustomAnnotation(uid = true, hidden = true)
    public String dsmTempId;


    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;


    @MethodChinaName(cname = "模板信息")
    @RequestMapping(method = RequestMethod.POST, value = "WebSiteInfo")
    @FormViewAnnotation
    @ModuleAnnotation(dock = Dock.top)
    @UIAnnotation( height = "220")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<WebSiteFormView> getDSMInfo(String dsmTempId) {
        ResultModel<WebSiteFormView> result = new ResultModel<WebSiteFormView>();
        DSMBean bean = null;
        try {
            bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            WebSiteFormView tempView = new WebSiteFormView(bean);
            result.setData(tempView);
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return result;
    }


    @MethodChinaName(cname = "JAVA模板")
    @RequestMapping(method = RequestMethod.POST, value = "CodeTempsNav")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", dynLoad = true, dock = Dock.fill)
    @ResponseBody
    public TreeListResultModel<List<WebSiteTreeView>> getCodeTemps(String dsmTempId) {
        TreeListResultModel<List<WebSiteTreeView>> resultModel = new TreeListResultModel<List<WebSiteTreeView>>();
        this.dsmTempId = dsmTempId;
        DSMType[] viewTypes = DSMType.values();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(viewTypes), WebSiteTreeView.class, Arrays.asList(DSMType.repository.getType()));
        return resultModel;

    }


    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
