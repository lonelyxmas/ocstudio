package com.ds.dsm.website.manager;


import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.FileUploadAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.annotation.GalleryAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.dsm.enums.ThumbnailType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.StretchType;

@PageBar
@GalleryAnnotation()
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Delete}, customService = {WebSiteService.class}, event = {CustomGridEvent.editor})
public class WebSiteGridView {
    @CustomAnnotation(uid = true, hidden = true)
    String dsmTempId;

    @FieldAnnotation( required = true)
    @CustomAnnotation(caption = "模板名称")
    private String name;

    @FieldAnnotation( required = true)
    @CustomAnnotation(caption = "命名空间")
    public String space = "dsm";

    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation( caption = "业务类型")
    public DSMTempType type;

    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "描述")
    public String desc;

    @CustomAnnotation(pid = true, hidden = true)
    public ThumbnailType thumbnailType = ThumbnailType.dsmTemp;

    @FieldAnnotation( componentType = ComponentType.Image, rowHeight = "100")
    @CustomAnnotation(caption = "图片", captionField = true)
    String image = "/RAD/img/project.png";

    @FileUploadAnnotation(src = "/custom/dsm/AttachUPLoad?thumbnailType=dsmTemp")
    @FieldAnnotation(componentType = ComponentType.FileUpload)
    @CustomAnnotation( caption = "上传略缩图")
    public String thumbnailFile;


    public WebSiteGridView() {

    }

    public WebSiteGridView(DSMBean bean) {
        this.name = bean.getName();
        this.space = bean.getSpace();
        this.dsmTempId = bean.getDsmTempId();
        this.desc = bean.getDesc();
        this.type = bean.getType();
        this.image = bean.getImage();


    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumbnailFile() {
        return thumbnailFile;
    }

    public void setThumbnailFile(String thumbnailFile) {
        this.thumbnailFile = thumbnailFile;
    }


    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }

    public DSMTempType getType() {
        return type;
    }

    public void setType(DSMTempType type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public ThumbnailType getThumbnailType() {
        return thumbnailType;
    }

    public void setThumbnailType(ThumbnailType thumbnailType) {
        this.thumbnailType = thumbnailType;
    }

}
