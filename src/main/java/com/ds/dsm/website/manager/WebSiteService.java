package com.ds.dsm.website.manager;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;

import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GalleryViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/website/")
@MethodChinaName(cname = "DSM站点", imageClass = "xui-icon-upload")

public class WebSiteService {

    @MethodChinaName(cname = "更新站点")
    @RequestMapping(method = RequestMethod.POST, value = "updateWebSite")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateDSM(@RequestBody WebSiteFormView tempInfo) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            DSMBean bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(tempInfo.getDsmTempId());
            if (bean == null) {
                bean = DSMFactory.getInstance().getTempManager().createDSMBean(tempInfo.getDsmTempId());
                bean.setDsmTempId(tempInfo.getDsmTempId());
            }
            bean.setType(tempInfo.getType());
            bean.setDesc(tempInfo.getDesc());
            bean.setName(tempInfo.getName());
            bean.setSpace(tempInfo.getSpace());
            DSMFactory.getInstance().getTempManager().updateTempBean(bean);

        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "刪除站点")
    @RequestMapping(method = RequestMethod.POST, value = "delWebSite")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delTemp(String id) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (id == null) {
                throw new JDSException("tempId is bull");
            }
            DSMFactory.getInstance().getTempManager().deleteDSMBean(id);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }


    @MethodChinaName(cname = "新建站点")
    @RequestMapping(value = {"CreateWebSite"}, method = {RequestMethod.GET, RequestMethod.POST})
    @FormViewAnnotation
    @ModuleAnnotation(  caption = "新建站点")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.add)
    @DialogAnnotation(height = "350", width = "480")
    @ResponseBody
    public ResultModel<WebSiteFormView> createTemp(String configKey) {
        ResultModel<WebSiteFormView> result = new ResultModel<WebSiteFormView>();
        try {
            DSMBean tempBean = DSMFactory.getInstance().getTempManager().createDSMBean(configKey);
            result.setData(new WebSiteFormView(tempBean));
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "WebSiteList")
    @GalleryViewAnnotation
    @ModuleAnnotation( caption = "站点管理")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public ListResultModel<List<WebSiteGallery>> getDSMBTempList(DSMTempType dsmTempType) {
        ListResultModel<List<WebSiteGallery>> result = new ListResultModel();
        List<DSMBean> dsmBeans = new ArrayList<>();
        try {
            List<DSMBean> tempBeans = DSMFactory.getInstance().getTempManager().getDSMBeanList();
            for (DSMBean bean : tempBeans) {
                if (bean.getType() == null || bean.getType().equals(dsmTempType)) {
                    dsmBeans.add(bean);
                }
            }
            result = PageUtil.getDefaultPageList(dsmBeans, WebSiteGallery.class);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    
    @RequestMapping(method = RequestMethod.POST, value = "WebSiteNav")
    @ModuleAnnotation(  caption = "站点详细信息")
    @NavGroupViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @APIEventAnnotation(bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<WebSiteNav> getTempNav(String dsmTempId) {
        ResultModel<WebSiteNav> result = new ResultModel<WebSiteNav>();
        return result;
    }


}
