package com.ds.dsm.website.manager;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.ImageAnnotation;
import com.ds.esd.custom.gallery.annotation.GalleryAnnotation;
import com.ds.esd.custom.gallery.enums.CustomGalleryEvent;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.tool.ui.enums.ComponentType;

@PageBar
@GalleryAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {WebSiteService.class}, event = CustomGalleryEvent.onDblclick)
public class WebSiteGallery {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;
    @CustomAnnotation()
    String caption;
    @CustomAnnotation(uid = true, hidden = true)
    String id;
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String comment;

    @ImageAnnotation
    @CustomAnnotation(caption = "图片")
    String image = "/RAD/img/project.png";


    public WebSiteGallery() {

    }

    public WebSiteGallery(DSMBean temp) {
        this.comment = temp.getName();
        if (temp.getImage() != null && !temp.getImage().equals("")) {
            this.image = temp.getImage();
        }
        this.id = temp.getDsmTempId();
        this.caption = "";
        this.dsmTempId = temp.getDsmTempId();

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

}
