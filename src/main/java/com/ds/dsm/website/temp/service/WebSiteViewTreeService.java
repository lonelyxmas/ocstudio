package com.ds.dsm.website.temp.service;


import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.dsm.website.temp.WebSiteCodeTempGrid;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.annotation.ViewType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/website/view/")
@MethodChinaName(cname = "资源库模板", imageClass = "spafont spa-icon-conf")
public class WebSiteViewTreeService {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;


    @RequestMapping(method = RequestMethod.POST, value = "AllViewTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<WebSiteCodeTempGrid>> getAllRepositoryTempGrid(String dsmTempId, ViewType viewType) {
        ListResultModel<List<WebSiteCodeTempGrid>> resultModel = new ListResultModel();
        try {
            DSMBean bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null && (viewType == null || javaTemp.getViewType().equals(viewType))) {
                        temps.add(javaTemp);
                    }
                }
            }

            resultModel = PageUtil.getDefaultPageList(temps, WebSiteCodeTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }
//
//    @RequestMapping(method = RequestMethod.POST, value = "loadChildView")
//    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.loadChild)
//    @ResponseBody
//    public TreeListResultModel<List<JavaTempNavTree>> loadChildView(String dsmTempId) {
//        this.dsmTempId = dsmTempId;
//        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel();
//        ViewType[] repositoryTypes = ViewType.values();
//        resultModel = TreePageUtil.getTreeList(Arrays.asList(repositoryTypes), JavaTempNavTree.class);
//        return resultModel;
//    }


    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }
}
