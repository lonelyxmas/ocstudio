package com.ds.dsm.website.temp.tree;

import com.ds.dsm.website.temp.service.*;
import com.ds.enums.IconEnumstype;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.domain.enums.*;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.ViewType;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(caption = "JAVA模板")
public class WebSiteTreeView<T extends WebSiteTreeView> extends TreeListItem<T> {

    @Pid
    String dsmTempId;

    @Pid
    DSMTempType dsmTempType;

    @Pid
    CustomDomainType customDomainType;

    @Pid
    DSMType dsmType;
    @Pid
    RepositoryType repositoryType;

    @Pid
    IconEnumstype itemDomainType;

    @Pid
    ViewType viewType;

    @Pid
    AggregationType aggregationType;

    @Pid
    String javaTempId;

    @TreeItemAnnotation(imageClass = "spafont spa-icon-settingprj", caption = "模板分类", bindService = WebSiteAdminTempService.class)
    public WebSiteTreeView(DSMType dsmType, String dsmTempId) {
        this.dsmTempId = dsmTempId;
        this.caption = dsmType.getName();
        this.id = dsmType.getType();
        this.imageClass = dsmType.getImageClass();
        this.dsmType = dsmType;
    }

    @TreeItemAnnotation(bindService = WebSiteAggregationService.class)
    public WebSiteTreeView(AggregationType aggregationType) {
        this.caption = aggregationType.getName();
        this.imageClass = aggregationType.getImageClass();
        this.id = DSMType.aggregation.getType() + "_" + aggregationType.getType();
        this.aggregationType = aggregationType;
        this.dsmType = DSMType.aggregation;

    }

    @TreeItemAnnotation(bindService = WebSiteRepositoryAdminService.class)
    public WebSiteTreeView(RepositoryType repositoryType) {
        this.caption = repositoryType.getName();
        this.imageClass = repositoryType.getImageClass();
        this.id = DSMType.repository.getType() + "_" + repositoryType.getType();
        this.repositoryType = repositoryType;
        this.dsmType = DSMType.repository;
    }

    @TreeItemAnnotation(bindService = WebSiteDomainService.class)
    public WebSiteTreeView(CustomDomainType customDomainType) {
        this.caption = customDomainType.getName();
        this.imageClass = customDomainType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + customDomainType.getType();
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = WebSiteViewTreeService.class)
    public WebSiteTreeView(ViewType viewType) {
        this.caption = viewType.getName();
        this.imageClass = viewType.getImageClass();
        this.id = DSMType.view.getType() + "_" + viewType.getType();
        this.viewType = viewType;

    }

    @TreeItemAnnotation(bindService = WebSiteDomainTreeService.class, dynDestory = true, lazyLoad = true)
    public WebSiteTreeView(NavDomainType itemDomainType, CustomDomainType customDomainType) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType;
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = WebSiteDomainTreeService.class, dynDestory = true, lazyLoad = true)
    public WebSiteTreeView(BpmDomainType itemDomainType, CustomDomainType customDomainType) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType;
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = WebSiteDomainTreeService.class, dynDestory = true, lazyLoad = true)
    public WebSiteTreeView(OrgDomainType itemDomainType, CustomDomainType customDomainType) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType;
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = WebSiteDomainTreeService.class, dynDestory = true, lazyLoad = true)
    public WebSiteTreeView(MsgDomainType itemDomainType, CustomDomainType customDomainType) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType;
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = WebSiteDomainTreeService.class, dynDestory = true, lazyLoad = true)
    public WebSiteTreeView(VfsDomainType itemDomainType, CustomDomainType customDomainType) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType;
        this.customDomainType = customDomainType;

    }

    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }

    public DSMTempType getDsmTempType() {
        return dsmTempType;
    }

    public void setDsmTempType(DSMTempType dsmTempType) {
        this.dsmTempType = dsmTempType;
    }

    public CustomDomainType getCustomDomainType() {
        return customDomainType;
    }

    public void setCustomDomainType(CustomDomainType customDomainType) {
        this.customDomainType = customDomainType;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public IconEnumstype getItemDomainType() {
        return itemDomainType;
    }

    public void setItemDomainType(IconEnumstype itemDomainType) {
        this.itemDomainType = itemDomainType;
    }

    public ViewType getViewType() {
        return viewType;
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }

    public RepositoryType getRepositoryType() {
        return repositoryType;
    }

    public void setRepositoryType(RepositoryType repositoryType) {
        this.repositoryType = repositoryType;
    }

    public AggregationType getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(AggregationType aggregationType) {
        this.aggregationType = aggregationType;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }
}
