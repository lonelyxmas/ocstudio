package com.ds.dsm.website.temp;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.dsm.admin.temp.JavaTempForm;
import com.ds.dsm.manager.temp.tree.DSMInstPopTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/website/temp/")
@MethodChinaName(cname = "模板管理")

public class WebSiteJavaTempService {


    @MethodChinaName(cname = "所有JAVA模板")
    @RequestMapping(method = RequestMethod.POST, value = "AllJavaTemp")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "所有JAVA模板")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<WebSiteCodeTempGrid>> getAllJavaTemps(String dsmTempId) {
        ListResultModel<List<WebSiteCodeTempGrid>> result = new ListResultModel();
        DSMBean bean = null;
        try {
            bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null) {
                        temps.add(javaTemp);
                    }
                }
            }
            result = PageUtil.getDefaultPageList(temps, WebSiteCodeTempGrid.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "DaoJavaTemp")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "代码模板")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<WebSiteCodeTempGrid>> getDSMJavaTemps(String dsmTempId, RepositoryType repositoryType) {
        ListResultModel<List<WebSiteCodeTempGrid>> result = new ListResultModel();
        DSMBean bean = null;
        try {
            bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null && javaTemp.getDsmType().equals(DSMType.repository)) {
                        if (javaTemp.getRepositoryType() != null && (repositoryType == null || javaTemp.getRepositoryType().equals(repositoryType))) {
                            temps.add(javaTemp);
                        }
                    }
                }
                result = PageUtil.getDefaultPageList(temps, WebSiteCodeTempGrid.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }


    @MethodChinaName(cname = "代码模板")
    @RequestMapping(method = RequestMethod.POST, value = "CodeJavaTemp")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "代码模板")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<WebSiteCodeTempGrid>> getDSMJavaTemps(String dsmTempId, DSMType dsmType) {
        ListResultModel<List<WebSiteCodeTempGrid>> result = new ListResultModel();
        DSMBean bean = null;
        try {
            bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null) {
                        if (javaTemp.getDsmType() != null && javaTemp.getDsmType().equals(dsmType)) {
                            temps.add(javaTemp);
                        }
                    }
                }
                result = PageUtil.getDefaultPageList(temps, WebSiteCodeTempGrid.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }


    @MethodChinaName(cname = "移除JAVA模板")
    @RequestMapping(method = RequestMethod.POST, value = "removeRefJavaTemp")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> removeRefJavaTemp(String javaTempId, String dsmTempId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (javaTempId == null) {
                throw new JDSException("javaTempId is bull");
            }
            DSMBean dsmBean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            dsmBean.getJavaTempIds().remove(javaTempId);
            DSMFactory.getInstance().getTempManager().updateTempBean(dsmBean);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "TempFileInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @ModuleAnnotation(caption = "模板信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    public @ResponseBody
    ResultModel<JavaTempForm> getJavaTempInfo(String javaTempId) {
        ResultModel<JavaTempForm> result = new ResultModel<JavaTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaTempId);
            result.setData(new JavaTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<JavaTempForm> errorResult = new ErrorResultModel<JavaTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "更新模板信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateDSMRefTemp")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateDSMRefTemp(String AddDSMTempTree, String dsmTempId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (AddDSMTempTree == null) {
                throw new JDSException("javaTempId is bull");
            }
            DSMBean dsmBean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            if (AddDSMTempTree != null) {
                String[] tempIds = StringUtility.split(AddDSMTempTree, ";");
                if (tempIds.length > 0) {
                    dsmBean.getJavaTempIds().clear();
                    for (String id : tempIds) {
                        JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(id);
                        if (temp != null) {
                            dsmBean.getJavaTempIds().add(id);
                        }
                    }
                }
            }
            DSMFactory.getInstance().getTempManager().updateTempBean(dsmBean);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @RequestMapping(value = {"AddDSMTemp"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation(saveUrl = "dsm.website.temp.updateDSMRefTemp")
    @ModuleAnnotation(dynLoad = true, caption = "添加JAVA模板")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = {CustomMenuItem.add})
    @DialogAnnotation(height = "450", width = "300")
    @ResponseBody
    public TreeListResultModel<List<DSMInstPopTree>> getDSMTempTree(String dsmTempId, DSMType dsmType, String domainId, String viewInstId) {
        TreeListResultModel<List<DSMInstPopTree>> result = new TreeListResultModel<List<DSMInstPopTree>>();
        try {
            List<DSMInstPopTree> treeList = new ArrayList<DSMInstPopTree>();
            //  DSMInstPopTree listItem = new DSMInstPopTree("DSMTempRoot", true);
            DSMInstPopTree listItem = new DSMInstPopTree(dsmType, "DSMTempRoot", domainId, viewInstId, null, true);
            DSMBean dsmBean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            treeList.add(listItem);
            result.setData(treeList);
            result.setIds(Arrays.asList(dsmBean.getJavaTempIds().toArray(new String[]{})));

        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


}
