package com.ds.dsm.website.temp.service;

import com.ds.common.JDSException;
import com.ds.config.*;
import com.ds.dsm.admin.temp.JavaTempForm;
import com.ds.dsm.admin.temp.JavaTempGallery;
import com.ds.dsm.admin.temp.JavaTempGrid;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.dsm.website.temp.WebSiteCodeTempGrid;
import com.ds.dsm.website.temp.tree.WebSiteTreeView;
import com.ds.enums.Enumstype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.ViewType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = {"/dsm/website/admin/"})
@MethodChinaName(cname = "模板管理", imageClass = "spafont spa-icon-c-gallery")
public class WebSiteAdminTempService {
    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;



    @RequestMapping(method = RequestMethod.POST, value = "AllTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AllTemp")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<WebSiteCodeTempGrid>> getAllJavaTemps(String dsmTempId, DSMType dsmType) {
        ListResultModel<List<WebSiteCodeTempGrid>> result = new ListResultModel();
        DSMBean bean = null;
        try {
            bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null&& javaTemp.getDsmType()!=null  && (dsmType == null || javaTemp.getDsmType().equals(dsmType))) {
                        temps.add(javaTemp);
                    }
                }
            }
            result = PageUtil.getDefaultPageList(temps, WebSiteCodeTempGrid.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }




    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "loadChildDSM")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public TreeListResultModel<List<WebSiteTreeView>> loadChildDSM(DSMType dsmType, String dsmTempId) {
        TreeListResultModel<List<WebSiteTreeView>> result = new TreeListResultModel<List<WebSiteTreeView>>();

        List<Enumstype> dsmtypes = new ArrayList<>();
        switch (dsmType) {
            case view:
                ViewType[] viewTypes = ViewType.values();
                dsmtypes.addAll(Arrays.asList(viewTypes));
                break;
            case repository:
                RepositoryType[] repositoryTypes = RepositoryType.values();
                dsmtypes.addAll(Arrays.asList(repositoryTypes));
                break;
            case aggregation:
                AggregationType[] aggregationTypes = AggregationType.values();
                dsmtypes.addAll(Arrays.asList(aggregationTypes));
                break;
            case customDomain:
                CustomDomainType[] domainTypes = CustomDomainType.values();
                dsmtypes.addAll(Arrays.asList(domainTypes));
                break;

        }

        result = TreePageUtil.getTreeList(dsmtypes, WebSiteTreeView.class);


        return result;

    }

    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }

}
