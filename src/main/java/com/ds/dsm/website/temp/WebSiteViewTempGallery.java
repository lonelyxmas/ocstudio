package com.ds.dsm.website.temp;


import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.gallery.annotation.GalleryAnnotation;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.enums.ComponentType;

@GalleryAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {WebSiteJavaTempService.class})
public class WebSiteViewTempGallery {

    @CustomAnnotation(pid = true, hidden = true)
    String fileId;

    @CustomAnnotation()
    String caption;


    @CustomAnnotation()
    String javaTempId;


    @CustomAnnotation(uid = true, hidden = true)
    String id;
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String comment;

    @FieldAnnotation( componentType = ComponentType.Image)
    @CustomAnnotation(caption = "图片")
    String image = "/RAD/img/project.png";


    public WebSiteViewTempGallery() {

    }

    public WebSiteViewTempGallery(JavaTemp temp) {
        this.comment = temp.getName();
        this.fileId = temp.getFileId();
        if (temp.getImage() != null && !temp.getImage().equals("")) {
            this.image = temp.getImage();
        }

        this.id = temp.getJavaTempId();
        this.caption = "";
        this.javaTempId = temp.getJavaTempId();


    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
