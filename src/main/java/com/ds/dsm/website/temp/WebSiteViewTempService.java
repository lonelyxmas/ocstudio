package com.ds.dsm.website.temp;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.dsm.manager.temp.tree.DSMInstPopTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GalleryViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.annotation.ViewType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/website/temp/")
@MethodChinaName(cname = "模板管理")
public class WebSiteViewTempService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewJavaTemp")
    @GalleryViewAnnotation
    @ModuleAnnotation( caption = "视图模板")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<WebSiteViewTempGallery>> getViewJavaTemps(String dsmTempId, ViewType viewType) {
        ListResultModel<List<WebSiteViewTempGallery>> result = new ListResultModel();
        DSMBean bean = null;
        try {
            bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null) {
                        if (viewType != null && javaTemp.getDsmType().equals(DSMType.aggregation) && javaTemp.getViewType().equals(viewType)) {
                            temps.add(javaTemp);
                        }
                    }
                }
                result = PageUtil.getDefaultPageList(temps, WebSiteViewTempGallery.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }


    @MethodChinaName(cname = "移除视图模板")
    @RequestMapping(method = RequestMethod.POST, value = "removeRefViewTemp")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> removeRefViewTemp(String id, String dsmTempId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (id == null) {
                throw new JDSException("javaTempId is bull");
            }
            DSMBean dsmBean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            dsmBean.getJavaTempIds().remove(id);
            DSMFactory.getInstance().getTempManager().updateTempBean(dsmBean);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "更新模板信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateDSMRefView")
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateDSMRefView(String AddViewTempTree, String dsmTempId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (AddViewTempTree == null) {
                throw new JDSException("javaTempId is bull");
            }
            DSMBean dsmBean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            if (AddViewTempTree != null) {
                String[] tempIds = StringUtility.split(AddViewTempTree, ";");
                if (tempIds.length > 0) {
                    dsmBean.getJavaTempIds().clear();
                    for (String id : tempIds) {
                        JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(id);
                        if (temp != null) {
                            dsmBean.getJavaTempIds().add(id);
                        }
                    }
                }
            }
            DSMFactory.getInstance().getTempManager().updateTempBean(dsmBean);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @RequestMapping(value = {"AddViewTemp"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation(saveUrl = "dsm.website.temp.updateDSMRefView")
    @ModuleAnnotation(dynLoad = true,caption = "添加视图模板")
    @APIEventAnnotation(autoRun = true)
    @DialogAnnotation( width = "300", height = "450")
    @ResponseBody
    public TreeListResultModel<List<DSMInstPopTree>> getViewTempTree(String dsmTempId, ViewType viewType) {
        TreeListResultModel<List<DSMInstPopTree>> result = new TreeListResultModel<List<DSMInstPopTree>>();
        try {
            List<DSMInstPopTree> treeList = new ArrayList<DSMInstPopTree>();
            DSMInstPopTree listItem = null;
            if (viewType != null) {
                listItem = new DSMInstPopTree(viewType, null,"DSMTempRoot", null, true);
            }
            DSMBean dsmBean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            treeList.add(listItem);
            result.setData(treeList);
            result.setIds(Arrays.asList(dsmBean.getJavaTempIds().toArray(new String[]{})));
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


}
