package com.ds.dsm.website.temp.service;


import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.dsm.admin.temp.domain.DomainTempGrid;
import com.ds.dsm.website.temp.WebSiteCodeTempGrid;
import com.ds.enums.IconEnumstype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.*;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/website/domain/")
@MethodChinaName(cname = "通用领域", imageClass = "spafont spa-icon-conf")

public class WebSiteDomainService {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;


    @RequestMapping(method = RequestMethod.POST, value = "AllTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AllTemp")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<WebSiteCodeTempGrid>> getAllTempGrid(String dsmTempId) {
        ListResultModel<List<WebSiteCodeTempGrid>> resultModel = new ListResultModel();
        try {
            DSMBean bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null && javaTemp.getDsmType() != null && (javaTemp.getDsmType().equals(DSMType.customDomain))) {
                        temps.add(javaTemp);
                    }
                }
            }
            resultModel = PageUtil.getDefaultPageList(temps, WebSiteCodeTempGrid.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadChildCustomDomain")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> loadChildCustomDomain(CustomDomainType customDomainType) {

        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel();
        List<IconEnumstype> domainTypes = new ArrayList<>();
        switch (customDomainType) {
            case bpm:
                BpmDomainType[] types = BpmDomainType.values();
                domainTypes.addAll(Arrays.asList(types));
                break;
            case msg:
                MsgDomainType[] msgtypes = MsgDomainType.values();
                domainTypes.addAll(Arrays.asList(msgtypes));
                break;
            case org:
                OrgDomainType[] orgtypes = OrgDomainType.values();
                domainTypes.addAll(Arrays.asList(orgtypes));
                break;
            case nav:
                NavDomainType[] navtypes = NavDomainType.values();
                domainTypes.addAll(Arrays.asList(navtypes));
                break;
            case vfs:
                VfsDomainType[] vfstypes = VfsDomainType.values();
                domainTypes.addAll(Arrays.asList(vfstypes));
                break;
        }

        resultModel = TreePageUtil.getTreeList(domainTypes, JavaTempNavTree.class);
        return resultModel;
    }

    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }
}
