package com.ds.dsm.website.temp.action;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.temp.tree.DSMInstPopTree;
import com.ds.dsm.website.select.WebSiteSelectNav;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.ViewType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(path = "/dsm/website/temp/")
@Aggregation(type = AggregationType.menu,rootClass = WebSiteImportAction.class)
public class WebSiteImportAction {


    @MethodChinaName(cname = "业务模板")
    @RequestMapping(method = RequestMethod.POST, value = "AddTempFromWebSite")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @DialogAnnotation(width = "900",height = "620")
    @ModuleAnnotation(
             caption = "业务模板", imageClass = "spafont spa-icon-c-menubar")
    @ResponseBody
    public ResultModel<WebSiteSelectNav> addTempFromWebSite(String projectVersionName, String domainId, String viewInstId) {
        ResultModel<WebSiteSelectNav> result = new ResultModel<WebSiteSelectNav>();
        return result;
    }


    @MethodChinaName(cname = "添加JAVA模板")
    @RequestMapping(value = {"AddTempFromJaveTemp"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation(
            saveUrl = "dsm.website.select.addTempFromJava")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-flash", caption = "添加JAVA模板",
            dynLoad = true
            )

    @DialogAnnotation(width = "300",
            height = "450")
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<DSMInstPopTree>> addTempFromJaveTemp(String viewInstId, String projectVersionName, String sourceClassName, String domainId, ViewType viewType, AggregationType aggregationType, RepositoryType repositoryType) {
        TreeListResultModel<List<DSMInstPopTree>> result = new TreeListResultModel<List<DSMInstPopTree>>();
        try {
            DSMInstPopTree listItem = new DSMInstPopTree(sourceClassName, null, domainId, viewInstId, true);
            List<DSMInstPopTree> itemList = new ArrayList<>();
            Set<String> javaTempIds = new HashSet<>();
            DSMType dsmType = null;

            if ( DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName) != null) {
                dsmType = DSMType.repository;
            } else if (DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId) != null) {
                dsmType = DSMType.aggregation;
            } else if (DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId) != null) {
                dsmType = DSMType.view;
            } else {
                dsmType = DSMType.customDomain;
            }

            switch (dsmType) {
                case repository:
                    listItem = new DSMInstPopTree(DSMType.repository, projectVersionName, null, null, sourceClassName, true);
                    if (repositoryType != null) {
                        listItem = new DSMInstPopTree(repositoryType, "RepositoryRoot", projectVersionName, true);
                    }

                    RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                    if (repositoryInst != null) {
                        javaTempIds = repositoryInst.getJavaTempIds();
                    }

                    break;
                case view:
                    listItem = new DSMInstPopTree(DSMType.view, projectVersionName, domainId, viewInstId, sourceClassName, true);
                    if (viewType != null) {
                        listItem = new DSMInstPopTree(viewType, sourceClassName, domainId, viewInstId, true);
                    }

                    if (sourceClassName != null && !sourceClassName.equals("")) {
                        ViewEntityConfig esdClassConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
                        if (esdClassConfig != null) {
                            javaTempIds = esdClassConfig.getJavaTempIds();
                        }
                    } else {
                        ViewInst tempBean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
                        if (tempBean != null) {
                            javaTempIds = tempBean.getJavaTempIds();
                        }
                    }
                    break;
                case aggregation:
                    if (aggregationType != null  && !aggregationType.equals(AggregationType.customapi)) {
                        listItem = new DSMInstPopTree(aggregationType, "AggregationType", domainId, true);
                    }
                    if (sourceClassName != null && !sourceClassName.equals("")) {
                        AggEntityConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
                        javaTempIds = esdClassConfig.getJavaTempIds();
                    } else {
                        DomainInst tempBean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                        javaTempIds = tempBean.getJavaTempIds();
                    }
                    break;
            }

            itemList.add(listItem);
            result.setData(itemList);
            result.setIds(Arrays.asList(javaTempIds.toArray(new String[]{})));
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


}

