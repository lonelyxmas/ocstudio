package com.ds.dsm.website.temp.service;


import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.admin.temp.JavaTempGallery;
import com.ds.dsm.admin.temp.aggreagtion.AggregationTempForm;
import com.ds.dsm.admin.temp.aggreagtion.AggregationTempGrid;
import com.ds.dsm.website.temp.WebSiteCodeTempGrid;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/website/agg/")
@MethodChinaName(cname = "聚合模板", imageClass = "spafont spa-icon-conf")
public class WebSiteAggregationService {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;



    @RequestMapping(method = RequestMethod.POST, value = "TempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "TempGrid")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<WebSiteCodeTempGrid>> getTempGrid(String dsmTempId, AggregationType aggregationType) {
        ListResultModel<List<WebSiteCodeTempGrid>> resultModel = new ListResultModel();
        try {
            DSMBean bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);
                    if (javaTemp != null && javaTemp.getAggregationType()!=null && (aggregationType == null || javaTemp.getAggregationType().equals(aggregationType))) {
                        temps.add(javaTemp);
                    }
                }
            }
            resultModel = PageUtil.getDefaultPageList(temps, WebSiteCodeTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }
}
