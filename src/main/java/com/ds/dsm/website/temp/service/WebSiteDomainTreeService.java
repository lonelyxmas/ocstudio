package com.ds.dsm.website.temp.service;


import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.dsm.admin.temp.domain.DomainTempGrid;
import com.ds.dsm.website.temp.WebSiteCodeTempGrid;
import com.ds.enums.IconEnumstype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.temp.DSMBean;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/website/customdomain/")
@MethodChinaName(cname = "资源库模板", imageClass = "spafont spa-icon-conf")
public class WebSiteDomainTreeService {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmTempId;


    @RequestMapping(method = RequestMethod.POST, value = "AllCustomDomainTempGrid")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "AllRepositoryTempGrid")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<WebSiteCodeTempGrid>> getAllCustomDomainTempGrid(IconEnumstype itemDomainType, String dsmTempId, CustomDomainType customDomainType) {
        ListResultModel<List<WebSiteCodeTempGrid>> resultModel = new ListResultModel();
        try {
            DSMBean bean = DSMFactory.getInstance().getTempManager().getDSMBeanById(dsmTempId);
            List<JavaTemp> temps = new ArrayList<>();
            if (bean != null) {
                for (String javaid : bean.getJavaTempIds()) {
                    JavaTemp javaTemp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaid);

                    switch (customDomainType) {
                        case bpm:
                            if (javaTemp != null && javaTemp.getBpmDomainType() != null && (javaTemp.getBpmDomainType().equals(itemDomainType))) {
                                temps.add(javaTemp);
                            }
                            break;
                        case msg:
                            if (javaTemp != null && javaTemp.getMsgDomainType() != null & (javaTemp.getMsgDomainType().equals(itemDomainType))) {
                                temps.add(javaTemp);
                            }
                            break;
                        case org:
                            if (javaTemp != null && javaTemp.getOrgDomainType() != null & (javaTemp.getOrgDomainType().equals(itemDomainType))) {
                                temps.add(javaTemp);
                            }

                            break;
                        case nav:
                            if (javaTemp != null && javaTemp.getNavDomainType() != null & (javaTemp.getNavDomainType().equals(itemDomainType))) {
                                temps.add(javaTemp);
                            }
                            break;

                    }
                }
            }
            resultModel = PageUtil.getDefaultPageList(temps, WebSiteCodeTempGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    public String getDsmTempId() {
        return dsmTempId;
    }

    public void setDsmTempId(String dsmTempId) {
        this.dsmTempId = dsmTempId;
    }
}
