package com.ds.dsm.website;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.tool.ui.component.list.TreeListItem;

import java.util.HashMap;

@TreeAnnotation(heplBar = true)
@TabsAnnotation(singleOpen = true)
public class DomainTempNavTree extends TreeListItem {


    public DomainTempNavTree(String className, String domainId) {
        this.caption = "所有模型";
        this.setId("DSMTempRoot");
        this.setIniFold(false);
        DSMTempType[] dsmTypes = DSMTempType.values();
        for (DSMTempType type : dsmTypes) {
            this.addChild(new DomainTempNavTree(type, className, domainId));

        }
    }

    public DomainTempNavTree(DSMTempType dsmType, String className, String domainId) {
        this.caption = dsmType.getName();
        this.imageClass = dsmType.getImageClass();
        this.setEuClassName(className);
        this.setId(dsmType.getType());
        this.setIniFold(false);
        this.tagVar = new HashMap<>();
        tagVar.put("dsmTempType", dsmType.getType());
        tagVar.put("domainId", domainId);

    }
}
