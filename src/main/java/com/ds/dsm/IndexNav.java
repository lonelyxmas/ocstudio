package com.ds.dsm;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.db.nav.DatabaseNav;
import com.ds.bpm.plugins.nav.BPDManagerTree;
import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.nav.DSMNavItems;
import com.ds.dsm.nav.DSMNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavFoldingTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGalleryAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.index.annotation.IndexAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutItemAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.LayoutType;
import com.ds.esd.tool.ui.enums.PosType;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/nav/")
@MethodChinaName(cname = "DSM导航")
@IndexAnnotation
@LayoutAnnotation(transparent = false, type = LayoutType.vertical, items = {@LayoutItemAnnotation(panelBgClr = "#3498DB", size = 28, pos = PosType.before, locked = true, moveDisplay = false, cmd = false)})
public class IndexNav {


    @CustomAnnotation(hidden = true, pid = true)
    public String projectVersionName;


    @MethodChinaName(cname = "DSM建模")
    @RequestMapping(method = RequestMethod.POST, value = "DSMInstNavTree")
    @NavTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-conf", caption = "DSM建模")
    @CustomAnnotation(index = 1)
    @ResponseBody
    public TreeListResultModel<List<DSMNavTree>> getDSMInstNavTree(String projectVersionName) {

        return TreePageUtil.getTreeList(Arrays.asList(DSMNavItems.values()), DSMNavTree.class);
        //  return new ResultModel<DSMInstNavTree>();
    }


    @MethodChinaName(cname = "流程配置")
    @RequestMapping(method = RequestMethod.POST, value = "BPDManagerTree")
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", caption = "流程配置", cache = false)
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ResultModel<BPDManagerTree> getBPMManager(String projectVersionName) {
        return new ResultModel<BPDManagerTree>();
    }

    @MethodChinaName(cname = "数据库")
    @RequestMapping(method = RequestMethod.POST, value = "DatabaseNav")
    @NavFoldingTreeViewAnnotation
    @ModuleAnnotation(imageClass = "iconfont iconchucun", caption = "数据库", cache = false)
    @CustomAnnotation(index = 3)
    @ResponseBody
    public ResultModel<DatabaseNav> getDatabaseNav(String projectVersionName) {
        return new ResultModel<DatabaseNav>();
    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    @JSONField(serialize = false)
    public ESDClient getEsdClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }
}
