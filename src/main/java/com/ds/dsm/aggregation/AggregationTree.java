package com.ds.dsm.aggregation;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Pid;

import java.util.List;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, caption = "JAVA模板")
public class AggregationTree extends TreeListItem {


    @Pid
    String domainId;
    @Pid
    AggregationType aggregationType;
    @Pid
    DSMType dsmType;
    @Pid
    String sourceClassName;
    @Pid
    String javaTempId;

    public AggregationTree(String domainId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = "模板分类";
        this.id = "ViewRoot";
        this.euClassName = "dsm.agg.temp.AggTempGrid";
        this.iniFold = false;
        this.addTagVar("aggregationType", AggregationType.customDomain);
        this.imageClass = "spafont spa-icon-settingprj";
        AggregationType[] aggregationTypes = AggregationType.values();
        for (AggregationType aggregationType : aggregationTypes) {
            if (hasTemp) {
                List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getAggregationTemps(aggregationType);
                if (javaTemps.size() > 0) {
                    AggregationTree dsmInstItem = new AggregationTree(aggregationType, domainId, sourceClassName, hasTemp);
                    this.addChild(dsmInstItem);
                }
            } else {
                this.addChild(new AggregationTree(aggregationType, domainId, sourceClassName, hasTemp));
            }
        }
    }

    public AggregationTree(AggregationType aggregationType, String domainId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = aggregationType.getName();
        this.id = aggregationType.getType();
        this.iniFold = false;
        this.euClassName = "dsm.agg.temp.AggTempGrid";
        this.addTagVar("aggregationType", aggregationType.getType());
        this.addTagVar("domainId", domainId);
        this.addTagVar("dsmType", DSMType.aggregation);
        this.addTagVar("sourceClassName", sourceClassName);
        this.setImageClass(aggregationType.getImageClass());

        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getAggregationTemps(aggregationType);
            if (javaTemps.size() > 0) {
                for (JavaTemp javaTemp : javaTemps) {
                    AggregationTree dsmInstItem = new AggregationTree(javaTemp, domainId, sourceClassName);
                    dsmInstItem.setIniFold(true);
                    this.addChild(dsmInstItem);
                }
            } else {
                this.addChild(new AggregationTree(aggregationType, domainId, sourceClassName, hasTemp));
            }
        }
    }


    public AggregationTree(JavaTemp javaTemp, String sourceClassName, String domainId) {
        this.caption = javaTemp.getName();
        this.imageClass = "spafont spa-icon-page";
        this.id = javaTemp.getJavaTempId();
        if (javaTemp.getAggregationType() != null) {
            this.addTagVar("aggregationType", javaTemp.getAggregationType().getType());
        }
        this.addTagVar("domainId", domainId);
        this.addTagVar("dsmType", DSMType.aggregation);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("javaTempId", javaTemp.getJavaTempId());
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public AggregationType getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(AggregationType aggregationType) {
        this.aggregationType = aggregationType;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }
}
