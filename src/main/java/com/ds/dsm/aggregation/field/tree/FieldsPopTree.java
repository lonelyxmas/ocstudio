package com.ds.dsm.aggregation.field.tree;


import com.ds.common.JDSException;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.ESDField;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Close, CustomFormMenu.Save})
@PopTreeAnnotation(caption = "添加字段")
public class FieldsPopTree extends TreeListItem {
    @CustomAnnotation(pid = true)
    String pattern = "";

    @CustomAnnotation(pid = true)
    String projectId;

    @CustomAnnotation(pid = true)
    String formulaInstId;

    @CustomAnnotation(pid = true)
    String parameterCode;

    @CustomAnnotation(pid = true)
    String paramsType;


    public FieldsPopTree(String pattern, ESDClass esdClass) throws JDSException {
        super(esdClass.getName(), esdClass.getDesc() + "(" + esdClass.getName() + ")");
        for (ESDField esdField : esdClass.getFieldList()) {
            if (pattern(pattern, esdField)) {
                this.addChild(new FieldsPopTree(esdField));
            }

        }
    }

    public FieldsPopTree(ESDField esdField) throws JDSException {
        super(esdField.getName(), esdField.getCaption());
    }

    private boolean pattern(String pattern, ESDField colInfo) {
        if (pattern != null && !pattern.equals("")) {
            Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher namematcher = p.matcher(colInfo.getName());
            Matcher cnnamematcher = p.matcher(colInfo.getCaption());
            Matcher fieldmatcher = p.matcher(colInfo.getDesc());
            if (namematcher.find() || cnnamematcher.find() || fieldmatcher.find()) {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }


    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParamsType() {
        return paramsType;
    }

    public void setParamsType(String paramsType) {
        this.paramsType = paramsType;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
