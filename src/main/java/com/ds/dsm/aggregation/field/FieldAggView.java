package com.ds.dsm.aggregation.field;

import com.ds.dsm.repository.db.action.ColAction;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.aggregation.FieldAggConfig;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;

@PageBar(pageCount = 100)
@RowHead(selMode = SelModeType.none, gridHandlerCaption = "排序|隐藏", rowHandlerWidth = "8em", rowNumbered = false)
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, menuClass = {ColAction.class})
@GridAnnotation(customService = {FieldAggService.class}, customMenu = {GridMenu.Reload},event = CustomGridEvent.editor)
public class FieldAggView {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    String methodName;

    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "显示名称", readonly = true)
    private String caption;


    @CustomAnnotation(caption = "主键", readonly = true)
    Boolean uid;


    @CustomAnnotation(caption = "禁用")
    Boolean disabled;


    @CustomAnnotation(caption = "只读")
    Boolean readonly;

    @CustomAnnotation(caption = "隐藏")
    Boolean colHidden;


    @FieldAnnotation(colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "过滤脚本")
    String filter;

    @FieldAnnotation( colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "默认值公式")
    String expression;



    @CustomAnnotation(hidden = true, pid = true)
    String entityClassName;


    public FieldAggView(FieldAggConfig config) {
        this.uid = getUid();

        this.entityClassName = config.getEntityClassName();
        this.domainId = config.getDomainId();
        this.caption = config.getCaption();
        this.fieldname = config.getFieldname();
        this.colHidden = config.getColHidden();
        this.expression = config.getExpression();
        this.uid = config.getUid();

        this.methodName = config.getMethodName();
        this.readonly = config.getReadonly();

        this.disabled = config.getDisabled();


    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Boolean getUid() {
        return uid;
    }

    public void setUid(Boolean uid) {
        this.uid = uid;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }


    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Boolean getColHidden() {
        return colHidden;
    }

    public void setColHidden(Boolean colHidden) {
        this.colHidden = colHidden;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }
}
