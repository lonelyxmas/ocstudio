package com.ds.dsm.aggregation.field;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.FieldAggConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;

import java.util.List;
import java.util.Set;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, lazyLoad = true, dynDestory = true)
public class AggFieldTree extends TreeListItem {


    public AggFieldTree(AggEntityConfig entityConfig) {
        this.caption = "字段列表";
        this.imageClass = "spafont spa-icon-c-comboinput";
        this.setEuClassName("dsm.agg.field.config.FieldAggList");
        this.setId(entityConfig.getDomainId() + entityConfig.getESDClass().getClassName());
        this.setIniFold(false);
        this.addTagVar("sourceClassName", entityConfig.getSourceClassName());
        this.addTagVar("entityClassName", entityConfig.getESDClass().getEntityClassName());
        this.addTagVar("domainId", entityConfig.getDomainId());
        this.addTagVar("viewType", ModuleViewType.FormConfig);
        this.addTagVar("entityClassName", entityConfig.getESDClass().getEntityClass().getClassName());
        List<String> fieldNames = entityConfig.getFieldNames();
        this.setDynDestory(true);
        for (String fieldname : fieldNames) {
            FieldAggConfig fieldFormConfig = entityConfig.getFieldByName(fieldname);
            if (fieldFormConfig != null) {

                this.addChild(new AggFieldTree(fieldFormConfig,entityConfig.getSourceClassName()));
            }
        }
    }



    public AggFieldTree(FieldAggConfig fieldFormInfo,String soruceClassName) {
        this.caption = fieldFormInfo.getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        this.setId(fieldFormInfo.getFieldname() + "_" + fieldFormInfo.getEntityClassName() + "_" + fieldFormInfo.getMethodName());
        this.setIniFold(false);

        this.imageClass = fieldFormInfo.getImageClass();
        if (imageClass == null || imageClass.equals("")) {
            if ( fieldFormInfo.getComponentType() != null) {
                imageClass =  fieldFormInfo.getComponentType().getImageClass();
            }
        }

        this.setEuClassName("dsm.agg.field.config.FieldInfo");
        this.addTagVar("sourceClassName", soruceClassName);
        this.addTagVar("entityClassName", fieldFormInfo.getEntityClassName());
        this.addTagVar("domainId", fieldFormInfo.getDomainId());
        this.addTagVar("fieldname", fieldFormInfo.getFieldname());
    }

}
