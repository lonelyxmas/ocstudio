package com.ds.dsm.aggregation.field.config;

import com.ds.dsm.aggregation.field.FieldAggService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.JavaEditorAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.toolbar.CodeEditorTools;
import com.ds.esd.dsm.aggregation.FieldAggConfig;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.StretchType;

@BottomBarMenu
@FormAnnotation(stretchHeight = StretchType.last, col = 1, customService = FieldAggService.class, bottombarMenu = CustomFormMenu.Save)
public class AggFieldFormView {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    String methodName;


    @CustomAnnotation(hidden = true, pid = true)
    String entityClassName;

    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "显示名称", readonly = true)
    private String caption;

    @ComboListBoxAnnotation()
    @FieldAnnotation(required = true, colSpan = -1)
    @CustomListAnnotation(filter = "source.isAbsValue()")
    @CustomAnnotation(caption = "控件类型")
    private ComponentType componentType;

    @CustomAnnotation(caption = "主键", readonly = true)
    Boolean uid;
    @CustomAnnotation(caption = "禁用")
    Boolean disabled;

    @CustomAnnotation(caption = "只读")
    Boolean readonly;

    @CustomAnnotation(caption = "是否隐藏")
    Boolean hidden;

    @JavaEditorAnnotation
    @ToolBarMenu(menuClasses = CodeEditorTools.class)
    @FieldAnnotation(colSpan = -1, haslabel = false, rowHeight = "350")
    @CustomAnnotation(caption = "默认值公式")
    String expression;


    public AggFieldFormView() {

    }

    public AggFieldFormView(FieldAggConfig config) {
        this.uid = config.getUid();
        this.entityClassName = config.getEntityClassName();
        this.domainId = config.getDomainId();
        this.componentType = config.getComponentType();
        this.caption = config.getCaption();
        this.fieldname = config.getFieldname();
        this.hidden = config.getColHidden();
        this.expression = config.getExpression();
        this.uid = config.getUid();

        this.methodName = config.getMethodName();
        this.readonly = config.getReadonly();
        this.disabled = config.getDisabled();


    }


    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }


    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getUid() {
        return uid;
    }

    public void setUid(Boolean uid) {
        this.uid = uid;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

}
