package com.ds.dsm.aggregation.field;

import com.alibaba.fastjson.JSONObject;
import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.field.config.AggFieldFormView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.FieldAggConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/field/config/")
@MethodChinaName(cname = "字段配置", imageClass = "spafont spa-icon-c-comboinput")

public class FieldAggService {


    @MethodChinaName(cname = "清空配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "clearFieldForm")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clearFieldForm(String entityClassName, String fieldname, String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (entityClassName != null && !entityClassName.equals("")) {
                AggEntityConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(entityClassName, domainId);
                esdClassConfig.getAllFieldMap().remove(fieldname);
                DSMFactory.getInstance().getAggregationManager().updateAggEntityConfig(esdClassConfig);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "编辑字段配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFieldForm")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateFieldForm(@RequestBody AggFieldFormView config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getEntityClassName();
            if (className != null && !className.equals("")) {
                AggEntityConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(className, config.getDomainId());
                String json = JSONObject.toJSONString(config);
                FieldAggConfig fieldAggConfig = JSONObject.parseObject(json, FieldAggConfig.class);
                esdClassConfig.getAllFieldMap().put(config.getFieldname(), fieldAggConfig);
                DSMFactory.getInstance().getAggregationManager().updateAggEntityConfig(esdClassConfig);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "字段信息")
    @RequestMapping(method = RequestMethod.POST, value = "FieldInfo")
    @FormViewAnnotation
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<AggFieldFormView> getColInfo(String entityClassName, String domainId, String fieldname) {
        ResultModel<AggFieldFormView> result = new ResultModel<AggFieldFormView>();
        try {
            AggFieldFormView view = null;
            AggEntityConfig classConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(entityClassName, domainId);
            FieldAggConfig formInfo = classConfig.getFieldByName(fieldname);
            if (formInfo != null) {
                view = new AggFieldFormView(formInfo);
            } else {
                view = new AggFieldFormView();
            }

            result.setData(view);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }
}
