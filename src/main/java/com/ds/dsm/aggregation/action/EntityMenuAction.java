package com.ds.dsm.aggregation.action;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.AggregationManager;
import com.ds.esd.dsm.aggregation.DomainInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/action/")
public class EntityMenuAction {


    @RequestMapping(method = RequestMethod.POST, value = "dsmEntityBuild")
    @APIEventAnnotation(customRequestData = RequestPathEnum.form)
    @CustomAnnotation(index = 0, caption = "编译", imageClass = "spafont spa-icon-coin")
    @ResponseBody
    public ResultModel<Boolean> dsmEntityBuild(String domainId, String sourceClassName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        try {
            AggregationManager manager = DSMFactory.getInstance().getAggregationManager();
            DomainInst dsmInst = manager.getDomainInstById(domainId);
            AggEntityConfig esdClassConfig = manager.getAggEntityConfig(sourceClassName, domainId);
            manager.genAggRootJava(dsmInst, esdClassConfig, null, chrome);

            DSMFactory.getInstance().compileDsmInst(dsmInst, chrome);
            DSMFactory.getInstance().reload();

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    public ESDChrome getCurrChromeDriver() {
        ESDChrome chrome = JDSActionContext.getActionContext().Par("$currChromeDriver", ESDChrome.class);
        return chrome;
    }


}

