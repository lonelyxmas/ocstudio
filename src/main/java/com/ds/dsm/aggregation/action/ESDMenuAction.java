package com.ds.dsm.aggregation.action;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/agg/action/")
public class ESDMenuAction {



    @MethodChinaName(cname = "编译")
    @RequestMapping(method = RequestMethod.POST, value = "dsmBuild")
    @CustomAnnotation(imageClass = "xuicon spafont spa-icon-debug1")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> dsmBuildView(String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (domainId != null) {
                String[] dsmIds = StringUtility.split(domainId, ";");
                Set<DomainInst> beans = new LinkedHashSet<>();
                for (String id : dsmIds) {
                    DomainInst dsmInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(id);
                    DSMFactory.getInstance().getAggregationManager().buildDomain(dsmInst, getCurrChromeDriver(), true);
                }
                ESDFacrory.getInstance().dyReload(null);
                getCurrChromeDriver().printLog("编译完成！", true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = new ErrorResultModel<>();

            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    public ESDChrome getCurrChromeDriver() {
        ESDChrome chrome = JDSActionContext.getActionContext().Par("$currChromeDriver", ESDChrome.class);
        return chrome;
    }


}

