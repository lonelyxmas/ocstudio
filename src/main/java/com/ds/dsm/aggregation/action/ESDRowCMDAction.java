package com.ds.dsm.aggregation.action;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/agg/action/row/")
public class ESDRowCMDAction {


    @MethodChinaName(cname = "编译")
    @RequestMapping(method = RequestMethod.POST, value = "dsmBuild")
    @CustomAnnotation(imageClass = "xuicon spafont spa-icon-debug1")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> dsmBuild(String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (domainId != null) {
                String[] dsmIds = StringUtility.split(domainId, ";");
                Set<DomainInst> beans = new LinkedHashSet<>();
                for (String id : dsmIds) {
                    DomainInst dsmInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(id);
                    DSMFactory.getInstance().getAggregationManager().buildAggregation(dsmInst, dsmInst.getJavaTempIds(), getCurrChromeDriver());
                    //DSMFactory.getInstance().getAggregationManager().buildCustomService(dsmInst, dsmInst.getJavaTempIds(), getCurrChromeDriver());
                    DSMFactory.getInstance().compileDsmInst(dsmInst, getCurrChromeDriver());
                }
                getCurrChromeDriver().printLog("重新装载服务...", true);
                reload();
                getCurrChromeDriver().printLog("编译完成！", true);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "删除")
    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "fa fa-lg fa-close")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> delete(String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            DSMFactory.getInstance().deleteDomainInst(domainId);

        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


    void rebuildCustomModule(String projectId, String packageName, String esdPackageName) {

        try {
            Map map = new HashMap();
            map.put("projectId", projectId);
            ESDChrome chrome = getCurrChromeDriver();
            ESDFacrory.getESDClient().buildCustomModule(projectId, packageName, esdPackageName, map, chrome);

        } catch (JDSException e) {
            e.printStackTrace();
        }

    }


    ResultModel<Boolean> reload() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ESDChrome chrome = getCurrChromeDriver();
        chrome.printLog("正在装载编译，请稍后！", true);
        try {
            ESDFacrory.getInstance().dyReload(null);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;

    }


    public ESDChrome getCurrChromeDriver() {
        ESDChrome chrome = JDSActionContext.getActionContext().Par("$currChromeDriver", ESDChrome.class);
        return chrome;
    }


}

