package com.ds.dsm.aggregation.module.panel.custom.dock;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.*;
import com.ds.esd.custom.bean.enums.PanelType;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.component.dialog.DialogBean;
import com.ds.web.json.JSONData;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/config/module/")
public class ModuleDockService {

    @RequestMapping(method = RequestMethod.POST, value = "DockConfig")
    @FormViewAnnotation(autoSave = true)
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "基础信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ModuleDockView> getDockConfig(String sourceClassName, String methodName, String domainId) {
        ResultModel<ModuleDockView> result = new ResultModel<ModuleDockView>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            DockBean dockBean = new DockBean();

            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            PanelType panelType = moduleBean.getPanelType();

            switch (panelType) {
                case dialog:
                    DialogBean dialogBean = moduleBean.getDialogBean();
                    dockBean = dialogBean.getContainerBean().getDockBean();
                    break;
                case block:
                    CustomBlockBean blockBean = moduleBean.getBlockBean();
                    dockBean = blockBean.getContainerBean().getDockBean();
                    break;
            }
            result.setData(new ModuleDockView(dockBean, sourceClassName, methodName, domainId));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "updateDock")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateDock(@RequestBody DockBean dockBean, String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            PanelType panelType = moduleBean.getPanelType();

            switch (panelType) {
                case dialog:
                    DialogBean dialogBean = moduleBean.getDialogBean();

                    dialogBean.getContainerBean().setDockBean(dockBean);

                    break;
                case block:
                    CustomBlockBean blockBean = moduleBean.getBlockBean();
                    blockBean.getContainerBean().setDockBean(dockBean);
                    break;
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetDock")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetDock(@JSONData DockBean dockBean, String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            PanelType panelType = moduleBean.getPanelType();

            switch (panelType) {
                case dialog:
                    DialogBean dialogBean = moduleBean.getDialogBean();
                    dialogBean.getContainerBean().setDockBean(new DockBean(AnnotationUtil.getAllAnnotations(customMethodAPIBean.getMethod())));
                    break;
                case block:
                    CustomBlockBean blockBean = moduleBean.getBlockBean();
                    blockBean.getContainerBean().setDockBean(new DockBean(AnnotationUtil.getAllAnnotations(customMethodAPIBean.getMethod())));
                    break;
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
