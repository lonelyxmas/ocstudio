package com.ds.dsm.aggregation.module.event;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.component.event.Event;
import com.ds.web.annotation.Uid;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Add, GridMenu.Delete, GridMenu.Reload}, customService = ModuleEventService.class)
public class ModuleEventView {

    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;
    @CustomAnnotation(hidden = true, pid = true)
    String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @Uid
    String eventName;

    @CustomAnnotation(caption = "名称")
    public String desc;

    @CustomAnnotation(caption = "事件")
    String eventKey;

    @CustomAnnotation(caption = "动作操作")
    public String eventType;

    @CustomAnnotation(caption = "脚本")
    public String script;


    public ModuleEventView() {

    }


    public ModuleEventView(Event event, MethodConfig methodConfig) {
        this.methodName = methodConfig.getMethodName();
        this.domainId = methodConfig.getDomainId();
        this.sourceClassName = methodConfig.getSourceClassName();
        this.script = event.getScript();
        this.desc = (event.getDesc() == null || event.getDesc().equals("")) ? event.getEventType() : event.getDesc();
        this.eventType = event.getEventType();
        this.eventKey = event.getEventKey().getEvent();
        this.eventName = eventKey + "|" + event.getEventType();
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }


    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
