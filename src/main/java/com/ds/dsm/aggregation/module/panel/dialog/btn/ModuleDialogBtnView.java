package com.ds.dsm.aggregation.module.panel.dialog.btn;

import com.alibaba.fastjson.JSONObject;
import com.ds.dsm.aggregation.module.panel.dialog.DialogService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.DialogBtnBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu
@FormAnnotation(col = 1, customService = ModuleDialogBtnService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ModuleDialogBtnView {



    @CustomAnnotation(caption = "最小化")
    Boolean minBtn;
    @CustomAnnotation(caption = "最大化")
    Boolean maxBtn;

    @CustomAnnotation(caption = "刷新按钮")
    Boolean refreshBtn;

    @CustomAnnotation(caption = "Info按钮")
    Boolean infoBtn;
    @CustomAnnotation(caption = "恢复按钮")
    Boolean pinBtn;
    @CustomAnnotation(caption = "落地")
    Boolean landBtn;


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;


    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    public ModuleDialogBtnView() {

    }

    public ModuleDialogBtnView(CustomModuleBean config) {
        DialogBtnBean btnBean = config.getDialogBean().getDialogBtnBean();
        if (btnBean == null) {
            btnBean = new DialogBtnBean();
        }
        String json = JSONObject.toJSONString(btnBean);
        BeanMap.create(this).putAll(BeanMap.create(JSONObject.parseObject(json,ModuleDialogBtnView.class)));
        this.viewInstId = config.getViewInstId();
        this.domainId = config.getDomainId();
        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getMethodName();


    }

    public Boolean getInfoBtn() {
        return infoBtn;
    }

    public void setInfoBtn(Boolean infoBtn) {
        this.infoBtn = infoBtn;
    }

    public Boolean getMinBtn() {
        return minBtn;
    }

    public void setMinBtn(Boolean minBtn) {
        this.minBtn = minBtn;
    }

    public Boolean getMaxBtn() {
        return maxBtn;
    }

    public void setMaxBtn(Boolean maxBtn) {
        this.maxBtn = maxBtn;
    }

    public Boolean getRefreshBtn() {
        return refreshBtn;
    }

    public void setRefreshBtn(Boolean refreshBtn) {
        this.refreshBtn = refreshBtn;
    }

    public Boolean getPinBtn() {
        return pinBtn;
    }

    public void setPinBtn(Boolean pinBtn) {
        this.pinBtn = pinBtn;
    }

    public Boolean getLandBtn() {
        return landBtn;
    }

    public void setLandBtn(Boolean landBtn) {
        this.landBtn = landBtn;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
