package com.ds.dsm.aggregation.module.event;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.action.module.*;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.ModuleEventBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.enums.event.enums.ModuleEventEnum;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping(path = "/dsm/agg/entity/config/module/")
@MethodChinaName(cname = "模块事件", imageClass = "spafont spa-icon-event")
public class ModuleEventService {


    @RequestMapping(method = RequestMethod.POST, value = "ModuleEvents")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-statusbutton", caption = "模块事件")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<ModuleEventView>> getModuleEvents(String sourceClassName, String domainId, String methodName) {
        ListResultModel<List<ModuleEventView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            if (esdClassConfig!=null){
                MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
                List<ModuleEventView> menuViews = new ArrayList<>();
                List<ModuleEventBean> moduleEventBeans = apiCallBean.getModuleBean().getAllEvent();
                for (ModuleEventBean eventBean : moduleEventBeans) {
                    menuViews.add(new ModuleEventView(eventBean, apiCallBean));
                }
                resultModel = PageUtil.getDefaultPageList(menuViews);
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @PopTreeViewAnnotation()
    @ModuleAnnotation(dynLoad = true,  caption = "列表事件")
    @RequestMapping("ModuleEventTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<ModuleEventPopTree>> getGridEventTree(String sourceClassName, String methodName, String domainId, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<ModuleEventPopTree>> model = new TreeListResultModel<>();
        List<ModuleEventPopTree> popTrees = new ArrayList<>();
        ModuleEventPopTree eventPopTree = new ModuleEventPopTree(sourceClassName, methodName, domainId);
        popTrees.add(eventPopTree);
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addCustomAction")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addEvent(String sourceClassName, String ModuleEventTree, String methodName, String domainId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig callBean = esdClassConfig.getMethodByName(methodName);
            CustomModuleBean moduleBean = callBean.getModuleBean();
            if (ModuleEventTree != null && !ModuleEventTree.equals("")) {
                String[] menuIds = StringUtility.split(ModuleEventTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        if (menuId.indexOf("|") > -1) {
                            String[] eventType = StringUtility.split(menuId, "|");
                            ModuleEventEnum apiEventEnum = ModuleEventEnum.valueOf(eventType[0]);
                            switch (apiEventEnum) {
                                case onFragmentChanged:
                                    moduleBean.getOnFragmentChanged().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case onMessage:
                                    moduleBean.getOnMessage().add(ModuleOnMessageEventEnum.valueOf(eventType[1]));
                                    break;
                                case beforeCreated:
                                    moduleBean.getBeforeCreated().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case onLoadBaseClass:
                                    moduleBean.getOnLoadBaseClass().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case onLoadRequiredClass:
                                    moduleBean.getOnLoadRequiredClass().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case onLoadRequiredClassErr:
                                    moduleBean.getOnLoadRequiredClassErr().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case onIniResource:
                                    moduleBean.getOnIniResource().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;

                                case beforeIniComponents:
                                    moduleBean.getBeforeIniComponents().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case afterIniComponents:
                                    moduleBean.getAfterIniComponents().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case afterShow:
                                    moduleBean.getAfterShow().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;

                                case onModulePropChange:
                                    moduleBean.getOnModulePropChange().add(ModuleOnPropChangeEventEnum.valueOf(eventType[1]));
                                    break;
                                case onReady:
                                    moduleBean.getOnReady().add(ModuleOnReadyEventEnum.valueOf(eventType[1]));
                                    break;
                                case beforeDestroy:
                                    moduleBean.getBeforeDestroy().add(CustomOnDestroyEventEnum.valueOf(eventType[1]));
                                    break;
                                case onDestroy:
                                    moduleBean.getOnDestroy().add(CustomOnDestroyEventEnum.valueOf(eventType[1]));
                                    break;
                            }
                        }
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


    @RequestMapping("delEvent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delEvent(String sourceClassName, String methodName, String eventName, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            String[] menuIds = StringUtility.split(eventName, ";");
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig callBean = esdClassConfig.getMethodByName(methodName);
            CustomModuleBean moduleBean = callBean.getModuleBean();
            for (String menuId : menuIds) {
                if (menuId != null && !menuIds.equals("")) {
                    if (menuId.indexOf("|") > -1) {
                        String[] eventType = StringUtility.split(menuId, "|");

                        ModuleEventEnum apiEventEnum = ModuleEventEnum.valueOf(eventType[0]);
                        switch (apiEventEnum) {
                            case onFragmentChanged:
                                moduleBean.getOnFragmentChanged().remove(CustomModuleEventEnum.valueOf(eventType[1]));
                                break;
                            case onMessage:
                                moduleBean.getOnMessage().remove(ModuleOnMessageEventEnum.valueOf(eventType[1]));
                                break;
                            case beforeCreated:
                                moduleBean.getBeforeCreated().remove(CustomModuleEventEnum.valueOf(eventType[1]));
                                break;
                            case onLoadBaseClass:
                                moduleBean.getOnLoadBaseClass().remove(CustomModuleEventEnum.valueOf(eventType[1]));
                                break;
                            case onLoadRequiredClass:
                                moduleBean.getOnLoadRequiredClass().remove(CustomModuleEventEnum.valueOf(eventType[1]));
                                break;
                            case onLoadRequiredClassErr:
                                moduleBean.getOnLoadRequiredClassErr().remove(CustomModuleEventEnum.valueOf(eventType[1]));
                                break;
                            case onIniResource:
                                moduleBean.getOnIniResource().remove(CustomModuleEventEnum.valueOf(eventType[1]));
                                break;

                            case beforeIniComponents:
                                moduleBean.getBeforeIniComponents().remove(CustomModuleEventEnum.valueOf(eventType[1]));
                                break;
                            case afterIniComponents:
                                moduleBean.getAfterIniComponents().remove(CustomModuleEventEnum.valueOf(eventType[1]));
                                break;
                            case afterShow:
                                moduleBean.getAfterShow().remove(CustomModuleEventEnum.valueOf(eventType[1]));
                                break;

                            case onModulePropChange:
                                moduleBean.getOnModulePropChange().remove(ModuleOnPropChangeEventEnum.valueOf(eventType[1]));
                                break;
                            case onReady:
                                moduleBean.getOnReady().remove(ModuleOnReadyEventEnum.valueOf(eventType[1]));
                                break;
                            case beforeDestroy:
                                moduleBean.getBeforeDestroy().remove(CustomOnDestroyEventEnum.valueOf(eventType[1]));
                                break;
                            case onDestroy:
                                moduleBean.getOnDestroy().remove(CustomOnDestroyEventEnum.valueOf(eventType[1]));
                                break;
                        }
                    }
                }

            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
