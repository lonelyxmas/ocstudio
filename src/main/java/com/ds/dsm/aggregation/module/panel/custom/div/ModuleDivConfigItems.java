package com.ds.dsm.aggregation.module.panel.custom.div;

import com.ds.dsm.aggregation.module.panel.custom.ui.ModuleUIService;
import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.esd.tool.ui.enums.CustomImageType;

public enum ModuleDivConfigItems implements TreeItem {
    Div("Div层信息", CustomImageType.div.getImageClass(), ModuleDivService.class, false, false, false),
    UI("原生HTML属性", CustomImageType.html.getImageClass(), ModuleUIService.class, false, false, false);

    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    ModuleDivConfigItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
