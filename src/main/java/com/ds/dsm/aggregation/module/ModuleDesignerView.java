package com.ds.dsm.aggregation.module;

import com.alibaba.fastjson.JSONObject;
import com.ds.esd.custom.annotation.ComboColorAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.ImageAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.ModuleViewBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.ThemesType;
import com.ds.esd.tool.ui.module.ModuleViewConfig;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu
@FormAnnotation(col = 1, customService = ModuleDeisignerService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ModuleDesignerView {


    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    String methodName;

    @CustomAnnotation(caption = "画布宽度")
    Integer width;
    @CustomAnnotation(caption = "画布高度")
    Integer height;
    @CustomAnnotation(caption = "移动支持")
    Boolean mobileFrame = false;

    @CustomAnnotation(caption = "风格样式")
    ThemesType theme;
    @CustomAnnotation(caption = "缩放")
    String zoom;

    @ComboColorAnnotation
    @CustomAnnotation(caption = "背景底色")
    String backgroundColor;
    @ImageAnnotation
    @CustomAnnotation(caption = "背景图片")
    String backgroundImage;
    @CustomAnnotation(caption = "背景裁切")
    String backgroundRepeat;
    @CustomAnnotation(caption = "背景位置")
    String backgroundPosition;
    @CustomAnnotation(caption = "背景平铺")
    String backgroundAttachment;


    public ModuleDesignerView() {

    }

    public ModuleDesignerView(CustomModuleBean dataBean) {
        ModuleViewBean moduleViewBean = dataBean.getViewConfig();
        ModuleViewConfig viewConfig = new ModuleViewConfig(moduleViewBean);
        String json = JSONObject.toJSONString(viewConfig.getDesignViewConf());
        BeanMap.create(this).putAll(BeanMap.create(JSONObject.parseObject(json, ModuleDesignerView.class)));
        String viewjson = JSONObject.toJSONString(viewConfig.getViewStyles());
        BeanMap.create(this).putAll(BeanMap.create(JSONObject.parseObject(viewjson, ModuleDesignerView.class)));

        this.domainId = dataBean.getDomainId();
        this.sourceClassName = dataBean.getSourceClassName();
        this.methodName = dataBean.getMethodName();

    }

    public String getZoom() {
        return zoom;
    }

    public void setZoom(String zoom) {
        this.zoom = zoom;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Boolean getMobileFrame() {
        return mobileFrame;
    }

    public void setMobileFrame(Boolean mobileFrame) {
        this.mobileFrame = mobileFrame;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


    public ThemesType getTheme() {
        return theme;
    }

    public void setTheme(ThemesType theme) {
        this.theme = theme;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getBackgroundRepeat() {
        return backgroundRepeat;
    }

    public void setBackgroundRepeat(String backgroundRepeat) {
        this.backgroundRepeat = backgroundRepeat;
    }

    public String getBackgroundPosition() {
        return backgroundPosition;
    }

    public void setBackgroundPosition(String backgroundPosition) {
        this.backgroundPosition = backgroundPosition;
    }

    public String getBackgroundAttachment() {
        return backgroundAttachment;
    }

    public void setBackgroundAttachment(String backgroundAttachment) {
        this.backgroundAttachment = backgroundAttachment;
    }
}
