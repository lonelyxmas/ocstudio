package com.ds.dsm.aggregation.module.event;

import com.ds.esd.custom.action.module.*;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.event.enums.ModuleEventEnum;


@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = {ModuleEventService.class}, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@BottomBarMenu
@PopTreeAnnotation(caption = "添加事件")
public class ModuleEventPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String sourceClassName;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String methodName;


    public ModuleEventPopTree(String id, String caption) {
        super(id, caption);

    }

    public ModuleEventPopTree(String sourceClassName, String methodName, String domainId) {
        super("allModuleEvent", "模块事件");
        for (ModuleEventEnum moduleEventEnum : ModuleEventEnum.values()) {
            ModuleEventPopTree apiEventPopTree = new ModuleEventPopTree(moduleEventEnum.name(), moduleEventEnum.getName());
            this.addChild(apiEventPopTree);
            switch (moduleEventEnum) {
                case onFragmentChanged:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onMessage:
                    for (ModuleOnMessageEventEnum data : ModuleOnMessageEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case beforeCreated:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onLoadBaseClass:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onLoadRequiredClass:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onLoadRequiredClassErr:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onIniResource:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;

                case beforeIniComponents:

                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case afterIniComponents:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case afterShow:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;

                case onModulePropChange:
                    for (ModuleOnPropChangeEventEnum data : ModuleOnPropChangeEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }

                    break;
                case onReady:

                    for (ModuleOnReadyEventEnum data : ModuleOnReadyEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case beforeDestroy:
                    for (CustomOnDestroyEventEnum data : CustomOnDestroyEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onDestroy:
                    for (CustomOnDestroyEventEnum data : CustomOnDestroyEventEnum.values()) {
                        ModuleEventPopTree popTree = new ModuleEventPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
            }
        }
    }

    ModuleEventPopTree(String id, String caption, String sourceClassName, String domainId, String methodName) {
        super(id, caption);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("domainId", domainId);
        this.addTagVar("methodName", methodName);

    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
