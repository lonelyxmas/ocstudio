package com.ds.dsm.aggregation.module.panel.panel;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.module.panel.ModuleConfigTree;
import com.ds.dsm.aggregation.module.panel.custom.div.ModuleDivConfigItems;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.CustomPanelBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/agg/config/module/panel/")
public class ModulePanelService {


    @RequestMapping(method = RequestMethod.POST, value = "PanelConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ModulePanelView> getPanelConfig(String sourceClassName, String methodName, String domainId) {
        ResultModel<ModulePanelView> resultModel = new ResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = config.getMethodByName(methodName);
            resultModel.setData(new ModulePanelView(methodAPIBean.getModuleBean()));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadPanelItem")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ModuleConfigTree>> loadPanelItem(String sourceClassName, String methodName, String domainId) {
        TreeListResultModel<List<ModuleConfigTree>> resultModel = new TreeListResultModel<List<ModuleConfigTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(ModuleDivConfigItems.values()), ModuleConfigTree.class);
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "updatePanel")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updatePanel(@RequestBody CustomPanelBean panelBean, String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();

            if (panelBean != null) {
                panelBean.setBtnBean(moduleBean.getPanelBean().getBtnBean());
                panelBean.setDivBean(moduleBean.getPanelBean().getDivBean());
            }
            moduleBean.setPanelBean(panelBean);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetPanel")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetPanel(String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            moduleBean.setPanelBean(new CustomPanelBean(AnnotationUtil.getAllAnnotations(customMethodAPIBean.getMethod())));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
