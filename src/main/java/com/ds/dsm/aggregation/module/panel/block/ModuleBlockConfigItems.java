package com.ds.dsm.aggregation.module.panel.block;

import com.ds.dsm.aggregation.module.panel.custom.container.ModuleContainerService;
import com.ds.dsm.aggregation.module.panel.panel.btn.ModuleBtnService;
import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.esd.tool.ui.enums.CustomImageType;

public enum ModuleBlockConfigItems implements TreeItem {
    Block("块容器", CustomImageType.dialog.getImageClass(), ModuleBlockService.class, false, false, false),
    Container("容器配置", CustomImageType.dialog.getImageClass(), ModuleContainerService.class, false, false, false),
    Btn("容器按钮", CustomImageType.button.getImageClass(), ModuleBtnService.class, false, false, false);

    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    ModuleBlockConfigItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
