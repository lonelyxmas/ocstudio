package com.ds.dsm.aggregation.module.panel.dialog;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.CustomUIBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.component.dialog.DialogBean;
import com.ds.web.APIConfigFactory;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/agg/config/module/")
public class DialogService {


    @RequestMapping(method = RequestMethod.POST, value = "DialogConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<DialogView> getDialogConfig(String sourceClassName, String methodName, String domainId) {
        ResultModel<DialogView> resultModel = new ResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            resultModel.setData(new DialogView(customMethodAPIBean.getModuleBean()));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "updateDialog")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateDialog(@RequestBody DialogBean dialogBean, String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            if (dialogBean != null) {
                if (moduleBean.getDialogBean()!=null){
                    dialogBean.setDialogBtnBean(moduleBean.getDialogBean().getDialogBtnBean());
                    dialogBean.setContainerBean(moduleBean.getDialogBean().getContainerBean());
                }
                moduleBean.setDialogBean(dialogBean);
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
            }


        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetDialog")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetUI(String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            moduleBean.setDialogBean(new DialogBean(AnnotationUtil.getAllAnnotations(customMethodAPIBean.getMethod())));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
