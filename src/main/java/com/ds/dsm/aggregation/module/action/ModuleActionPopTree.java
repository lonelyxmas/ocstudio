package com.ds.dsm.aggregation.module.action;

import com.ds.dsm.aggregation.module.event.ModuleEventService;
import com.ds.esd.custom.action.module.*;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.event.enums.ModuleEventEnum;


@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = {ModuleActionService.class}, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@BottomBarMenu
@PopTreeAnnotation(caption = "添加事件")
public class ModuleActionPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String sourceClassName;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String methodName;


    public ModuleActionPopTree(String id, String caption) {
        super(id, caption);

    }

    public ModuleActionPopTree(String sourceClassName, String methodName, String domainId) {
        super("allModuleEvent", "模块事件");
        for (ModuleEventEnum moduleEventEnum : ModuleEventEnum.values()) {
            ModuleActionPopTree apiEventPopTree = new ModuleActionPopTree(moduleEventEnum.name(), moduleEventEnum.getName());
            this.addChild(apiEventPopTree);
            switch (moduleEventEnum) {
                case onFragmentChanged:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onMessage:
                    for (ModuleOnMessageEventEnum data : ModuleOnMessageEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case beforeCreated:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onLoadBaseClass:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onLoadRequiredClass:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onLoadRequiredClassErr:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onIniResource:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;

                case beforeIniComponents:

                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case afterIniComponents:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case afterShow:
                    for (CustomModuleEventEnum data : CustomModuleEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;

                case onModulePropChange:
                    for (ModuleOnPropChangeEventEnum data : ModuleOnPropChangeEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }

                    break;
                case onReady:

                    for (ModuleOnReadyEventEnum data : ModuleOnReadyEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case beforeDestroy:
                    for (CustomOnDestroyEventEnum data : CustomOnDestroyEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
                case onDestroy:
                    for (CustomOnDestroyEventEnum data : CustomOnDestroyEventEnum.values()) {
                        ModuleActionPopTree popTree = new ModuleActionPopTree(moduleEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);
                    }
                    break;
            }
        }
    }

    ModuleActionPopTree(String id, String caption, String sourceClassName, String domainId, String methodName) {
        super(id, caption);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("domainId", domainId);
        this.addTagVar("methodName", methodName);

    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
