package com.ds.dsm.aggregation.module.panel;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.module.panel.block.ModuleBlockConfigItems;
import com.ds.dsm.aggregation.module.panel.dialog.DialogConfigItems;
import com.ds.dsm.aggregation.module.panel.panel.ModulePanelConfigItems;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.enums.PanelType;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/config/module/win/")
public class ModuleWinService {


    @RequestMapping(method = RequestMethod.POST, value = "WinConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ModuleWinView> getWinConfig(String sourceClassName, String methodName, String domainId) {
        ResultModel<ModuleWinView> resultModel = new ResultModel();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            if (classConfig != null) {
                MethodConfig methodAPIBean = classConfig.getMethodByName(methodName);
                resultModel.setData(new ModuleWinView(methodAPIBean.getModuleBean()));
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "编辑数据信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateWinData")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save, callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateWinData(@RequestBody ModuleWinView winView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(winView.getSourceClassName(), winView.getDomainId());
            MethodConfig methodAPIBean = classConfig.getMethodByName(winView.getMethodName());
            CustomModuleBean customModuleBean = methodAPIBean.getModuleBean();
            customModuleBean.setName(winView.getMethodName());
            customModuleBean.setCaption(winView.getCaption());
            customModuleBean.setDock(winView.getDock());
            customModuleBean.setImageClass(winView.getImageClass());
            customModuleBean.setRotate(winView.getRotate());
            customModuleBean.setTarget(winView.getTarget());
            customModuleBean.setDynLoad(winView.getDynLoad());
            customModuleBean.setCache(winView.getCache());
            customModuleBean.setPanelType(winView.getPanelType());
            customModuleBean.setBindService(winView.getBindService());
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadWinItem")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ModuleConfigTree>> loadWinItem(String domainId, String sourceClassName, String methodName) {
        TreeListResultModel<List<ModuleConfigTree>> resultModel = new TreeListResultModel<List<ModuleConfigTree>>();
        ApiClassConfig classConfig = null;
        try {
            classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = classConfig.getMethodByName(methodName);
            PanelType panelType = methodAPIBean.getModuleBean().getPanelType();
            switch (panelType) {
                case panel:
                    resultModel = TreePageUtil.getTreeList(Arrays.asList(ModulePanelConfigItems.values()), ModuleConfigTree.class);
                    break;
                case dialog:
                    resultModel = TreePageUtil.getTreeList(Arrays.asList(DialogConfigItems.values()), ModuleConfigTree.class);
                    break;
                case block:
                    resultModel = TreePageUtil.getTreeList(Arrays.asList(ModuleBlockConfigItems.values()), ModuleConfigTree.class);
                    break;
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return resultModel;
    }

}
