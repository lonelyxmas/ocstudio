package com.ds.dsm.aggregation.module.panel.custom.container;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.module.panel.ModuleConfigTree;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.*;
import com.ds.esd.custom.bean.enums.PanelType;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.component.dialog.DialogBean;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.APIConfigFactory;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/agg/config/module/")
public class ModuleContainerService {


    @RequestMapping(method = RequestMethod.POST, value = "ContainerConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ModuleContainerView> getContainerConfig(String domainId, String sourceClassName, String methodName) {
        ResultModel<ModuleContainerView> resultModel = new ResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            ContainerBean containerBean = new ContainerBean();
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            PanelType panelType = moduleBean.getPanelType();

            switch (panelType) {
                case dialog:
                    DialogBean dialogBean = moduleBean.getDialogBean();
                    containerBean = dialogBean.getContainerBean();
                    break;
                case block:
                    CustomBlockBean blockBean = moduleBean.getBlockBean();
                    containerBean = blockBean.getContainerBean();
                    break;
            }

            resultModel.setData(new ModuleContainerView(containerBean, domainId, sourceClassName, methodName));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadContainerItem")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ModuleConfigTree>> loadContainerItem(String sourceClassName, String methodName, String domainId) {
        TreeListResultModel<List<ModuleConfigTree>> resultModel = new TreeListResultModel<List<ModuleConfigTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(ModuleContainerConfigItems.values()), ModuleConfigTree.class);
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "updateContainer")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateContainer(@RequestBody ContainerBean containerBean, String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            PanelType panelType = moduleBean.getPanelType();

            switch (panelType) {
                case dialog:
                    DialogBean dialogBean = moduleBean.getDialogBean();
                    if (containerBean != null) {
                        containerBean.setDockBean(dialogBean.getContainerBean().getDockBean());
                    }
                    dialogBean.setContainerBean(containerBean);
                    break;
                case block:
                    CustomBlockBean blockBean = moduleBean.getBlockBean();
                    if (containerBean != null) {
                        containerBean.setDockBean(blockBean.getContainerBean().getDockBean());
                    }
                    blockBean.setContainerBean(containerBean);
                    break;
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetContainer")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetContainer(String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            PanelType panelType = moduleBean.getPanelType();

            switch (panelType) {
                case dialog:
                    DialogBean dialogBean = moduleBean.getDialogBean();
                    dialogBean.setContainerBean(new ContainerBean(AnnotationUtil.getAllAnnotations(customMethodAPIBean.getMethod())));
                    break;
                case block:
                    CustomBlockBean blockBean = moduleBean.getBlockBean();
                    blockBean.setContainerBean(new ContainerBean(AnnotationUtil.getAllAnnotations(customMethodAPIBean.getMethod())));
                    break;
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
