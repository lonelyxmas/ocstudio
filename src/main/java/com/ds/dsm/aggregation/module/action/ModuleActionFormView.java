package com.ds.dsm.aggregation.module.action;

import com.alibaba.fastjson.JSONObject;
import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.dsm.aggregation.api.method.action.condition.ConditionGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.CodeEditorTools;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.enums.event.ActionTypeEnum;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/module/config/action/")
@BottomBarMenu
@MethodChinaName(cname = "绑定菜单", imageClass = "spafont spa-icon-app")
@FormAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = ModuleActionService.class)
public class ModuleActionFormView {
    @CustomAnnotation(hidden = true, uid = true)
    String id;
    @CustomAnnotation(hidden = true, pid = true)
    String sourceClass;
    @CustomAnnotation(hidden = true, pid = true)
    String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    String event;
    @CustomAnnotation(caption = "成功", hidden = true)
    String okFlag;
    @CustomAnnotation(caption = "放弃", hidden = true)
    String koFlag;
    @CustomAnnotation(caption = "模块", hidden = true)
    String className;

    @CustomAnnotation(caption = "执行方式")
    ActionTypeEnum type;

    @FieldAnnotation(dynCheck = true, required = true)
    @CustomAnnotation(caption = "方法")
    String method;

    @CodeEditorAnnotation()
    @ToolBarMenu(menuClasses = CodeEditorTools.class)
    @FieldAnnotation(rowHeight = "80", colSpan = -1)
    @CustomAnnotation(caption = "执行脚本")
    String script;

    @CustomAnnotation(caption = "源目标")
    String target;

    @CustomAnnotation(caption = "执行目标")
    String redirection;

    @CustomAnnotation(caption = "执行终止")
    Boolean canReturn;

    @TextEditorAnnotation()
    @FieldAnnotation(rowHeight = "60", colSpan = -1)
    @CustomAnnotation(caption = "参数")
    String args;

    @JavaEditorAnnotation()
    @FieldAnnotation(rowHeight = "60", colSpan = -1)
    @CustomAnnotation(caption = "条件表达式")
    String expression;

    @CustomAnnotation(caption = "执行条件")
    ListResultModel<List<ConditionGridView>> conditions;

    @MethodChinaName(cname = "执行条件")
    @RequestMapping("Conditions")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-app", caption = "执行条件")
    @APIEventAnnotation(autoRun = true)
    @ComboModuleAnnotation
    @FieldAnnotation(haslabel = false, colSpan = -1)
    @ResponseBody
    public ListResultModel<List<ConditionGridView>> getConditions(String sourceClassName, String domainId, String methodName, String actionId) {
        ListResultModel<List<ConditionGridView>> result = new ListResultModel<List<ConditionGridView>>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Action currAction = apiCallBean.getApi().getActionById(actionId);
            if (currAction != null) {
                result = PageUtil.getDefaultPageList(currAction.getConditions(), ConditionGridView.class);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return result;
    }

    public ModuleActionFormView() {

    }


    public ModuleActionFormView(Action action, MethodConfig config) {
        this.methodName = config.getMethodName();
        this.sourceClass = config.getSourceClassName();
        this.domainId = config.getDomainId();
        this.id = action.getId();
        this.type = action.getType();
        this.target = action.getTarget();
        this.className = action.getClassName();
        this.expression = action.getExpression();
        this.script = action.getScript();
        this.canReturn = action.get_return();
        this.method = action.getMethod();
        this.redirection = action.getRedirection();
        this.okFlag = action.getOkFlag();
        this.koFlag = action.getKoFlag();
        this.args = JSONObject.toJSONString(action.getArgs());
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClass() {
        return sourceClass;
    }

    public void setSourceClass(String sourceClass) {
        this.sourceClass = sourceClass;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }


    public ActionTypeEnum getType() {
        return type;
    }

    public void setType(ActionTypeEnum type) {
        this.type = type;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }


    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getRedirection() {
        return redirection;
    }

    public void setRedirection(String redirection) {
        this.redirection = redirection;
    }

    public String getOkFlag() {
        return okFlag;
    }

    public void setOkFlag(String okFlag) {
        this.okFlag = okFlag;
    }

    public String getKoFlag() {
        return koFlag;
    }

    public void setKoFlag(String koFlag) {
        this.koFlag = koFlag;
    }

    public Boolean getCanReturn() {
        return canReturn;
    }

    public void setCanReturn(Boolean canReturn) {
        this.canReturn = canReturn;
    }
}
