package com.ds.dsm.aggregation.module.panel;

import com.alibaba.fastjson.JSONObject;
import com.ds.dsm.manager.view.BuildViewMenu;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.CodeEditorAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.enums.PanelType;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.esd.tool.ui.enums.Dock;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu(menuClass = BuildViewMenu.class)
@FormAnnotation(col = 1, customService = ModuleWinService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ModuleWinView {


    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    String methodName;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "面板类型")
    PanelType panelType;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "标题")
    String caption;

    @CustomAnnotation(caption = "页面缓存")
    Boolean cache;

    @CustomAnnotation(caption = "动态装载")
    Boolean dynLoad;

    @CustomAnnotation(caption = "平铺")
    Dock dock;

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;


    @CustomAnnotation(caption = "缩放")
    Integer rotate;
    @CustomAnnotation(caption = "目标框架")
    String target;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "绑定服务")
    Class bindService;

    @FieldAnnotation(componentType = ComponentType.CodeEditor, colSpan = -1)
    @CodeEditorAnnotation(codeType="css")
    @CustomAnnotation(caption = "样式")
    String cssStyle;


    public ModuleWinView() {

    }

    public ModuleWinView(CustomModuleBean dataBean) {
        String json = JSONObject.toJSONString(dataBean);
        BeanMap.create(this).putAll(BeanMap.create(JSONObject.parseObject(json, ModuleWinView.class)));
        this.domainId = dataBean.getDomainId();
        this.methodName = dataBean.getMethodName();
        this.sourceClassName = dataBean.getSourceClassName();
        this.imageClass = dataBean.getImageClass();

        if (dataBean.getBindService() != null && !dataBean.getBindService().equals(Void.class)) {
            this.bindService =dataBean.getBindService();
        }

        this.cache = dataBean.getCache();
        this.dock = dataBean.getDock();
        this.dynLoad = dataBean.getDynLoad();
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getCssStyle() {
        return cssStyle;
    }

    public void setCssStyle(String cssStyle) {
        this.cssStyle = cssStyle;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Integer getRotate() {
        return rotate;
    }

    public void setRotate(Integer rotate) {
        this.rotate = rotate;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Class getBindService() {
        return bindService;
    }

    public void setBindService(Class bindService) {
        this.bindService = bindService;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }

    public PanelType getPanelType() {
        return panelType;
    }

    public void setPanelType(PanelType panelType) {
        this.panelType = panelType;
    }

    public Boolean getCache() {
        return cache;
    }

    public void setCache(Boolean cache) {
        this.cache = cache;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }
}
