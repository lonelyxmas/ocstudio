package com.ds.dsm.aggregation.module.panel.panel;

import com.alibaba.fastjson.JSONObject;
import com.ds.dsm.aggregation.module.panel.ModuleWinService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.CustomPanelBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.*;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu
@FormAnnotation(col = 1, customService = ModulePanelService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ModulePanelView {


    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;

    @CustomAnnotation(caption = "显示名称")
    String caption;

    @CustomAnnotation(caption = "平铺")
    Dock dock;
    @CustomAnnotation(caption = "html")
    String html;
    @CustomAnnotation(caption = "切换按钮")
    Boolean toggle;
    @CustomAnnotation(caption = "图片")
    String image;
    @CustomAnnotation(caption = "图片位置")
    ImagePos imagePos;
    @CustomAnnotation(caption = "图片大小")
    String imageBgSize;
    @CustomAnnotation(caption = "图标字体")
    String iconFontCode;
    @CustomAnnotation(caption = "边框")
    BorderType borderType;
    @CustomAnnotation(caption = "是否使用Iframe")
    Boolean noFrame;
    @CustomAnnotation(caption = "垂直对齐")
    HAlignType hAlign;


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;


    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    public ModulePanelView() {

    }

    public ModulePanelView(CustomModuleBean config) {
        CustomPanelBean panelBean = config.getPanelBean();
        if (panelBean == null) {
            panelBean = new CustomPanelBean();
        }
        String json = JSONObject.toJSONString(panelBean);
        BeanMap.create(this).putAll( BeanMap.create(JSONObject.parseObject(json,ModulePanelView.class)));
        this.viewInstId = config.getViewInstId();
        this.domainId = config.getDomainId();
        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getMethodName();


    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public Boolean getToggle() {
        return toggle;
    }

    public void setToggle(Boolean toggle) {
        this.toggle = toggle;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ImagePos getImagePos() {
        return imagePos;
    }

    public void setImagePos(ImagePos imagePos) {
        this.imagePos = imagePos;
    }

    public String getImageBgSize() {
        return imageBgSize;
    }

    public void setImageBgSize(String imageBgSize) {
        this.imageBgSize = imageBgSize;
    }

    public String getIconFontCode() {
        return iconFontCode;
    }

    public void setIconFontCode(String iconFontCode) {
        this.iconFontCode = iconFontCode;
    }

    public BorderType getBorderType() {
        return borderType;
    }

    public void setBorderType(BorderType borderType) {
        this.borderType = borderType;
    }

    public Boolean getNoFrame() {
        return noFrame;
    }

    public void setNoFrame(Boolean noFrame) {
        this.noFrame = noFrame;
    }

    public HAlignType gethAlign() {
        return hAlign;
    }

    public void sethAlign(HAlignType hAlign) {
        this.hAlign = hAlign;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

}
