package com.ds.dsm.aggregation.module.panel.dialog;

import com.alibaba.fastjson.JSONObject;
import com.ds.dsm.aggregation.module.panel.panel.btn.ModuleBtnService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.dialog.DialogBean;
import com.ds.esd.tool.ui.enums.CustomImageType;
import net.sf.cglib.beans.BeanMap;
@BottomBarMenu
@FormAnnotation(col = 1, customService = DialogService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class DialogView {


    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    public String imageClass;

    @CustomAnnotation(caption = "标题")
    public String caption;
    @CustomAnnotation(caption = "左边距")
    public String left;
    @CustomAnnotation(caption = "上边距")
    public String top;
    @CustomAnnotation(caption = "宽度")
    public String width;
    @CustomAnnotation(caption = "高宽")
    public String height;

    @CustomAnnotation(caption = "显示动画")
    String showEffects;
    @CustomAnnotation(caption = "隐藏动画")
    String hideEffects;

    @CustomAnnotation(caption = "自动加载")
    String iframeAutoLoad;

    @CustomAnnotation(caption = "ajaxAutoLoad")
    public String ajaxAutoLoad;
    @CustomAnnotation(caption = "窗口名称")
    String name;

    @CustomAnnotation(caption = "是否命令窗口")
    Boolean cmd;
    @CustomAnnotation(caption = "是否可移动")
    Boolean movable;
    @CustomAnnotation(caption = "重设大小")
    Boolean resizer;
    @CustomAnnotation(caption = "模态窗口")
    Boolean modal;
    @CustomAnnotation(caption = "锁定")
    Boolean locked;
    @CustomAnnotation(caption = "弹出源")
    String fromRegion;
    @CustomAnnotation(caption = "最小宽度")
    String minWidth;
    @CustomAnnotation(caption = "最小高度")
    String minHeight;


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    public DialogView() {

    }

    public DialogView(CustomModuleBean config) {
        DialogBean dialogBean = config.getDialogBean();
        if (dialogBean == null) {
            dialogBean = new DialogBean();
        }
        String json = JSONObject.toJSONString(dialogBean);
        BeanMap.create(this).putAll(BeanMap.create(JSONObject.parseObject(json, DialogView.class)));
        this.viewInstId = config.getViewInstId();
        this.domainId = config.getDomainId();

        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getMethodName();


    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getShowEffects() {
        return showEffects;
    }

    public void setShowEffects(String showEffects) {
        this.showEffects = showEffects;
    }

    public String getHideEffects() {
        return hideEffects;
    }

    public void setHideEffects(String hideEffects) {
        this.hideEffects = hideEffects;
    }

    public String getIframeAutoLoad() {
        return iframeAutoLoad;
    }

    public void setIframeAutoLoad(String iframeAutoLoad) {
        this.iframeAutoLoad = iframeAutoLoad;
    }

    public String getAjaxAutoLoad() {
        return ajaxAutoLoad;
    }

    public void setAjaxAutoLoad(String ajaxAutoLoad) {
        this.ajaxAutoLoad = ajaxAutoLoad;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getCmd() {
        return cmd;
    }

    public void setCmd(Boolean cmd) {
        this.cmd = cmd;
    }

    public Boolean getMovable() {
        return movable;
    }

    public void setMovable(Boolean movable) {
        this.movable = movable;
    }

    public Boolean getResizer() {
        return resizer;
    }

    public void setResizer(Boolean resizer) {
        this.resizer = resizer;
    }

    public Boolean getModal() {
        return modal;
    }

    public void setModal(Boolean modal) {
        this.modal = modal;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public String getFromRegion() {
        return fromRegion;
    }

    public void setFromRegion(String fromRegion) {
        this.fromRegion = fromRegion;
    }

    public String getMinWidth() {
        return minWidth;
    }

    public void setMinWidth(String minWidth) {
        this.minWidth = minWidth;
    }

    public String getMinHeight() {
        return minHeight;
    }

    public void setMinHeight(String minHeight) {
        this.minHeight = minHeight;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

}
