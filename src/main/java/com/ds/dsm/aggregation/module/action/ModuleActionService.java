package com.ds.dsm.aggregation.module.action;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.action.module.*;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.APIEventBean;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.enums.event.enums.ModuleEventEnum;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/module/config/actions/")
public class ModuleActionService {


    @MethodChinaName(cname = "调用事件")
    @RequestMapping(method = RequestMethod.POST, value = "ModuleActions")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-event", caption = "调用事件")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<ModuleActionGirdView>> getCustomActions(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<ModuleActionGirdView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            List<APIEventBean> events = apiCallBean.getApi().getAllEvent();
            List<Action> actions = new ArrayList<>();
            List<ModuleActionGirdView> actionViews = new ArrayList<>();
            for (APIEventBean apiEvent : events) {
                for (Action action : apiEvent.getActions()) {
                    actionViews.add(new ModuleActionGirdView(action, apiCallBean));
                }
            }

            resultModel = PageUtil.getDefaultPageList(actions, ModuleActionGirdView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping("addCustomAction")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addEvent(String sourceClassName, String ModuleEventTree, String methodName, String domainId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig callBean = esdClassConfig.getMethodByName(methodName);
            CustomModuleBean moduleBean = callBean.getModuleBean();
            if (ModuleEventTree != null && !ModuleEventTree.equals("")) {
                String[] menuIds = StringUtility.split(ModuleEventTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        if (menuId.indexOf("|") > -1) {
                            String[] eventType = StringUtility.split(menuId, "|");
                            ModuleEventEnum apiEventEnum = ModuleEventEnum.valueOf(eventType[0]);
                            switch (apiEventEnum) {
                                case onFragmentChanged:
                                    moduleBean.getOnFragmentChanged().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case onMessage:
                                    moduleBean.getOnMessage().add(ModuleOnMessageEventEnum.valueOf(eventType[1]));
                                    break;
                                case beforeCreated:
                                    moduleBean.getBeforeCreated().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case onLoadBaseClass:
                                    moduleBean.getOnLoadBaseClass().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case onLoadRequiredClass:
                                    moduleBean.getOnLoadRequiredClass().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case onLoadRequiredClassErr:
                                    moduleBean.getOnLoadRequiredClassErr().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case onIniResource:
                                    moduleBean.getOnIniResource().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;

                                case beforeIniComponents:
                                    moduleBean.getBeforeIniComponents().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case afterIniComponents:
                                    moduleBean.getAfterIniComponents().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;
                                case afterShow:
                                    moduleBean.getAfterShow().add(CustomModuleEventEnum.valueOf(eventType[1]));
                                    break;

                                case onModulePropChange:
                                    moduleBean.getOnModulePropChange().add(ModuleOnPropChangeEventEnum.valueOf(eventType[1]));
                                    break;
                                case onReady:
                                    moduleBean.getOnReady().add(ModuleOnReadyEventEnum.valueOf(eventType[1]));
                                    break;
                                case beforeDestroy:
                                    moduleBean.getBeforeDestroy().add(CustomOnDestroyEventEnum.valueOf(eventType[1]));
                                    break;
                                case onDestroy:
                                    moduleBean.getOnDestroy().add(CustomOnDestroyEventEnum.valueOf(eventType[1]));
                                    break;
                            }
                        }
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }

    @PopTreeViewAnnotation()
    @ModuleAnnotation(dynLoad = true, caption = "列表事件")
    @RequestMapping("ModuleEventTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<ModuleActionPopTree>> getModuleEventTree(String sourceClassName, String methodName, String domainId, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<ModuleActionPopTree>> model = new TreeListResultModel<>();
        List<ModuleActionPopTree> popTrees = new ArrayList<>();
        ModuleActionPopTree eventPopTree = new ModuleActionPopTree(sourceClassName, methodName, domainId);
        popTrees.add(eventPopTree);
        model.setData(popTrees);
        return model;
    }


    @FormViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "常用事件")
    @RequestMapping("ActionInfo")
    @DialogAnnotation(width = "800", height = "650")
    @APIEventAnnotation(bindMenu = CustomMenuItem.editor, autoRun = true)
    @ResponseBody
    public ResultModel<ModuleActionFormView> getActionInfo(String sourceClassName, String actionId, String domainId, String methodName) {
        ResultModel<ModuleActionFormView> model = new ResultModel<>();
        ApiClassConfig esdClassConfig = null;
        try {
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Action currAction = apiCallBean.getApi().getActionById(actionId);
            if (currAction != null) {
                model.setData(new ModuleActionFormView(currAction, apiCallBean));
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return model;
    }


}
