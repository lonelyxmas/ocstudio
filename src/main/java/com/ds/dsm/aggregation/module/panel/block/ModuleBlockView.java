package com.ds.dsm.aggregation.module.panel.block;

import com.alibaba.fastjson.JSONObject;
import com.ds.dsm.aggregation.module.panel.custom.container.ModuleContainerView;
import com.ds.dsm.aggregation.module.panel.dialog.btn.ModuleDialogBtnService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboColorAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.CustomBlockBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BorderType;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 1, customService = ModuleBlockService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ModuleBlockView {

    @CustomAnnotation(caption = "边框")
    BorderType borderType;
    @CustomAnnotation(caption = "是否可缩放")
    Boolean resizer;
    @CustomAnnotation(caption = "缩放参数")
    Map<String, Object> resizerProp;
    @CustomAnnotation(caption = "边栏标题")
    String sideBarCaption;
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = SideBarStatusType.class)
    @CustomAnnotation(caption = "边栏类型")
    String sideBarType;
    @CustomAnnotation(caption = "边栏状态")
    SideBarStatusType sideBarStatus;
    @CustomAnnotation(caption = "边栏大小")
    String sideBarSize;
    @ComboColorAnnotation
    @CustomAnnotation(caption = "背景颜色")
    String background;


    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    public ModuleBlockView() {

    }

    public ModuleBlockView(CustomBlockBean blockBean, String domainId, String sourceClassName, String methodName) {
        String json = JSONObject.toJSONString(blockBean);
        BeanMap.create(this).putAll(BeanMap.create(JSONObject.parseObject(json, ModuleContainerView.class)));

        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;

    }

    public BorderType getBorderType() {
        return borderType;
    }

    public void setBorderType(BorderType borderType) {
        this.borderType = borderType;
    }

    public Boolean getResizer() {
        return resizer;
    }

    public void setResizer(Boolean resizer) {
        this.resizer = resizer;
    }

    public Map<String, Object> getResizerProp() {
        return resizerProp;
    }

    public void setResizerProp(Map<String, Object> resizerProp) {
        this.resizerProp = resizerProp;
    }

    public String getSideBarCaption() {
        return sideBarCaption;
    }

    public void setSideBarCaption(String sideBarCaption) {
        this.sideBarCaption = sideBarCaption;
    }

    public String getSideBarType() {
        return sideBarType;
    }

    public void setSideBarType(String sideBarType) {
        this.sideBarType = sideBarType;
    }

    public SideBarStatusType getSideBarStatus() {
        return sideBarStatus;
    }

    public void setSideBarStatus(SideBarStatusType sideBarStatus) {
        this.sideBarStatus = sideBarStatus;
    }

    public String getSideBarSize() {
        return sideBarSize;
    }

    public void setSideBarSize(String sideBarSize) {
        this.sideBarSize = sideBarSize;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

}
