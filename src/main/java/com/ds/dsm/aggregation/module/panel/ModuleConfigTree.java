package com.ds.dsm.aggregation.module.panel;

import com.ds.dsm.aggregation.module.panel.block.ModuleBlockConfigItems;
import com.ds.dsm.aggregation.module.panel.custom.container.ModuleContainerConfigItems;
import com.ds.dsm.aggregation.module.panel.custom.div.ModuleDivConfigItems;
import com.ds.dsm.aggregation.module.panel.dialog.DialogConfigItems;
import com.ds.dsm.aggregation.module.panel.panel.ModulePanelConfigItems;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class ModuleConfigTree extends TreeListItem {

    @Pid
    String sourceClassName;
    @Pid
    String methodName;
    @Pid
    String domainId;


    @TreeItemAnnotation(customItems = DialogConfigItems.class)
    public ModuleConfigTree(DialogConfigItems groupNavItems, String sourceClassName, String methodName, String domainId) {
        this.caption = groupNavItems.getName();
        this.bindClassName = groupNavItems.getBindClass().getName();
        this.dynLoad = groupNavItems.isDynLoad();
        this.dynDestory = groupNavItems.isDynDestory();
        this.iniFold = groupNavItems.isIniFold();
        this.imageClass = groupNavItems.getImageClass();
        this.id = sourceClassName + "_" + methodName + "_" + groupNavItems.getType();
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = ModuleContainerConfigItems.class)
    public ModuleConfigTree(ModuleContainerConfigItems configItem, String sourceClassName, String methodName, String domainId) {
        this.caption = configItem.getName();
        this.bindClassName = configItem.getBindClass().getName();
        this.dynLoad = configItem.isDynLoad();
        this.dynDestory = configItem.isDynDestory();
        this.iniFold = configItem.isIniFold();
        this.imageClass = configItem.getImageClass();
        this.id = sourceClassName + "_" + methodName + "_" + configItem.getType();
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = ModuleBlockConfigItems.class)
    public ModuleConfigTree(ModuleBlockConfigItems configItem, String sourceClassName, String methodName, String domainId) {
        this.caption = configItem.getName();
        this.bindClassName = configItem.getBindClass().getName();
        this.dynLoad = configItem.isDynLoad();
        this.dynDestory = configItem.isDynDestory();
        this.iniFold = configItem.isIniFold();
        this.imageClass = configItem.getImageClass();
        this.id = sourceClassName + "_" + methodName + "_" + configItem.getType();
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = ModulePanelConfigItems.class)
    public ModuleConfigTree(ModulePanelConfigItems configItem, String sourceClassName, String methodName, String domainId) {
        this.caption = configItem.getName();
        this.bindClassName = configItem.getBindClass().getName();
        this.dynLoad = configItem.isDynLoad();
        this.dynDestory = configItem.isDynDestory();
        this.iniFold = configItem.isIniFold();
        this.imageClass = configItem.getImageClass();
        this.id = sourceClassName + "_" + methodName + "_" + configItem.getType();
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;


    }

    @TreeItemAnnotation(customItems = ModuleDivConfigItems.class)
    public ModuleConfigTree(ModuleDivConfigItems configItem, String sourceClassName, String methodName, String domainId) {
        this.caption = configItem.getName();
        this.bindClassName = configItem.getBindClass().getName();
        this.dynLoad = configItem.isDynLoad();
        this.dynDestory = configItem.isDynDestory();
        this.iniFold = configItem.isIniFold();
        this.imageClass = configItem.getImageClass();
        this.id = sourceClassName + "_" + methodName + "_" + configItem.getType();
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;

    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

}
