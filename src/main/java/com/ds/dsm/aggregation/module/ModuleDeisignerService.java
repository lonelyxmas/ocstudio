package com.ds.dsm.aggregation.module;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.module.panel.ModuleConfigTree;
import com.ds.dsm.aggregation.module.panel.block.ModuleBlockConfigItems;
import com.ds.dsm.aggregation.module.panel.dialog.DialogConfigItems;
import com.ds.dsm.aggregation.module.panel.panel.ModulePanelConfigItems;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.ModuleViewBean;
import com.ds.esd.custom.bean.enums.PanelType;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.module.ModuleViewConfig;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/config/module/designer/")
public class ModuleDeisignerService {


    @RequestMapping(method = RequestMethod.POST, value = "ModuleDesignerView")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ModuleDesignerView> getModuleDesignerView(String sourceClassName, String methodName, String domainId) {
        ResultModel<ModuleDesignerView> resultModel = new ResultModel();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            if (esdClassConfig != null) {
                MethodConfig methodAPIBean = esdClassConfig.getMethodByName(methodName);
                resultModel.setData(new ModuleDesignerView(methodAPIBean.getModuleBean()));
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "编辑数据信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateWinData")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save, callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateWinData(@RequestBody ModuleDesignerView winView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(winView.getSourceClassName(), winView.getDomainId());
            MethodConfig methodAPIBean = classConfig.getMethodByName(winView.getMethodName());
            ModuleViewBean viewConfig = methodAPIBean.getModuleBean().getViewConfig();
            viewConfig.getDesignViewBean().setHeight(winView.getHeight());
            viewConfig.getDesignViewBean().setWidth(winView.getWidth());
            viewConfig.getDesignViewBean().setMobileFrame(winView.getMobileFrame());

            viewConfig.getViewStyles().setBackgroundAttachment(winView.getBackgroundAttachment());
            viewConfig.getViewStyles().setBackgroundColor(winView.getBackgroundColor());
            viewConfig.getViewStyles().setBackgroundImage(winView.getBackgroundImage());
            viewConfig.getViewStyles().setBackgroundPosition(winView.getBackgroundPosition());
            viewConfig.getViewStyles().setBackgroundRepeat(winView.getBackgroundRepeat());
            viewConfig.getViewStyles().setTheme(winView.getTheme());
            viewConfig.getViewStyles().setZoom(winView.getZoom());

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "清空窗体配置")
    @RequestMapping(method = RequestMethod.POST, value = "clear")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet, callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clear(String sourceClassName, String methodName, String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = classConfig.getMethodByName(methodName);
            methodAPIBean.getModuleBean().setModuleViewType(null);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }



}
