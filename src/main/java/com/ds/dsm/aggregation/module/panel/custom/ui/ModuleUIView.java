package com.ds.dsm.aggregation.module.panel.custom.ui;

import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.CustomUIBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.enums.css.VisibilityType;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu
@FormAnnotation(col = 2, customService = ModuleUIService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ModuleUIView {

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    public String imageClass;

    @CustomAnnotation(caption = "标题")
    public String caption;

    @CustomAnnotation(caption = "平铺")
    public Dock dock;
    @CustomAnnotation(caption = "位置")
    public String position;

    @CustomAnnotation(caption = "显示属性")
    public VisibilityType visibility;

    @CustomAnnotation(caption = "图层可见属性")
    public String display;
    @CustomAnnotation(caption = "是否可选")
    public Boolean selectable;
    @CustomAnnotation(caption = "动态装载")
    public Boolean dynLoad;


    @CustomAnnotation(caption = "左边距")
    public String left;
    @CustomAnnotation(caption = "右边距")
    public String right;
    @CustomAnnotation(caption = "上边距")
    public String top;
    @CustomAnnotation(caption = "下边距")
    public String bottom;

    @CustomAnnotation(caption = "宽度")
    public String width;
    @CustomAnnotation(caption = "高宽")
    public String height;




    @CustomAnnotation(caption = "渲染目标")
    public String renderer;

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    public ModuleUIView() {

    }

    public ModuleUIView(CustomUIBean uiBean, String domainId, String sourceClassName, String methodName) {
        BeanMap.create(this).putAll(BeanMap.create(uiBean));
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;


    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }

    public VisibilityType getVisibility() {
        return visibility;
    }

    public void setVisibility(VisibilityType visibility) {
        this.visibility = visibility;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    public String getRenderer() {
        return renderer;
    }

    public void setRenderer(String renderer) {
        this.renderer = renderer;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public String getBottom() {
        return bottom;
    }

    public void setBottom(String bottom) {
        this.bottom = bottom;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

}
