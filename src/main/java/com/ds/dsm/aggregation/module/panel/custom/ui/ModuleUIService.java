package com.ds.dsm.aggregation.module.panel.custom.ui;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.*;
import com.ds.esd.custom.bean.enums.PanelType;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.component.dialog.DialogBean;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/agg/config/module/ui/")
public class ModuleUIService {


    @RequestMapping(method = RequestMethod.POST, value = "UIConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ModuleUIView> getUIConfig(String sourceClassName, String methodName, String domainId) {
        ResultModel<ModuleUIView> resultModel = new ResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomUIBean uiBean  = new CustomUIBean();

            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            PanelType panelType = moduleBean.getPanelType();

            switch (panelType) {
                case panel:
                    CustomPanelBean panelBean = moduleBean.getPanelBean();
                    uiBean = panelBean.getDivBean().getUiBean();
                case dialog:
                    DialogBean dialogBean = moduleBean.getDialogBean();
                    uiBean = dialogBean.getContainerBean().getUiBean();
                    break;
                case block:
                    CustomBlockBean blockBean = moduleBean.getBlockBean();
                    uiBean = blockBean.getContainerBean().getUiBean();
                    break;
            }

            resultModel.setData(new ModuleUIView(uiBean, domainId, sourceClassName, methodName));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "updateUI")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateUI(@RequestBody CustomUIBean uiBean, String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();

            PanelType panelType = moduleBean.getPanelType();

            switch (panelType) {
                case panel:
                    CustomPanelBean panelBean = moduleBean.getPanelBean();
                      panelBean.getDivBean().setUiBean(uiBean);
                case dialog:
                    DialogBean dialogBean = moduleBean.getDialogBean();
                     dialogBean.getContainerBean().setUiBean(uiBean);
                    break;
                case block:
                    CustomBlockBean blockBean = moduleBean.getBlockBean();
                     blockBean.getContainerBean().setUiBean(uiBean);
                    break;
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetUI")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetUI(String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            CustomUIBean uiBean=new CustomUIBean(AnnotationUtil.getAllAnnotations(customMethodAPIBean.getMethod()));

            PanelType panelType = moduleBean.getPanelType();

            switch (panelType) {
                case panel:
                    CustomPanelBean panelBean = moduleBean.getPanelBean();
                    panelBean.getDivBean().setUiBean(uiBean);
                case dialog:
                    DialogBean dialogBean = moduleBean.getDialogBean();
                    dialogBean.getContainerBean().setUiBean(uiBean);
                    break;
                case block:
                    CustomBlockBean blockBean = moduleBean.getBlockBean();
                    blockBean.getContainerBean().setUiBean(uiBean);
                    break;
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
