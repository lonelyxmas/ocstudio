package com.ds.dsm.aggregation.module.panel.block;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.container.CustomBlockView;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomBlockBean;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/agg/config/module/")
public class ModuleBlockService {


    @RequestMapping(method = RequestMethod.POST, value = "BlockConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ModuleBlockView> getBlockConfig(String sourceClassName, String methodName, String domainId) {
        ResultModel<ModuleBlockView> resultModel = new ResultModel();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = esdClassConfig.getMethodByName(methodName);
            CustomBlockBean blockBean = customMethodAPIBean.getModuleBean().getBlockBean();
            if (blockBean == null) {
                blockBean = new CustomBlockBean();
            }

            resultModel.setData(new ModuleBlockView(blockBean, domainId, sourceClassName, methodName));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "updateBlock")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateBlock(@RequestBody CustomBlockView blockView, String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            CustomBlockBean blockBean = moduleBean.getBlockBean();
            blockBean.setBackground(blockView.getBackground());
            blockBean.setResizer(blockView.getResizer());
            blockBean.setSideBarCaption(blockView.getSideBarCaption());
            blockBean.setBackground(blockView.getBackground());
            blockBean.setSideBarType(blockView.getSideBarType());
            blockBean.setSideBarStatus(blockView.getSideBarStatus());
            blockBean.setResizer(blockView.getResizer());
            blockBean.setResizerProp(blockView.getResizerProp());

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetBlock")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetBlock(String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomModuleBean moduleBean = customMethodAPIBean.getModuleBean();
            moduleBean.setBlockBean(new CustomBlockBean());
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
