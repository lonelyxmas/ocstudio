package com.ds.dsm.aggregation.config.data.tree;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.api.enums.ResponsePathTypeEnum;
import com.ds.esd.custom.form.TreeDataBaseBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.enums.ComponentType;

@FormAnnotation(col = 2, customService = TreeDataService.class)
public class TreeBaseDataView {


    @CustomAnnotation(caption = "根节点ID")
    String rootId;

    @CustomAnnotation(caption = "显示字段")
    String fieldCaption;

    @CustomAnnotation(caption = "ID字段名")
    String fieldId;
    @CustomAnnotation(caption = "缓存页面")
    Boolean cache;
    @CustomAnnotation(caption = "自动保存")
    Boolean autoSave;

    @CustomAnnotation(caption = "参数选择")
    ResponsePathTypeEnum itemType;

    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation( caption = "编辑地址")
    String editorPath;
    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation( caption = "保存地址")
    String saveUrl;
    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation( caption = "数据装载")
    String dataUrl;
    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation( caption = "延迟加载地址")
    String loadChildUrl;

    @FieldAnnotation( colSpan = -1, rowHeight = "150", componentType = ComponentType.CodeEditor)
    @CustomAnnotation(caption = "表达式")
    String expression;


    @CustomAnnotation(pid = true, hidden = true)
    String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    String methodName;


    public TreeBaseDataView() {

    }

    public TreeBaseDataView(TreeDataBaseBean treeDataBean) {

        this.sourceClassName = treeDataBean.getSourceClassName();
        this.fieldCaption = treeDataBean.getFieldCaption();
        this.fieldId = treeDataBean.getFieldId();
        this.rootId = treeDataBean.getRootId();
        this.domainId = treeDataBean.getDomainId();
        this.methodName = treeDataBean.getMethodName();
        this.cache = treeDataBean.getCache();
        this.autoSave = treeDataBean.getAutoSave();
        this.editorPath = treeDataBean.getEditorPath();
        this.saveUrl = treeDataBean.getSaveUrl();
        this.dataUrl = treeDataBean.getDataUrl();
        this.loadChildUrl = treeDataBean.getDataUrl();
        this.expression = treeDataBean.getDataUrl();
        this.itemType = treeDataBean.getItemType();

    }

    public Boolean getCache() {
        return cache;
    }

    public void setCache(Boolean cache) {
        this.cache = cache;
    }

    public Boolean getAutoSave() {
        return autoSave;
    }

    public void setAutoSave(Boolean autoSave) {
        this.autoSave = autoSave;
    }

    public String getEditorPath() {
        return editorPath;
    }

    public void setEditorPath(String editorPath) {
        this.editorPath = editorPath;
    }

    public String getSaveUrl() {
        return saveUrl;
    }

    public void setSaveUrl(String saveUrl) {
        this.saveUrl = saveUrl;
    }

    public String getDataUrl() {
        return dataUrl;
    }

    public void setDataUrl(String dataUrl) {
        this.dataUrl = dataUrl;
    }

    public String getLoadChildUrl() {
        return loadChildUrl;
    }

    public void setLoadChildUrl(String loadChildUrl) {
        this.loadChildUrl = loadChildUrl;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public ResponsePathTypeEnum getItemType() {
        return itemType;
    }

    public void setItemType(ResponsePathTypeEnum itemType) {
        this.itemType = itemType;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getRootId() {
        return rootId;
    }

    public void setRootId(String rootId) {
        this.rootId = rootId;
    }

    public String getFieldCaption() {
        return fieldCaption;
    }

    public void setFieldCaption(String fieldCaption) {
        this.fieldCaption = fieldCaption;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }


}
