package com.ds.dsm.aggregation.config.entity.tree;

import com.ds.dsm.aggregation.api.method.AggAPINavItem;
import com.ds.dsm.aggregation.config.entity.ref.AggRefService;
import com.ds.dsm.aggregation.config.entity.tree.field.AggEntityFieldService;
import com.ds.dsm.aggregation.module.ModuleEventNavItem;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.enums.PanelType;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.aggregation.FieldAggConfig;
import com.ds.esd.dsm.aggregation.ref.AggEntityRef;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;


@TabsAnnotation(singleOpen = true)
@TreeAnnotation(lazyLoad = true,
        dynDestory = true,
        customService = AggEntityNavService.class)
public class AggEntityConfigTree extends TreeListItem {


    @Pid
    String domainId;
    @Pid
    String refId;
    @Pid
    String sourceClassName;
    @Pid
    String viewClassName;
    @Pid
    String methodName;
    @Pid
    PanelType panelType;
    @Pid
    String fieldname;


    @TreeItemAnnotation(bindService = EntityMethodService.class, iniFold = true)
    public AggEntityConfigTree(MethodConfig methodAPIBean) {
        CustomMenuItem customMenuItem = methodAPIBean.getDefaultMenuItem();
        String methodName = methodAPIBean.getMethodName();
        if (methodAPIBean.getCaption() != null && !methodAPIBean.getCaption().equals("")) {
            this.caption = methodAPIBean.getName() + "(" + methodAPIBean.getCaption() + ")";
        } else {
            if (customMenuItem != null) {
                this.caption = "WebAPI(" + methodAPIBean.getName() + ")-" + methodAPIBean.getModuleViewType().getName();
            } else {
                this.caption = "WebAPI(" + methodAPIBean.getName() + ")";
            }
        }
        this.methodName = methodName;
        this.sourceClassName = methodAPIBean.getSourceClassName();
        this.viewClassName = methodAPIBean.getViewClassName();
        this.id = sourceClassName.replace(".", "_") + "_" + methodName;
        this.domainId = methodAPIBean.getDomainId();
        this.imageClass = methodAPIBean.getImageClass();
        this.tips = methodAPIBean.getCaption() + "<br/>";
        this.tips = tips + methodAPIBean.getMetaInfo();

    }


    @TreeItemAnnotation(customItems = AggEntityMethodNavItem.class)
    public AggEntityConfigTree(AggEntityMethodNavItem aggEntityMethodNavItem, PanelType panelType, String sourceClassName, String methodName, String domainId) {
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;
        this.methodName = methodName;
        this.panelType = panelType;
        this.id = sourceClassName.replace(".", "_") + "_" + methodName + "_" + aggEntityMethodNavItem.name();
        this.caption = aggEntityMethodNavItem.getName();
        this.imageClass = aggEntityMethodNavItem.getImageClass();
    }


    @TreeItemAnnotation(customItems = ModuleEventNavItem.class)
    public AggEntityConfigTree(ModuleEventNavItem moduleEventNavItem, String sourceClassName, String methodName, String domainId) {
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;
        this.methodName = methodName;
        this.id = sourceClassName.replace(".", "_") + "_" + methodName + "_" + moduleEventNavItem.name();
        this.caption = moduleEventNavItem.getName();
        this.imageClass = moduleEventNavItem.getImageClass();
    }


    @TreeItemAnnotation(customItems = AggEntityNavItem.class, dynDestory = true, dynLoad = true, lazyLoad = true)
    public AggEntityConfigTree(AggEntityNavItem aggEntityNavItem, String sourceMethodName, String methodName, String sourceClassName, String domainId) {
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;
        if (sourceMethodName != null && methodName != null) {
            this.id = sourceClassName + "_" + sourceMethodName + "_" + methodName + aggEntityNavItem.name();
        } else {
            this.id = sourceClassName + "_" + aggEntityNavItem.name();
        }
        this.caption = aggEntityNavItem.getName();
        this.imageClass = aggEntityNavItem.getImageClass();
    }


    @TreeItemAnnotation(customItems = AggAPINavItem.class, dynDestory = true)
    public AggEntityConfigTree(AggAPINavItem aggEntityNavItem, String methodName, String sourceClassName, String domainId) {
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;
        this.id = sourceClassName.replace(".", "_") + "_" + methodName + "_" + aggEntityNavItem.name();
        this.caption = aggEntityNavItem.getName();
        this.methodName = methodName;
        this.imageClass = aggEntityNavItem.getImageClass();
    }

    @TreeItemAnnotation(bindService = AggEntityViewService.class, lazyLoad = true, iniFold = true, dynDestory = true)
    public AggEntityConfigTree(CustomViewBean customView, String sourceClassName) {
        this.methodName = customView.getMethodName();
        if (customView.getCaption() != null && !customView.getCaption().equals("")) {
            this.caption = customView.getCaption() + "(" + customView.getName() + ")";
        } else {
            this.caption = methodName + "(" + customView.getName() + ")-" + customView.getModuleViewType().getName();
        }
        this.panelType = customView.getMethodConfig().getModuleBean().getPanelType();
        this.sourceClassName = sourceClassName;
        this.viewClassName = customView.getViewClassName();
        this.id = sourceClassName.replace(".", "_") + "_" + methodName;
        this.domainId = customView.getDomainId();
        this.imageClass = customView.getImageClass();
        this.tips = customView.getCaption() + "(" + customView.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + customView.getName() + ")";


    }


    @TreeItemAnnotation(bindService = AggRefService.class, iniFold = true, dynDestory = true)
    public AggEntityConfigTree(AggEntityRef aggEntityRef, String sourceClassName, String domainId) {
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;
        this.id = sourceClassName.replace(".", "_") + "_" + aggEntityRef.getRefId();
        this.refId = aggEntityRef.getRefId();
        this.caption = aggEntityRef.getMethodName() + "(" + aggEntityRef.getRefBean().getRef().getName() + ")";
        this.imageClass = aggEntityRef.getRefBean().getRef().getImageClass();
    }


    @TreeItemAnnotation(bindService = AggEntityFieldService.class)
    public AggEntityConfigTree(FieldAggConfig fieldFormInfo, String sourceClassName, String methodName) {
        this.caption = fieldFormInfo.getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        } else {
            caption = fieldFormInfo.getFieldname() + "(" + caption + ")";
        }

        this.imageClass = fieldFormInfo.getImageClass();
        if (imageClass == null || imageClass.equals("")) {
            if (fieldFormInfo.getComponentType() != null) {
                imageClass = fieldFormInfo.getComponentType().getImageClass();
            }
        }
        this.methodName = methodName;
        this.sourceClassName = sourceClassName;
        this.viewClassName = fieldFormInfo.getEntityClassName();
        this.fieldname = fieldFormInfo.getFieldname();
        this.domainId = fieldFormInfo.getDomainId();
        this.id = sourceClassName.replace(".", "_") + "_" + fieldFormInfo.getFieldname() ;


    }

    public PanelType getPanelType() {
        return panelType;
    }

    public void setPanelType(PanelType panelType) {
        this.panelType = panelType;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


}


