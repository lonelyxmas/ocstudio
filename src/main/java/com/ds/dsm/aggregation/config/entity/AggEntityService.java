package com.ds.dsm.aggregation.config.entity;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.info.AggEntityNav;
import com.ds.dsm.aggregation.config.entity.pop.AggEntityTree;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityConfigTree;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/view/entity/config/")
@MethodChinaName(cname = "底部菜单栏", imageClass = "spafont spa-icon-c-gallery")

public class AggEntityService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "聚合实体信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggEntityInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "装载", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggEntityNav> getAggEntityInfo(String domainId, String sourceClassName) {
        ResultModel<AggEntityNav> result = new ResultModel<AggEntityNav>();

        return result;
    }

    @MethodChinaName(cname = "添加实体")
    @RequestMapping(value = {"AggEntityTree"}, method = {RequestMethod.GET, RequestMethod.POST})

    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "添加控制接口", dynLoad = true, dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<AggEntityTree>> getAggEntityTree(String domainId) {
        TreeListResultModel<List<AggEntityTree>> result = new TreeListResultModel<List<AggEntityTree>>();
        try {
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            List<String> ids = Arrays.asList(bean.getAggEntityNames().toArray(new String[]{}));
            result = TreePageUtil.getTreeList(Arrays.asList(domainId), AggEntityTree.class, ids);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "保存控制接口")
    @RequestMapping(value = {"saveAggEntity"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveAggEntity(String domainId, String AggEntityTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (AggEntityTree != null) {
            try {
                DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                if (bean != null) {
                    String[] esdClassNames = StringUtility.split(AggEntityTree, ";");
                    Set classNameSet = new HashSet();
                    for (String esdClassName : esdClassNames) {
                        if (esdClassName.indexOf(":") > -1) {
                            String className = StringUtility.split(esdClassName, ":")[0];
                            if (className.startsWith(bean.getPackageName())) {
                                classNameSet.add(className);
                            }
                        } else {
                            if (esdClassName.startsWith(bean.getPackageName())) {
                                classNameSet.add(esdClassName);
                            }
                        }
                    }
                    DSMFactory.getInstance().getAggregationManager().addAggEntity(domainId, classNameSet);
                }

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除实体关系")
    @RequestMapping(value = {"delAggEntity"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delAggEntity(String domainId, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (domainId != null) {
            try {
                String[] esdClassNames = StringUtility.split(sourceClassName, ";");
                Set classNameSet = new HashSet();
                for (String esdClassName : esdClassNames) {
                    if (esdClassName.indexOf(":") > -1) {
                        String className = StringUtility.split(esdClassName, ":")[0];
                        classNameSet.add(className);
                    } else {
                        classNameSet.add(esdClassName);
                    }
                }
                DSMFactory.getInstance().getAggregationManager().delAggEntity(domainId, classNameSet, true);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    @RequestMapping(method = RequestMethod.POST, value = "AggEntityEditorView")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "950", height = "680")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "聚合配置")
    @ResponseBody
    public TreeListResultModel<List<AggEntityConfigTree>> getAggConfigTree(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggEntityConfigTree>> resultModel = TreePageUtil.getTreeList(Arrays.asList(AggEntityNavItem.values()), AggEntityConfigTree.class);
        return resultModel;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
