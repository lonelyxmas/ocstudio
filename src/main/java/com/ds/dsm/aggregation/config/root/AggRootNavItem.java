package com.ds.dsm.aggregation.config.root;

import com.ds.dsm.aggregation.config.entity.ref.AggEntityRefService;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityMethodNavService;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityViewNavService;
import com.ds.dsm.aggregation.config.entity.tree.field.AggEntityEnumsService;
import com.ds.dsm.aggregation.config.entity.tree.field.AggEntityFieldsService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum AggRootNavItem implements TreeItem {
    ViewMethodConfig("聚合实体", "spafont spa-icon-project", AggEntityViewNavService.class, false, true, true),
    FieldsConfig("实体属性", "spafont spa-icon-c-comboinput", AggEntityFieldsService.class, true, true, true),
    EnumsConfig("值对象", "spafont spa-icon-c-comboinput", AggEntityEnumsService.class, true, true, true),
    CustomMethodConfig("领域事件", "spafont spa-icon-event", AggEntityMethodNavService.class, true, true, true),
    RefsConfig("聚合关系", "spafont spa-icon-c-databinder", AggEntityRefService.class, true, true, true);

    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    AggRootNavItem(String name, String imageClass, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
