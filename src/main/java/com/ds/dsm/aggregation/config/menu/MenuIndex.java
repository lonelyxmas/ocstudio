package com.ds.dsm.aggregation.config.menu;


import com.ds.enums.db.MethodChinaName;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/dsm/agg/menu/")
@MethodChinaName(cname = "动作菜单", imageClass = "spafont spa-icon-c-menu")
public class MenuIndex {

}
