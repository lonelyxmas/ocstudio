package com.ds.dsm.aggregation.config.domain;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.domain.pop.AggDomainTree;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityConfigTree;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(path = "/dsm/agg/domain/config/")
@MethodChinaName(cname = "领域管理", imageClass = "spafont spa-icon-c-gallery")

public class AggDomainService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "通用域服务")
    @RequestMapping(method = RequestMethod.POST, value = "AggDomainList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-buttonview", caption = "通用域服务")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<AggDomainGridView>> getAggDomainList(String domainId) {
        ListResultModel<List<AggDomainGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                for (String className : bean.getAggDomainNames()) {
                    ESDClass esdClass = DSMFactory.getInstance().getClassManager().getAggEntityByName(className, domainId, true);
                    if (esdClass != null) {
                        esdClassList.add(esdClass);
                    }
                }
                result = PageUtil.getDefaultPageList(esdClassList, AggDomainGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "通用域服务")
    @RequestMapping(method = RequestMethod.POST, value = "AggDomainInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "通用域服务", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggDomainNav> getAggDomainInfo(String domainId, String sourceClassName) {
        ResultModel<AggDomainNav> result = new ResultModel<AggDomainNav>();

        return result;
    }

    @MethodChinaName(cname = "导入通用域")
    @RequestMapping(value = {"AggDomainPopTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "导入通用域模型", dynLoad = true, imageClass = "spafont spa-icon-c-cssbox", dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<AggDomainTree>> getAggDomainPopTree(String domainId) {
        TreeListResultModel<List<AggDomainTree>> result = new TreeListResultModel<List<AggDomainTree>>();
        try {
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            List<String> ids = Arrays.asList(bean.getAggDomainNames().toArray(new String[]{}));
            result = TreePageUtil.getTreeList(Arrays.asList(CustomDomainType.values()), AggDomainTree.class, ids);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "保存通用域")
    @RequestMapping(value = {"saveAggDomain"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveAggDomain(String domainId, String AggDomainPopTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (AggDomainPopTree != null) {
            try {
                DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                if (bean != null) {
                    String[] esdClassNames = StringUtility.split(AggDomainPopTree, ";");
                    Set classNameSet = new HashSet();
                    for (String esdClassName : esdClassNames) {
                        AggEntityConfig aggEntityConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(esdClassName, domainId);
                        if (aggEntityConfig != null) {
                            classNameSet.add(esdClassName);
                            List<FieldModuleConfig> moduleConfigs = aggEntityConfig.getModuleMethods();
                            for (FieldModuleConfig moduleConfig : moduleConfigs) {
                                Set<ESDClass> childClasses = moduleConfig.getMethodConfig().getViewClass().getServiceClass();
                                for (ESDClass esdClass : childClasses) {
                                    if (esdClass.getRequestMappingBean() != null && !esdClass.getClassName().equals(esdClassName)) {
                                        classNameSet.add(esdClass.getClassName());
                                    }
                                }
                            }
                        }

                    }
                    DSMFactory.getInstance().getAggregationManager().addAggDomain(domainId, classNameSet);
                }

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除通用域")
    @RequestMapping(value = {"delAggDomain"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delAggDomain(String domainId, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (domainId != null) {
            try {
                String[] esdClassNames = StringUtility.split(sourceClassName, ";");
                Set classNameSet = new HashSet();
                for (String esdClassName : esdClassNames) {
                    if (esdClassName.indexOf(":") > -1) {
                        String className = StringUtility.split(esdClassName, ":")[0];
                        classNameSet.add(className);
                    } else {
                        classNameSet.add(esdClassName);
                    }
                }
                DSMFactory.getInstance().getAggregationManager().delAggDomain(domainId, classNameSet);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    @RequestMapping(method = RequestMethod.POST, value = "AggDomainTree")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "900", height = "680")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "聚合配置")
    @ResponseBody
    public TreeListResultModel<List<AggEntityConfigTree>> getAggDomainTree(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggEntityConfigTree>> resultModel = new TreeListResultModel<List<AggEntityConfigTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(AggEntityNavItem.values()), AggEntityConfigTree.class);
        return resultModel;
    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
