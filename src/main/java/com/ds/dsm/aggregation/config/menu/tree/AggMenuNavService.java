package com.ds.dsm.aggregation.config.menu.tree;

import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/agg/menu/config/")
public class AggMenuNavService {

    @RequestMapping(method = RequestMethod.POST, value = "loadTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public TreeListResultModel<List<AggMenuConfigTree>> loadTree(String domainId, String sourceClassName) {
        TreeListResultModel<List<AggMenuConfigTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(AggMenuNavItem.values()), AggMenuConfigTree.class);
        return result;
    }

}
