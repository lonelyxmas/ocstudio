package com.ds.dsm.aggregation.config.map.pop;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.root.pop.AggRootTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/map/config/")
@MethodChinaName(cname = "值对象管理", imageClass = "spafont spa-icon-c-gallery")

public class AggMapTreeService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "loadChildMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public TreeListResultModel<List<AggRootTree>> loadChildMenu(String domainId) {
        TreeListResultModel<List<AggRootTree>> result = new TreeListResultModel<List<AggRootTree>>();
        try {
            DSMFactory.getInstance().reload();
            List<ESDClass> aggRootBeans = DSMFactory.getInstance().getClassManager().getAllAggRoot();
            result = TreePageUtil.getTreeList(aggRootBeans, AggRootTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return result;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
