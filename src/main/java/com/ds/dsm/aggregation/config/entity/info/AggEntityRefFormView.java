package com.ds.dsm.aggregation.config.entity.info;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.aggregation.ref.AggEntityRef;
import com.ds.web.annotation.RefType;
import com.ds.web.annotation.Required;
import com.ds.web.annotation.ViewType;

@BottomBarMenu
@FormAnnotation(col = 1, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {AggEntityRefService.class})
public class AggEntityRefFormView {


    @CustomAnnotation(uid = true, hidden = true)
    String refId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(caption = "表名", pid = true, readonly = true)
    String className;

    @CustomAnnotation(caption = "关联对象", readonly = true)
    String otherClassName;

    @Required
    @CustomAnnotation(caption = "引用关系")
    RefType ref;

    @Required
    @CustomAnnotation(caption = "默认视图")
    ViewType viewType;


    AggEntityRefFormView() {

    }


    public AggEntityRefFormView(AggEntityRef ref) {
        this.domainId = ref.getDomainId();
        this.ref = ref.getRefBean().getRef();
        this.className = ref.getClassName();
        this.otherClassName = ref.getOtherClassName();
        this.refId = ref.getRefId();
        this.viewType=ref.getRefBean().getView();


    }

    public ViewType getViewType() {
        return viewType;
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getOtherClassName() {
        return otherClassName;
    }

    public void setOtherClassName(String otherClassName) {
        this.otherClassName = otherClassName;
    }

    public RefType getRef() {
        return ref;
    }

    public void setRef(RefType ref) {
        this.ref = ref;
    }

}
