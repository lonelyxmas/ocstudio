package com.ds.dsm.aggregation.config.root;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;


@TabsAnnotation(singleOpen = true)
@TreeAnnotation(lazyLoad = true,
        dynDestory = true,
        customService = AggRootService.class)
public class AggRootConfigTree extends TreeListItem {

    @Pid
    String domainId;
    @Pid
    String sourceClassName;

    @Pid
    String entityClassName;

    @Pid
    String methodName;


    @TreeItemAnnotation(customItems = AggRootNavItem.class, lazyLoad = true, dynDestory = true)
    public AggRootConfigTree(AggRootNavItem aggEntityNavItem, String sourceClassName, String domainId) {
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;
        this.id = aggEntityNavItem.name();
        this.caption = aggEntityNavItem.getName();
        this.imageClass = aggEntityNavItem.getImageClass();
    }


    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

}


