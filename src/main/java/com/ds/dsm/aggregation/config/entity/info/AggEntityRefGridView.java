package com.ds.dsm.aggregation.config.entity.info;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.gallery.annotation.GalleryAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.aggregation.ref.AggEntityRef;
import com.ds.web.annotation.RefType;

@PageBar
@GalleryAnnotation()
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {AggEntityRefService.class}, event = CustomGridEvent.editor)
public class AggEntityRefGridView {


    @CustomAnnotation(uid = true, hidden = true)
    String refId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(caption = "关联表", hidden = true, pid = true)
    String className;

    @CustomAnnotation(caption = "名称", pid = true)
    String name;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "引用关系")
    RefType ref;

    @CustomAnnotation(caption = "关联字段", pid = true)
    String methodName;

    @CustomAnnotation(caption = "关联对象")
    String otherClassName;


    AggEntityRefGridView() {

    }


    public AggEntityRefGridView(AggEntityRef ref) {
        this.name = ref.getRefBean().getRef().getName();
        this.ref = ref.getRefBean().getRef();
        this.domainId = ref.getDomainId();
        this.className = ref.getClassName();
        this.otherClassName = ref.getOtherClassName();
        this.methodName = ref.getMethodName();
        this.refId = ref.getRefId();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getOtherClassName() {
        return otherClassName;
    }

    public void setOtherClassName(String otherClassName) {
        this.otherClassName = otherClassName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RefType getRef() {
        return ref;
    }

    public void setRef(RefType ref) {
        this.ref = ref;
    }
}
