package com.ds.dsm.aggregation.config.view;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/agg/view/config/")
public class AggViewNavService {


    @RequestMapping(method = RequestMethod.POST, value = "loadTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public TreeListResultModel<List<AggViewConfigTree>> loadTree(String domainId, String sourceClassName) {
        TreeListResultModel<List<AggViewConfigTree>> result = new TreeListResultModel<>();
        List<AggViewConfigTree> menuTrees = new ArrayList<>();

        ApiClassConfig customESDClassAPIBean = null;
        try {
            customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
        for (MethodConfig methodAPIBean : customMethods) {
            if (methodAPIBean.getView() != null) {
                AggViewConfigTree aggConfigTree = new AggViewConfigTree(methodAPIBean);
                aggConfigTree.setIniFold(false);
                aggConfigTree.setSub(new ArrayList());
                menuTrees.add(aggConfigTree);
            }
        }

        for (MethodConfig methodAPIBean : customMethods) {
            if (methodAPIBean.getView() == null) {
                AggViewConfigTree aggConfigTree = new AggViewConfigTree(methodAPIBean);
                aggConfigTree.setIniFold(false);
                aggConfigTree.setSub(new ArrayList());
                menuTrees.add(aggConfigTree);
            }
        }
        result.setData(menuTrees);

        return result;
    }

//
//    @RequestMapping(method = RequestMethod.POST, value = "loadChild")
//    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
//    @ResponseBody
//    public TreeListResultModel<List<TreeListItem>> loadChild(String sourceClassName, String methodName, String domainId) {
//        TreeListResultModel<List<TreeListItem>> result = new TreeListResultModel<>();
//        List<TreeListItem> menuTrees = new ArrayList<>();
//        try {
//            AggEntityConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId, domainId);
//
//            //  ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
//            if (methodName == null || methodName.equals("")) {
//                List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
//                for (MethodConfig methodAPIBean : customMethods) {
//                    if (methodAPIBean.isModule()) {
//                        AggMenuConfigTree aggConfigTree = new AggMenuConfigTree(methodAPIBean);
//                        aggConfigTree.setIniFold(false);
//                        aggConfigTree.setSub(new ArrayList());
//                        menuTrees.add(aggConfigTree);
//                    }
//                }
//                for (MethodConfig methodAPIBean : customMethods) {
//                    if (!methodAPIBean.isModule() && !methodAPIBean.getCustomMethodInfo().isSplit()) {
//                        menuTrees.add(new MethodAPITree(methodAPIBean));
//                    }
//                }
//
//            } else {
//                MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
//                menuTrees.add(new WinConfigTree(methodAPIBean));
//                menuTrees.add(new MethodAPITree(methodAPIBean));
//            }
//            result.setData(menuTrees);
//        } catch (JDSException e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }


}
