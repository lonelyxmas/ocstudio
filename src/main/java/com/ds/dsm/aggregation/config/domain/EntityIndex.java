package com.ds.dsm.aggregation.config.domain;


import com.ds.enums.db.MethodChinaName;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/dsm/agg/entity")
@MethodChinaName(cname = "实体服务", imageClass = "spafont spa-icon-c-gallery")
public class EntityIndex {


}
