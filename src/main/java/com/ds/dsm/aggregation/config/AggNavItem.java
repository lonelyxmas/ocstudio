package com.ds.dsm.aggregation.config;

import com.ds.enums.IconEnumstype;

public enum AggNavItem implements IconEnumstype {
    Action("动作", "spafont spa-icon-action"),
    Event("事件", "spafont spa-icon-event"),
    EntityRoot("实体", "spafont spa-icon-c-gallery"),
    Field("字段", "spafont spa-icon-c-comboinput"),
    MenuRoot("菜单", "spafont spa-icon-conf"),
    Menu("菜单", "spafont spa-icon-conf"),
    Menus("菜单集合", "spafont spa-icon-c-menu"),
    ViewRoot("菜单", "spafont spa-icon-c-buttonview"),
    AggRoot("聚合跟", "spafont spa-icon-c-buttonview"),
    DomainRoot("共享域", "spafont spa-icon-c-buttonview"),
    View("视图", "spafont spa-icon-project"),
    Views("视图集合", "spafont spa-icon-c-buttonview"),
    Methods("方法集合", "spafont spa-icon-c-databinder"),
    Method("方法", "spafont spa-icon-c-webapi");
    private final String name;
    private final String className;
    private final String imageClass;
    private String packageName = "dsm.agg.config.grid";


    AggNavItem(String name, String imageClass) {
        this.name = name;
        this.className = packageName + "." + name();
        this.imageClass = imageClass;
    }


    public String getClassName() {
        return className;
    }

    public String getPackageName() {
        return packageName;
    }


    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
