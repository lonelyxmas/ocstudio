package com.ds.dsm.aggregation.config.menu.tree;

import com.ds.esd.custom.tree.enums.TreeItem;

public enum AggMenuNavItem implements TreeItem {
    WebMethodConfig("Web配置", "spafont spa-icon-c-webapi", AggMenuWebService.class, false, true, true),
    MenuMethodConfig("菜单配置", "spafont spa-icon-project", AggMenuConfigNavService.class, false, true, true),
     CustomMethodConfig("方法调用", "spafont spa-icon-conf", AggMenuMethodNavService.class, true, true, true);
    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    AggMenuNavItem(String name, String imageClass, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
