package com.ds.dsm.aggregation.config.menu.pop;

import com.ds.dsm.aggregation.config.menu.AggMenuService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = AggMenuService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class AggMenuTree extends TreeListItem {

    @Pid
    CustomMenuType menuType;
    @Pid
    String domainId;
    @Pid
    String euClassName;

    @TreeItemAnnotation(bindService = AggMenuTreeService.class, dynLoad = true, dynDestory = true)
    public AggMenuTree(CustomMenuType menuType, String domainId) {
        this.caption = menuType.getName();
        this.imageClass = menuType.getImageClass();
        this.id = menuType.getType();
        this.domainId = domainId;
        this.menuType = menuType;

    }


    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-grid")
    public AggMenuTree(ESDClass esdClass, String domainId) {
        this.caption = esdClass.getName() + "(" + esdClass.getDesc() + ")";
        this.euClassName = esdClass.getClassName();
        this.id = esdClass.getClassName();
        this.domainId = domainId;

    }

    public CustomMenuType getMenuType() {
        return menuType;
    }

    public void setMenuType(CustomMenuType menuType) {
        this.menuType = menuType;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

}
