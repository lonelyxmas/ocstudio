package com.ds.dsm.aggregation.config.menu.tree;

import com.ds.bpm.client.RouteToType;

public class RouteToTypeViewBean {
    String methodName;
    RouteToType routeToType;

    public RouteToTypeViewBean() {

    }

    public RouteToTypeViewBean(RouteToType routeToType, String methodName) {
        this.routeToType = routeToType;
        this.methodName = methodName;

    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public RouteToType getRouteToType() {
        return routeToType;
    }

    public void setRouteToType(RouteToType routeToType) {
        this.routeToType = routeToType;
    }
}
