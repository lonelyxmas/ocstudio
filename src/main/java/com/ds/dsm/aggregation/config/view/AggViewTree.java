package com.ds.dsm.aggregation.config.view;

import com.ds.common.JDSException;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.AggregationType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@TabsAnnotation(singleOpen = true)
@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = AggViewService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class AggViewTree extends TreeListItem {

    public AggViewTree(String domainId) {
        this.caption = "聚合实体";
        this.setIniFold(false);
        this.addTagVar("domainId", domainId);
        this.setImageClass("spafont spa-icon-coin");
        this.id = "all";
        try {
            DSMFactory.getInstance().reload();
            List<ESDClass> esdServiceBeans = new ArrayList<>();
            List<ESDClass> aggServiceBeans = DSMFactory.getInstance().getClassManager().getAllAggView();

            for (ESDClass esdClass : aggServiceBeans) {
                if (esdClass.getSourceClass().getClassName().equals(esdClass.getClassName())) {
                    esdServiceBeans.add(esdClass);
                }
            }
            if (esdServiceBeans.size() > 0) {
                this.addChild(new AggViewTree(esdServiceBeans, AggregationType.view, domainId));
            }


        } catch (JDSException e) {
            e.printStackTrace();
        }
    }

    public AggViewTree(List<ESDClass> esdServiceBeans, AggregationType type, String domainId) {
        this.caption = type.getName();
        this.setIniFold(false);
        this.addTagVar("domainId", domainId);
        this.setImageClass(type.getImageClass());
        this.id = type.getType();
        if (esdServiceBeans.size() > 0) {
            for (ESDClass child : esdServiceBeans) {
                this.addChild(new AggViewTree(child, domainId));
            }
        }

    }

    public AggViewTree(ESDClass esdClass, String domainId) {
        this.caption = esdClass.getName() + "(" + esdClass.getDesc() + ")";
        this.setEuClassName(esdClass.getClassName());
        this.setImageClass("spafont spa-icon-c-grid");
        this.setIniFold(false);
        this.tagVar = new HashMap<>();
        this.tagVar.put("euClassName", esdClass.getClassName());
        this.tagVar.put("domainId", domainId);
        this.id = esdClass.getClassName();

    }

}
