package com.ds.dsm.aggregation.config.entity.pop;

import com.ds.dsm.aggregation.config.entity.AggEntityService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@BottomBarMenu()
@TreeAnnotation(heplBar = true, lazyLoad = true, selMode = SelModeType.multibycheckbox, customService = AggEntityService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class AggEntityTree extends TreeListItem {

    @Pid
    String domainId;

    @Pid
    String euClassName;

    @TreeItemAnnotation(imageClass = "spafont spa-icon-coin", dynDestory = true, lazyLoad = true, caption = "聚合实体", bindService = AggEntityTreeService.class)
    public AggEntityTree(String domainId) {
        this.caption = "聚合实体";
        this.domainId = domainId;
        this.id = "all";

    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-grid")
    public AggEntityTree(ESDClass esdClass, String domainId) {
        this.caption = esdClass.getName() + "(" + esdClass.getDesc() + ")";
        this.euClassName = esdClass.getClassName();
        this.id = esdClass.getClassName();
        this.domainId = domainId;

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

}
