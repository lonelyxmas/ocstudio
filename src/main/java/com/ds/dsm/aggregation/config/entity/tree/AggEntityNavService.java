package com.ds.dsm.aggregation.config.entity.tree;

import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.AggNavItem;
import com.ds.dsm.aggregation.config.entity.info.AggEntityNav;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/agg/entity/config/nav/")
public class AggEntityNavService {


    @MethodChinaName(cname = "聚合实体信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggEntityInfo")
    @NavGroupViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, caption = "装载", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggEntityNav> getAggEntityInfo(String domainId, String sourceClassName) {
        ResultModel<AggEntityNav> result = new ResultModel<AggEntityNav>();

        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadEntity")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggEntityConfigTree>> loadEntity(String sourceClassName, AggNavItem aggNavItem, String domainId) {
        TreeListResultModel<List<AggEntityConfigTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(AggEntityMethodNavItem.values()), AggEntityConfigTree.class);
        return result;
    }


}
