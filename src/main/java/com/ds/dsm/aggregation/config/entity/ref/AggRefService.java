package com.ds.dsm.aggregation.config.entity.ref;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.config.entity.info.AggEntityRefFormView;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.ref.AggEntityRef;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/agg/entity/config/ref/")
public class AggRefService {


    @RequestMapping(method = RequestMethod.POST, value = "RefInfo")
    @FormViewAnnotation
    @ModuleAnnotation(caption = "关联表信息")
    @DialogAnnotation(width = "300", height = "220")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor, autoRun = true)
    @ResponseBody
    public ResultModel<AggEntityRefFormView> getRefInfo(String refId, String domainId) {
        ResultModel<AggEntityRefFormView> result = new ResultModel<AggEntityRefFormView>();
        try {
            AggEntityRef ref = DSMFactory.getInstance().getAggregationManager().getAggEntityRefById(refId, domainId);
            result.setData(new AggEntityRefFormView(ref));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
