package com.ds.dsm.aggregation.config.data.tree;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.tree.TreeBaseDataView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.foldingtree.NavFoldingTreeViewBean;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.TreeDataBaseBean;
import com.ds.esd.custom.form.pop.PopTreeDataBean;
import com.ds.esd.custom.form.pop.PopTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/entity/config/tree/")

public class TreeDataService {


    @MethodChinaName(cname = "清空列表配置")
    @RequestMapping(method = RequestMethod.POST, value = "clearData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formReSet)
    public @ResponseBody
    ResultModel<Boolean> clearData(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            classConfig.getSourceConfig().getMethodByName(methodName).setView(null);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateTreeDataBase")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formSave)
    public @ResponseBody
    ResultModel<Boolean> updateTreeDataBase(@RequestBody TreeBaseDataView dataView) {

        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {

            TreeDataBaseBean customTreeDataBean = null;
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(dataView.getSourceClassName(), dataView.getDomainId());


            customTreeDataBean.setFieldCaption(dataView.getFieldCaption());
            customTreeDataBean.setFieldId(dataView.getFieldId());
            customTreeDataBean.setRootId(dataView.getRootId());
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
