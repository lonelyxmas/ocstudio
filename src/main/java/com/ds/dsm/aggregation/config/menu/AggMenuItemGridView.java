package com.ds.dsm.aggregation.config.menu;


import com.ds.enums.IconEnumstype;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.web.annotation.Required;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload})
public class AggMenuItemGridView {

    @CustomAnnotation(hidden = true, uid = true)
    public String id;
    @Required
    @CustomAnnotation(caption = "方法名")
    public String type;
    @Required
    @CustomAnnotation(caption = "名称")
    public String caption;
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    private String imageClass;
    @CustomAnnotation(caption = "提示")
    public String tips;


    public AggMenuItemGridView(IconEnumstype routeToType) {
        this.caption = routeToType.getName();
        this.type = routeToType.getType();
        this.tips = routeToType.getType() + "(" + routeToType.getName() + ")";
        this.imageClass = routeToType.getImageClass();
        this.id = routeToType.getType();

    }

    public AggMenuItemGridView(TreeListItem item) {
        this.caption = item.getCaption();
        this.tips = item.getTips();
        this.type = item.getId();
        this.imageClass = item.getImageClass();
        this.id = item.getId();

    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }
}
