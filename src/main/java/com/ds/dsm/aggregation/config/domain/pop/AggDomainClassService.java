package com.ds.dsm.aggregation.config.domain.pop;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/agg/domain/config/")
public class AggDomainClassService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @RequestMapping(method = RequestMethod.POST, value = "loadChildAgg")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggDomainTree>> loadChildAgg(String esdClassName, String domainId) {
        TreeListResultModel<List<AggDomainTree>> result = new TreeListResultModel<List<AggDomainTree>>();
        AggEntityConfig aggEntityConfig = null;
        List<ESDClass> apiClassConfigSet = new ArrayList<>();
        try {
            aggEntityConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(esdClassName, domainId);
            List<FieldModuleConfig> moduleConfigs = aggEntityConfig.getModuleMethods();
            for (FieldModuleConfig moduleConfig : moduleConfigs) {
                Set<ESDClass> childClasses = moduleConfig.getMethodConfig().getViewClass().getServiceClass();
                for (ESDClass esdClass : childClasses) {

                    if (esdClass.getRequestMappingBean() != null && !esdClass.getClassName().equals(esdClassName) && !apiClassConfigSet.contains(esdClass)) {
                        apiClassConfigSet.add(esdClass);
                    }
                }
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

        result = TreePageUtil.getTreeList(apiClassConfigSet, AggDomainTree.class);
        return result;

    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
