package com.ds.dsm.aggregation.config.view;


import com.ds.enums.db.MethodChinaName;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/dsm/agg/root/")
@MethodChinaName(cname = "聚合跟", imageClass = "spafont spa-icon-module")
public class AggViewIndex {

}
