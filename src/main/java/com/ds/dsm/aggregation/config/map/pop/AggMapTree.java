package com.ds.dsm.aggregation.config.map.pop;

import com.ds.dsm.aggregation.config.root.AggRootService;
import com.ds.dsm.aggregation.config.root.pop.AggRootTreeService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = AggRootService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class AggMapTree extends TreeListItem {


    @Pid
    String domainId;

    @Pid
    String className;

    @TreeItemAnnotation(imageClass = "spafont spa-icon-coin", dynDestory = true, lazyLoad = true, caption = "值对象", bindService = AggRootTreeService.class)
    public AggMapTree(String domainId) {
        this.caption = "值对象";
        this.domainId = domainId;
        this.id = "all";

    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-grid")
    public AggMapTree(ESDClass esdClass, String domainId) {
        this.caption = esdClass.getName() + "(" + esdClass.getDesc() + ")";
        this.setEuClassName(esdClass.getClassName());
        this.id = esdClass.getClassName();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


}
