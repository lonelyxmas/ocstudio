package com.ds.dsm.aggregation.config.entity.tree.field;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.field.config.AggFieldFormView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.FieldAggConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/agg/entity/config/field/")
public class AggEntityFieldService {

    @MethodChinaName(cname = "字段信息")
    @RequestMapping(method = RequestMethod.POST, value = "FieldInfo")
    @FormViewAnnotation
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<AggFieldFormView> getColInfo(String entityClassName, String domainId, String fieldname) {
        ResultModel<AggFieldFormView> result = new ResultModel<AggFieldFormView>();
        try {
            AggFieldFormView view = null;
            AggEntityConfig classConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(entityClassName, domainId);
            FieldAggConfig formInfo = classConfig.getFieldByName(fieldname);
            if (formInfo != null) {

                view = new AggFieldFormView(formInfo);
            } else {
                view = new AggFieldFormView();
            }
            result.setData(view);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


}
