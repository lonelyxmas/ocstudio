package com.ds.dsm.aggregation.config.api.tree;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/agg/api/config/")
public class AggAPINavService {


    @RequestMapping(method = RequestMethod.POST, value = "loadTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public TreeListResultModel<List<AggAPIConfigTree>> loadTree(String domainId, String sourceClassName) {
        TreeListResultModel<List<AggAPIConfigTree>> result = new TreeListResultModel<>();
        List<AggAPIConfigTree> menuTrees = new ArrayList<>();

        ApiClassConfig customESDClassAPIBean = null;
        try {
            customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
        for (MethodConfig methodAPIBean : customMethods) {
            if (methodAPIBean.getView() != null) {
                AggAPIConfigTree aggConfigTree = new AggAPIConfigTree(methodAPIBean);
                aggConfigTree.setIniFold(false);
                aggConfigTree.setSub(new ArrayList());
                menuTrees.add(aggConfigTree);
            }
        }

        for (MethodConfig methodAPIBean : customMethods) {
            if (methodAPIBean.getView() == null) {
                AggAPIConfigTree aggConfigTree = new AggAPIConfigTree(methodAPIBean);
                aggConfigTree.setIniFold(false);
                aggConfigTree.setSub(new ArrayList());
                menuTrees.add(aggConfigTree);
            }
        }
        result.setData(menuTrees);

        return result;
    }




}
