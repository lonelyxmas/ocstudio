package com.ds.dsm.aggregation.config.map;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.info.AggEntityNav;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.dsm.aggregation.config.map.pop.AggMapTree;
import com.ds.dsm.aggregation.config.root.AggRootConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(path = "/dsm/agg/map/config/")
@MethodChinaName(cname = "值对象管理", imageClass = "spafont spa-icon-c-gallery")

public class AggMapService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;

    @MethodChinaName(cname = "值对象信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggMapList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-buttonview", caption = "值对象信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public ListResultModel<List<AggMapGridView>> getAggEntityList(String domainId) {
        ListResultModel<List<AggMapGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                for (String className : bean.getAggRootNames()) {
                    ESDClass esdClass = DSMFactory.getInstance().getClassManager().getAggEntityByName(className, domainId, true);
                    if (esdClass != null) {
                        esdClassList.add(esdClass);
                    }
                }
                result = PageUtil.getDefaultPageList(esdClassList, AggMapGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "值对象信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggMapInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "装载", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggEntityNav> getAggMapInfo(String domainId, String sourceClassName) {
        ResultModel<AggEntityNav> result = new ResultModel<AggEntityNav>();

        return result;
    }

    @MethodChinaName(cname = "添加值对象")
    @RequestMapping(value = {"AggMapTree"}, method = {RequestMethod.GET, RequestMethod.POST})

    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation( caption = "添加聚合接口", dynLoad = true,dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<AggMapTree>> getAggMapTree(String domainId) {
        TreeListResultModel<List<AggMapTree>> result = new TreeListResultModel<List<AggMapTree>>();
        try {
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            List<String> ids = Arrays.asList(bean.getAggMapNames().toArray(new String[]{}));
            result = TreePageUtil.getTreeList(Arrays.asList(domainId), AggMapTree.class, ids);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "保存聚合跟接口")
    @RequestMapping(value = {"saveAggMap"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveAggMap(String domainId, String AggMapTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (AggMapTree != null) {
            try {
                DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                if (bean != null) {
                    Set classNameSet = new HashSet();
                    String[] esdClassNames = StringUtility.split(AggMapTree, ";");
                    for (String esdClassName : esdClassNames) {
                        if (esdClassName.indexOf(":") > -1) {
                            String className = StringUtility.split(esdClassName, ":")[0];
                            classNameSet.add(className);
                        } else {
                            classNameSet.add(esdClassName);
                        }
                    }
                    DSMFactory.getInstance().getAggregationManager().addAggMap(domainId, classNameSet);
                }

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除实体关系")
    @RequestMapping(value = {"delAggMap"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delAggMap(String domainId, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (domainId != null) {
            try {
                Set classNameSet = new HashSet();
                String[] esdClassNames = StringUtility.split(sourceClassName, ";");
                for (String esdClassName : esdClassNames) {
                    if (esdClassName.indexOf(":") > -1) {
                        String className = StringUtility.split(esdClassName, ":")[0];
                        classNameSet.add(className);
                    } else {
                        classNameSet.add(esdClassName);
                    }
                }
                DSMFactory.getInstance().getAggregationManager().delAggMap(domainId, classNameSet);

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    @RequestMapping(method = RequestMethod.POST, value = "AggMapEditorView")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "900", height = "680")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "值对象配置")
    @ResponseBody
    public TreeListResultModel<List<AggMapConfigTree>> getAggRootConfigTree(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggMapConfigTree>> resultModel = TreePageUtil.getTreeList(Arrays.asList(AggEntityNavItem.values()), AggMapConfigTree.class);
        return resultModel;
    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
