package com.ds.dsm.aggregation.config.api.tree;

import com.ds.dsm.aggregation.config.entity.tree.EntityMethodService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;


@TabsAnnotation(singleOpen = true)
@TreeAnnotation(lazyLoad = true,
        dynDestory = true,
        customService = AggAPINavService.class)
public class AggAPIConfigTree extends TreeListItem {

    @Pid
    String sourceClassName;
    @Pid
    String methodName;
    @Pid
    String domainId;


    @TreeItemAnnotation(bindService = EntityMethodService.class)
    public AggAPIConfigTree(MethodConfig methodAPIBean) {
        ModuleViewType viewType = methodAPIBean.getModuleViewType();
        String methodName = methodAPIBean.getMethodName();
        CustomMenuItem customMenuItem = methodAPIBean.getDefaultMenuItem();
        if (methodAPIBean.getCaption() != null && !methodAPIBean.getCaption().equals("")) {
            this.caption = methodAPIBean.getCaption() + "(" + methodAPIBean.getName() + ")";
        } else {
            if (customMenuItem != null) {
                this.caption = methodName + "(" + methodAPIBean.getName() + ")-" + methodAPIBean.getModuleViewType().getName();
            } else {
                this.caption = methodName + "(" + viewType.getName() + ")-" + methodAPIBean.getModuleViewType().getName();
            }
        }
        if (viewType != null) {
            this.tips = methodAPIBean.getCaption() + "(" + viewType.getName() + ")<br/>";
            this.tips = tips + methodName + "(" + viewType.getName() + ")";
            this.imageClass = viewType.getImageClass();
            this.id = viewType.getType() + methodName;
        } else {
            this.tips = methodAPIBean.getCaption() + "(" + methodName + ")";
            this.imageClass = methodAPIBean.getImageClass();
            this.id = methodName;
        }


    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}


