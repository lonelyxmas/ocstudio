package com.ds.dsm.aggregation.config.view;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.action.ESDMenuAction;
import com.ds.dsm.aggregation.api.method.MethodAPITree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavFoldingTreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RequestMapping("/dsm/agg/view/config/")
@MethodChinaName(cname = "视图配置")
@Controller
@BottomBarMenu(menuClass = ESDMenuAction.class)
@TabsAnnotation(singleOpen = true)
@TreeAnnotation
@NavFoldingTreeAnnotation(bottombarMenu = CustomFormMenu.Close)
public class AggViewEditorView {
    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    public AggViewEditorView() {

    }


    @RequestMapping(method = RequestMethod.POST, value = "AggConfigTree")
    @APIEventAnnotation(autoRun = true)
    @TreeViewAnnotation
    @ModuleAnnotation( imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "聚合配置")
    @ResponseBody
    public TreeListResultModel<List<AggViewConfigTree>> getAggConfigTree(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggViewConfigTree>> resultModel = new TreeListResultModel<List<AggViewConfigTree>>();
        try {
            AggViewConfigTree aggItem = new AggViewConfigTree(sourceClassName, domainId);
            List<AggViewConfigTree> items = new ArrayList<>();
            items.add(aggItem);
            AggEntityConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName,  domainId);
            List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
            for (MethodConfig methodAPIBean : customMethods) {
                if (methodAPIBean.getView() != null) {
                    aggItem.addChild(new AggViewConfigTree(methodAPIBean));
                }
            }
            for (MethodConfig methodAPIBean : customMethods) {
                if (methodAPIBean.getView() == null) {
                    aggItem.addChild(new MethodAPITree(methodAPIBean));
                }
            }


            if (id != null && !id.equals("")) {
                String[] orgIdArr = StringUtility.split(id, ";");
                resultModel.setIds(Arrays.asList(orgIdArr));
            } else {
                resultModel.setIds(Arrays.asList(new String[]{aggItem.getFristClassItem(aggItem).getId()}));
            }
            resultModel.setData(items);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return resultModel;

    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }


}
