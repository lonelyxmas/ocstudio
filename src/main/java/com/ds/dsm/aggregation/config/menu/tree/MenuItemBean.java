package com.ds.dsm.aggregation.config.menu.tree;

import com.ds.esd.custom.enums.CustomMenuItem;

public class MenuItemBean {
    String methodName;
    CustomMenuItem customMenuItem;

    public MenuItemBean() {

    }

    public MenuItemBean(CustomMenuItem customMenuItem, String methodName) {
        this.customMenuItem = customMenuItem;
        this.methodName = methodName;

    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public CustomMenuItem getCustomMenuItem() {
        return customMenuItem;
    }

    public void setCustomMenuItem(CustomMenuItem customMenuItem) {
        this.customMenuItem = customMenuItem;
    }
}
