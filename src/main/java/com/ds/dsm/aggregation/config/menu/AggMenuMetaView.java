package com.ds.dsm.aggregation.config.menu;


import com.ds.common.JDSException;
import com.ds.common.util.ClassUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.toolbar.RouteInstToolBar;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.bpm.RouteCustomMenu;
import com.ds.esd.custom.toolbar.dynbar.CustomContextBar;
import com.ds.esd.custom.toolbar.dynbar.CustomMenusBar;
import com.ds.esd.custom.toolbar.dynbar.CustomToolsBar;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.editor.extmenu.MenuBarBean;
import com.ds.esd.editor.extmenu.PluginsFactory;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.component.navigation.MenuBarProperties;
import com.ds.esd.tool.ui.component.navigation.PopMenuProperties;
import com.ds.esd.tool.ui.component.navigation.ToolBarProperties;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.enums.event.enums.MenuEventEnum;
import com.ds.esd.tool.ui.enums.event.enums.PopMenuEventEnum;
import com.ds.esd.tool.ui.enums.event.enums.ToolBarEventEnum;
import com.ds.web.util.AnnotationUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/agg/menu/ref/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "菜单应用", imageClass = "spafont spa-icon-c-databinder")
public class AggMenuMetaView {
    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @RequestMapping(method = RequestMethod.POST, value = "AggMenuConfig")
    @CustomAnnotation(index = 0)
    @ModuleAnnotation(caption = "路由视图", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<AggMenuConfigView>> getAggMenuConfig(String sourceClassName, String domainId) {
        ListResultModel<List<AggMenuConfigView>> result = new ListResultModel<List<AggMenuConfigView>>();
        try {
            AggEntityConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName,  domainId);
            List<FieldModuleConfig> attMethods = tableConfig.getModuleMethods();
            result = PageUtil.getDefaultPageList(attMethods, AggMenuConfigView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AttMenuMethod")
    @CustomAnnotation(index = 1)
    @ModuleAnnotation(caption = "菜单", imageClass = "spafont spa-icon-project",dock = Dock.fill)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<AggMenuItemGridView>> getAttMethod(String sourceClassName, String domainId) {
        ListResultModel<List<AggMenuItemGridView>> result = new ListResultModel<List<AggMenuItemGridView>>();
        try {
            AggEntityConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName,  domainId);
            List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
            List<TreeListItem> childs = new ArrayList<>();
            for (MethodConfig methodAPIBean : customMethods) {
                Class clazz = null;
                try {
                    clazz = ClassUtility.loadClass(methodAPIBean.getSourceClassName());
                    MenuBarBean menuBarBean = PluginsFactory.getInstance().checkMenuBar(ClassUtility.loadClass(methodAPIBean.getSourceClassName()));
                    RouteCustomMenu customMenu = AnnotationUtil.getMethodAnnotation(methodAPIBean.getMethod(), RouteCustomMenu.class);
                    switch (menuBarBean.getMenuType()) {
                        case bpm:

                            RouteInstToolBar<MenuBarProperties, MenuEventEnum> bpmmenubar = (RouteInstToolBar) PluginsFactory.getInstance().initMenuClass(clazz);
                            childs = bpmmenubar.getProperties().getItems();
                            break;
                        case toolbar:
                            CustomToolsBar<ToolBarProperties, ToolBarEventEnum> toolsBar = (CustomToolsBar) PluginsFactory.getInstance().initMenuClass(clazz);
                            List<TreeListItem> items = toolsBar.getProperties().getItems();
                            if (items.size() > 0) {
                                childs = items.get(0).getSub();
                            }

                            break;
                        case menubar:
                            CustomMenusBar<MenuBarProperties, MenuEventEnum> menubar = (CustomMenusBar) PluginsFactory.getInstance().initMenuClass(clazz);
                            childs = menubar.getProperties().getItems();
                            break;
                        case contextmenu:
                            CustomContextBar<PopMenuProperties<TreeListItem>, PopMenuEventEnum> contextBar = (CustomContextBar) PluginsFactory.getInstance().initMenuClass(clazz);
                            childs = contextBar.getProperties().getItems();
                            break;
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }


            result = PageUtil.getDefaultPageList(childs, AggMenuItemGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
