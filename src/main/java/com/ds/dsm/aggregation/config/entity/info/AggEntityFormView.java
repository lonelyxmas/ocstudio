package com.ds.dsm.aggregation.config.entity.info;


import com.ds.dsm.aggregation.config.entity.AggEntityService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = AggEntityService.class)
public class AggEntityFormView {


    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(uid = true, hidden = true)
    public String sourceClassName;

    @Required
    @CustomAnnotation(caption = "名称")
    public String name;
    @Required
    @CustomAnnotation(caption = "包名")
    public String packageName;


    @CustomAnnotation(caption = "描述", hidden = true)
    private String desc;

    public AggEntityFormView(ESDClass esdClass) {
        this.name = esdClass.getName();

        this.domainId=esdClass.getDomainId();
        this.sourceClassName = esdClass.getCtClass().getName();
        this.packageName = esdClass.getCtClass().getPackage().getName();
        this.desc = esdClass.getDesc();

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
