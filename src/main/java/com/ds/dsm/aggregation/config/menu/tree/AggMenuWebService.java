package com.ds.dsm.aggregation.config.menu.tree;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.api.APIConfigBaseView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/agg/menu/config/method/")
public class AggMenuWebService {

    @MethodChinaName(cname = "WEB配置")
    @RequestMapping(method = RequestMethod.POST, value = "ConfigBaseInfo")
    @FormViewAnnotation
    @ModuleAnnotation(dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<APIConfigBaseView> getConfigBaseInfo(String domainId, String sourceClassName) {
        ResultModel<APIConfigBaseView> result = new ResultModel<APIConfigBaseView>();
        AggEntityConfig tableConfig = null;
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        result.setData(new APIConfigBaseView(tableConfig));
        return result;
    }


    @MethodChinaName(cname = "保存控制接口")
    @RequestMapping(value = {"saveWebMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveAggMenu(String domainId, String sourceClassName, String url) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        AggEntityConfig tableConfig = null;
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
            tableConfig.setUrl(url);
            DSMFactory.getInstance().getAggregationManager().updateAggEntityConfig(tableConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return result;

    }


}
