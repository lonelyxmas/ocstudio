package com.ds.dsm.aggregation.config.entity;


import com.ds.enums.db.MethodChinaName;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/dsm/agg/domain")
@MethodChinaName(cname = "领域服务", imageClass = "spafont spa-icon-module")
public class EntityIndex {


}
