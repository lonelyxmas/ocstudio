package com.ds.dsm.aggregation.config.entity.tree;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.info.AggEntityNav;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/dsm/agg/entity/config/method/")
public class AggEntityMethodNavService {


    @MethodChinaName(cname = "聚合实体信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggEntityInfo")
    @NavGroupViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, caption = "装载", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggEntityNav> getAggEntityInfo(String domainId, String sourceClassName) {
        ResultModel<AggEntityNav> result = new ResultModel<AggEntityNav>();

        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadMethod")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggEntityConfigTree>> loadMethod(String sourceClassName, String domainId) {
        TreeListResultModel<List<AggEntityConfigTree>> result = new TreeListResultModel<>();
        try {
            AggEntityConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
            if (customESDClassAPIBean!=null){
                List<MethodConfig> customMethods = customESDClassAPIBean.getServiceMethods();
                result = TreePageUtil.getDefaultTreeList(customMethods, AggEntityConfigTree.class);
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


}
