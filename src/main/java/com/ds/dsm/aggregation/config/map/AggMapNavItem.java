package com.ds.dsm.aggregation.config.map;

import com.ds.dsm.aggregation.config.entity.tree.AggEntityMethodNavService;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityViewNavService;
import com.ds.dsm.aggregation.config.entity.tree.field.AggEntityFieldsService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum AggMapNavItem implements TreeItem {
    ViewMethodConfig("视图应用", "spafont spa-icon-project", AggEntityViewNavService.class, false, true, true),
    CustomMethodConfig("API服务", "spafont spa-icon-conf", AggEntityMethodNavService.class, true, true, true),
    //  RefsConfig("关联关系", "spafont spa-icon-c-databinder", AggEntityFieldsService.class, true, true, true),
    FieldsConfig("实体子项", "spafont spa-icon-c-comboinput", AggEntityFieldsService.class, true, true, true);

    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    AggMapNavItem(String name, String imageClass, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
