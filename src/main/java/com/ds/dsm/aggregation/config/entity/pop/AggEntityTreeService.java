package com.ds.dsm.aggregation.config.entity.pop;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/entity/config/")
@MethodChinaName(cname = "实体管理", imageClass = "spafont spa-icon-c-gallery")

public class AggEntityTreeService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;

    @RequestMapping(method = RequestMethod.POST, value = "loadChildEntity")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggEntityTree>> loadChildEntity(String domainId) {
        TreeListResultModel<List<AggEntityTree>> result = new TreeListResultModel<List<AggEntityTree>>();
        try {
            // DSMFactory.getInstance().reload();
            List<ESDClass> esdServiceBeans = new ArrayList<>();
            List<ESDClass> aggEntitiesBeans = DSMFactory.getInstance().getClassManager().getAggEntities();
            DomainInst domainInst=DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);

            for (ESDClass esdClass : aggEntitiesBeans) {
                if (esdClass.getPackageName().startsWith(domainInst.getPackageName())){
                    esdServiceBeans.add(esdClass);
                }

            }
            result = TreePageUtil.getTreeList(esdServiceBeans, AggEntityTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }


        return result;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
