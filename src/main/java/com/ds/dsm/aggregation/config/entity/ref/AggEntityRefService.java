package com.ds.dsm.aggregation.config.entity.ref;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.info.AggEntityRefGridView;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityConfigTree;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.ref.AggEntityRef;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/dsm/agg/entity/config/ref/")
public class AggEntityRefService {


    @RequestMapping(method = RequestMethod.POST, value = "EsdClassRefs")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-databinder", caption = "关联关系")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor, autoRun = true)
    @ResponseBody
    public ListResultModel<List<AggEntityRefGridView>> getEsdClassRefs(String sourceClassName, String domainId) {
        ListResultModel<List<AggEntityRefGridView>> tables = new ListResultModel();
        try {
            DSMFactory factory = DSMFactory.getInstance(ESDFacrory.getESDClient().getSpace());
            List<AggEntityRef> dsmRefs = factory.getAggregationManager().getEntityRefByName(sourceClassName, domainId);
            tables = PageUtil.getDefaultPageList(dsmRefs, AggEntityRefGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return tables;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadRefs")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggEntityConfigTree>> loadRefs(String sourceClassName, String domainId) {
        TreeListResultModel<List<AggEntityConfigTree>> result = new TreeListResultModel<>();
        try {
            DSMFactory factory = DSMFactory.getInstance(ESDFacrory.getESDClient().getSpace());
            List<AggEntityRef> dsmRefs = factory.getAggregationManager().getEntityRefByName(sourceClassName, domainId);
            result = TreePageUtil.getDefaultTreeList(dsmRefs, AggEntityConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


}
