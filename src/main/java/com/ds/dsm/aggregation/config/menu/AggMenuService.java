package com.ds.dsm.aggregation.config.menu;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.menu.pop.AggMenuTree;
import com.ds.dsm.aggregation.config.menu.tree.AggMenuConfigTree;
import com.ds.dsm.aggregation.config.menu.tree.AggMenuMainNavItem;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(path = "/dsm/agg/menu/config/")
@MethodChinaName(cname = "菜单管理", imageClass = "spafont spa-icon-c-gallery")

public class AggMenuService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;




    @MethodChinaName(cname = "聚合动作信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggMenuInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "装载", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggMenuNav> getAggMenuInfo(String domainId, String sourceClassName) {
        ResultModel<AggMenuNav> result = new ResultModel<AggMenuNav>();

        return result;
    }

    @MethodChinaName(cname = "导入菜单动作")
    @RequestMapping(value = {"AggMenuTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "导入菜单动作", dynLoad = true, dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<AggMenuTree>> getAggMenuTree(String domainId) {
        TreeListResultModel<List<AggMenuTree>> result = new TreeListResultModel<List<AggMenuTree>>();
        try {
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            List<String> ids = Arrays.asList(bean.getAggMenuNames().toArray(new String[]{}));
            result = TreePageUtil.getTreeList(Arrays.asList(CustomMenuType.values()), AggMenuTree.class, ids);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "保存控制接口")
    @RequestMapping(value = {"saveAggMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveAggMenu(String domainId, String AggMenuTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (AggMenuTree != null) {
            try {
                DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                if (bean != null) {
                    String[] esdClassNames = StringUtility.split(AggMenuTree, ";");
                    Set classNameSet = new HashSet();
                    for (String esdClassName : esdClassNames) {
                        if (esdClassName.indexOf(":") > -1) {
                            String className = StringUtility.split(esdClassName, ":")[0];
                            classNameSet.add(className);
                        } else {
                            classNameSet.add(esdClassName);
                        }
                    }
                    DSMFactory.getInstance().getAggregationManager().addAggMenu(domainId, classNameSet);
                }

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除实体关系")
    @RequestMapping(value = {"delAggMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delAggMenu(String domainId, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (domainId != null) {
            try {
                String[] esdClassNames = StringUtility.split(sourceClassName, ";");
                Set classNameSet = new HashSet();
                for (String esdClassName : esdClassNames) {
                    if (esdClassName.indexOf(":") > -1) {
                        String className = StringUtility.split(esdClassName, ":")[0];
                        classNameSet.add(className);
                    } else {
                        classNameSet.add(esdClassName);
                    }
                }
                DSMFactory.getInstance().getAggregationManager().delAggMenu(domainId, classNameSet);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @RequestMapping(method = RequestMethod.POST, value = "AggConfigTree")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "900", height = "680")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "菜单配置")
    @ResponseBody
    public TreeListResultModel<List<AggMenuConfigTree>> getAggConfigTree(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggMenuConfigTree>> resultModel = new TreeListResultModel<List<AggMenuConfigTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(AggMenuMainNavItem.values()), AggMenuConfigTree.class);
        return resultModel;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
