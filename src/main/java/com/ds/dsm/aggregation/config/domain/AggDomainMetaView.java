package com.ds.dsm.aggregation.config.domain;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.aggregation.api.method.APIMethodBaseGridView;
import com.ds.dsm.aggregation.config.menu.AggMenuConfigView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/agg/domain/ref/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "领域信息", imageClass = "spafont spa-icon-c-databinder")
public class AggDomainMetaView {
    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @RequestMapping(method = RequestMethod.POST, value = "AggDomainConfig")
    @CustomAnnotation( index = 0)
    @ModuleAnnotation(caption = "视图", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<AggDomainConfigView>> getAggDomainConfig(String sourceClassName, String domainId) {
        ListResultModel<List<AggDomainConfigView>> result = new ListResultModel<List<AggDomainConfigView>>();
        try {
            AggEntityConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName,  domainId);
            List<FieldModuleConfig> attMethods = tableConfig.getModuleMethods();
            result = PageUtil.getDefaultPageList(attMethods, AggDomainConfigView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AttDomainMethod")
    @CustomAnnotation( index = 1)
    @ModuleAnnotation(caption = "菜单", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<APIMethodBaseGridView>> getAttDomainMethod(String sourceClassName, String domainId, String viewInstId) {
        ListResultModel<List<APIMethodBaseGridView>> result = new ListResultModel<List<APIMethodBaseGridView>>();
        try {
            AggEntityConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName,domainId);
            List<MethodConfig> attMethods = new ArrayList<>();
            for (MethodConfig apiBean : tableConfig.getAllMethods()) {
                if (!apiBean.isModule()) {
                    attMethods.add(apiBean);
                }
            }
            result = PageUtil.getDefaultPageList(attMethods, APIMethodBaseGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
