package com.ds.dsm.aggregation.config.menu.tree;

import com.ds.common.JDSException;
import com.ds.common.util.ClassUtility;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.config.menu.AggMenuConfigView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.AggregationManager;
import com.ds.esd.editor.extmenu.MenuBarBean;
import com.ds.esd.editor.extmenu.PluginsFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/agg/menu/config/method/group/")
public class AggMenuConfigNavService {


    @MethodChinaName(cname = "菜单配置")
    @RequestMapping(method = RequestMethod.POST, value = "AggMenuConfigView")
    @FormViewAnnotation

    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<AggMenuConfigView> getAggMenuConfigView(String sourceClassName, String domainId) {
        ResultModel<AggMenuConfigView> result = new ResultModel<AggMenuConfigView>();
        try {
            MenuBarBean menuBarBean = PluginsFactory.getInstance().checkMenuBar(ClassUtility.loadClass(sourceClassName));
            result.setData(new AggMenuConfigView(menuBarBean));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }

    @MethodChinaName(cname = "保存控制接口")
    @RequestMapping(value = {"saveWebMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveAggMenu(@RequestBody MenuBarBean menuBarBean) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            AggregationManager aggregationManager = DSMFactory.getInstance().getAggregationManager();
            AggEntityConfig aggEntityConfig = aggregationManager.getAggEntityConfig(menuBarBean.getSourceClassName(), menuBarBean.getDomainId());
            aggEntityConfig.setMenuBarBean(menuBarBean);
            DSMFactory.getInstance().getAggregationManager().updateAggEntityConfig(aggEntityConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;

    }


}
