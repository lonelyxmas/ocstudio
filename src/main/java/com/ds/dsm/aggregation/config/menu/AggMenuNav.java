package com.ds.dsm.aggregation.config.menu;

import com.ds.common.JDSException;
import com.ds.common.util.ClassUtility;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.api.APIConfigBaseView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.editor.extmenu.MenuBarBean;
import com.ds.esd.editor.extmenu.PluginsFactory;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@BottomBarMenu
@NavGroupAnnotation(customService = AggMenuService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
@RequestMapping("/dsm/agg/menu/config/")
public class AggMenuNav {

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;
    @CustomAnnotation(hidden = true, uid = true)
    public String sourceClassName;

    public AggMenuNav() {

    }


    @MethodChinaName(cname = "菜单配置")
    @RequestMapping(method = RequestMethod.POST, value = "AggMenuConfigView")
    @FormViewAnnotation
    @ModuleAnnotation(dock = Dock.top)
    @CustomAnnotation(index = 1)
    @UIAnnotation( height = "300")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<AggMenuConfigView> getAggMenuConfigView(String sourceClassName, String domainId) {
        ResultModel<AggMenuConfigView> result = new ResultModel<AggMenuConfigView>();
        try {
            MenuBarBean menuBarBean = PluginsFactory.getInstance().checkMenuBar(ClassUtility.loadClass(sourceClassName));
            result.setData(new AggMenuConfigView(menuBarBean));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }

    @MethodChinaName(cname = "WEB配置")
    @RequestMapping(method = RequestMethod.POST, value = "ConfigBaseInfo")
    @FormViewAnnotation
    @CustomAnnotation(index = 2)
    @ModuleAnnotation(dock = Dock.fill)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<APIConfigBaseView> getConfigBaseInfo(String domainId, String sourceClassName) {
        ResultModel<APIConfigBaseView> result = new ResultModel<APIConfigBaseView>();
        AggEntityConfig tableConfig = null;
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        result.setData(new APIConfigBaseView(tableConfig));
        return result;

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}

