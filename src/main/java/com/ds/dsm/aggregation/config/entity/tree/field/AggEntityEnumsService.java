package com.ds.dsm.aggregation.config.entity.tree.field;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityConfigTree;
import com.ds.dsm.aggregation.field.FieldAggView;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.FieldAggConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/dsm/agg/field/entity/config/")
public class AggEntityEnumsService {


    @RequestMapping(method = RequestMethod.POST, value = "AggFieldEnumsList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "字段")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor, autoRun = true)
    @ResponseBody
    public ListResultModel<List<FieldAggView>> getAggFieldEnumsList(String sourceClassName, String domainId) {
        ListResultModel<List<FieldAggView>> cols = new ListResultModel();
        try {
            AggEntityConfig aggEntityConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
            List<FieldAggConfig> fields = aggEntityConfig.getEnumsFieldList();
            cols = PageUtil.getDefaultPageList(fields, FieldAggView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadFieldList")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggEntityConfigTree>> loadFieldList(String sourceClassName, String domainId) {
        TreeListResultModel<List<AggEntityConfigTree>> result = new TreeListResultModel<>();
        try {

            AggEntityConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName,  domainId);
            AggEntityConfig entityConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(customESDClassAPIBean.getESDClass().getEntityClass().getClassName(), domainId);
            List<FieldAggConfig> fields = entityConfig.getEnumsFieldList();
            result = TreePageUtil.getDefaultTreeList(fields, AggEntityConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


}
