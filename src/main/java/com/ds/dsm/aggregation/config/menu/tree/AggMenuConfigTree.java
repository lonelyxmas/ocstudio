package com.ds.dsm.aggregation.config.menu.tree;

import com.ds.bpm.client.RouteToType;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavService;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityViewService;
import com.ds.dsm.aggregation.config.entity.tree.EntityMethodService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.CustomAPICallBean;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;


@TabsAnnotation(singleOpen = true)
@TreeAnnotation(lazyLoad = true,
        dynDestory = true,
        customService = AggMenuNavService.class)
public class AggMenuConfigTree extends TreeListItem {


    @Pid
    String domainId;
    @Pid
    String sourceClassName;
    @Pid
    String viewClassName;
    @Pid
    String methodName;
    @Pid
    String fieldname;

    @Pid
    CustomMenuType menuType;


    @TreeItemAnnotation(customItems = AggMenuMainNavItem.class)
    public AggMenuConfigTree(AggMenuMainNavItem menuItem, String sourceClassName, String domainId) {
        this.caption = menuItem.getName();
        this.bindClassName = menuItem.getBindClass().getName();
        this.dynLoad = menuItem.isDynLoad();
        this.dynDestory = menuItem.isDynDestory();
        this.iniFold = menuItem.isIniFold();
        this.imageClass = menuItem.getImageClass();
        this.id = menuItem.getType() + "_" + sourceClassName;
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;

    }


    @TreeItemAnnotation(customItems = AggMenuNavItem.class)
    public AggMenuConfigTree(AggMenuNavItem menuItem, String sourceClassName, String domainId) {
        this.caption = menuItem.getName();
        this.bindClassName = menuItem.getBindClass().getName();
        this.dynLoad = menuItem.isDynLoad();
        this.dynDestory = menuItem.isDynDestory();
        this.iniFold = menuItem.isIniFold();
        this.imageClass = menuItem.getImageClass();
        this.id = menuItem.getType() + "_" + sourceClassName;
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;

    }

    @TreeItemAnnotation(bindService = EntityMethodService.class, iniFold = true, dynDestory = true, lazyLoad = true)
    public AggMenuConfigTree(MethodConfig methodAPIBean) {
        CustomMenuItem customMenuItem = methodAPIBean.getDefaultMenuItem();
        String methodName = methodAPIBean.getMethodName();
        if (methodAPIBean.getCaption() != null && !methodAPIBean.getCaption().equals("")) {
            this.caption = "WebAPI(" + methodAPIBean.getCaption() + ")";
        } else {
            if (customMenuItem != null) {
                this.caption = "WebAPI(" + methodAPIBean.getName() + ")-" + methodAPIBean.getModuleViewType().getName();
            } else {
                this.caption = "WebAPI(" + methodAPIBean.getName() + ")";
            }
        }
        this.methodName = methodName;
        this.sourceClassName = methodAPIBean.getSourceClassName();
        this.viewClassName = methodAPIBean.getViewClassName();
        this.id = methodName;
        this.domainId = methodAPIBean.getDomainId();
        this.imageClass = methodAPIBean.getImageClass();
        this.tips = methodAPIBean.getCaption() + "<br/>";
        this.tips = tips + methodAPIBean.getMetaInfo();
    }

    @TreeItemAnnotation(bindService = AggEntityViewService.class, iniFold = true, dynDestory = true, lazyLoad = true)
    public AggMenuConfigTree(CustomViewBean customView) {
        this.methodName = customView.getMethodName();
        if (customView.getCaption() != null && !customView.getCaption().equals("")) {
            this.caption = customView.getCaption() + "(" + customView.getName() + ")";
        } else {
            this.caption = methodName + "(" + customView.getName() + ")-" + customView.getModuleViewType().getName();
        }
        this.sourceClassName = customView.getSourceClassName();
        this.viewClassName = customView.getViewClassName();
        this.id = customView.getMethodName();
        this.domainId = customView.getDomainId();
        this.imageClass = customView.getImageClass();
        this.tips = customView.getCaption() + "(" + customView.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + customView.getName() + ")";


    }


    @TreeItemAnnotation(bindService = AggEntityViewService.class, iniFold = true, dynDestory = true, lazyLoad = true)
    public AggMenuConfigTree(RouteToTypeViewBean routeToTypeBean, String sourceClassName, String domainId) {
        RouteToType routeToType = routeToTypeBean.getRouteToType();
        this.methodName = routeToTypeBean.getMethodName();
        this.caption = routeToType.getName();
        this.tips = routeToType.getType() + "(" + routeToType.getName() + ")<br/>";
        this.imageClass = routeToType.getImageClass();
        this.id = methodName + "_" + routeToType.getType();
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;

    }

    @TreeItemAnnotation(bindService = EntityMethodService.class, iniFold = true, dynDestory = true, lazyLoad = true)
    public AggMenuConfigTree(MenuItemViewBean menuItemBean, String sourceClassName, String methodName, String domainId) {
        CustomMenuItem menuItem = menuItemBean.getCustomMenuItem();
        this.methodName = menuItemBean.getMethodName();
        this.caption = menuItem.getName();
        this.tips = menuItem.getType() + "(" + menuItem.getName() + ")<br/>";
        this.imageClass = menuItem.getImageClass();
        this.id = methodName + "_" + menuItem.getType();
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;
    }


    @TreeItemAnnotation(bindService = EntityMethodService.class, iniFold = true, dynDestory = true, lazyLoad = true)
    public AggMenuConfigTree(RouteToTypeBean routeToTypeBean, String sourceClassName, String domainId) {
        RouteToType routeToType = routeToTypeBean.getRouteToType();
        this.methodName = routeToTypeBean.getMethodName();
        this.caption = routeToType.getName();
        this.tips = routeToType.getType() + "(" + routeToType.getName() + ")<br/>";
        this.imageClass = routeToType.getImageClass();
        this.id = methodName + "_" + routeToType.getType();
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;

    }

    @TreeItemAnnotation(bindService = AggEntityViewService.class, iniFold = true, dynDestory = true, lazyLoad = true)
    public AggMenuConfigTree(MenuItemBean menuItemBean, String sourceClassName, String methodName, String domainId) {
        CustomMenuItem menuItem = menuItemBean.getCustomMenuItem();
        this.methodName = menuItemBean.getMethodName();
        this.caption = menuItem.getName();
        this.tips = menuItem.getType() + "(" + menuItem.getName() + ")<br/>";
        this.imageClass = menuItem.getImageClass();
        this.id = methodName + "_" + menuItem.getType();
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;
    }


    @TreeItemAnnotation(bindService = AggEntityNavService.class)
    public AggMenuConfigTree(CustomAPICallBean customAPICallBean) {
        this.methodName = customAPICallBean.getApiCallerProperties().getMethodName();
        this.caption = methodName + "(" + customAPICallBean.getApiCallerProperties().getDesc() + ")";
        this.tips = caption + "<br/>";
        this.tips = tips + methodName + "(" + customAPICallBean.getApiCallerProperties().getDesc() + ")";
        this.imageClass = customAPICallBean.getApiCallerProperties().getImageClass();
        this.id = methodName + "_" + customAPICallBean.getApiCallerProperties().getId();
        this.sourceClassName = customAPICallBean.getApiCallerProperties().getSourceClassName();
        this.domainId = customAPICallBean.getApiCallerProperties().getDomainId();


    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public CustomMenuType getMenuType() {
        return menuType;
    }

    public void setMenuType(CustomMenuType menuType) {
        this.menuType = menuType;
    }
}


