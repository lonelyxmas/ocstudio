package com.ds.dsm.aggregation.config.menu;

import com.ds.dsm.aggregation.config.menu.tree.AggMenuConfigNavService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.editor.extmenu.MenuBarBean;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.tool.ui.enums.VAlignType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@BottomBarMenu
@FormAnnotation(col = 4, customService = AggMenuConfigNavService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class AggMenuConfigView {

    @Pid
    String domainId;
    @Pid
    String parentId;
    @Uid
    String id;
    @CustomAnnotation(hidden = true)
    Integer index;

    @RadioBoxAnnotation(checkBox = true)
    @ListAnnotation()
    @FieldAnnotation(colSpan = 4, rowHeight = "100")
    @CustomAnnotation(caption = "菜单类型")
    CustomMenuType menuType;
    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation(caption = "显示名称")
    String caption;
    @FieldAnnotation(colSpan = 2)
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;
    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation(caption = "垂直对齐")
    HAlignType hAlign;
    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation(caption = "横向对齐")
    VAlignType vAlign;

    @CustomAnnotation(caption = "显示标题")
    Boolean showCaption = true;
    @CustomAnnotation(caption = "行头手柄")
    Boolean handler;

    @CustomAnnotation(caption = "动态加载")
    Boolean dynLoad = false;


    @CustomAnnotation(caption = "延迟加载")
    Boolean lazy;


    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "实现类")
    Class serviceClass;

    public AggMenuConfigView() {

    }


    public AggMenuConfigView(MenuBarBean menuBarBean) {
        this.menuType = menuBarBean.getMenuType();
        this.domainId = menuBarBean.getDomainId();
        this.serviceClass = menuBarBean.getServiceClass();
        if (serviceClass == null && menuBarBean.getMenuClasses().length > 0) {
            serviceClass = menuBarBean.getMenuClasses()[0];
        }

        this.caption = menuBarBean.getCaption();
        this.parentId = menuBarBean.getParentId();
        this.id = menuBarBean.getId();
        this.index = menuBarBean.getIndex();
        this.dynLoad = menuBarBean.getDynLoad();
        this.showCaption = menuBarBean.getShowCaption();
        this.imageClass = menuBarBean.getImageClass();
        this.lazy = menuBarBean.getLazy();
        this.hAlign = menuBarBean.gethAlign();
        this.vAlign = menuBarBean.getvAlign();
        this.handler = menuBarBean.getHandler();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public CustomMenuType getMenuType() {
        return menuType;
    }

    public void setMenuType(CustomMenuType menuType) {
        this.menuType = menuType;
    }

    public Class getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(Class serviceClass) {
        this.serviceClass = serviceClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public Boolean getShowCaption() {
        return showCaption;
    }

    public void setShowCaption(Boolean showCaption) {
        this.showCaption = showCaption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public Boolean getLazy() {
        return lazy;
    }

    public void setLazy(Boolean lazy) {
        this.lazy = lazy;
    }

    public HAlignType gethAlign() {
        return hAlign;
    }

    public void sethAlign(HAlignType hAlign) {
        this.hAlign = hAlign;
    }

    public VAlignType getvAlign() {
        return vAlign;
    }

    public void setvAlign(VAlignType vAlign) {
        this.vAlign = vAlign;
    }

    public Boolean getHandler() {
        return handler;
    }

    public void setHandler(Boolean handler) {
        this.handler = handler;
    }

}
