package com.ds.dsm.aggregation.config.domain.pop;

import com.ds.common.JDSException;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.domain.enums.*;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = AggDomainTreeService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class AggDomainTree extends TreeListItem {


    @Pid
    String domainId;

    @Pid
    String esdClassName;

    @Pid
    CustomDomainType customDomainType;

    @Pid
    String itemDomainType;


    @TreeItemAnnotation(caption = "通用域", bindService = AggDomainTreeService.class)
    public AggDomainTree(CustomDomainType customDomainType, String domainId) {
        this.caption = customDomainType.getName();
        this.imageClass = customDomainType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + customDomainType.getType();
        this.domainId = domainId;
        this.customDomainType = customDomainType;
    }

    @TreeItemAnnotation()
    public AggDomainTree(NavDomainType itemDomainType, CustomDomainType customDomainType, String domainId) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.domainId = domainId;
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType.getType();
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = AggDomainTreeTypeService.class)
    public AggDomainTree(BpmDomainType itemDomainType, CustomDomainType customDomainType, String domainId) {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.domainId = domainId;
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType.getType();
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = AggDomainTreeTypeService.class)
    public AggDomainTree(OrgDomainType itemDomainType, CustomDomainType customDomainType, String domainId) throws JDSException {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.domainId = domainId;
        this.itemDomainType = itemDomainType.getType();
        this.customDomainType = customDomainType;

    }

    @TreeItemAnnotation(bindService = AggDomainTreeTypeService.class)
    public AggDomainTree(MsgDomainType itemDomainType, CustomDomainType customDomainType, String domainId) throws JDSException {
        this.caption = itemDomainType.getName();
        this.imageClass = itemDomainType.getImageClass();
        this.domainId = domainId;
        this.id = customDomainType.getType() + "_" + itemDomainType.getType();
        this.itemDomainType = itemDomainType.getType();
        this.customDomainType = customDomainType;

    }


    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-grid")
    public AggDomainTree(ESDClass esdClass, String domainId) {
        this.caption = esdClass.getName() + "(" + esdClass.getDesc() + ")";
        this.esdClassName = esdClass.getClassName();
        this.id = esdClass.getClassName();
        this.domainId = domainId;

    }

    public String getEsdClassName() {
        return esdClassName;
    }

    public void setEsdClassName(String esdClassName) {
        this.esdClassName = esdClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public CustomDomainType getCustomDomainType() {
        return customDomainType;
    }

    public void setCustomDomainType(CustomDomainType customDomainType) {
        this.customDomainType = customDomainType;
    }

    public String getItemDomainType() {
        return itemDomainType;
    }

    public void setItemDomainType(String itemDomainType) {
        this.itemDomainType = itemDomainType;
    }
}
