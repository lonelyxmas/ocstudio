package com.ds.dsm.aggregation.config.entity.tree;

import com.ds.dsm.aggregation.module.ModuleConfigService;
import com.ds.dsm.aggregation.module.ModuleDeisignerService;
import com.ds.dsm.aggregation.module.panel.ModuleWinService;
import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.esd.tool.ui.enums.CustomImageType;

public enum AggEntityMethodNavItem implements TreeItem {
    ViewConfig("控制调用", "spafont spa-icon-c-webapi", EntityMethodService.class, true, true, false),
    ModuleConfig("动作事件", CustomImageType.action1.getImageClass(), ModuleConfigService.class, true, true, false),
    WinConfig("模块配置", "spafont spa-icon-rendermode", ModuleWinService.class, true, true, false),
    DesignerConfig("画布配置", CustomImageType.designview.getImageClass(), ModuleDeisignerService.class, true, true, false);
    private final String name;

    private final Class bindClass;
    private final String imageClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    AggEntityMethodNavItem(String name, String imageClass, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
