package com.ds.dsm.aggregation.config.api;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.api.APIConfigBaseView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@NavGroupAnnotation(customService = AggAPIService.class)
@RequestMapping("/dsm/agg/api/config/")
public class AggAPINav {

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;
    @CustomAnnotation(hidden = true, uid = true)
    public String sourceClassName;

    public AggAPINav() {

    }


    @MethodChinaName(cname = "基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "ConfigBaseInfo")
    @FormViewAnnotation
    @ModuleAnnotation( dock = Dock.top)
    @UIAnnotation( height = "180")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<APIConfigBaseView> getConfigBaseInfo(String domainId, String sourceClassName) {
        ResultModel<APIConfigBaseView> result = new ResultModel<APIConfigBaseView>();
        AggEntityConfig tableConfig = null;
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        result.setData(new APIConfigBaseView(tableConfig));
        return result;

    }


    @MethodChinaName(cname = "详细信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggAPIMetaView")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation( dock = Dock.fill)
    @ResponseBody
    public ResultModel<AggAPIMetaView> getAggViewMetaView(String sourceClassName, String domainId) {
        ResultModel<AggAPIMetaView> result = new ResultModel<AggAPIMetaView>();
        return result;

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}

