package com.ds.dsm.aggregation.config.domain.pop;

import com.ds.config.TreeListResultModel;
import com.ds.enums.IconEnumstype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.domain.enums.*;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/domain/config/")
@MethodChinaName(cname = "领域管理", imageClass = "spafont spa-icon-c-gallery")

public class AggDomainTreeService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @RequestMapping(method = RequestMethod.POST, value = "loadChildDomain")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggDomainTree>> loadChildDomain(CustomDomainType customDomainType, String domainId) {
        TreeListResultModel<List<AggDomainTree>> result = new TreeListResultModel<List<AggDomainTree>>();
        List<IconEnumstype> iconEnumstypes = new ArrayList<>();
        switch (customDomainType) {
            case bpm:
                BpmDomainType[] types = BpmDomainType.values();
                iconEnumstypes.addAll(Arrays.asList(types));
                break;
            case msg:
                MsgDomainType[] msgtypes = MsgDomainType.values();
                iconEnumstypes.addAll(Arrays.asList(msgtypes));
                break;
            case org:
                OrgDomainType[] orgtypes = OrgDomainType.values();
                iconEnumstypes.addAll(Arrays.asList(orgtypes));
                break;
            case nav:
                NavDomainType[] navtypes = NavDomainType.values();
                iconEnumstypes.addAll(Arrays.asList(navtypes));
                break;
        }

        result = TreePageUtil.getTreeList(iconEnumstypes, AggDomainTree.class);
        return result;

    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
