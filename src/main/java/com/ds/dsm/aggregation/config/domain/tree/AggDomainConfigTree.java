package com.ds.dsm.aggregation.config.domain.tree;

import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;


@TabsAnnotation(singleOpen = true)
@TreeAnnotation(lazyLoad = true,
        dynDestory = true,
        customService = AggDomainNavService.class)
public class AggDomainConfigTree extends TreeListItem {

    @Pid
    String domainId;
    @Pid
    String sourceClassName;
    @Pid
    String methodName;
    @Pid
    String entityClassName;


    @TreeItemAnnotation(customItems = AggEntityNavItem.class, lazyLoad = true, dynDestory = true)
    public AggDomainConfigTree(AggEntityNavItem aggEntityNavItem, String sourceClassName, String domainId) {
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;
        this.id = aggEntityNavItem.name();
        this.caption = aggEntityNavItem.getName();
        this.imageClass = aggEntityNavItem.getImageClass();
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }
}


