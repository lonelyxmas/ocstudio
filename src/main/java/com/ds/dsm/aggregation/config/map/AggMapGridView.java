package com.ds.dsm.aggregation.config.map;


import com.ds.dsm.aggregation.config.root.AggRootService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.web.annotation.Required;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {AggRootService.class}, event = CustomGridEvent.editor)
public class AggMapGridView {


    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(uid = true, hidden = true)
    public String sourceClassName;

    @Required
    @CustomAnnotation(caption = "名称")
    public String name;
    @Required
    @CustomAnnotation(caption = "包名")
    public String packageName;


    @CustomAnnotation(caption = "描述", hidden = true)
    private String desc;

    public AggMapGridView(ESDClass esdClass) {
        this.name = esdClass.getName();
        this.domainId = esdClass.getDomainId();
        this.sourceClassName = esdClass.getCtClass().getName();
        this.packageName = esdClass.getCtClass().getPackage().getName();
        this.desc = esdClass.getDesc();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
