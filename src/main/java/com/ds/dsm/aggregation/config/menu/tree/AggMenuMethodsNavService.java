package com.ds.dsm.aggregation.config.menu.tree;

import com.ds.bpm.client.RouteToType;
import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.menu.AggMenuItemGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.CustomAPICallBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.bpm.RouteCustomMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.AnnotationUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/agg/menu/config/")
public class AggMenuMethodsNavService {


    @MethodChinaName(cname = "菜单项")
    @RequestMapping(method = RequestMethod.POST, value = "MenuItemList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-buttonview", caption = "菜单项")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<AggMenuItemGridView>> getAggMenuItemList(String sourceClassName, String menuClass, String domainId) {
        ListResultModel<List<AggMenuItemGridView>> result = new ListResultModel();

        try {
            if (menuClass != null) {
                sourceClassName = menuClass;
            }
            AggEntityConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
            List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
            List<Object> iconEnumstypes = new ArrayList<>();
            for (MethodConfig methodAPIBean : customMethods) {
                if (!methodAPIBean.getCustomMethodInfo().isSplit() && !methodAPIBean.getField()) {
                    CustomAPICallBean customAPICallBean = methodAPIBean.getApi();
                    RouteCustomMenu customMenu = AnnotationUtil.getMethodAnnotation(methodAPIBean.getMethod(), RouteCustomMenu.class);
                    if (customMenu != null && customMenu.routeType() != null && customMenu.routeType().length > 0) {
                        RouteToType[] bindMenus = customMenu.routeType();
                        for (RouteToType bindMenu : bindMenus) {
                            iconEnumstypes.add(bindMenu);
                        }
                    } else if (customAPICallBean != null) {
                        if (customAPICallBean.getBindMenu().size() > 0) {
                            for (CustomMenuItem bindMenu : customAPICallBean.getBindMenu()) {
                                iconEnumstypes.add(bindMenu);
                            }

                        }
                    } else {
                        AggMenuConfigTree treeListItem = new AggMenuConfigTree(customAPICallBean);
                        iconEnumstypes.add(treeListItem);
                    }
                }
            }
            result = PageUtil.getDefaultPageList(iconEnumstypes, AggMenuItemGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadAllMethod")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggMenuConfigTree>> loadAllMethod(String domainId, String sourceClassName) {
        TreeListResultModel<List<AggMenuConfigTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(AggMenuNavItem.values()), AggMenuConfigTree.class);
        return result;
    }

}
