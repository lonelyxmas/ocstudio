package com.ds.dsm.aggregation.config.menu.pop;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.editor.extmenu.MenuBarBean;
import com.ds.esd.editor.extmenu.PluginsFactory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/menu/config/")

public class AggMenuTreeService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "loadChildMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public TreeListResultModel<List<AggMenuTree>> loadChildMenu(CustomMenuType menuType, String domainId, String soruceClassName, String methodName) {
        TreeListResultModel<List<AggMenuTree>> result = new TreeListResultModel<List<AggMenuTree>>();
        try {
            List<ESDClass> esdServiceBeans = new ArrayList<>();
            List<ESDClass> aggServiceBeans = DSMFactory.getInstance().getClassManager().getAllAggMenu();
            for (ESDClass esdClass : aggServiceBeans) {
                if (esdClass.getRootClass().getClassName().equals(esdClass.getClassName())) {
                    MenuBarBean menuBarBean = PluginsFactory.getInstance().checkMenuBar(esdClass.getCtClass());
                    if (menuBarBean != null && menuBarBean.getMenuType().equals(menuType)) {
                        esdServiceBeans.add(esdClass);
                    }
                }
            }
            result = TreePageUtil.getTreeList(esdServiceBeans, AggMenuTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return result;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
