package com.ds.dsm.aggregation.config.menu.tree;

import com.ds.bpm.client.RouteToType;
import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.menu.AggMenuNav;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.CustomAPICallBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.bpm.RouteMenuBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/dsm/agg/menu/config/method/")
public class AggMenuMethodNavService {


    @MethodChinaName(cname = "聚合菜单信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggMenuInfo")
    @NavGroupViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, caption = "装载", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggMenuNav> getAggMenuInfo(String domainId, String sourceClassName) {
        ResultModel<AggMenuNav> result = new ResultModel<AggMenuNav>();

        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadMenuMethod")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggMenuConfigTree>> loadMenuMethod(String sourceClassName, String domainId) {
        TreeListResultModel<List<AggMenuConfigTree>> result = new TreeListResultModel<>();
        try {
            AggEntityConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
            List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
            List<Object> iconEnumstypes = new ArrayList<>();
            for (MethodConfig methodAPIBean : customMethods) {
                if (!methodAPIBean.getCustomMethodInfo().isSplit() && !methodAPIBean.getField()) {
                    CustomAPICallBean customAPICallBean = methodAPIBean.getApi();

                    RouteMenuBean menuBean = methodAPIBean.getRouteMenuBean();
                    if (menuBean != null && menuBean.getRouteType() != null && menuBean.getRouteType().size() > 0) {
                        Set<RouteToType> bindMenus = menuBean.getRouteType();
                        for (RouteToType bindMenu : bindMenus) {
                            if (methodAPIBean.isModule()) {
                                iconEnumstypes.add(new RouteToTypeViewBean(bindMenu, methodAPIBean.getMethodName()));
                            } else {
                                iconEnumstypes.add(new RouteToTypeBean(bindMenu, methodAPIBean.getMethodName()));
                            }
                        }
                    } else if (customAPICallBean != null) {
                        if (customAPICallBean.getBindMenu().size() > 0) {
                            for (CustomMenuItem bindMenu : customAPICallBean.getBindMenu()) {
                                iconEnumstypes.add(new MenuItemBean(bindMenu, methodAPIBean.getMethodName()));
                            }

                        } else {
                            iconEnumstypes.add(customAPICallBean);
                        }
                    }
                }
            }

            result = TreePageUtil.getDefaultTreeList(iconEnumstypes, AggMenuConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


}
