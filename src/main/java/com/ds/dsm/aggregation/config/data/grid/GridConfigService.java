package com.ds.dsm.aggregation.config.data.grid;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.grid.GridDataView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.grid.CustomGridDataBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.view.ViewEntityConfig;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/entity/config/grid/")
public class GridConfigService {


    @MethodChinaName(cname = "编辑数据信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateGridData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateGridData(@RequestBody GridDataView dataView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(dataView.getSourceClassName(), dataView.getViewInstId());
            MethodConfig methodAPIBean = classConfig.getMethodByName(dataView.getMethodName());
            CustomGridDataBean customGridDataBean = (CustomGridDataBean) methodAPIBean.getDataBean();
            BeanMap.create(customGridDataBean).putAll(BeanMap.create(dataView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "清空列表配置")
    @RequestMapping(method = RequestMethod.POST, value = "clearData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formReSet)
    public @ResponseBody
    ResultModel<Boolean> clearData(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            methodAPIBean.setView(null);

            DSMFactory.getInstance().getViewManager().updateViewEntityConfig(classConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

}
