package com.ds.dsm.aggregation.config.menu.tree;

import com.ds.bpm.client.RouteToType;
import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.info.AggEntityNav;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.CustomAPICallBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.bpm.RouteCustomMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/agg/menu/config/view/")
public class AggMenuViewNavService {


    @MethodChinaName(cname = "聚合实体信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggMenuViewInfo")
    @NavGroupViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, caption = "装载", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggEntityNav> getAggEntityInfo(String domainId, String sourceClassName) {
        ResultModel<AggEntityNav> result = new ResultModel<AggEntityNav>();

        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadMenuView")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggMenuConfigTree>> loadView(String sourceClassName, String domainId) {
        TreeListResultModel<List<AggMenuConfigTree>> result = new TreeListResultModel<>();
        try {
            AggEntityConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName,  domainId);
            List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
            List<Object> iconEnumstypes = new ArrayList<>();

            for (MethodConfig methodAPIBean : customMethods) {
                if (methodAPIBean.isModule()) {
                    CustomAPICallBean customAPICallBean = methodAPIBean.getApi();
                    RouteCustomMenu customMenu = AnnotationUtil.getMethodAnnotation(methodAPIBean.getMethod(), RouteCustomMenu.class);
                    if (customMenu != null && customMenu.routeType() != null && customMenu.routeType().length > 0) {
                        RouteToType[] bindMenus = customMenu.routeType();
                        for (RouteToType bindMenu : bindMenus) {
                            iconEnumstypes.add(new RouteToTypeViewBean(bindMenu, methodAPIBean.getMethodName()));
                        }
                    } else if (customAPICallBean != null) {
                        if (customAPICallBean.getBindMenu().size() > 0) {
                            for (CustomMenuItem bindMenu : customAPICallBean.getBindMenu()) {
                                iconEnumstypes.add(new MenuItemViewBean(bindMenu, methodAPIBean.getMethodName()));
                            }
                        } else {
                            iconEnumstypes.add(customAPICallBean);
                        }
                    } else if (methodAPIBean.isModule()) {
                        iconEnumstypes.add(methodAPIBean.getView());
                    }
                }
            }

            result = TreePageUtil.getDefaultTreeList(iconEnumstypes, AggMenuConfigTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


}
