package com.ds.dsm.aggregation.config.data.grid;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.grid.CustomGridDataBean;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.StretchType;

@FormAnnotation(stretchHeight = StretchType.last, col = 2, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class GridDataView {

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, uid = true)
    String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;
    @FieldAnnotation(  colSpan = -1)
    @CustomAnnotation(caption = "数据源（URL）")
    String dataUrl;
    @FieldAnnotation( rowHeight = "50")
    @CustomAnnotation(caption = "新增操作（URL）")
    String addPath;
    @FieldAnnotation( rowHeight = "50")
    @CustomAnnotation(caption = "编辑操作（URL）")
    String editorPath;
    @FieldAnnotation( rowHeight = "50")
    @CustomAnnotation(caption = "排序操作（URL）")
    String sortPath;
    @FieldAnnotation( rowHeight = "50")
    @CustomAnnotation(caption = "删除操作（URL）")
    String delPath;
    @FieldAnnotation(  colSpan = -1, rowHeight = "150", componentType = ComponentType.CodeEditor)
    @CustomAnnotation(caption = "表达式")
    String expression;

    public GridDataView() {

    }

    public GridDataView(CustomGridDataBean dataBean) {

        this.viewInstId = dataBean.getViewInstId();
        this.domainId=dataBean.getDomainId();
        this.sourceClassName = dataBean.getSourceClassName();
        this.expression = dataBean.getExpression();
        this.dataUrl = dataBean.getDataUrl();
        this.addPath = dataBean.getAddPath();
        this.editorPath = dataBean.getEditorPath();
        this.sortPath = dataBean.getSortPath();
        this.delPath = dataBean.getDelPath();
        this.methodName = dataBean.getMethodName();


    }

    public GridDataView(MethodConfig methodAPICallBean) {

        this.methodName = methodAPICallBean.getMethodName();
        this.viewInstId = methodAPICallBean.getViewInstId();
        this.domainId=methodAPICallBean.getDomainId();
        this.dataUrl = methodAPICallBean.getUrl();
        this.sourceClassName = methodAPICallBean.getSourceClassName();
        CustomGridDataBean dataBean = (CustomGridDataBean) methodAPICallBean.getDataBean();
        if (dataBean != null) {
            this.expression = dataBean.getExpression();
            this.addPath = dataBean.getAddPath();
            this.editorPath = dataBean.getEditorPath();
            this.sortPath = dataBean.getSortPath();
            this.delPath = dataBean.getDelPath();
            this.methodName = dataBean.getMethodName();
        }


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getDataUrl() {
        return dataUrl;
    }

    public void setDataUrl(String dataUrl) {
        this.dataUrl = dataUrl;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getAddPath() {
        return addPath;
    }

    public void setAddPath(String addPath) {
        this.addPath = addPath;
    }


    public String getEditorPath() {
        return editorPath;
    }

    public void setEditorPath(String editorPath) {
        this.editorPath = editorPath;
    }

    public String getSortPath() {
        return sortPath;
    }

    public void setSortPath(String sortPath) {
        this.sortPath = sortPath;
    }

    public String getDelPath() {
        return delPath;
    }

    public void setDelPath(String delPath) {
        this.delPath = delPath;
    }


}
