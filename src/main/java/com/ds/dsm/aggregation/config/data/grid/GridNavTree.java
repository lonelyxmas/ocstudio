package com.ds.dsm.aggregation.config.data.grid;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class GridNavTree extends TreeListItem {


    public GridNavTree(String sourceClassName, MethodConfig customMethodAPIBean, String viewInstId) {
        this.caption = "动作事件";
        this.imageClass = "spafont spa-icon-action";
        String methodName = customMethodAPIBean.getMethodName();
        this.setEuClassName(" dsm.agg.config.grid.GridActionGroup");
        this.setId(sourceClassName + "_" + methodName);
        this.addTagVar("methodName", methodName);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("viewInstId", viewInstId);

    }
}
