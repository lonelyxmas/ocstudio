package com.ds.dsm.aggregation.config.domain;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.view.field.FieldModuleConfig;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload})
public class AggDomainConfigView {

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(caption = "视图名称")
    String viewName;

    @CustomAnnotation(caption = "视图类型")
    ModuleViewType viewType;

    @CustomAnnotation(caption = "方法名称", uid = true)
    String methodName;

    @FieldAnnotation(rowHeight = "150")
    @CustomAnnotation(caption = "类名", pid = true)
    String sourceClassName;

    @CustomAnnotation(caption = "访问地址")
    String url;

    public AggDomainConfigView() {

    }


    public AggDomainConfigView(FieldModuleConfig moduleConfig) {
        this.methodName = moduleConfig.getMethodName();

        this.domainId = moduleConfig.getDomainId();
        this.viewType = moduleConfig.getMethodConfig().getModuleViewType();
        this.url = moduleConfig.getUrl();
        this.viewName = moduleConfig.getCaption();
        this.sourceClassName = moduleConfig.getSourceClassName();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public ModuleViewType getViewType() {
        return viewType;
    }

    public void setViewType(ModuleViewType viewType) {
        this.viewType = viewType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
