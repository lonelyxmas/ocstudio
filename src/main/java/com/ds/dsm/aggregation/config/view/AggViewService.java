package com.ds.dsm.aggregation.config.view;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavFoldingTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(path = "/dsm/agg/view/config/")
@MethodChinaName(cname = "视图管理", imageClass = "spafont spa-icon-c-gallery")

public class AggViewService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "聚合动作菜单")
    @RequestMapping(method = RequestMethod.POST, value = "AggViewList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-buttonview", caption = "聚合动作菜单")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public ListResultModel<List<AggViewGridView>> getAggViewList(String domainId) {
        ListResultModel<List<AggViewGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                for (String className : bean.getAggViewNames()) {
                    ESDClass esdClass = DSMFactory.getInstance().getClassManager().getAggEntityByName(className, domainId, true);
                    if (esdClass != null) {
                        esdClassList.add(esdClass);
                    }
                }
                result = PageUtil.getDefaultPageList(esdClassList, AggViewGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "视图信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggViewInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation( dock = Dock.fill, caption = "装载", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggViewNav> getAggViewInfo(String domainId, String sourceClassName) {
        ResultModel<AggViewNav> result = new ResultModel<AggViewNav>();

        return result;
    }

    @MethodChinaName(cname = "导入菜单动作")
    @RequestMapping(value = {"AggViewTree"}, method = {RequestMethod.GET, RequestMethod.POST})

    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "导入菜单动作", dynLoad = true,dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<AggViewTree>> getAggViewTree(String domainId) {
        TreeListResultModel<List<AggViewTree>> result = new TreeListResultModel<List<AggViewTree>>();
        try {
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            List<AggViewTree> treeViews = new ArrayList<>();
            treeViews.add(new AggViewTree(domainId));
            result.setData(treeViews);
            result.setIds(Arrays.asList(bean.getAggViewNames().toArray(new String[]{})));
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "保存控制接口")
    @RequestMapping(value = {"saveAggView"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveAggMenu(String domainId, String AggViewTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (AggViewTree != null) {
            try {
                DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                if (bean != null) {
                    String[] esdClassNames = StringUtility.split(AggViewTree, ";");
                    Set classNameSet = new HashSet();
                    for (String esdClassName : esdClassNames) {
                        if (esdClassName.indexOf(":") > -1) {
                            String className = StringUtility.split(esdClassName, ":")[0];
                            classNameSet.add(className);
                        } else {
                            classNameSet.add(esdClassName);
                        }
                    }
                    DSMFactory.getInstance().getAggregationManager().addAggView(domainId, classNameSet);
                }

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除实体关系")
    @RequestMapping(value = {"delAggView"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delAggView(String domainId, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (domainId != null) {
            try {
                String[] esdClassNames = StringUtility.split(sourceClassName, ";");
                Set classNameSet = new HashSet();
                for (String esdClassName : esdClassNames) {
                    if (esdClassName.indexOf(":") > -1) {
                        String className = StringUtility.split(esdClassName, ":")[0];
                        classNameSet.add(className);
                    } else {
                        classNameSet.add(esdClassName);
                    }
                }
                DSMFactory.getInstance().getAggregationManager().delAggView(domainId, classNameSet);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    @RequestMapping(method = RequestMethod.POST, value = "AggViewEditorView")
    @NavFoldingTreeViewAnnotation
    @DialogAnnotation(width = "900", height = "680" )
    @ModuleAnnotation( caption = "实体信息",cache = false)
    @DynLoadAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<AggViewEditorView> getAggViewEditorView(String domainId, String sourceClassName) {
        ResultModel<AggViewEditorView> result = new ResultModel<AggViewEditorView>();
        return result;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
