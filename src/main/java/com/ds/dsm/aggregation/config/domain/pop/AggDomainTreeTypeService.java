package com.ds.dsm.aggregation.config.domain.pop;

import com.ds.common.JDSConstants;
import com.ds.common.JDSException;
import com.ds.common.logging.Log;
import com.ds.common.logging.LogFactory;
import com.ds.config.TreeListResultModel;
import com.ds.enums.IconEnumstype;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/domain/config/dommaintype/")
public class AggDomainTreeTypeService {
    protected Log log = LogFactory.getLog(JDSConstants.CONFIG_KEY, AggDomainTreeTypeService.class);

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "loadChildDomainType")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public TreeListResultModel<List<AggDomainTree>> loadChildDomainType(CustomDomainType customDomainType, String itemDomainType, String domainId) {
        TreeListResultModel<List<AggDomainTree>> result = new TreeListResultModel<List<AggDomainTree>>();

        try {
            // DSMFactory.getInstance().reload();
            List<ESDClass> esdServiceBeans = new ArrayList<>();
            List<ESDClass> aggServiceBeans = DSMFactory.getInstance().getClassManager().getAllAggDomain();

            for (ESDClass esdClass : aggServiceBeans) {
                if (esdClass.getSourceClass().getClassName().equals(esdClass.getClassName())) {
                    CustomDomainType domainType = esdClass.getCustomDomain().getDomainType();
                    IconEnumstype beanDomainType = null;

                    switch (domainType) {
                        case bpm:
                            beanDomainType = esdClass.getCustomDomain().getBpmDomainType();
                            break;
                        case msg:
                            beanDomainType = esdClass.getCustomDomain().getMsgDomainType();
                            break;
                        case org:
                            beanDomainType = esdClass.getCustomDomain().getOrgDomainType();
                            break;
                        case nav:
                            beanDomainType = esdClass.getCustomDomain().getNavDomainType();
                            break;
                        case vfs:
                            beanDomainType = esdClass.getCustomDomain().getNavDomainType();
                            break;
                    }

                    if (domainType != null && beanDomainType != null && beanDomainType.getType().equals(itemDomainType)) {
                        esdServiceBeans.add(esdClass);
                    }

                }
            }
            result = TreePageUtil.getTreeList(esdServiceBeans, AggDomainTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;

    }

}
