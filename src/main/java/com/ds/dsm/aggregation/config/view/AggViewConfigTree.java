package com.ds.dsm.aggregation.config.view;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.tool.ui.component.list.TreeListItem;

import java.util.ArrayList;


@TabsAnnotation(singleOpen = true)
@TreeAnnotation(lazyLoad = true,
        dynDestory = true,
        customService = AggViewNavService.class)
public class AggViewConfigTree extends TreeListItem {

    public AggViewConfigTree(String sourceClassName, String domainId) throws JDSException {
        super("AggViewInfo", "聚合视图", "spafont spa-icon-c-webapi");
        this.setEuClassName("dsm.agg.view.config.AggViewInfo");
        this.addTagVar("domainId", domainId);
        this.setSub(new ArrayList());
    }


    public AggViewConfigTree(MethodConfig methodAPIBean) {
        ModuleViewType viewType = methodAPIBean.getModuleViewType();
        String methodName = methodAPIBean.getMethodName();
        CustomMenuItem customMenuItem = methodAPIBean.getDefaultMenuItem();
        if (methodAPIBean.getCaption() != null && !methodAPIBean.getCaption().equals("")) {
            this.caption = methodAPIBean.getCaption() + "(" + methodAPIBean.getName() + ")";
        } else {
            if (customMenuItem != null) {
                this.caption = methodName + "(" + methodAPIBean.getName() + ")-" + methodAPIBean.getModuleViewType().getName();
            } else {
                this.caption = methodName + "(" + viewType.getName() + ")-" + methodAPIBean.getModuleViewType().getName();
            }
        }
        this.tips = methodAPIBean.getCaption() + "(" + methodAPIBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.imageClass = viewType.getImageClass();
        this.setIniFold(false);
        this.setEuClassName(viewType.getClassName());
        this.setId(viewType.getType() + methodName);
        this.setGroup(true);
        this.addTagVar("sourceClassName", methodAPIBean.getSourceClassName());
        this.addTagVar("methodName", methodName);
        this.addTagVar("domainId", methodAPIBean.getDomainId());
        this.setSub(new ArrayList());


    }

}


