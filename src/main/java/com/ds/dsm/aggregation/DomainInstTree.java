package com.ds.dsm.aggregation;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.CustomDomainType;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.enums.RepositoryType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.ViewType;

import java.util.List;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, caption = "JAVA模板")
public class DomainInstTree extends TreeListItem {
    public DomainInstTree(String projectVersionName, String domainId, String viewInstId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = "模板分类";
        this.id = "ViewRoot";
        this.euClassName = "dsm.manager.temp.AllJavaTempGrid";
        this.iniFold = false;
        this.imageClass = "spafont spa-icon-settingprj";
        this.addTagVar("domainId", domainId);
        DSMType[] dsmTypes = DSMType.values();
        for (DSMType dsmType : dsmTypes) {
            if (hasTemp && !dsmType.equals(DSMType.aggregation)) {
                List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                if (javaTemps.size() > 0) {
                    DomainInstTree dsmInstItem = new DomainInstTree(dsmType, projectVersionName, domainId, viewInstId, sourceClassName, hasTemp);
                    this.addChild(dsmInstItem);
                }
            } else {
                this.addChild(new DomainInstTree(dsmType, projectVersionName, domainId, viewInstId, sourceClassName, hasTemp));
            }
        }
    }

    public DomainInstTree(DSMType dsmType, String projectVersionName, String domainId, String viewInstId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = dsmType.getName();
        this.imageClass = dsmType.getImageClass();
        this.euClassName = "dsm.manager.temp.CustomTempGrid";
        this.iniFold = false;
        this.id = dsmType.getType();
        this.addTagVar("dsmType", dsmType.getType());
        this.addTagVar("domainId", domainId);
        this.addTagVar("viewInstId", viewInstId);
        this.addTagVar("projectVersionName", projectVersionName);
        this.addTagVar("sourceClassName", sourceClassName);
        switch (dsmType) {
            case repository:
                RepositoryType[] repositoryTypes = RepositoryType.values();
                for (RepositoryType type : repositoryTypes) {
                    if (hasTemp) {
                        List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getRepositoryTypeTemps(type);
                        if (javaTemps.size() > 0) {
                            DomainInstTree dsmInstItem = new DomainInstTree(type, projectVersionName, sourceClassName, hasTemp);
                            dsmInstItem.setIniFold(true);
                            this.addChild(dsmInstItem);
                        }
                    } else {
                        this.addChild(new DomainInstTree(type, projectVersionName, sourceClassName, hasTemp));
                    }
                }
                break;
            case aggregation:
                AggregationType[] aggregationTypes = AggregationType.values();
                for (AggregationType type : aggregationTypes) {
                    if (hasTemp) {
                        List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getAggregationTemps(type);
                        if (javaTemps.size() > 0) {
                            AggregationTree dsmInstItem = new AggregationTree(type, domainId, sourceClassName, hasTemp);
                            dsmInstItem.setIniFold(true);
                            this.addChild(dsmInstItem);
                        }
                    } else {
                        this.addChild(new AggregationTree(type, domainId, sourceClassName, hasTemp));
                    }
                }
                break;

            case view:
                ViewType[] viewTypes = ViewType.values();
                for (ViewType type : viewTypes) {
                    if (hasTemp) {
                        List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getViewTemps(type);
                        if (javaTemps.size() > 0) {
                            DomainInstTree dsmInstItem = new DomainInstTree(type, domainId, viewInstId, sourceClassName, hasTemp);
                            dsmInstItem.setIniFold(true);
                            this.addChild(dsmInstItem);
                        }
                    } else {
                        this.addChild(new DomainInstTree(type, domainId, viewInstId, sourceClassName, hasTemp));
                    }
                }
                break;


            case customDomain:
                CustomDomainType[] customDomainTypes = CustomDomainType.values();
                for (CustomDomainType customDomainType : customDomainTypes) {
                    if (hasTemp) {
                        List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(customDomainType);
                        if (javaTemps.size() > 0) {
                            CustomDomainTree dsmInstItem = new CustomDomainTree(customDomainType, domainId, sourceClassName, hasTemp);
                            dsmInstItem.setIniFold(true);
                            this.addChild(dsmInstItem);
                        }
                    } else {
                        this.addChild(new CustomDomainTree(customDomainType, domainId, sourceClassName, hasTemp));
                    }
                }
                break;

            default:
                if (hasTemp) {
                    List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                    for (JavaTemp javaTemp : javaTemps) {
                        this.addChild(new DomainInstTree(javaTemp, dsmType, sourceClassName, projectVersionName, domainId, viewInstId));
                    }
                }

        }

    }

    public DomainInstTree(ViewType viewType, String domainId, String viewInstId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = viewType.getName();
        this.imageClass = viewType.getImageClass();
        this.id = DSMType.view.getType() + "_" + viewType.getType();
        this.euClassName = "dsm.manager.temp.ViewJavaTempGrid";
        this.iniFold = false;
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("dsmType", DSMType.view.getType());
        this.addTagVar("domainId", domainId);
        this.addTagVar("viewInstId", viewInstId);
        this.addTagVar("viewType", viewType.getType());
    }

    public DomainInstTree(RepositoryType type, String projectVersionName, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = type.getName();
        this.imageClass = type.getImageClass();
        this.id = DSMType.view.getType() + "_" + type.getType();
        this.euClassName = "dsm.repository.temp.RepositoryTempGrid";
        this.iniFold = false;
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("dsmType", DSMType.repository.getType());

        this.addTagVar("projectVersionName", projectVersionName);
        this.addTagVar("repositoryType", type);
    }


    public DomainInstTree(DSMTempType dsmTempType, String projectVersionName, String domainId, String viewInstId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = dsmTempType.getName();
        this.id = dsmTempType.getType();
        this.iniFold = false;
        this.euClassName = "dsm.manager.temp.CustomTempGrid";
        this.addTagVar("dsmTempType", dsmTempType.getType());

        this.addTagVar("viewInstId", viewInstId);
        this.addTagVar("projectVersionName", projectVersionName);
        this.addTagVar("domainId", domainId);
        this.addTagVar("sourceClassName", sourceClassName);
        this.setImageClass(dsmTempType.getImageClass());
        DSMType[] dsmTypes = DSMType.values();
        for (DSMType dsmType : dsmTypes) {
            if (hasTemp && !dsmType.equals(DSMType.aggregation)) {
                List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getDSMTypeTemps(dsmType);
                if (javaTemps.size() > 0) {
                    DomainInstTree dsmInstItem = new DomainInstTree(dsmType, projectVersionName, domainId, viewInstId, sourceClassName, hasTemp);
                    dsmInstItem.setIniFold(true);
                    this.addChild(dsmInstItem);
                }
            } else {
                this.addChild(new DomainInstTree(dsmType, projectVersionName, domainId, viewInstId, sourceClassName, hasTemp));
            }
        }
    }


    public DomainInstTree(JavaTemp javaTemp, DSMType dsmType, String sourceClassName, String projectVersionName, String domainId, String viewInstId) {
        this.caption = javaTemp.getName();
        this.imageClass = "xui-icon-code";
        this.id = javaTemp.getJavaTempId();
        this.addTagVar("dsmType", dsmType.getType());
        this.addTagVar("viewInstId", viewInstId);
        this.addTagVar("projectVersionName", projectVersionName);
        this.addTagVar("domainId", domainId);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("javaTempId", javaTemp.getJavaTempId());
    }
}
