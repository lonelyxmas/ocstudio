package com.ds.dsm.aggregation.api.method.parameter.args;

import com.ds.dsm.aggregation.api.method.parameter.request.RequestParameterService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.RequestPathBean;
import com.ds.esd.custom.api.enums.RequestPathTypeEnum;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.StretchType;


@BottomBarMenu
@FormAnnotation(stretchHeight = StretchType.last, col = 1, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {ArgsParameterService.class})
public class ArgsParameterFormView {

    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    private String methodName;

    @CustomAnnotation(caption = "参数类型")
    RequestPathTypeEnum type = RequestPathTypeEnum.form;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "参数名称", uid = true)
    String paramsname;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "映射路径")
    String path;


    public ArgsParameterFormView() {

    }


    public ArgsParameterFormView(RequestPathBean parameter) {
        this.methodName = parameter.getMethodName();
        this.domainId = parameter.getDomainId();
        this.sourceClassName = parameter.getSourceClassName();
        this.type = parameter.getType();
        this.paramsname = parameter.getParamsname();
        this.path = parameter.getPath();
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public RequestPathTypeEnum getType() {
        return type;
    }

    public void setType(RequestPathTypeEnum type) {
        this.type = type;
    }

    public String getParamsname() {
        return paramsname;
    }

    public void setParamsname(String paramsname) {
        this.paramsname = paramsname;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
