package com.ds.dsm.aggregation.api.method.service;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.api.method.bind.APIBindMenuView;
import com.ds.dsm.aggregation.api.method.bind.BindMenuPopTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping(path = "/dsm/agg/api/config/bindmenu/")
public class APIBindMenuService {


    @MethodChinaName(cname = "绑定按钮")
    @RequestMapping(method = RequestMethod.POST, value = "APIBindMenus")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-statusbutton", caption = "按钮事件")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<APIBindMenuView>> getBindMenus(String sourceClassName, String domainId, String methodName) {
        ListResultModel<List<APIBindMenuView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            List<APIBindMenuView> menuViews = new ArrayList<>();
            if (apiCallBean.getApi() != null) {
                for (CustomMenuItem item : apiCallBean.getApi().getBindMenu()) {
                    menuViews.add(new APIBindMenuView(item, apiCallBean));
                }
            }

            resultModel = PageUtil.getDefaultPageList(menuViews);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "绑定方法")
    @RequestMapping("BindMenuPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<BindMenuPopTree>> getBindMenuPopTree(String sourceClassName, String methodName, String domainId) {
        TreeListResultModel<List<BindMenuPopTree>> model = new TreeListResultModel<>();
        List<BindMenuPopTree> popTrees = new ArrayList<>();
        BindMenuPopTree menuPopTree = new BindMenuPopTree(sourceClassName, domainId, methodName);
        popTrees.add(menuPopTree);
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addMenu(String sourceClassName, String BindMenuPopTree, String methodName, String domainId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig callBean = esdClassConfig.getMethodByName(methodName);
            if (BindMenuPopTree != null && !BindMenuPopTree.equals("")) {
                String[] menuIds = StringUtility.split(BindMenuPopTree, ";");
                for (String menuId : menuIds) {
                    try {
                        if (menuId.indexOf("|") > -1) {
                            menuId = StringUtility.split(menuId, "|")[1];
                        }
                        CustomMenuItem item = CustomMenuItem.valueOf(menuId);
                        callBean.getApi().getBindMenu().add(item);
                    } catch (Throwable e) {

                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (
                JDSException e)

        {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delMenu(String sourceClassName, String type, String methodName, String domainId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig callBean = esdClassConfig.getMethodByName(methodName);
            String[] menuIds = StringUtility.split(type, ";");
            for (String menuId : menuIds) {
                try {
                    CustomMenuItem item = CustomMenuItem.valueOf(menuId);
                    callBean.getApi().getBindMenu().remove(item);
                } catch (Throwable e) {

                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }
}
