package com.ds.dsm.aggregation.api.method.service;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.api.method.action.ActionGirdView;
import com.ds.dsm.aggregation.api.method.action.bind.BindActionFormView;
import com.ds.dsm.aggregation.api.method.callback.APICallBackPopTree;
import com.ds.dsm.aggregation.api.method.event.APIGridEventView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.APIEventBean;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/customactions/")
public class CustomActionService {

    @RequestMapping(method = RequestMethod.POST, value = "Actions")
    @GridViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.index)
    @ResponseBody
    public ResultModel<ActionGirdView> getActions(String sourceClassName, String methodName, String domainId, String eventId) {
        ResultModel<ActionGirdView> result = new ResultModel<ActionGirdView>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            APIEventBean apiEvent = apiCallBean.getApi().getEventById(eventId);
            List<ActionGirdView> actionViews = new ArrayList<>();
            for (Action action : apiEvent.getActions()) {
                actionViews.add(new ActionGirdView(action, apiCallBean));
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return result;
    }


    @MethodChinaName(cname = "调用事件")
    @RequestMapping(method = RequestMethod.POST, value = "CustomActions")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-event", caption = "调用事件")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<APIGridEventView>> getCustomActions(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<APIGridEventView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            List<APIEventBean> events = apiCallBean.getApi().getAllEvent();
            List<APIGridEventView> actionViews = new ArrayList<>();
            for (APIEventBean apiEvent : events) {
                actionViews.add(new APIGridEventView(apiEvent, apiCallBean));
//                for (Action action : apiEvent.getActions()) {
////                    actionViews.add(new APIGridEventView(action, apiCallBean));
////                }
            }

            resultModel = PageUtil.getDefaultPageList(actionViews, APIGridEventView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "常用事件")
    @RequestMapping("APIEventPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<APICallBackPopTree>> addActionInfo(String sourceClassName, String domainId, String methodName) {
        TreeListResultModel<List<APICallBackPopTree>> model = new TreeListResultModel<>();
        List<APICallBackPopTree> popTrees = new ArrayList<>();
        APICallBackPopTree menuPopTree = new APICallBackPopTree(sourceClassName, domainId, methodName);
        popTrees.add(menuPopTree);
        model.setData(popTrees);
        return model;
    }


    @FormViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "常用事件")
    @RequestMapping("ActionInfo")
    @DialogAnnotation(width = "650", height = "550")
    @APIEventAnnotation(bindMenu = CustomMenuItem.editor, autoRun = true)
    @ResponseBody
    public ResultModel<BindActionFormView> getActionInfo(String sourceClassName, String actionId, String domainId, String methodName) {
        ResultModel<BindActionFormView> model = new ResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Action currAction = apiCallBean.getApi().getActionById(actionId);
            if (currAction != null) {
                model.setData(new BindActionFormView(currAction, apiCallBean));
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return model;
    }


}
