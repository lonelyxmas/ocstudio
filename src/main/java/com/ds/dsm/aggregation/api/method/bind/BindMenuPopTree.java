package com.ds.dsm.aggregation.api.method.bind;

import com.ds.common.JDSException;
import com.ds.dsm.aggregation.api.method.service.APIBindMenuService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu()
@TreeAnnotation(heplBar = true,  selMode = SelModeType.multibycheckbox, customService = {APIBindMenuService.class}, bottombarMenu = {CustomFormMenu.Close, CustomFormMenu.Save})
@PopTreeAnnotation(caption = "绑定按钮")
public class BindMenuPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String euClassName;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String methodName;


    public BindMenuPopTree(String euClassName, String domainId, String methodName) {
        super("allBindMenu", "绑定按钮");
        ComponentType[] componentTypes = CustomMenuItem.getAllTypes();
        for (ComponentType type : componentTypes) {
            BindMenuPopTree item = null;
            try {
                item = new BindMenuPopTree(euClassName, domainId, methodName, type);
                item.addTagVar("euClassName", euClassName);
                item.addTagVar("domainId", domainId);
                item.addTagVar("methodName", methodName);
                this.addChild(item);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }

    }


    public BindMenuPopTree(String euClassName, String domainId, String methodName, ComponentType type) throws JDSException {
        super(type.getType(), type.name() + "(" + type.getName() + ")", type.getImageClass());
        for (CustomMenuItem menu : CustomMenuItem.getAllCustomItems(type)) {
            BindMenuPopTree item = new BindMenuPopTree(euClassName, domainId, methodName, menu, type);
            item.addTagVar("euClassName", euClassName);
            item.addTagVar("domainId", domainId);
            item.addTagVar("methodName", methodName);
            this.addChild(item);
        }

    }


    public BindMenuPopTree(String euClassName, String domainId, String methodName, CustomMenuItem menu, ComponentType type) throws JDSException {
        super(type.name() + "|" + menu.getType(), menu.getType() + "(" + menu.getName() + ")", menu.getImageClass());
        this.setIniFold(true);
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
