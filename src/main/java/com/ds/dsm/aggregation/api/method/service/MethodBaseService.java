package com.ds.dsm.aggregation.api.method.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.api.method.APIMethodBaseFormView;
import com.ds.dsm.aggregation.api.method.APIMethodInfo;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.CustomAPICallBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.component.data.APICallerProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/")
public class MethodBaseService {

    @MethodChinaName(cname = "API信息")
    @RequestMapping(method = RequestMethod.POST, value = "APIBaseMethodInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(caption = "API信息", imageClass = "spafont spa-icon-c-webapi")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<APIMethodInfo> getAPIBaseMethodInfo(String domainId, String methodName, String sourceClassName) {
        ResultModel<APIMethodInfo> result = new ResultModel<APIMethodInfo>();
        result.setData(new APIMethodInfo());

        return result;
    }


    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFormBase")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = {CustomMenuItem.save})
    public @ResponseBody
    ResultModel<Boolean> updateFormBase(@RequestBody APIMethodBaseFormView base) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig esdClassConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(base.getSourceClassName(), base.getViewInstId());
            MethodConfig customFormViewBean = esdClassConfig.getSourceConfig().getMethodByName(base.getMethodName());
            CustomAPICallBean apiCallBean = customFormViewBean.getApi();
            APICallerProperties apiCallerProperties = apiCallBean.getApiCallerProperties();
            apiCallBean.setAllform(base.getAllform());
            apiCallBean.setAutoRun(base.getAutoRun());
            apiCallBean.setQueryAsync(base.getQueryAsync());
            apiCallBean.setIndex(base.getIndex());
            customFormViewBean.getRequestMapping().getValue().clear();
            customFormViewBean.getRequestMapping().setMethod(base.getMethod());
            customFormViewBean.getRequestMapping().getValue().add(base.getUrl());
            customFormViewBean.getRequestMapping().setFristUrl(base.getUrl());
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig.getSourceConfig());
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "清空配置")
    @RequestMapping(method = RequestMethod.POST, value = "clear")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clearForm(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig esdClassConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);

            esdClassConfig.getSourceConfig().getAllMethodMap().remove(methodName);
            DSMFactory.getInstance().getViewManager().updateViewEntityConfig(esdClassConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
