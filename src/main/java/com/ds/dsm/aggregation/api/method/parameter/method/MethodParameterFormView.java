package com.ds.dsm.aggregation.api.method.parameter.method;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.web.RequestParamBean;
import com.ds.web.annotation.Required;


@BottomBarMenu
@FormAnnotation(stretchHeight = StretchType.last, col = 1, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {MethodParameterService.class})
public class MethodParameterFormView {


    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    private String methodName;


    @Required
    @CustomAnnotation(caption = "参数名称", uid = true)
    String paramName;

    @CustomAnnotation(caption = "JSON转换")
    Boolean jsonData = false;

    @FieldAnnotation(required = true, colSpan = 1)
    @CustomAnnotation(caption = "参数类型")
    String paramClassName;


    public MethodParameterFormView() {

    }

    public MethodParameterFormView(RequestParamBean parameter) {
        this.methodName = parameter.getMethodName();
        this.domainId = parameter.getDomainId();
        this.sourceClassName = parameter.getSourceClassName();
        this.paramName = parameter.getParamName();
        this.jsonData = parameter.getJsonData();
        this.paramClassName = parameter.getParamClassName();

    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public Boolean getJsonData() {
        return jsonData;
    }

    public void setJsonData(Boolean jsonData) {
        this.jsonData = jsonData;
    }

    public String getParamClassName() {
        return paramClassName;
    }

    public void setParamClassName(String paramClassName) {
        this.paramClassName = paramClassName;
    }
}
