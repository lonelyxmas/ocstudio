package com.ds.dsm.aggregation.api.method.parameter.method;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.web.RequestParamBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/parameters/method/")
@MethodChinaName(cname = "方法参数", imageClass = "spafont spa-icon-tools")

public class MethodParameterService {

    @RequestMapping(method = RequestMethod.POST, value = "AddParameters")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add}, autoRun = true)
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350")
    @ModuleAnnotation(caption = "添加参数")
    @ResponseBody
    public ResultModel<MethodParameterFormView> addParamter(String methodName, String sourceClassName, String domainId) {
        ResultModel<MethodParameterFormView> result = new ResultModel<MethodParameterFormView>();
        RequestParamBean parameter = new RequestParamBean();
        parameter.setDomainId(domainId);
        parameter.setParamName("arg");
        parameter.setParamClassName(String.class.getName());
        parameter.setMethodName(methodName);
        parameter.setSourceClassName(sourceClassName);
        result.setData(new MethodParameterFormView(parameter));
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ParameterInfo")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor})
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350")
    @ModuleAnnotation(caption = "编辑参数")
    @ResponseBody
    public ResultModel<MethodParameterFormView> getParamterInfo(String methodName, String sourceClassName, String domainId, String paramName) {
        ResultModel<MethodParameterFormView> result = new ResultModel<MethodParameterFormView>();
        ApiClassConfig esdClassConfig = null;
        try {
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            RequestParamBean parameter = null;
            if (paramName != null) {
                parameter = apiCallBean.getParamByName(paramName);
                if (parameter == null) {
                    parameter = new RequestParamBean();
                    parameter.setParamName(paramName);
                    parameter.setParamClassName(String.class.toString());
                    apiCallBean.updateParam(parameter);
                    DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
                }

            } else {
                parameter = new RequestParamBean();
            }

            MethodParameterFormView formView = new MethodParameterFormView(parameter);
            result.setData(formView);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


    @MethodChinaName(cname = "保存参数")
    @RequestMapping(method = RequestMethod.POST, value = "saveParameters")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save})
    @ResponseBody
    public ResultModel<Boolean> saveParamter(@RequestBody RequestParamBean parameter) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ApiClassConfig esdClassConfig = null;
        try {
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(parameter.getSourceClassName(), parameter.getDomainId());
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(parameter.getMethodName());

            apiCallBean.updateParam(parameter);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "删除参数")
    @RequestMapping(method = RequestMethod.POST, value = "delParameters")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = {CustomMenuItem.delete})
    @ResponseBody
    public ResultModel<Boolean> deleteParams(String methodName, String sourceClassName, String domainId, String paramName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ApiClassConfig esdClassConfig = null;
        try {
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            if (paramName != null && apiCallBean.getParamByName(paramName) != null) {
                RequestParamBean parameter = apiCallBean.getParamByName(paramName);
                apiCallBean.getParamSet().remove(parameter);
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
