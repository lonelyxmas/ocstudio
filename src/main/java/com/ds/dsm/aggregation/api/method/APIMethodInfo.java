package com.ds.dsm.aggregation.api.method;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.api.method.parameter.MethodParamsMetaGroup;
import com.ds.dsm.aggregation.api.method.service.MethodBaseService;
import com.ds.dsm.view.config.action.CustomBuildAction;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.annotation.Pid;
import org.checkerframework.checker.guieffect.qual.UI;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/")
@BottomBarMenu(menuClass = CustomBuildAction.class)
@NavGroupAnnotation(customService = MethodBaseService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class APIMethodInfo {

    @Pid
    String domainId;
    @Pid
    String methodName;
    @Pid
    String viewInstId;
    @Pid
    String sourceClassName;


    @MethodChinaName(cname = "API信息")
    @RequestMapping(method = RequestMethod.POST, value = "APIMethodBaseView")
    @FormViewAnnotation()
    @UIAnnotation( height = "220")
    @ModuleAnnotation(caption = "API信息", imageClass = "spafont spa-icon-c-webapi", dock = Dock.top)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<APIMethodBaseFormView> getAPIMethodBaseView(String domainId, String methodName, String sourceClassName) {
        ResultModel<APIMethodBaseFormView> result = new ResultModel<APIMethodBaseFormView>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            result.setData(new APIMethodBaseFormView(apiCallBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "详细信息")
    @RequestMapping(method = RequestMethod.POST, value = "MethodParamsMetaGroup")
    @NavTabsViewAnnotation
    @DynLoadAnnotation
    @ModuleAnnotation(dock = Dock.fill)
    @ResponseBody
    public ResultModel<MethodParamsMetaGroup> getAPIMethodMetaView(String domainId, String sourceClassName, String methodName, String projectId) {
        ResultModel<MethodParamsMetaGroup> result = new ResultModel<MethodParamsMetaGroup>();
        return result;

    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
