package com.ds.dsm.aggregation.api.method;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;


@TabsAnnotation(singleOpen = true)
@TreeAnnotation()
public class MethodAPITree<T extends MethodAPITree> extends TreeListItem<T> {


    @Pid
    ModuleViewType dsmType;
    @Pid
    String sourceClassName;
    @Pid
    String methodName;
    @Pid
    String domainId;
    @Pid
    String viewInstId;


    @TreeItemAnnotation(bindService = MethodConfigService.class, caption = "控制调用", imageClass = "spafont spa-icon-c-webapi")
    public MethodAPITree(MethodConfig methodAPIBean) {
        CustomMenuItem customMenuItem = methodAPIBean.getDefaultMenuItem();
        this.imageClass = methodAPIBean.getImageClass();
        this.domainId = methodAPIBean.getDomainId();
        this.viewInstId = methodAPIBean.getViewInstId();
        this.methodName = methodAPIBean.getMethodName();
        this.dsmType = methodAPIBean.getModuleViewType();
        if (methodAPIBean.getCaption() != null && !methodAPIBean.getCaption().equals("")) {
            this.caption = methodAPIBean.getCaption() + "(" + methodAPIBean.getName() + ")";
        } else {
            if (customMenuItem != null) {
                this.caption = "WebAPI(" + methodAPIBean.getName() + ")-" + methodAPIBean.getModuleViewType().getName();
            } else {
                this.caption = "WebAPI(" + methodAPIBean.getCaption() + ")";
            }
        }
        this.id = methodName + "_MethodAPI";
        this.tips = methodAPIBean.getCaption() + "<br/>";
        this.tips = tips + methodAPIBean.getMetaInfo();


    }

    public ModuleViewType getDsmType() {
        return dsmType;
    }

    public void setDsmType(ModuleViewType dsmType) {
        this.dsmType = dsmType;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}


