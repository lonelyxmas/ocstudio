package com.ds.dsm.aggregation.api.method.parameter.response;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.ResponsePathBean;
import com.ds.esd.custom.api.enums.ResponsePathTypeEnum;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.web.annotation.Required;


@BottomBarMenu
@FormAnnotation(stretchHeight = StretchType.last, col = 1, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {ResponseParameterService.class})
public class ResponseParameterFormView {


    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    private String methodName;

    @CustomAnnotation(caption = "参数类型")
    ResponsePathTypeEnum type = ResponsePathTypeEnum.form;

    @Required
    @CustomAnnotation(caption = "参数名称")
    String paramsname;

    @CustomAnnotation(caption = "映射路径")
    String path;

    public ResponseParameterFormView() {

    }

    public ResponseParameterFormView(ResponsePathBean parameter) {
        this.methodName = parameter.getMethodName();
        this.domainId = parameter.getDomainId();
        this.sourceClassName = parameter.getSourceClassName();
        this.type = parameter.getType();
        this.paramsname = parameter.getParamsname();
        this.path = parameter.getPath();

    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public ResponsePathTypeEnum getType() {
        return type;
    }

    public void setType(ResponsePathTypeEnum type) {
        this.type = type;
    }

    public String getParamsname() {
        return paramsname;
    }

    public void setParamsname(String paramsname) {
        this.paramsname = paramsname;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
