package com.ds.dsm.aggregation.api.method.parameter.method;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.web.RequestParamBean;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {MethodParameterService.class}, event = CustomGridEvent.editor)
public class MethodParameterGridView {

    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    private String methodName;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "参数名称", uid = true)
    String paramName;

    @CustomAnnotation(caption = "json转换")
    Boolean jsonData = false;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "参数类型")
    String paramClassName;


    public MethodParameterGridView() {

    }


    public MethodParameterGridView(RequestParamBean parameter, MethodConfig methodConfig) {
        this.methodName = methodConfig.getMethodName();
        this.domainId = methodConfig.getDomainId();
        this.sourceClassName = methodConfig.getSourceClassName();
        this.paramName = parameter.getParamName();
        this.jsonData = parameter.getJsonData();
        this.paramClassName = parameter.getParamClassName();

    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public Boolean getJsonData() {
        return jsonData;
    }

    public void setJsonData(Boolean jsonData) {
        this.jsonData = jsonData;
    }

    public String getParamClassName() {
        return paramClassName;
    }

    public void setParamClassName(String paramClassName) {
        this.paramClassName = paramClassName;
    }
}
