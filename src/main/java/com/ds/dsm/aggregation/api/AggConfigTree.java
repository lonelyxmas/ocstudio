package com.ds.dsm.aggregation.api;

import com.ds.common.JDSException;
import com.ds.dsm.aggregation.api.method.MethodAPITree;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;

import java.util.List;


@TabsAnnotation(singleOpen = true)
@TreeAnnotation()
public class AggConfigTree extends TreeListItem {
    public AggConfigTree(String sourceClassName, String domainId, String viewInstId) throws JDSException {
        super("CustomAllMethodNode", "视图配置", "spafont spa-icon-c-webapi");
        this.setEuClassName("dsm.agg.entity.config.AggEntityInfo");
        ViewEntityConfig esdClassConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId);
        ESDClass esdServiceClass = esdClassConfig.getSourceClass();
        ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getViewManager().getViewEntityConfig(esdServiceClass.getClassName(),  viewInstId).getSourceConfig();
        this.setIniFold(false);
        List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
        for (MethodConfig methodAPIBean : customMethods) {
            if (methodAPIBean.getView() != null) {
                this.addChild(new AggConfigTree(methodAPIBean));
            }
        }
    }


    public AggConfigTree(MethodConfig methodAPIBean) {
        ModuleViewType dsmType = methodAPIBean.getModuleViewType();
        String methodName = methodAPIBean.getMethodName();
        CustomMenuItem customMenuItem = methodAPIBean.getDefaultMenuItem();
        if (methodAPIBean.getCaption() != null && !methodAPIBean.getCaption().equals("")) {
            this.caption = methodAPIBean.getCaption() + "(" + methodAPIBean.getName() + ")";
        } else {
            if (customMenuItem != null) {
                this.caption = methodName + "(" + methodAPIBean.getName() + ")-" + methodAPIBean.getModuleViewType().getName();
            } else {
                this.caption = methodName + "(" + dsmType.getName() + ")-" + methodAPIBean.getModuleViewType().getName();
            }
        }
        this.tips = methodAPIBean.getCaption() + "(" + methodAPIBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + dsmType.getName() + ")";
        this.imageClass = dsmType.getImageClass();
        this.setIniFold(false);
        this.setEuClassName(dsmType.getClassName());
        this.setId(dsmType.getType() + methodName);
        this.setGroup(true);
        this.addTagVar("sourceClassName", methodAPIBean.getSourceClassName());
        this.addTagVar("methodName", methodName);
        this.addTagVar("viewInstId", methodAPIBean.getViewInstId());
        this.addTagVar("domainId", methodAPIBean.getDomainId());

      //  this.addChild(new WinConfigTree(methodAPIBean));
        this.addChild(new MethodAPITree(methodAPIBean));


    }

}


