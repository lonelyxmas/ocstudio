package com.ds.dsm.aggregation.api.method.parameter.custom;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = {CustomRequestService.class}, bottombarMenu = {CustomFormMenu.Save})
@PopTreeAnnotation(caption = "常用参数")
public class CustomRequestParamsPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String euClassName;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String methodName;


    public CustomRequestParamsPopTree(String sourceClassName, String domainId, String methodName) {
        super("customParams", "常用参数");
        for (RequestPathEnum data : RequestPathEnum.values()) {
            CustomRequestParamsPopTree popTree = new CustomRequestParamsPopTree(data.name(), data.getType().getName(), sourceClassName, domainId, methodName);
            this.addChild(popTree);
        }
    }


    CustomRequestParamsPopTree(String id, String caption) {
        super(id, caption);
    }

    CustomRequestParamsPopTree(String id, String caption, String sourceClassName, String domainId, String methodName) {
        super(id, caption);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("domainId", domainId);
        this.addTagVar("methodName", methodName);

    }

    @Override
    public String getEuClassName() {
        return euClassName;
    }

    @Override
    public void setEuClassName(String euClassName) {
        this.euClassName = euClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

}
