package com.ds.dsm.aggregation.api.method;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.enums.*;
import com.ds.esd.tool.ui.json.EnumSetDeserializer;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@BottomBarMenu
@FormAnnotation(col = 2, stretchHeight = StretchType.last)
public class APIMethodBaseFormView {

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;


    @CustomAnnotation(hidden = true, uid = true)
    String sourceClassName;

    @CustomAnnotation(caption = "方法名称")
    String methodName;

    @ComboNumberAnnotation
    @ComboInputAnnotation(inputType = ComboInputType.spin)
    @FieldAnnotation(componentType = ComponentType.ComboInput)
    @CustomAnnotation(caption = "显示顺序")
    public Integer index;


    @CustomAnnotation(caption = "JSON格式")
    Boolean responseBody;

    @CustomAnnotation(caption = "异步调用")
    public Boolean queryAsync;

    @CustomAnnotation(caption = "自动运行")
    public Boolean autoRun;

    @CustomAnnotation(caption = "级联处理")
    public Boolean allform;


    @JSONField(deserializeUsing = EnumSetDeserializer.class)
    @ListAnnotation(selMode = SelModeType.multibycheckbox, itemRow = ItemRow.cell)
    @FieldAnnotation(colSpan = -1, rowHeight = "60", componentType = ComponentType.List)
    @CustomAnnotation(caption = "HttpMethod")
    Set<RequestMethod> method = new LinkedHashSet<>();

    @FieldAnnotation(colSpan = -1, rowHeight = "30")
    @CustomAnnotation(caption = "访问地址")
    String url;


    public APIMethodBaseFormView() {

    }


    public APIMethodBaseFormView(MethodConfig methodAPICallBean) {
        this.autoRun = methodAPICallBean.getApi().getApiCallerProperties().getAutoRun();
        this.methodName = methodAPICallBean.getMethodName();
        this.viewInstId = methodAPICallBean.getViewInstId();
        this.domainId = methodAPICallBean.getDomainId();


        this.index = methodAPICallBean.getApi().getIndex();
        this.queryAsync = methodAPICallBean.getApi().getApiCallerProperties().getQueryAsync();
        this.url = methodAPICallBean.getRequestMapping().getFristUrl();
        this.method = methodAPICallBean.getRequestMapping().getMethod();
        if (method == null) {
            method = new HashSet<>();
        }
        if (method.isEmpty()) {
            method.add(RequestMethod.POST);
        }
        this.allform = methodAPICallBean.getApi().getApiCallerProperties().getAllform();
        this.responseBody = methodAPICallBean.getResponseBody();
        this.sourceClassName = methodAPICallBean.getSourceClassName();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public Set<RequestMethod> getMethod() {
        return method;
    }

    public void setMethod(Set<RequestMethod> method) {
        this.method = method;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }


    public Boolean getQueryAsync() {
        return queryAsync;
    }

    public void setQueryAsync(Boolean queryAsync) {
        this.queryAsync = queryAsync;
    }

    public Boolean getAutoRun() {
        return autoRun;
    }

    public void setAutoRun(Boolean autoRun) {
        this.autoRun = autoRun;
    }

    public Boolean getAllform() {
        return allform;
    }

    public void setAllform(Boolean allform) {
        allform = allform;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public Boolean getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(Boolean responseBody) {
        this.responseBody = responseBody;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
