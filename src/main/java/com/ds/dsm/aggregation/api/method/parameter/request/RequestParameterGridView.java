package com.ds.dsm.aggregation.api.method.parameter.request;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.api.RequestPathBean;
import com.ds.esd.custom.api.enums.RequestPathTypeEnum;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.web.annotation.Pid;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {RequestParameterService.class}, event = CustomGridEvent.editor)
public class RequestParameterGridView {

    @Pid
    private String sourceClassName;

    @Pid
    private String domainId;

    @Pid
    private String methodName;

    @CustomAnnotation(caption = "参数类型")
    RequestPathTypeEnum type;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "参数名称", uid = true)
    String paramsname;

    @CustomAnnotation(caption = "映射路径")
    String path;


    public RequestParameterGridView() {

    }


    public RequestParameterGridView(RequestPathBean parameter, MethodConfig methodConfig) {
        this.methodName = methodConfig.getMethodName();
        this.domainId = methodConfig.getDomainId();
        this.sourceClassName = methodConfig.getSourceClassName();
        this.type = parameter.getType();
        this.paramsname = parameter.getParamsname();
        this.path = parameter.getPath();

    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public RequestPathTypeEnum getType() {
        return type;
    }

    public void setType(RequestPathTypeEnum type) {
        this.type = type;
    }

    public String getParamsname() {
        return paramsname;
    }

    public void setParamsname(String paramsname) {
        this.paramsname = paramsname;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
