package com.ds.dsm.aggregation.api.method.bind;


import com.ds.dsm.aggregation.api.method.service.APIBindMenuService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.CustomMenu;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.CustomImageType;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = APIBindMenuService.class)
public class APIBindMenuView {
    @CustomAnnotation(caption = "按钮名称")
    private String menucaption;

    @CustomAnnotation(caption = "类型", uid = true)
    private String type;

    @CustomAnnotation(caption = "位置")
    private String pos;

    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;


    @CustomAnnotation(pid = true, hidden = true)
    private String methodName;


    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;

    @CustomAnnotation(caption = "表达式")
    private String expression;

    @CustomAnnotation(caption = "类型")
    public ComboInputType itemType = ComboInputType.button;

    public APIBindMenuView() {

    }

    public APIBindMenuView(CustomMenu customMenu, MethodConfig methodConfig) {
        this.type = customMenu.type();
        this.menucaption = customMenu.caption();
        this.expression = customMenu.expression();
        this.imageClass = customMenu.imageClass();
        this.itemType = customMenu.itemType();


    }


    public APIBindMenuView(CustomMenuItem item, MethodConfig methodConfig) {
        this.type = item.getType();
        this.menucaption = item.getMenu().caption();
        this.expression = item.getMenu().expression();
        this.imageClass = item.getMenu().imageClass();
        this.itemType = item.getMenu().itemType();


    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMenucaption() {
        return menucaption;
    }

    public void setMenucaption(String menucaption) {
        this.menucaption = menucaption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public ComboInputType getItemType() {
        return itemType;
    }

    public void setItemType(ComboInputType itemType) {
        this.itemType = itemType;
    }

}
