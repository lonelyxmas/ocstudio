package com.ds.dsm.aggregation.api;

import com.ds.dsm.aggregation.config.menu.AggMenuService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.dsm.aggregation.AggEntityConfig;


@BottomBarMenu
@FormAnnotation(customService = AggMenuService.class)
public class APIConfigBaseView {

    @CustomAnnotation(caption = "名称")
    String name;
    @CustomAnnotation(caption = "描述")
    String desc;

    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation(caption = "访问地址")
    String url;

    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation(caption = "类名")
    String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    public APIConfigBaseView() {

    }

    public APIConfigBaseView(AggEntityConfig aggEntityConfig) {
        ESDClass esdClass = aggEntityConfig.getESDClass();
        this.sourceClassName = esdClass.getClassName();
        this.url = aggEntityConfig.getUrl();
        this.viewInstId = esdClass.getViewInstId();
        this.domainId = esdClass.getDomainId();
        this.desc = esdClass.getDesc();
        this.name = esdClass.getName();
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}
