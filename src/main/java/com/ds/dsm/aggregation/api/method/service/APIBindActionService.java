package com.ds.dsm.aggregation.api.method.service;

import com.alibaba.fastjson.JSONArray;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.api.method.action.bind.BindActionFormView;
import com.ds.dsm.aggregation.api.method.action.bind.BindActionGirdView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.enums.event.ActionTypeEnum;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/bindactions/")
public class APIBindActionService {


    @MethodChinaName(cname = "绑定动作")
    @RequestMapping(method = RequestMethod.POST, value = "BindActions")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-event", caption = "绑定动作")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<BindActionGirdView>> getBindActions(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<BindActionGirdView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Set<Action> actions = apiCallBean.getApi().getBindAction();
            List<BindActionGirdView> actionViews = new ArrayList<>();
            for (Action action : actions) {
                actionViews.add(new BindActionGirdView(action, apiCallBean));
            }
            resultModel = PageUtil.getDefaultPageList(actionViews, BindActionGirdView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @FormViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "常用事件")
    @RequestMapping("AddBindAction")
    @DialogAnnotation(width = "650", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public ResultModel<BindActionFormView> addAction(String sourceClassName, String domainId, String methodName) {
        ResultModel<BindActionFormView> model = new ResultModel<>();
        BindActionFormView formView = new BindActionFormView();
        formView.setSourceClass(sourceClassName);
        formView.setDomainId(domainId);
        formView.setMethodName(methodName);
        formView.setCanReturn(true);
        formView.setType(ActionTypeEnum.other);
        model.setData(formView);
        return model;
    }

    @FormViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "常用事件")
    @RequestMapping("ActionInfo")
    @DialogAnnotation(width = "650", height = "550")
    @APIEventAnnotation(bindMenu = CustomMenuItem.editor, autoRun = true)
    @ResponseBody
    public ResultModel<BindActionFormView> getActionInfo(String sourceClassName, String actionId, String domainId, String methodName) {
        ResultModel<BindActionFormView> model = new ResultModel<>();
        ApiClassConfig esdClassConfig = null;
        try {
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Action currAction = apiCallBean.getApi().getActionById(actionId);
            if (currAction != null) {
                model.setData(new BindActionFormView(currAction, apiCallBean));
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return model;
    }

    @RequestMapping("updateAction")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> updateAction(@RequestBody BindActionFormView formView) {
        ResultModel<Boolean> model = new ResultModel<>();
        ApiClassConfig esdClassConfig;
        try {
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(formView.getSourceClass(), formView.getDomainId());
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(formView.getMethodName());
            Action currAction = apiCallBean.getApi().getActionById(formView.getId());
            if (currAction != null) {
                currAction.set_return(formView.getCanReturn());
                currAction.setMethod(formView.getMethod());
                currAction.setType(formView.getType());
                currAction.setScript(formView.getScript());
                if (formView.getArgs() != null && formView.getArgs().equals("")) {
                    currAction.setArgs(JSONArray.parseArray(formView.getArgs()));
                }
            } else {
                currAction = new Action();
                currAction.set_return(formView.getCanReturn());
                currAction.setMethod(formView.getMethod());
                currAction.setType(formView.getType());
                currAction.setScript(formView.getScript());
                if (formView.getArgs() != null && formView.getArgs().equals("")) {
                    currAction.setArgs(JSONArray.parseArray(formView.getArgs()));
                }
                apiCallBean.getApi().getBindAction().add(currAction);

            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);

        } catch (JDSException e) {
            e.printStackTrace();
        }


        return model;
    }


    @RequestMapping("delAction")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delAction(String sourceClassName, String methodName, String actionId, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        ApiClassConfig esdClassConfig;
        try {
            String[] menuIds = StringUtility.split(actionId, ";");
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            for (String menuId : menuIds) {
                Action currAction = apiCallBean.getApi().getActionById(menuId);
                apiCallBean.getApi().getBindAction().remove(currAction);
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
