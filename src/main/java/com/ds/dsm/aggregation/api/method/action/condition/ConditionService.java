package com.ds.dsm.aggregation.api.method.action.condition;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.component.event.Condition;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/condition/")
@MethodChinaName(cname = "执行条件", imageClass = "spafont spa-icon-tools")
public class ConditionService {

    @RequestMapping(method = RequestMethod.POST, value = "AddCondition")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add}, autoRun = true)
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "450", caption = "添加条件")
    @ModuleAnnotation()
    @ResponseBody
    public ResultModel<ConditionView> addCondition(String sourceClassName, String actionId, String methodName, String domainId) {
        ResultModel<ConditionView> result = new ResultModel<ConditionView>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Action currAction = apiCallBean.getApi().getActionById(actionId);
            if (currAction != null) {
                result.setData(new ConditionView(new Condition(), currAction, apiCallBean));
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ConditionInfo")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor})
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350", caption = "编辑条件")
    @ModuleAnnotation()
    @ResponseBody
    public ResultModel<ConditionView> getCondition(String sourceClassName, String conditionId, String actionId, String methodName, String domainId) {
        ResultModel<ConditionView> result = new ResultModel<ConditionView>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Action currAction = apiCallBean.getApi().getActionById(actionId);
            result.setData(new ConditionView(currAction.getConditionById(conditionId), currAction, apiCallBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "保存条件")
    @RequestMapping(method = RequestMethod.POST, value = "saveCondition")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save})
    @ResponseBody
    public ResultModel<Boolean> saveCondition(@RequestBody ConditionView condition) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();

        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(condition.sourceClassName, condition.getDomainId());
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(condition.getMethodName());
            Action currAction = apiCallBean.getApi().getActionById(condition.getActionId());
            Condition currCondition = currAction.getConditionById(condition.getConditionId());
            if (currCondition != null) {
                currCondition.setExpression(condition.getExpression());
                currCondition.setRight(condition.getRight());
                currCondition.setLeft(condition.getLeft());
                currCondition.setSymbol(condition.getSymbol());
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


    @MethodChinaName(cname = "删除条件")
    @RequestMapping(method = RequestMethod.POST, value = "delCondition")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = {CustomMenuItem.delete})
    @ResponseBody
    public ResultModel<Boolean> deleteCondition(String sourceClassName, String conditionId, String actionId, String methodName, String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Action currAction = apiCallBean.getApi().getActionById(actionId);
            if (currAction != null) {
                currAction.deleteCondition(conditionId);

            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
