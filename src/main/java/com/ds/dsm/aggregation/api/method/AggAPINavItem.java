package com.ds.dsm.aggregation.api.method;

import com.ds.dsm.aggregation.api.method.service.*;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum AggAPINavItem implements TreeItem {
    APIConfig("接口配置", "spafont spa-icon-c-webapi", MethodBaseService.class, true, false, false),
    EventConfig("监听事件", "spafont spa-icon-event", MethodEventService.class, false, false, false),
    BindConfig("关联按钮", "spafont spa-icon-values", APIBindMenuService.class, false, false, false),
    BindActionConfig("绑定动作", "spafont spa-icon-values", APIBindActionService.class, false, false, false),
    CallBackActionConfig("回调动作", "spafont spa-icon-action", CustomActionService.class, false, false, false);
    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    AggAPINavItem(String name, String imageClass, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
