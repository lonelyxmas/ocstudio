package com.ds.dsm.aggregation.api.method.event;

import com.ds.dsm.aggregation.api.method.service.MethodEventService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.enums.*;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.event.enums.APIEventEnum;
import com.ds.web.annotation.Pid;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = {MethodEventService.class}, bottombarMenu = {CustomFormMenu.Save})
@PopTreeAnnotation(caption = "API事件")
public class APIEventPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String euClassName;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String methodName;



    public APIEventPopTree(String sourceClassName, String domainId, String methodName) {

        super("allApiEvent", "API事件");

        for (APIEventEnum apiEventEnum : APIEventEnum.values()) {
            APIEventPopTree apiEventPopTree = new APIEventPopTree(apiEventEnum.name(), apiEventEnum.getName());
            apiEventPopTree.setImageClass(apiEventEnum.getImageClass());
            this.addChild(apiEventPopTree);
            switch (apiEventEnum) {


                case beforeData:
                    for (CustomBeforData data : CustomBeforData.values()) {
                        APIEventPopTree popTree = new APIEventPopTree(apiEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);

                    }
                    break;
                case onData:
                    for (CustomOnData data : CustomOnData.values()) {
                        APIEventPopTree popTree = new APIEventPopTree(apiEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);

                    }
                    break;
                case beforeInvoke:
                    for (CustomBeforInvoke data : CustomBeforInvoke.values()) {
                        APIEventPopTree popTree = new APIEventPopTree(apiEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);

                    }
                    break;
                case onError:
                    for (CustomOnData data : CustomOnData.values()) {
                        APIEventPopTree popTree = new APIEventPopTree(apiEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);

                    }
                    break;

                case afterInvoke:
                    for (CustomCallBack data : CustomCallBack.values()) {
                        APIEventPopTree popTree = new APIEventPopTree(apiEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);

                    }
                    break;
                case onExecuteSuccess:
                    for (CustomOnExecueSuccess data : CustomOnExecueSuccess.values()) {
                        APIEventPopTree popTree = new APIEventPopTree(apiEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);

                    }
                    break;

                case onExecuteError:
                    for (CustomOnExecueError data : CustomOnExecueError.values()) {
                        APIEventPopTree popTree = new APIEventPopTree(apiEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);

                    }
                    break;
                case callback:
                    for (CustomCallBack data : CustomCallBack.values()) {
                        APIEventPopTree popTree = new APIEventPopTree(apiEventEnum.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
                        apiEventPopTree.addChild(popTree);

                    }
                    break;
            }

        }
    }


    APIEventPopTree(String id, String caption) {
        super(id, caption);
    }

    APIEventPopTree(String id, String caption, String sourceClassName, String domainId, String methodName) {
        super(id, caption);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("domainId", domainId);
        this.addTagVar("methodName", methodName);

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

}
