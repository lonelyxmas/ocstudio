package com.ds.dsm.aggregation.api.method.service;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.api.method.event.APIEventPopTree;
import com.ds.dsm.aggregation.api.method.event.APIEventView;
import com.ds.dsm.aggregation.api.method.event.APIGridEventView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.APIEventBean;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.*;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.enums.event.enums.APIEventEnum;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/events/")
public class MethodEventService {


    @MethodChinaName(cname = "自定义事件")
    @RequestMapping(method = RequestMethod.POST, value = "CustomEvents")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-event", caption = "自定义事件")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<APIGridEventView>> getCustomEvents(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<APIGridEventView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            List<APIEventBean> events = apiCallBean.getApi().getAllEvent();
            List<APIGridEventView> eventViews = new ArrayList<>();
            for (APIEventBean apiEvent : events) {
                eventViews.add(new APIGridEventView(apiEvent, apiCallBean));
            }
            resultModel = PageUtil.getDefaultPageList(eventViews);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @FormViewAnnotation
    @ModuleAnnotation(dynLoad = true)
    @RequestMapping("EventInfo")
    @DialogAnnotation(width = "750", height = "550", caption = "事件信息")
    @APIEventAnnotation(bindMenu = CustomMenuItem.editor, autoRun = true)
    @ResponseBody
    public ResultModel<APIEventView> getEventInfo(String sourceClassName, String eventName, String domainId, String methodName) {
        ResultModel<APIEventView> model = new ResultModel<>();
        ApiClassConfig esdClassConfig = null;
        try {
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            APIEventBean eventBean = apiCallBean.getApi().getEventById(eventName);
            model.setData(new APIEventView(eventBean, apiCallBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return model;
    }


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true,caption = "常用事件")
    @RequestMapping("APIEventPopTree")
    @DialogAnnotation( width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<APIEventPopTree>> getMenuBottomTree(String sourceClassName, String domainId, String methodName) {
        TreeListResultModel<List<APIEventPopTree>> model = new TreeListResultModel<>();
        List<APIEventPopTree> popTrees = new ArrayList<>();
        APIEventPopTree menuPopTree = new APIEventPopTree(sourceClassName, domainId, methodName);
        popTrees.add(menuPopTree);
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addCustomAction")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addEvent(String sourceClassName, String APIEventPopTree, String methodName, String domainId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig callBean = esdClassConfig.getMethodByName(methodName);
            if (APIEventPopTree != null && !APIEventPopTree.equals("")) {
                String[] menuIds = StringUtility.split(APIEventPopTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        if (menuId.indexOf("|") > -1) {
                            String[] eventType = StringUtility.split(menuId, "|");
                            APIEventEnum apiEventEnum = APIEventEnum.valueOf(eventType[0]);
                            String action=eventType[1];
                            switch (apiEventEnum) {
                                case beforeData:
                                    callBean.getApi().getBeforeData().add(CustomBeforData.valueOf(action));
                                    break;
                                case onData:
                                    callBean.getApi().getOnData().add(CustomOnData.valueOf(action));
                                    break;
                                case beforeInvoke:
                                    callBean.getApi().getBeforeInvoke().add(CustomBeforInvoke.valueOf(action));
                                    break;
                                case onError:
                                    callBean.getApi().getOnError().add(CustomOnError.valueOf(action));
                                    break;
                                case afterInvoke:
                                    callBean.getApi().getCallback().add(CustomCallBack.valueOf(action));
                                    break;
                                case onExecuteSuccess:
                                    callBean.getApi().getOnExecuteSuccess().add(CustomOnExecueSuccess.valueOf(action));
                                    break;
                                case onExecuteError:
                                    callBean.getApi().getOnExecuteError().add(CustomOnExecueError.valueOf(action));
                                    break;
                            }
                        }
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


    @RequestMapping("delEvent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delEvent(String sourceClassName, String methodName, String eventName, String domainId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            String[] menuIds = StringUtility.split(eventName, ";");
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig callBean = esdClassConfig.getMethodByName(methodName);
            for (String menuId : menuIds) {
                if (menuId != null && !menuIds.equals("")) {
                    if (menuId.indexOf("|") > -1) {
                        String[] eventType = StringUtility.split(menuId, "|");
                        try {
                            APIEventEnum apiEventEnum = APIEventEnum.valueOf(eventType[0]);
                            switch (apiEventEnum) {
                                case beforeData:
                                    callBean.getApi().getBeforeData().remove(CustomBeforData.valueOf(eventType[1]));
                                    break;
                                case onData:
                                    callBean.getApi().getOnData().remove(CustomOnData.valueOf(eventType[1]));
                                    break;
                                case beforeInvoke:
                                    callBean.getApi().getBeforeInvoke().remove(CustomBeforInvoke.valueOf(eventType[1]));
                                    break;
                                case onError:
                                    callBean.getApi().getOnError().remove(CustomOnError.valueOf(eventType[1]));
                                    break;
                                case afterInvoke:
                                    callBean.getApi().getCallback().remove(CustomCallBack.valueOf(eventType[1]));
                                    break;
                                case onExecuteSuccess:
                                    callBean.getApi().getOnExecuteSuccess().remove(CustomOnExecueSuccess.valueOf(eventType[1]));
                                    break;
                                case onExecuteError:
                                    callBean.getApi().getOnExecuteError().remove(CustomOnExecueError.valueOf(eventType[1]));
                                    break;
                            }
                        } catch (Throwable e) {

                        }

                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
