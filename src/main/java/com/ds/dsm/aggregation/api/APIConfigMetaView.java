package com.ds.dsm.aggregation.api;

import com.ds.common.JDSException;
import com.ds.config.*;
import com.ds.dsm.aggregation.api.method.APIMethodBaseGridView;
import com.ds.dsm.aggregation.api.method.MethodAPITree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.enums.VAlignType;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping(path = "/dsm/aggreagtion/config/api/")
@MethodChinaName(cname = "API配置")
@ButtonViewsAnnotation(barLocation = BarLocationType.left, barVAlign = VAlignType.top)
public class APIConfigMetaView {


    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    private String url;

    public APIConfigMetaView() {

    }


    @MethodChinaName(cname = "API信息")
    @RequestMapping(method = RequestMethod.POST, value = "APIConfigBaseView")
    @FormViewAnnotation(saveUrl = "updateFormBase")
    @ModuleAnnotation(caption = "API信息", imageClass = "spafont spa-icon-c-webapi")
    @CustomAnnotation( index = 0)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<APIConfigBaseView> getAPIConfigBaseView(String domainId, String sourceClassName) {
        ResultModel<APIConfigBaseView> result = new ResultModel<APIConfigBaseView>();
        try {
            AggEntityConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
            APIConfigBaseView view = new APIConfigBaseView(tableConfig);
            result.setData(view);

        } catch (Exception e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "通用方法")
    @RequestMapping(method = RequestMethod.POST, value = "AllCustomMethod")
    @CustomAnnotation( index = 1)
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-databinder", dynLoad = true, caption = "通用方法", dock = Dock.fill)
    @ResponseBody
    public TreeListResultModel<List<MethodAPITree>> getAllCustomMethod(String sourceClassName, String domainId) {
        TreeListResultModel<List<MethodAPITree>> result = new TreeListResultModel<List<MethodAPITree>>();
        try {
            AggEntityConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
            List<MethodConfig> customMethods = tableConfig.getOtherMethodsList();
            result = TreePageUtil.getTreeList(customMethods, MethodAPITree.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;

    }

    @RequestMapping(method = RequestMethod.POST, value = "AttMethod")
    @CustomAnnotation( index = 2)
    @ModuleAnnotation(caption = "扩展方法", imageClass = "spafont spa-icon-project",dock = Dock.fill)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<APIMethodBaseGridView>> getAttMethod(String sourceClassName, String domainId) {
        ListResultModel<List<APIMethodBaseGridView>> result = new ListResultModel<List<APIMethodBaseGridView>>();
        try {
            AggEntityConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName, domainId);
            List<MethodConfig> attMethods = new ArrayList<>();
            for (MethodConfig apiBean : tableConfig.getAllMethods()) {
                if (!apiBean.isModule()) {
                    attMethods.add(apiBean);
                }
            }
            result = PageUtil.getDefaultPageList(attMethods, APIMethodBaseGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
