package com.ds.dsm.aggregation.api.method;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.ListAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.component.data.APICallerProperties;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.json.EnumSetDeserializer;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.LinkedHashSet;
import java.util.Set;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload})
public class APIMethodBaseGridView {

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, uid = true)
    String sourceClassName;

    @CustomAnnotation(caption = "方法名称")
    String methodName;

    @ComboInputAnnotation(inputType = ComboInputType.spin)
    @FieldAnnotation(componentType = ComponentType.ComboInput)
    @CustomAnnotation(caption = "显示顺序")
    public Integer index;


    @CustomAnnotation(caption = "JSON格式")
    Boolean responseBody;

    @CustomAnnotation(caption = "异步调用")
    public Boolean queryAsync;

    @CustomAnnotation(caption = "自动运行")
    public Boolean autoRun;

    @CustomAnnotation(caption = "级联处理")
    public Boolean allform;


    @JSONField(deserializeUsing = EnumSetDeserializer.class)
    @FieldAnnotation(colSpan = -1, rowHeight = "100", componentType = ComponentType.List)
    @ListAnnotation(selMode = SelModeType.singlecheckbox)
    @CustomAnnotation(caption = "HttpMethod")
    Set<RequestMethod> method = new LinkedHashSet<>();

    @FieldAnnotation(colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "访问地址")
    String url;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "接口信息", readonly = true)
    String methodInfo;

    @FieldAnnotation(componentType = ComponentType.JavaEditor, haslabel = false, colSpan = -1, rowHeight = "100")
    @CustomAnnotation(caption = "模板内容")
    String methodBody;


    public APIMethodBaseGridView() {

    }


    public APIMethodBaseGridView(MethodConfig methodAPICallBean) {
        APICallerProperties apiCallerProperties = methodAPICallBean.getApi().getApiCallerProperties();
        this.autoRun = apiCallerProperties.getAutoRun();
        this.allform = apiCallerProperties.getAllform();
        this.queryAsync = apiCallerProperties.getQueryAsync();
        this.url = apiCallerProperties.getQueryURL();
        this.method = methodAPICallBean.getRequestMapping().getMethod();
        this.methodBody = methodAPICallBean.getBody();
        this.responseBody = methodAPICallBean.getResponseBody();

        this.methodName = methodAPICallBean.getMethodName();
        this.viewInstId = methodAPICallBean.getViewInstId();
        this.domainId = methodAPICallBean.getDomainId();
        this.index = methodAPICallBean.getApi().getIndex();
        this.sourceClassName = methodAPICallBean.getSourceClassName();
        this.methodInfo = methodAPICallBean.getMetaInfo();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public Set<RequestMethod> getMethod() {
        return method;
    }

    public void setMethod(Set<RequestMethod> method) {
        this.method = method;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }


    public Boolean getQueryAsync() {
        return queryAsync;
    }

    public void setQueryAsync(Boolean queryAsync) {
        this.queryAsync = queryAsync;
    }

    public Boolean getAutoRun() {
        return autoRun;
    }

    public void setAutoRun(Boolean autoRun) {
        this.autoRun = autoRun;
    }

    public Boolean getAllform() {
        return allform;
    }

    public void setAllform(Boolean allform) {
        allform = allform;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethodInfo() {
        return methodInfo;
    }

    public void setMethodInfo(String methodInfo) {
        this.methodInfo = methodInfo;
    }

    public String getMethodBody() {
        return methodBody;
    }

    public void setMethodBody(String methodBody) {
        this.methodBody = methodBody;
    }

    public Boolean getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(Boolean responseBody) {
        this.responseBody = responseBody;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
