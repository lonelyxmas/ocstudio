package com.ds.dsm.aggregation.api.method.action.condition;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.component.event.Condition;
import com.ds.esd.tool.ui.enums.SymbolType;

@FormAnnotation(col = 3, customService = {ConditionService.class}, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@BottomBarMenu
public class ConditionView {

    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;
    @CustomAnnotation(hidden = true, pid = true)
    String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    String actionId;
    @CustomAnnotation(hidden = true, uid = true)
    String conditionId;

    @CustomAnnotation(caption = "条件")
    String left;

    @CustomAnnotation(caption = "类型")
    SymbolType symbol;

    @CustomAnnotation(caption = "条件")
    String right;

    @FieldAnnotation( colSpan = -1, rowHeight = "100")
    @CustomAnnotation(caption = "表达式")
    String expression;

    public ConditionView() {

    }

    public ConditionView(Condition condition, Action action, MethodConfig methodConfig) {
        this.expression = condition.expression();
        this.symbol = condition.symbol();
        this.right = condition.right();
        this.left = condition.left();
        this.actionId = action.getId();
        this.methodName = methodConfig.getMethodName();
        this.sourceClassName = methodConfig.getSourceClassName();
        this.domainId = methodConfig.getDomainId();
        this.conditionId = condition.getConditionId();
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getConditionId() {
        return conditionId;
    }

    public void setConditionId(String conditionId) {
        this.conditionId = conditionId;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public SymbolType getSymbol() {
        return symbol;
    }

    public void setSymbol(SymbolType symbol) {
        this.symbol = symbol;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

}

