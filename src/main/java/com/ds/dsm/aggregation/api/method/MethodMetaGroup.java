package com.ds.dsm.aggregation.api.method;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.dsm.aggregation.api.method.action.ActionGirdView;
import com.ds.dsm.aggregation.api.method.bind.APIBindMenuView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.NavTreeAnnotation;
import com.ds.esd.custom.api.APIEventBean;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/")
@NavTreeAnnotation
public class MethodMetaGroup {
    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(hidden = true)
    private String url;

    @CustomAnnotation(pid = true, hidden = true)
    private String methodName;

    public MethodMetaGroup() {

    }

    public MethodMetaGroup(String methodName) {
        this.methodName = methodName;
    }

    @MethodChinaName(cname = "绑定按钮")
    @RequestMapping(method = RequestMethod.POST, value = "BindMenus")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-statusbutton", caption = "按钮事件")
    @CustomAnnotation( index = 0)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<APIBindMenuView>> getBindMenus(String sourceClassName, String domainId, String methodName) {
        ListResultModel<List<APIBindMenuView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            List<APIBindMenuView> menuViews = new ArrayList<>();
            if (apiCallBean.getApi() != null) {
                for (CustomMenuItem item : apiCallBean.getApi().getBindMenu()) {
                    menuViews.add(new APIBindMenuView(item, apiCallBean));
                }
            }

            resultModel = PageUtil.getDefaultPageList(menuViews);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "调用事件")
    @RequestMapping(method = RequestMethod.POST, value = "CustomActions")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-event", caption = "调用事件")
    @CustomAnnotation( index = 1)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<ActionGirdView>> getCustomActions(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<ActionGirdView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            List<APIEventBean> events = apiCallBean.getApi().getAllEvent();
            List<Action> actions = new ArrayList<>();
            List<ActionGirdView> actionViews = new ArrayList<>();
            for (APIEventBean apiEvent : events) {
                for (Action action : apiEvent.getActions()) {
                    actionViews.add(new ActionGirdView(action, apiCallBean));
                }
            }

            resultModel = PageUtil.getDefaultPageList(actions, ActionGirdView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @MethodChinaName(cname = "JavaScript脚本")
    @RequestMapping(method = RequestMethod.POST, value = "JSActions")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-event", caption = "JavaScript脚本")
    @CustomAnnotation( index = 2)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<ActionGirdView>> getJSActions(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<ActionGirdView>> resultModel = new ListResultModel<>();

        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Set<Action> customActions = apiCallBean.getApi().getBindAction();
            List<ActionGirdView> actionViews = new ArrayList<>();
            for (Action customAction : customActions) {
                ActionGirdView action = new ActionGirdView(customAction, apiCallBean);
                actionViews.add(action);
            }
            resultModel = PageUtil.getDefaultPageList(actionViews);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassNamee(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
