package com.ds.dsm.aggregation.api.method.event;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.api.method.action.ActionGirdView;
import com.ds.dsm.aggregation.api.method.service.CustomActionService;
import com.ds.dsm.aggregation.api.method.service.MethodEventService;
import com.ds.esd.custom.annotation.ComboModuleAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.APIEventBean;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.AppendType;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.event.enums.APIEventEnum;
import com.ds.web.annotation.Uid;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping(path = "/dsm/agg/api/config/events/")
@BottomBarMenu
@FormAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = MethodEventService.class)
public class APIEventView {

    String id;
    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;
    @CustomAnnotation(hidden = true, pid = true)
    String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @Uid
    String eventId;

    @CustomAnnotation(caption = "事件名称", uid = true)
    String eventName;

    @CustomAnnotation(caption = "监听事件")
    String eventKey;

    @CustomAnnotation(caption = "描述")
    public String desc;


    @CustomAnnotation(caption = "表达式")
    public String expression;

    @FieldAnnotation(colSpan = -1, rowHeight = "150", componentType = ComponentType.CodeEditor)
    @CustomAnnotation(caption = "脚本")
    public String script;

    @ComboModuleAnnotation(bindClass = CustomActionService.class,append = AppendType.ref)
    @FieldAnnotation(haslabel = false, colSpan = -1)
    @CustomAnnotation(caption = "执行动作")
    List<ActionGirdView> actions;


    public APIEventView() {

    }


    public APIEventView(APIEventBean event, MethodConfig methodConfig) {
        this.methodName = methodConfig.getMethodName();
        this.domainId = methodConfig.getDomainId();
        this.sourceClassName = methodConfig.getSourceClassName();
        this.expression = event.getEventReturn();
        this.script = event.getScript();
        this.desc = event.getDesc();
        this.eventName = event.getEventKey().getEvent();
        this.eventKey = event.getEventKey().getEvent();
        this.eventId = event.getEventId();

    }

    public APIEventView(APIEventEnum event) {

        this.eventKey = event.getEvent();
        this.desc = event.getName();
        this.eventName = event.name();


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<ActionGirdView> getActions() {
        return actions;
    }

    public void setActions(List<ActionGirdView> actions) {
        this.actions = actions;
    }
}
