package com.ds.dsm.aggregation.api.method.parameter;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.dsm.aggregation.api.method.parameter.custom.CustomRequestParameterGridView;
import com.ds.dsm.aggregation.api.method.parameter.custom.CustomResponseParameterGridView;
import com.ds.dsm.aggregation.api.method.parameter.method.MethodParameterGridView;
import com.ds.dsm.aggregation.api.method.parameter.request.RequestParameterGridView;
import com.ds.dsm.aggregation.api.method.parameter.response.ResponseParameterGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.NavTreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.api.RequestPathBean;
import com.ds.esd.custom.api.ResponsePathBean;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.api.enums.ResponsePathEnum;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.web.RequestParamBean;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/")
@NavTreeAnnotation
public class MethodParamsMetaGroup {
    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;


    @CustomAnnotation(pid = true, hidden = true)
    private String methodName;

    @CustomAnnotation(hidden = true)
    private String url;


    public MethodParamsMetaGroup() {

    }

    public MethodParamsMetaGroup(String methodName) {
        this.methodName = methodName;
    }

    @MethodChinaName(cname = "方法参数")
    @RequestMapping(method = RequestMethod.POST, value = "MethodParameters")
    @GridViewAnnotation
    @ModuleAnnotation()
    @TreeItemAnnotation(index = 1,imageClass = "spafont spa-icon-c-databinder", caption = "接口参数")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<MethodParameterGridView>> getMethodParameters(String sourceClassName, String domainId, String methodName) {
        ListResultModel<List<MethodParameterGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            List<MethodParameterGridView> parameterGridViews = new ArrayList<>();
            Set<RequestParamBean> paramBeans = apiCallBean.getParamSet();
            if (apiCallBean.getApi() != null) {
                for (RequestParamBean item : paramBeans) {
                    parameterGridViews.add(new MethodParameterGridView(item, apiCallBean));
                }
            }
            resultModel = PageUtil.getDefaultPageList(parameterGridViews);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "常用请求")
    @RequestMapping(method = RequestMethod.POST, value = "CustomRequestParameters")
    @GridViewAnnotation
    @ModuleAnnotation()
    @TreeItemAnnotation(imageClass = "spafont spa-icon-com3", caption = "常用请求", index = 2)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<CustomRequestParameterGridView>> getCustomRequestParameters(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<CustomRequestParameterGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Set<RequestPathEnum> requestDataSource = apiCallBean.getApi().getCustomRequestData();
            List<CustomRequestParameterGridView> parameterGridViews = new ArrayList<>();
            if (apiCallBean.getApi() != null) {
                for (RequestPathEnum item : requestDataSource) {
                    parameterGridViews.add(new CustomRequestParameterGridView(item, apiCallBean));
                }
            }
            resultModel = PageUtil.getDefaultPageList(parameterGridViews);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @MethodChinaName(cname = "常用映射")
    @RequestMapping(method = RequestMethod.POST, value = "CustomResponseParameter")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-project", caption = "常用映射")
    @CustomAnnotation( index = 0)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<CustomResponseParameterGridView>> getCustomResponseParameter(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<CustomResponseParameterGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Set<ResponsePathEnum> responseDataTarget = apiCallBean.getApi().getCustomResponseData();
            List<CustomResponseParameterGridView> parameterGridViews = new ArrayList<>();
            if (apiCallBean.getApi() != null) {
                for (ResponsePathEnum item : responseDataTarget) {
                    parameterGridViews.add(new CustomResponseParameterGridView(item, apiCallBean));
                }
            }
            resultModel = PageUtil.getDefaultPageList(parameterGridViews);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "自定义请求")
    @RequestMapping(method = RequestMethod.POST, value = "RequestParameters")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf", caption = "自定义请求")
    @CustomAnnotation( index = 1)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<RequestParameterGridView>> getRequestParameters(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<RequestParameterGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Set<RequestPathBean> requestDataSource = apiCallBean.getApi().getRequestDataSource();
            List<RequestParameterGridView> parameterGridViews = new ArrayList<>();
            if (apiCallBean.getApi() != null) {
                for (RequestPathBean item : requestDataSource) {
                    parameterGridViews.add(new RequestParameterGridView(item, apiCallBean));
                }
            }
            resultModel = PageUtil.getDefaultPageList(parameterGridViews);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @MethodChinaName(cname = "自定义映射")
    @RequestMapping(method = RequestMethod.POST, value = "BindViews")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-project", caption = "自定义映射")
    @CustomAnnotation( index = 0)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<ResponseParameterGridView>> getBindViews(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<ResponseParameterGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Set<ResponsePathBean> responseDataTarget = apiCallBean.getApi().getResponseDataTarget();
            List<ResponseParameterGridView> parameterGridViews = new ArrayList<>();
            if (apiCallBean.getApi() != null) {
                for (ResponsePathBean item : responseDataTarget) {
                    parameterGridViews.add(new ResponseParameterGridView(item, apiCallBean));
                }
            }
            resultModel = PageUtil.getDefaultPageList(parameterGridViews);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassNamee(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
