package com.ds.dsm.aggregation.api.method.action.custom;

import com.ds.dsm.aggregation.api.method.service.CustomActionService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.enums.event.ActionTypeEnum;
import com.ds.web.annotation.DynCheck;
import com.ds.web.annotation.Required;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Add, GridMenu.Delete, GridMenu.Reload}, event = CustomGridEvent.editor, customService = CustomActionService.class)
public class CustomActionGirdView {


    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;
    @CustomAnnotation(hidden = true, pid = true)
    String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, uid = true)
    String actionId;

    @CustomAnnotation(caption = "事件")
    ActionTypeEnum type;

    @Required
    @DynCheck
    @CustomAnnotation(caption = "方法")
    String method;

    @CustomAnnotation(caption = "源目标")
    String target;

    @CustomAnnotation(caption = "执行目标")
    String redirection;

    @CustomAnnotation(caption = "执行终止")
    Boolean canReturn;

    @CustomAnnotation(caption = "成功", hidden = true)
    String okFlag;
    @CustomAnnotation(caption = "放弃", hidden = true)
    String koFlag;

    @CustomAnnotation(caption = "模块", hidden = true)
    String className;
    @CustomAnnotation(caption = "表达式", hidden = true)
    String expression;


    public CustomActionGirdView(Action action, MethodConfig config) {
        this.methodName = config.getMethodName();
        this.sourceClassName = config.getSourceClassName();
        this.domainId = config.getDomainId();
        this.type = action.getType();
        this.target = action.getTarget();
        this.className = action.getClassName();
        this.expression = action.getExpression();
        this.canReturn = action.get_return();
        this.method = action.getMethod();
        this.redirection = action.getRedirection();
        this.okFlag = action.getOkFlag();
        this.koFlag = action.getKoFlag();
        this.actionId = action.getId();
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public ActionTypeEnum getType() {
        return type;
    }

    public void setType(ActionTypeEnum type) {
        this.type = type;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getRedirection() {
        return redirection;
    }

    public void setRedirection(String redirection) {
        this.redirection = redirection;
    }

    public String getOkFlag() {
        return okFlag;
    }

    public void setOkFlag(String okFlag) {
        this.okFlag = okFlag;
    }

    public String getKoFlag() {
        return koFlag;
    }

    public void setKoFlag(String koFlag) {
        this.koFlag = koFlag;
    }

    public Boolean getCanReturn() {
        return canReturn;
    }

    public void setCanReturn(Boolean canReturn) {
        this.canReturn = canReturn;
    }
}
