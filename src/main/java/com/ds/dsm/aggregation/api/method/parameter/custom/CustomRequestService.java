package com.ds.dsm.aggregation.api.method.parameter.custom;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/parameters/request/custom/")
public class CustomRequestService {


    @MethodChinaName(cname = "常用参数")
    @RequestMapping(method = RequestMethod.POST, value = "CustomRequestParams")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-event", caption = "常用参数")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<CustomRequestParameterGridView>> getCustomRequestParams(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<CustomRequestParameterGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            Set<RequestPathEnum> pathEnums = apiCallBean.getApi().getCustomRequestData();
            List<CustomRequestParameterGridView> eventViews = new ArrayList<>();
            for (RequestPathEnum pathEnum : pathEnums) {
                eventViews.add(new CustomRequestParameterGridView(pathEnum, apiCallBean));
            }
            resultModel = PageUtil.getDefaultPageList(eventViews);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "常用事件")
    @RequestMapping("CustomRequstPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<CustomRequestParamsPopTree>> getCustomRequstTree(String sourceClassName, String domainId, String methodName) {
        TreeListResultModel<List<CustomRequestParamsPopTree>> model = new TreeListResultModel<>();
        List<CustomRequestParamsPopTree> popTrees = new ArrayList<>();
        CustomRequestParamsPopTree menuPopTree = new CustomRequestParamsPopTree(sourceClassName, domainId, methodName);
        popTrees.add(menuPopTree);
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addParams")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addParams(String sourceClassName, String CustomRequstPopTree, String methodName, String domainId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig callBean = esdClassConfig.getMethodByName(methodName);
            if (CustomRequstPopTree != null && !CustomRequstPopTree.equals("")) {
                String[] menuIds = StringUtility.split(CustomRequstPopTree, ";");
                for (String menuId : menuIds) {
                    callBean.getApi().getCustomRequestData().add(RequestPathEnum.valueOf(menuId));
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


    @RequestMapping("delParams")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delParams(String sourceClassName, String methodName, String type, String domainId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            String[] menuIds = StringUtility.split(type, ";");
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig callBean = esdClassConfig.getMethodByName(methodName);
            for (String menuId : menuIds) {
                callBean.getApi().getCustomRequestData().remove(RequestPathEnum.valueOf(menuId));
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
