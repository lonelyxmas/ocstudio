package com.ds.dsm.aggregation.api.method.callback;

import com.ds.dsm.aggregation.api.method.service.MethodEventService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.event.enums.APIEventEnum;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = {MethodEventService.class}, bottombarMenu = {CustomFormMenu.Save})
@PopTreeAnnotation(caption = "API事件")
public class APICallBackPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String euClassName;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String methodName;


    public APICallBackPopTree(String sourceClassName, String domainId, String methodName) {
        super("allApiEvent", "API事件");
        APICallBackPopTree apiEventPopTree = new APICallBackPopTree(APIEventEnum.callback.name(), APIEventEnum.callback.getName());
        apiEventPopTree.setImageClass(APIEventEnum.callback.getImageClass());
        this.addChild(apiEventPopTree);
        for (CustomCallBack data : CustomCallBack.values()) {
            APICallBackPopTree popTree = new APICallBackPopTree(APIEventEnum.callback.name() + "|" + data.name(), data.getDesc(), sourceClassName, domainId, methodName);
            apiEventPopTree.addChild(popTree);
        }
    }


    APICallBackPopTree(String id, String caption) {
        super(id, caption);
    }

    APICallBackPopTree(String id, String caption, String sourceClassName, String domainId, String methodName) {
        super(id, caption);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("domainId", domainId);
        this.addTagVar("methodName", methodName);

    }

    @Override
    public String getEuClassName() {
        return euClassName;
    }

    @Override
    public void setEuClassName(String euClassName) {
        this.euClassName = euClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

}
