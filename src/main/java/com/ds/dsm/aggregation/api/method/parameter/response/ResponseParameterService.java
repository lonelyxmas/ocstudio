package com.ds.dsm.aggregation.api.method.parameter.response;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.ResponsePathBean;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/api/config/parameters/response/")
@MethodChinaName(cname = "绑定组件", imageClass = "spafont spa-icon-tools")
public class ResponseParameterService {
    @RequestMapping(method = RequestMethod.POST, value = "AddParameters")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add})
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350")
    @ModuleAnnotation( caption = "添加参数")
    @ResponseBody
    public ResultModel<ResponseParameterFormView> addParamter(String methodName, String sourceClassName, String domainId) {
        ResultModel<ResponseParameterFormView> result = new ResultModel<ResponseParameterFormView>();
        ResponsePathBean parameter = new ResponsePathBean();
        parameter.setDomainId(domainId);
        parameter.setParamsname("arg");
        parameter.setMethodName(methodName);
        parameter.setSourceClassName(sourceClassName);
        result.setData(new ResponseParameterFormView(parameter));

        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ParameterInfo")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor})
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350")
    @ModuleAnnotation( caption = "编辑参数")
    @ResponseBody
    public ResultModel<ResponseParameterFormView> getParamterInfo(String methodName, String sourceClassName, String domainId, String paramName) {
        ResultModel<ResponseParameterFormView> result = new ResultModel<ResponseParameterFormView>();

        ApiClassConfig esdClassConfig = null;
        try {
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            ResponsePathBean parameter = apiCallBean.getApi().getResponseParamByName(paramName);
            apiCallBean.getApi().updateResponseParam(parameter);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;

    }


    @MethodChinaName(cname = "保存参数")
    @RequestMapping(method = RequestMethod.POST, value = "saveParameters")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save})
    @ResponseBody
    public ResultModel<Boolean> saveParamter(@RequestBody ResponsePathBean parameter) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();

        ApiClassConfig esdClassConfig = null;
        try {
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(parameter.getSourceClassName(), parameter.getDomainId());
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(parameter.getMethodName());
            apiCallBean.getApi().updateResponseParam(parameter);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


    @MethodChinaName(cname = "删除参数")
    @RequestMapping(method = RequestMethod.POST, value = "delParameters")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = {CustomMenuItem.delete})
    @ResponseBody
    public ResultModel<Boolean> deleteParams(String paramsname, String methodName, String sourceClassName, String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ApiClassConfig esdClassConfig = null;
        try {
            esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig apiCallBean = esdClassConfig.getMethodByName(methodName);
            ResponsePathBean parameter = apiCallBean.getApi().getResponseParamByName(paramsname);
            apiCallBean.getApi().getResponseDataTarget().remove(parameter);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
