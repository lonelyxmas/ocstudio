package com.ds.dsm.aggregation;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.enums.*;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.tool.ui.component.list.TreeListItem;

import java.util.List;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, caption = "JAVA模板")
public class CustomDomainTree extends TreeListItem {
    public CustomDomainTree(String domainId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = "模板分类";
        this.id = "ViewRoot";
        this.euClassName = "dsm.manager.temp.AllJavaTemp";
        this.iniFold = false;
        this.imageClass = "spafont spa-icon-settingprj";
        CustomDomainType[] customDomainTypes = CustomDomainType.values();
        for (CustomDomainType customDomainType : customDomainTypes) {
            if (hasTemp) {
                List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(customDomainType);
                if (javaTemps.size() > 0) {
                    CustomDomainTree dsmInstItem = new CustomDomainTree(customDomainType, domainId, sourceClassName, hasTemp);
                    this.addChild(dsmInstItem);
                }
            } else {
                this.addChild(new CustomDomainTree(customDomainType, domainId, sourceClassName, hasTemp));
            }
        }
    }

    public CustomDomainTree(CustomDomainType customDomainType, String domainId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.iniFold = false;
        this.euClassName = "dsm.manager.temp.AllJavaTempGrid";
        this.caption = customDomainType.getName();
        this.imageClass = customDomainType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + customDomainType.getType();

        this.addTagVar("domainId", domainId);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("dsmType", DSMType.customDomain.getType());
        this.addTagVar("customDomainType", customDomainType.getType());

        switch (customDomainType) {
            case bpm:
                BpmDomainType[] types = BpmDomainType.values();
                for (BpmDomainType bpmDomainType : types) {
                    this.addChild(new CustomDomainTree(bpmDomainType, domainId, sourceClassName, hasTemp));
                }
                break;
            case msg:
                MsgDomainType[] msgtypes = MsgDomainType.values();
                for (MsgDomainType type : msgtypes) {
                    this.addChild(new CustomDomainTree(type, domainId, sourceClassName, hasTemp));
                }
                break;
            case org:
                OrgDomainType[] orgtypes = OrgDomainType.values();
                for (OrgDomainType type : orgtypes) {
                    this.addChild(new CustomDomainTree(type, domainId, sourceClassName, hasTemp));
                }
                break;
            case nav:
                NavDomainType[] navtypes = NavDomainType.values();
                for (NavDomainType type : navtypes) {
                    this.addChild(new CustomDomainTree(type, domainId, sourceClassName, hasTemp));
                }
                break;
        }

    }


    public CustomDomainTree(BpmDomainType customDomainType, String domainId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = customDomainType.getName();
        this.imageClass = customDomainType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + CustomDomainType.bpm + "_" + customDomainType.getType();
        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(CustomDomainType.bpm);
            if (javaTemps.size() > 0) {
                for (JavaTemp javaTemp : javaTemps) {
                    CustomDomainTree dsmInstItem = new CustomDomainTree(javaTemp, sourceClassName, domainId);
                    this.addChild(dsmInstItem);
                }
                this.setIniFold(true);
            }
        }
        this.addTagVar("domainId", domainId);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("dsmType", DSMType.customDomain.getType());
        this.addTagVar("customDomainType", customDomainType.getType());
    }

    public CustomDomainTree(NavDomainType customDomainType, String dsmId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = customDomainType.getName();
        this.imageClass = customDomainType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + CustomDomainType.nav + "_" + customDomainType.getType();
        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(CustomDomainType.nav);
            if (javaTemps.size() > 0) {
                for (JavaTemp javaTemp : javaTemps) {
                    CustomDomainTree dsmInstItem = new CustomDomainTree(javaTemp, sourceClassName, dsmId);
                    this.addChild(dsmInstItem);
                }
                this.setIniFold(true);
            }
        }
        this.addTagVar("dsmId", dsmId);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("dsmType", DSMType.customDomain.getType());
        this.addTagVar("customDomainType", customDomainType.getType());
    }

    public CustomDomainTree(OrgDomainType customDomainType, String domainId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = customDomainType.getName();
        this.imageClass = customDomainType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + CustomDomainType.org + "_" + customDomainType.getType();
        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(CustomDomainType.org);
            if (javaTemps.size() > 0) {
                for (JavaTemp javaTemp : javaTemps) {
                    CustomDomainTree dsmInstItem = new CustomDomainTree(javaTemp, sourceClassName, domainId);
                    this.addChild(dsmInstItem);
                }
                this.setIniFold(true);
            }
        }
        this.addTagVar("aggregationType", customDomainType.getType());
        this.addTagVar("domainId", domainId);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("dsmType", DSMType.customDomain.getType());
        this.addTagVar("customDomainType", customDomainType.getType());
    }

    public CustomDomainTree(MsgDomainType customDomainType, String dsmId, String sourceClassName, boolean hasTemp) throws JDSException {
        this.caption = customDomainType.getName();
        this.imageClass = customDomainType.getImageClass();
        this.id = DSMType.customDomain.getType() + "_" + CustomDomainType.msg + "_" + customDomainType.getType();
        if (hasTemp) {
            List<JavaTemp> javaTemps = DSMFactory.getInstance().getTempManager().getCustomDomainTemps(CustomDomainType.msg);
            if (javaTemps.size() > 0) {
                for (JavaTemp javaTemp : javaTemps) {
                    CustomDomainTree dsmInstItem = new CustomDomainTree(javaTemp, sourceClassName, dsmId);
                    this.addChild(dsmInstItem);
                }
                this.setIniFold(true);
            }
        }

        this.addTagVar("dsmId", dsmId);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("dsmType", DSMType.customDomain.getType());
        this.addTagVar("customDomainType", customDomainType.getType());
    }


    public CustomDomainTree(JavaTemp javaTemp, String sourceClassName, String dsmId) {
        this.caption = javaTemp.getName();
        this.imageClass = "spafont spa-icon-page";
        this.id = javaTemp.getJavaTempId();
        this.addTagVar("domainType", javaTemp.getCustomDomainType().getType());
        this.addTagVar("dsmId", dsmId);
        this.addTagVar("sourceClassName", sourceClassName);
        this.addTagVar("javaTempId", javaTemp.getJavaTempId());
    }

}
