package com.ds.dsm.aggregation.event;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.gallery.enums.CustomGalleryEvent;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping(path = "/dsm/view/config/gallery/")
public class GalleryEventService {


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "列表事件")
    @RequestMapping("CustomGalleryEventTree")
    @DialogAnnotation( width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<GalleryEventPopTree>> getCustomGalleryEventTree(String sourceClassName, String domainId, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<GalleryEventPopTree>> model = new TreeListResultModel<>();
        List<GalleryEventPopTree> popTrees = new ArrayList<>();
        try {
            GalleryEventPopTree eventPopTree = new GalleryEventPopTree("CustomGalleryEvent", "常用事件");
            for (CustomGalleryEvent CustomGalleryEvent : CustomGalleryEvent.values()) {
                GalleryEventPopTree item = new GalleryEventPopTree(esdsearchpattern, CustomGalleryEvent);
                item.addTagVar("sourceClassName", sourceClassName);
                item.addTagVar("domainId", domainId);
                item.addTagVar("viewInstId", viewInstId);
                eventPopTree.addChild(item);
            }
            popTrees.add(eventPopTree);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addEvent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addEvent(String sourceClassName, String methodName, String CustomGalleryEventTree, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            CustomGalleryViewBean galleryViewBean = (CustomGalleryViewBean) tableConfig.getSourceConfig().getMethodByName(methodName).getView();
            if (CustomGalleryEventTree != null && !CustomGalleryEventTree.equals("")) {
                String[] menuIds = StringUtility.split(CustomGalleryEventTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        galleryViewBean.getEvent().add(CustomGalleryEvent.valueOf(menuId));
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig.getSourceConfig());
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


    @RequestMapping("delEvent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delEvent(String sourceClassName, String methodName, String eventName, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId);
            CustomGalleryViewBean galleryViewBean = (CustomGalleryViewBean) tableConfig.getSourceConfig().getMethodByName(methodName).getView();
            String[] menuIds = StringUtility.split(eventName, ";");
            for (String menuId : menuIds) {
                galleryViewBean.getEvent().remove(CustomGalleryEvent.valueOf(menuId));
            }
            DSMFactory.getInstance().getViewManager().updateViewEntityConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
