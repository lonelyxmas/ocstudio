package com.ds.dsm.aggregation.event;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.foldingtree.NavFoldingTreeViewBean;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.pop.PopTreeViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.custom.tree.enums.CustomTreeEvent;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping(path = "/dsm/view/config/tree/")
public class TreeEventService {


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "列表事件")
    @RequestMapping("TreeEventTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<TreeEventPopTree>> getTreeEventTree(String sourceClassName, String domainId, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<TreeEventPopTree>> model = new TreeListResultModel<>();
        List<TreeEventPopTree> popTrees = new ArrayList<>();
        try {
            TreeEventPopTree eventPopTree = new TreeEventPopTree("TreeEvent", "常用事件");
            for (CustomTreeEvent treeEvent : CustomTreeEvent.values()) {
                TreeEventPopTree item = new TreeEventPopTree(esdsearchpattern, treeEvent);
                item.addTagVar("euClassName", sourceClassName);
                item.addTagVar("viewInstId", viewInstId);
                item.addTagVar("domainId", domainId);
                eventPopTree.addChild(item);
            }
            popTrees.add(eventPopTree);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addEvent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addEvent(String sourceClassName, String methodName, String TreeEventTree, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {

            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = config.getMethodByName(methodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }

            if (TreeEventTree != null && !TreeEventTree.equals("")) {
                String[] menuIds = StringUtility.split(TreeEventTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        customTreeViewBean.getEvent().add(CustomTreeEvent.valueOf(menuId));
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


    @RequestMapping("delEvent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delEvent(String sourceClassName, String methodName, String eventName, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                NavTreeViewBean baseViewBean = (NavTreeViewBean) customMethodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean baseViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeViewBean();
            } else if (customMethodAPIBean.getView() instanceof PopTreeViewBean) {
                PopTreeViewBean baseViewBean = (PopTreeViewBean) customMethodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeViewBean();
            } else if (customMethodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) customMethodAPIBean.getView();
            }
            String[] menuIds = StringUtility.split(eventName, ";");
            for (String menuId : menuIds) {
                customTreeViewBean.getEvent().remove(CustomTreeEvent.valueOf(menuId));
            }
            DSMFactory.getInstance().getViewManager().updateViewEntityConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
