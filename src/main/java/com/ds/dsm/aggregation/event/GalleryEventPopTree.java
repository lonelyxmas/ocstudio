package com.ds.dsm.aggregation.event;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.enums.CustomGalleryEvent;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = {GridEventService.class}, bottombarMenu = {CustomFormMenu.Close, CustomFormMenu.Save})
@PopTreeAnnotation(caption = "添加事件")
public class GalleryEventPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String euClassName;

    @CustomAnnotation(pid = true)
    String viewInstId;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String bar;


    public GalleryEventPopTree(String id, String caption) {
        super(id, caption);

    }

    public GalleryEventPopTree(String pattern, CustomGalleryEvent event) throws JDSException {
        super(event.name(), event.getEventEnum().getEvent() + "(" + event.getName() + ")");

    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }

}
