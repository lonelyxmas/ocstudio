package com.ds.dsm.editor.agg.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityConfigTree;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.dsm.editor.agg.JavaAggEditor;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/java/agg/")
public class AggFileService {


    @RequestMapping(method = RequestMethod.POST, value = "AggJavaEditor")
    @ModuleAnnotation(caption = "JAVA树", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @FormViewAnnotation()
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<JavaAggEditor> getAggJavaEditor(String filePath, String domainId, String javaTempId, String sourceClassName, String methodName) {
        ResultModel<JavaAggEditor> result = new ResultModel<>();
        File desFile = new File(filePath);
        JavaSrcBean srcBean = null;
        try {
            DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            srcBean = DSMFactory.getInstance().getTempManager().genJavaSrc(desFile, domainInst, javaTempId);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        if (sourceClassName != null && !sourceClassName.equals("")) {
            srcBean.setSourceClassName(sourceClassName);
        }
        if (methodName != null && !methodName.equals("")) {
            srcBean.setMethodName(methodName);
        }

        JavaAggEditor editor = new JavaAggEditor(srcBean);
        result.setData(editor);
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadEntity")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggEntityConfigTree>> loadEntity(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggEntityConfigTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(AggEntityNavItem.values()), AggEntityConfigTree.class);
        return result;
    }


}
