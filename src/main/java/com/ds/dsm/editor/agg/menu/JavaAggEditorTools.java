package com.ds.dsm.editor.agg.menu;

import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.editor.action.BaseEditorTools;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = {"/java/agg/editor/"})
@MenuBarMenu(menuType = CustomMenuType.toolbar, caption = "菜单", hAlign = HAlignType.right, id = "JavaAggEditorTools")
@Aggregation(type = AggregationType.menu,rootClass =JavaAggEditorTools.class )
public class JavaAggEditorTools extends BaseEditorTools {

    @MethodChinaName(cname = "")
    @RequestMapping(method = RequestMethod.POST, value = "saveJava")
    @ComboInputAnnotation(inputType = ComboInputType.button)
    @CustomAnnotation(index = 0, caption = "保存", tips = "保存", imageClass = "spafont spa-icon-save")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.form, RequestPathEnum.ctx})
    public @ResponseBody
    ResultModel<Boolean> saveJava(String content, String projectName, DSMType dsmType, String dsmId, String javaTempId, String filePath) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            DSMFactory.getInstance().getBuildFactory().updateJava(content, dsmType, dsmId, javaTempId, filePath);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


}
