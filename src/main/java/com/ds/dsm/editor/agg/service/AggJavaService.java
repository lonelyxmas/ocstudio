package com.ds.dsm.editor.agg.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.editor.agg.JavaAggTree;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@RequestMapping("/java/agg/")
public class AggJavaService {


    @RequestMapping(method = RequestMethod.POST, value = "JavaAggTree")
    @ModuleAnnotation(dynLoad = true, caption = "资源域", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @NavTreeViewAnnotation()
    @ResponseBody
    public TreeListResultModel<List<JavaAggTree>> getJavaAggTree(String projectVersionName, String parentId) {
        TreeListResultModel<List<JavaAggTree>> result = new TreeListResultModel<>();
        try {
            List<DomainInst> domainInsts = DSMFactory.getInstance().getAggregationManager().getDomainInstList(projectVersionName);
            result = TreePageUtil.getTreeList(domainInsts, JavaAggTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "uploadAgg")
    @APIEventAnnotation(bindMenu = CustomMenuItem.upload)
    @CustomAnnotation(caption = "上传文件")
    public @ResponseBody
    ResultModel<Boolean> addDocument(String domainId, String packageName, @RequestParam("files") MultipartFile file) {
        ResultModel<Boolean> resultModel = new ResultModel<Boolean>();
        DomainInst domainInst = null;

        return resultModel;

    }


    @RequestMapping(method = RequestMethod.POST, value = "loadDomainPackage")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaAggTree>> loadDomainPackage(String domainId) {

        TreeListResultModel<List<JavaAggTree>> result = new TreeListResultModel<>();
        DomainInst domainInst = null;
        try {
            domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            JavaPackage rootPackage = domainInst.getRootPackage();
            List<JavaPackage> javaPackageList = rootPackage.getChildPackages();
            result = TreePageUtil.getTreeList(javaPackageList, JavaAggTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }
}
