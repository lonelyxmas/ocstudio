package com.ds.dsm.editor.agg.menu;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.editor.agg.JavaAggTree;
import com.ds.dsm.manager.aggregation.AggregationNav;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggregationManager;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/java/agg/context/domain/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "菜单")
@Aggregation(type = AggregationType.menu, rootClass = JavaAggMenu.class)
public class JavaAggMenu {


    @MethodChinaName(cname = "编译")
    @RequestMapping(method = RequestMethod.POST, value = "dsmBuild")
    @CustomAnnotation(imageClass = "xuicon spafont spa-icon-debug1", index = 2)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treegridrow, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaAggTree>> dsmBuild(String domainId) {
        TreeListResultModel<List<JavaAggTree>> resultModel = new TreeListResultModel<List<JavaAggTree>>();
        ESDChrome chrome = getCurrChromeDriver();
        try {
            DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            DSMFactory.getInstance().buildDomain(domainInst, getCurrChromeDriver());
            // DSMFactory.getInstance().getAggregationManager().buildDomain(domainInst, getCurrChromeDriver(), true);
            DSMFactory.getInstance().reload();
            getCurrChromeDriver().printLog("编译完成！", true);
            resultModel.setIds(Arrays.asList(new String[]{domainId}));
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            chrome.printError(e.getMessage());
            ((ErrorListResultModel) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }


    @RequestMapping(value = {"javaBuild"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.form, RequestPathEnum.ctx})
    @CustomAnnotation(index = 1, caption = "混合编译", imageClass = "spafont spa-icon-moveforward")
    @ResponseBody
    public ResultModel<Boolean> javaBuild(String dommainId, String packageName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        try {
            if (packageName == null || packageName.equals("")) {
                packageName = "App";
            }
            AggregationManager aggregationManager = DSMFactory.getInstance().getAggregationManager();
            DomainInst domainInst = aggregationManager.getDomainInstById(dommainId);
            aggregationManager.buildDomain(domainInst, getCurrChromeDriver(), true);
        } catch (Exception e) {
            e.printStackTrace();
            chrome.printError(e.getMessage());
            ((ErrorResultModel) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AggregationNav")
    @NavGroupViewAnnotation()
    @ModuleAnnotation(caption = "聚合信息")
    @DialogAnnotation(width = "900")
    @CustomAnnotation(index = 1, caption = "DSM配置", imageClass = "spafont spa-icon-values")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treegridrow)
    @ResponseBody
    public ResultModel<AggregationNav> getTempNav(String domainId) {
        ResultModel<AggregationNav> result = new ResultModel<AggregationNav>();
        return result;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 3)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "fa fa-lg fa-close", index = 4, caption = "删除")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> delete(String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            DSMFactory.getInstance().getAggregationManager().deleteDomainInst(domainId, true);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
