package com.ds.dsm.editor.agg.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.editor.agg.JavaAggTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/java/agg/")
public class AllAggJavaService {


    @RequestMapping(method = RequestMethod.POST, value = "AllJavaAggTree")
    @ModuleAnnotation(dynLoad = true, caption = "资源域", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaAggTree>> getAllJavaAggTree(String projectVersionName, String parentId) {
        TreeListResultModel<List<JavaAggTree>> result = new TreeListResultModel<>();
        try {
            List<DomainInst> domainInsts = DSMFactory.getInstance().getAggregationManager().getDomainInstList(projectVersionName);
            result = TreePageUtil.getTreeList(domainInsts, JavaAggTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
