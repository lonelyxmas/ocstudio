package com.ds.dsm.editor.agg;

import com.ds.dsm.editor.agg.menu.JavaAggFileMenu;
import com.ds.dsm.editor.agg.menu.JavaAggMenu;
import com.ds.dsm.editor.agg.menu.JavaAggPackageMenu;
import com.ds.dsm.editor.agg.menu.JavaAggToolBar;
import com.ds.dsm.editor.agg.service.AggFileService;
import com.ds.dsm.editor.agg.service.AggJavaService;
import com.ds.dsm.editor.agg.service.AggPackageService;
import com.ds.dsm.editor.agg.service.AllAggJavaService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(closeBtn = true, noHandler = false)
@ToolBarMenu(menuClasses = JavaAggToolBar.class)
@TreeAnnotation(caption = "视图编辑",
        customService = AggJavaService.class,
        size = 300, lazyLoad = true, heplBar = true)
public class JavaAggTree extends TreeListItem {

    @Pid
    String projectVersionName;

    @Pid
    String domainId;

    @Pid
    String filePath;

    @Pid
    String packageName;

    @Pid
    String parentId;

    @Pid
    String sourceClassName;

    @Pid
    String javaTempId;

    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-buttonview", bindService = AllAggJavaService.class, lazyLoad = true, dynDestory = true)
    public JavaAggTree(String projectVersionName) {
        this.caption = DSMType.aggregation.getName() + "(" + projectVersionName + ")";
        this.id = DSMType.aggregation.getType() + projectVersionName;
        this.projectVersionName = projectVersionName;
        this.parentId = id;
    }

    @RightContextMenu(menuClass = JavaAggMenu.class)
    @TreeItemAnnotation(bindService = AggJavaService.class, imageClass = "spafont spa-icon-c-databinder", lazyLoad = true, dynDestory = true)
    public JavaAggTree(DomainInst domainInst) {
        this.caption = (domainInst.getDesc() == null || domainInst.getDesc().equals("")) ? domainInst.getName() : domainInst.getDesc();
        this.domainId = domainInst.getDomainId();
        this.id = domainInst.getDomainId();
        this.parentId = DSMType.repository.getType() + domainInst.getProjectVersionName();

    }


    @RightContextMenu(menuClass = JavaAggPackageMenu.class)
    @TreeItemAnnotation(imageClass = "spafont spa-icon-package", bindService = AggPackageService.class, iniFold = true, lazyLoad = true, dynDestory = true)
    public JavaAggTree(JavaPackage javaPackage, String domainId) {
        this.caption = javaPackage.getPackageName();
        this.id = domainId + "|" + javaPackage.getPackageName();
        this.domainId = domainId;
        this.filePath = javaPackage.getPackageFile().getPath();
        this.packageName = javaPackage.getPackageName();
        this.parentId = domainId;

    }


    @RightContextMenu(menuClass = JavaAggFileMenu.class)
    @TreeItemAnnotation(imageClass = "spafont spa-icon-json-file", bindService = AggFileService.class, closeBtn = true)
    public JavaAggTree(JavaSrcBean javaFile) {
        this.caption = javaFile.getName();
        this.javaTempId = javaFile.getJavaTempId();
        this.packageName = javaFile.getPackageName();
        this.domainId = javaFile.getDsmId();
        this.sourceClassName = javaFile.getClassName();
        this.id = javaFile.getClassName();
        this.parentId = domainId + "|" + packageName;
        this.filePath = javaFile.getFile().getAbsolutePath();

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }
}
