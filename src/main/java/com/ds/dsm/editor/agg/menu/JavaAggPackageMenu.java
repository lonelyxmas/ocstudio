package com.ds.dsm.editor.agg.menu;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.editor.action.UPLoadFile;
import com.ds.dsm.editor.agg.JavaAggTree;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/java/agg/context/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "菜单")
@Aggregation(type = AggregationType.menu,rootClass =JavaAggPackageMenu.class )
public class JavaAggPackageMenu {

    @RequestMapping(method = RequestMethod.POST, value = "paste")
    @CustomAnnotation(imageClass = "spafont spa-icon-paste", index = 1, caption = "粘贴")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.treeview, RequestPathEnum.sTagVar}, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaAggTree>> paste(String sfilePath, String packageName, String domainId, String projectName) {
        TreeListResultModel<List<JavaAggTree>> result = new TreeListResultModel<List<JavaAggTree>>();
        try {
            DSMFactory dsmFactory = DSMFactory.getInstance();
            File desFile = new File(sfilePath);
            DomainInst domainInst = dsmFactory.getAggregationManager().getDomainInstById(domainId);
            JavaSrcBean srcBean = dsmFactory.getTempManager().genJavaSrc(desFile, domainInst, null);
            DSMFactory.getInstance().getBuildFactory().copy(srcBean, packageName);
            result.setIds(Arrays.asList(new String[]{domainId + "|" + packageName}));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reLoad")
    @CustomAnnotation(imageClass = "xuicon xui-refresh", index = 1, caption = "刷新")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaAggTree>> reLoad(String packageName, String domainId, String filePath) {
        TreeListResultModel<List<JavaAggTree>> result = new TreeListResultModel<List<JavaAggTree>>();
        String id = domainId + "|" + packageName;
        result.setIds(Arrays.asList(new String[]{id}));
        return result;
    }


    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ResultModel<Boolean> split2() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }

    @RequestMapping(value = "UploadFile")
    @APIEventAnnotation(autoRun = true)
    @DialogAnnotation(width = "450", height = "380", caption = "上传JAVA文件")
    @ModuleAnnotation
    @FormViewAnnotation
    @CustomAnnotation(caption = "上传", index = 3, imageClass = "xui-icon-upload", tips = "上传")
    public @ResponseBody
    ResultModel<UPLoadFile> uploadFile(String domainId, String packageName) {
        ResultModel<UPLoadFile> resultModel = new ResultModel<UPLoadFile>();
        try {
            UPLoadFile upLoadFile = new UPLoadFile(domainId, packageName);
            resultModel.setData(upLoadFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 4)
    @ResponseBody
    public ResultModel<Boolean> split6() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "newApp")
    @CustomAnnotation(imageClass = "xuicon xui-uicmd-add", index = 5)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "新建", imageClass = "xuicon xui-uicmd-add", index = 5)
    public JavaAggNewMenu getJavaJarAction() {
        return new JavaAggNewMenu();
    }


    @RequestMapping(method = RequestMethod.POST, value = "importAgg")
    @CustomAnnotation(imageClass = "xuicon xui-uicmd-add", index = 6)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "导入", imageClass = "spafont spa-icon-html", index = 5)
    public JavaAggImportMenu importAgg() {
        return new JavaAggImportMenu();
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 7)
    @ResponseBody
    public ResultModel<Boolean> split7() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @MethodChinaName(cname = "删除")
    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "xuicon xui-icon-minus", index = 8)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaAggTree>> delete(String domainId, String parentId, String filePath, String javaTempId) {
        TreeListResultModel<List<JavaAggTree>> result = new TreeListResultModel<List<JavaAggTree>>();
        try {
            File desFile = new File(filePath);
            DSMFactory dsmFactory = DSMFactory.getInstance();
            DomainInst domainInst = dsmFactory.getAggregationManager().getDomainInstById(domainId);
            if (desFile.exists()) {
                if (desFile.isDirectory()) {
                    JavaPackage javaPackage = domainInst.getPackageByFile(desFile);
                    dsmFactory.getTempManager().deleteJavaPackage(javaPackage);
                } else {
                    JavaSrcBean srcBean = dsmFactory.getTempManager().genJavaSrc(desFile, domainInst, javaTempId);
                    dsmFactory.getTempManager().deleteJavaFile(srcBean);
                }
            }

            result.setIds(Arrays.asList(new String[]{parentId}));

        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
