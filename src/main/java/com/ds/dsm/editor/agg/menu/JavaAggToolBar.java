package com.ds.dsm.editor.agg.menu;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.editor.agg.JavaAggTree;
import com.ds.dsm.manager.aggregation.AggregationCreateView;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.RemoteConnectionManager;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping(value = {"/java/agg/toolbar/"})
@Aggregation(type = AggregationType.menu,rootClass = JavaAggToolBar.class)
@MenuBarMenu(menuType = CustomMenuType.toolbar, showCaption = false, caption = "菜单", hAlign = HAlignType.right, id = "JavaAggEditorToolbar")
public class JavaAggToolBar {
    @MethodChinaName(cname = "")
    @RequestMapping(method = RequestMethod.POST, value = "reLoad")
    @CustomAnnotation(imageClass = "xuicon xui-refresh", index = 1, tips = "刷新")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaAggTree>> reLoad(String projectName) {
        TreeListResultModel<List<JavaAggTree>> result = new TreeListResultModel<>();
        try {
            List<DomainInst> domainInsts = DSMFactory.getInstance().getAggregationManager().getDomainInstList(projectName);
            result = TreePageUtil.getTreeList(domainInsts, JavaAggTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "")
    @RequestMapping(value = {"download"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 2, tips = "打包下载", imageClass = "spafont spa-icon-select1")
    @ResponseBody
    public ResultModel<Boolean> download(String projectVersionName) {
        ESDChrome defaultChrome = getCurrChromeDriver();
        if (projectVersionName != null) {
            RemoteConnectionManager.getConntctionService(projectVersionName).execute(new Runnable() {
                @Override
                public void run() {
                    exportProject(projectVersionName, defaultChrome, false, true);
                }
            });
        }
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }

    @MethodChinaName(cname = "")
    @RequestMapping(value = "ImportAgg")
    @APIEventAnnotation(bindMenu = CustomMenuItem.upload, autoRun = true)
    @FormViewAnnotation
    @DialogAnnotation(width = "450", height = "380")
    @ModuleAnnotation
    @CustomAnnotation(index = 2, imageClass = "xui-icon-upload", tips = "导入工程")
    public @ResponseBody
    ResultModel<UPLoadAgg> importAgg() {
        ResultModel<UPLoadAgg> resultModel = new ResultModel<UPLoadAgg>();
        try {
            UPLoadAgg upLoadFile = new UPLoadAgg();
            resultModel.setData(upLoadFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;

    }

    @MethodChinaName(cname = "")
    @RequestMapping(value = {"CreateDomain"}, method = {RequestMethod.GET, RequestMethod.POST})
    @FormViewAnnotation
    @CustomAnnotation(imageClass = "xuicon xui-uicmd-add", index = 4, tips = "新建领域模型")
    @ModuleAnnotation()
    @DialogAnnotation(height = "260", width = "380")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add, callback = CustomCallBack.TreeReloadNode)
    @ResponseBody
    public ResultModel<AggregationCreateView> createDomain(String projectVersionName) {
        ResultModel<AggregationCreateView> result = new ResultModel<AggregationCreateView>();
        try {
            DomainInst dsmInst = DSMFactory.getInstance().createDomain(projectVersionName);
            result.setData(new AggregationCreateView(dsmInst));
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    private void exportProject(String projectVersionName, ESDChrome chrome, boolean deploy, boolean download) {
        try {
            ESDFacrory.getESDClient().exportProject(projectVersionName, chrome, deploy, download);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            chrome.execScript("xui.free('export')");
        }

    }

    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }
}
