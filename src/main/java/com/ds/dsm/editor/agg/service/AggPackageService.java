package com.ds.dsm.editor.agg.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.editor.agg.JavaAggTree;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/java/agg/package/")
public class AggPackageService {


    @RequestMapping(method = RequestMethod.POST, value = "loadDomainFile")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaAggTree>> loadDomainFile(String id, String filePath, String domainId) {
        TreeListResultModel<List<JavaAggTree>> result = new TreeListResultModel<>();
        List<Object> javaFileList = new ArrayList<>();
        File desFile = new File(filePath);
        DomainInst domainInst = null;
        try {
            domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            JavaPackage javaPackage = domainInst.getPackageByFile(desFile);
            if (javaPackage != null) {
                javaFileList.addAll(javaPackage.listFiles());
                javaFileList.addAll(javaPackage.listChildren());
            }

            result = TreePageUtil.getTreeList(javaFileList, JavaAggTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "uploadFile")
    @APIEventAnnotation(bindMenu = CustomMenuItem.upload)
    @CustomAnnotation(caption = "上传文件")
    public @ResponseBody
    ResultModel<Boolean> addDocument(String domainId, String packageName, @RequestParam("files") MultipartFile file) {
        ResultModel<Boolean> resultModel = new ResultModel<Boolean>();
        DomainInst domainInst = null;
        try {
            if (file != null) {
                String fileName = file.getOriginalFilename();
                if (fileName != null && fileName.endsWith(".java")) {
                    domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                    JavaPackage javaPackage = domainInst.getPackageByName(packageName);
                    InputStream inputStream = file.getInputStream();
                    javaPackage.upload(file.getOriginalFilename(), inputStream);
                } else {
                    return new ErrorResultModel<>("只支持JAVA文件上传");
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultModel;

    }

}
