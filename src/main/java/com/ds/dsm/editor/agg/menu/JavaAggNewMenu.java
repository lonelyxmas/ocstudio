package com.ds.dsm.editor.agg.menu;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.aggregation.config.api.AggAPIGridView;
import com.ds.dsm.aggregation.config.domain.AggDomainGridView;
import com.ds.dsm.aggregation.config.entity.info.AggEntityGridView;
import com.ds.dsm.aggregation.config.menu.AggMenuGridView;
import com.ds.dsm.aggregation.config.root.AggRootGridView;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping(value = {"/java/agg/create/"})
@Aggregation(type = AggregationType.menu, rootClass = JavaAggNewMenu.class)
public class JavaAggNewMenu {


    @RequestMapping(method = RequestMethod.POST, value = "newAggRoot")
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @CustomAnnotation(imageClass = "spafont spa-icon-c-gallery", caption = "聚合根", index = 1)
    @ModuleAnnotation()
    @ResponseBody
    public ListResultModel<List<AggRootGridView>> newAggRoot(String domainId, String packageName) {
        ListResultModel<List<AggRootGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAggRoots();
                result = PageUtil.getDefaultPageList(esdClassList, AggRootGridView.class);
            }

        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "newAggEntity")
    @GridViewAnnotation
    @APIEventAnnotation()
    @ModuleAnnotation()
    @CustomAnnotation(imageClass = "spafont spa-icon-conf", caption = "聚合实体", index = 2)
    @ResponseBody
    public ListResultModel<List<AggEntityGridView>> newAggEntity(String domainId, String packageName) {
        ListResultModel<List<AggEntityGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAggEntities();
                result = PageUtil.getDefaultPageList(esdClassList, AggEntityGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "newAggMenu")
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @CustomAnnotation(imageClass = "spafont spa-icon-c-menu", caption = "菜单动作", index = 3)
    @ModuleAnnotation()
    @ResponseBody
    public ListResultModel<List<AggMenuGridView>> getAggMenuList(String domainId) {
        ListResultModel<List<AggMenuGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAggMenus();
                result = PageUtil.getDefaultPageList(esdClassList, AggMenuGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "newAggApi")
    @GridViewAnnotation
    @ModuleAnnotation()
    @CustomAnnotation(imageClass = "spafont spa-icon-c-webapi", caption = "服务接口", index = 4)
    @ResponseBody
    public ListResultModel<List<AggAPIGridView>> newAggApi(String domainId) {
        ListResultModel<List<AggAPIGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAggAPIs();
                result = PageUtil.getDefaultPageList(esdClassList, AggAPIGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "newAggDomain")
    @GridViewAnnotation
    @APIEventAnnotation()
    @CustomAnnotation(imageClass = "spafont spa-icon-conf", caption = "通用域", index = 5)
    @ModuleAnnotation()
    @ResponseBody
    public ListResultModel<List<AggDomainGridView>> newAggDomain(String domainId) {
        ListResultModel<List<AggDomainGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            if (bean != null) {
                esdClassList = bean.getAggDomains();
                result = PageUtil.getDefaultPageList(esdClassList, AggDomainGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }

}
