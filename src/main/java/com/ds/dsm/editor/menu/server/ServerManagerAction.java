package com.ds.dsm.editor.menu.server;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.esdserver.ESDServerUtil;
import com.ds.esd.project.config.LocalServer;
import com.ds.server.eumus.SystemStatus;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = {"/editor/debug/"})
@MethodChinaName(cname = "编译")
@MenuBarMenu(menuType = CustomMenuType.menubar, caption = "服务器", index = 9, imageClass = "spafont spa-icon-alignw")
@Aggregation(type = AggregationType.menu,rootClass =ServerManagerAction.class )
public class ServerManagerAction {
    @RequestMapping(value = {"startDebugServer"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 0, imageClass = "spafont spa-icon-debug1", caption = "启动服务")
    @ResponseBody
    public ResultModel<Boolean> startDebugServer(String projectVersionName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        if (projectVersionName != null) {
            LocalServer server = null;
            Project project = null;
            try {
                server = ESDFacrory.getESDClient().getDefaultLocalServer(projectVersionName);
                project = ESDFacrory.getESDClient().getProjectByName(projectVersionName);
            } catch (JDSException e) {
                chrome.printLog(e.getMessage(), true);

            }
            if (server == null) {
                chrome.printLog("请配置本地服务地址！", true);

            } else {
                ESDServerUtil.startESDServer(project, server);
                server.setStatus(SystemStatus.ONLINE);
            }
        }
        return resultModel;
    }


    @RequestMapping(value = {"stopDebugServer"}, method = {RequestMethod.POST})
    @CustomAnnotation(index = 1, imageClass = "fa fa-lg fa-close", caption = "关闭服务")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @ResponseBody
    public ResultModel<Boolean> stopDebugServer(String projectVersionName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        if (projectVersionName != null) {
            LocalServer server = null;
            try {
                server = ESDFacrory.getESDClient().getDefaultLocalServer(projectVersionName);
            } catch (JDSException e) {
                chrome.printLog(e.getMessage(), true);
            }
            if (server == null) {
                chrome.printLog("请配置本地服务地址！", true);
            } else {
                ESDServerUtil.stopESDServer(server);
                server.setStatus(SystemStatus.OFFLINE);
            }
        }
        return resultModel;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "SeverConfig")
    @CustomAnnotation(index = 3)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "服务器配置", imageClass = "spafont spa-icon-conf")
    public ServerConfigAction getSeverConfigService() {
        return new ServerConfigAction();
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
