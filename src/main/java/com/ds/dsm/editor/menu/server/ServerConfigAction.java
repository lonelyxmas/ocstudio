package com.ds.dsm.editor.menu.server;

import com.ds.config.DSMResultModel;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = {"/action/topmenu/serverconfig/"})
@Aggregation(type = AggregationType.menu, rootClass = ServerConfigAction.class)
public class ServerConfigAction {
    public ServerConfigAction() {

    }

    @RequestMapping(value = {"ESDServerList"}, method = {RequestMethod.POST})
    @DialogAnnotation(width = "600", height = "380")
    @ModuleAnnotation(caption = "本地配置")
    @DynLoadAnnotation(refClassName = "RAD.server.ESDServerList", projectName = "DSMdsm")
    @CustomAnnotation(index = 0, imageClass = "spafont spa-icon-coms")
    @ResponseBody
    public ResultModel<Boolean> getESDServerList(String projectName) {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(value = {"RemoteServerList"}, method = {RequestMethod.POST})
    @DialogAnnotation( width = "600", height = "380")
    @ModuleAnnotation(caption = "远程配置")
    @DynLoadAnnotation(refClassName = "RAD.server.RemoteServerList", projectName = "DSMdsm")
    @CustomAnnotation(index = 1, imageClass = "spafont spa-icon-c-slider")
    @ResponseBody
    public DSMResultModel<Boolean> getRemoteServerList(String projectName) {
        DSMResultModel resultModel = new DSMResultModel();
        return resultModel;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }

    @RequestMapping(value = {"ProxyHostList"}, method = {RequestMethod.POST})
    @DialogAnnotation( width = "600", height = "380")
    @ModuleAnnotation(caption = "代理配置")
    @DynLoadAnnotation(refClassName = "RAD.server.ProxyHostList", projectName = "DSMdsm")
    @CustomAnnotation(index = 3, imageClass = "spafont spa-icon-alignw")
    @ResponseBody
    public DSMResultModel<Boolean> getProxyHostList(String projectName) {
        DSMResultModel resultModel = new DSMResultModel();
        return resultModel;
    }

}
