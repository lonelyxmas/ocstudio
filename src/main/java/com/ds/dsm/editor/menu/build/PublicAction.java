package com.ds.dsm.editor.menu.build;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.web.RemoteConnectionManager;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = {"/editor/build/public/"})
@Aggregation(type=AggregationType.menu,rootClass =PublicAction.class )
public class PublicAction {
    @RequestMapping(value = {"exportLocalServer"}, method = {RequestMethod.POST})
    @CustomAnnotation(index = 1, caption = "本地发布", imageClass = "spafont spa-icon-jumpto")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @ResponseBody
    public ResultModel<Boolean> exportLocalServer(String projectName, String serverId) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        if (projectName != null) {
            String finalLocalServerId = serverId;
            RemoteConnectionManager.getConntctionService(projectName).execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        ESDFacrory.getESDClient().exportLocalServer(projectName, finalLocalServerId, chrome);
                    } catch (JDSException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        return resultModel;
    }

    @RequestMapping(value = {"exportRemoteServer"}, method = {RequestMethod.POST})
    @CustomAnnotation(index = 2, caption = "远程发布", imageClass = "spafont spa-icon-indent")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @ResponseBody
    public ResultModel<Boolean> exportRemoteServer(String projectName, String serverId) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        String remoteServerId = serverId.toString();
        if (projectName != null) {
            RemoteConnectionManager.getConntctionService(projectName.toString()).execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        ESDFacrory.getESDClient().exportRemoteServer(projectName, remoteServerId, chrome);
                    } catch (JDSException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        return resultModel;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
