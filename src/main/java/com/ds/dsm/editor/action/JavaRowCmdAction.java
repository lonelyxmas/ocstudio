package com.ds.dsm.editor.action;

import com.ds.common.JDSException;
import com.ds.common.util.FileUtility;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.debug.bean.ConfigModuleTree;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.tool.enums.FileType;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.module.ModuleComponent;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = {"/action/java/row/"})
public class JavaRowCmdAction {


    @MethodChinaName(cname = "编译")
    @RequestMapping(method = RequestMethod.POST, value = "dsmBuild")
    @CustomAnnotation(imageClass = "fa fa-lg fa-circle-o-notch")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> dsmBuild(String id, String projectName,String domainId,  String parentId, FileType type, String packageName, String className) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();
        try {

            Map<String, ?> valueMap = JDSActionContext.getActionContext().getContext();
            result.setIds(Arrays.asList(new String[]{parentId}));
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);

            switch (type) {
                case EUPackage:
                    ESDFacrory.getESDClient().buildPackage(projectName, packageName, domainId,  valueMap, this.getCurrChromeDriver());
                    break;
                case EUModule:

                    EUModule module = version.getModule(className);
                    ESDFacrory.getESDClient().rebuildCustomModule(className, projectName, valueMap);
                    if (module != null) {
                        List<ModuleComponent> moduleComponents = module.getComponent().findComponents(ComponentType.Module, null);
                        for (ModuleComponent moduleComponent : moduleComponents) {
                            ESDFacrory.getESDClient().delModule(moduleComponent.getEuModule());
                        }
                        version.delModule(module);
                    }
                    break;
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "删除")
    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "fa fa-lg fa-close", index = 2)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> delete(String id, String parentId, FileType type, String filePath) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();
        result.setIds(Arrays.asList(new String[]{parentId}));

        File file = new File(filePath);
        try {
            FileUtility.forceDelete(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }


    void rebuildCustomModule(String projectId, String packageName, String esdPackageName) {
        try {
            Map map = new HashMap();
            map.put("projectId", projectId);
            ESDChrome chrome = getCurrChromeDriver();
            ESDFacrory.getESDClient().buildCustomModule(projectId, packageName, esdPackageName, map, chrome);
        } catch (JDSException e) {
            e.printStackTrace();
        }
    }

    ResultModel<Boolean> reload(String projectId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ESDChrome chrome = getCurrChromeDriver();
        chrome.printLog("正在装载编译，请稍后！", true);
        try {
            ESDFacrory.getInstance().dyReload(null);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;

    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
