package com.ds.dsm.editor.action;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.debug.bean.ConfigModuleTree;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.gen.GenJava;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.enums.FileType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import com.ds.web.json.JSONData;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = {"/action/java/context/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "菜单")
@Aggregation(type = AggregationType.menu, rootClass = JavaContextMenuAction.class)
public class JavaContextMenuAction {


    @RequestMapping(method = RequestMethod.POST, value = "copy")
    @CustomAnnotation(imageClass = "spafont spa-icon-copy", index = 1, caption = "复制")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview)
    public @ResponseBody
    ResultModel<Boolean> copy(String id, @JSONData Map<String, String> tagVar) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "paste")
    @CustomAnnotation(imageClass = "spafont spa-icon-paste", index = 2, caption = "粘贴")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> paste(String parentId, String path, String domainId, String projectName) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();
        try {
            DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            result.setIds(Arrays.asList(new String[]{parentId}));
            GenJava genJava = GenJava.getInstance(projectName);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reLoad")
    @CustomAnnotation(imageClass = "xuicon xui-refresh", index = 3, caption = "刷新")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> reLoad(String id, String projectName, String parentId, FileType type, String path) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();
        result.setIds(Arrays.asList(new String[]{parentId}));
        switch (type) {
            case JavaPackage:
                result.setIds(Arrays.asList(new String[]{id}));
                break;
            case JavaFile:
                result.setIds(Arrays.asList(new String[]{parentId}));
                break;
        }
        return result;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 4)
    @ResponseBody
    public ResultModel<Boolean> split6() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "newApp")
    @CustomAnnotation(imageClass = "xuicon xui-uicmd-add", index = 5)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "新建", imageClass = "xuicon xui-uicmd-add", index = 5)
    public JavaNewAction getJavaJarAction() {
        return new JavaNewAction();
    }


    @MethodChinaName(cname = "删除")
    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "xuicon xui-icon-minus", index = 6)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> delete(String domainId, String parentId, String path, String javaTempId) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();
        try {
            File desFile = new File(path);
            DSMFactory dsmFactory = DSMFactory.getInstance();
            DomainInst domainInst = dsmFactory.getAggregationManager().getDomainInstById(domainId);
            if (desFile.exists()) {
                if (desFile.isDirectory()) {
                    JavaPackage javaPackage = domainInst.getPackageByFile(desFile);
                    dsmFactory.getTempManager().deleteJavaPackage(javaPackage);
                } else {
                    JavaSrcBean srcBean = dsmFactory.getTempManager().genJavaSrc(desFile, domainInst, javaTempId);
                    dsmFactory.getTempManager().deleteJavaFile(srcBean);
                }
            }

            result.setIds(Arrays.asList(new String[]{parentId}));

        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 7)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
