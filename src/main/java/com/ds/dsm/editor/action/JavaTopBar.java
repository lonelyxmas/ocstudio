package com.ds.dsm.editor.action;

import com.ds.context.JDSActionContext;
import com.ds.dsm.editor.menu.server.ServerManagerAction;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(value = {"/editor/java/topbar/"})
@MethodChinaName(cname = "菜单")
@MenuBarMenu(menuType = CustomMenuType.menubar, caption = "菜单", id = "JavaBuildTopBar")
@Aggregation(type = AggregationType.menu,rootClass = JavaTopBar.class)
public class JavaTopBar {

    @RequestMapping(method = RequestMethod.POST, value = "BuildAction")
    @CustomAnnotation(index = 0)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "编译")
    public JavaBuildAction getJavaBuildAction() {
        return new JavaBuildAction();
    }


    @RequestMapping(method = RequestMethod.POST, value = "ServerManagerAction")
    @CustomAnnotation(index = 1)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "服务器")
    public ServerManagerAction getServerManagerAction() {
        return new ServerManagerAction();
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
