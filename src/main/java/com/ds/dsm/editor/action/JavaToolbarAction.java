package com.ds.dsm.editor.action;

import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.debug.bean.ConfigModuleTree;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.enums.FileType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.json.JSONData;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = {"/action/java/toolbar/"})
@MenuBarMenu(menuType = CustomMenuType.toolbar, showCaption = false, caption = "菜单", id = "JavaToolbar")
@Aggregation(type = AggregationType.menu, rootClass = JavaToolbarAction.class)
public class JavaToolbarAction {
    @MethodChinaName(cname = "")
    @RequestMapping(method = RequestMethod.POST, value = "reLoad")
    @CustomAnnotation(imageClass = "xuicon xui-refresh", index = 1, tips = "刷新")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> reLoad(String id, String projectName, String parentId, FileType type, String filePath) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();


        result.setIds(Arrays.asList(new String[]{parentId}));
        switch (type) {
            case JavaPackage:
                result.setIds(Arrays.asList(new String[]{id}));
                break;
            case JavaFile:
                result.setIds(Arrays.asList(new String[]{parentId}));
                break;
        }

        return result;
    }

    @MethodChinaName(cname = "")
    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "xuicon xui-icon-minus", index = 2, tips = "删除")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> delete(String id, String parentId, String javatempId, String projectVersionName, String domainId, String viewInstId, String sourceClassName, String methodName, FileType type, String filePath) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();

        return result;
    }

    @MethodChinaName(cname = "")
    @RequestMapping(method = RequestMethod.POST, value = "newClass")
    @CustomAnnotation(imageClass = "xuicon xui-uicmd-add", index = 3, tips = "新建类")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    ResultModel<Boolean> newClass(String id, @JSONData Map<String, String> tagVar) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @MethodChinaName(cname = "")
    @RequestMapping(method = RequestMethod.POST, value = "upload")
    @CustomAnnotation(imageClass = "xui-icon-upload", index = 4, tips = "上传")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    ResultModel<Boolean> upload(String id, @JSONData Map<String, String> tagVar) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
