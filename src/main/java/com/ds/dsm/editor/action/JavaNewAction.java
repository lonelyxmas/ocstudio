package com.ds.dsm.editor.action;

import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.json.JSONData;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


@Controller
@RequestMapping(value = {"/action/java/create/"})
@Aggregation(type=AggregationType.menu,rootClass = JavaNewAction.class)
public class JavaNewAction {

    @RequestMapping(method = RequestMethod.POST, value = "newClass")
    @CustomAnnotation(imageClass = "xuicon xui-uicmd-add", index = 1, caption = "新建类")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    ResultModel<Boolean> newClass(String id, @JSONData Map<String, String> tagVar) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "newFolder")
    @CustomAnnotation(imageClass = "fa fa-folder-o", index = 1, caption = "新建包")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    ResultModel<Boolean> newFolder(String id, @JSONData Map<String, String> tagVar) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "upload")
    @CustomAnnotation(imageClass = "xuicon xui-uicmd-add", index = 1, caption = "上传")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    ResultModel<Boolean> upload(String id, @JSONData Map<String, String> tagVar) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }

    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }

}
