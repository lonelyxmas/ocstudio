package com.ds.dsm.editor.action;

import com.ds.dsm.editor.agg.service.AggPackageService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.FileUploadAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.esd.tool.ui.enums.event.annotation.FileUploadEvent;
import com.ds.esd.tool.ui.enums.event.enums.FileUploadEventEnum;

@FormAnnotation(stretchHeight = StretchType.last)
public class UPLoadFile {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    String packageName;

    @CustomAnnotation(pid = true, hidden = true)
    String sourceClassName;

    @FileUploadAnnotation(bindClass = AggPackageService.class)
    @FieldAnnotation(componentType = ComponentType.FileUpload, haslabel = false, colSpan = -1, required = true)
    @CustomAnnotation( caption = "上传文件")
    String thumbnailFile;

    public UPLoadFile() {

    }

    public UPLoadFile(String domainId, String packageName) {
        this.domainId = domainId;
        this.packageName = packageName;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getThumbnailFile() {
        return thumbnailFile;
    }

    public void setThumbnailFile(String thumbnailFile) {
        this.thumbnailFile = thumbnailFile;
    }
}
