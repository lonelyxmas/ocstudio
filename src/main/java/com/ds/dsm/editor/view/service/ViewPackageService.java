package com.ds.dsm.editor.view.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.editor.view.JavaViewTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/java/view/")
public class ViewPackageService {


    @RequestMapping(method = RequestMethod.POST, value = "loadViewFile")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaViewTree>> loadViewFile(String id, String filePath, String viewInstId) {
        TreeListResultModel<List<JavaViewTree>> result = new TreeListResultModel<>();
        List<Object> javaFileList = new ArrayList<>();
        File desFile = new File(filePath);
        ViewInst viewInst = null;
        try {
            viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            JavaPackage javaPackage = viewInst.getPackageByFile(desFile);
            if (javaPackage != null) {
                javaFileList.addAll(javaPackage.listFiles());
                javaFileList.addAll(javaPackage.listChildren());
            }

            result = TreePageUtil.getTreeList(javaFileList, JavaViewTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }
}
