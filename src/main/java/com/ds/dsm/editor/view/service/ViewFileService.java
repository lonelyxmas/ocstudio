package com.ds.dsm.editor.view.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.editor.view.JavaViewEditor;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/java/view/")
public class ViewFileService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewJavaEditor")
    @ModuleAnnotation(caption = "JAVA树", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @FormViewAnnotation()
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<JavaViewEditor> getViewJavaEditor(String filePath, String viewInstId, String sourceClassName, String sourceMethodName) {
        ResultModel<JavaViewEditor> result = new ResultModel<>();
        File desFile = new File(filePath);
        JavaSrcBean srcBean = null;
        try {
            ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            srcBean = DSMFactory.getInstance().getTempManager().genJavaSrc(desFile, viewInst, viewInstId);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        if (sourceClassName != null && !sourceClassName.equals("")) {
            srcBean.setSourceClassName(sourceClassName);
        }
        if (sourceMethodName != null && !sourceMethodName.equals("")) {
            srcBean.setMethodName(sourceMethodName);
        }

        JavaViewEditor editor = new JavaViewEditor(srcBean);
        result.setData(editor);
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadChildAPI")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> loadChildAPI(String filePath, String domainId, String viewInstId, String sourceClassName, String sourceMethodName) {
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<List<ViewConfigTree>>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId).getCurrConfig();
            MethodConfig methodConfig = customESDClassAPIBean.getMethodByName(sourceMethodName);
            List<CustomView> customViews = new ArrayList<>();
            if (methodConfig.getView() != null) {
                customViews.add(methodConfig.getView());
            }
            resultModel = TreePageUtil.getTreeList(customViews, ViewConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }


}
