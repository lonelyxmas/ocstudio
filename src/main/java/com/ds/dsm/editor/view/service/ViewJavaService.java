package com.ds.dsm.editor.view.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.editor.view.JavaViewTree;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/java/view/")
public class ViewJavaService {


    @RequestMapping(method = RequestMethod.POST, value = "JavaViewTree")
    @ModuleAnnotation(dynLoad = true, caption = "", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @NavTreeViewAnnotation()
    @ResponseBody
    public TreeListResultModel<List<JavaViewTree>> getJavaViewTree(String projectVersionName, String parentId) {
        TreeListResultModel<List<JavaViewTree>> result = new TreeListResultModel<>();
        try {
            List<ViewInst> viewList = DSMFactory.getInstance().getViewManager().getViewList(projectVersionName);
            result = TreePageUtil.getTreeList(viewList, JavaViewTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadViewPackage")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaViewTree>> loadViewPackage(String viewInstId) {

        TreeListResultModel<List<JavaViewTree>> result = new TreeListResultModel<>();
        ViewInst viewInst = null;
        try {
            viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            JavaPackage rootPackage = viewInst.getRootPackage();
            List<JavaPackage> javaPackageList = rootPackage.getChildPackages();
            result = TreePageUtil.getTreeList(javaPackageList, JavaViewTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }
}
