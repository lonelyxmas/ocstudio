package com.ds.dsm.editor.view;

import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.dsm.editor.view.menu.JavaViewFileMenu;
import com.ds.dsm.editor.view.menu.JavaViewMenu;
import com.ds.dsm.editor.view.menu.JavaViewPackageMenu;
import com.ds.dsm.editor.view.menu.JavaViewToolbar;
import com.ds.dsm.editor.view.service.AllViewJavaService;
import com.ds.dsm.editor.view.service.ViewFileService;
import com.ds.dsm.editor.view.service.ViewJavaService;
import com.ds.dsm.editor.view.service.ViewPackageService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(closeBtn = true, noHandler = false)
@ToolBarMenu(menuClasses = JavaViewToolbar.class)
@TreeAnnotation(caption = "视图编辑",
        customService = ViewJavaService.class,
        size = 300, lazyLoad = true, heplBar = true)
public class JavaViewTree extends TreeListItem {

    @Pid
    String projectVersionName;
    @Pid
    String viewInstId;

    @Pid
    String filePath;
    @Pid
    String packageName;
    @Pid
    String methodName;
    @Pid
    String sourceMethodName;
    @Pid
    String sourceClassName;
    @Pid
    String parentId;
    @Pid
    String entityClassName;
    @Pid
    String javaTempId;

    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-cssbox", bindService = AllViewJavaService.class, lazyLoad = true, dynDestory = true)
    public JavaViewTree(String projectVersionName) {
        this.caption = DSMType.view.getName() + "(" + projectVersionName + ")";
        this.id = DSMType.view.getType() + projectVersionName;
        this.projectVersionName = projectVersionName;
        this.parentId = id;
    }

    @RightContextMenu(menuClass = JavaViewMenu.class)
    @TreeItemAnnotation(bindService = ViewJavaService.class, imageClass = "spafont spa-icon-c-cssbox", lazyLoad = true, dynDestory = true)
    public JavaViewTree(ViewInst viewInst) {
        this.caption = (viewInst.getDesc() == null || viewInst.getDesc().equals("")) ? viewInst.getName() : viewInst.getDesc();
        this.viewInstId = viewInst.getViewInstId();
        this.id = viewInst.getViewInstId();
        this.parentId = DSMType.view.getType() + viewInst.getProjectVersionName();

    }


    @RightContextMenu(menuClass = JavaViewPackageMenu.class)
    @TreeItemAnnotation(imageClass = "spafont spa-icon-package", bindService = ViewPackageService.class, iniFold = true, lazyLoad = true, dynDestory = true)
    public JavaViewTree(JavaPackage javaPackage, String viewInstId) {
        this.caption = javaPackage.getPackageName();
        this.id = viewInstId + "|" + javaPackage.getPackageName();
        this.viewInstId = viewInstId;
        this.filePath = javaPackage.getPackageFile().getPath();
        this.packageName = javaPackage.getPackageName();
        this.parentId = viewInstId;

    }


    @RightContextMenu(menuClass = JavaViewFileMenu.class)
    @TreeItemAnnotation(imageClass = "spafont spa-icon-json-file", bindService = ViewFileService.class, closeBtn = true)
    public JavaViewTree(JavaSrcBean javaFile) {
        this.caption = javaFile.getName();
        this.javaTempId = javaFile.getJavaTempId();
        this.packageName = javaFile.getPackageName();
        this.viewInstId = javaFile.getDsmId();
        this.entityClassName = javaFile.getClassName();
        this.sourceClassName = javaFile.getSourceClassName();
        this.id = javaFile.getClassName();
        this.filePath = javaFile.getFile().getAbsolutePath();
        this.parentId = viewInstId + "|" + packageName;
        this.imageClass = DSMType.view.getFileImgClass();
        this.methodName = javaFile.getMethodName();
        this.sourceMethodName = javaFile.getMethodName();
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }
}
