package com.ds.dsm.editor.view.menu;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.admin.temp.aggreagtion.AggregationTempForm;
import com.ds.dsm.editor.view.JavaViewTree;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import com.ds.web.json.JSONData;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = {"/java/view/context/file/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "菜单")
@Aggregation(type = AggregationType.menu,rootClass =JavaViewFileMenu.class )
public class JavaViewFileMenu {


    @RequestMapping(method = RequestMethod.POST, value = "ViewEntityInfo")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @DialogAnnotation(caption = "视图配置", width = "900", height = "680")
    @CustomAnnotation( index = 0)
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> getViewEntityMetaView(String sourceClassName, String sourceMethodName, String viewInstId, String domainId, String id) {
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<List<ViewConfigTree>>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId).getCurrConfig();
            MethodConfig methodConfig = customESDClassAPIBean.getMethodByName(sourceMethodName);
            List<CustomView> customViews = new ArrayList<>();
            if (methodConfig.getView() != null) {
                customViews.add(methodConfig.getView());
            }

            resultModel = TreePageUtil.getTreeList(customViews, ViewConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ResultModel<Boolean> split2() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "copy")
    @CustomAnnotation(imageClass = "spafont spa-icon-copy", index = 3, caption = "复制")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview)
    public @ResponseBody
    ResultModel<Boolean> copy(String id, @JSONData Map<String, String> tagVar) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "xuicon xui-icon-minus", index = 4, caption = "删除")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaViewTree>> delete(String viewInstId, String filePath, String javaTempId) {
        TreeListResultModel<List<JavaViewTree>> result = new TreeListResultModel<List<JavaViewTree>>();
        try {
            File desFile = new File(filePath);
            DSMFactory dsmFactory = DSMFactory.getInstance();
            ViewInst viewInst = dsmFactory.getViewManager().getViewInstById(viewInstId);
            String parentId = viewInst.getViewInstId();
            if (desFile.exists()) {
                if (desFile.isDirectory()) {
                    JavaPackage javaPackage = viewInst.getPackageByFile(desFile);
                    dsmFactory.getTempManager().deleteJavaPackage(javaPackage);
                } else {
                    JavaSrcBean srcBean = dsmFactory.getTempManager().genJavaSrc(desFile, viewInst, javaTempId);
                    dsmFactory.getTempManager().deleteJavaFile(srcBean);
                    parentId = viewInst.getViewInstId() + "|" + srcBean.getPackageName();
                }
            }
            result.setIds(Arrays.asList(new String[]{parentId}));
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 5)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ViewTempInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @CustomAnnotation(imageClass = "spafont spa-icon-conf", index = 6, caption = "模板信息")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true, customRequestData = RequestPathEnum.treeview)
    public @ResponseBody
    ResultModel<AggregationTempForm> getViewTempInfo(String javaTempId) {
        ResultModel<AggregationTempForm> result = new ResultModel<AggregationTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaTempId);
            result.setData(new AggregationTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<AggregationTempForm> errorResult = new ErrorResultModel<AggregationTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
