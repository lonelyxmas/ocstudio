package com.ds.dsm.editor.view.menu;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.editor.view.JavaViewTree;
import com.ds.dsm.manager.view.ViewNav;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/java/view/context/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "菜单")
@Aggregation(type = AggregationType.menu, rootClass = JavaViewMenu.class)
public class JavaViewMenu {

    @RequestMapping(method = RequestMethod.POST, value = "ViewNav")
    @NavGroupViewAnnotation()
    @ModuleAnnotation(caption = "视图配置")
    @DialogAnnotation(width = "900")
    @CustomAnnotation(index = 1, caption = "DSM配置", imageClass = "spafont spa-icon-values")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treegridrow)
    @ResponseBody
    public ResultModel<ViewNav> getViewNav(String viewInstId) {
        ResultModel<ViewNav> result = new ResultModel<ViewNav>();
        return result;
    }

    @MethodChinaName(cname = "编译")
    @RequestMapping(method = RequestMethod.POST, value = "dsmBuild")
    @CustomAnnotation(imageClass = "xuicon spafont spa-icon-debug1", index = 2)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treegridrow, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaViewTree>> dsmBuild(String viewInstId) {
        TreeListResultModel<List<JavaViewTree>> resultModel = new TreeListResultModel<List<JavaViewTree>>();
        ESDChrome chrome = getCurrChromeDriver();
        try {
            ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            DSMFactory.getInstance().getViewManager().buildCustomView(viewInst, getCurrChromeDriver());
            DSMFactory.getInstance().reload();
            getCurrChromeDriver().printLog("编译完成！", true);
            resultModel.setIds(Arrays.asList(new String[]{viewInstId}));
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            chrome.printError(e.getMessage());
            ((ErrorListResultModel) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }


    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 3)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @MethodChinaName(cname = "删除")
    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "fa fa-lg fa-close", caption = "删除")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow, index = 4)
    public @ResponseBody
    ResultModel<Boolean> delete(String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            DSMFactory.getInstance().getViewManager().deleteViewInst(viewInstId, true);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
