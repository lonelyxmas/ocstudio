package com.ds.dsm.editor.view.menu;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.editor.view.JavaViewTree;
import com.ds.dsm.manager.view.ViewCreateView;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.RemoteConnectionManager;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping(value = {"/java/view/toolbar/"})
@MenuBarMenu(menuType = CustomMenuType.toolbar, showCaption = false, caption = "菜单", id = "JavaToolbar")
@Aggregation(type = AggregationType.menu, rootClass = JavaViewToolbar.class)
public class JavaViewToolbar {
    @MethodChinaName(cname = "")
    @RequestMapping(method = RequestMethod.POST, value = "reLoad")
    @CustomAnnotation(imageClass = "xuicon xui-refresh", index = 1, tips = "刷新")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaViewTree>> reLoad(String projectName) {
        TreeListResultModel<List<JavaViewTree>> result = new TreeListResultModel<>();
        try {
            List<ViewInst> viewInsts = DSMFactory.getInstance().getViewManager().getViewList(projectName);
            result = TreePageUtil.getTreeList(viewInsts, JavaViewTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "")
    @RequestMapping(value = "ImportView")
    @APIEventAnnotation(autoRun = true)
    @FormViewAnnotation
    @DialogAnnotation(width = "450", height = "380")
    @ModuleAnnotation
    @CustomAnnotation(index = 2, imageClass = "xui-icon-upload", tips = "导入工程")
    public @ResponseBody
    ResultModel<UPLoadView> importView() {
        ResultModel<UPLoadView> resultModel = new ResultModel<UPLoadView>();
        try {
            UPLoadView upLoadFile = new UPLoadView();
            resultModel.setData(upLoadFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;

    }

    @MethodChinaName(cname = "")
    @RequestMapping(value = {"download"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 3, tips = "打包下载", imageClass = "spafont spa-icon-select1")
    @ResponseBody
    public ResultModel<Boolean> download(String projectVersionName) {
        ESDChrome defaultChrome = getCurrChromeDriver();
        if (projectVersionName != null) {
            RemoteConnectionManager.getConntctionService(projectVersionName).execute(new Runnable() {
                @Override
                public void run() {
                    exportProject(projectVersionName, defaultChrome, false, true);
                }
            });
        }
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }

    @MethodChinaName(cname = "")
    @RequestMapping(value = {"CreateView"}, method = {RequestMethod.GET, RequestMethod.POST})
    @FormViewAnnotation
    @CustomAnnotation(imageClass = "xuicon xui-uicmd-add", index = 4, tips = "新建领域模型")
    @ModuleAnnotation
    @DialogAnnotation(height = "260", width = "380")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add, callback = CustomCallBack.TreeReloadNode)
    @ResponseBody
    public ResultModel<ViewCreateView> createDomain(String projectVersionName) {
        ResultModel<ViewCreateView> result = new ResultModel<ViewCreateView>();
        try {
            ViewInst viewInst = DSMFactory.getInstance().createDefaultView(projectVersionName, projectVersionName);
            result.setData(new ViewCreateView(viewInst));
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    private void exportProject(String projectVersionName, ESDChrome chrome, boolean deploy, boolean download) {
        try {
            ESDFacrory.getESDClient().exportProject(projectVersionName, chrome, deploy, download);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            chrome.execScript("xui.free('export')");
        }

    }

    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
