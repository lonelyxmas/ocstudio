package com.ds.dsm.editor.view.menu;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.debug.bean.ConfigModuleTree;
import com.ds.dsm.editor.action.JavaNewAction;
import com.ds.dsm.editor.view.JavaViewTree;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/java/view/context/package/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "菜单")
@Aggregation(type = AggregationType.menu, rootClass = JavaViewPackageMenu.class)
public class JavaViewPackageMenu {


    @RequestMapping(method = RequestMethod.POST, value = "paste")
    @CustomAnnotation(imageClass = "spafont spa-icon-paste", index = 2, caption = "粘贴")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaViewTree>> paste(String packageName, String sfilePath, String viewInstId, String sjavaTempId) {
        TreeListResultModel<List<JavaViewTree>> result = new TreeListResultModel<List<JavaViewTree>>();
        try {
            DSMFactory dsmFactory = DSMFactory.getInstance();
            File desFile = new File(sfilePath);
            ViewInst viewInst = dsmFactory.getViewManager().getViewInstById(viewInstId);
            JavaSrcBean srcBean = dsmFactory.getTempManager().genJavaSrc(desFile, viewInst, sjavaTempId);
            DSMFactory.getInstance().getBuildFactory().copy(srcBean, packageName);
            result.setIds(Arrays.asList(new String[]{viewInstId + "|" + packageName}));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reLoad")
    @CustomAnnotation(imageClass = "xuicon xui-refresh", index = 3, caption = "刷新")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaViewTree>> reLoad(String packageName, String viewInstId) {
        TreeListResultModel<List<JavaViewTree>> result = new TreeListResultModel<List<JavaViewTree>>();
        String id = viewInstId + "|" + packageName;
        result.setIds(Arrays.asList(new String[]{id}));
        return result;
    }

    @RequestMapping(value = {"split6"})
    @Split
    @CustomAnnotation(index = 4)
    @ResponseBody
    public ResultModel<Boolean> split6() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "newApp")
    @CustomAnnotation(imageClass = "xuicon xui-uicmd-add", index = 5)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "新建", imageClass = "xuicon xui-uicmd-add", index = 5)
    public JavaNewAction getJavaJarAction() {
        return new JavaNewAction();
    }


    @MethodChinaName(cname = "删除")
    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "xuicon xui-icon-minus", index = 6)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> delete(String domainId, String parentId, String path, String javaTempId) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();
        try {
            File desFile = new File(path);
            DSMFactory dsmFactory = DSMFactory.getInstance();
            DomainInst domainInst = dsmFactory.getAggregationManager().getDomainInstById(domainId);
            if (desFile.exists()) {
                if (desFile.isDirectory()) {
                    JavaPackage javaPackage = domainInst.getPackageByFile(desFile);
                    dsmFactory.getTempManager().deleteJavaPackage(javaPackage);
                } else {
                    JavaSrcBean srcBean = dsmFactory.getTempManager().genJavaSrc(desFile, domainInst, javaTempId);
                    dsmFactory.getTempManager().deleteJavaFile(srcBean);
                }
            }

            result.setIds(Arrays.asList(new String[]{parentId}));

        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }

    @RequestMapping(value = {"split7"})
    @Split
    @CustomAnnotation(index = 7)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
