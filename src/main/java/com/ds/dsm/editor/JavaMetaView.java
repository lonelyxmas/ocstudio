package com.ds.dsm.editor;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.editor.agg.JavaAggTree;
import com.ds.dsm.editor.repository.JavaRepositoryTree;
import com.ds.dsm.editor.view.JavaViewTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.VAlignType;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@RequestMapping("/java/nav/")
@MethodChinaName(cname = "视图配置")
@Controller
@Aggregation(type = AggregationType.aggregationRoot)
@ButtonViewsAnnotation(barLocation = BarLocationType.left, barSize = "3em", autoReload = false, barVAlign = VAlignType.top, sideBarStatus = SideBarStatusType.expand)
public class JavaMetaView {

    @CustomAnnotation(pid = true, hidden = true)
    private String projectVersionName;


    public JavaMetaView() {

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    @RequestMapping(method = RequestMethod.POST, value = "RepositoryTree")
    @ModuleAnnotation(dynLoad = true, caption = "资源库", imageClass = "iconfont iconchucun")
    @CustomAnnotation( index = 0)
    @NavTreeViewAnnotation
    @ResponseBody
    public TreeListResultModel<List<JavaRepositoryTree>> getRepositoryTree(String projectVersionName) {
        TreeListResultModel<List<JavaRepositoryTree>> result = TreePageUtil.getTreeList(Arrays.asList(projectVersionName), JavaRepositoryTree.class);
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "JavaAggTree")
    @CustomAnnotation( index = 1)
    @ModuleAnnotation(dynLoad = true, caption = "聚合应用", imageClass = "spafont spa-icon-c-databinder")
    @NavTreeViewAnnotation
    @APIEventAnnotation()
    @ResponseBody
    public TreeListResultModel<List<JavaAggTree>> getJavaAggTree(String projectVersionName) {
        TreeListResultModel<List<JavaAggTree>> result = TreePageUtil.getTreeList(Arrays.asList(projectVersionName), JavaAggTree.class);
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "JavaViewTree")
    @CustomAnnotation( index = 2)
    @ModuleAnnotation(dynLoad = true, caption = "视图工厂", imageClass = "spafont spa-icon-c-cssbox")
    @NavTreeViewAnnotation
    @ResponseBody
    public TreeListResultModel<List<JavaViewTree>> getJavaViewTree(String projectVersionName) {
        TreeListResultModel<List<JavaViewTree>> result = TreePageUtil.getTreeList(Arrays.asList(projectVersionName), JavaViewTree.class);
        return result;
    }

}
