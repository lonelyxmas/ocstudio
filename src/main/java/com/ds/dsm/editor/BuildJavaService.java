package com.ds.dsm.editor;

import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.nav.NavButtonViewsViewAnnotation;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.web.annotation.Pid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/java/")
public class BuildJavaService {

    @Pid
    public String projectVersionName;


    @RequestMapping(method = RequestMethod.POST, value = "BuildTree")
    @ModuleAnnotation(cache = false, caption = "JAVA树", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @NavButtonViewsViewAnnotation()
    @DynLoadAnnotation
    @ResponseBody
    public ResultModel<List<JavaMetaView>> getBuildTree(String projectVersionName) {
        ResultModel<List<JavaMetaView>> result = new ResultModel<List<JavaMetaView>>();
        return result;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
