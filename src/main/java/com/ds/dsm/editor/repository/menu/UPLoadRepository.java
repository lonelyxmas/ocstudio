package com.ds.dsm.editor.repository.menu;

import com.ds.dsm.editor.agg.service.AggJavaService;
import com.ds.dsm.editor.repository.service.RepositoryJavaService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.FileUploadAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.StretchType;

@FormAnnotation(stretchHeight = StretchType.last)
public class UPLoadRepository {

    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;

    @CustomAnnotation(pid = true, hidden = true)
    String packageName;

    @CustomAnnotation(pid = true, hidden = true)
    String sourceClassName;

    @FileUploadAnnotation(bindClass = RepositoryJavaService.class)
    @FieldAnnotation(componentType = ComponentType.FileUpload, haslabel = false, colSpan = -1, required = true)
    @CustomAnnotation(caption = "上传文件")
    String thumbnailFile;

    public UPLoadRepository() {

    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getThumbnailFile() {
        return thumbnailFile;
    }

    public void setThumbnailFile(String thumbnailFile) {
        this.thumbnailFile = thumbnailFile;
    }
}
