package com.ds.dsm.editor.repository.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.editor.repository.JavaRepositoryTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/java/repository/")
public class AllRepositoryService {


    @RequestMapping(method = RequestMethod.POST, value = "AllRepository")
    @ModuleAnnotation(dynLoad = true, caption = "资源域", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaRepositoryTree>> getAllRepository(String projectVersionName) {
        TreeListResultModel<List<JavaRepositoryTree>> result = new TreeListResultModel<>();
        try {
            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            result = TreePageUtil.getTreeList(Arrays.asList(repositoryInst), JavaRepositoryTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
