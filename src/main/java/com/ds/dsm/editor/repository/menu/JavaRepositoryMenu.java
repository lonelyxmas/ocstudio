package com.ds.dsm.editor.repository.menu;

import com.ds.config.ErrorListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.editor.repository.JavaRepositoryTree;
import com.ds.dsm.manager.repository.RepositoryNav;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/java/repository/context/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "菜单")
@Aggregation(type = AggregationType.menu,rootClass =JavaRepositoryMenu.class )
public class JavaRepositoryMenu {


    @RequestMapping(method = RequestMethod.POST, value = "DSMInstNav")
    @NavGroupViewAnnotation()
    @ModuleAnnotation(caption = "资源库配置")
    @DialogAnnotation(width = "900")
    @CustomAnnotation(index = 1, caption = "DSM配置", imageClass = "spafont spa-icon-values")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treegridrow)
    @ResponseBody
    public ResultModel<RepositoryNav> getTempNav(String projectVersionName) {
        ResultModel<RepositoryNav> result = new ResultModel<RepositoryNav>();
        return result;
    }


    @MethodChinaName(cname = "编译")
    @RequestMapping(method = RequestMethod.POST, value = "dsmBuild")
    @CustomAnnotation(imageClass = "xuicon spafont spa-icon-debug1", index = 2)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treegridrow, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaRepositoryTree>> dsmBuild(String projectVersionName) {
        TreeListResultModel<List<JavaRepositoryTree>> resultModel = new TreeListResultModel<List<JavaRepositoryTree>>();
        ESDChrome chrome = getCurrChromeDriver();
        try {
            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            DSMFactory.getInstance().getRepositoryManager().buildRepository(repositoryInst, getCurrChromeDriver());
            DSMFactory.getInstance().reload();
            getCurrChromeDriver().printLog("编译完成！", true);
            resultModel.setIds(Arrays.asList(new String[]{projectVersionName}));
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            chrome.printError(e.getMessage());
            ((ErrorListResultModel) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 3)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
