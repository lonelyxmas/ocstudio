package com.ds.dsm.editor.repository.menu;

import com.ds.admin.db.nav.DBNavTreeView;
import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/java/repository/config/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "菜单")
@Aggregation(type = AggregationType.menu,rootClass =RepositoryConfigMenu.class )
public class RepositoryConfigMenu {

    @MethodChinaName(cname = "数据源配置")
    @RequestMapping(method = RequestMethod.POST, value = "ProviderList")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "900")
    @CustomAnnotation(imageClass = "iconfont iconchucun")
    @ModuleAnnotation(dynLoad = true, caption = "数据源配置")
    @ResponseBody
    public TreeListResultModel<List<DBNavTreeView>> getProviderList(String id, String projectName) {
        TreeListResultModel<List<DBNavTreeView>> result = new TreeListResultModel<>();
        ProjectVersion version = null;
        try {
            version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            result = TreePageUtil.getDefaultTreeList(Arrays.asList(version), DBNavTreeView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return result;

    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
