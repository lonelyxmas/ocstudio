package com.ds.dsm.editor.repository.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.editor.repository.JavaRepositoryTree;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/java/repository/")
public class RepositoryJavaService {


    @RequestMapping(method = RequestMethod.POST, value = "RepositoryTree")
    @ModuleAnnotation(dynLoad = true, caption = "资源域", imageClass = "iconfont iconchucun")
    @NavTreeViewAnnotation()
    @ResponseBody
    public TreeListResultModel<List<JavaRepositoryTree>> getRepositoryTree(String projectVersionName, String parentId) {
        TreeListResultModel<List<JavaRepositoryTree>> result = TreePageUtil.getTreeList(Arrays.asList(projectVersionName), JavaRepositoryTree.class);
        return result;
    }



    @RequestMapping(method = RequestMethod.POST, value = "AddRepository")
    @APIEventAnnotation(bindMenu = CustomMenuItem.upload)
    @CustomAnnotation(caption = "导入")
    public @ResponseBody
    ResultModel<Boolean> addDocument( @RequestParam("files") MultipartFile file) {
        ResultModel<Boolean> resultModel = new ResultModel<Boolean>();

        return resultModel;

    }


    @RequestMapping(method = RequestMethod.POST, value = "loadRepositoryPackage")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaRepositoryTree>> loadRepositoryPackage(String projectVersionName) {

        TreeListResultModel<List<JavaRepositoryTree>> result = new TreeListResultModel<>();
        try {
            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            JavaPackage rootPackage = repositoryInst.getRootPackage();
            List<JavaPackage> javaPackageList = rootPackage.getChildPackages();
            result = TreePageUtil.getTreeList(javaPackageList, JavaRepositoryTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }
}
