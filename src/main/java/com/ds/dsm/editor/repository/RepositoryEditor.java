package com.ds.dsm.editor.repository;


import com.ds.common.util.IOUtility;
import com.ds.dsm.editor.repository.menu.JavaRepositoryEditorTools;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.JavaEditorAnnotation;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.tool.ui.enums.StretchType;

import java.io.FileInputStream;
import java.io.IOException;


@ToolBarMenu(hAlign = HAlignType.left, handler = false, menuClasses = JavaRepositoryEditorTools.class)
@FormAnnotation(stretchHeight = StretchType.last)
public class RepositoryEditor {

    @CustomAnnotation(uid = true, hidden = true)
    String filePath;

    @CustomAnnotation(pid = true, hidden = true)
    String dsmId;

    @CustomAnnotation(pid = true, hidden = true)
    DSMType dsmType;

    @CustomAnnotation(pid = true, hidden = true)
    String javaTempId;

    @CustomAnnotation(pid = true, hidden = true)
    String sourceClassName;

    @JavaEditorAnnotation
    @FieldAnnotation(haslabel = false, colSpan = -1, rowHeight = "350", required = true)
    @CustomAnnotation()
    String content;

    public RepositoryEditor() {

    }

    public RepositoryEditor(JavaSrcBean javaSrcBean) {

        try {
            FileInputStream javaFileStream = new FileInputStream(javaSrcBean.getFile());
            this.dsmId = javaSrcBean.getDsmId();
            this.dsmType = javaSrcBean.getDsmType();
            this.javaTempId = javaSrcBean.getJavaTempId();
            this.sourceClassName = javaSrcBean.getSourceClassName();
            this.content = IOUtility.toString(javaFileStream, "utf-8");
            IOUtility.shutdownStream(javaFileStream);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDsmId() {
        return dsmId;
    }

    public void setDsmId(String dsmId) {
        this.dsmId = dsmId;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
