package com.ds.dsm.editor.repository;

import com.ds.dsm.editor.repository.menu.*;
import com.ds.dsm.editor.repository.service.AllRepositoryService;
import com.ds.dsm.editor.repository.service.RepositoryFileService;
import com.ds.dsm.editor.repository.service.RepositoryJavaService;
import com.ds.dsm.editor.repository.service.RepositoryPackageService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(closeBtn = true, noHandler = false)
@ToolBarMenu(menuClasses = JavaRepositoryToolbar.class)
@TreeAnnotation(caption = "视图编辑",
        customService = RepositoryJavaService.class,
        size = 300, lazyLoad = true, heplBar = true)
public class JavaRepositoryTree extends TreeListItem {

    @Pid
    String projectVersionName;

    @Pid
    String filePath;

    @Pid
    String packageName;

    @Pid
    String parentId;

    @Pid
    String sourceClassName;

    @Pid
    String javaTempId;

    @RightContextMenu(menuClass = RepositoryConfigMenu.class)
    @TreeItemAnnotation(imageClass = "iconfont iconchucun", bindService = AllRepositoryService.class, lazyLoad = true, dynDestory = true)
    public JavaRepositoryTree(String projectVersionName) {
        this.caption = DSMType.repository.getName() + "(" + projectVersionName + ")";
        this.id = DSMType.repository.getType() + projectVersionName;
        this.projectVersionName = projectVersionName;
        this.parentId = id;

    }

    @RightContextMenu(menuClass = JavaRepositoryMenu.class)
    @TreeItemAnnotation(bindService = RepositoryJavaService.class, imageClass = "iconfont iconchucun", lazyLoad = true, dynDestory = true)
    public JavaRepositoryTree(RepositoryInst repositoryInst) {
        this.caption = (repositoryInst.getDesc() == null || repositoryInst.getDesc().equals("")) ? repositoryInst.getName() : repositoryInst.getDesc();
        this.projectVersionName = repositoryInst.getProjectVersionName();
        this.id = repositoryInst.getProjectVersionName();
        this.parentId = DSMType.aggregation.getType() + repositoryInst.getProjectVersionName();

    }

    @RightContextMenu(menuClass = JavaRepositoryPackageMenu.class)
    @TreeItemAnnotation(imageClass = "spafont spa-icon-package", bindService = RepositoryPackageService.class, iniFold = true, lazyLoad = true, dynDestory = true)
    public JavaRepositoryTree(JavaPackage javaPackage, String projectVersionName) {
        this.caption = javaPackage.getPackageName();
        this.id = projectVersionName + "|" + javaPackage.getId();
        this.projectVersionName = projectVersionName;
        this.filePath = javaPackage.getPackageFile().getPath();
        this.packageName = javaPackage.getPackageName();
        this.parentId = projectVersionName;

    }

    @RightContextMenu(menuClass = JavaRepositoryFileMenu.class)
    @TreeItemAnnotation(imageClass = "spafont spa-icon-json-file", bindService = RepositoryFileService.class, closeBtn = true)
    public JavaRepositoryTree(JavaSrcBean javaFile) {
        this.caption = javaFile.getName();
        this.javaTempId = javaFile.getJavaTempId();
        this.packageName = javaFile.getPackageName();
        this.projectVersionName = javaFile.getDsmId();
        this.sourceClassName = javaFile.getClassName();
        this.id = javaFile.getClassName();
        this.filePath = javaFile.getFile().getAbsolutePath();
        this.parentId = projectVersionName + "|" + packageName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }


    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }
}
