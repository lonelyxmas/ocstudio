package com.ds.dsm.editor.repository.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.editor.repository.JavaRepositoryTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/java/repository/")
public class RepositoryPackageService {


    @RequestMapping(method = RequestMethod.POST, value = "loadRepositoryFile")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<JavaRepositoryTree>> loadRepositoryFile(String id, String filePath, String projectVersionName) {
        TreeListResultModel<List<JavaRepositoryTree>> result = new TreeListResultModel<>();
        List<Object> javaFileList = new ArrayList<>();
        File desFile = new File(filePath);
        try {
            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            JavaPackage javaPackage = repositoryInst.getPackageByFile(desFile);
            if (javaPackage != null) {
                javaFileList.addAll(javaPackage.listFiles());
                javaFileList.addAll(javaPackage.listChildren());
            }

            result = TreePageUtil.getTreeList(javaFileList, JavaRepositoryTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }
}
