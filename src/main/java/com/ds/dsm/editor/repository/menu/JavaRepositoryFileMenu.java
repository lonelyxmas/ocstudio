package com.ds.dsm.editor.repository.menu;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.admin.temp.JavaTempForm;
import com.ds.dsm.editor.repository.JavaRepositoryTree;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.dsm.temp.JavaTemp;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import com.ds.web.json.JSONData;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = {"/java/repository/context/file/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "菜单")
@Aggregation(type = AggregationType.menu,rootClass = JavaRepositoryFileMenu.class)
public class JavaRepositoryFileMenu {


    @RequestMapping(method = RequestMethod.POST, value = "copy")
    @CustomAnnotation(imageClass = "spafont spa-icon-copy", index = 1, caption = "复制")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview)
    public @ResponseBody
    ResultModel<Boolean> copy(String id, @JSONData Map<String, String> tagVar) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "RepositoryTempInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "850", height = "750")
    @CustomAnnotation(imageClass = "spafont spa-icon-conf", index = 2, caption = "模板信息")
    @ModuleAnnotation
    @APIEventAnnotation(autoRun = true, customRequestData = RequestPathEnum.treeview)
    public @ResponseBody
    ResultModel<JavaTempForm> getRepositoryTempInfo(String javaTempId) {
        ResultModel<JavaTempForm> result = new ResultModel<JavaTempForm>();
        try {
            JavaTemp temp = DSMFactory.getInstance().getTempManager().getJavaTempById(javaTempId);
            result.setData(new JavaTempForm(temp));
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<JavaTempForm> errorResult = new ErrorResultModel<JavaTempForm>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 3)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }

    @MethodChinaName(cname = "")
    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "xuicon xui-icon-minus", index = 4, caption = "删除")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaRepositoryTree>> delete(String id, String javaTempId, String projectVersionName, String filePath) {
        TreeListResultModel<List<JavaRepositoryTree>> result = new TreeListResultModel<List<JavaRepositoryTree>>();
        try {
            File desFile = new File(filePath);
            DSMFactory dsmFactory = DSMFactory.getInstance();
            RepositoryInst repositoryInst = dsmFactory.getRepositoryManager().getProjectRepository(projectVersionName);
            String parentId = projectVersionName;
            if (desFile.exists()) {
                if (desFile.isDirectory()) {
                    JavaPackage javaPackage = repositoryInst.getPackageByFile(desFile);
                    dsmFactory.getTempManager().deleteJavaPackage(javaPackage);
                } else {
                    JavaSrcBean srcBean = dsmFactory.getTempManager().genJavaSrc(desFile, repositoryInst, javaTempId);
                    dsmFactory.getTempManager().deleteJavaFile(srcBean);
                    parentId = projectVersionName + "|" + srcBean.getPackageName();
                }
            }
            result.setIds(Arrays.asList(new String[]{parentId}));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
