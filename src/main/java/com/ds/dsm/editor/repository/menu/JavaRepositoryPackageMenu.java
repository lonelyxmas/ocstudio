package com.ds.dsm.editor.repository.menu;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.debug.bean.ConfigModuleTree;
import com.ds.dsm.editor.action.JavaNewAction;
import com.ds.dsm.editor.repository.JavaRepositoryTree;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaPackage;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Split;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.util.Arrays;
import java.util.List;


@Controller
@RequestMapping(value = {"/java/repository/context/package/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "菜单")
@Aggregation(type = AggregationType.menu, rootClass = JavaRepositoryPackageMenu.class)
public class JavaRepositoryPackageMenu {


    @RequestMapping(method = RequestMethod.POST, value = "paste")
    @CustomAnnotation(imageClass = "spafont spa-icon-paste", index = 2, caption = "粘贴")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaRepositoryTree>> paste(String packageName, String sfilePath, String projectVersionName, String sjavaTempId) {
        TreeListResultModel<List<JavaRepositoryTree>> result = new TreeListResultModel<List<JavaRepositoryTree>>();
        try {
            DSMFactory dsmFactory = DSMFactory.getInstance();
            File desFile = new File(sfilePath);
            RepositoryInst repositoryInst = dsmFactory.getRepositoryManager().getProjectRepository(projectVersionName);
            JavaSrcBean srcBean = dsmFactory.getTempManager().genJavaSrc(desFile, repositoryInst, sjavaTempId);
            DSMFactory.getInstance().getBuildFactory().copy(srcBean, packageName);
            result.setIds(Arrays.asList(new String[]{projectVersionName + "|" + packageName}));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }

    @RequestMapping(method = RequestMethod.POST, value = "reLoad")
    @CustomAnnotation(imageClass = "xuicon xui-refresh", index = 3, caption = "刷新")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<JavaRepositoryTree>> reLoad(String projectVersionName, String packageName) {
        TreeListResultModel<List<JavaRepositoryTree>> result = new TreeListResultModel<>();
        String id = projectVersionName + "|" + packageName;
        result.setIds(Arrays.asList(new String[]{id}));
        return result;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 4)
    @ResponseBody
    public ResultModel<Boolean> split6() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "newApp")
    @CustomAnnotation(imageClass = "xuicon xui-uicmd-add", index = 5)
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "新建", imageClass = "xuicon xui-uicmd-add", index = 5)
    public JavaNewAction getJavaJarAction() {
        return new JavaNewAction();
    }


    @MethodChinaName(cname = "删除")
    @RequestMapping(method = RequestMethod.POST, value = "delete")
    @CustomAnnotation(imageClass = "xuicon xui-icon-minus", index = 6)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> delete(String projectVersionName, String packageName, String filePath, String javatempId) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();
        try {
            DSMFactory dsmFactory = DSMFactory.getInstance();
            RepositoryInst repositoryInst = dsmFactory.getRepositoryManager().getProjectRepository(projectVersionName);
            JavaPackage javaPackage = repositoryInst.getPackageByName(packageName);
            dsmFactory.getTempManager().deleteJavaPackage(javaPackage);
            result.setIds(Arrays.asList(new String[]{javaPackage.getParent().getId()}));

        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }

    @RequestMapping(value = {"split"})
    @Split
    @CustomAnnotation(index = 7)
    @ResponseBody
    public ResultModel<Boolean> split() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        return result;
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
