package com.ds.dsm.editor.repository.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.editor.repository.RepositoryEditor;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.repository.RepositoryInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;

@Controller
@RequestMapping("/java/repository/")
public class RepositoryFileService {


    @RequestMapping(method = RequestMethod.POST, value = "RepositoryJavaEditor")
    @ModuleAnnotation(caption = "JAVA树", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @FormViewAnnotation()
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<RepositoryEditor> getRepositoryJavaEditor(String filePath, String projectVersionName, String javatempId, DSMType dsmType, String sourceClassName, String methodName) {
        ResultModel<RepositoryEditor> result = new ResultModel<>();
        File desFile = new File(filePath);
        JavaSrcBean srcBean = null;
        try {
            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            srcBean = DSMFactory.getInstance().getTempManager().genJavaSrc(desFile, repositoryInst, javatempId);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        if (sourceClassName != null && !sourceClassName.equals("")) {
            srcBean.setSourceClassName(sourceClassName);
        }
        if (methodName != null && !methodName.equals("")) {
            srcBean.setMethodName(methodName);
        }
        if (dsmType != null) {
            srcBean.setDsmType(dsmType);
        }
        RepositoryEditor editor = new RepositoryEditor(srcBean);
        result.setData(editor);
        return result;
    }


}
