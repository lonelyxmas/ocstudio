package com.ds.dsm.editor.repository.menu;

import com.ds.common.JDSException;
import com.ds.common.util.IOUtility;
import com.ds.common.util.java.DynamicClassLoader;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.editor.action.BaseEditorTools;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.gen.GenJava;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;


@Controller
@RequestMapping(value = {"/java/repository/editor/"})
@MenuBarMenu(menuType = CustomMenuType.toolbar, caption = "菜单", hAlign = HAlignType.right, id = "JavaEditorToolbar")
@Aggregation(type = AggregationType.menu,rootClass =JavaRepositoryEditorTools.class )
public class JavaRepositoryEditorTools extends BaseEditorTools {

    @MethodChinaName(cname = "")
    @RequestMapping(method = RequestMethod.POST, value = "saveJava")
    @ComboInputAnnotation(inputType = ComboInputType.button)
    @CustomAnnotation(index = 0, caption = "保存", tips = "保存", imageClass = "spafont spa-icon-save")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.form, RequestPathEnum.ctx})
    public @ResponseBody
    ResultModel<Boolean> saveJava(String content, String projectVersionName, String javaTempId, String filePath) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            File desFile = new File(filePath);
            JavaSrcBean srcBean = null;
            try {
                RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                srcBean = DSMFactory.getInstance().getTempManager().genJavaSrc(desFile, repositoryInst, javaTempId);
            } catch (JDSException e) {
                e.printStackTrace();
            }
            String className = srcBean.getClassName();
            ByteArrayInputStream input = new ByteArrayInputStream(content.getBytes("utf-8"));
            FileOutputStream output = new FileOutputStream(desFile);
            IOUtility.copy(input, output);
            IOUtility.shutdownStream(input);
            IOUtility.shutdownStream(output);
            Class clazz = GenJava.getInstance(projectVersionName).dynCompile(className, content);
            File packageFile = desFile.getParentFile();
            if (className.indexOf(".") > -1) {
                className = className.substring(className.lastIndexOf(".") + 1);
            }
            className = className + ".class";
            File classFile = new File(packageFile, className);
            DynamicClassLoader loader = (DynamicClassLoader) clazz.getClassLoader();
            loader.dumpFile(classFile);
            DSMFactory.getInstance().dyReload();


        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


}
