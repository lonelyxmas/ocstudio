package com.ds.dsm.view.entity;

import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.APIConfig;
import com.ds.web.annotation.AggregationType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = ViewEntityService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class ViewEntityTree extends TreeListItem {
    @Pid
    String viewInstId;
    @Pid
    AggregationType aggregationType;
    @Pid
    String domainId;
    @Pid
    String euClassName;


    @TreeItemAnnotation(bindService = EntityTreeService.class)
    public ViewEntityTree(AggregationType aggregationType, String viewInstId) {
        this.caption = aggregationType.getName();
        this.viewInstId = viewInstId;
        this.id = aggregationType.getType();
        this.imageClass = aggregationType.getImageClass();
        this.aggregationType = aggregationType;


    }


    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-grid")
    public ViewEntityTree(APIConfig apiConfig, String viewInstId) {
        this.caption = apiConfig.getName() + "(" + apiConfig.getDesc() + ")";
        this.euClassName = apiConfig.getClassName();
        this.id = apiConfig.getClassName();

    }


    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-grid")
    public ViewEntityTree(ESDClass esdClass, String viewInstId) {
        this.caption = esdClass.getName() + "(" + esdClass.getDesc() + ")";
        this.euClassName = esdClass.getClassName();
        this.viewInstId = viewInstId;
        this.domainId = esdClass.getDomainId();
        this.id = esdClass.getClassName();
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public AggregationType getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(AggregationType aggregationType) {
        this.aggregationType = aggregationType;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

}
