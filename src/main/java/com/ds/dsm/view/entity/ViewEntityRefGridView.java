package com.ds.dsm.view.entity;

import com.ds.dsm.repository.entity.ref.EntityRefService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.gallery.annotation.GalleryAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.view.ref.ViewEntityRef;
import com.ds.web.annotation.Required;

@PageBar
@GalleryAnnotation()
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {EntityRefService.class}, event = CustomGridEvent.editor)
public class ViewEntityRefGridView {


    @CustomAnnotation(uid = true, hidden = true)
    String refId;

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(caption = "关联表", hidden = true, pid = true)
    String className;

    @CustomAnnotation(caption = "名称", pid = true)
    String name;

    @Required
    @CustomAnnotation(caption = "引用关系")
    String ref;

    @CustomAnnotation(caption = "关联字段", pid = true)
    String methodName;

    @CustomAnnotation(caption = "关联对象")
    String otherClassName;


    ViewEntityRefGridView() {

    }


    public ViewEntityRefGridView(ViewEntityRef ref) {
        this.name = ref.getRefBean().getRef().getName();
        this.viewInstId = ref.getViewInstId();
        this.domainId=ref.getDomainId();
        this.ref = ref.getRefBean().getRef().getType();
        this.className = ref.getClassName();
        this.otherClassName = ref.getOtherClassName();
        this.methodName = ref.getMethodName();
        this.refId = ref.getRefId();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getOtherClassName() {
        return otherClassName;
    }

    public void setOtherClassName(String otherClassName) {
        this.otherClassName = otherClassName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
}
