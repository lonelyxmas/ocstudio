package com.ds.dsm.view.entity;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.repository.entity.ref.EntityFieldItem;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ref.ViewEntityRef;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping(path = "/dsm/manager/view/entity/ref/")
@MethodChinaName(cname = "关联表信息", imageClass = "spafont spa-icon-c-databinder")

public class ViewEntityRefService {

    @RequestMapping(method = RequestMethod.POST, value = "RefInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "300", height = "350")
    @ModuleAnnotation(caption = "关联表信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<ViewEntityRefFormView> getRefInfo(String refId, String projectVersionName) {
        ResultModel<ViewEntityRefFormView> result = new ResultModel<ViewEntityRefFormView>();
        try {
            ViewEntityRef ref = DSMFactory.getInstance().getViewManager().getViewEntityRefById(refId, projectVersionName);
            result.setData(new ViewEntityRefFormView(ref));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "CreateRefInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "300", height = "350")
    @ModuleAnnotation(caption = "关联表信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public ResultModel<ViewEntityRefFormView> createRefInfo(String className, String refId, String viewInstId) {
        ResultModel<ViewEntityRefFormView> result = new ResultModel<ViewEntityRefFormView>();
        ViewEntityRef ref = new ViewEntityRef();
        ref.setClassName(className);
        ref.setViewInstId(viewInstId);
        refId = UUID.randomUUID().toString();
        ref.setRefId(refId);
        result.setData(new ViewEntityRefFormView(ref));
        return result;

    }

    @MethodChinaName(cname = "更新外键关系")
    @RequestMapping(value = {"updateRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateRef(@RequestBody ViewEntityRef ref) {
        ResultModel result = new ResultModel<>();
        try {
            DSMFactory factory = DSMFactory.getInstance(ESDFacrory.getESDClient().getSpace());
            String otherClassName = null;
            if (ref.getRefBean() == null) {
                throw new JDSException("关系不能为空！");
            }
            String mainClassName = ref.getClassName();
            ref.setOtherClassName(otherClassName);
            factory.getViewManager().updateViewEntityRef(ref);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }

        return result;

    }

    @MethodChinaName(cname = "删除关联信息")
    @RequestMapping(value = {"deleteRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> deleteRef(String refId, String viewInstId) {
        ResultModel result = new ResultModel<>();
        try {
            DSMFactory factory = DSMFactory.getInstance(ESDFacrory.getESDClient().getSpace());
            factory.getRepositoryManager().delTableRef(refId, viewInstId);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }

        return new ResultModel<>();

    }


    @RequestMapping(value = {"AddFkField"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation(fieldId = "fkField",
            fieldCaption = "fkField",
            saveUrl = "updateFk")
    @DialogAnnotation(width = "350", height = "400")
    @ModuleAnnotation(dynLoad = true, caption = "添加外键关系", dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<EntityFieldItem>> addAddFkField(String className, String projectId, String viewInstId) {
        TreeListResultModel<List<EntityFieldItem>> result = new TreeListResultModel<List<EntityFieldItem>>();
        try {
            List<EntityFieldItem> list = new ArrayList<>();
            list.add(new EntityFieldItem(viewInstId));
            result.setData(list);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "添加主键")
    @RequestMapping(value = {"AddPkField"}, method = {RequestMethod.GET, RequestMethod.POST})

    @PopTreeViewAnnotation(fieldId = "pkField",
            fieldCaption = "pkField",
            saveUrl = "updatePk")
    @DialogAnnotation( width = "350", height = "400")
    @ModuleAnnotation(dynLoad = true, caption = "添加主键",dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<EntityFieldItem>> addPkField(String className, String viewInstId, String projectId) {
        TreeListResultModel<List<EntityFieldItem>> result = new TreeListResultModel<List<EntityFieldItem>>();
        try {
            List<EntityFieldItem> list = new ArrayList<>();
            list.add(new EntityFieldItem(viewInstId));
            result.setData(list);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "添加显示选项")
    @RequestMapping(value = {"AddCaptionField"}, method = {RequestMethod.GET, RequestMethod.POST})

    @PopTreeViewAnnotation(
            fieldId = "mainCaption",
            fieldCaption = "mainCaption",
            saveUrl = "dsm.manager.table.ref.updateCaption"
    )
    @DialogAnnotation(width = "350", height = "400")
    @ModuleAnnotation(dynLoad = true, caption = "添加显示选项", dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<EntityFieldItem>> addCaptionField(String className, String viewInstId, String projectId) {
        TreeListResultModel<List<EntityFieldItem>> result = new TreeListResultModel<List<EntityFieldItem>>();
        try {
            List<EntityFieldItem> list = new ArrayList<>();
            list.add(new EntityFieldItem(viewInstId));
            result.setData(list);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "添加显示选项")
    @RequestMapping(value = {"AddOtherCaptionField"}, method = {RequestMethod.GET, RequestMethod.POST})

    @PopTreeViewAnnotation(fieldId = "otherCaption",
            fieldCaption = "otherCaption",
            saveUrl = "updateOtherCaption")
    @DialogAnnotation(width = "350", height = "400")
    @ModuleAnnotation(dynLoad = true, caption = "添加显示选项", dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<EntityFieldItem>> addOtherCaptionField(String className, String viewInstId, String projectId) {
        TreeListResultModel<List<EntityFieldItem>> result = new TreeListResultModel<List<EntityFieldItem>>();
        try {
            List<EntityFieldItem> list = new ArrayList<>();
            list.add(new EntityFieldItem(viewInstId));
            result.setData(list);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "更新外键关系")
    @RequestMapping(value = {"updateFk"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> updateFk(String viewInstId, String className, String AddFkFieldTree) {
        return new ResultModel<>();

    }


    @MethodChinaName(cname = "更新外键关系")
    @RequestMapping(value = {"updatePk"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> updatePk(String className, String AddPkFieldTree) {
        return new ResultModel<>();

    }

    @MethodChinaName(cname = "更新库表关系")
    @RequestMapping(value = {"updateCaption"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> updateCaption(String className, String AddCaptionFieldTree) {
        return new ResultModel<>();

    }

    @MethodChinaName(cname = "更新外键库表关系")
    @RequestMapping(value = {"updateOtherCaption"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> updateOtherCaption(String className, String AddCaptionFieldTree) {
        return new ResultModel<>();
    }

}
