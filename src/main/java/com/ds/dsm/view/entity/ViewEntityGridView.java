package com.ds.dsm.view.entity;


import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.DSMFactory;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {ViewEntityService.class}, event = CustomGridEvent.editor)
public class ViewEntityGridView {

    @CustomAnnotation(hidden = true, pid = true)
    public String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    public String domainId;

    @CustomAnnotation(uid = true, hidden = true)
    public String sourceClassName;


    @FieldAnnotation(  required = true)
    @CustomAnnotation(caption = "名称")
    public String name;

    @FieldAnnotation(  required = true)
    @CustomAnnotation(caption = "包名")
    public String packageName;


    @CustomAnnotation(caption = "描述", hidden = true)
    private String desc;



    public ViewEntityGridView(ESDClass esdClass) {
        this.name = esdClass.getName();
        this.viewInstId = esdClass.getViewInstId();
        this.domainId = esdClass.getDomainId();
        this.sourceClassName = esdClass.getCtClass().getName();
        try {
            sourceClassName = DSMFactory.getInstance().getViewManager().changeSourceClass(sourceClassName, viewInstId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.packageName = esdClass.getCtClass().getPackage().getName();
        this.desc = esdClass.getDesc();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
