package com.ds.dsm.view.entity;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.DomainInstTree;
import com.ds.dsm.editor.view.JavaViewTree;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.enums.DSMTempType;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/dsm/view/config/meta/")
@MethodChinaName(cname = "视图配置")
@Controller
@TreeAnnotation
public class ViewEntityMetaView {
    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;


    public ViewEntityMetaView() {

    }


    @RequestMapping(method = RequestMethod.POST, value = "ViewConfigTree")
    @APIEventAnnotation(autoRun = true)
    @TreeViewAnnotation
    @CustomAnnotation( index = 0)
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "视图配置")
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> getCustomViewConfig(String sourceClassName, String viewInstId, String domainId, String id) {
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<List<ViewConfigTree>>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId).getCurrConfig();
            List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
            List<CustomView> customViews = new ArrayList<>();
            for (MethodConfig methodAPIBean : customMethods) {
                if (methodAPIBean.getView() != null) {
                    customViews.add(methodAPIBean.getView());
                }
            }
            resultModel = TreePageUtil.getTreeList(customViews, ViewConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }


    @MethodChinaName(cname = "源码工程")
    @RequestMapping(method = RequestMethod.POST, value = "SoruceCode")
    @TreeViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @CustomAnnotation( index = 1)
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-databinder", dynLoad = true, caption = "源码工程")
    @ResponseBody
    public TreeListResultModel<List<JavaViewTree>> getSoruceCode(String viewInstId, String id, String parentId) {
        TreeListResultModel<List<JavaViewTree>> result = new TreeListResultModel<>();
        List<JavaViewTree> menuTrees = new ArrayList<>();
        String javaPath = null;
        try {
            ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            JavaViewTree tree = new JavaViewTree(viewInst);
            tree.setIniFold(false);
            menuTrees.add(tree);
            result.setData(menuTrees);
            if (id != null && !id.equals("")) {
                String[] orgIdArr = StringUtility.split(id, ";");
                result.setIds(Arrays.asList(orgIdArr));
            } else {
                result.setIds(Arrays.asList(new String[]{tree.getFristClassItem(tree).getId()}));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }


    @MethodChinaName(cname = "视图模板")
    @RequestMapping(method = RequestMethod.POST, value = "ViewNavTemps")
    @APIEventAnnotation(autoRun = true)
    @TreeViewAnnotation
    @CustomAnnotation( index = 2)
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", dynLoad = true, caption = "视图模板")
    @ResponseBody
    public TreeListResultModel<List<DomainInstTree>> getViewNavTemps(String id, String sourceClassName, String projectVersionName, String domainId, String viewInstId) {
        TreeListResultModel<List<DomainInstTree>> resultModel = new TreeListResultModel<List<DomainInstTree>>();
        DomainInstTree viewItem = null;
        try {
            viewItem = new DomainInstTree(DSMTempType.custom, projectVersionName, domainId, viewInstId, sourceClassName, false);
            List<DomainInstTree> items = new ArrayList<>();
            items.add(viewItem);
            if (id != null && !id.equals("")) {
                String[] orgIdArr = StringUtility.split(id, ";");
                resultModel.setIds(Arrays.asList(orgIdArr));
            } else {
                resultModel.setIds(Arrays.asList(new String[]{((DomainInstTree) viewItem.getSub().get(0)).getId()}));
            }

            resultModel.setData(items);
        } catch (JDSException e) {
            resultModel = new ErrorListResultModel<>();
            ((ErrorListResultModel<List<DomainInstTree>>) resultModel).setErrdes(e.getMessage());
        }

        return resultModel;

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String className) {
        this.sourceClassName = className;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }


}
