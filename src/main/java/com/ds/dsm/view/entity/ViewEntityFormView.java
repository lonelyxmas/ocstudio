package com.ds.dsm.view.entity;


import com.ds.common.JDSException;
import com.ds.dsm.aggregation.config.entity.AggEntityService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(customService = AggEntityService.class)
public class ViewEntityFormView {

    @CustomAnnotation(hidden = true, pid = true)
    public String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    public String domainId;

    @CustomAnnotation(uid = true, hidden = true)
    public String sourceClassName;


    @Required
    @CustomAnnotation(caption = "URL")
    public String url;

    @Required
    @CustomAnnotation(caption = "名称")
    public String name;

    @Required
    @CustomAnnotation(caption = "包名")
    public String packageName;

    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "描述")
    private String desc;

    public ViewEntityFormView(ViewEntityConfig entityConfig) {
        this.name = entityConfig.getSourceClass().getName();
        this.domainId = entityConfig.getDomainId();
        this.viewInstId = entityConfig.getViewInstId();
        this.sourceClassName = entityConfig.getSourceClass().getName();
        this.packageName = entityConfig.getSourceClass().getCtClass().getPackage().getName();
        this.desc = entityConfig.getSourceClass().getDesc();
        try {
            entityConfig.getRequestMapping().getFristUrl();
            if (entityConfig.getServiceConfig().size() > 0) {
                this.url = entityConfig.getServiceConfig().iterator().next().getUrl();
            } else {
                this.url = entityConfig.getCurrConfig().getUrl();
            }
            if (this.url == null || this.url.equals("")) {
                this.url = entityConfig.getSourceConfig().getUrl();
            }

            if (this.url == null || this.url.equals("")) {
                ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(entityConfig.getViewInstId());
                if (viewInst == null) {
                    viewInst = DSMFactory.getInstance().getDefaultView(DSMFactory.getInstance().getProjectName());
                    url = viewInst.getSpace() + "/" + entityConfig.getCurrClass().getName();
                }
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
