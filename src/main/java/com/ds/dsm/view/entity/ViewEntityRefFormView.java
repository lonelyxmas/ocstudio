package com.ds.dsm.view.entity;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.ref.ViewEntityRef;
import com.ds.web.annotation.RefType;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(col = 1, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {ViewEntityRefService.class})
public class ViewEntityRefFormView {


    @CustomAnnotation(uid = true, hidden = true)
    String refId;

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(caption = "表名", pid = true)
    String className;

    @CustomAnnotation(caption = "关联对象")
    String otherClassName;

    @Required
    @CustomAnnotation(caption = "引用关系")
    RefType ref;


    ViewEntityRefFormView() {

    }


    public ViewEntityRefFormView(ViewEntityRef ref) {
        this.viewInstId = ref.getViewInstId();
        this.ref = ref.getRefBean().getRef();
        this.className = ref.getClassName();
        this.otherClassName = ref.getOtherClassName();
        this.refId = ref.getRefId();


    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getOtherClassName() {
        return otherClassName;
    }

    public void setOtherClassName(String otherClassName) {
        this.otherClassName = otherClassName;
    }

    public RefType getRef() {
        return ref;
    }

    public void setRef(RefType ref) {
        this.ref = ref;
    }

}
