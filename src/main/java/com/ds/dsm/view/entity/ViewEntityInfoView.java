package com.ds.dsm.view.entity;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.aggregation.api.method.APIMethodBaseGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.ref.ViewEntityRef;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/view/config/entity/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "视图详细信息", imageClass = "spafont spa-icon-c-databinder")
public class ViewEntityInfoView {
    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @RequestMapping(method = RequestMethod.POST, value = "AllEntityView")
    @ModuleAnnotation(caption = "视图", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @CustomAnnotation( index = 0)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<APIMethodBaseGridView>> getAllEntityView(String sourceClassName,String domainId, String viewInstId) {
        ListResultModel<List<APIMethodBaseGridView>> result = new ListResultModel<List<APIMethodBaseGridView>>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,viewInstId);
            List<MethodConfig> attMethods = tableConfig.getSourceConfig().getAllViewMethods();
            result = PageUtil.getDefaultPageList(attMethods, APIMethodBaseGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AttMethod")
    @ModuleAnnotation(caption = "方法", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @CustomAnnotation( index = 1)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<APIMethodBaseGridView>> getAttMethod(String sourceClassName,String domainId, String viewInstId) {
        ListResultModel<List<APIMethodBaseGridView>> result = new ListResultModel<List<APIMethodBaseGridView>>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            List<MethodConfig> attMethods = new ArrayList<>();
            for (MethodConfig apiBean : tableConfig.getSourceConfig().getAllMethods()) {
                if (!apiBean.isModule()) {
                    attMethods.add(apiBean);
                }
            }
            result = PageUtil.getDefaultPageList(attMethods, APIMethodBaseGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "ViewEntityRef")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-databinder", caption = "关联关系")
    @CustomAnnotation( index = 2)
    @ResponseBody
    public ListResultModel<List<ViewEntityRefGridView>> getViewEntityRef(String sourceClassName,String domainId, String viewInstId) {
        ListResultModel<List<ViewEntityRefGridView>> tables = new ListResultModel();
        try {
            DSMFactory factory = DSMFactory.getInstance(ESDFacrory.getESDClient().getSpace());
            ESDClass esdClass =  DSMFactory.getInstance().getClassManager().getViewClassByName(sourceClassName, viewInstId, false);
            String entityClassName = esdClass.getTopSourceClass().getEntityClass().getClassName();
            List<ViewEntityRef> dsmRefs = factory.getViewManager().getViewEntityRefByName(entityClassName,viewInstId);
            tables = PageUtil.getDefaultPageList(dsmRefs, ViewEntityRefGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return tables;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
