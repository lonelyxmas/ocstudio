package com.ds.dsm.view.entity;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/entity/select/")
public class EntityTreeService {


    @RequestMapping(method = RequestMethod.POST, value = "loadRoot")
    @ModuleAnnotation(dynLoad = true, caption = "装载跟实体")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewEntityTree>> loadRoot(AggregationType aggregationType, String viewInstId) {
        TreeListResultModel<List<ViewEntityTree>> result = new TreeListResultModel<>();
        List<ESDClass> esdServiceBeans = new ArrayList<>();
        try {
            switch (aggregationType) {
                case aggregationEntity:
                    esdServiceBeans = DSMFactory.getInstance().getClassManager().getAggEntities();
                    break;
                case customDomain:
                    esdServiceBeans = DSMFactory.getInstance().getClassManager().getAllAggDomain();
                    break;
                case customapi:
                    esdServiceBeans = DSMFactory.getInstance().getClassManager().getAllAggAPI();
                    break;
                case aggregationRoot:
                    esdServiceBeans = DSMFactory.getInstance().getClassManager().getAllAggAPI();
                    break;
                case menu:
                    esdServiceBeans = DSMFactory.getInstance().getClassManager().getAllAggMenu();
                    break;
                case view:
                    esdServiceBeans = DSMFactory.getInstance().getClassManager().getAllAggView();
                    break;
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

        result = TreePageUtil.getTreeList(esdServiceBeans, ViewEntityTree.class);
        return result;
    }


}
