package com.ds.dsm.view.entity;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/agg/select/")
public class AggTreeService {


    @RequestMapping(method = RequestMethod.POST, value = "loadRoot")
    @ModuleAnnotation(dynLoad = true, caption = "装载跟实体")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggEntityTree>> loadRoot(AggregationType aggregationType, String viewInstId) {
        TreeListResultModel<List<AggEntityTree>> result = new TreeListResultModel<>();
        List<ESDClass> esdServiceBeans = new ArrayList<>();
        try {
            ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(viewInst.getDomainId());
            switch (aggregationType) {
                case aggregationEntity:
                    esdServiceBeans = domainInst.getAggEntities();
                    break;
                case customDomain:
                    esdServiceBeans = domainInst.getAggDomains();
                    break;
                case customapi:
                    esdServiceBeans = domainInst.getAggAPIs();
                    break;
                case aggregationRoot:
                    esdServiceBeans = domainInst.getAggRoots();
                    break;
                case menu:
                    esdServiceBeans = domainInst.getAggMenus();
                    break;
                case view:
                    esdServiceBeans = domainInst.getAggViews();
                    break;
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }
        result = TreePageUtil.getTreeList(esdServiceBeans, AggEntityTree.class);
        return result;
    }


}
