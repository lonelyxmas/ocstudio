package com.ds.dsm.view.entity;

import com.ds.common.JDSConstants;
import com.ds.common.JDSException;
import com.ds.common.logging.Log;
import com.ds.common.logging.LogFactory;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/entity/")
@MethodChinaName(cname = "视图实体", imageClass = "spafont spa-icon-c-gallery")

public class ViewEntityService {
    protected Log log = LogFactory.getLog(JDSConstants.CONFIG_KEY, ViewEntityService.class);

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "视图信息")
    @RequestMapping(method = RequestMethod.POST, value = "ViewEntityList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-buttonview", caption = "视图信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public ListResultModel<List<ViewEntityGridView>> getViewEntityList(String viewInstId) {
        ListResultModel<List<ViewEntityGridView>> result = new ListResultModel();
        try {
            ViewInst bean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            if (bean != null) {
                List<ESDClass> esdClassList = bean.getViewProxyClasses();
                result = PageUtil.getDefaultPageList(esdClassList, ViewEntityGridView.class);
            }
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "添加控制接口")
    @RequestMapping(value = {"ViewEntityTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "添加控制接口", dynLoad = true, dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<AggEntityTree>> getViewEntityTree(String viewInstId) {
        TreeListResultModel<List<AggEntityTree>> result = new TreeListResultModel<List<AggEntityTree>>();
        AggregationType[] aggregationTypes = new AggregationType[]{
                AggregationType.customapi,
                AggregationType.aggregationEntity,
                AggregationType.aggregationRoot,
                AggregationType.menu,
                AggregationType.view,
                AggregationType.aggregationSet,
                AggregationType.customDomain
        };

        result = TreePageUtil.getTreeList(Arrays.asList(aggregationTypes), AggEntityTree.class);

        return result;
    }

    @MethodChinaName(cname = "保存控制接口")
    @RequestMapping(value = {"saveViewEntity"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveViewEntity(String viewInstId, String ViewEntityTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (ViewEntityTree != null) {
            try {
                ViewInst bean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
                if (bean != null) {
                    String[] esdClassNames = StringUtility.split(ViewEntityTree, ";");
                    DSMFactory.getInstance().getViewManager().addViewEntity(viewInstId, esdClassNames);
                }

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除实体关系")
    @RequestMapping(value = {"delViewEntity"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delViewEntity(String viewInstId, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (viewInstId != null) {
            try {
                ViewInst bean = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
                String[] classNames = StringUtility.split(sourceClassName, ";");
                for (String esdClassName : classNames) {
                    try {
                        String viewSourceClassName = DSMFactory.getInstance().getViewManager().changeSourceClass(esdClassName, viewInstId);
                        if (!viewSourceClassName.equals(esdClassName)) {
                            bean.getViewEntityList().remove(viewSourceClassName);
                            DSMFactory.getInstance().getViewManager().delViewEntity(viewSourceClassName, viewInstId);
                        }
                        bean.getViewEntityList().remove(esdClassName);
                        bean.getAggEntityList().remove(esdClassName);
                        DSMFactory.getInstance().getViewManager().delViewEntity(esdClassName, viewInstId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                DSMFactory.getInstance().getViewManager().updateViewInst(bean, true);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    @RequestMapping(method = RequestMethod.POST, value = "ViewEntityMetaView")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @NavTreeViewAnnotation
    @DialogAnnotation(caption = "视图配置", width = "900", height = "680")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> getViewEntityMetaView(String viewInstId, String domainId, String sourceClassName) {
        TreeListResultModel<List<ViewConfigTree>> result = new TreeListResultModel<>();
        ESDClass esdClass = null;
        try {
            esdClass = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId).getSourceClass();
        } catch (JDSException e) {
            e.printStackTrace();
        }
        result = TreePageUtil.getTreeList(Arrays.asList(esdClass), ViewConfigTree.class);
        return result;
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}
