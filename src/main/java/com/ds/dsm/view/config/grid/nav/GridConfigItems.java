package com.ds.dsm.view.config.grid.nav;

import com.ds.dsm.view.config.grid.GridBaseInfoService;
import com.ds.dsm.view.config.grid.GridPageBarService;

import com.ds.dsm.view.config.grid.ViewGridContextService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum GridConfigItems implements TreeItem {
    GridConfig("视图配置", "spafont spa-icon-values", GridBaseInfoService.class, false, false, false),
    PageBar("分页栏配置", "spafont spa-icon-c-pager", GridPageBarService.class, true, true, true),
    FieldHiddenGridList("环境变量", "spafont spa-icon-c-hiddeninput", ViewGridContextService.class, true, true, true);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    GridConfigItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
