package com.ds.dsm.view.config.form.field.event;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.enums.FieldEvent;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RequestMapping(path = "/dsm/view/config/form/field/event/")
public class FieldEventService {

    @MethodChinaName(cname = "监听事件")
    @RequestMapping(method = RequestMethod.POST, value = "FieldBindMenus")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-statusbutton", caption = "监听事件")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<FieldEventView>> getFieldBindMenus(String sourceClassName, String viewInstId, String sourceMethodName, String domainId, String fieldname) {
        ListResultModel<List<FieldEventView>> resultModel = new ListResultModel<>();
        try {

            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);
            Set<FieldEvent> events = formInfo.getFieldBean().getEvent();
            List<FieldEventView> views = new ArrayList<>();
            for (FieldEvent gridEvent : events) {
                views.add(new FieldEventView(gridEvent, formInfo));
            }
            resultModel = PageUtil.getDefaultPageList(views);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "字段事件")
    @RequestMapping("FieldEventTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<FieldEventPopTree>> getFieldEventTree(String sourceClassName, String domainId, String fieldname, String sourceMethodName, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<FieldEventPopTree>> model = new TreeListResultModel<>();
        List<FieldEventPopTree> popTrees = new ArrayList<>();
        FieldEventPopTree eventPopTree = new FieldEventPopTree(" FieldEvent", "字段事件");
        List<FieldEventPopTree> childPops = TreePageUtil.fillObjs(Arrays.asList(FieldEvent.values()), FieldEventPopTree.class);
        eventPopTree.setSub(childPops);
        popTrees.add(eventPopTree);
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addEvent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addEvent(String sourceClassName, String sourceMethodName, String FieldEventTree, String domainId, String viewInstId, String fieldname) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            Set<FieldEvent> events = formInfo.getFieldBean().getEvent();
            if (FieldEventTree != null && !FieldEventTree.equals("")) {
                String[] menuIds = StringUtility.split(FieldEventTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        events.add(FieldEvent.valueOf(menuId));
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


    @RequestMapping("delEvent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delEvent(String sourceClassName, String viewInstId, String sourceMethodName, String eventName, String domainId, String fieldname) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            Set<FieldEvent> events = formInfo.getFieldBean().getEvent();
            String[] menuIds = StringUtility.split(eventName, ";");
            for (String menuId : menuIds) {
                events.remove(FieldEvent.valueOf(menuId));
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
