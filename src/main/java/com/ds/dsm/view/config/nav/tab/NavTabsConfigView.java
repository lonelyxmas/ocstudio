package com.ds.dsm.view.config.nav.tab;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.module.ModuleView;
import com.ds.dsm.view.config.form.field.FieldFormGridInfo;
import com.ds.dsm.view.config.nav.items.ModuleItemsInfo;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.foldingtree.NavFoldingTreeViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.VAlignType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/tabs/")
@MethodChinaName(cname = "配置信息")
@ButtonViewsAnnotation(barLocation = BarLocationType.left, barVAlign = VAlignType.top)
public class NavTabsConfigView {
    @CustomAnnotation(uid = true, hidden = true)
    private String esdclass;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    public NavTabsConfigView() {

    }


    @RequestMapping(method = RequestMethod.POST, value = "WinConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ModuleView> getWinConfig(String sourceClassName, String methodName, String domainId) {
        ResultModel<ModuleView> resultModel = new ResultModel();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            if (classConfig != null) {
                MethodConfig methodAPIBean = classConfig.getMethodByName(methodName);
                resultModel.setData(new ModuleView(methodAPIBean.getModuleBean()));
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldFormList")
    @GridViewAnnotation
    @CustomAnnotation(index = 1)
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "表单字段")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<FieldFormGridInfo>> getFieldFormList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldFormGridInfo>> cols = new ListResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            NavTabsViewBean navTabsViewBean = null;
            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                NavTreeViewBean navTreeViewBean = (NavTreeViewBean) customMethodAPIBean.getView();
                navTabsViewBean = navTreeViewBean.getTabsViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean navTreeViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                navTabsViewBean = navTreeViewBean.getTabsViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavGalleryViewBean) {
                NavGalleryViewBean navTabViewBean = (NavGalleryViewBean) customMethodAPIBean.getView();
                navTabsViewBean = navTabViewBean.getTabsViewBean();
            } else {
                navTabsViewBean = (NavTabsViewBean) customMethodAPIBean.getView();
            }
            Set<String> fieldNames = navTabsViewBean.getFieldNames();
            List<FieldFormConfig> fields = new ArrayList<>();
            for (String fieldName : fieldNames) {
                FieldFormConfig fieldFormConfig = (FieldFormConfig) navTabsViewBean.getFieldConfigMap().get(fieldName);
                if (fieldFormConfig != null) {


                    fieldFormConfig.setDomainId(domainId);
                    fields.add(fieldFormConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldFormGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }

    @RequestMapping(method = RequestMethod.POST, value = "ModuleList")
    @GridViewAnnotation
    @CustomAnnotation(index = 2)
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf", caption = "模块列表")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<ModuleItemsInfo>> getModuleList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<ModuleItemsInfo>> cols = new ListResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            NavTabsViewBean navTabsViewBean = null;

            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                NavTreeViewBean navTreeViewBean = (NavTreeViewBean) customMethodAPIBean.getView();
                navTabsViewBean = navTreeViewBean.getTabsViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean navTreeViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                navTabsViewBean = navTreeViewBean.getTabsViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavGalleryViewBean) {
                NavGalleryViewBean navTabViewBean = (NavGalleryViewBean) customMethodAPIBean.getView();
                navTabsViewBean = navTabViewBean.getTabsViewBean();
            } else {
                navTabsViewBean = (NavTabsViewBean) customMethodAPIBean.getView();
            }


            List<FieldModuleConfig> items = navTabsViewBean.getItems();
            List<FieldModuleConfig> fields = new ArrayList<>();
            for (FieldModuleConfig fieldFormConfig : items) {
                if (fieldFormConfig != null) {
                    fieldFormConfig.setSourceClassName(sourceClassName);
                    fieldFormConfig.setDomainId(domainId);

                    fields.add(fieldFormConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, ModuleItemsInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    public String getEsdclass() {
        return esdclass;
    }

    public void setEsdclass(String esdclass) {
        this.esdclass = esdclass;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}
