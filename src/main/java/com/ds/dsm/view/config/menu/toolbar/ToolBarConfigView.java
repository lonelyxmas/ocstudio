package com.ds.dsm.view.config.menu.toolbar;

import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.toolbar.ToolBarMenuBean;
import com.ds.esd.tool.ui.enums.FontStyleType;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.tool.ui.enums.VAlignType;
import com.ds.web.annotation.Pid;

@BottomBarMenu
@FormAnnotation(col = 2, customService = ToolBarConfigService.class)
public class ToolBarConfigView {

    @Pid
    String domainId;

    @Pid
    String sourceClassName;


    @Pid
    String methodName;

    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation(caption = "组ID")
    public String groupId;

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = FontStyleType.class)
    @CustomAnnotation(caption = "字体大小")
    public String iconFontSize;

    @CustomAnnotation(caption = "垂直对齐")
    HAlignType hAlign;

    @CustomAnnotation(caption = "横向对齐")
    VAlignType vAlign;

    @CustomAnnotation(caption = "显示标题")
    Boolean showCaption = true;
    @CustomAnnotation(caption = "行头手柄")
    Boolean handler;

    @CustomAnnotation(caption = "动态加载")
    Boolean dynLoad = false;

    @CustomAnnotation(caption = "禁用")
    public Boolean disabled;

    @CustomAnnotation(caption = "表单字段")
    public Boolean formField;

    @CustomAnnotation(caption = "延迟加载")
    Boolean lazy;


    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "实现类")
    Class bindService;

    public ToolBarConfigView() {

    }


    public ToolBarConfigView(ToolBarMenuBean menuBarBean, String domainId, String sourceClassName, String methodName) {


        this.groupId = menuBarBean.getGroupId();
        this.iconFontSize = menuBarBean.getIconFontSize();
        this.formField = menuBarBean.getFormField();

        this.domainId = menuBarBean.getDomainId();
        this.bindService = menuBarBean.getBindService();

        this.dynLoad = menuBarBean.getDynLoad();
        this.showCaption = menuBarBean.getShowCaption();
        this.lazy = menuBarBean.getLazy();
        this.hAlign = menuBarBean.gethAlign();
        this.vAlign = menuBarBean.getvAlign();
        this.handler = menuBarBean.getHandler();
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;

    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public Boolean getShowCaption() {
        return showCaption;
    }

    public void setShowCaption(Boolean showCaption) {
        this.showCaption = showCaption;
    }


    public Boolean getLazy() {
        return lazy;
    }

    public void setLazy(Boolean lazy) {
        this.lazy = lazy;
    }

    public HAlignType gethAlign() {
        return hAlign;
    }

    public void sethAlign(HAlignType hAlign) {
        this.hAlign = hAlign;
    }

    public VAlignType getvAlign() {
        return vAlign;
    }

    public void setvAlign(VAlignType vAlign) {
        this.vAlign = vAlign;
    }

    public Boolean getHandler() {
        return handler;
    }

    public void setHandler(Boolean handler) {
        this.handler = handler;
    }


    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getFormField() {
        return formField;
    }

    public void setFormField(Boolean formField) {
        this.formField = formField;
    }

    public String getIconFontSize() {
        return iconFontSize;
    }

    public void setIconFontSize(String iconFontSize) {
        this.iconFontSize = iconFontSize;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }


    public Class getBindService() {
        return bindService;
    }

    public void setBindService(Class bindService) {
        this.bindService = bindService;
    }
}
