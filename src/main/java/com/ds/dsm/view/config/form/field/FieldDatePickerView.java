package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.form.field.combo.service.ComboDateService;
import com.ds.dsm.view.config.form.field.service.FormComboBoxService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.combo.ComboDateFieldBean;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 2, customService =ComboDateService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FieldDatePickerView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;


    @CustomAnnotation(caption = "高度")
    String height;
    @CustomAnnotation(caption = "宽度")
    String width;
    @CustomAnnotation(caption = "默认值")
    String value;
    @CustomAnnotation(caption = "关闭按钮")
    Boolean closeBtn;


    @CustomAnnotation(caption = "显示时间")
    Boolean timeInput;
    @CustomAnnotation(caption = "第一天")
    Integer firstDayOfWeek;
    @CustomAnnotation(caption = "下一天")
    String offDays;
    @CustomAnnotation(caption = "隐藏周")
    Boolean hideWeekLabels;
    @CustomAnnotation(caption = "日期格式")
    String dateInputFormat;


    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;

    public FieldDatePickerView() {

    }

    public FieldDatePickerView(FieldFormConfig<ComboInputFieldBean,ComboDateFieldBean> config) {
        ComboDateFieldBean inputFieldBean = config.getComboConfig();
        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(inputFieldBean.getDataPickBean()));
        this.methodName = config.getSourceMethodName();
        this.viewClassName= config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();

        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getCloseBtn() {
        return closeBtn;
    }

    public void setCloseBtn(Boolean closeBtn) {
        this.closeBtn = closeBtn;
    }

    public Boolean getTimeInput() {
        return timeInput;
    }

    public void setTimeInput(Boolean timeInput) {
        this.timeInput = timeInput;
    }

    public Integer getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    public void setFirstDayOfWeek(Integer firstDayOfWeek) {
        this.firstDayOfWeek = firstDayOfWeek;
    }

    public String getOffDays() {
        return offDays;
    }

    public void setOffDays(String offDays) {
        this.offDays = offDays;
    }

    public Boolean getHideWeekLabels() {
        return hideWeekLabels;
    }

    public void setHideWeekLabels(Boolean hideWeekLabels) {
        this.hideWeekLabels = hideWeekLabels;
    }

    public String getDateInputFormat() {
        return dateInputFormat;
    }

    public void setDateInputFormat(String dateInputFormat) {
        this.dateInputFormat = dateInputFormat;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
