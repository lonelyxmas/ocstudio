package com.ds.dsm.view.config.form.field.combo;


import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.component.UIItem;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.web.annotation.Required;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete})
public class ComboItemGridView {

    @CustomAnnotation(readonly = true, caption = "key", uid = true)
    public String id;
    @Required
    @CustomAnnotation(caption = "名称")
    public String caption;
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;
    @CustomAnnotation(caption = "提示")
    public String tips;

    public ComboItemGridView(UIItem item) {
        this.caption = item.getCaption();
        this.tips = item.getTips();
        this.imageClass = item.getImageClass();
        this.id = item.getId();

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }
}
