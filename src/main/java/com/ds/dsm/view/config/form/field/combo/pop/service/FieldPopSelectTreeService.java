package com.ds.dsm.view.config.form.field.combo.pop.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.combo.pop.PopBindEntityTree;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/dsm/view/config/field/popselect/")
public class FieldPopSelectTreeService {


    @RequestMapping(method = RequestMethod.POST, value = "loadChildPackage")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<PopBindEntityTree>> loadChildPackage(String projectName, String packageName, String euPackageName, String domainId, String parentId) {
        TreeListResultModel<List<PopBindEntityTree>> result = new TreeListResultModel<>();
        try {

            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName("DSMdsm");
            EUPackage euPackage = version.getEUPackage(packageName);
            List<Object> objs = new ArrayList<>();
            List<String> esdClassNameList = new ArrayList<>();

            Set<EUModule> moduleList = euPackage.listModules();
            for (EUModule module : moduleList) {
                if (module.getRealClassName().equals(module.getClassName())) {
                    MethodConfig methodConfig = module.getComponent().getMethodAPIBean();
                    if (methodConfig != null ){
                        if (methodConfig.isModule()
                                && !esdClassNameList.contains(methodConfig.getSourceClassName())
                                && (
                                 methodConfig.getBindMenus().contains(CustomMenuItem.fieldNodeEditor)
                                 || methodConfig.getBindMenus().contains(CustomMenuItem.index)
                                  )
                                ) {
                            esdClassNameList.add(methodConfig.getSourceClassName());
                        }
                    }

                }
            }

            for (String esdClassName : esdClassNameList) {
                ESDClass esdClass = DSMFactory.getInstance().getClassManager().getAggEntityByName(esdClassName, domainId, false);
                objs.add(esdClass);
            }


            result = TreePageUtil.getTreeList(objs, PopBindEntityTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }


}


