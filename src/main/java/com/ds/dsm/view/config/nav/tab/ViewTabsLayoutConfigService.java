package com.ds.dsm.view.config.nav.tab;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.nav.layout.NavLayoutConfigView;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.foldingtree.NavFoldingTreeViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.layout.CustomLayoutViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/tabs/layout/")
public class ViewTabsLayoutConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "LayoutConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf", caption = "布局信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<NavLayoutConfigView> getLayoutConfig(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<NavLayoutConfigView> resultModel = new ResultModel();
        try {
            ViewEntityConfig config = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId);
            MethodConfig customMethodAPIBean = config.getSourceConfig().getMethodByName(methodName);
            CustomLayoutViewBean layoutConfigView = null;

            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                NavTreeViewBean navTreeViewBean = (NavTreeViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTreeViewBean.getLayoutViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean navTreeViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTreeViewBean.getLayoutViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavGalleryViewBean) {
                NavGalleryViewBean navTabViewBean = (NavGalleryViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTabViewBean.getLayoutViewBean();
            }
            resultModel.setData(new NavLayoutConfigView(layoutConfigView));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }



}
