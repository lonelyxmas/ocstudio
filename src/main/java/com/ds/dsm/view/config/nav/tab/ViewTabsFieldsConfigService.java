package com.ds.dsm.view.config.nav.tab;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.enums.CustomBean;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavTabsBaseViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tabs/field/")
public class ViewTabsFieldsConfigService {

    @RequestMapping(method = RequestMethod.POST, value = "TabsFieldsConfig")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<NavTabsTree>> getTabsFieldsConfig(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<NavTabsTree>> result = new TreeListResultModel<>();
        try {
            List<CustomBean> customBeans = new ArrayList<>();
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            if (methodAPIBean!=null){
                NavTabsViewBean navTabsViewBean = null;
                if (methodAPIBean.getView() instanceof NavTabsBaseViewBean) {
                    NavTabsBaseViewBean baseViewBean = (NavTabsBaseViewBean) methodAPIBean.getView();
                    navTabsViewBean = baseViewBean.getTabsViewBean();
                } else if (methodAPIBean.getView() instanceof NavTabsViewBean) {
                    navTabsViewBean = (NavTabsViewBean) methodAPIBean.getView();
                }

                List<String> fieldNames = navTabsViewBean.getDisplayFieldNames();
                for (String fieldname : fieldNames) {
                    FieldFormConfig fieldFormConfig = (FieldFormConfig) navTabsViewBean.getFieldConfigMap().get(fieldname);
                    if (fieldFormConfig != null) {
                        customBeans.add(fieldFormConfig);
                    }
                }

            }

            result = TreePageUtil.getTreeList(customBeans, NavTabsTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
