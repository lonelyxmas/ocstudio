package com.ds.dsm.view.config.form.field.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.item.FieldWidgetNavItem;
import com.ds.dsm.view.config.form.field.item.FormFieldNavTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/widget/")
public class FieldWidgetService {


    @RequestMapping(method = RequestMethod.POST, value = "WidgetConfig")
    @ModuleAnnotation(dynLoad = true, caption = "字段子域")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<FormFieldNavTree>> getWidgetConfig(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        TreeListResultModel<List<FormFieldNavTree>> result = new TreeListResultModel<>();

        List<FieldWidgetNavItem> fieldWidgetNavItems = new ArrayList<>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);
            fieldWidgetNavItems = FieldWidgetNavItem.getWidgetByComponentType(formInfo.getWidgetConfig().getComponentType());
        } catch (JDSException e) {
            e.printStackTrace();
        }

        result = TreePageUtil.getTreeList(fieldWidgetNavItems, FormFieldNavTree.class);
        return result;
    }

}
