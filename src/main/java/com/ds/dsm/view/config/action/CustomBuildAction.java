package com.ds.dsm.view.config.action;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.CustomViewFactory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.gen.GenJava;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.module.ModuleComponent;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/action/")
@Aggregation(type = AggregationType.menu, rootClass = CustomBuildAction.class)
public class CustomBuildAction {


    @RequestMapping(value = {"javaBuild"}, method = {RequestMethod.POST})
    @APIEventAnnotation()
    @CustomAnnotation(index = 0, caption = "编译", imageClass = "spafont spa-icon-coin")
    @ResponseBody
    public ResultModel<Boolean> build(String viewInstId, String domainId, String sourceClassName, String methodName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();

        try {
            List<ViewInst> viewInsts = new ArrayList<>();
            if (viewInstId != null && !viewInstId.equals("")) {
                ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
                viewInsts.add(viewInst);
            } else {
                DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
                viewInsts = DSMFactory.getInstance().getViewManager().getViewInstList(domainInst.getProjectVersionName(), domainId);
            }
            for (ViewInst viewInst : viewInsts) {
                viewInstId = viewInst.getViewInstId();
                ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
                List<MethodConfig> allViewConfigs = new ArrayList<>();
                if (methodName == null || methodName.equals("")) {
                    allViewConfigs = apiClassConfig.getAllViewMethods();
                } else {
                    MethodConfig methodAPIBean = apiClassConfig.getMethodByName(methodName);
                    allViewConfigs.add(methodAPIBean);
                }
                for (MethodConfig methodConfig : allViewConfigs) {
                    GenJava.getInstance(viewInst.getProjectVersionName()).updateViewJava(methodConfig, viewInstId, chrome);
                }
                DSMFactory.getInstance().compileDsmInst(viewInst, this.getCurrChromeDriver());
            }

            DSMFactory.getInstance().reload();

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(value = {"buildView"}, method = {RequestMethod.POST})
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName})
    @CustomAnnotation(index = 1, caption = "编译视图", imageClass = "spafont spa-icon-moveforward")
    @ResponseBody
    public ResultModel<Boolean> buildView(String viewInstId, String domainId, String sourceClassName, String methodName) {
        ResultModel resultModel = new ResultModel();
        ESDChrome chrome = getCurrChromeDriver();
        try {
            DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            List<MethodConfig> allViewConfigs = new ArrayList<>();
            if (methodName == null || methodName.equals("")) {
                allViewConfigs = esdClassConfig.getAllMethods();
            } else {
                MethodConfig methodAPIBean = esdClassConfig.getMethodByName(methodName);
                allViewConfigs.add(methodAPIBean);
            }

            for (MethodConfig methodAPIBean : allViewConfigs) {
                if (methodAPIBean.isModule()) {
                    EUModule module = ESDFacrory.getESDClient().getModule(methodAPIBean.getUrl(), domainInst.getProjectVersionName());
                    if (module != null) {
                        List<ModuleComponent> moduleComponents = module.getComponent().findComponents(ComponentType.Module, null);
                        for (ModuleComponent moduleComponent : moduleComponents) {
                            if (moduleComponent.getEuModule() != null) {
                                ESDFacrory.getESDClient().delModule(moduleComponent.getEuModule());
                            }
                        }
                        ESDFacrory.getESDClient().delModule(module);

                    }
                    CustomViewFactory.getInstance().buildView(methodAPIBean, domainInst.getProjectVersionName(), (Map<String, ?>) JDSActionContext.getActionContext().getContext(), true);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            chrome.printError(e.getMessage());
            ((ErrorResultModel) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }

    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }

}

