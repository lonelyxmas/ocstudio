package com.ds.dsm.view.config.gallery.view;

import com.ds.dsm.view.config.gallery.service.GalleryBaseInfoService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.tool.ui.enums.SelModeType;

@FormAnnotation(col = 2, customService = GalleryBaseInfoService.class, customMenu = {CustomFormMenu.Close, CustomFormMenu.ReSet})
@BottomBarMenu()
public class GalleryBaseView {


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    @CustomAnnotation(caption = "自适应图片大小")
    public Boolean autoImgSize;
    @CustomAnnotation(caption = "自适应大小")
    public Boolean autoItemSize;
    @CustomAnnotation(caption = "仅显示图片")
    public Boolean iconOnly;


    @CustomAnnotation(caption = "列数")
    public Integer columns;
    @CustomAnnotation(caption = "行数")
    public Integer rows;
    @CustomAnnotation(caption = "选中方式")
    SelModeType selMode;

    public GalleryBaseView() {

    }

    public GalleryBaseView(CustomGalleryViewBean gridConfig) {
        if (gridConfig == null) {
            gridConfig = new CustomGalleryViewBean();
        }
        this.viewClassName = gridConfig.getViewClassName();
        this.sourceClassName = gridConfig.getSourceClassName();
        this.methodName = gridConfig.getMethodName();
        this.viewInstId = gridConfig.getViewInstId();
        this.domainId = gridConfig.getDomainId();

        this.autoImgSize = gridConfig.getAutoImgSize();
        this.autoItemSize = gridConfig.getAutoItemSize();
        this.iconOnly = gridConfig.getIconOnly();

        this.selMode = gridConfig.getSelMode();
        this.columns = gridConfig.getColumns();
        this.rows = gridConfig.getRows();

    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }

    public Boolean getAutoImgSize() {
        return autoImgSize;
    }

    public void setAutoImgSize(Boolean autoImgSize) {
        this.autoImgSize = autoImgSize;
    }

    public Boolean getAutoItemSize() {
        return autoItemSize;
    }

    public void setAutoItemSize(Boolean autoItemSize) {
        this.autoItemSize = autoItemSize;
    }

    public Boolean getIconOnly() {
        return iconOnly;
    }

    public void setIconOnly(Boolean iconOnly) {
        this.iconOnly = iconOnly;
    }


    public Integer getColumns() {
        return columns;
    }

    public void setColumns(Integer columns) {
        this.columns = columns;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

}
