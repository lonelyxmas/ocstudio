package com.ds.dsm.view.config.form.field.service;

import com.alibaba.fastjson.JSONObject;
import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.custom.FieldLayoutView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.FieldBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/form/field/layout/")
public class FieldLayoutService {


    @RequestMapping(method = RequestMethod.POST, value = "FieldLayoutView")
    @FormViewAnnotation(autoSave = true)
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-layout",  caption = "布局", dock = Dock.bottom)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @UIAnnotation(height = "150")
    @ResponseBody
    public ResultModel<FieldLayoutView> getFieldLayoutView(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<FieldLayoutView> result = new ResultModel<FieldLayoutView>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new FieldLayoutView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "编辑字段配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFieldLayout")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateFieldLayout(@RequestBody FieldLayoutView config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getSourceClassName();
            if (className != null && !className.equals("")) {
                String json = JSONObject.toJSONString(config);
                FieldBean fieldBean = JSONObject.parseObject(json, FieldBean.class);
                ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(className, config.getDomainId());
                MethodConfig customMethodAPIBean = classConfig.getMethodByName(config.getMethodName());
                CustomView viewBean = customMethodAPIBean.getView();
                FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(config.getFieldname());
                formInfo.setFieldBean(fieldBean);
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

}
