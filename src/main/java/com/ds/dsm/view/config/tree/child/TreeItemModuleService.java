package com.ds.dsm.view.config.tree.child;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.CustomTreeTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tree/child/item/")
public class TreeItemModuleService {

    @MethodChinaName(cname = "树形节点服务")
    @RequestMapping(method = RequestMethod.POST, value = "TreeModuleItems")
    @FormViewAnnotation()
    @ModuleAnnotation(imageClass = "spafont spa-icon-values", caption = "树形节点服务")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<CustomTreeTree>> getTreeModuleItems(String sourceClassName, String childViewId, String sourceMethodName, String serviceClassName, String domainId, String viewInstId) {
        TreeListResultModel<List<CustomTreeTree>> result = new TreeListResultModel<List<CustomTreeTree>>();
        result = TreePageUtil.getTreeList(Arrays.asList(NavTreeModuleItems.values()), CustomTreeTree.class);
        return result;
    }


}