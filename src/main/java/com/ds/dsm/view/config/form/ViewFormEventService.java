package com.ds.dsm.view.config.form;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.enums.CustomFormEvent;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/dsm/view/config/form/")
public class ViewFormEventService {


    @MethodChinaName(cname = "监听事件")
    @RequestMapping(method = RequestMethod.POST, value = "BindMenus")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-statusbutton", caption = "监听事件")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<FormEventView>> getBindMenus(String sourceClassName, String domainId, String viewInstId, String methodName) {
        ListResultModel<List<FormEventView>> resultModel = new ListResultModel<>();
        try {
            ViewEntityConfig esdClassConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId);
            MethodConfig methodAPIBean = esdClassConfig.getSourceConfig().getMethodByName(methodName);
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) methodAPIBean.getView();
            Set<CustomFormEvent> events = customFormViewBean.getEvent();

            List<FormEventView> views = new ArrayList<>();
            for (CustomFormEvent formEvent : events) {
                views.add(new FormEventView(formEvent, methodAPIBean));
            }
            resultModel = PageUtil.getDefaultPageList(views);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
