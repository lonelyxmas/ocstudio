package com.ds.dsm.view.config.form.field.event;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.form.enums.FieldEvent;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, caption = "添加事件", customService = {FieldEventService.class}, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Close, CustomFormMenu.Save})
public class FieldEventPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String sourceClassName;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String viewInstId;

    @CustomAnnotation(pid = true)
    String sourceMethodName;

    @CustomAnnotation(pid = true)
    String bar;

    public FieldEventPopTree(String id, String caption) {
        super(id, caption);

    }

    public FieldEventPopTree(FieldEvent event, String pattern, String sourceClassName, String sourceMethodName, String domainId, String viewInstId) throws JDSException {
        super(event.name(), event.getEventEnum().getEvent() + "(" + event.getName() + ")");
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.viewInstId = viewInstId;


    }

    @Override
    public String getEuClassName() {
        return euClassName;
    }

    @Override
    public void setEuClassName(String euClassName) {
        this.euClassName = euClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }

}
