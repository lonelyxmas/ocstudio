package com.ds.dsm.view.config.form.field.combo;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.item.FormFieldNavTree;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/combo/")
public class ComboWidgetService {


    @RequestMapping(method = RequestMethod.POST, value = "ComboWidgetConfig")
    @ModuleAnnotation(dynLoad = true, caption = "字段子域")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<FormFieldNavTree>> getComboWidgetConfig(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        TreeListResultModel<List<FormFieldNavTree>> result = new TreeListResultModel<>();
        ViewEntityConfig classConfig = null;
        List<ComboWidgetNavItem> fieldWidgetNavItems = new ArrayList<>();
        try {
            classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);
            if (formInfo.getComponentType().equals(ComponentType.ComboInput)) {
                ComboBoxBean comboBoxBean = formInfo.getComboConfig();
                ComboInputType inputType = comboBoxBean.getInputType();
                List<ComboWidgetNavItem> fieldInputItems = ComboWidgetNavItem.getInputByComponentType(inputType);
                for (ComboWidgetNavItem fieldInputItem : fieldInputItems) {
                    if (!fieldInputItem.equals(ComboWidgetNavItem.none)) {
                        fieldWidgetNavItems.add(fieldInputItem);
                    }
                }
            }
        } catch (
                JDSException e)

        {
            e.printStackTrace();
        }

        result = TreePageUtil.getTreeList(fieldWidgetNavItems, FormFieldNavTree.class);
        return result;
    }

}
