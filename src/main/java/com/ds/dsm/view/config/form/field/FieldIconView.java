package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.form.field.service.FormButtonService;
import com.ds.dsm.view.config.form.field.service.FormIconService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.IconFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.esd.tool.ui.enums.FontStyleType;
import com.ds.esd.tool.ui.enums.ImagePos;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 2, customService =FormIconService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FieldIconView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;


    @CustomAnnotation(caption = "html")
    String html;
    @CustomAnnotation(caption = "扩展属性")
    String attributes;
    @CustomAnnotation(caption = "渲染目标")
    String renderer;
    @CustomAnnotation(caption = "默认焦点")
    Boolean defaultFocus;
    @CustomAnnotation(caption = "TAB顺序")
    Integer tabindex;
    @CustomAnnotation(caption = "图片")
    String image;
    @CustomAnnotation(caption = "图片位置")
    ImagePos imagePos;
    @CustomAnnotation(caption = "图片大小")
    String imageBgSize;
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;
    @CustomAnnotation(caption = "字体代码")
    String iconFontCode;
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = FontStyleType.class)
    @CustomAnnotation(caption = "字体大小")
    String iconFontSize;
    @CustomAnnotation(caption = "字体颜色")
    String iconColor;


    public FieldIconView() {

    }

    public FieldIconView(FieldFormConfig<IconFieldBean,?> config) {
        IconFieldBean inputFieldBean = config.getWidgetConfig();
        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(inputFieldBean));
        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
 ;
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getRenderer() {
        return renderer;
    }

    public void setRenderer(String renderer) {
        this.renderer = renderer;
    }

    public Boolean getDefaultFocus() {
        return defaultFocus;
    }

    public void setDefaultFocus(Boolean defaultFocus) {
        this.defaultFocus = defaultFocus;
    }

    public Integer getTabindex() {
        return tabindex;
    }

    public void setTabindex(Integer tabindex) {
        this.tabindex = tabindex;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ImagePos getImagePos() {
        return imagePos;
    }

    public void setImagePos(ImagePos imagePos) {
        this.imagePos = imagePos;
    }

    public String getImageBgSize() {
        return imageBgSize;
    }

    public void setImageBgSize(String imageBgSize) {
        this.imageBgSize = imageBgSize;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getIconFontCode() {
        return iconFontCode;
    }

    public void setIconFontCode(String iconFontCode) {
        this.iconFontCode = iconFontCode;
    }

    public String getIconFontSize() {
        return iconFontSize;
    }

    public void setIconFontSize(String iconFontSize) {
        this.iconFontSize = iconFontSize;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
