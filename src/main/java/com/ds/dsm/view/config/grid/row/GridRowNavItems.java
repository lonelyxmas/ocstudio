package com.ds.dsm.view.config.grid.row;

import com.ds.dsm.view.config.grid.ViewGridHiddensService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum GridRowNavItems implements TreeItem {
    GridRowHeard("行头信息", "spafont spa-icon-rendermode", GridRowHeaderService.class, false, false, false),
    GridRowButton("动作事件", "spafont spa-icon-event", ViewRowActionService.class, false, false, false),
    FieldHiddenGridList("环境变量", "spafont spa-icon-c-hiddeninput", ViewGridHiddensService.class, true, true, true);


    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    GridRowNavItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
