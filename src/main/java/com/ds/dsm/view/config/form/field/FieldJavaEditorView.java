package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.form.field.service.FormInputService;
import com.ds.dsm.view.config.form.field.service.FormJavaEditorService;
import com.ds.esd.custom.annotation.CodeEditorAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.JavaEditorFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.tool.ui.enums.LabelPos;
import com.ds.esd.tool.ui.enums.VAlignType;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 2, customService =FormJavaEditorService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FieldJavaEditorView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;

    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;
    @CustomAnnotation(caption = "延迟加载")
    Boolean dynLoad;
    @CustomAnnotation(caption = "是否可选中")
    Boolean selectable;
    @CustomAnnotation(caption = "值")
    String value;
    @CustomAnnotation(caption = "宽")
    String width;
    @CustomAnnotation(caption = "高")
    String height;
    @CodeEditorAnnotation(codeType="css")
    @CustomAnnotation(caption = "容器样式")
    String frameStyle;
    @CustomAnnotation(caption = "命令按钮集合")
    String cmdList;
    @CustomAnnotation(caption = "命令按钮过滤")
    String cmdFilter;
    @CustomAnnotation(caption = "文本类型")
    String textType;
    @CustomAnnotation(caption = "steps")
    Integer steps;
    @CustomAnnotation(caption = "标签大小")
    Integer labelSize;
    @CustomAnnotation(caption = "标签位置")
    LabelPos labelPos;
    @CustomAnnotation(caption = "标签间距")
    Integer labelGap;
    @CustomAnnotation(caption = "标签名称")
    String labelCaption;
    @CustomAnnotation(caption = "垂直对齐")
    HAlignType labelHAlign;
    @CustomAnnotation(caption = "左右对齐")
    VAlignType labelVAlign;


    public FieldJavaEditorView() {

    }

    public FieldJavaEditorView(FieldFormConfig<JavaEditorFieldBean,?> config) {
        JavaEditorFieldBean inputFieldBean = config.getWidgetConfig();
        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(inputFieldBean));
        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.domainId = config.getDomainId();
        this.viewInstId = config.getDomainId();
        this.fieldname = config.getFieldname();

    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getFrameStyle() {
        return frameStyle;
    }

    public void setFrameStyle(String frameStyle) {
        this.frameStyle = frameStyle;
    }

    public String getCmdList() {
        return cmdList;
    }

    public void setCmdList(String cmdList) {
        this.cmdList = cmdList;
    }

    public String getCmdFilter() {
        return cmdFilter;
    }

    public void setCmdFilter(String cmdFilter) {
        this.cmdFilter = cmdFilter;
    }

    public String getTextType() {
        return textType;
    }

    public void setTextType(String textType) {
        this.textType = textType;
    }

    public Integer getSteps() {
        return steps;
    }

    public void setSteps(Integer steps) {
        this.steps = steps;
    }

    public Integer getLabelSize() {
        return labelSize;
    }

    public void setLabelSize(Integer labelSize) {
        this.labelSize = labelSize;
    }

    public LabelPos getLabelPos() {
        return labelPos;
    }

    public void setLabelPos(LabelPos labelPos) {
        this.labelPos = labelPos;
    }

    public Integer getLabelGap() {
        return labelGap;
    }

    public void setLabelGap(Integer labelGap) {
        this.labelGap = labelGap;
    }

    public String getLabelCaption() {
        return labelCaption;
    }

    public void setLabelCaption(String labelCaption) {
        this.labelCaption = labelCaption;
    }

    public HAlignType getLabelHAlign() {
        return labelHAlign;
    }

    public void setLabelHAlign(HAlignType labelHAlign) {
        this.labelHAlign = labelHAlign;
    }

    public VAlignType getLabelVAlign() {
        return labelVAlign;
    }

    public void setLabelVAlign(VAlignType labelVAlign) {
        this.labelVAlign = labelVAlign;
    }
    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
