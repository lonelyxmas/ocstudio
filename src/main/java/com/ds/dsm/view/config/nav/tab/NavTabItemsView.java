package com.ds.dsm.view.config.nav.tab;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.bean.nav.tab.TabItemBean;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.web.annotation.Pid;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload})
public class NavTabItemsView {


    @Pid
    String id;
    @Pid
    String childTabId;


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;
    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    @CustomAnnotation(caption = "标题")
    String caption;
    @CustomAnnotation(caption = "顺序")
    Integer index = 1;
    @CustomAnnotation(caption = "绑定服务")
    Class bindService;
    @CustomAnnotation(caption = "自动重载")
    Boolean autoReload;


    public NavTabItemsView() {

    }

    public NavTabItemsView(TabItemBean config) {
        this.viewInstId = config.getViewInstId();
        this.domainId = config.getDomainId();

        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getMethodName();

        this.id = config.getId();
        this.childTabId = config.getId();

        this.caption = config.getCaption();
        this.autoReload = config.getAutoReload();
        this.index = config.getIndex();

        if (config.getBindService() != null && !config.getBindService().equals(Void.class)) {
            this.bindService = config.getBindService();
        }


    }

    public String getChildTabId() {
        return childTabId;
    }

    public void setChildTabId(String childTabId) {
        this.childTabId = childTabId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Boolean getAutoReload() {
        return autoReload;
    }

    public void setAutoReload(Boolean autoReload) {
        this.autoReload = autoReload;
    }


    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


    public Class getBindService() {
        return bindService;
    }

    public void setBindService(Class bindService) {
        this.bindService = bindService;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}


