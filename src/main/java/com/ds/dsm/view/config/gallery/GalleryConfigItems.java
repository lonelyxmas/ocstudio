package com.ds.dsm.view.config.gallery;

import com.ds.dsm.view.config.gallery.service.GalleryBaseInfoService;
import com.ds.dsm.view.config.gallery.service.GalleryItemInfoService;
import com.ds.dsm.view.config.grid.GridPageBarService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum GalleryConfigItems implements TreeItem {
    GalleryBaseConfig("九宫格基础配置", "spafont spa-icon-values", GalleryBaseInfoService.class, true, true, true),
    GalleryItemInfo("画廊项", "spafont spa-icon-values", GalleryItemInfoService.class, true, true, true),
    GalleryPageBar("分页栏配置", "spafont spa-icon-c-pager", GridPageBarService.class, true, true, true);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    GalleryConfigItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
