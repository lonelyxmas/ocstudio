package com.ds.dsm.view.config.form.field.item;

import com.ds.dsm.view.config.form.field.service.FieldAggConfigService;
import com.ds.dsm.view.config.form.field.service.FieldItemEventService;
import com.ds.dsm.view.config.form.field.service.FieldLayoutService;
import com.ds.dsm.view.config.form.field.service.FieldWidgetService;
import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.esd.tool.ui.enums.CustomImageType;

public enum FieldNavItem implements TreeItem {
    FieldAggConfig("常用配置", CustomImageType.conf1, FieldAggConfigService.class, false, false, false),
    LayoutConfig("表格布局", CustomImageType.layout, FieldLayoutService.class, false, false, false),
    EventConfig("监听事件", CustomImageType.event, FieldItemEventService.class, false, false, false),
    WidgetConfig("扩展配置", CustomImageType.com3, FieldWidgetService.class, false, false, false);

    private final String name;
    private final Class bindClass;
    private final String imageClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    FieldNavItem(String name, CustomImageType imageType, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.name = name;
        this.imageClass = imageType.getImageClass();
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
