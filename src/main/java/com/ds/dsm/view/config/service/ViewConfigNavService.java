package com.ds.dsm.view.config.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.nav.ViewEntityNav;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/meta/")
public class ViewConfigNavService {


    @RequestMapping(method = RequestMethod.POST, value = "loadTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeReload)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> loadTree(String viewInstId, String domainId, String sourceClassName) {
        TreeListResultModel<List<ViewConfigTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(sourceClassName), ViewConfigTree.class);
        return result;
    }

    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @MethodChinaName(cname = "视图集合信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggEntityConfig")
    @NavGroupViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "视图集合信息", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<ViewEntityNav> getViewEntityNav(String viewInstId, String domainId, String sourceClassName) {
        ResultModel<ViewEntityNav> result = new ResultModel<ViewEntityNav>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadChild")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> loadChild(String domainId, String sourceClassName) {
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<List<ViewConfigTree>>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
            List<CustomView> customViews = new ArrayList<>();
            for (MethodConfig methodAPIBean : customMethods) {
                if (methodAPIBean.getView() != null) {
                    customViews.add(methodAPIBean.getView());
                }
            }
            resultModel = TreePageUtil.getTreeList(customViews, ViewConfigTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
