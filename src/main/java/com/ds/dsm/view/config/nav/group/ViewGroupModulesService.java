package com.ds.dsm.view.config.nav.group;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.view.config.nav.items.ModuleItemsInfo;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.group.GroupItemBean;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/dsm/view/config/group/modules/")
public class ViewGroupModulesService {

    @RequestMapping(method = RequestMethod.POST, value = "ChildModuleList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "子模块")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<ModuleItemsInfo>> getChildModuleList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<ModuleItemsInfo>> cols = new ListResultModel();
        try {
            ViewEntityConfig config = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getSourceConfig().getMethodByName(methodName);
            NavGroupViewBean groupViewBean = (NavGroupViewBean) customMethodAPIBean.getView();
            List<FieldModuleConfig> fieldModuleConfigs = groupViewBean.getItems();
            List<FieldModuleConfig> fields = new ArrayList<>();
            for (FieldModuleConfig fieldModuleConfig : fieldModuleConfigs) {
                if (fieldModuleConfig != null) {

                    fields.add(fieldModuleConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, ModuleItemsInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadView")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<NavGroupTree>> loadView(String domainId, String sourceClassName, String methodName, String viewInstId)

    {
        TreeListResultModel<List<NavGroupTree>> result = new TreeListResultModel<>();
        try {
            List<CustomViewBean> viewBeans = new ArrayList<>();
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            NavGroupViewBean groupViewBean = (NavGroupViewBean) methodAPIBean.getView();
            List<GroupItemBean> itemBean = groupViewBean.getItemBeans();

            for (GroupItemBean groupItemBean : itemBean) {
                viewBeans.add((CustomViewBean) groupItemBean.getMethodConfig().getView());
            }

            JDSActionContext.getActionContext().getContext().put("sourceClassName", groupViewBean.getViewClassName());
            result = TreePageUtil.getDefaultTreeList(viewBeans, NavGroupTree.class);
            JDSActionContext.getActionContext().getContext().put("sourceClassName", sourceClassName);
            JDSActionContext.getActionContext().getContext().put("methodName", methodName);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }

}
