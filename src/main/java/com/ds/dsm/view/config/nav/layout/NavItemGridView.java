package com.ds.dsm.view.config.nav.layout;

import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.custom.layout.CustomLayoutItemBean;
import com.ds.esd.tool.ui.enums.OverflowType;
import com.ds.esd.tool.ui.enums.PosType;
import org.springframework.stereotype.Controller;

@Controller
@GridAnnotation(customMenu = {GridMenu.Add, GridMenu.Delete, GridMenu.Reload})
@MethodChinaName(cname = "布局", imageClass = "spafont spa-icon-c-gallery")
public class NavItemGridView {


    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(uid = true, hidden = true)
    private String methodName;

    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;

    @CustomAnnotation(uid = true, hidden = true)
    public String projectVersionName;


    @CustomAnnotation(caption = "表达式")
    public String expression;
    @CustomAnnotation(caption = "大小")
    public Integer size;
    @CustomAnnotation(caption = "最小值")
    public Integer min;
    @CustomAnnotation(caption = "最大值")
    public Integer max;
    @CustomAnnotation(caption = "默认折叠")
    public Boolean folded;
    @CustomAnnotation(caption = "命令按钮")
    public Boolean cmd;
    @CustomAnnotation(caption = "位置")
    public PosType pos;
    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "HTML")
    public String html;
    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "滚动条")
    public OverflowType overflow;
    @CustomAnnotation(uid = true, hidden = true)
    public String id;

    public NavItemGridView(CustomLayoutItemBean itemBean) {
        this.expression = itemBean.getExpression();
        this.size = itemBean.getSize();
        this.max = itemBean.getMax();
        this.min = itemBean.getMin();
        this.folded = itemBean.getFolded();
        this.cmd = itemBean.getCmd();
        this.pos = itemBean.getPos();
        this.overflow = itemBean.getOverflow();
        this.id = itemBean.getId();

    }


    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Boolean getFolded() {
        return folded;
    }

    public void setFolded(Boolean folded) {
        this.folded = folded;
    }

    public Boolean getCmd() {
        return cmd;
    }

    public void setCmd(Boolean cmd) {
        this.cmd = cmd;
    }

    public PosType getPos() {
        return pos;
    }

    public void setPos(PosType pos) {
        this.pos = pos;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public OverflowType getOverflow() {
        return overflow;
    }

    public void setOverflow(OverflowType overflow) {
        this.overflow = overflow;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
