package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.form.field.service.FormInputService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.ComboNumberAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.InputFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.tool.ui.enums.InputType;
import com.ds.esd.tool.ui.enums.ValueFormat;

@BottomBarMenu
@FormAnnotation(col = 2, customService = FormInputService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FieldInputView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;

    @CustomAnnotation(caption = "默认值")
    String value;

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = ValueFormat.class)
    @CustomAnnotation(caption = "输入校验")
    String valueFormat;
    @ComboNumberAnnotation(increment = "1", precision = 0)
    @ComboInputAnnotation(inputType = ComboInputType.spin)
    @CustomAnnotation(caption = "最大长度")
    Integer maxlength;
    @CustomAnnotation(caption = "输入类型")
    InputType inputType;
    @CustomAnnotation(caption = "输入域位置")
    HAlignType hAlign;
    @CustomAnnotation(caption = "是否多行")
    Boolean multiLines;
    @ComboNumberAnnotation(increment = "1", precision = 0)
    @ComboInputAnnotation(inputType = ComboInputType.spin)
    @CustomAnnotation(caption = "缩放倍率")
    Integer autoexpand;
    @CustomAnnotation(caption = "是否可选中")
    Boolean selectable;


    public FieldInputView() {

    }

    public InputType getInputType() {
        return inputType;
    }

    public void setInputType(InputType inputType) {
        this.inputType = inputType;
    }

    public FieldInputView(FieldFormConfig<InputFieldBean,?> config) {
        InputFieldBean inputFieldBean = config.getWidgetConfig();
        this.methodName = config.getSourceMethodName();
        this.viewClassName= config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.inputType = inputFieldBean.getInputType();
        this.selectable = inputFieldBean.getSelectable();

        this.valueFormat = inputFieldBean.getValueFormat();
        this.value = inputFieldBean.getValue();
        this.hAlign = inputFieldBean.gethAlign();

        this.maxlength = inputFieldBean.getMaxlength();
        this.multiLines = inputFieldBean.getMultiLines();
        this.autoexpand = inputFieldBean.getAutoexpand();


    }


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    public String getValueFormat() {
        return valueFormat;
    }

    public void setValueFormat(String valueFormat) {
        this.valueFormat = valueFormat;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public HAlignType gethAlign() {
        return hAlign;
    }

    public void sethAlign(HAlignType hAlign) {
        this.hAlign = hAlign;
    }


    public Integer getMaxlength() {
        return maxlength;
    }

    public void setMaxlength(Integer maxlength) {
        this.maxlength = maxlength;
    }

    public Boolean getMultiLines() {
        return multiLines;
    }

    public void setMultiLines(Boolean multiLines) {
        this.multiLines = multiLines;
    }

    public Integer getAutoexpand() {
        return autoexpand;
    }

    public void setAutoexpand(Integer autoexpand) {
        this.autoexpand = autoexpand;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
