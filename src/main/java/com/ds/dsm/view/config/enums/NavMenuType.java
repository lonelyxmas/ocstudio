package com.ds.dsm.view.config.enums;

import com.ds.enums.IconEnumstype;

public enum NavMenuType implements IconEnumstype {
    ViewConfig("视图配置", "spafont spa-icon-c-buttonview"),
    JavaTempConfig("代码模板", "spafont spa-icon-settingprj"),
    BindConfig("服务绑定", "spafont spa-icon-shukongjian");


    private final String name;
    private final String className;
    private final String imageClass;
    private final String packageName = "dsm.view.config";

    NavMenuType(String name, String imageClass) {
        this.name = name;
        this.className = packageName + "." + name();
        this.imageClass = imageClass;
    }


    public String getClassName() {
        return className;
    }

    public String getPackageName() {
        return packageName;
    }


    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
