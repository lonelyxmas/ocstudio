package com.ds.dsm.view.config.nav.items;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.custom.FieldBaseView;
import com.ds.dsm.view.config.form.field.custom.FieldLayoutView;
import com.ds.dsm.view.config.form.service.FormFieldService;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/items/")
@MethodChinaName(cname = "通用子项")
@BottomBarMenu
@NavGroupAnnotation(
        bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet}, customService = FormFieldService.class)
public class ModuleItemGroup {
    @CustomAnnotation(pid = true, hidden = true)
    private String esdclass;

    @CustomAnnotation(pid = true, hidden = true)
    private String methodName;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;

    @CustomAnnotation(uid = true, hidden = true)
    private String fieldname;

    public ModuleItemGroup() {
    }

    @RequestMapping(method = RequestMethod.POST, value = "FieldBaseView")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "基础信息", dock = Dock.top)
    @APIEventAnnotation(autoRun = true)
    @DialogAnnotation(height = "300")
    @ResponseBody
    public ResultModel<FieldBaseView> getFieldBaseView(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<FieldBaseView> result = new ResultModel<FieldBaseView>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomViewBean customFormViewBean = (CustomViewBean) customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) customFormViewBean.getFieldConfigMap().get(fieldname);
            result.setData(new FieldBaseView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldLayoutView")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-layout", caption = "布局", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<FieldLayoutView> getFieldLayoutView(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<FieldLayoutView> result = new ResultModel<FieldLayoutView>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) customFormViewBean.getFieldConfigMap().get(fieldname);

            result.setData(new FieldLayoutView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getEsdclass() {
        return esdclass;
    }

    public void setEsdclass(String esdclass) {
        this.esdclass = esdclass;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}
