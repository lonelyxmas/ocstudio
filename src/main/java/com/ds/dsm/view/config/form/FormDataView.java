package com.ds.dsm.view.config.form;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.CustomFormDataBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.ComponentType;

@BottomBarMenu
@FormAnnotation(col = 2, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FormDataView {

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "数据源（URL）")
    String dataUrl;

    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "保存操作（URL）")
    String saveUrl;

    @FieldAnnotation( rowHeight = "50")
    @CustomAnnotation(caption = "查询操作（URL）")
    String searchUrl;

    @FieldAnnotation( rowHeight = "50")
    @CustomAnnotation(caption = "重置操作（URL）")
    String reSetUrl;

    @FieldAnnotation(colSpan = -1, rowHeight = "150", componentType = ComponentType.CodeEditor)
    @CustomAnnotation(caption = "表达式")
    String expression;


    public FormDataView() {

    }

    public FormDataView(CustomFormDataBean dataBean) {

        this.viewInstId = dataBean.getViewInstId();
        this.domainId = dataBean.getDomainId();
        this.sourceClassName = dataBean.getSourceClassName();
        this.dataUrl = dataBean.getDataUrl();
        this.expression = dataBean.getExpression();
        this.reSetUrl = dataBean.getReSetUrl();
        this.searchUrl = dataBean.getSearchUrl();
        this.methodName = dataBean.getMethodName();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getSaveUrl() {
        return saveUrl;
    }

    public void setSaveUrl(String saveUrl) {
        this.saveUrl = saveUrl;
    }

    public String getSearchUrl() {
        return searchUrl;
    }

    public void setSearchUrl(String searchUrl) {
        this.searchUrl = searchUrl;
    }

    public String getReSetUrl() {
        return reSetUrl;
    }

    public void setReSetUrl(String reSetUrl) {
        this.reSetUrl = reSetUrl;
    }

    public String getDataUrl() {
        return dataUrl;
    }

    public void setDataUrl(String dataUrl) {
        this.dataUrl = dataUrl;
    }


}
