package com.ds.dsm.view.config.menu.bottommenu;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.BottomBarMenuBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.editor.extmenu.MenuBarBean;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.jds.core.esb.util.OgnlUtil;
import com.ds.web.util.AnnotationUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/entity/config/bottom/")
@MethodChinaName(cname = "底部菜单管理", imageClass = "spafont spa-icon-c-gallery")

public class BottomBarService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "菜单配置")
    @RequestMapping(method = RequestMethod.POST, value = "BottomMenuInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "菜单配置", imageClass = "spafont spa-icon-c-grid")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<BottomBarNav> getBottomMenuInfo(String domainId, String sourceClassName, String methodName) {
        ResultModel<BottomBarNav> result = new ResultModel<BottomBarNav>();
        return result;
    }


    @MethodChinaName(cname = "保存控制接口")
    @RequestMapping(value = {"saveBottomMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveAggEntity(@RequestBody BottomMenuConfigView menuConfigView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ApiClassConfig tableConfig = null;
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(menuConfigView.getSourceClassName(), domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(menuConfigView.getMethodName());
            BottomBarMenuBean barMenuBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                if (customTreeViewBean != null) {
                    barMenuBean = customTreeViewBean.getBottomBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                    }
                    OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                    customTreeViewBean.setBottomBar(barMenuBean);

                } else {
                    barMenuBean = viewBean.getBottomBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                    }
                    OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                    viewBean.setBottomBar(barMenuBean);
                }

            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                CustomTreeViewBean treeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                barMenuBean = treeViewBean.getBottomBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                treeViewBean.setBottomBar(barMenuBean);
            } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                barMenuBean = formViewBean.getBottomBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                formViewBean.setBottomBar(barMenuBean);

            } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                barMenuBean = customGridViewBean.getBottomBar();

                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                customGridViewBean.setBottomBar(barMenuBean);

            }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
                barMenuBean = customGalleryViewBean.getBottomBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                customGalleryViewBean.setBottomBar(barMenuBean);

            } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                barMenuBean = customGalleryViewBean.getBottomBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                customGalleryViewBean.setBottomBar(barMenuBean);
            } else if (methodAPIBean.getView() instanceof NavTabsViewBean) {
                NavTabsViewBean navTabsViewBean = (NavTabsViewBean) methodAPIBean.getView();
                barMenuBean = navTabsViewBean.getBottomBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                navTabsViewBean.setBottomBar(barMenuBean);
            }


            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;

    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
