package com.ds.dsm.view.config.nav.buttonviews;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.nav.CustomNavViewBean;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.buttonviews.NavButtonViewsViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/buttonviews/")
public class NavButtonViewsConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "NavButtonViewsBarView")
    @FormViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ModuleAnnotation(dock = Dock.fill, imageClass = "spafont spa-icon-rendermode", caption = "导航栏配置")
    @ResponseBody
    public ResultModel<NavButtonViewsBarView> getNavButtonViewBarView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<NavButtonViewsBarView> result = new ResultModel<NavButtonViewsBarView>();

        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig methodAPIBean = tableConfig.getCurrConfig().getMethodByName(methodName);
            NavButtonViewsViewBean tabsViewBean = (NavButtonViewsViewBean) methodAPIBean.getView();
            result.setData(new NavButtonViewsBarView(tabsViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateButtonViewsBase")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateButtonViewsBase(@RequestBody CustomNavViewBean configView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(configView.getSourceClassName(),configView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(configView.getMethodName());
            NavButtonViewsViewBean navGroupViewBean = (NavButtonViewsViewBean) customMethodAPIBean.getView();
            BeanMap.create(navGroupViewBean).putAll(BeanMap.create(configView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "清空列表配置")
    @RequestMapping(method = RequestMethod.POST, value = "clear")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clear(String sourceClassName, String methodName,String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(methodName);
            NavButtonViewsViewBean navGroupViewBean = (NavButtonViewsViewBean) customMethodAPIBean.getView();
            customMethodAPIBean.setDataBean(null);
            customMethodAPIBean.setView(navGroupViewBean);

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

}
