package com.ds.dsm.view.config.form;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.item.FormFieldNavTree;
import com.ds.dsm.view.config.form.field.module.FieldModuleGridInfo;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.field.combo.ComboModuleFieldBean;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.ESDFieldConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/dsm/view/config/form/module/")
public class ViewFormModulesService {

    @RequestMapping(method = RequestMethod.POST, value = "ChildModuleTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<FormFieldNavTree>> getChildModuleTree(String domainId, String sourceClassName, String methodName, String viewInstId) {

        TreeListResultModel<List<FormFieldNavTree>> result = new TreeListResultModel<>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = esdClassConfig.getMethodByName(methodName);
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) methodAPIBean.getView();
            List<ESDFieldConfig> esdFieldConfigs = new ArrayList<>();
            List<String> fieldNames = customFormViewBean.getDisplayFieldNames();
            for (String fieldname : fieldNames) {
                FieldFormConfig fieldFormConfig = (FieldFormConfig) customFormViewBean.getFieldConfigMap().get(fieldname);
                if (fieldFormConfig != null && fieldFormConfig.getComboConfig() != null && (fieldFormConfig.getComboConfig() instanceof ComboModuleFieldBean)) {
                    esdFieldConfigs.add(fieldFormConfig);
                }
            }
            result = TreePageUtil.getTreeList(esdFieldConfigs, FormFieldNavTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;

    }


    @RequestMapping(method = RequestMethod.POST, value = "ModuleFormList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "子模块")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<FieldModuleGridInfo>> getModuleFormList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldModuleGridInfo>> cols = new ListResultModel();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = esdClassConfig.getMethodByName(methodName);
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) customMethodAPIBean.getView();
            Set<String> fieldNames = customFormViewBean.getFieldNames();
            List<FieldFormConfig> fields = new ArrayList<>();
            for (String fieldname : fieldNames) {
                FieldFormConfig fieldFormConfig = (FieldFormConfig) customFormViewBean.getFieldConfigMap().get(fieldname);
                if (fieldFormConfig != null && fieldFormConfig.getComboConfig() != null && (fieldFormConfig.getComboConfig() instanceof ComboModuleFieldBean)) {
                    fields.add(fieldFormConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldModuleGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


}
