package com.ds.dsm.view.config.form.field.item;

import com.ds.dsm.view.config.form.field.combo.ComboWidgetService;
import com.ds.dsm.view.config.form.field.combo.service.AnimBinderService;
import com.ds.dsm.view.config.form.field.custom.ContainerService;
import com.ds.dsm.view.config.form.field.custom.DisabledService;
import com.ds.dsm.view.config.form.field.custom.LabelService;
import com.ds.dsm.view.config.form.field.custom.TipsService;
import com.ds.dsm.view.config.form.field.list.FormCheckBoxService;
import com.ds.dsm.view.config.form.field.list.FormListService;
import com.ds.dsm.view.config.form.field.list.FormRadioService;
import com.ds.dsm.view.config.form.field.list.FormStatusButtonsService;
import com.ds.dsm.view.config.form.field.service.*;
import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.CustomImageType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum FieldWidgetNavItem implements TreeItem {
    MultiLines(ComponentType.MultiLines, FormTextEditorService.class, false, false, false),
    Element(ComponentType.Element, FormElementService.class, false, false, false),
    StatusButtons(ComponentType.HiddenInput, FormStatusButtonsService.class, false, false, false),
    FileUpload(ComponentType.FileUpload, FormFileUploadService.class, false, false, false),
    Hidden(ComponentType.HiddenInput, FormSpanService.class, false, false, false),
    Span(ComponentType.Span, FormSpanService.class, false, false, false),
    List(ComponentType.List, FormListService.class, false, false, false),
    RadioBox(ComponentType.RadioBox, FormRadioService.class, false, false, false),
    CheckBox(ComponentType.CheckBox, FormCheckBoxService.class, false, false, false),
    AnimBinder(ComponentType.AnimBinder, AnimBinderService.class, false, false, false),
    Audio(ComponentType.Audio, FormAudioService.class, false, false, false),
    Video(ComponentType.Video, FormVideoService.class, false, false, false),
    Button(ComponentType.Button, FormButtonService.class, false, false, false),
    JavaEditor(ComponentType.JavaEditor, FormJavaEditorService.class, false, false, false),
    CodeEditor(ComponentType.CodeEditor, FormCodeEditorService.class, false, false, false),


    Flash(ComponentType.Flash, FormFlashService.class, false, false, false),
    Icon(ComponentType.Icon, FormIconService.class, false, false, false),
    Image(ComponentType.Image, FileImageService.class, false, false, false),
    Input(ComponentType.Input, FormInputService.class, false, false, false),
    Label(ComponentType.Label, FormLabelService.class, false, false, false),
    Module(ComponentType.Module, FormModuleService.class, false, false, false),
    ProgressBar(ComponentType.ProgressBar, FormProgressBarService.class, false, false, false),
    Slider(ComponentType.Slider, FormSliderService.class, false, false, false),
    SVGPaper(ComponentType.SVGPaper, FormSVGPaperService.class, false, false, false),
    RichEditor(ComponentType.RichEditor, FormRichEditorService.class, false, false, false),
    TextEditor(ComponentType.TextEditor, FormTextEditorService.class, false, false, false),

    //ComboInput(ComponentType.ComboInput, ComboWidgetService.class, false, false, false),
    ComboInput("扩展配置", ComponentType.ComboInput.getImageClass(), new ComponentType[]{ComponentType.ComboInput}, new ComponentType[]{}, ComboWidgetService.class, false, false, false),
    CustomLabel("标签配置(Lable)", CustomImageType.label.getImageClass(), ComponentType.values(), new ComponentType[]{ComponentType.Module, ComponentType.Image, ComponentType.Label, ComponentType.Icon, ComponentType.Element, ComponentType.SVGPaper}, LabelService.class, false, false, false),
    Tips("提示信息(Tips)", CustomImageType.info.getImageClass(), ComponentType.values(), new ComponentType[]{}, TipsService.class, false, false, false),
    Container("容器配置", CustomImageType.pop.getImageClass(), new ComponentType[]{ComponentType.SVGPaper, ComponentType.Element, ComponentType.Image, ComponentType.Module, ComponentType.ComboInput}, new ComponentType[]{}, ContainerService.class, false, false, false),
    Disabled("禁用配置（Disabled）", CustomImageType.dragstop.getImageClass(), new ComponentType[]{ComponentType.ComboInput, ComponentType.TextEditor, ComponentType.Slider, ComponentType.RichEditor, ComponentType.Input, ComponentType.ProgressBar, ComponentType.List, ComponentType.RadioBox, ComponentType.CheckBox}, new ComponentType[]{}, DisabledService.class, false, false, false);


    private final ComponentType[] componentType;
    private final ComponentType[] exclude;
    private final String name;
    private final Class bindClass;
    private final String imageClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    FieldWidgetNavItem(String name, String imageClass, ComponentType[] componentType, ComponentType[] exclude, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {

        this.exclude = exclude;
        this.componentType = componentType;
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    FieldWidgetNavItem(ComponentType componentType, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.componentType = new ComponentType[]{componentType};
        this.exclude = new ComponentType[]{};
        this.name = componentType.getName();
        this.imageClass = componentType.getImageClass();
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }


    public static List<FieldWidgetNavItem> getWidgetByComponentType(ComponentType componentType) {
        List items = new ArrayList();
        for (FieldWidgetNavItem widgetNavItem : FieldWidgetNavItem.values()) {
            List<ComponentType> comType = Arrays.asList(widgetNavItem.getComponentType());
            List<ComponentType> excludeType = Arrays.asList(widgetNavItem.getExclude());
            if (comType.contains(componentType) && !excludeType.contains(componentType)) {
                items.add(widgetNavItem);
            }

        }
        return items;
    }

    public ComponentType[] getExclude() {
        return exclude;
    }

    public ComponentType[] getComponentType() {
        return componentType;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
