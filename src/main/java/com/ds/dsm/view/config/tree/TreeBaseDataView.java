package com.ds.dsm.view.config.tree;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.form.TreeDataBaseBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;

@FormAnnotation(col = 1)
public class TreeBaseDataView {


    @CustomAnnotation(caption = "根节点ID")
    String rootId;


    @CustomAnnotation(caption = "显示字段")
    String fieldCaption;

    @CustomAnnotation(caption = "ID字段名")
    String fieldId;

    @CustomAnnotation(caption = "分割值")
    String valueSeparator;


    @CustomAnnotation(caption = "查找栏")
    Boolean heplBar;

    @CustomAnnotation(caption = "表单提交")
    Boolean formField;


    @CustomAnnotation(hidden = true, uid = true)
    String sourceClassName;


    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;


    public TreeBaseDataView() {

    }

    public TreeBaseDataView(CustomTreeViewBean treeConfig, TreeDataBaseBean treeDataBean) {
        this.formField = treeConfig.getFormField();
        this.heplBar = treeConfig.getHeplBar();
        this.valueSeparator = treeConfig.getValueSeparator();
        this.sourceClassName = treeConfig.getClassName();
        this.fieldCaption = treeDataBean.getFieldCaption();
        this.fieldId = treeDataBean.getFieldId();
        this.rootId = treeDataBean.getRootId();
        this.domainId = treeConfig.getDomainId();
        this.viewInstId = treeConfig.getViewInstId();
        this.methodName = treeConfig.getMethodName();
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getRootId() {
        return rootId;
    }

    public void setRootId(String rootId) {
        this.rootId = rootId;
    }

    public String getFieldCaption() {
        return fieldCaption;
    }

    public void setFieldCaption(String fieldCaption) {
        this.fieldCaption = fieldCaption;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public Boolean getHeplBar() {
        return heplBar;
    }

    public void setHeplBar(Boolean heplBar) {
        this.heplBar = heplBar;
    }

    public Boolean getFormField() {
        return formField;
    }

    public void setFormField(Boolean formField) {
        this.formField = formField;
    }

    public String getValueSeparator() {
        return valueSeparator;
    }

    public void setValueSeparator(String valueSeparator) {
        this.valueSeparator = valueSeparator;
    }
}
