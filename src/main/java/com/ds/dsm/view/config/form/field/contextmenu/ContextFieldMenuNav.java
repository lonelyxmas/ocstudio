package com.ds.dsm.view.config.form.field.contextmenu;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.contextmenu.menuclass.ContextFieldMenuGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.RightContextMenuBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.AnnotationUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save}, customService = {ContextFieldService.class})
@RequestMapping("/dsm/view/config/form/field/contextmenu/")
public class ContextFieldMenuNav {

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;
    @CustomAnnotation(hidden = true, uid = true)
    public String sourceClassName;

    public ContextFieldMenuNav() {

    }

    @MethodChinaName(cname = "右键菜单信息")
    @RequestMapping(method = RequestMethod.POST, value = "FieldMenuConfig")
    @FormViewAnnotation
    @UIAnnotation(dock = Dock.top, height = "220")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<ContextFieldMenuConfigView> getFieldMenuConfig(String domainId, String sourceClassName, String fieldname, String methodName) {
        ResultModel<ContextFieldMenuConfigView> resultModel = new ResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) methodAPIBean.getView();
            FieldFormConfig fieldConfig = (FieldFormConfig) customFormViewBean.getFieldConfigMap().get(fieldname);
            RightContextMenuBean barMenuBean = fieldConfig.getContextMenuBean();
            if (barMenuBean == null) {
                barMenuBean = AnnotationUtil.fillDefaultValue(RightContextMenu.class, new RightContextMenuBean());
            }

            ContextFieldMenuConfigView configView = new ContextFieldMenuConfigView(barMenuBean, domainId, fieldname, sourceClassName, methodName);
            resultModel.setData(configView);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }


    @MethodChinaName(cname = "右键菜单选择")
    @RequestMapping(method = RequestMethod.POST, value = "FieldMenuClass")
    @ModuleAnnotation(caption = "右键菜单选择", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @CustomAnnotation(index = 1)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<ContextFieldMenuGridView>> getFieldMenuClass(String domainId, String methodName, String sourceClassName, String fieldname) {
        ListResultModel<List<ContextFieldMenuGridView>> result = new ListResultModel();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            List<ESDClass> esdClassList = new ArrayList<>();
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) methodAPIBean.getView();
            FieldFormConfig fieldConfig = (FieldFormConfig) customFormViewBean.getFieldConfigMap().get(fieldname);
            RightContextMenuBean barMenuBean = fieldConfig.getContextMenuBean();
            if (barMenuBean != null && barMenuBean.getMenuClass() != null) {
                Class[] clazzs = barMenuBean.getMenuClass();
                for (Class clazz : clazzs) {
                    esdClassList.add(DSMFactory.getInstance().getClassManager().getAggEntityByName(clazz.getName(), domainId, false));
                }
            }
            result = PageUtil.getDefaultPageList(esdClassList, ContextFieldMenuGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}

