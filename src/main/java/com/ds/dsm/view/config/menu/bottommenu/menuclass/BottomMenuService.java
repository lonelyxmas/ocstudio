package com.ds.dsm.view.config.menu.bottommenu.menuclass;

import com.ds.common.JDSException;
import com.ds.common.util.ClassUtility;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.menu.AggMenuNav;
import com.ds.dsm.aggregation.config.menu.tree.AggMenuConfigTree;
import com.ds.dsm.aggregation.config.menu.tree.AggMenuMainNavItem;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.BottomBarMenuBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/menu/bottom/")
@MethodChinaName(cname = "菜单管理", imageClass = "spafont spa-icon-c-gallery")
public class BottomMenuService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "聚合动作信息")
    @RequestMapping(method = RequestMethod.POST, value = "BottomMenuInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "装载", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggMenuNav> getBottomMenuInfo(String domainId, String sourceClassName) {
        ResultModel<AggMenuNav> result = new ResultModel<AggMenuNav>();
        return result;
    }

    @MethodChinaName(cname = "导入菜单动作")
    @RequestMapping(value = {"BottomMenuTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "导入菜单动作", dynLoad = true, dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<AllBottomMenuTree>> getBottomMenuTree(String domainId, String sourceClassName, String methodName) {
        TreeListResultModel<List<AllBottomMenuTree>> result = new TreeListResultModel<List<AllBottomMenuTree>>();
        try {
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            List<String> ids = Arrays.asList(bean.getAggMenuNames().toArray(new String[]{}));
            result = TreePageUtil.getTreeList(Arrays.asList(CustomMenuType.values()), AllBottomMenuTree.class, ids);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "保存菜单")
    @RequestMapping(value = {"saveBottomMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveBottomMenu(String domainId, String sourceClassName, String methodName, String BottomMenuTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (BottomMenuTree != null) {
            try {
                ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
                MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);

                String[] esdClassNames = StringUtility.split(BottomMenuTree, ";");
                List<Class> menus = new ArrayList<>();
                for (String esdClassName : esdClassNames) {
                    try {
                        menus.add(ClassUtility.loadClass(esdClassName));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                BottomBarMenuBean barMenuBean = null;


                if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                    NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                    NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                    CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                    if (customTreeViewBean != null) {
                        barMenuBean = customTreeViewBean.getBottomBar();
                        if (barMenuBean == null) {
                            barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                        }
                        barMenuBean.setMenuClass(menus.toArray(new Class[]{}));
                        customTreeViewBean.setBottomBar(barMenuBean);

                    } else {
                        barMenuBean = viewBean.getBottomBar();
                        if (barMenuBean == null) {
                            barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                        }
                        barMenuBean.setMenuClass(menus.toArray(new Class[]{}));
                        viewBean.setBottomBar(barMenuBean);
                    }

                } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                    CustomTreeViewBean treeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                    barMenuBean = treeViewBean.getBottomBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                    }
                    barMenuBean.setMenuClass(menus.toArray(new Class[]{}));
                    treeViewBean.setBottomBar(barMenuBean);

                } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                    CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                    barMenuBean = formViewBean.getBottomBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                    }
                    barMenuBean.setMenuClass(menus.toArray(new Class[]{}));
                    formViewBean.setBottomBar(barMenuBean);

                } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                    CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                    barMenuBean = customGridViewBean.getBottomBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                    }
                    barMenuBean.setMenuClass(menus.toArray(new Class[]{}));
                    customGridViewBean.setBottomBar(barMenuBean);
                }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                    CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
                    barMenuBean = customGalleryViewBean.getBottomBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                    }
                    barMenuBean.setMenuClass(menus.toArray(new Class[]{}));
                    customGalleryViewBean.setBottomBar(barMenuBean);

                } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                    CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                    barMenuBean = customGalleryViewBean.getBottomBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(BottomBarMenu.class, new BottomBarMenuBean());
                    }
                    barMenuBean.setMenuClass(menus.toArray(new Class[]{}));
                    customGalleryViewBean.setBottomBar(barMenuBean);
                }

                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除实体关系")
    @RequestMapping(value = {"delButtomMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delButtomMenu(String domainId, String sourceClassName, String methodName, String menuClass) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (domainId != null) {
            try {
                String[] esdClassNames = StringUtility.split(menuClass, ";");
                List<Class> menus = new ArrayList<>();
                for (String esdClassName : esdClassNames) {
                    try {
                        menus.add(ClassUtility.loadClass(esdClassName));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
                MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);


                BottomBarMenuBean barMenuBean = null;
                if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                    NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                    NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                    CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                    if (customTreeViewBean != null) {
                        barMenuBean = customTreeViewBean.getBottomBar();
                    } else {
                        barMenuBean = viewBean.getBottomBar();
                    }
                } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                    CustomTreeViewBean treeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                    barMenuBean = treeViewBean.getBottomBar();
                } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                    CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                    barMenuBean = formViewBean.getBottomBar();
                } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                    CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                    barMenuBean = customGridViewBean.getBottomBar();
                }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                    CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
                    barMenuBean = customGalleryViewBean.getBottomBar();
                } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                    CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                    barMenuBean = customGalleryViewBean.getBottomBar();
                }

                Class[] menuClassList = barMenuBean.getMenuClass();
                List<Class> classList = new ArrayList<>();
                classList.addAll(Arrays.asList(menuClassList));
                for (Class clazz : menus) {
                    classList.remove(clazz);
                }
                barMenuBean.setMenuClass(classList.toArray(new Class[]{}));
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @RequestMapping(method = RequestMethod.POST, value = "AggConfigTree")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "900", height = "680")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "菜单配置")
    @ResponseBody
    public TreeListResultModel<List<AggMenuConfigTree>> getAggConfigTree(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggMenuConfigTree>> resultModel = new TreeListResultModel<List<AggMenuConfigTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(AggMenuMainNavItem.values()), AggMenuConfigTree.class);
        return resultModel;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
