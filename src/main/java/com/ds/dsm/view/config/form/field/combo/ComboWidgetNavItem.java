package com.ds.dsm.view.config.form.field.combo;

import com.ds.dsm.view.config.form.field.combo.list.CustomListService;
import com.ds.dsm.view.config.form.field.combo.service.*;
import com.ds.dsm.view.config.form.field.custom.DockService;
import com.ds.dsm.view.config.form.field.list.FormCheckBoxService;
import com.ds.dsm.view.config.form.field.list.FormRadioService;
import com.ds.dsm.view.config.form.field.service.*;
import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.CustomImageType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ComboWidgetNavItem implements TreeItem {


    radiobox(ComboInputType.radiobox, FormRadioService.class, false, false, false),
    combobox(ComboInputType.combobox, FormComboBoxService.class, false, false, false),
    listbox(ComboInputType.listbox, ComboListBoxService.class, false, false, false),
    getter(ComboInputType.getter, ComboListBoxService.class, false, false, false),
    helpinput(ComboInputType.helpinput, ComboListBoxService.class, false, false, false),
    dropbutton(ComboInputType.dropbutton, FormButtonService.class, false, false, false),
    cmdbox(ComboInputType.cmdbox, ComboPopService.class, false, false, false),
    cmd(ComboInputType.cmd, ComboPopService.class, false, false, false),
    checkbox(ComboInputType.checkbox, FormCheckBoxService.class, false, false, false),
    password(ComboInputType.password, FormInputService.class, false, false, false),
    file(ComboInputType.file, ComboFileService.class, false, false, false),
    textarea(ComboInputType.textarea, FormTextEditorService.class, false, false, false),
    label(ComboInputType.label, FormLabelService.class, false, false, false),
    image(ComboInputType.image, ComboImageService.class, false, false, false),
    text(ComboInputType.text, FormTextEditorService.class, false, false, false),
    datetime(ComboInputType.datetime, ComboDateTimeService.class, false, false, false),
    color(ComboInputType.color, ComboColorService.class, false, false, false),
    counter(ComboInputType.counter, ComboNumberService.class, false, false, false),
    currency(ComboInputType.currency, ComboNumberService.class, false, false, false),
    spin(ComboInputType.spin, ComboNumberService.class, false, false, false),
    split(ComboInputType.split, FormTextEditorService.class, false, false, false),
    none(ComboInputType.none, ComboNoneService.class, false, false, false),


    module("模块配置", ComboInputType.module.getImageClass(), new ComboInputType[]{ComboInputType.module, ComboInputType.combobox}, ComboModuleService.class, false, false, false),
    button("按钮", "spafont spa-icon-c-nativebutton", new ComboInputType[]{ComboInputType.button, ComboInputType.dropbutton}, FormButtonService.class, false, false, false),
    popbox("弹出框配置", "xui-uicmd-popbox", new ComboInputType[]{ComboInputType.popbox, ComboInputType.cmdbox}, ComboPopService.class, false, false, false),
    combolist("集合配置", "spafont spa-icon-tools", new ComboInputType[]{ComboInputType.radiobox, ComboInputType.helpinput, ComboInputType.listbox, ComboInputType.combobox}, ComboListService.class, false, false, false),
    itemlist("选择项配置", "spafont spa-icon-c-iconslist", new ComboInputType[]{ComboInputType.radiobox, ComboInputType.helpinput, ComboInputType.listbox, ComboInputType.getter, ComboInputType.combobox}, CustomListService.class, false, false, false),
    dock("停靠配置", CustomImageType.div.getImageClass(), new ComboInputType[]{ComboInputType.radiobox, ComboInputType.listbox, ComboInputType.module, ComboInputType.popbox, ComboInputType.helpinput, ComboInputType.cmdbox, ComboInputType.getter, ComboInputType.combobox}, DockService.class, false, false, false),
    number("数字配置", "spafont spa-icon-c-numberinput", new ComboInputType[]{ComboInputType.currency, ComboInputType.counter, ComboInputType.number}, ComboNumberService.class, false, false, false),
    time("时间配置", "spafont spa-icon-c-timeinput", new ComboInputType[]{ComboInputType.time, ComboInputType.datetime}, ComboTimeService.class, false, false, false),
    date("日期配置", "spafont spa-icon-c-dateinput", new ComboInputType[]{ComboInputType.date, ComboInputType.datetime}, ComboTimeService.class, false, false, false);

    private final ComboInputType[] inputTypes;
    private final String name;
    private final Class bindClass;
    private final String imageClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;

    ComboWidgetNavItem(ComboInputType inputType, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.inputTypes = new ComboInputType[]{inputType};
        this.name = inputType.getName();
        this.imageClass = inputType.getImageClass();
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    ComboWidgetNavItem(String name, String imageClass, ComboInputType[] inputTypes, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.inputTypes = inputTypes;
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }


    public static List<ComboWidgetNavItem> getInputByComponentType(ComboInputType inputType) {
        List items = new ArrayList();
        for (ComboWidgetNavItem inputItem : ComboWidgetNavItem.values()) {
            if (Arrays.asList(inputItem.getInputTypes()).contains(inputType)) {
                items.add(inputItem);
            }
        }
        return items;
    }

    public ComboInputType[] getInputTypes() {
        return inputTypes;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
