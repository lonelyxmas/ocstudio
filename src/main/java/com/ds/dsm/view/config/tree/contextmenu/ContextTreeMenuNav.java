package com.ds.dsm.view.config.tree.contextmenu;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.manager.view.BuildViewMenu;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.RightContextMenuBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@BottomBarMenu(menuClass = BuildViewMenu.class)
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save}, customService = {ContextTreeService.class})
@RequestMapping("/dsm/view/config/tree/contextmenu/")
@MethodChinaName(cname = "右键菜单按钮列表", imageClass = "spafont spa-icon-c-databinder")
public class ContextTreeMenuNav {

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;
    @CustomAnnotation(hidden = true, uid = true)
    public String sourceClassName;

    public ContextTreeMenuNav() {

    }

    @MethodChinaName(cname = "右键菜单操作按钮")
    @RequestMapping(method = RequestMethod.POST, value = "ContextMenuConfig")
    @FormViewAnnotation
    @UIAnnotation(dock = Dock.top, height = "220")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<ContextTreeMenuConfigView> getContextMenuConfig(String domainId, String sourceClassName, String entityClassName, String childViewId, String methodName) {
        ResultModel<ContextTreeMenuConfigView> resultModel = new ResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }
            RightContextMenuBean customMenu = customTreeViewBean.getContextMenuBean();
            if (customMenu == null) {
                customMenu = AnnotationUtil.fillDefaultValue(RightContextMenu.class, new RightContextMenuBean());
            }

            ContextTreeMenuConfigView configView = new ContextTreeMenuConfigView(customMenu, domainId, childViewId, sourceClassName, methodName);
            resultModel.setData(configView);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }


    @MethodChinaName(cname = "详细信息")
    @RequestMapping(method = RequestMethod.POST, value = "ContextMenuMetaView")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill)
    @ResponseBody
    public ResultModel<ContextTreeMenuMetaView> getContextMenuMetaView(String sourceClassName, String methodName, String childViewId, String domainId) {
        ResultModel<ContextTreeMenuMetaView> result = new ResultModel<ContextTreeMenuMetaView>();
        return result;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}

