package com.ds.dsm.view.config.nav.tab;

import com.ds.dsm.view.config.menu.bottommenu.BottomBarService;
import com.ds.dsm.view.config.menu.menubar.MenuBarConfigService;
import com.ds.dsm.view.config.menu.toolbar.ToolBarConfigService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum NavTabsButtonItems implements TreeItem {

    TabsButtonList("工具栏", "spafont spa-icon-c-toolbar", ToolBarConfigService.class, false, false, false),
    TabsMenuList("菜单栏", "spafont spa-icon-c-menu", MenuBarConfigService.class, false, false, false),
    TabsBottomButtonList("底部按钮组", "spafont spa-icon-c-statusbutton", BottomBarService.class, false, false, false);

    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    NavTabsButtonItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
