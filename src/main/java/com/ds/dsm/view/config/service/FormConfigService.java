package com.ds.dsm.view.config.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.FormBean;
import com.ds.dsm.view.config.form.FormDataView;
import com.ds.dsm.view.config.form.FormRowView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.dsm.DSMFactory;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/form/")
public class FormConfigService {


    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFormInfo")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formSave)
    public @ResponseBody
    ResultModel<Boolean> updateFormInfo(@RequestBody FormBean bean) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(bean.getSourceClassName(), bean.getDomainId());
            MethodConfig customMethodAPIBean = apiClassConfig.getMethodByName(bean.getMethodName());
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) customMethodAPIBean.getView();
            BeanMap.create(customFormViewBean).putAll(BeanMap.create(bean));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(apiClassConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "编辑数据信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFormData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateFormData(@RequestBody FormDataView bean) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(bean.getSourceClassName(), bean.getDomainId());
            MethodConfig customMethodAPIBean = apiClassConfig.getMethodByName(bean.getMethodName());
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) customMethodAPIBean.getView();
            BeanMap.create(customFormViewBean).putAll(BeanMap.create(bean));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(apiClassConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "清空配置")
    @RequestMapping(method = RequestMethod.POST, value = "clearData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clearData(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = apiClassConfig.getMethodByName(methodName);
            methodAPIBean.setView(null);

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(apiClassConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "编辑行配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFormRow")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateFormRow(@RequestBody FormRowView rowView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(rowView.getSourceClassName(), rowView.getDomainId());
            MethodConfig customMethodAPIBean = apiClassConfig.getMethodByName(rowView.getMethodName());
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) customMethodAPIBean.getView();
            BeanMap.create(customFormViewBean).putAll(BeanMap.create(rowView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(apiClassConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
