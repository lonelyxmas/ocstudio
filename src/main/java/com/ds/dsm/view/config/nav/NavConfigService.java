package com.ds.dsm.view.config.nav;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/")
@MethodChinaName(cname = "导航视图")
public class NavConfigService {


    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateGroupBase")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateGroupBase(@RequestBody NavTabsViewBean configView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(configView.getSourceClassName(), configView.getViewInstId());
            MethodConfig methodAPIBean = classConfig.getMethodByName(configView.getMethodName());
            NavTabsViewBean customViewBean = (NavTabsViewBean) methodAPIBean.getView();
            BeanMap.create(customViewBean).putAll(BeanMap.create(configView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
            //DSMFactory.getInstance().getViewManager().updateViewEntityConfig(classConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

//    @MethodChinaName(cname = "编辑数据信息")
//    @RequestMapping(method = RequestMethod.POST, value = "updateWinData")
//    @APIEventAnnotation(callback = {CustomCallBack.Reload})
//    public @ResponseBody
//    ResultModel<Boolean> updateGroupWinData(@RequestBody ModuleConfigView bean) {
//        ResultModel<Boolean> result = new ResultModel<Boolean>();
//        try {
//            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(bean.getSourceClassName(),bean.getViewInstId());
//            MethodConfig methodAPIBean = classConfig.getSourceConfig().getMethodByName(bean.getMethodName());
//            CustomModuleBean customModuleBean = methodAPIBean.getModuleBean();
//            BeanMap.create(customModuleBean).putAll(BeanMap.create(bean));
//            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
//           // DSMFactory.getInstance().getViewManager().updateViewEntityConfig(classConfig);
//        } catch (JDSException e) {
//            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
//            errorResult.setErrdes(e.getMessage());
//            result = errorResult;
//        }
//        return result;
//    }
//
//
//    @MethodChinaName(cname = "清空列表配置")
//    @RequestMapping(method = RequestMethod.POST, value = "clearData")
//    @APIEventAnnotation(callback = {CustomCallBack.Reload})
//    public @ResponseBody
//    ResultModel<Boolean> clearData(String sourceClassName, String methodName,String domainId, String viewInstId) {
//        ResultModel<Boolean> result = new ResultModel<Boolean>();
//        try {
//            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
//
//            DSMFactory.getInstance().getViewManager().updateViewEntityConfig(classConfig);
//        } catch (JDSException e) {
//            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
//            errorResult.setErrdes(e.getMessage());
//            result = errorResult;
//        }
//
//        return result;
//    }

    @MethodChinaName(cname = "清空列表配置")
    @RequestMapping(method = RequestMethod.POST, value = "clear")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clear(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {

            ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig methodAPIBean = apiClassConfig.getMethodByName(methodName);
            methodAPIBean.setView(null);

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(apiClassConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

}
