package com.ds.dsm.view.config.gallery.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.gallery.view.GalleryBaseView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.jds.core.esb.util.OgnlUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/gallery/")
public class GalleryBaseInfoService {


    @MethodChinaName(cname = "基础信息配置")
    @RequestMapping(method = RequestMethod.POST, value = "GalleryBaseView")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-values", caption = "列表信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<GalleryBaseView> getGalleryBaseView(String sourceClassName, String methodName, String domainId, String viewInstId) {

        ResultModel<GalleryBaseView> result = new ResultModel<GalleryBaseView>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomGalleryViewBean customGalleryViewBean=null;
            if (methodAPIBean.getView() instanceof NavGalleryViewBean){
                customGalleryViewBean=((NavGalleryViewBean)methodAPIBean.getView()).getGalleryViewBean();
            }else if(methodAPIBean.getView() instanceof NavGalleryViewBean){
                customGalleryViewBean= (CustomGalleryViewBean) methodAPIBean.getView();
            }
            result.setData(new GalleryBaseView(customGalleryViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "编辑配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateGridBase")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateGalleryBase(@RequestBody GalleryBaseView config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(config.getSourceClassName(), config.getDomainId());
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(config.getMethodName());
            CustomGalleryViewBean viewBean = (CustomGalleryViewBean) customMethodAPIBean.getView();
            OgnlUtil.setProperties(BeanMap.create(config), viewBean, false, false);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

}
