package com.ds.dsm.view.config.nav.layout;

import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.layout.CustomLayoutViewBean;
import com.ds.esd.tool.ui.enums.BorderType;
import com.ds.esd.tool.ui.enums.LayoutType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/layout/")
@FormAnnotation(col = 2, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@MethodChinaName(cname = "布局", imageClass = "spafont spa-icon-c-gallery")
public class NavLayoutConfigView {


    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(uid = true, hidden = true)
    private String methodName;

    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(uid = true, hidden = true)
    public String projectVersionName;

    @CustomAnnotation(caption = "上边距")
    String top;
    @CustomAnnotation(caption = "左边距")
    String left;
    @CustomAnnotation(caption = "边框类型")
    BorderType borderType;
    @CustomAnnotation(caption = "布局类型")
    LayoutType type;
    @CustomAnnotation(caption = "是否可拖动")
    Boolean dragSortable;
    @CustomAnnotation(caption = "可拖放KEY")
    List<String> listKey;
    @CustomAnnotation(caption = "自适应")
    Boolean flexSize;
    @CustomAnnotation(caption = "背景透明")
    Boolean transparent;
    @CustomAnnotation(caption = "布局列数")
    Integer conLayoutColumns;



    public NavLayoutConfigView() {

    }

    public NavLayoutConfigView(CustomLayoutViewBean layoutViewBean) {
        this.domainId = layoutViewBean.getDomainId();
        this.viewInstId = layoutViewBean.getViewInstId();
        this.top = layoutViewBean.getTop();
        this.left = layoutViewBean.getLeft();
        this.borderType = layoutViewBean.getBorderType();
        this.type = layoutViewBean.getType();
        this.dragSortable = layoutViewBean.getDragSortable();
        this.listKey = layoutViewBean.getListKey();
        this.flexSize = layoutViewBean.getFlexSize();
        this.transparent = layoutViewBean.getTransparent();
        this.conLayoutColumns = layoutViewBean.getConLayoutColumns();
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public BorderType getBorderType() {
        return borderType;
    }

    public void setBorderType(BorderType borderType) {
        this.borderType = borderType;
    }

    public LayoutType getType() {
        return type;
    }

    public void setType(LayoutType type) {
        this.type = type;
    }

    public Boolean getDragSortable() {
        return dragSortable;
    }

    public void setDragSortable(Boolean dragSortable) {
        this.dragSortable = dragSortable;
    }

    public List<String> getListKey() {
        return listKey;
    }

    public void setListKey(List<String> listKey) {
        this.listKey = listKey;
    }

    public Boolean getFlexSize() {
        return flexSize;
    }

    public void setFlexSize(Boolean flexSize) {
        this.flexSize = flexSize;
    }

    public Boolean getTransparent() {
        return transparent;
    }

    public void setTransparent(Boolean transparent) {
        this.transparent = transparent;
    }

    public Integer getConLayoutColumns() {
        return conLayoutColumns;
    }

    public void setConLayoutColumns(Integer conLayoutColumns) {
        this.conLayoutColumns = conLayoutColumns;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
