package com.ds.dsm.view.config.tree;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;

import com.ds.esd.custom.bean.MethodConfig;

import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.form.TreeDataBaseBean;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/tree/")
@ButtonViewsAnnotation(barLocation = BarLocationType.top, sideBarStatus = SideBarStatusType.expand, barSize = "3em")
public class TreeDataGroup {
    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;
    public TreeDataGroup() {
    }


    @RequestMapping(method = RequestMethod.POST, value = "TreeDataView")
    @FormViewAnnotation(reSetUrl = "clearData", saveUrl = "updateFormData")
    @ModuleAnnotation( imageClass = "spafont spa-icon-values", caption = "数据信息",dock = Dock.fill)
    @CustomAnnotation( index = 0)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<TreeDataView> getTreeDataView(String sourceClassName, String methodName,String domainId, String viewInstId) {
        ResultModel<TreeDataView> result = new ResultModel<TreeDataView>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);

            //DSMFactory.getInstance().getViewManager().updateViewEntityConfig(classConfig);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(methodName);
            TreeDataBaseBean customTreeViewBean = (TreeDataBaseBean) customMethodAPIBean.getDataBean();

            result.setData(new TreeDataView(customTreeViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}
