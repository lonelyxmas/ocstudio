package com.ds.dsm.view.config.nav.folding;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class NavFoldingTabsTree extends TreeListItem {
    @Pid
    String sourceClassName;
    @Pid
    String sourceMethodName;
    @Pid
    String viewInstId;
    @Pid
    String domainId;
    @Pid
    String fieldname;
    @Pid
    String childTabId;
    @Pid
    String methodName;

    @TreeItemAnnotation(customItems = NavFoldingTabsItems.class)
    public NavFoldingTabsTree(NavFoldingTabsItems tabItems, String sourceClassName, String childTabId, String methodName, String groupName, String domainId, String viewInstId) {
        this.caption = tabItems.getName();
        this.bindClassName = tabItems.getBindClass().getName();
        this.imageClass = tabItems.getImageClass();
        this.groupName = groupName;
        this.id = tabItems.getType() + "_" + sourceClassName + "_" + methodName + "_" + childTabId;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.childTabId = childTabId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;

    }


    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getChildTabId() {
        return childTabId;
    }

    public void setChildTabId(String childTabId) {
        this.childTabId = childTabId;
    }
}
