package com.ds.dsm.view.config.form.field.combo.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.combo.ComboListView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.field.CustomListBean;
import com.ds.esd.custom.field.RadioBoxFieldBean;
import com.ds.esd.custom.field.combo.ComboListBoxFieldBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/combo/")
@MethodChinaName(cname = "List组合", imageClass = "spafont spa-icon-c-comboinput")

public class ComboListService {


    @RequestMapping(method = RequestMethod.POST, value = "ComboList")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-radiobox", caption = "List组合", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ComboListView> getComboList(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<ComboListView> result = new ResultModel<ComboListView>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new ComboListView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "updateComboList")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateComboList(@RequestBody ComboListView comboListView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(comboListView.getSourceClassName(), comboListView.getViewInstId());
            MethodConfig customMethodAPIBean = config.getMethodByName(comboListView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboListView.getFieldname());
            ComboBoxBean widgetBean = formInfo.getComboConfig();
            CustomListBean listFieldBean = null;
            if (widgetBean instanceof ComboListBoxFieldBean) {
                listFieldBean = ((ComboListBoxFieldBean) widgetBean).getListFieldBean();
            } else if (widgetBean instanceof RadioBoxFieldBean) {
                listFieldBean = ((RadioBoxFieldBean) widgetBean).getListFieldBean();
            }

            Map<String, Object> configMap = BeanMap.create(listFieldBean);
            configMap.putAll(BeanMap.create(comboListView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetComboList")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetComboList(@RequestBody ComboListView comboListView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(comboListView.getSourceClassName(), comboListView.getViewInstId());
            MethodConfig customMethodAPIBean = config.getMethodByName(comboListView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboListView.getFieldname());
            formInfo.setWidgetConfig(null);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
