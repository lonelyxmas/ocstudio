package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.action.FieldAction;
import com.ds.dsm.view.config.form.service.FormFieldService;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.custom.grid.enums.GridRowMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;

@PageBar(pageCount = 100)
@RowHead(selMode = SelModeType.none, gridHandlerCaption = "排序|隐藏", rowHandlerWidth = "12em", rowNumbered = false)
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, menuClass = {FieldAction.class}, rowMenu = GridRowMenu.SaveRow)
@BottomBarMenu
@GridAnnotation(customService = {FormFieldService.class}, customMenu = {GridMenu.Add}, editable = true, bottombarMenu = {GridMenu.Reload, GridMenu.SubmitForm})
public class FieldFormGridInfo {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "中文名")
    private String caption;

    @FieldAnnotation(required = true, colWidth = "12em")
    @CustomListAnnotation(filter = "source.isAbsValue()")
    @CustomAnnotation(caption = "控件类型")
    private ComponentType componentType;


    @FieldAnnotation(colWidth = "12em")
    @CustomAnnotation(caption = "字段类型")
    private ComboInputType inputType;
    @CustomAnnotation(caption = "隐藏")
    Boolean colHidden;

    @CustomAnnotation(caption = "只读")
    Boolean readonly;
    @CustomAnnotation(caption = "禁用")
    Boolean disabled;


    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    public FieldFormGridInfo(FieldFormConfig esdField) {

        this.domainId = esdField.getDomainId();
        this.caption = esdField.getAggConfig().getCaption();
        this.fieldname = esdField.getFieldname();
        if (caption == null || caption.equals("")) {
            caption = fieldname;
        }
        this.colHidden = esdField.getColHidden();
        if (esdField.getCustomBean() != null) {
            this.readonly = esdField.getCustomBean().getReadonly();
            this.disabled = esdField.getCustomBean().getDisabled();
            this.disabled = esdField.getCustomBean().getDisabled();
        }

        componentType = esdField.getWidgetConfig().getComponentType();
        this.sourceClassName = esdField.getSourceClassName();
        this.viewClassName = esdField.getViewClassName();
        this.methodName = esdField.getSourceMethodName();
        this.componentType = esdField.getComponentType();
        if (esdField.getWidgetConfig() instanceof ComboBoxBean) {
            this.inputType = esdField.getComboConfig().getInputType();
        }


    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getColHidden() {
        return colHidden;
    }

    public void setColHidden(Boolean colHidden) {
        this.colHidden = colHidden;
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public ComboInputType getInputType() {
        return inputType;
    }

    public void setInputType(ComboInputType inputType) {
        this.inputType = inputType;
    }
}
