package com.ds.dsm.view.config.tree;

import com.ds.dsm.aggregation.config.entity.tree.AggEntityViewService;
import com.ds.dsm.view.config.form.ViewFormNavService;
import com.ds.dsm.view.config.form.field.service.FieldItemsService;
import com.ds.dsm.view.config.form.field.service.FormModuleService;
import com.ds.dsm.view.config.gallery.ViewGalleryConfigService;
import com.ds.dsm.view.config.grid.ViewGridFieldService;
import com.ds.dsm.view.config.grid.nav.GridNavService;
import com.ds.dsm.view.config.nav.buttonviews.ViewNavButtonViewsConfigService;
import com.ds.dsm.view.config.nav.gallery.ViewNavGalleryInfoService;
import com.ds.dsm.view.config.nav.group.ViewGroupService;
import com.ds.dsm.view.config.nav.tab.ViewNavTabsConfigService;
import com.ds.dsm.view.config.nav.tree.NavTreeNavItems;
import com.ds.dsm.view.config.service.ViewConfigNavService;
import com.ds.dsm.view.config.service.ViewLayoutConfigService;
import com.ds.dsm.view.config.service.ViewNavFoldingTreeConfigService;
import com.ds.dsm.view.config.service.ViewNavStacksConfigService;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.bean.nav.buttonviews.NavButtonViewsViewBean;
import com.ds.esd.custom.bean.nav.foldingtree.NavFoldingTreeViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.custom.bean.nav.stacks.NavStacksViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.pop.PopTreeViewBean;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.layout.CustomLayoutViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;


@TabsAnnotation(singleOpen = true, noHandler = true, autoSave = true)
@TreeAnnotation(
        lazyLoad = true,
        customService = ViewConfigNavService.class)
public class ViewConfigTree extends TreeListItem {
    @Pid
    String domainId;
    @Pid
    String viewInstId;
    @Pid
    String sourceClassName;
    @Pid
    String methodName;
    @Pid
    String sourceMethodName;
    @Pid
    ModuleViewType viewType;
    @Pid
    String fieldname;


    @TreeItemAnnotation(bindService = ViewConfigNavService.class, imageClass = "spafont spa-icon-c-webapi", lazyLoad = true, dynDestory = true, caption = "视图配置")
    public ViewConfigTree(String sourceClassName, String domainId, String viewInstId) {
        this.id = sourceClassName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
    }

    @TreeItemAnnotation(bindService = ViewConfigNavService.class, imageClass = "spafont spa-icon-c-webapi", lazyLoad = true, dynDestory = true, caption = "视图配置")
    public ViewConfigTree(ESDClass esdClass, String domainId, String viewInstId) {
        this.id = esdClass.getClassName();
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.caption = esdClass.getCtClass().getSimpleName() + "(" + esdClass.getClassName() + ")";
        this.sourceClassName = esdClass.getClassName();
    }


    @TreeItemAnnotation(bindService = AggEntityViewService.class, imageClass = "spafont spa-icon-c-webapi", iniFold = true, dynDestory = true, lazyLoad = true)
    public ViewConfigTree(MethodConfig methodAPIBean) {
        CustomMenuItem customMenuItem = methodAPIBean.getDefaultMenuItem();
        String methodName = methodAPIBean.getMethodName();
        if (methodAPIBean.getCaption() != null && !methodAPIBean.getCaption().equals("")) {
            this.caption = methodAPIBean.getName() + "(" + methodAPIBean.getCaption() + ")";
        } else {
            if (customMenuItem != null) {
                this.caption = "WebAPI(" + methodAPIBean.getName() + ")-" + methodAPIBean.getModuleViewType().getName();
            } else {
                this.caption = "WebAPI(" + methodAPIBean.getName() + ")";
            }
        }
        this.methodName = methodName;
        this.sourceClassName = methodAPIBean.getSourceClassName();
        this.id = sourceClassName.replace(".", "_") + "_" + methodName;
        this.domainId = methodAPIBean.getDomainId();
        this.imageClass = methodAPIBean.getImageClass();
        this.tips = methodAPIBean.getCaption() + "<br/>";
        this.tips = tips + methodAPIBean.getMetaInfo();

    }


    @TreeItemAnnotation(bindService = ViewGridFieldService.class)
    public ViewConfigTree(FieldGridConfig fieldGridInfo, String sourceClassName, String methodName) {
        this.caption = fieldGridInfo.getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldGridInfo.getFieldname();
        }
        this.methodName = methodName;
        this.sourceClassName = sourceClassName;
        this.imageClass = fieldGridInfo.getGridColItemBean().getInputType().getImageClass();
        this.id = fieldGridInfo.getFieldname() + "_" + sourceClassName + "_" + methodName;
    }

    @TreeItemAnnotation(bindService = FormModuleService.class)
    public ViewConfigTree(FieldModuleConfig fieldFormInfo, String sourceMethodName) {
        this.caption = fieldFormInfo.getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        this.imageClass = fieldFormInfo.getImageClass();
        this.id = fieldFormInfo.getSourceClassName() + "_" + sourceMethodName + "_" + fieldFormInfo.getFieldname();
        this.domainId = fieldFormInfo.getDomainId();

        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = sourceMethodName;
        this.methodName = sourceMethodName;
        this.fieldname = fieldFormInfo.getFieldname();
    }

    @TreeItemAnnotation(bindService = FieldItemsService.class)
    public ViewConfigTree(FieldFormConfig fieldFormInfo, String sourceClassName, String methodName) {
        this.caption = fieldFormInfo.getAggConfig().getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        if (fieldFormInfo.getWidgetConfig() instanceof ComboBoxBean) {
            ComboBoxBean comboBoxBean = (ComboBoxBean) fieldFormInfo.getWidgetConfig();
            this.imageClass = comboBoxBean.getInputType().getImageClass();
        } else {
            this.imageClass = fieldFormInfo.getWidgetConfig().getComponentType().getImageClass();
        }

        this.id = fieldFormInfo.getSourceClassName() + "_" + sourceMethodName + "_" + fieldFormInfo.getFieldname();
        this.domainId = fieldFormInfo.getDomainId();

        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = methodName;
        this.fieldname = fieldFormInfo.getFieldname();
        this.methodName = methodName;

    }


    @TreeItemAnnotation(bindService = GridNavService.class, imageClass = "spafont spa-icon-c-grid", iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(CustomGridViewBean customGridViewBean, String viewInstId, String domainId) {
        this.caption = customGridViewBean.getName();
        if (domainId == null) {
            domainId = customGridViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = customGridViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.methodName = customGridViewBean.getMethodName();
        this.sourceClassName = customGridViewBean.getSourceClassName();
        this.sourceMethodName = methodName;
        this.viewType = customGridViewBean.getModuleViewType();
        this.id = sourceClassName + "_" + methodName;

        String methodName = customGridViewBean.getMethodName();
        if (customGridViewBean.getCaption() != null && !customGridViewBean.getCaption().equals("")) {
            this.caption = customGridViewBean.getCaption() + "(" + customGridViewBean.getName() + ")";
        } else {

            this.caption = methodName + "(" + viewType.getName() + ")-" + customGridViewBean.getModuleViewType().getName();

        }
        this.tips = customGridViewBean.getCaption() + "(" + customGridViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";


    }

    @TreeItemAnnotation(bindService = ViewFormNavService.class, iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(CustomFormViewBean formViewBean, String viewInstId, String domainId) {
        this.caption = formViewBean.getName();
        this.imageClass = formViewBean.getImageClass();

        if (domainId == null) {
            domainId = formViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = formViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = formViewBean.getSourceClassName();
        this.methodName = formViewBean.getMethodName();
        this.viewType = formViewBean.getModuleViewType();
        if (formViewBean.getCaption() != null && !formViewBean.getCaption().equals("")) {
            this.caption = formViewBean.getCaption() + "(" + formViewBean.getName() + ")";
        } else {
            this.caption = methodName + "(" + viewType.getName() + ")-" + formViewBean.getModuleViewType().getName();
        }
        this.tips = formViewBean.getCaption() + "(" + formViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;

    }

    @TreeItemAnnotation(bindService = ViewGalleryConfigService.class, iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(CustomGalleryViewBean viewBean, String viewInstId, String domainId) {
        this.caption = viewBean.getName();
        this.imageClass = viewBean.getImageClass();
        if (domainId == null) {
            domainId = viewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = viewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = viewBean.getSourceClassName();
        this.methodName = viewBean.getMethodName();
        ;
        this.viewType = viewBean.getModuleViewType();
        if (viewBean.getCaption() != null && !viewBean.getCaption().equals("")) {
            this.caption = viewBean.getCaption() + "(" + viewBean.getName() + ")";
        } else {

            this.caption = methodName + "(" + viewType.getName() + ")-" + viewBean.getModuleViewType().getName();

        }
        this.tips = viewBean.getCaption() + "(" + viewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;
    }


    @TreeItemAnnotation(bindService = ViewPopTreeConfigService.class, caption = "弹出树配置", iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(PopTreeViewBean popTreeViewBean, String viewInstId, String domainId) {
        this.imageClass = ModuleViewType.PopTreeConfig.getImageClass();

        if (domainId == null) {
            domainId = popTreeViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = popTreeViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = popTreeViewBean.getSourceClassName();
        this.sourceMethodName = popTreeViewBean.getMethodName();
        this.methodName = popTreeViewBean.getMethodName();
        this.viewType = popTreeViewBean.getModuleViewType();
        if (popTreeViewBean.getCaption() != null && !popTreeViewBean.getCaption().equals("")) {
            this.caption = popTreeViewBean.getCaption() + "(" + popTreeViewBean.getName() + ")";
        } else {

            this.caption = methodName + "(" + viewType.getName() + ")-" + popTreeViewBean.getModuleViewType().getName();

        }
        this.tips = popTreeViewBean.getCaption() + "(" + popTreeViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;

    }


    @TreeItemAnnotation(bindService = ViewTreeConfigService.class, caption = "通用树", iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(CustomTreeViewBean customTreeViewBean, String viewInstId, String domainId) {
        this.imageClass = ModuleViewType.TreeConfig.getImageClass();

        if (domainId == null) {
            domainId = customTreeViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = customTreeViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = customTreeViewBean.getSourceClassName();
        this.sourceMethodName = customTreeViewBean.getMethodName();
        this.methodName = customTreeViewBean.getMethodName();
        this.viewType = customTreeViewBean.getModuleViewType();

        if (customTreeViewBean.getCaption() != null && !customTreeViewBean.getCaption().equals("")) {
            this.caption = customTreeViewBean.getCaption() + "(" + customTreeViewBean.getName() + ")";

        } else {

            this.caption = methodName + "(" + viewType.getName() + ")-" + customTreeViewBean.getModuleViewType().getName();

        }
        this.tips = customTreeViewBean.getCaption() + "(" + customTreeViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;
    }


    @TreeItemAnnotation(bindService = ViewNavTreeConfigService.class, caption = "导航树", iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(NavTreeViewBean navTreeViewBean, String viewInstId, String domainId) {
        this.imageClass = ModuleViewType.NavTreeConfig.getImageClass();
        if (domainId == null) {
            domainId = navTreeViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = navTreeViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = navTreeViewBean.getSourceClassName();
        this.sourceMethodName = navTreeViewBean.getMethodName();
        this.methodName = navTreeViewBean.getMethodName();
        this.viewType = navTreeViewBean.getModuleViewType();
        if (navTreeViewBean.getCaption() != null && !navTreeViewBean.getCaption().equals("")) {
            this.caption = navTreeViewBean.getCaption() + "(" + navTreeViewBean.getName() + ")";
        } else {

            this.caption = methodName + "(" + viewType.getName() + ")-" + navTreeViewBean.getModuleViewType().getName();

        }
        this.tips = navTreeViewBean.getCaption() + "(" + navTreeViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;
    }

    @TreeItemAnnotation(customItems = NavTreeNavItems.class, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(NavTreeNavItems treeNavItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = treeNavItems.getName();
        this.bindClassName = treeNavItems.getBindClass().getName();
        this.imageClass = treeNavItems.getImageClass();
        this.id = treeNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(bindService = ViewNavTabsConfigService.class, caption = "Tabs", iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(NavTabsViewBean navTabsViewBean, String viewInstId, String domainId) {
        this.setImageClass(ModuleViewType.NavTabsConfig.getImageClass());

        if (domainId == null) {
            domainId = navTabsViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = navTabsViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = navTabsViewBean.getSourceClassName();
        this.sourceMethodName = navTabsViewBean.getMethodName();
        this.methodName = navTabsViewBean.getMethodName();
        this.viewType = navTabsViewBean.getModuleViewType();
        if (navTabsViewBean.getCaption() != null && !navTabsViewBean.getCaption().equals("")) {
            this.caption = navTabsViewBean.getCaption() + "(" + navTabsViewBean.getName() + ")";
        } else {

            this.caption = methodName + "(" + viewType.getName() + ")-" + navTabsViewBean.getModuleViewType().getName();

        }
        this.tips = navTabsViewBean.getCaption() + "(" + navTabsViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;
        ;
    }

    @TreeItemAnnotation(bindService = ViewNavFoldingTreeConfigService.class, caption = "NavFolding", iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(NavFoldingTreeViewBean foldingTabsViewBean, String viewInstId, String domainId) {
        this.imageClass = ModuleViewType.NavFoldingTreeConfig.getImageClass();

        if (domainId == null) {
            domainId = foldingTabsViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = foldingTabsViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = foldingTabsViewBean.getSourceClassName();
        this.sourceMethodName = foldingTabsViewBean.getMethodName();
        this.methodName = foldingTabsViewBean.getMethodName();
        this.viewType = foldingTabsViewBean.getModuleViewType();

        if (foldingTabsViewBean.getCaption() != null && !foldingTabsViewBean.getCaption().equals("")) {
            this.caption = foldingTabsViewBean.getCaption() + "(" + foldingTabsViewBean.getName() + ")";
        } else {

            this.caption = methodName + "(" + viewType.getName() + ")-" + foldingTabsViewBean.getModuleViewType().getName();

        }
        this.tips = foldingTabsViewBean.getCaption() + "(" + foldingTabsViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;
    }

    @TreeItemAnnotation(bindService = ViewNavGalleryInfoService.class, caption = "NavGallery", iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(NavGalleryViewBean navGalleryTabsViewBean, String viewInstId, String domainId) {
        this.imageClass = ModuleViewType.NavGalleryConfig.getImageClass();
        if (domainId == null) {
            domainId = navGalleryTabsViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = navGalleryTabsViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = navGalleryTabsViewBean.getSourceClassName();
        this.sourceMethodName = navGalleryTabsViewBean.getMethodName();
        this.methodName = navGalleryTabsViewBean.getMethodName();
        this.viewType = navGalleryTabsViewBean.getModuleViewType();
        if (navGalleryTabsViewBean.getCaption() != null && !navGalleryTabsViewBean.getCaption().equals("")) {
            this.caption = navGalleryTabsViewBean.getCaption() + "(" + navGalleryTabsViewBean.getName() + ")";
        } else {
            this.caption = methodName + "(" + viewType.getName() + ")-" + navGalleryTabsViewBean.getModuleViewType().getName();
        }
        this.tips = navGalleryTabsViewBean.getCaption() + "(" + navGalleryTabsViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;
    }

    @TreeItemAnnotation(bindService = ViewNavButtonViewsConfigService.class, caption = "NavButtonViews", iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(NavButtonViewsViewBean navButtonViewsViewBean, String viewInstId, String domainId) {
        this.imageClass = ModuleViewType.NavButtonViewsConfig.getImageClass();

        if (domainId == null) {
            domainId = navButtonViewsViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = navButtonViewsViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = navButtonViewsViewBean.getSourceClassName();
        this.sourceMethodName = navButtonViewsViewBean.getMethodName();
        this.methodName = navButtonViewsViewBean.getMethodName();
        this.viewType = navButtonViewsViewBean.getModuleViewType();
        if (navButtonViewsViewBean.getCaption() != null && !navButtonViewsViewBean.getCaption().equals("")) {
            this.caption = navButtonViewsViewBean.getCaption() + "(" + navButtonViewsViewBean.getName() + ")";
        } else {
            this.caption = methodName + "(" + viewType.getName() + ")-" + navButtonViewsViewBean.getModuleViewType().getName();
        }
        this.tips = navButtonViewsViewBean.getCaption() + "(" + navButtonViewsViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;
    }

    @TreeItemAnnotation(bindService = ViewNavStacksConfigService.class, caption = "NavStacksConfig", lazyLoad = true, dynDestory = true)
    public ViewConfigTree(NavStacksViewBean navStacksViewBean, String viewInstId, String domainId) {
        this.imageClass = ModuleViewType.NavStacksConfig.getImageClass();

        if (domainId == null) {
            domainId = navStacksViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = navStacksViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = navStacksViewBean.getSourceClassName();
        this.sourceMethodName = navStacksViewBean.getMethodName();
        this.methodName = navStacksViewBean.getMethodName();
        this.viewType = navStacksViewBean.getModuleViewType();
        if (navStacksViewBean.getCaption() != null && !navStacksViewBean.getCaption().equals("")) {
            this.caption = navStacksViewBean.getCaption() + "(" + navStacksViewBean.getName() + ")";
        } else {

            this.caption = methodName + "(" + viewType.getName() + ")-" + navStacksViewBean.getModuleViewType().getName();

        }
        this.tips = navStacksViewBean.getCaption() + "(" + navStacksViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;
    }

    @TreeItemAnnotation(bindService = ViewGroupService.class, caption = "NavGroupConfig", iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(NavGroupViewBean navGroupViewBean, String viewInstId, String domainId) {
        this.imageClass = ModuleViewType.NavGroupConfig.getImageClass();

        if (domainId == null) {
            domainId = navGroupViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = navGroupViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = navGroupViewBean.getSourceClassName();
        this.sourceMethodName = navGroupViewBean.getMethodName();
        this.methodName = navGroupViewBean.getMethodName();
        this.viewType = navGroupViewBean.getModuleViewType();
        if (navGroupViewBean.getCaption() != null && !navGroupViewBean.getCaption().equals("")) {
            this.caption = navGroupViewBean.getCaption() + "(" + navGroupViewBean.getName() + ")";
        } else {

            this.caption = methodName + "(" + viewType.getName() + ")-" + navGroupViewBean.getModuleViewType().getName();

        }
        this.tips = navGroupViewBean.getCaption() + "(" + navGroupViewBean.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + viewType.getName() + ")";
        this.id = sourceClassName + "_" + methodName;
    }

    @TreeItemAnnotation(bindService = ViewLayoutConfigService.class, caption = "LayoutConfig", iniFold = true, lazyLoad = true, dynDestory = true)
    public ViewConfigTree(CustomLayoutViewBean layoutViewBean, String viewInstId, String domainId) {
        this.imageClass = ModuleViewType.LayoutConfig.getImageClass();
        if (domainId == null) {
            domainId = layoutViewBean.getDomainId();
        }
        this.domainId = domainId;
        if (viewInstId == null) {
            viewInstId = layoutViewBean.getViewInstId();
        }
        this.viewInstId = viewInstId;
        this.sourceClassName = layoutViewBean.getSourceClassName();
        this.sourceMethodName = layoutViewBean.getMethodName();
        this.methodName = layoutViewBean.getMethodName();
        this.id = sourceClassName + "_" + methodName;

    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public ModuleViewType getViewType() {
        return viewType;
    }

    public void setViewType(ModuleViewType viewType) {
        this.viewType = viewType;
    }
}


