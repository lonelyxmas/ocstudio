package com.ds.dsm.view.config.custom.panel.div;

import com.alibaba.fastjson.JSONObject;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.RichEditorAnnotation;
import com.ds.esd.custom.bean.CustomDivBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.nav.PanelItemBean;
import com.ds.esd.tool.ui.enums.OverflowType;
import net.sf.cglib.beans.BeanMap;

@FormAnnotation(col = 2)
public class DivView {

    @CustomAnnotation(caption = "Iframe自动装载")
    public String iframeAutoLoad;
    @CustomAnnotation(caption = "Ajax自动装载")
    public String ajaxAutoLoad;
    @CustomAnnotation(caption = "宽度")
    public String width;
    @CustomAnnotation(caption = "高宽")
    public String height;

    @RichEditorAnnotation
    @CustomAnnotation(caption = "Html")
    public String html;

    @CustomAnnotation(caption = "溢出(OverflowType)")
    public OverflowType overflow;



    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String entityClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    @CustomAnnotation(hidden = true, uid = true)
    String sourceMethodName;

    public DivView() {

    }

    public DivView(PanelItemBean config) {

        CustomDivBean divBean = config.getPanelBean().getDivBean();
        if (divBean == null) {
            divBean = new CustomDivBean();
        }
        String json = JSONObject.toJSONString(divBean);
        BeanMap.create(this).putAll(JSONObject.parseObject(json));
        this.viewInstId = config.getViewInstId();
        this.domainId = config.getDomainId();
        this.entityClassName = config.getEntityClassName();
        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getSourceMethodName();
        this.sourceMethodName =config.getSourceMethodName();


    }

    public String getIframeAutoLoad() {
        return iframeAutoLoad;
    }

    public void setIframeAutoLoad(String iframeAutoLoad) {
        this.iframeAutoLoad = iframeAutoLoad;
    }

    public String getAjaxAutoLoad() {
        return ajaxAutoLoad;
    }

    public void setAjaxAutoLoad(String ajaxAutoLoad) {
        this.ajaxAutoLoad = ajaxAutoLoad;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public OverflowType getOverflow() {
        return overflow;
    }

    public void setOverflow(OverflowType overflow) {
        this.overflow = overflow;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

}
