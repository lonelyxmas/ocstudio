package com.ds.dsm.view.config.form.field.custom;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.DisabledBean;
import com.ds.esd.custom.field.InputFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu
@FormAnnotation(customService = DisabledService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class DisabledView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    public String fieldname;

    @CustomAnnotation(caption = "禁用")
    Boolean disabled;
    @CustomAnnotation(caption = "禁用点击")
    Boolean disableClickEffect;
    @CustomAnnotation(caption = "禁用悬浮")
    Boolean disableHoverEffect;
    @CustomAnnotation(caption = "禁用提示")
    Boolean disableTips;
    @CustomAnnotation(caption = "禁用提示")
    Boolean defaultFocus;

    public DisabledView() {

    }


    public DisabledView(FieldFormConfig<InputFieldBean,?> config) {
        DisabledBean disabledBean = config.getDisabledBean();
        if (disabledBean == null) {
            disabledBean = new DisabledBean();
        }
        BeanMap.create(this).putAll(BeanMap.create(disabledBean));
        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();


    }

    public Boolean getDefaultFocus() {
        return defaultFocus;
    }

    public void setDefaultFocus(Boolean defaultFocus) {
        this.defaultFocus = defaultFocus;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getDisableClickEffect() {
        return disableClickEffect;
    }

    public void setDisableClickEffect(Boolean disableClickEffect) {
        this.disableClickEffect = disableClickEffect;
    }

    public Boolean getDisableHoverEffect() {
        return disableHoverEffect;
    }

    public void setDisableHoverEffect(Boolean disableHoverEffect) {
        this.disableHoverEffect = disableHoverEffect;
    }

    public Boolean getDisableTips() {
        return disableTips;
    }

    public void setDisableTips(Boolean disableTips) {
        this.disableTips = disableTips;
    }
}
