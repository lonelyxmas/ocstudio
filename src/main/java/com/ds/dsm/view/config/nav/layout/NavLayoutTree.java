package com.ds.dsm.view.config.nav.layout;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.tool.ui.component.list.TreeListItem;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class NavLayoutTree extends TreeListItem {

    String domainId;

    String viewInstId;

    String methodName;

    String sourceClassName;

    @TreeItemAnnotation(bindService = LayoutItemService.class)
    public NavLayoutTree(String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = "Layout配置";
        this.setImageClass(ModuleViewType.NavTabsConfig.getImageClass());
        this.sourceClassName = sourceClassName;
        this.setId(ModuleViewType.NavTabsConfig.name() + methodName);
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.methodName = methodName;
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
