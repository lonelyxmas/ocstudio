package com.ds.dsm.view.config.tree;

import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.TreeDataBaseBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;

public class TreeView {

    String viewInstId;

    String domainId;

    String sourceClassName;


    String methodName;

    String caption;

    SelModeType selMode;

    TagCmdsAlign tagCmdsAlign;

    Boolean group;

    Boolean togglePlaceholder;

    Boolean singleOpen;

    Boolean iniFold;

    String optBtn;

    Boolean animCollapse;

    Boolean dynDestory;

    String rootId;


    String fieldCaption;

    String fieldId;

    String valueSeparator;

    Boolean heplBar;

    Boolean formField;


    public TreeView() {

    }

    public TreeView(MethodConfig<CustomTreeViewBean, TreeDataBaseBean> methodConfig) {
        CustomTreeViewBean viewConfig = methodConfig.getView();
        TreeDataBaseBean dataConfig = methodConfig.getDataBean();

        this.viewInstId = viewConfig.getViewInstId();
        this.domainId = viewConfig.getDomainId();
        this.sourceClassName = viewConfig.getSourceClassName();
        this.animCollapse = viewConfig.getAnimCollapse();
        this.iniFold = viewConfig.getIniFold();
        this.dynDestory = viewConfig.getDynDestory();

        this.togglePlaceholder = viewConfig.getTogglePlaceholder();
        //  this.tagCmdsAlign = viewConfig.getTagCmdsAlign();
        this.group = viewConfig.getGroup();
        this.optBtn = viewConfig.getOptBtn();
        this.singleOpen = viewConfig.getSingleOpen();

        this.selMode = viewConfig.getSelMode();
        this.caption = viewConfig.getCaption();

        this.formField = viewConfig.getFormField();
        this.heplBar = viewConfig.getHeplBar();
        this.valueSeparator = viewConfig.getValueSeparator();

        this.fieldCaption = dataConfig.getFieldCaption();
        this.fieldId = dataConfig.getFieldId();
        this.rootId = dataConfig.getRootId();
        this.methodName = dataConfig.getMethodName();

    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getRootId() {
        return rootId;
    }

    public void setRootId(String rootId) {
        this.rootId = rootId;
    }

    public String getFieldCaption() {
        return fieldCaption;
    }

    public void setFieldCaption(String fieldCaption) {
        this.fieldCaption = fieldCaption;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getValueSeparator() {
        return valueSeparator;
    }

    public void setValueSeparator(String valueSeparator) {
        this.valueSeparator = valueSeparator;
    }

    public Boolean getHeplBar() {
        return heplBar;
    }

    public void setHeplBar(Boolean heplBar) {
        this.heplBar = heplBar;
    }

    public Boolean getFormField() {
        return formField;
    }

    public void setFormField(Boolean formField) {
        this.formField = formField;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public Boolean getIniFold() {
        return iniFold;
    }

    public void setIniFold(Boolean iniFold) {
        this.iniFold = iniFold;
    }

    public Boolean getAnimCollapse() {
        return animCollapse;
    }

    public void setAnimCollapse(Boolean animCollapse) {
        this.animCollapse = animCollapse;
    }

    public Boolean getDynDestory() {
        return dynDestory;
    }

    public void setDynDestory(Boolean dynDestory) {
        this.dynDestory = dynDestory;
    }

    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }

    public Boolean getTogglePlaceholder() {
        return togglePlaceholder;
    }

    public void setTogglePlaceholder(Boolean togglePlaceholder) {
        this.togglePlaceholder = togglePlaceholder;
    }

    public TagCmdsAlign getTagCmdsAlign() {
        return tagCmdsAlign;
    }

    public void setTagCmdsAlign(TagCmdsAlign tagCmdsAlign) {
        this.tagCmdsAlign = tagCmdsAlign;
    }

    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }

    public String getOptBtn() {
        return optBtn;
    }

    public void setOptBtn(String optBtn) {
        this.optBtn = optBtn;
    }


    public Boolean getSingleOpen() {
        return singleOpen;
    }

    public void setSingleOpen(Boolean singleOpen) {
        this.singleOpen = singleOpen;
    }


    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
