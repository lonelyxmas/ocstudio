package com.ds.dsm.view.config.nav;

import com.ds.enums.IconEnumstype;

public enum NavStacksItems implements IconEnumstype {
    StacksInfoGroup("基础信息", "spafont spa-icon-config"),
    FieldFormList("参数列表", "spafont spa-icon-c-comboinput"),
    StackItemList("子项列表","spafont spa-icon-conf");
    private final String name;
    private final String className;
    private final String imageClass;
    private String packageName = "dsm.view.config.nav.stacks";


    NavStacksItems(String name, String imageClass) {
        this.name = name;
        this.className = packageName + "." + name();
        this.imageClass = imageClass;
    }


    public String getClassName() {
        return className;
    }

    public String getPackageName() {
        return packageName;
    }


    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
