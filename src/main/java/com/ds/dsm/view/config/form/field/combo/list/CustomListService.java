package com.ds.dsm.view.config.form.field.combo.list;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.field.CustomListBean;
import com.ds.esd.custom.field.RadioBoxFieldBean;
import com.ds.esd.custom.field.combo.ComboListBoxFieldBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.AnnotationUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/list/")
@MethodChinaName(cname = "List组合", imageClass = "spafont spa-icon-c-comboinput")

public class CustomListService {


    @RequestMapping(method = RequestMethod.POST, value = "CustomList")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-radiobox", caption = "List组合", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<CustomListView> getComboList(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<CustomListView> result = new ResultModel<CustomListView>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new CustomListView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "updateComboList")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateComboList(@RequestBody CustomListView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean = config.getMethodByName(comboView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboView.getFieldname());
            ComboBoxBean widgetBean = formInfo.getComboConfig();
            CustomListBean listFieldBean = null;
            if (widgetBean instanceof ComboListBoxFieldBean) {
                listFieldBean = ((ComboListBoxFieldBean) widgetBean).getListFieldBean();
            } else if (widgetBean instanceof RadioBoxFieldBean) {
                listFieldBean = ((RadioBoxFieldBean) widgetBean).getListFieldBean();
            }

            Map<String, Object> configMap = BeanMap.create(listFieldBean);
            configMap.putAll(BeanMap.create(comboView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetComboList")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetComboList(@RequestBody CustomListView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean = config.getMethodByName(comboView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboView.getFieldname());
            ComboBoxBean widgetBean = formInfo.getComboConfig();
            CustomListBean listFieldBean = new CustomListBean(customMethodAPIBean.getCustomMethodInfo(), AnnotationUtil.getAllAnnotations(customMethodAPIBean.getMethod()));
            if (widgetBean instanceof ComboListBoxFieldBean) {
                ((ComboListBoxFieldBean) widgetBean).setListFieldBean(listFieldBean);
            } else if (widgetBean instanceof RadioBoxFieldBean) {
                ((RadioBoxFieldBean) widgetBean).setListFieldBean(listFieldBean);
            }


            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadChildModule")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> loadChildModule(String domainId, String sourceClassName, String fieldname, String methodName) {
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<List<ViewConfigTree>>();
        ApiClassConfig classConfig = null;
        try {
            classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(methodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            ComboListBoxFieldBean boxFieldBean = (ComboListBoxFieldBean) formInfo.getComboConfig();
            CustomListBean fieldBean = boxFieldBean.getListFieldBean();
            MethodConfig fieldMethodConfig = null;
            if (fieldBean.getBindClass() != null && !fieldBean.getBindClass().equals(Void.class)) {
                ApiClassConfig bindConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(fieldBean.getBindClass().getName(), domainId);
                fieldMethodConfig = bindConfig.getMethodByItem(CustomMenuItem.index);
            } else {
                ApiClassConfig bindConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(formInfo.getViewClassName(), domainId);
                fieldMethodConfig = bindConfig.getMethodByName(formInfo.getMethodName());
            }

            List<Object> obj = new ArrayList<>();
            if (fieldMethodConfig != null) {
                obj.add(fieldMethodConfig);
                if (fieldMethodConfig.getView() != null) {
                    obj.add(fieldMethodConfig.getView());
                }
            }

            resultModel = TreePageUtil.getTreeList(obj, ViewConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return resultModel;
    }

}
