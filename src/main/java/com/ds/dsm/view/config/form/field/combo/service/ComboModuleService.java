package com.ds.dsm.view.config.form.field.combo.service;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.combo.ComboModuleView;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.*;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.field.combo.ComboModuleFieldBean;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/combo/module/")
@MethodChinaName(cname = "模块信息", imageClass = "spafont spa-icon-module")

public class ComboModuleService {


    @RequestMapping(method = RequestMethod.POST, value = "AddFieldModuleInfo")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "模块信息", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.add})
    @DialogAnnotation(width = "600", height = "480")
    @ResponseBody
    public ResultModel<ComboModuleView> addFieldModuleInfo(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<ComboModuleView> result = new ResultModel<ComboModuleView>();
        ComboModuleView moduleView = new ComboModuleView(null, ComboInputType.module);
        moduleView.setSourceClassName(sourceClassName);
        moduleView.setMethodName(methodName);
        moduleView.setDomainId(domainId);
        result.setData(moduleView);

        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldModuleInfo")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "基础信息", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.editor})
    @DialogAnnotation(width = "600", height = "480")
    @ResponseBody
    public ResultModel<ComboModuleView> getFieldModuleInfo(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<ComboModuleView> result = new ResultModel<ComboModuleView>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(methodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new ComboModuleView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ComboModule")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "模块信息", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ComboModuleView> getComboModule(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<ComboModuleView> result = new ResultModel<ComboModuleView>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(methodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new ComboModuleView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadChildModule")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> loadChildModule(String domainId, String sourceClassName, String fieldname, String methodName) {
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<List<ViewConfigTree>>();
        ApiClassConfig classConfig = null;
        try {
            classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(methodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);
            MethodConfig fieldMethodConfig = null;
            ComboModuleFieldBean fieldBean = (ComboModuleFieldBean) formInfo.getComboConfig();
            if (fieldBean.getSrc() != null && !fieldBean.getSrc().equals("")) {
                fieldMethodConfig = classConfig.getMethodByName(fieldBean.getSrc());
            } else if (fieldBean.getBindClass() != null && !fieldBean.getBindClass().equals(Void.class)) {
                ApiClassConfig bindConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(fieldBean.getBindClass().getName(), domainId);
                fieldMethodConfig = bindConfig.getMethodByItem(CustomMenuItem.index);
            } else {
                ApiClassConfig bindConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(formInfo.getViewClassName(), domainId);
                fieldMethodConfig = bindConfig.getMethodByName(formInfo.getMethodName());
            }
            List<Object> obj = new ArrayList<>();
            obj.add(fieldMethodConfig);
            if (fieldMethodConfig.getView() != null) {
                obj.add(fieldMethodConfig.getView());
            }
            resultModel = TreePageUtil.getTreeList(obj, ViewConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "updateComboModule")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.save}, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<ComboModuleView> updateComboModule(@RequestBody ComboModuleView iModuleView) {
        ResultModel<ComboModuleView> result = new ResultModel<ComboModuleView>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(iModuleView.getSourceClassName(), iModuleView.getDomainId());
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(iModuleView.getMethodName());
            CustomFormViewBean viewBean = (CustomFormViewBean) customMethodAPIBean.getView();

            FieldFormConfig<ComboInputFieldBean, ComboModuleFieldBean> fieldFormConfig = viewBean.createField(iModuleView.getFieldname(), iModuleView.getComponentType(), iModuleView.getInputType(), ResultModel.class);
            if (iModuleView.getInputType().equals(ComboInputType.module)) {
                FieldBean fieldBean = new FieldBean();
                fieldBean.setRowHeight("200");
                fieldBean.setColSpan(-1);
                fieldFormConfig.setFieldBean(fieldBean);

            }
            ComboModuleFieldBean fieldBean = fieldFormConfig.getComboConfig();
            fieldBean.setAppend(iModuleView.getAppend());
            fieldBean.setSrc(iModuleView.getSrc());
            if (iModuleView.getBindClass() != null) {
                fieldBean.setBindClass(iModuleView.getBindClass());
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "delFieldModuleInfo")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.delete}, callback = CustomCallBack.Reload)
    @ResponseBody
    public ResultModel<ComboModuleView> delFieldModuleInfo(String sourceClassName, String methodName, String fieldname, String domainId) {
        ResultModel<ComboModuleView> result = new ResultModel<ComboModuleView>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(methodName);
            CustomView viewBean = customMethodAPIBean.getView();

            String[] fieldnameArr = StringUtility.split(fieldname, ";");
            for (String id : fieldnameArr) {
                ((CustomViewBean) viewBean).getFieldConfigMap().remove(id);
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "reSetComboModule")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetComboModule(@RequestBody ComboModuleView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(comboView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboView.getFieldname());
            formInfo.setWidgetConfig(null);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
