package com.ds.dsm.view.config.grid.field;

import com.ds.dsm.view.config.action.FieldAction;
import com.ds.dsm.view.config.grid.cell.GridCellService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.grid.GridColItemBean;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;

@PageBar(pageCount = 100)
@RowHead(selMode = SelModeType.none, gridHandlerCaption = "排序|隐藏", rowHandlerWidth = "8em", rowNumbered = false)
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, menuClass = {FieldAction.class})
@BottomBarMenu
@GridAnnotation(customService = {GridCellService.class}, customMenu = GridMenu.Add, editable = true, bottombarMenu = {GridMenu.Reload, GridMenu.SubmitForm})
public class FieldGridInfo {

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "列标题")
    private String title;

    @CustomAnnotation(caption = "隐藏")
    Boolean colHidden;

    @CustomAnnotation(caption = "自动延伸")
    Boolean flexSize;

    @CustomAnnotation(caption = "可变列宽")
    Boolean colResizer;

    @CustomAnnotation(caption = "列可编辑")
    Boolean editable;

    @CustomAnnotation(caption = "宽度")
    String width;

    @CustomAnnotation(caption = "字段类型")
    public ComboInputType inputType;

    @CustomAnnotation(hidden = true, pid = true)
    public String entityClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    public FieldGridInfo() {

    }

    public FieldGridInfo(FieldGridConfig esdField) {
        this.viewInstId = esdField.getDomainId();
        this.domainId = esdField.getDomainId();


        this.title = esdField.getCaption();
        this.colHidden = esdField.getColHidden();

        GridColItemBean gridItemBean = esdField.getGridColItemBean();
        if (gridItemBean != null) {

            this.flexSize = gridItemBean.getFlexSize();
            this.colResizer = gridItemBean.getColResizer();
            this.inputType = gridItemBean.getInputType();
            if (gridItemBean.getTitle() != null && !gridItemBean.getTitle().equals("")) {
                this.title = gridItemBean.getTitle();
            }
            this.width = gridItemBean.getWidth();
            this.inputType = gridItemBean.getInputType();
        }


        if (title == null || title.equals("")) {
            title = fieldname;
        }

        this.fieldname = esdField.getFieldname();
        this.entityClassName = esdField.getEntityClassName();
        this.sourceClassName = esdField.getSourceClassName();


    }

    public Boolean getColHidden() {
        return colHidden;
    }

    public void setColHidden(Boolean colHidden) {
        this.colHidden = colHidden;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public Boolean getFlexSize() {
        return flexSize;
    }

    public void setFlexSize(Boolean flexSize) {
        this.flexSize = flexSize;
    }

    public Boolean getColResizer() {
        return colResizer;
    }

    public void setColResizer(Boolean colResizer) {
        this.colResizer = colResizer;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


    public ComboInputType getInputType() {
        return inputType;
    }

    public void setInputType(ComboInputType inputType) {
        this.inputType = inputType;
    }
}
