package com.ds.dsm.view.config.custom.panel.btn;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.nav.PanelItemBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/custom/btn/")
public class BtnService {


    @RequestMapping(method = RequestMethod.POST, value = "BtnConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "按钮配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<BtnView> getBtnConfig(String sourceClassName, String sourceMethodName, String groupId, String domainId, String viewInstId) {
        ResultModel<BtnView> resultModel = new ResultModel();
        try {
            ViewEntityConfig config = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getSourceConfig().getMethodByName(sourceMethodName);
            NavBaseViewBean baseViewBean = (NavBaseViewBean) customMethodAPIBean.getView();
            PanelItemBean panelItemBean = baseViewBean.getItemBean(groupId);
            resultModel.setData(new BtnView(panelItemBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
