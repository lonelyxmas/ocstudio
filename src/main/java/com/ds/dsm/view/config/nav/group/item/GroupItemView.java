package com.ds.dsm.view.config.nav.group.item;

import com.alibaba.fastjson.JSONObject;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.CodeEditorAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.bean.nav.group.GroupItemBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.enums.CustomImageType;
import net.sf.cglib.beans.BeanMap;

@FormAnnotation(col = 2)
public class GroupItemView {

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;
    @CustomAnnotation(caption = "标题")
    String caption;
    @CustomAnnotation(caption = "默认折叠")
    Boolean iniFold;
    @CustomAnnotation(caption = "异步渲染")
    Boolean lazyAppend;
    @CustomAnnotation(caption = "自动刷新")
    Boolean autoReload;
    @CustomAnnotation(caption = "动态销毁")
    Boolean dynDestory;
    @CustomAnnotation(caption = "延迟加载")
    Boolean lazyLoad;
    @CustomAnnotation(caption = "自定义组名称")
    String groupName;
    @FieldAnnotation(colSpan = -1)
    @CodeEditorAnnotation(codeType="css")
    @CustomAnnotation(caption = "CSS样式")
    String className;


    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "绑定服务")
    Class bindService;

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, uid = true)
    String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String entityClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String sourceMethodName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    public GroupItemView() {

    }

    public GroupItemView(GroupItemBean config) {
        GroupItemView view = JSONObject.parseObject(JSONObject.toJSONString(config), this.getClass());
        BeanMap.create(this).putAll(BeanMap.create(view));

        this.viewInstId = config.getViewInstId();
        this.domainId = config.getDomainId();
        this.methodName = config.getSourceMethodName();
        this.bindService = config.getBindService();
        this.sourceClassName = config.getSourceClassName();
        this.entityClassName = config.getEntityClassName();
        this.sourceMethodName = config.getSourceMethodName();

        if (config.getBindService() != null && !config.getBindService().equals(Void.class)) {
            this.bindService = config.getBindService();
        }
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Boolean getIniFold() {
        return iniFold;
    }

    public void setIniFold(Boolean iniFold) {
        this.iniFold = iniFold;
    }

    public Boolean getLazyAppend() {
        return lazyAppend;
    }

    public void setLazyAppend(Boolean lazyAppend) {
        this.lazyAppend = lazyAppend;
    }

    public Boolean getAutoReload() {
        return autoReload;
    }

    public void setAutoReload(Boolean autoReload) {
        this.autoReload = autoReload;
    }

    public Boolean getDynDestory() {
        return dynDestory;
    }

    public void setDynDestory(Boolean dynDestory) {
        this.dynDestory = dynDestory;
    }

    public Boolean getLazyLoad() {
        return lazyLoad;
    }

    public void setLazyLoad(Boolean lazyLoad) {
        this.lazyLoad = lazyLoad;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Class getBindService() {
        return bindService;
    }

    public void setBindService(Class bindService) {
        this.bindService = bindService;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
