package com.ds.dsm.view.config.nav.tab.child;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tabs/child/")
public class TabModuleService {


    @RequestMapping(method = RequestMethod.POST, value = "ChildModuleTab")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> getChildModuleTree(String sourceClassName, String serviceClassName, String methodName, String domainId, String id) {
        List<Object> objs = new ArrayList<>();
        try {
            if (serviceClassName != null) {
                ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(serviceClassName, domainId);
                MethodConfig methodConfig = apiClassConfig.getMethodByItem(CustomMenuItem.tabEditor);
                if (methodConfig != null && methodConfig.getView() != null) {
                    objs.add(methodConfig);
                    objs.add(methodConfig.getView());
                }
            } else {
                ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
                MethodConfig methodConfig = apiClassConfig.getMethodByName(methodName);
                if (methodConfig != null) {
                    objs.add(methodConfig);
                }

            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

        TreeListResultModel resultModel = TreePageUtil.getTreeList(objs, ViewConfigTree.class);
        return resultModel;
    }


}
