package com.ds.dsm.view.config.tree.rowcmd.custom;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.RowCmdMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.TreeRowCmdBean;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.custom.tree.enums.TreeMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.AnnotationUtil;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping(path = "/dsm/view/config/tree/menu/rowmenu/")
public class CustomTreeRowMenuService {

    @RequestMapping(method = RequestMethod.POST, value = "TreeRowMenu")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "TreeRowMenu",
            imageClass = "spafont spa-icon-c-toolbar")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<CustomTreeRowMenuGridView>> getTreeRowMenu(String sourceClassName, String sourceMethodName, String domainId, String childViewId) {
        ListResultModel<List<CustomTreeRowMenuGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(sourceMethodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }
            if (childViewId != null) {
                ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                resultModel = PageUtil.getDefaultPageList(Arrays.asList(childTreeViewBean.getContextMenu()), CustomTreeRowMenuGridView.class);
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "表单按钮")
    @RequestMapping("CustomRowMenuTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<CustomTreeRowMenuPopTree>> getCustomRowMenuTree(String sourceClassName, String methodName, String domainId, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<CustomTreeRowMenuPopTree>> model = new TreeListResultModel<>();
        List<CustomTreeRowMenuPopTree> popTrees = new ArrayList<>();
        CustomTreeRowMenuPopTree menuPopTree = new CustomTreeRowMenuPopTree("rowMenu", "常用按钮", "rowMenu");
        List<CustomTreeRowMenuPopTree> menuTrees = TreePageUtil.fillObjs(Arrays.asList(TreeMenu.values()), CustomTreeRowMenuPopTree.class);
        menuPopTree.setSub(menuTrees);
        popTrees.add(menuPopTree);
        model.setData(popTrees);

        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addCustomRowMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addCustomRowMenu(String sourceClassName, String methodName, String CustomRowMenuTree, String domainId, String childViewId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            try {
                if (childViewId != null) {

                    ChildTreeViewBean childTreeViewBean = null;
                    if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                        NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                        CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                        childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                    } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                        CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                        childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                    }

                    if (CustomRowMenuTree != null && !CustomRowMenuTree.equals("")) {

                        TreeRowCmdBean barMenuBean = childTreeViewBean.getRowCmdBean();
                        if (barMenuBean == null) {
                            barMenuBean = AnnotationUtil.fillDefaultValue(RowCmdMenu.class, new TreeRowCmdBean());
                        }
                        String[] menuIds = StringUtility.split(CustomRowMenuTree, ";");
                        for (String menuId : menuIds) {
                            if (menuId != null && !menuIds.equals("")) {
                                barMenuBean.getRowMenu().add(TreeMenu.valueOf(menuId));
                            }
                        }
                        childTreeViewBean.setRowCmdBean(barMenuBean);
                    }


                    DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(customESDClassAPIBean);

                }

            } catch (JDSException e) {
                model = new ErrorResultModel();
                ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
            }
            return model;
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delCustomContextMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delCustomContextMenu(String sourceClassName, String methodName, String type, String bar, String domainId, String childViewId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);

            ChildTreeViewBean childTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
            }

            TreeRowCmdBean barMenuBean = childTreeViewBean.getRowCmdBean();

            if (barMenuBean != null) {
                String[] menuIds = StringUtility.split(type, ";");
                for (String menuId : menuIds) {
                    barMenuBean.getRowMenu().remove(TreeMenu.valueOf(menuId));
                }
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
