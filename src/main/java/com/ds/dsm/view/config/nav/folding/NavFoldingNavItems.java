package com.ds.dsm.view.config.nav.folding;

import com.ds.dsm.view.config.nav.layout.LayoutService;
import com.ds.dsm.view.config.nav.tab.ViewNavTabsConfigService;
import com.ds.dsm.view.config.tree.ViewChildTreesConfigService;
import com.ds.dsm.view.config.tree.ViewTreeConfigService;

import com.ds.esd.custom.tree.enums.TreeItem;

public enum NavFoldingNavItems implements TreeItem {
    TreeNavConfig("模块配置", "spafont spa-icon-values", ViewTreeConfigService.class, false, false, false),
    TabsConfig("Tab页配置", "spafont spa-icon-c-dialog", ViewNavTabsConfigService.class, false, false, false),
    NavTreeItemList("子项列表", "spafont spa-icon-conf", ViewChildTreesConfigService.class, true, true, true),
    Layout("布局配置",  "spafont spa-icon-c-layout", LayoutService.class, false, false, false);
    private final String imageClass;
    private final String name;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    NavFoldingNavItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
