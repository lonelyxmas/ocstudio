package com.ds.dsm.view.config.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/module/")
public class ChildModuleTreeService {


    @RequestMapping(method = RequestMethod.POST, value = "ChildModuleTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> getChildModuleTree(String sourceClassName, String domainId, String methodName, String id) {
        List<Object> obj = new ArrayList<>();
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<>();
        ApiClassConfig customESDClassAPIBean = null;
        try {
            customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            obj.add(methodAPIBean);
            if (methodAPIBean.getView() != null) {
                obj.add(methodAPIBean.getView());
            }
            resultModel = TreePageUtil.getTreeList(Arrays.asList(methodAPIBean), ViewConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return resultModel;
    }

}
