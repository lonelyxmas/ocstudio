package com.ds.dsm.view.config.tree.contextmenu;

import com.ds.dsm.view.config.tree.contextmenu.menuclass.ContextTreeMenuService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.toolbar.RightContextMenuBean;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@BottomBarMenu
@FormAnnotation(col = 2, customService = ContextTreeMenuService.class)
public class ContextTreeMenuConfigView {
    @Pid
    String domainId;
    @Pid
    String sourceClassName;
    @Pid
    String childViewId;
    @Pid
    String methodName;
    @Uid
    String id;
    @CustomAnnotation(caption = "行头手柄")
    Boolean handler;
    @CustomAnnotation(caption = "动态加载")
    Boolean dynLoad = false;
    @CustomAnnotation(caption = "延迟加载")
    Boolean lazy;
    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "实现类")
    Class serviceClass;

    @CustomAnnotation(caption = "点击隐藏")
    public Boolean hideAfterClick;
    @CustomAnnotation(caption = "拖动支持KEY")
    public String listKey;
    @CustomAnnotation(caption = "自动隐藏")
    public Boolean autoHide;
    @CustomAnnotation(caption = "父级ID")
    public String parentID;
    @CustomAnnotation(caption = "是否数据域")
    public Boolean formField;
    @CustomAnnotation(caption = "字体大小")
    public String iconFontSize;
    @CustomAnnotation(caption = "菜单项Class")
    public String itemClass;
    @CustomAnnotation(caption = "菜单项Style")
    public String itemStyle;
    @CustomAnnotation(caption = "绑定服务")
    public Class bindService;


    public ContextTreeMenuConfigView() {

    }


    public ContextTreeMenuConfigView(RightContextMenuBean menuBarBean, String domainId, String childViewId, String sourceClassName, String methodName) {
        this.domainId = domainId;
        this.serviceClass = menuBarBean.getServiceClass();
        if (serviceClass == null && menuBarBean.getMenuClass().length > 0) {
            serviceClass = menuBarBean.getFristMenuClass();
        }

        this.sourceClassName = sourceClassName;
        this.methodName = methodName;
        this.childViewId = childViewId;
        this.id = menuBarBean.getId();
        this.dynLoad = menuBarBean.getDynLoad();
        this.lazy = menuBarBean.getLazy();
        this.handler = menuBarBean.getHandler();
        this.hideAfterClick = menuBarBean.getHideAfterClick();
        this.listKey = menuBarBean.getListKey();
        this.autoHide = menuBarBean.getAutoHide();
        this.parentID = menuBarBean.getParentID();
        this.formField = menuBarBean.getFormField();
        this.iconFontSize = menuBarBean.getIconFontSize();
        this.formField = menuBarBean.getFormField();
        this.parentID = menuBarBean.getParentID();
        this.itemClass = menuBarBean.getItemClass();
        this.itemStyle = menuBarBean.getItemStyle();
        this.lazy = menuBarBean.getLazy();
        if (menuBarBean.getBindService() != null && !menuBarBean.getBindService().equals(Void.class)) {
            this.bindService = menuBarBean.getBindService();
        }

    }

    public String getChildViewId() {
        return childViewId;
    }

    public void setChildViewId(String childViewId) {
        this.childViewId = childViewId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getHandler() {
        return handler;
    }

    public void setHandler(Boolean handler) {
        this.handler = handler;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public Boolean getLazy() {
        return lazy;
    }

    public void setLazy(Boolean lazy) {
        this.lazy = lazy;
    }

    public Class getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(Class serviceClass) {
        this.serviceClass = serviceClass;
    }

    public Boolean getHideAfterClick() {
        return hideAfterClick;
    }

    public void setHideAfterClick(Boolean hideAfterClick) {
        this.hideAfterClick = hideAfterClick;
    }

    public String getListKey() {
        return listKey;
    }

    public void setListKey(String listKey) {
        this.listKey = listKey;
    }

    public Boolean getAutoHide() {
        return autoHide;
    }

    public void setAutoHide(Boolean autoHide) {
        this.autoHide = autoHide;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public Boolean getFormField() {
        return formField;
    }

    public void setFormField(Boolean formField) {
        this.formField = formField;
    }

    public String getIconFontSize() {
        return iconFontSize;
    }

    public void setIconFontSize(String iconFontSize) {
        this.iconFontSize = iconFontSize;
    }

    public String getItemClass() {
        return itemClass;
    }

    public void setItemClass(String itemClass) {
        this.itemClass = itemClass;
    }

    public String getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(String itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Class getBindService() {
        return bindService;
    }

    public void setBindService(Class bindService) {
        this.bindService = bindService;
    }
}
