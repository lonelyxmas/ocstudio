package com.ds.dsm.view.config.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.tree.TreeView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.foldingtree.NavFoldingTreeViewBean;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.TreeDataBaseBean;
import com.ds.esd.custom.form.pop.PopTreeDataBean;
import com.ds.esd.custom.form.pop.PopTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/tree/")

public class TreeConfigService {


    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateTreeView")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formSave)
    public @ResponseBody
    ResultModel<Boolean> updateTreeBase(@RequestBody TreeView configView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(configView.getSourceClassName(), configView.getViewInstId());
            MethodConfig customMethodAPIBean = config.getMethodByName(configView.getMethodName());
            TreeDataBaseBean customTreeDataBean = null;
            CustomTreeViewBean customTreeViewBean = null;
            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                customTreeViewBean = ((NavTreeViewBean) customMethodAPIBean.getView()).getTreeViewBean();
                customTreeDataBean = (TreeDataBaseBean) customMethodAPIBean.getDataBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean navTreeViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                customTreeViewBean = navTreeViewBean.getTreeViewBean();
                customTreeDataBean = (TreeDataBaseBean) customMethodAPIBean.getDataBean();

            } else if (customMethodAPIBean.getView() instanceof PopTreeViewBean) {
                PopTreeViewBean treeViewBean = (PopTreeViewBean) customMethodAPIBean.getView();
                customTreeViewBean = treeViewBean.getTreeViewBean();
                customTreeDataBean = (PopTreeDataBean) customMethodAPIBean.getDataBean();
            } else {
                customTreeViewBean = (CustomTreeViewBean) customMethodAPIBean.getView();
                customTreeDataBean = (TreeDataBaseBean) customMethodAPIBean.getDataBean();
            }


            customTreeViewBean.setAnimCollapse(configView.getAnimCollapse());
            customTreeViewBean.setIniFold(configView.getIniFold());
            customTreeViewBean.setDynDestory(configView.getDynDestory());
            customTreeViewBean.setTogglePlaceholder(configView.getTogglePlaceholder());
            customTreeViewBean.setGroup(configView.getGroup());
            customTreeViewBean.setOptBtn(configView.getOptBtn());
            customTreeViewBean.setSingleOpen(configView.getSingleOpen());
            customTreeViewBean.setSelMode(configView.getSelMode());
            customTreeViewBean.setCaption(configView.getCaption());
            customTreeViewBean.setFormField(configView.getFormField());
            customTreeViewBean.setHeplBar(configView.getHeplBar());
            customTreeViewBean.setValueSeparator(configView.getValueSeparator());
            customTreeDataBean.setFieldCaption(configView.getFieldCaption());
            customTreeDataBean.setFieldId(configView.getFieldId());
            customTreeDataBean.setRootId(configView.getRootId());
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "清空列表配置")
    @RequestMapping(method = RequestMethod.POST, value = "clearData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formReSet)
    public @ResponseBody
    ResultModel<Boolean> clearData(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            config.getMethodByName(methodName).setView(null);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
