package com.ds.dsm.view.config.tree.child;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.jds.core.esb.util.OgnlUtil;
import ognl.OgnlException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.beans.IntrospectionException;

@Controller
@RequestMapping("/dsm/view/config/tree/child/item/")
public class TreeItemInfoService {

    @MethodChinaName(cname = "展示配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "TreeBaseView")
    @FormViewAnnotation()
    @ModuleAnnotation(imageClass = "spafont spa-icon-values", caption = "展示配置信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ChildTreeInfoView> getTreeBaseView(String sourceClassName, String childViewId, String sourceMethodName, String domainId, String viewInstId) {
        ResultModel<ChildTreeInfoView> result = new ResultModel<ChildTreeInfoView>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = classConfig.getMethodByName(sourceMethodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }
            ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
            result.setData(new ChildTreeInfoView(childTreeViewBean, sourceClassName, sourceMethodName));
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return result;
    }

    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateTreeView")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formSave)
    public @ResponseBody
    ResultModel<Boolean> updateTreeBase(@RequestBody ChildTreeInfoView childTreeInfoView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(childTreeInfoView.getSourceClassName(), childTreeInfoView.getDomainId());
            MethodConfig methodAPIBean = classConfig.getMethodByName(childTreeInfoView.getSourceMethodName());

            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }
            ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(childTreeInfoView.getChildTreeInfoId());
            try {
                OgnlUtil.setProperties(OgnlUtil.getBeanMap(childTreeInfoView), childTreeViewBean, false, false);
            } catch (IntrospectionException e) {
                e.printStackTrace();
            } catch (OgnlException e) {
                e.printStackTrace();
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}