package com.ds.dsm.view.config.nav.tree;

import com.ds.config.TreeListResultModel;

import com.ds.dsm.view.config.tree.CustomTreeTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tree/")
public class ViewTreeButtonService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewTreeButtons")
    @ModuleAnnotation(dynLoad = true, caption = "工具栏", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<CustomTreeTree>> getViewTreeButtons(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<CustomTreeTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(NavTreeButtonItems.values()), CustomTreeTree.class);
        return result;
    }


}
