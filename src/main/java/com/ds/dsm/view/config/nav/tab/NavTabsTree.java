package com.ds.dsm.view.config.nav.tab;

import com.ds.dsm.view.config.form.field.service.FormModuleService;
import com.ds.dsm.view.config.nav.tab.child.TabItemService;
import com.ds.dsm.view.config.service.ViewLayoutConfigService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.bean.nav.tab.TabItemBean;
import com.ds.esd.custom.layout.CustomLayoutItemBean;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class NavTabsTree extends TreeListItem {
    @Pid
    String sourceClassName;
    @Pid
    String sourceMethodName;
    @Pid
    String serviceClassName;
    @Pid
    String viewInstId;
    @Pid
    String domainId;
    @Pid
    String fieldname;
    @Pid
    String childTabId;
    @Pid
    String methodName;


    @TreeItemAnnotation(customItems = NavTabsItems.class)
    public NavTabsTree(NavTabsItems tabItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = tabItems.getName();
        this.bindClassName = tabItems.getBindClass().getName();
        this.imageClass = tabItems.getImageClass();
        this.id = tabItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = NavTabsButtonItems.class)
    public NavTabsTree(NavTabsButtonItems tabItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = tabItems.getName();
        this.bindClassName = tabItems.getBindClass().getName();
        this.imageClass = tabItems.getImageClass();
        this.id = tabItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(bindService = TabItemService.class)
    public NavTabsTree(TabItemBean childTabViewBean, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = childTabViewBean.getCaption();
        this.imageClass = childTabViewBean.getImageClass();
        this.childTabId = childTabViewBean.getId();
        if (childTabViewBean.getBindService() != null && !childTabViewBean.getBindService().equals(Void.class)) {
            this.serviceClassName = childTabViewBean.getBindService().getName();
        }
        this.id = sourceClassName + "_" + methodName + "_" + childTabId;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = childTabViewBean.getRootClassName();
        this.methodName = childTabViewBean.getRootMethodName();
        this.groupName = childTabViewBean.getGroupName();
    }


    @TreeItemAnnotation(bindService = ViewTabsInfoConfigService.class, caption = "Tab页配置")
    public NavTabsTree(NavTabsViewBean navTabsViewBean) {
        this.imageClass = ModuleViewType.NavTabsConfig.getImageClass();
        this.id = sourceClassName + "_" + methodName + "_" + navTabsViewBean.getId();
        this.domainId = navTabsViewBean.getDomainId();
        this.viewInstId = navTabsViewBean.getViewInstId();
        this.sourceClassName = navTabsViewBean.getSourceClassName();
        this.sourceMethodName = navTabsViewBean.getMethodName();

    }


    @TreeItemAnnotation(bindService = ViewLayoutConfigService.class, caption = "Layout配置")
    public NavTabsTree(CustomLayoutItemBean layoutItemBean, String sourceClassName, String methodName, String sourceMethodName, String domainId, String viewInstId) {
        this.caption = "Layout配置";
        this.imageClass = ModuleViewType.NavTabsConfig.getImageClass();
        this.id = "DSMNavLayoutRoot_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceMethodName;
        this.sourceMethodName = methodName;

    }

    @TreeItemAnnotation(bindService = FormModuleService.class)
    public NavTabsTree(FieldModuleConfig fieldFormInfo, String sourceMethodName) {
        this.caption = fieldFormInfo.getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        this.imageClass = fieldFormInfo.getImageClass();
        this.id = fieldFormInfo.getSourceClassName() + "_" + sourceMethodName + "_" + fieldFormInfo.getFieldname();
        this.domainId = fieldFormInfo.getDomainId();
        this.viewInstId = fieldFormInfo.getDomainId();
        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = sourceMethodName;
        this.fieldname = fieldFormInfo.getFieldname();
    }

    public String getServiceClassName() {
        return serviceClassName;
    }

    public void setServiceClassName(String serviceClassName) {
        this.serviceClassName = serviceClassName;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getChildTabId() {
        return childTabId;
    }

    public void setChildTabId(String childTabId) {
        this.childTabId = childTabId;
    }
}
