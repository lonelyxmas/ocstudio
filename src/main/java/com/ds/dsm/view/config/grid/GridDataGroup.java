package com.ds.dsm.view.config.grid;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.api.method.APIMethodBaseFormView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.grid.CustomGridDataBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/grid/")
@ButtonViewsAnnotation(barLocation = BarLocationType.top, sideBarStatus = SideBarStatusType.expand, barSize = "3em")
public class GridDataGroup {


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, uid = true)
    String sourceClassName;
    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    public GridDataGroup() {

    }

    @MethodChinaName(cname = "接口信息")
    @RequestMapping(method = RequestMethod.POST, value = "APIMethodBaseView")
    @FormViewAnnotation()
    @ModuleAnnotation( caption = "接口信息", imageClass = "spafont spa-icon-c-webapi", dock = Dock.top)
    @UIAnnotation(height = "150")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<APIMethodBaseFormView> getAPIMethodBaseView(String viewInstId,String domainId, String methodName, String sourceClassName) {
        ResultModel<APIMethodBaseFormView> result = new ResultModel<APIMethodBaseFormView>();
        try {
            ViewEntityConfig esdClassConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,viewInstId);
            MethodConfig apiCallBean = esdClassConfig.getSourceConfig().getMethodByName(methodName);
            result.setData(new APIMethodBaseFormView(apiCallBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "视图绑定配置")
    @RequestMapping(method = RequestMethod.POST, value = "GridDataView")
    @FormViewAnnotation(reSetUrl = "clearGridData", saveUrl = "updateGridData")
    @ModuleAnnotation( dock = Dock.fill, imageClass = "spafont spa-icon-values", caption = "视图绑定配置")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<GridDataView> getGridDataView(String sourceClassName, String methodName, String domainId,String viewInstId) {
        ResultModel<GridDataView> result = new ResultModel<GridDataView>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridDataBean gridDataBean = (CustomGridDataBean) methodAPIBean.getDataBean();
            result.setData(new GridDataView(gridDataBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
