package com.ds.dsm.view.config.action;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/field/action/")
public class FieldAction {


    @MethodChinaName(cname = "置顶")
    @RequestMapping(method = RequestMethod.POST, value = "moveTop")
    @CustomAnnotation(imageClass = "xui-icon-doubleup", index = 0)
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> moveTop(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);

            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(methodName);
            CustomViewBean customViewBean = (CustomViewBean) customMethodAPIBean.getView();
            List<String> fieldNames = new ArrayList<>();
            List<String> displayFieldNames = customViewBean.getDisplayFieldNames();

            fieldNames.addAll(displayFieldNames);
            fieldNames.addAll(customViewBean.getHiddenFieldNames());

            int k = fieldNames.indexOf(fieldname);
            fieldNames.add(0, fieldNames.remove(k));


            LinkedHashSet<String> setNames = new LinkedHashSet<>();

            for (String displayName : fieldNames) {
                setNames.add(displayName);
            }

            customViewBean.setFieldNames(setNames);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "置底")
    @RequestMapping(method = RequestMethod.POST, value = "moveBottom")
    @CustomAnnotation(imageClass = "xui-icon-doubledown", index = 0)
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> moveBottom(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);

            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(methodName);
            CustomViewBean customViewBean = (CustomViewBean) customMethodAPIBean.getView();
            List<String> fieldNames = new ArrayList<>();
            List<String> displayFieldNames = customViewBean.getDisplayFieldNames();
            fieldNames.addAll(displayFieldNames);
            fieldNames.addAll(customViewBean.getHiddenFieldNames());

            int k = fieldNames.indexOf(fieldname);
            fieldNames.add(fieldNames.size() - 1, fieldNames.remove(k));


            LinkedHashSet<String> setNames = new LinkedHashSet<>();

            for (String displayName : fieldNames) {
                setNames.add(displayName);
            }

            customViewBean.setFieldNames(setNames);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "向上")
    @RequestMapping(method = RequestMethod.POST, value = "moveUP")
    @CustomAnnotation(imageClass = "spafont spa-icon-move-up", index = 0)
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> moveUP(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);

            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(methodName);
            CustomViewBean customViewBean = (CustomViewBean) customMethodAPIBean.getView();
            List<String> fieldNames = new ArrayList<>();
            List<String> displayFieldNames = customViewBean.getDisplayFieldNames();
            int k = displayFieldNames.indexOf(fieldname);
            if (k < displayFieldNames.size()) {
                displayFieldNames.add(k - 1, displayFieldNames.remove(k));
            }

            fieldNames.addAll(displayFieldNames);
            fieldNames.addAll(customViewBean.getHiddenFieldNames());

            LinkedHashSet<String> setNames = new LinkedHashSet<>();

            for (String displayName : fieldNames) {
                setNames.add(displayName);
            }

            customViewBean.setFieldNames(setNames);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "向下")
    @RequestMapping(method = RequestMethod.POST, value = "moveDown")
    @CustomAnnotation(imageClass = "spafont spa-icon-move-down", index = 1)
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> moveDown(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);

            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(methodName);
            CustomViewBean customViewBean = (CustomViewBean) customMethodAPIBean.getView();

            List<String> fieldNames = new ArrayList<>();
            List<String> displayFieldNames = customViewBean.getDisplayFieldNames();
            int k = displayFieldNames.indexOf(fieldname);
            if (k < displayFieldNames.size()) {
                displayFieldNames.add(k + 1, displayFieldNames.remove(k));
            }

            fieldNames.addAll(displayFieldNames);
            fieldNames.addAll(customViewBean.getHiddenFieldNames());

            LinkedHashSet<String> setNames = new LinkedHashSet<>();

            for (String displayName : fieldNames) {
                setNames.add(displayName);
            }

            customViewBean.setFieldNames(setNames);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}

