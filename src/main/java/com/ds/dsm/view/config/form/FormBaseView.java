package com.ds.dsm.view.config.form;

import com.ds.dsm.view.config.service.FormConfigService;
import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.form.formlayout.LayoutData;
import com.ds.esd.tool.ui.enums.*;

@BottomBarMenu()
@FormAnnotation(
        bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet}, customService = FormConfigService.class)
public class FormBaseView {

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String entityClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    @ComboInputAnnotation(inputType = ComboInputType.spin)
    @FieldAnnotation(componentType = ComponentType.ComboInput)
    @CustomAnnotation(caption = "默认列数")
    Integer col;




    @CustomAnnotation(caption = "默认列宽")
    Integer defaultColumnSize;

    @CustomAnnotation(caption = "延伸列宽方式")
    StretchType stretchH;

    @CustomAnnotation(caption = "高度延伸方式")
    StretchType stretchHeight;


    @CustomAnnotation(caption = "渲染方式")
    FormLayModeType mode;

    @CustomAnnotation(caption = "标题高度")
    Integer columnHeaderHeight;

    @CustomAnnotation(caption = "文本对齐")
    HAlignType textAlign = HAlignType.center;

    //是否 显示网格线
    @CustomAnnotation(caption = "隐藏网格")
    Boolean solidGridlines;

    @CustomAnnotation(caption = "浮动表格手柄")
    Boolean floatHandler;
    @CustomAnnotation(caption = "行头宽度")
    Integer rowHeaderWidth;
    @CustomAnnotation(caption = "默认行大小")
    Integer defaultRowSize;
    @CustomAnnotation(caption = "默认高度")
    Integer defaultRowHeight;
    @CustomAnnotation(caption = "默认列宽")
    Integer defaultColWidth;
    @CustomAnnotation(caption = "行间隔")
    Integer lineSpacing;

    @CustomAnnotation(caption = "自定义样式")
    String cssStyle;





    public FormBaseView() {

    }

    public FormBaseView(CustomFormViewBean formConfig) {
        this.mode = formConfig.getMode();
        this.viewInstId = formConfig.getViewInstId();
        this.domainId = formConfig.getDomainId();
        this.viewClassName = formConfig.getViewClassName();
        this.sourceClassName = formConfig.getSourceClassName();
        this.defaultColumnSize = formConfig.getDefaultColumnSize();
        this.columnHeaderHeight = formConfig.getColumnHeaderHeight();
        this.stretchH = formConfig.getStretchH();
        this.col = formConfig.getCol();
        this.solidGridlines = formConfig.getSolidGridlines();
        this.textAlign = formConfig.getTextAlign();
        this.methodName = formConfig.getMethodName();
        this.stretchHeight=formConfig.getStretchHeight();
        this.floatHandler=formConfig.getFloatHandler();
        this.rowHeaderWidth=formConfig.getRowHeaderWidth();
        this.defaultRowSize=formConfig.getDefaultRowSize();
        this.defaultRowHeight=formConfig.getDefaultRowHeight();
        this.defaultColWidth=formConfig.getDefaultColWidth();
        this.lineSpacing=formConfig.getLineSpacing();
        this.cssStyle=formConfig.getCssStyle();




    }

    public StretchType getStretchHeight() {
        return stretchHeight;
    }

    public void setStretchHeight(StretchType stretchHeight) {
        this.stretchHeight = stretchHeight;
    }

    public Boolean getFloatHandler() {
        return floatHandler;
    }

    public void setFloatHandler(Boolean floatHandler) {
        this.floatHandler = floatHandler;
    }

    public Integer getRowHeaderWidth() {
        return rowHeaderWidth;
    }

    public void setRowHeaderWidth(Integer rowHeaderWidth) {
        this.rowHeaderWidth = rowHeaderWidth;
    }

    public Integer getDefaultRowSize() {
        return defaultRowSize;
    }

    public void setDefaultRowSize(Integer defaultRowSize) {
        this.defaultRowSize = defaultRowSize;
    }

    public Integer getDefaultRowHeight() {
        return defaultRowHeight;
    }

    public void setDefaultRowHeight(Integer defaultRowHeight) {
        this.defaultRowHeight = defaultRowHeight;
    }

    public Integer getDefaultColWidth() {
        return defaultColWidth;
    }

    public void setDefaultColWidth(Integer defaultColWidth) {
        this.defaultColWidth = defaultColWidth;
    }

    public Integer getLineSpacing() {
        return lineSpacing;
    }

    public void setLineSpacing(Integer lineSpacing) {
        this.lineSpacing = lineSpacing;
    }

    public String getCssStyle() {
        return cssStyle;
    }

    public void setCssStyle(String cssStyle) {
        this.cssStyle = cssStyle;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public HAlignType getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(HAlignType textAlig) {
        this.textAlign = textAlig;
    }

    public FormLayModeType getMode() {
        return mode;
    }

    public void setMode(FormLayModeType mode) {
        this.mode = mode;
    }

    public StretchType getStretchH() {
        return stretchH;
    }

    public void setStretchH(StretchType stretchH) {
        this.stretchH = stretchH;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getCol() {
        return col;
    }

    public void setCol(Integer col) {
        this.col = col;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public Integer getColumnHeaderHeight() {
        return columnHeaderHeight;
    }

    public void setColumnHeaderHeight(Integer columnHeaderHeight) {
        this.columnHeaderHeight = columnHeaderHeight;
    }


    public Integer getDefaultColumnSize() {
        return defaultColumnSize;
    }

    public void setDefaultColumnSize(Integer defaultColumnSize) {
        this.defaultColumnSize = defaultColumnSize;
    }

    public void setSolidGridlines(Boolean solidGridlines) {
        this.solidGridlines = solidGridlines;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public Boolean getSolidGridlines() {
        return solidGridlines;
    }

}
