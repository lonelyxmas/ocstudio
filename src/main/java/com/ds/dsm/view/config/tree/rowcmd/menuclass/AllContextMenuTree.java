package com.ds.dsm.view.config.tree.rowcmd.menuclass;

import com.ds.dsm.aggregation.config.menu.pop.AggMenuTreeService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = TreeRowMenuService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class AllContextMenuTree extends TreeListItem {

    @Pid
    CustomMenuType menuType;
    @Pid
    String domainId;
    @Pid
    String sourceClassName;
    @Pid
    String methodName;
    @Pid
    String childViewId;
    @Pid
    String euClassName;

    @TreeItemAnnotation(bindService = AggMenuTreeService.class, dynLoad = true, dynDestory = true)
    public AllContextMenuTree(CustomMenuType menuType, String domainId, String childViewId, String sourceClassName, String methodName) {
        this.caption = menuType.getName();
        this.imageClass = menuType.getImageClass();
        this.id = menuType.getType();
        this.domainId = domainId;
        this.menuType = menuType;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;
        this.childViewId = childViewId;

    }


    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-grid")
    public AllContextMenuTree(ESDClass esdClass, String domainId, String childViewId, String sourceClassName, String methodName) {
        this.caption = esdClass.getName() + "(" + esdClass.getDesc() + ")";
        this.euClassName = esdClass.getClassName();
        this.id = esdClass.getClassName();
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;
        this.childViewId = childViewId;

    }

    public String getChildViewId() {
        return childViewId;
    }

    public void setChildViewId(String childViewId) {
        this.childViewId = childViewId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    @Override
    public String getEuClassName() {
        return euClassName;
    }

    @Override
    public void setEuClassName(String euClassName) {
        this.euClassName = euClassName;
    }

    public CustomMenuType getMenuType() {
        return menuType;
    }

    public void setMenuType(CustomMenuType menuType) {
        this.menuType = menuType;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

}
