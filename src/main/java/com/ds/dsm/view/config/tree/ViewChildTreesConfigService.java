package com.ds.dsm.view.config.tree;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.child.ChildTreeGrid;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tree/")
public class ViewChildTreesConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewTreeGrid")
    @ModuleAnnotation(dynLoad = true, caption = "获取子项", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<ChildTreeGrid>> getViewTreeGrid(String domainId, String sourceClassName, String sourceMethodName, String childViewId) {
        ListResultModel<List<ChildTreeGrid>> result = new ListResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(sourceMethodName);
            List<Object> childTreeViewBeans = new ArrayList<>();
            if (methodAPIBean != null && methodAPIBean.getView() != null) {
                CustomTreeViewBean customTreeViewBean = null;
                if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                    NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                    customTreeViewBean = baseViewBean.getTreeView();
                } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                    customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                }
                if (childViewId != null) {
                    customTreeViewBean = customTreeViewBean.getChildBean(childViewId);
                }
                if (customTreeViewBean != null) {
                    childTreeViewBeans.addAll(customTreeViewBean.getChildBeans());
                    childTreeViewBeans.addAll(customTreeViewBean.getChildTreeBeans());
                }
            }
            result = PageUtil.getDefaultPageList(childTreeViewBeans, ChildTreeGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ViewTreeModule")
    @ModuleAnnotation(dynLoad = true, caption = "获取字段列", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<CustomTreeTree>> getViewTreeModule(String domainId, String sourceClassName, String sourceMethodName, String childViewId, String viewInstId) {
        TreeListResultModel<List<CustomTreeTree>> result = new TreeListResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(sourceMethodName);
            List<Object> treeViewBeans = new ArrayList<>();
            if (methodAPIBean != null && methodAPIBean.getView() != null) {
                CustomTreeViewBean customTreeViewBean = null;
                if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                    NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                    customTreeViewBean = baseViewBean.getTreeView();
                } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                    customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                }

                if (childViewId != null) {
                    customTreeViewBean = customTreeViewBean.getChildBean(childViewId);
                }


                if (customTreeViewBean != null) {
                    treeViewBeans.addAll(customTreeViewBean.getChildBeans());
                    List<ChildTreeViewBean> childTreeViewBeans = customTreeViewBean.getChildTreeBeans();
                    for (ChildTreeViewBean childTreeViewBean : childTreeViewBeans) {
                        if (childTreeViewBean.getBindService() != null && !childTreeViewBean.getBindService().equals(Void.class)) {
                            treeViewBeans.add(childTreeViewBean);
                        }

                    }
                }
            }


            result = TreePageUtil.getTreeList(treeViewBeans, CustomTreeTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
