package com.ds.dsm.view.config.nav.gallery;


import com.ds.dsm.view.config.container.CustomNavService;
import com.ds.dsm.view.config.gallery.ViewGalleryConfigService;
import com.ds.dsm.view.config.nav.layout.LayoutService;
import com.ds.dsm.view.config.nav.tab.ViewNavTabsConfigService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum GalleryNavItems implements TreeItem {
    GalleryNavConfig("画廊配置", "spafont spa-icon-values", ViewGalleryConfigService.class, false, false, false),
    CustomNav("导航容器配置", "spafont spa-icon-values", CustomNavService.class, false, false, false),
    TabsConfig("Tab页配置", "spafont spa-icon-c-dialog", ViewNavTabsConfigService.class, false, false, false),
    Layout("布局配置", "spafont spa-icon-c-layout", LayoutService.class, false, false, false);

    private final String name;

    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    GalleryNavItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
