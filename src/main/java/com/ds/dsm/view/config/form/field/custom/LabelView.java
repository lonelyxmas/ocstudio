package com.ds.dsm.view.config.form.field.custom;

import com.ds.dsm.view.config.form.field.service.FormInputService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.InputFieldBean;
import com.ds.esd.custom.field.LabelBean;
import com.ds.esd.custom.field.TipsBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.tool.ui.enums.LabelPos;
import com.ds.esd.tool.ui.enums.VAlignType;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu
@FormAnnotation(col = 2, customService = FormInputService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class LabelView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;

    @CustomAnnotation(hidden = true, pid = true)
    public String fieldname;

    @CustomAnnotation(caption = "标签定位")
    LabelPos labelPos;
    @CustomAnnotation(caption = "标签间隔")
    String labelGap;
    @CustomAnnotation(caption = "上下居中")
    HAlignType labelHAlign;
    @CustomAnnotation(caption = "左右居中")
    VAlignType labelVAlign;
    @CustomAnnotation(caption = "标签大小")
    String labelSize;

    public LabelView() {

    }


    public LabelView(FieldFormConfig<InputFieldBean,?> config) {
        LabelBean labelBean = config.getLabelBean();
        if (labelBean == null) {
            labelBean = new LabelBean();
        }
        BeanMap.create(this).putAll(BeanMap.create(labelBean));

        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();

    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public LabelPos getLabelPos() {
        return labelPos;
    }

    public void setLabelPos(LabelPos labelPos) {
        this.labelPos = labelPos;
    }

    public String getLabelGap() {
        return labelGap;
    }

    public void setLabelGap(String labelGap) {
        this.labelGap = labelGap;
    }

    public HAlignType getLabelHAlign() {
        return labelHAlign;
    }

    public void setLabelHAlign(HAlignType labelHAlign) {
        this.labelHAlign = labelHAlign;
    }

    public VAlignType getLabelVAlign() {
        return labelVAlign;
    }

    public void setLabelVAlign(VAlignType labelVAlign) {
        this.labelVAlign = labelVAlign;
    }

    public String getLabelSize() {
        return labelSize;
    }

    public void setLabelSize(String labelSize) {
        this.labelSize = labelSize;
    }
}
