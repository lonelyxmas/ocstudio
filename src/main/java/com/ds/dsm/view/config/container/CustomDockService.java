package com.ds.dsm.view.config.container;

import com.alibaba.fastjson.JSONObject;
import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.*;
import com.ds.esd.custom.bean.nav.NavTabsBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/container/")
public class CustomDockService {

    @RequestMapping(method = RequestMethod.POST, value = "CustomDockConfig")
    @FormViewAnnotation(autoSave = true)
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "基础信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<CustomDockView> getCustomDockConfig(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<CustomDockView> result = new ResultModel<CustomDockView>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            NavTabsBaseViewBean tabsBaseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
            CustomViewBean  viewBean = tabsBaseViewBean.getCurrViewBean();
            result.setData(new CustomDockView(viewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "updateCustomDock")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateCustomDock(@RequestBody CustomDockView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(comboView.getMethodName());
            NavTabsBaseViewBean tabsBaseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
            CustomViewBean  viewBean = tabsBaseViewBean.getCurrViewBean();
            ContainerBean containerBean = viewBean.getContainerBean();
            if (containerBean == null) {
                containerBean = new ContainerBean();
            }
            String json = JSONObject.toJSONString(comboView);
            DockBean dockBean = JSONObject.parseObject(json, DockBean.class);
            containerBean.setDockBean(dockBean);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetCustomDock")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetCustomDock(@RequestBody CustomDisabledView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(comboView.getSourceMethodName());
            NavTabsBaseViewBean tabsBaseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
            CustomViewBean  viewBean = tabsBaseViewBean.getCurrViewBean();
            ContainerBean containerBean = viewBean.getContainerBean();
            if (containerBean == null) {
                containerBean = new ContainerBean();
            }
            containerBean.setDockBean(new DockBean());
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
