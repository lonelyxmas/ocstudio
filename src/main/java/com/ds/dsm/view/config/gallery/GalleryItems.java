package com.ds.dsm.view.config.gallery;

import com.ds.dsm.view.config.gallery.service.GalleryConfigService;
import com.ds.dsm.view.config.gallery.service.GalleryContextService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum GalleryItems implements TreeItem {
    GalleryConfig("画廊配置", "spafont spa-icon-values", GalleryConfigService.class, false, false, false),

    GalleryButtons("工具栏", "spafont spa-icon-c-toolbar", GalleryEventService.class, true, false, false),
    FieldHiddenGridList("环境变量", "spafont spa-icon-c-hiddeninput", GalleryContextService.class, true, true, true);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    GalleryItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
