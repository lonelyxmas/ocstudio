package com.ds.dsm.view.config.form.field.combo.service;

import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.combo.ComboDateTimeGroup;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/combo/")
@MethodChinaName(cname = "时间选择配置", imageClass = "spafont spa-icon-c-dateinput")

public class ComboDateTimeService {


    @RequestMapping(method = RequestMethod.POST, value = "ComboDateTimeGroup")
    @NavGroupViewAnnotation(autoSave = true)
    @ModuleAnnotation(caption = "时间选择配置")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.treeNodeEditor})
    @ResponseBody
    public ResultModel<ComboDateTimeGroup> getComboDateTimeGroup(String sourceClassName, String projectId, String fieldname) {
        return new ResultModel<>();
    }
}
