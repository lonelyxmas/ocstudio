package com.ds.dsm.view.config.tree.child;

import com.ds.dsm.view.config.grid.cell.contextmenu.ContextCellService;
import com.ds.dsm.view.config.tree.rowcmd.TreeRowService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum NavTreeModuleItems implements TreeItem {
    TabsItemInfo("Tree标签", "spafont spa-icon-config", TreeItemInfoService.class, true, true, true),
    TreeRowButtonList("行按钮", "spafont spa-icon-project", TreeRowService.class, false, false, false),

    TreeRightMenuList("右键菜单", "spafont spa-icon-c-url", ContextCellService.class, false, false, false),
    TabsItemServiceInfo("绑定服务", "spafont spa-icon-config", TreeModuleService.class, true, true, true);

    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    NavTreeModuleItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
