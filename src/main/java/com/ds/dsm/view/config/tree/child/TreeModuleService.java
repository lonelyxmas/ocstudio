package com.ds.dsm.view.config.tree.child;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tree/child/item/")
public class TreeModuleService {


    @MethodChinaName(cname = "聚合实体信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggEntityInfo")
    @NavGroupViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, caption = "装载", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<AggChildEntityNav> getAggEntityInfo(String domainId, String serviceClassName) {
        ResultModel<AggChildEntityNav> result = new ResultModel<AggChildEntityNav>();

        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ChildModuleTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> getChildModuleTree(String sourceClassName, String serviceClassName, String methodName, String domainId, String id) {
        List<Object> objs = new ArrayList<>();
        try {
            if (serviceClassName != null && !serviceClassName.equals(sourceClassName)) {
                ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(serviceClassName, domainId);
                MethodConfig methodConfig = apiClassConfig.getMethodByItem(CustomMenuItem.treeNodeEditor);
                if (methodConfig != null && methodConfig.getView() != null) {
                    objs.add(methodConfig);
                    objs.add(methodConfig.getView());
                }
                MethodConfig childConfig = apiClassConfig.getMethodByItem(CustomMenuItem.loadChild);
                if (childConfig != null) {
                    objs.add(childConfig);
                }
            } else {
                ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
                MethodConfig methodConfig = apiClassConfig.getMethodByName(methodName);
                if (methodConfig != null) {
                    objs.add(methodConfig);
                }

            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

        TreeListResultModel resultModel = TreePageUtil.getTreeList(objs, ViewConfigTree.class);
        return resultModel;
    }


}
