package com.ds.dsm.view.config.tree;

import com.ds.dsm.view.config.form.field.service.FormModuleService;
import com.ds.dsm.view.config.nav.tree.NavTreeButtonItems;
import com.ds.dsm.view.config.nav.tree.NavTreeConfigItems;
import com.ds.dsm.view.config.nav.tree.NavTreePopItems;
import com.ds.dsm.view.config.tree.child.NavTreeModuleItems;
import com.ds.dsm.view.config.tree.child.TreeItemModuleService;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class CustomTreeTree extends TreeListItem {

    @Pid
    String domainId;
    @Pid
    String methodName;
    @Pid
    String viewInstId;
    @Pid
    String sourceMethodName;
    @Pid
    String sourceClassName;
    @Pid
    String serviceClassName;

    @Pid
    String fieldname;
    @Pid
    String childViewId;


    @TreeItemAnnotation(customItems = NavTreeConfigItems.class)
    public CustomTreeTree(NavTreeConfigItems treeNavItems, String sourceClassName, String sourceMethodName, String methodName, String domainId, String viewInstId) {
        this.caption = treeNavItems.getName();
        this.bindClassName = treeNavItems.getBindClass().getName();
        this.imageClass = treeNavItems.getImageClass();
        this.id = treeNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(customItems = NavTreeButtonItems.class)
    public CustomTreeTree(NavTreeButtonItems treeNavItems, String sourceClassName, String sourceMethodName, String methodName, String domainId, String viewInstId) {
        this.caption = treeNavItems.getName();
        this.bindClassName = treeNavItems.getBindClass().getName();
        this.imageClass = treeNavItems.getImageClass();
        this.id = treeNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(customItems = NavTreePopItems.class)
    public CustomTreeTree(NavTreePopItems treeNavItems, String sourceClassName, String methodName, String sourceMethodName, String domainId, String viewInstId) {
        this.caption = treeNavItems.getName();
        this.bindClassName = treeNavItems.getBindClass().getName();
        this.imageClass = treeNavItems.getImageClass();
        this.id = treeNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = NavTreeModuleItems.class)
    public CustomTreeTree(NavTreeModuleItems treeNavItems, String sourceClassName, String serviceClassName, String childViewId, String methodName, String sourceMethodName, String domainId, String viewInstId) {
        this.caption = treeNavItems.getName();
        this.bindClassName = treeNavItems.getBindClass().getName();
        this.imageClass = treeNavItems.getImageClass();
        this.id = childViewId + "_" + treeNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.childViewId = childViewId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.serviceClassName = serviceClassName;
        this.sourceMethodName = sourceMethodName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(bindService = ViewChildTreesConfigService.class, iniFold = true, lazyLoad = true, dynDestory = true)
    public CustomTreeTree(CustomTreeViewBean customTreeViewBean) {
        this.caption = customTreeViewBean.getCaption();
        this.setImageClass(customTreeViewBean.getImageClass());
        this.id = customTreeViewBean.getSourceClassName() + "_" + customTreeViewBean.getMethodName() + "_" + customTreeViewBean.getId();
        this.domainId = customTreeViewBean.getDomainId();
        this.viewInstId = customTreeViewBean.getViewInstId();
        this.childViewId = customTreeViewBean.getId();
        this.sourceClassName = customTreeViewBean.getRootClassName();
        this.sourceMethodName = customTreeViewBean.getRootMethodName();
        this.methodName = customTreeViewBean.getRootMethodName();
        if (customTreeViewBean.getBindService() != null) {
            this.serviceClassName = customTreeViewBean.getBindService().getName();
        }

    }

    @TreeItemAnnotation(bindService = TreeItemModuleService.class, imageClass = "xui-icon-indent")
    public CustomTreeTree(ChildTreeViewBean childTreeViewBean, String sourceClassName, String sourceMethodName, String methodName) {
        this.caption = childTreeViewBean.getCaption();
        this.imageClass = childTreeViewBean.getImageClass();
        this.id = sourceClassName + "_" + methodName + "_" + childTreeViewBean.getId();
        this.domainId = childTreeViewBean.getDomainId();
        this.viewInstId = childTreeViewBean.getViewInstId();
        this.sourceClassName = sourceClassName;
        this.methodName = childTreeViewBean.getMethodName();
        if (childTreeViewBean.getBindService() != null) {
            this.serviceClassName = childTreeViewBean.getBindService().getName();
        }
        this.childViewId = childTreeViewBean.getId();
        this.sourceMethodName = sourceMethodName;
    }


    @TreeItemAnnotation(bindService = FormModuleService.class)
    public CustomTreeTree(FieldFormConfig fieldFormInfo, String sourceMethodName) {
        this.caption = fieldFormInfo.getAggConfig().getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        if (fieldFormInfo.getWidgetConfig() instanceof ComboInputFieldBean) {
            ComboBoxBean comboBoxBean = (ComboBoxBean) fieldFormInfo.getWidgetConfig();
            this.imageClass = comboBoxBean.getInputType().getImageClass();
        } else {
            this.imageClass = fieldFormInfo.getWidgetConfig().getComponentType().getImageClass();
        }

        this.domainId = fieldFormInfo.getDomainId();
        this.viewInstId = fieldFormInfo.getDomainId();
        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = sourceMethodName;
        this.fieldname = fieldFormInfo.getFieldname();

    }

    public String getServiceClassName() {
        return serviceClassName;
    }

    public void setServiceClassName(String serviceClassName) {
        this.serviceClassName = serviceClassName;
    }

    public String getChildViewId() {
        return childViewId;
    }

    public void setChildViewId(String childViewId) {
        this.childViewId = childViewId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
