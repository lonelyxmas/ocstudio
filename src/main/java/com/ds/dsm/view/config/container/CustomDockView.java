package com.ds.dsm.view.config.container;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.CodeEditorAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.ContainerBean;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.DockBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.style.Margin;
import com.ds.esd.tool.ui.component.style.Spacing;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.enums.DockFlexType;
import com.ds.esd.tool.ui.enums.DockStretchType;
import com.ds.esd.tool.ui.json.JSONEditorSerializer;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu
@FormAnnotation(col = 2, customService = CustomDockService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class CustomDockView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    public String fieldname;


    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "平铺")
    public Dock dock;
    @CustomAnnotation(caption = "忽略悬浮")
    public Boolean dockIgnore;
    @CustomAnnotation(caption = "启用悬浮")
    public Boolean dockFloat;
    @CustomAnnotation(caption = "停靠忽略")
    public Boolean dockOrder;
    @CustomAnnotation(caption = "停靠柔性填充")
    public Boolean dockIgnoreFlexFill;


    @CustomAnnotation(caption = "最小宽度")
    public String dockMinW;
    @CustomAnnotation(caption = "最小高度")
    public String dockMinH;
    @CustomAnnotation(caption = "最大宽度")
    public String dockMaxW;
    @CustomAnnotation(caption = "最大高度")
    public String dockMaxH;

    @FieldAnnotation(colSpan = -1)
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = DockStretchType.class)
    @CustomAnnotation(caption = "停靠延展")
    public String dockStretch;

    @FieldAnnotation(colSpan = -1, rowHeight = "100")
    @CodeEditorAnnotation
    @CustomAnnotation(caption = "停靠外补丁")
    @JSONField(serializeUsing = JSONEditorSerializer.class, deserializeUsing = JSONEditorSerializer.class)
    public Margin dockMargin;


    @CustomAnnotation(caption = "子控件停靠柔性")
    public DockFlexType conDockFlexFill;

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = DockStretchType.class)
    @CustomAnnotation(caption = "子控件延展")
    public String conDockStretch;
    @CustomAnnotation(caption = "子控件关联")
    public Boolean conDockRelative;

    @FieldAnnotation(colSpan = -1, rowHeight = "100")
    @CodeEditorAnnotation
    @CustomAnnotation(caption = "子控件停靠边距")
    @JSONField(serializeUsing = JSONEditorSerializer.class, deserializeUsing = JSONEditorSerializer.class)
    public Margin conDockPadding;

    @FieldAnnotation(colSpan = -1, rowHeight = "100")
    @CodeEditorAnnotation
    @CustomAnnotation(caption = "子控件停靠间隔")
    @JSONField(serializeUsing = JSONEditorSerializer.class, deserializeUsing = JSONEditorSerializer.class)
    public Spacing conDockSpacing;


    public CustomDockView() {

    }


    public CustomDockView(CustomViewBean config) {
        ContainerBean containerBean = config.getContainerBean();
        if (containerBean == null) {
            containerBean = new ContainerBean();
        }
        DockBean dockBean = containerBean.getDockBean();
        if (dockBean == null) {
            dockBean = new DockBean();
        }

        String json = JSONObject.toJSONString(dockBean);
        BeanMap.create(this).putAll(JSONObject.parseObject(json));
        this.methodName = config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();


    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getConDockStretch() {
        return conDockStretch;
    }

    public void setConDockStretch(String conDockStretch) {
        this.conDockStretch = conDockStretch;
    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }

    public Boolean getDockIgnore() {
        return dockIgnore;
    }

    public void setDockIgnore(Boolean dockIgnore) {
        this.dockIgnore = dockIgnore;
    }

    public Boolean getDockFloat() {
        return dockFloat;
    }

    public void setDockFloat(Boolean dockFloat) {
        this.dockFloat = dockFloat;
    }

    public Boolean getDockOrder() {
        return dockOrder;
    }

    public void setDockOrder(Boolean dockOrder) {
        this.dockOrder = dockOrder;
    }

    public Margin getDockMargin() {
        return dockMargin;
    }

    public void setDockMargin(Margin dockMargin) {
        this.dockMargin = dockMargin;
    }

    public String getDockMinW() {
        return dockMinW;
    }

    public void setDockMinW(String dockMinW) {
        this.dockMinW = dockMinW;
    }

    public String getDockMinH() {
        return dockMinH;
    }

    public void setDockMinH(String dockMinH) {
        this.dockMinH = dockMinH;
    }

    public String getDockMaxW() {
        return dockMaxW;
    }

    public void setDockMaxW(String dockMaxW) {
        this.dockMaxW = dockMaxW;
    }

    public String getDockMaxH() {
        return dockMaxH;
    }

    public void setDockMaxH(String dockMaxH) {
        this.dockMaxH = dockMaxH;
    }

    public Boolean getDockIgnoreFlexFill() {
        return dockIgnoreFlexFill;
    }

    public void setDockIgnoreFlexFill(Boolean dockIgnoreFlexFill) {
        this.dockIgnoreFlexFill = dockIgnoreFlexFill;
    }

    public String getDockStretch() {
        return dockStretch;
    }

    public void setDockStretch(String dockStretch) {
        this.dockStretch = dockStretch;
    }

    public Margin getConDockPadding() {
        return conDockPadding;
    }

    public void setConDockPadding(Margin conDockPadding) {
        this.conDockPadding = conDockPadding;
    }

    public Spacing getConDockSpacing() {
        return conDockSpacing;
    }

    public void setConDockSpacing(Spacing conDockSpacing) {
        this.conDockSpacing = conDockSpacing;
    }

    public Boolean getConDockRelative() {
        return conDockRelative;
    }

    public void setConDockRelative(Boolean conDockRelative) {
        this.conDockRelative = conDockRelative;
    }

    public DockFlexType getConDockFlexFill() {
        return conDockFlexFill;
    }

    public void setConDockFlexFill(DockFlexType conDockFlexFill) {
        this.conDockFlexFill = conDockFlexFill;
    }
}
