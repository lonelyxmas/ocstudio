package com.ds.dsm.view.config.grid.field;

import com.ds.dsm.view.config.action.HiddenFieldAction;
import com.ds.dsm.view.config.grid.cell.GridCellService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;

@PageBar(pageCount = 100)
@RowHead(selMode = SelModeType.none, gridHandlerCaption = "排序|隐藏", rowHandlerWidth = "8em", rowNumbered = false)
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, menuClass = {HiddenFieldAction.class})
@GridAnnotation(customService = {GridCellService.class}, customMenu = {GridMenu.Reload})
public class HiddenFieldGridInfo {

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "列标题", readonly = true)
    private String caption;

    @CustomAnnotation(caption = "字段类型")
    private ComponentType componentType;

    @CustomAnnotation(hidden = true, pid = true)
    public String entityClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    public HiddenFieldGridInfo(FieldGridConfig esdField) {
        this.viewInstId = esdField.getDomainId();
        this.domainId = esdField.getDomainId();
        this.caption = esdField.getCaption();
        this.fieldname = esdField.getFieldname();
        this.entityClassName = esdField.getEntityClassName();
        this.sourceClassName = esdField.getSourceClassName();

        this.componentType = ComponentType.HiddenInput;

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }


    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }
}
