package com.ds.dsm.view.config.form;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.FieldFormGridInfo;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.ESDFieldConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/form/hidden/")
public class ViewFormHiddenFieldsService {


    @RequestMapping(method = RequestMethod.POST, value = "HiddenFieldList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "字段")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<FieldFormGridInfo>> getHiddenFieldList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldFormGridInfo>> cols = new ListResultModel();
        try {
            ViewEntityConfig config = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getSourceConfig().getMethodByName(methodName);
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) customMethodAPIBean.getView();
            List<String> fieldNames = customFormViewBean.getHiddenFieldNames();
            List<FieldFormConfig> fields = new ArrayList<>();
            for (String fieldname : fieldNames) {
                FieldFormConfig fieldFormConfig = (FieldFormConfig) customFormViewBean.getFieldConfigMap().get(fieldname);
                if (fieldFormConfig != null) {
                    fields.add(fieldFormConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldFormGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }

    @RequestMapping(method = RequestMethod.POST, value = "HiddenFieldTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<FormNavTree>> getHiddenFieldTree(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<FormNavTree>> result = new TreeListResultModel<>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) methodAPIBean.getView();
            List<ESDFieldConfig> esdFieldConfigs = new ArrayList<>();
            List<String> fieldNames = customFormViewBean.getHiddenFieldNames();
            for (String fieldname : fieldNames) {
                FieldFormConfig fieldFormConfig = (FieldFormConfig) customFormViewBean.getFieldConfigMap().get(fieldname);
                if (fieldFormConfig != null) {
                    esdFieldConfigs.add(fieldFormConfig);
                }
            }
            result = TreePageUtil.getTreeList(esdFieldConfigs, FormNavTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
