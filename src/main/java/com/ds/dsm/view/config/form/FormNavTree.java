package com.ds.dsm.view.config.form;

import com.ds.dsm.view.config.form.field.service.FieldItemsService;
import com.ds.dsm.view.config.form.field.service.FormModuleService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.dsm.view.field.FieldTreeConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, lazyLoad = true, dynDestory = true)
public class FormNavTree extends TreeListItem {


    @Pid
    String sourceClassName;
    @Pid
    String sourceMethodName;
    @Pid
    String viewInstId;
    @Pid
    String methodName;
    @Pid
    String domainId;
    @Pid
    String fieldname;

    @TreeItemAnnotation(customItems = FormNavItems.class)
    public FormNavTree(FormNavItems formNavItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = formNavItems.getName();
        this.bindClassName = formNavItems.getBindClass().getName();
        this.dynLoad = formNavItems.isDynLoad();
        this.dynDestory = formNavItems.isDynDestory();
        this.iniFold = formNavItems.isIniFold();
        this.imageClass = formNavItems.getImageClass();
        this.id = formNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(customItems = FormEventMenuItems.class)
    public FormNavTree(FormEventMenuItems formNavItems, String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        this.caption = formNavItems.getName();
        this.bindClassName = formNavItems.getBindClass().getName();
        this.dynLoad = formNavItems.isDynLoad();
        this.dynDestory = formNavItems.isDynDestory();
        this.iniFold = formNavItems.isIniFold();
        this.imageClass = formNavItems.getImageClass();
        this.id = formNavItems.getType() + "_" + sourceClassName + "_" + methodName + "_" + fieldname;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(customItems = FormViewConfigItems.class)
    public FormNavTree(FormViewConfigItems formNavItems, String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        this.caption = formNavItems.getName();
        this.bindClassName = formNavItems.getBindClass().getName();
        this.dynLoad = formNavItems.isDynLoad();
        this.dynDestory = formNavItems.isDynDestory();
        this.iniFold = formNavItems.isIniFold();
        this.imageClass = formNavItems.getImageClass();
        this.id = formNavItems.getType() + "_" + sourceClassName + "_" + methodName + "_" + fieldname;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(bindService = ViewFormNavService.class)
    public FormNavTree(CustomFormViewBean formViewBean) {
        this.caption = formViewBean.getName();
        this.imageClass = formViewBean.getImageClass();
        String methodName = formViewBean.getMethodName();
        this.setId(formViewBean.getSourceClassName() + "_" + methodName);
        this.domainId = formViewBean.getDomainId();
        this.viewInstId = formViewBean.getViewInstId();
        this.sourceClassName = formViewBean.getSourceClassName();
        this.methodName = methodName;

    }

    @TreeItemAnnotation(bindService = ViewFormFieldsService.class, dynDestory = true, lazyLoad = true)
    public FormNavTree(MethodConfig customMethodAPIBean) {
        this.caption = customMethodAPIBean.getName();
        this.imageClass = customMethodAPIBean.getImageClass();
        this.id = customMethodAPIBean.getMethodName() + "_" + sourceClassName + "_" + methodName;
        this.domainId = customMethodAPIBean.getDomainId();
        this.viewInstId = customMethodAPIBean.getViewInstId();
        this.sourceClassName = customMethodAPIBean.getSourceClassName();
        this.sourceMethodName = customMethodAPIBean.getMethodName();
        this.methodName = customMethodAPIBean.getMethodName();
    }


    @TreeItemAnnotation(bindService = FormModuleService.class)
    public FormNavTree(FieldModuleConfig fieldFormInfo, String sourceMethodName) {
        this.caption = fieldFormInfo.getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        this.imageClass = fieldFormInfo.getImageClass();
        this.id = fieldFormInfo.getSourceClassName() + "_" + fieldFormInfo.getMethodName() + "_" + fieldFormInfo.getFieldname();
        this.domainId = fieldFormInfo.getDomainId();
        this.viewInstId = fieldFormInfo.getDomainId();
        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = sourceMethodName;
        this.methodName = fieldFormInfo.getMethodName();
        this.fieldname = fieldFormInfo.getFieldname();

    }


    @TreeItemAnnotation(bindService = FieldItemsService.class)
    public FormNavTree(FieldTreeConfig fieldFormInfo, String methodName) {
        this.caption = fieldFormInfo.getAggConfig().getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }

        this.id = fieldFormInfo.getSourceClassName() + "_" + sourceMethodName + "_" + fieldFormInfo.getFieldname();
        this.domainId = fieldFormInfo.getDomainId();
        this.viewInstId = fieldFormInfo.getDomainId();
        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = methodName;
        this.fieldname = fieldFormInfo.getFieldname();
        this.methodName = methodName;

    }


    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
