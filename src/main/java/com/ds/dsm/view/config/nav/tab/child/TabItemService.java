package com.ds.dsm.view.config.nav.tab.child;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.NavTabsBaseViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.bean.nav.tab.TabItemBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tabs/child/")
public class TabItemService {


    @RequestMapping(method = RequestMethod.POST, value = "TabItemInfo")
    @FormViewAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, imageClass = "spafont spa-icon-rendermode", caption = "Tab页子项")
    @ResponseBody
    public ResultModel<TabItemInfoView> getTabItemInfo(String sourceClassName, String childTabId, String methodName, String domainId, String viewInstId) {
        ResultModel<TabItemInfoView> result = new ResultModel<TabItemInfoView>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            NavTabsViewBean tabsViewBean = null;
            if (customMethodAPIBean.getView() instanceof NavTabsBaseViewBean) {
                NavTabsBaseViewBean navTabsBaseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
                tabsViewBean = navTabsBaseViewBean.getTabsViewBean();
            } else {
                tabsViewBean = (NavTabsViewBean) customMethodAPIBean.getView();
            }
            TabItemBean childTabViewBean = tabsViewBean.getChildTabBean(childTabId);
            result.setData(new TabItemInfoView(childTabViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "TabItemChild")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> getTabItemChild(String sourceClassName, String serviceClassName, String childTabId, String methodName, String domainId, String id) {
        List<Object> objs = new ArrayList<>();
        try {

            MethodConfig methodConfig = null;
            if (serviceClassName != null) {
                ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(serviceClassName, domainId);
                methodConfig = apiClassConfig.getMethodByItem(CustomMenuItem.tabEditor);
            } else {
                ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
                MethodConfig customMethodAPIBean = apiClassConfig.getMethodByName(methodName);
                NavTabsViewBean tabsViewBean = null;
                if (customMethodAPIBean.getView() instanceof NavTabsBaseViewBean) {
                    NavTabsBaseViewBean navTabsBaseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
                    tabsViewBean = navTabsBaseViewBean.getTabsViewBean();
                } else {
                    tabsViewBean = (NavTabsViewBean) customMethodAPIBean.getView();
                }
                TabItemBean childTabViewBean = tabsViewBean.getChildTabBean(childTabId);
                methodConfig = childTabViewBean.getMethodConfig();
            }
            if (methodConfig != null) {
                objs.add(methodConfig);
                if (methodConfig.getView() != null) {
                    objs.add(methodConfig.getView());
                }
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }

        TreeListResultModel resultModel = TreePageUtil.getTreeList(objs, ViewConfigTree.class);
        return resultModel;
    }


    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateTabItem")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formSave)
    public @ResponseBody
    ResultModel<Boolean> updateTabItem(@RequestBody TabItemInfoView configView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(configView.getSourceClassName(), configView.getViewInstId());
            MethodConfig methodAPIBean = config.getMethodByName(configView.getMethodName());
            NavTabsViewBean customTabViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavTabsBaseViewBean baseViewBean = (NavTabsBaseViewBean) methodAPIBean.getView();
                customTabViewBean = baseViewBean.getTabsViewBean();
            } else if (methodAPIBean.getView() instanceof NavTabsViewBean) {
                customTabViewBean = (NavTabsViewBean) methodAPIBean.getView();
            }
            TabItemBean itemBean = customTabViewBean.getChildTabBean(configView.getChildTabId());

            if (itemBean != null) {
                BeanMap.create(itemBean).putAll(BeanMap.create(configView));
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

}
