package com.ds.dsm.view.config.form.field.module;

import com.ds.dsm.view.config.form.field.combo.service.ComboModuleService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.enums.AppendType;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.field.combo.ComboModuleFieldBean;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;

@PageBar(pageCount = 100)
@MenuBarMenu
@GridAnnotation(customService = {ComboModuleService.class}, event = CustomGridEvent.editor, customMenu = {GridMenu.Add, GridMenu.Reload, GridMenu.Delete})
public class FieldModuleGridInfo {

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String id;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;
    @CustomAnnotation(caption = "链接")
    String src;
    @CustomAnnotation(caption = "动态加载")
    Boolean dynLoad;
    @CustomAnnotation(caption = "嵌入方式")
    AppendType append;
    @CustomAnnotation(caption = "绑定服务")
    Class bindClass;


    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    public FieldModuleGridInfo(FieldFormConfig<ComboInputFieldBean, ComboModuleFieldBean> config) {

        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getSourceMethodName();
        this.viewClassName = config.getViewClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();
        ComboModuleFieldBean comboPopFieldBean = config.getComboConfig();
        this.dynLoad = comboPopFieldBean.isDynLoad();
        this.src = comboPopFieldBean.getSrc();
        this.append = comboPopFieldBean.getAppend();
        this.bindClass = comboPopFieldBean.getBindClass();
        if ( comboPopFieldBean.getBindClass() != null && ! comboPopFieldBean.getBindClass().equals(Void.class)) {
            this.bindClass =  comboPopFieldBean.getBindClass();
        }

    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public AppendType getAppend() {
        return append;
    }

    public void setAppend(AppendType append) {
        this.append = append;
    }

    public Class getBindClass() {
        return bindClass;
    }

    public void setBindClass(Class bindClass) {
        this.bindClass = bindClass;
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


}
