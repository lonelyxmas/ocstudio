package com.ds.dsm.view.config.grid.row;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.grid.GridRowHead;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/grid/")
public class GridRowHeaderService {


    @RequestMapping(method = RequestMethod.POST, value = "GridRowView")
    @FormViewAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, imageClass = "spafont spa-icon-rendermode", caption = "行头信息配置")
    @ResponseBody
    public ResultModel<GridRowHead> getGridRowView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<GridRowHead> result = new ResultModel<GridRowHead>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomGridViewBean gridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            result.setData(new GridRowHead(gridViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
