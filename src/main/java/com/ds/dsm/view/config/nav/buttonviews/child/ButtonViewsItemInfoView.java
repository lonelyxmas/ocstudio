package com.ds.dsm.view.config.nav.buttonviews.child;

import com.ds.dsm.manager.view.BuildViewMenu;
import com.ds.dsm.view.config.nav.tab.child.TabItemService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.nav.tab.TabItemBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.web.annotation.Pid;


@BottomBarMenu(menuClass = BuildViewMenu.class)
@FormAnnotation(
        bottombarMenu = {CustomFormMenu.Save}, customService = ButtonViewsItemService.class)
public class ButtonViewsItemInfoView {


    @Pid
    String id;
    @Pid
    String childTabId;

    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation(caption = "标题")
    String caption;

    @FieldAnnotation(colSpan = 2)
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;


    @CustomAnnotation(caption = "自动重载")
    Boolean autoReload;

    @CustomAnnotation(caption = "顺序")
    Integer index = 1;

    @CustomAnnotation(caption = "配置按钮")
    String optBtn;

    @CustomAnnotation(caption = "关闭按钮")
    Boolean closeBtn;

    @CustomAnnotation(caption = "弹出按钮")
    Boolean popBtn;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "绑定服务")
    Class bindService;

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;
    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    public ButtonViewsItemInfoView() {

    }

    public ButtonViewsItemInfoView(TabItemBean config) {
        this.viewInstId = config.getViewInstId();
        this.domainId = config.getDomainId();
        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getMethodName();
        this.closeBtn = config.getCloseBtn();
        this.id = config.getId();
        this.childTabId = config.getId();
        this.imageClass = config.getImageClass();
        this.caption = config.getCaption();
        this.autoReload = config.getAutoReload();
        this.index = config.getIndex();
        this.popBtn = config.getPopBtn();
        if (config.getBindService() != null && !config.getBindService().equals(Void.class)) {
            this.bindService = config.getBindService();
        }


    }

    public String getChildTabId() {
        return childTabId;
    }

    public void setChildTabId(String childTabId) {
        this.childTabId = childTabId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Boolean getAutoReload() {
        return autoReload;
    }

    public void setAutoReload(Boolean autoReload) {
        this.autoReload = autoReload;
    }



    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getOptBtn() {
        return optBtn;
    }

    public void setOptBtn(String optBtn) {
        this.optBtn = optBtn;
    }


    public Boolean getCloseBtn() {
        return closeBtn;
    }

    public void setCloseBtn(Boolean closeBtn) {
        this.closeBtn = closeBtn;
    }

    public Boolean getPopBtn() {
        return popBtn;
    }

    public void setPopBtn(Boolean popBtn) {
        this.popBtn = popBtn;
    }


    public Class getBindService() {
        return bindService;
    }

    public void setBindService(Class bindService) {
        this.bindService = bindService;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

}
