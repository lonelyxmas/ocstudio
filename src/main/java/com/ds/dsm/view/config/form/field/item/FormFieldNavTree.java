package com.ds.dsm.view.config.form.field.item;

import com.ds.dsm.view.config.form.field.combo.ComboWidgetNavItem;
import com.ds.dsm.view.config.form.field.service.FieldItemsService;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, lazyLoad = true, dynDestory = true)
public class FormFieldNavTree extends TreeListItem {


    @Pid
    String sourceClassName;
    @Pid
    String sourceMethodName;
    @Pid
    String viewInstId;
    @Pid
    String methodName;
    @Pid
    String domainId;
    @Pid
    String fieldname;

    @TreeItemAnnotation(customItems = FieldNavItem.class)
    public FormFieldNavTree(FieldNavItem aggFieldNavItem, String sourceClassName, String fieldname, String methodName, String domainId, String viewInstId) {
        this.caption = aggFieldNavItem.getName();
        this.bindClassName = aggFieldNavItem.getBindClass().getName();
        this.dynLoad = aggFieldNavItem.isDynLoad();
        this.dynDestory = aggFieldNavItem.isDynDestory();
        this.iniFold = aggFieldNavItem.isIniFold();
        this.imageClass = aggFieldNavItem.getImageClass();
        this.id = aggFieldNavItem.getType() + "_" + sourceClassName + "_" + methodName + "_" + fieldname;
        this.domainId = domainId;
        this.fieldname = fieldname;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;
    }


    @TreeItemAnnotation(customItems = ComboWidgetNavItem.class)
    public FormFieldNavTree(ComboWidgetNavItem formNavItems, String sourceMethodName, String sourceClassName, String fieldname, String methodName, String domainId, String viewInstId) {
        this.caption = formNavItems.getName();
        this.bindClassName = formNavItems.getBindClass().getName();
        this.dynLoad = formNavItems.isDynLoad();
        this.dynDestory = formNavItems.isDynDestory();
        this.iniFold = formNavItems.isIniFold();
        this.imageClass = formNavItems.getImageClass();
        this.id = formNavItems.getType() + "_" + sourceClassName + "_" + methodName + "_" + fieldname;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.fieldname = fieldname;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(customItems = FieldMenuItems.class)
    public FormFieldNavTree(FieldMenuItems formNavItems, String sourceMethodName, String sourceClassName, String fieldname, String methodName, String domainId, String viewInstId) {
        this.caption = formNavItems.getName();
        this.bindClassName = formNavItems.getBindClass().getName();
        this.dynLoad = formNavItems.isDynLoad();
        this.dynDestory = formNavItems.isDynDestory();
        this.iniFold = formNavItems.isIniFold();
        this.imageClass = formNavItems.getImageClass();
        this.id = formNavItems.getType() + "_" + sourceClassName + "_" + methodName + "_" + fieldname;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.fieldname = fieldname;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = FieldWidgetNavItem.class)
    public FormFieldNavTree(FieldWidgetNavItem formNavItems, String sourceMethodName, String sourceClassName, String fieldname, String methodName, String domainId, String viewInstId) {
        this.caption = formNavItems.getName();
        this.bindClassName = formNavItems.getBindClass().getName();
        this.dynLoad = formNavItems.isDynLoad();
        this.dynDestory = formNavItems.isDynDestory();
        this.iniFold = formNavItems.isIniFold();
        this.imageClass = formNavItems.getImageClass();
        this.id = formNavItems.getType() + "_" + sourceClassName + "_" + methodName + "_" + fieldname;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.fieldname = fieldname;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(bindService = FieldItemsService.class, iniFold = true)
    public FormFieldNavTree(FieldFormConfig fieldFormInfo, String sourceClassName, String sourceMethodName) {
        this.caption = fieldFormInfo.getAggConfig().getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        if (fieldFormInfo.getComboConfig() instanceof ComboBoxBean) {
            ComboBoxBean comboBoxBean = fieldFormInfo.getComboConfig();
            this.imageClass = comboBoxBean.getInputType().getImageClass();
        } else {
            this.imageClass = fieldFormInfo.getWidgetConfig().getComponentType().getImageClass();
        }

        this.domainId = fieldFormInfo.getDomainId();
        this.viewInstId = fieldFormInfo.getDomainId();
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.fieldname = fieldFormInfo.getFieldname();

    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
