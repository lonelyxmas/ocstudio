package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.form.field.service.FieldAggConfigService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.JavaEditorAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.toolbar.CodeEditorTools;
import com.ds.esd.dsm.aggregation.FieldAggConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu
@FormAnnotation(col = 2, customService = FieldAggConfigService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FieldAggConfigView {

    @Uid
    String id;

    @Pid
    String methodName;


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;



    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "标签名称", readonly = true)
    private String caption;

    @FieldAnnotation(required = true, colSpan = -1)
    @CustomListAnnotation(filter = "source.isAbsValue()")
    @CustomAnnotation(caption = "控件类型", readonly = true)
    private ComponentType componentType;

    @CustomAnnotation(caption = "主键", readonly = true)
    Boolean uid;

    @CustomAnnotation(caption = "禁用")
    Boolean disabled;

    @CustomAnnotation(caption = "只读")
    Boolean readonly;

    @CustomAnnotation(caption = "隐藏")
    Boolean hidden;

    @CustomAnnotation(caption = "是序列化")
    Boolean serialize;
    @CustomAnnotation(caption = "环境常量（pid）")
    Boolean pid;

    @CustomAnnotation(caption = "分隔符")
    Boolean split;
    @CustomAnnotation(caption = "默认展示字段")
    Boolean captionField;

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "枚举类")
    String enumClass;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "服务代理")
    String serviceClassName;
    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "当前值(value)")
    Object value;

    @JavaEditorAnnotation
    @ToolBarMenu(menuClasses = CodeEditorTools.class)
    @FieldAnnotation(colSpan = -1, rowHeight = "200")
    @CustomAnnotation(caption = "表达式")
    String expression;


    public FieldAggConfigView() {

    }

    public FieldAggConfigView(FieldFormConfig config) {
        FieldAggConfig fieldAggConfig = config.getAggConfig();
        BeanMap.create(this).putAll(BeanMap.create(fieldAggConfig));
        this.methodName = config.getMethodName();
        this.viewClassName= config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();

        this.domainId = config.getDomainId();

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }


    public Boolean getSerialize() {
        return serialize;
    }

    public void setSerialize(Boolean serialize) {
        this.serialize = serialize;
    }

    public Boolean getPid() {
        return pid;
    }

    public void setPid(Boolean pid) {
        this.pid = pid;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Boolean getSplit() {
        return split;
    }

    public void setSplit(Boolean split) {
        this.split = split;
    }

    public Boolean getCaptionField() {
        return captionField;
    }

    public void setCaptionField(Boolean captionField) {
        this.captionField = captionField;
    }

    public String getEnumClass() {
        return enumClass;
    }

    public void setEnumClass(String enumClass) {
        this.enumClass = enumClass;
    }

    public String getServiceClassName() {
        return serviceClassName;
    }

    public void setServiceClassName(String serviceClassName) {
        this.serviceClassName = serviceClassName;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getUid() {
        return uid;
    }

    public void setUid(Boolean uid) {
        this.uid = uid;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }


}
