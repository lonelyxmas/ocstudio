package com.ds.dsm.view.config.form.field.combo;

import com.ds.dsm.view.config.form.field.combo.pop.PopBindEntityService;
import com.ds.dsm.view.config.form.field.combo.service.ComboModuleService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboPopAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.enums.AppendType;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.field.combo.ComboModuleFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(col = 1, customService = ComboModuleService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ComboModuleView {
    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String id;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @Required
    @CustomAnnotation(caption = "字段名")
    String fieldname;

    @CustomAnnotation(caption = "组件类型", readonly = true)
    @CustomListAnnotation(filter = "source.isAbsValue()")
    ComponentType componentType = ComponentType.ComboInput;

    @CustomAnnotation(caption = "模块类型")
    @CustomListAnnotation(filter = "source.comboType.name=='module'")
    ComboInputType inputType = ComboInputType.module;

    @CustomAnnotation(caption = "自定义链接")
    String src;

    @CustomAnnotation(caption = "嵌入方式")
    AppendType append = AppendType.ref;
    @CustomAnnotation(caption = "动态加载")
    Boolean dynLoad;
    @CustomAnnotation(caption = "绑定服务")
    @ComboPopAnnotation(bindClass = PopBindEntityService.class)
    Class bindClass;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    public ComboModuleView() {

    }

    public ComboModuleView(String fieldname, ComboInputType inputType) {
        this.fieldname = fieldname;
        this.componentType = ComponentType.ComboInput;
        this.inputType = inputType;

    }

    public ComboModuleView(String fieldname, ComponentType componentType, ComboInputType inputType) {
        this.componentType = componentType;
        this.fieldname = fieldname;
        this.inputType = inputType;
    }

    public ComboModuleView(FieldFormConfig<ComboInputFieldBean, ComboModuleFieldBean> config) {
        this.methodName = config.getSourceMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();
        ComboModuleFieldBean comboPopFieldBean = config.getComboConfig();
        this.dynLoad = comboPopFieldBean.isDynLoad();
        this.src = comboPopFieldBean.getSrc();
        this.append = comboPopFieldBean.getAppend();
        this.bindClass = comboPopFieldBean.getBindClass();
        if (comboPopFieldBean.getBindClass() != null && !comboPopFieldBean.getBindClass().equals(Void.class)) {
            this.bindClass = comboPopFieldBean.getBindClass();
        }

    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public ComboInputType getInputType() {
        return inputType;
    }

    public void setInputType(ComboInputType inputType) {
        this.inputType = inputType;
    }


    public Class getBindClass() {
        return bindClass;
    }

    public void setBindClass(Class bindClass) {
        this.bindClass = bindClass;
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public AppendType getAppend() {
        return append;
    }

    public void setAppend(AppendType append) {
        this.append = append;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
