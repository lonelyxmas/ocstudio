package com.ds.dsm.view.config.gallery;


import com.ds.dsm.view.config.form.field.service.FormModuleService;
import com.ds.dsm.view.config.form.service.FormFieldService;
import com.ds.dsm.view.config.gallery.field.FieldGalleryInfo;
import com.ds.dsm.view.config.grid.ViewGridFieldService;
import com.ds.dsm.view.config.nav.gallery.GalleryNavItems;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.dsm.view.field.FieldGalleryConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, lazyLoad = true)
public class GalleryConfigTree extends TreeListItem {
    @Pid
    String sourceClassName;
    @Pid
    String viewInstId;
    @Pid
    String methodName;
    @Pid
    String sourceMethodName;
    @Pid
    String domainId;
    @Pid
    String fieldname;


    @TreeItemAnnotation(caption = "画廊导航配置", customItems = GalleryNavItems.class)
    public GalleryConfigTree(GalleryNavItems dsmType, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = dsmType.getName();
        this.imageClass = dsmType.getImageClass();
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.setId(dsmType.getType() + "_" + sourceClassName + "_" + methodName);
        this.methodName = methodName;
        this.sourceClassName = sourceClassName;

    }

    @TreeItemAnnotation(caption = "画廊配置", customItems = GalleryItems.class)
    public GalleryConfigTree(GalleryItems galleryItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = galleryItems.getName();
        this.imageClass = galleryItems.getImageClass();
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.id = galleryItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.methodName = methodName;
        this.sourceClassName = sourceClassName;

    }

    @TreeItemAnnotation(iniFold = false, caption = "动作事件", customItems = NavGalleryButtonItems.class)
    public GalleryConfigTree(NavGalleryButtonItems dsmType, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = dsmType.getName();
        this.imageClass = dsmType.getImageClass();
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.setId(dsmType.getType() + "_" + sourceClassName + "_" + methodName);
        this.methodName = methodName;
        this.sourceClassName = sourceClassName;

    }

    @TreeItemAnnotation(iniFold = false, caption = "画廊基础配置", customItems = GalleryConfigItems.class)
    public GalleryConfigTree(GalleryConfigItems dsmType, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = dsmType.getName();
        this.imageClass = dsmType.getImageClass();
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.setId(dsmType.getType() + "_" + sourceClassName + "_" + methodName);
        this.methodName = methodName;
        this.sourceClassName = sourceClassName;

    }


    @TreeItemAnnotation(bindService = ViewGridFieldService.class)
    public GalleryConfigTree(FieldGalleryConfig fieldGalleryInfo, String sourceClassName, String methodName) {
        this.caption = fieldGalleryInfo.getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldGalleryInfo.getFieldname();
        }

        this.id = fieldGalleryInfo.getFieldname() + "_" + sourceClassName + "_" + methodName;
        this.domainId = fieldGalleryInfo.getDomainId();
        this.viewInstId = fieldGalleryInfo.getDomainId();
        this.sourceClassName = fieldGalleryInfo.getSourceClassName();
        this.sourceMethodName = methodName;
        this.methodName = methodName;
        this.fieldname = fieldGalleryInfo.getFieldname();
    }
    @TreeItemAnnotation(bindService = FormModuleService.class)
    public GalleryConfigTree(FieldModuleConfig fieldFormInfo, String sourceMethodName) {
        this.caption = fieldFormInfo.getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        this.imageClass = fieldFormInfo.getImageClass();
        this.id = fieldFormInfo.getSourceClassName() + "_" + sourceMethodName + "_" + fieldFormInfo.getFieldname();
        this.domainId = fieldFormInfo.getDomainId();
        this.viewInstId = fieldFormInfo.getDomainId();
        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = sourceMethodName;
        this.fieldname = fieldFormInfo.getFieldname();
    }


    @TreeItemAnnotation(bindService = FormFieldService.class)
    public GalleryConfigTree(FieldFormConfig fieldFormInfo, String sourceMethodName) {
        this.caption = fieldFormInfo.getAggConfig().getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        if (fieldFormInfo.getWidgetConfig() instanceof ComboBoxBean) {
            ComboBoxBean comboBoxBean = (ComboBoxBean) fieldFormInfo.getWidgetConfig();
            this.imageClass = comboBoxBean.getInputType().getImageClass();
        } else {
            this.imageClass = fieldFormInfo.getWidgetConfig().getComponentType().getImageClass();
        }

        this.domainId = fieldFormInfo.getDomainId();
        this.viewInstId = fieldFormInfo.getDomainId();
        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = sourceMethodName;
        this.fieldname = fieldFormInfo.getFieldname();

    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }
}
