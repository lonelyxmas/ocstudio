package com.ds.dsm.view.config.custom.panel;

import com.ds.dsm.view.config.custom.panel.div.DivConfigItems;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class PanelConfigTree extends TreeListItem {

    @Pid
    String sourceClassName;
    @Pid
    String sourceMethodName;
    @Pid
    String viewInstId;
    @Pid
    String rootClassName;
    @Pid
    String methodName;
    @Pid
    String groupId;
    @Pid
    String domainId;


    @TreeItemAnnotation(customItems = PanelConfigItems.class)
    public PanelConfigTree(PanelConfigItems groupNavItems, String sourceClassName, String groupId, String methodName, String domainId, String viewInstId) {
        this.caption = groupNavItems.getName();
        this.bindClassName = groupNavItems.getBindClass().getName();
        this.dynLoad = groupNavItems.isDynLoad();
        this.dynDestory = groupNavItems.isDynDestory();
        this.iniFold = groupNavItems.isIniFold();
        this.imageClass = groupNavItems.getImageClass();
        this.id = groupNavItems.getType() + "_" + groupId;
        this.domainId = domainId;
        this.groupId = groupId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = DivConfigItems.class)
    public PanelConfigTree(DivConfigItems divConfig, String sourceClassName, String groupId, String methodName, String domainId, String viewInstId) {
        this.caption = divConfig.getName();
        this.bindClassName = divConfig.getBindClass().getName();
        this.dynLoad = divConfig.isDynLoad();
        this.dynDestory = divConfig.isDynDestory();
        this.iniFold = divConfig.isIniFold();
        this.imageClass = divConfig.getImageClass();
        this.id = divConfig.getType() + "_" + groupId;
        this.domainId = domainId;
        this.groupId = groupId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getRootClassName() {
        return rootClassName;
    }

    public void setRootClassName(String rootClassName) {
        this.rootClassName = rootClassName;
    }
}
