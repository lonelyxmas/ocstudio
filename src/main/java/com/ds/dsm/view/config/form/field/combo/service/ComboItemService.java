package com.ds.dsm.view.config.form.field.combo.service;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.dsm.view.config.form.field.combo.ComboItemGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.field.combo.ComboListBoxFieldBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/item/")
@MethodChinaName(cname = "字典项", imageClass = "spafont spa-icon-c-comboinput")

public class ComboItemService {


    @RequestMapping(method = RequestMethod.POST, value = "ListItemList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "字典项", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.index)
    @ResponseBody
    public ListResultModel<List<ComboItemGridView>> getItemList(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ListResultModel<List<ComboItemGridView>> result = new ListResultModel<List<ComboItemGridView>>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            ComboBoxBean comboBoxBean = formInfo.getComboConfig();
            List<TreeListItem> items = new ArrayList<>();

            if (comboBoxBean instanceof ComboListBoxFieldBean) {
                ComboListBoxFieldBean listBoxFieldBean = (ComboListBoxFieldBean) formInfo.getComboConfig();
                items = listBoxFieldBean.getListFieldBean().getItems();
            }
            if (items != null) {
                result = PageUtil.getDefaultPageList(items, ComboItemGridView.class);
            }


        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
