package com.ds.dsm.view.config.menu.toolbar.custom;

import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.CustomMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.web.annotation.Pid;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = CustomToolBarService.class)
public class CustomToolGridView {
    @Pid
    String domainId;
    @Pid
    String methodName;
    @Pid
    String sourceClassName;
    @CustomAnnotation(caption = "按钮名称")
    String menucaption;

    @CustomAnnotation(caption = "类型", uid = true)
    String type;

    @CustomAnnotation(caption = "位置")
    String tag;

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;

    @CustomAnnotation(caption = "表达式")
    String expression;

    @CustomAnnotation(caption = "类型")
    ComboInputType itemType = ComboInputType.button;


    public CustomToolGridView(CustomMenu customMenu, String sourceClassName, String methodName, String domainId) {
        this.type = customMenu.type();
        this.menucaption = customMenu.caption();
        this.expression = customMenu.expression();
        this.imageClass = customMenu.imageClass();
        this.itemType = customMenu.itemType();
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;
        this.domainId = domainId;


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMenucaption() {
        return menucaption;
    }

    public void setMenucaption(String menucaption) {
        this.menucaption = menucaption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public ComboInputType getItemType() {
        return itemType;
    }

    public void setItemType(ComboInputType itemType) {
        this.itemType = itemType;
    }

}
