package com.ds.dsm.view.config.form.field.service;

import com.alibaba.fastjson.JSONObject;
import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.FieldAggConfigView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.FieldAggConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.jds.core.esb.util.OgnlUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/agg/config/form/field/")
public class FieldAggConfigService {

    @RequestMapping(method = RequestMethod.POST, value = "FieldAggConfig")
    @FormViewAnnotation(autoSave = true)
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "基础信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<FieldAggConfigView> getFieldAggConfig(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<FieldAggConfigView> result = new ResultModel<FieldAggConfigView>();
        try {
         //   ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            AggEntityConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(sourceClassName,domainId);
            MethodConfig customMethodAPIBean = esdClassConfig.getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new FieldAggConfigView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "清空配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "clearFieldForm")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clearFieldForm(String entityClassName, String fieldname, String domainId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (entityClassName != null && !entityClassName.equals("")) {
                AggEntityConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(entityClassName, domainId);
                esdClassConfig.getAllFieldMap().remove(fieldname);
                DSMFactory.getInstance().getAggregationManager().updateAggEntityConfig(esdClassConfig);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "编辑字段配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFieldForm")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateFieldForm(@RequestBody FieldAggConfigView config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getViewClassName();
            if (className != null && !className.equals("")) {
                AggEntityConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(className, config.getDomainId());
                FieldAggConfig fieldAggConfig = esdClassConfig.getFieldByName(config.getFieldname());
                if (fieldAggConfig != null) {
                    OgnlUtil.setProperties(BeanMap.create(config), fieldAggConfig,false,false);
                } else {
                    String json = JSONObject.toJSONString(config);
                    JSONObject.parseObject(json, FieldAggConfig.class);
                    esdClassConfig.getAllFieldMap().put(config.getFieldname(), fieldAggConfig);
                }

                DSMFactory.getInstance().getAggregationManager().updateAggEntityConfig(esdClassConfig);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
