package com.ds.dsm.view.config.grid.row.rowcmd;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.view.config.grid.row.rowcmd.custom.CustomGridRowMenuGridView;
import com.ds.dsm.view.config.grid.row.rowcmd.menuclass.GridRowMenuGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.GridRowCmdBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/view/config/grid/rowmenu/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "行按钮列表", imageClass = "spafont spa-icon-c-databinder")
public class GridRowMenuMetaView {
    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;

    @RequestMapping(method = RequestMethod.POST, value = "CustomGridRowMenu")
    @ModuleAnnotation(caption = "常用按钮", imageClass = "spafont spa-icon-c-toolbar", dock = Dock.fill)
    @CustomAnnotation(index = 0)
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<CustomGridRowMenuGridView>> getCustomGridRowMenu(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<CustomGridRowMenuGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            GridRowCmdBean barMenuBean = customGridViewBean.getRowCmdBean();
            if (barMenuBean != null) {
                resultModel = PageUtil.getDefaultPageList(barMenuBean.getRowMenu(), CustomGridRowMenuGridView.class);
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "GridRowMenuClass")
    @ModuleAnnotation(caption = "扩展菜单", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @CustomAnnotation(index = 1)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<GridRowMenuGridView>> getGridRowMenuClass(String domainId, String methodName, String sourceClassName) {
        ListResultModel<List<GridRowMenuGridView>> result = new ListResultModel();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            GridRowCmdBean barMenuBean = customGridViewBean.getRowCmdBean();
            List<ESDClass> esdClassList = new ArrayList<>();
            if (barMenuBean != null && barMenuBean.getMenuClass() != null) {
                Class[] clazzs = barMenuBean.getMenuClass();
                for (Class clazz : clazzs) {
                    esdClassList.add(DSMFactory.getInstance().getClassManager().getAggEntityByName(clazz.getName(), domainId, false));
                }
            }

            result = PageUtil.getDefaultPageList(esdClassList, GridRowMenuGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
