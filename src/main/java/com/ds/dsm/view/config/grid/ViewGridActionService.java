package com.ds.dsm.view.config.grid;

        import com.ds.config.TreeListResultModel;
        import com.ds.esd.custom.api.annotation.APIEventAnnotation;
        import com.ds.esd.custom.enums.CustomMenuItem;
        import com.ds.esd.custom.module.annotation.ModuleAnnotation;
        import com.ds.esd.util.TreePageUtil;
        import org.springframework.stereotype.Controller;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestMethod;
        import org.springframework.web.bind.annotation.ResponseBody;

        import java.util.Arrays;
        import java.util.List;

        @Controller
        @RequestMapping("/dsm/view/config/grid/group/")
        public class ViewGridActionService {


        @RequestMapping(method = RequestMethod.POST, value = "GridEventMenuItems")
        @ModuleAnnotation(dynLoad = true, caption = "动作事件", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
        @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
        @ResponseBody
        public TreeListResultModel<List<GridNavTree>> getGridEventMenuItems(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<GridNavTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(GridEventMenuItems.values()), GridNavTree.class);
        return result;
        }

        }
