package com.ds.dsm.view.config.tree.contextmenu;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.RightContextMenuBean;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.jds.core.esb.util.OgnlUtil;
import com.ds.web.util.AnnotationUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/tree/contextmenu/")
@MethodChinaName(cname = "右键菜单按钮", imageClass = "spafont spa-icon-c-gallery")

public class ContextTreeService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "右键菜单操作按钮")
    @RequestMapping(method = RequestMethod.POST, value = "ContextMenuInfo")
    @NavGroupViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, caption = "右键菜单操作按钮", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<ContextTreeMenuNav> getContextMenuInfo(String domainId, String sourceClassName, String methodName, String childViewId) {
        ResultModel<ContextTreeMenuNav> result = new ResultModel<ContextTreeMenuNav>();
        return result;
    }


    @MethodChinaName(cname = "保存右键菜单操作按钮")
    @RequestMapping(value = {"saveContextMenuInfo"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveContextMenuInfo(@RequestBody ContextTreeMenuConfigView menuConfigView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ApiClassConfig tableConfig = null;
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(menuConfigView.getSourceClassName(), domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(menuConfigView.getMethodName());
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean formViewBean = (NavBaseViewBean) methodAPIBean.getView();

                ChildTreeViewBean childTreeViewBean = formViewBean.getTreeView().getChildTreeBean(menuConfigView.getChildViewId());
                RightContextMenuBean barMenuBean = childTreeViewBean.getContextMenuBean();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(RightContextMenu.class, new RightContextMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                childTreeViewBean.setContextMenuBean(barMenuBean);
            } else {
                CustomTreeViewBean formViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                ChildTreeViewBean childTreeViewBean = formViewBean.getChildTreeBean(menuConfigView.getChildViewId());
                RightContextMenuBean barMenuBean = childTreeViewBean.getContextMenuBean();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(RightContextMenu.class, new RightContextMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                childTreeViewBean.setContextMenuBean(barMenuBean);
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;

    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
