package com.ds.dsm.view.config.gallery;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.grid.nav.GridEventConfigItems;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/gallery/event/")
public class GalleryEventService {

    @RequestMapping(method = RequestMethod.POST, value = "GalleryEvent")
    @ModuleAnnotation(dynLoad = true, caption = "表格信息")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<GalleryConfigTree>> getGalleryEvent(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<GalleryConfigTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(NavGalleryButtonItems.values()), GalleryConfigTree.class);
        return result;
    }

}
