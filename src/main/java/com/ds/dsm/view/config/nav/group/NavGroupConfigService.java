package com.ds.dsm.view.config.nav.group;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.nav.CustomNavViewBean;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/group/")
public class NavGroupConfigService {


    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateGroupBase")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateGroupBase(@RequestBody CustomNavViewBean configView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {

            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(configView.getSourceClassName(),configView.getViewInstId());
            MethodConfig customMethodAPIBean = config.getMethodByName(configView.getMethodName());
            NavGroupViewBean navGroupViewBean = (NavGroupViewBean) customMethodAPIBean.getView();
            BeanMap.create(navGroupViewBean).putAll(BeanMap.create(configView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "清空列表配置")
    @RequestMapping(method = RequestMethod.POST, value = "clear")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clear(String sourceClassName, String methodName,String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            NavGroupViewBean navGroupViewBean = (NavGroupViewBean) customMethodAPIBean.getView();
            customMethodAPIBean.setDataBean(null);
            customMethodAPIBean.setView(navGroupViewBean);

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

}
