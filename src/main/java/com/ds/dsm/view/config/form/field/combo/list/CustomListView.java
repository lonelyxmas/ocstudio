package com.ds.dsm.view.config.form.field.combo.list;

import com.ds.config.ListResultModel;
import com.ds.dsm.view.config.form.field.combo.ComboItemGridView;
import com.ds.dsm.view.config.form.field.combo.service.ComboItemService;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.field.CustomListBean;
import com.ds.esd.custom.field.RadioBoxFieldBean;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.field.combo.ComboListBoxFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.toolbar.CodeEditorTools;
import com.ds.esd.dsm.view.field.FieldFormConfig;

import java.util.List;

@BottomBarMenu
@FormAnnotation(col = 2, customService = CustomListService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class CustomListView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    @FieldAnnotation()
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;


    @CustomAnnotation(caption = "动态装载")
    Boolean dynLoad;


    @TextEditorAnnotation
    @ToolBarMenu(menuClasses = CodeEditorTools.class)
    @FieldAnnotation(colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "动态过滤")
    String filter;

    @JavaEditorAnnotation
    @ToolBarMenu(menuClasses = CodeEditorTools.class)
    @FieldAnnotation(colSpan = -1, rowHeight = "80")
    @CustomAnnotation(caption = "子项表达式")
    String itemsExpression;

    @CustomAnnotation(caption = "绑定服务")
    @ComboPopAnnotation(bindClass = ListBindEntityService.class)
    Class bindClass;


    @FieldAnnotation(colSpan = -1, haslabel = false, rowHeight = "300")
    @ComboModuleAnnotation(bindClass = ComboItemService.class)
    ListResultModel<List<ComboItemGridView>> items;


    public CustomListView() {

    }

    public CustomListView(FieldFormConfig<ComboInputFieldBean, ComboBoxBean> config) {
        ComboBoxBean comboConfig = config.getComboConfig();
        CustomListBean listFieldBean = null;
        if (comboConfig instanceof ComboListBoxFieldBean) {
            listFieldBean = ((ComboListBoxFieldBean) comboConfig).getListFieldBean();
        } else if (comboConfig instanceof RadioBoxFieldBean) {
            listFieldBean = ((RadioBoxFieldBean) comboConfig).getListFieldBean();
        }

        this.bindClass = listFieldBean.getBindClass();
        this.filter = listFieldBean.getFilter();
        this.itemsExpression = listFieldBean.getItemsExpression();
        this.dynLoad = listFieldBean.getDynLoad();
        this.methodName = config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();
        if ( listFieldBean.getBindClass() != null && ! listFieldBean.getBindClass().equals(Void.class)) {
            this.bindClass =  listFieldBean.getBindClass();
        }

    }

    public Class getBindClass() {
        return bindClass;
    }

    public void setBindClass(Class bindClass) {
        this.bindClass = bindClass;
    }

    public String getItemsExpression() {
        return itemsExpression;
    }

    public void setItemsExpression(String itemsExpression) {
        this.itemsExpression = itemsExpression;
    }

    public ListResultModel<List<ComboItemGridView>> getItems() {
        return items;
    }

    public void setItems(ListResultModel<List<ComboItemGridView>> items) {
        this.items = items;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }


    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
