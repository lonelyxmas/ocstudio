package com.ds.dsm.view.config.form.field.event;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.enums.FieldEvent;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.custom.tree.enums.TreeMenu;
import com.ds.esd.custom.tree.enums.TreeRowMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RequestMapping(path = "/dsm/view/config/form/field/event/context/")
public class FieldContextMenuService {


    @MethodChinaName(cname = "监听事件")
    @RequestMapping(method = RequestMethod.POST, value = "FieldContextMenus")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-statusbutton", caption = "监听事件")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<FieldEventView>> getFieldContextMenus(String sourceClassName, String viewInstId, String sourceMethodName, String domainId, String fieldname) {
        ListResultModel<List<FieldEventView>> resultModel = new ListResultModel<>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);
            Set<FieldEvent> events = formInfo.getFieldBean().getEvent();
            List<FieldEventView> views = new ArrayList<>();
            for (FieldEvent gridEvent : events) {
                views.add(new FieldEventView(gridEvent, formInfo));
            }
            resultModel = PageUtil.getDefaultPageList(views);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "字段事件")
    @RequestMapping("FieldContextMenuTree")
    @DialogAnnotation( width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<FieldContextMenuPopTree>> getFieldContextMenuTree(String sourceClassName, String methodName, String feildname, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<FieldContextMenuPopTree>> model = new TreeListResultModel<>();
        List<FieldContextMenuPopTree> popTrees = new ArrayList<>();
        try {
            FieldContextMenuPopTree menuPopTree = new FieldContextMenuPopTree("treeMenu", "字段按钮");
            for (TreeMenu menu : TreeMenu.values()) {
                FieldContextMenuPopTree item = new FieldContextMenuPopTree(esdsearchpattern, menu);
                item.addTagVar("sourceClassName", sourceClassName);
                item.addTagVar("methodName", methodName);
                item.addTagVar("viewInstId", viewInstId);
                item.addTagVar("feildname", feildname);
                menuPopTree.addChild(item);
            }
            popTrees.add(menuPopTree);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }
        model.setData(popTrees);
        return model;
    }


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "字段按钮")
    @RequestMapping("ContextMenuTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<FieldContextMenuPopTree>> getContextMenuTree(String sourceClassName, String methodName, String feildname, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<FieldContextMenuPopTree>> model = new TreeListResultModel<>();
        List<FieldContextMenuPopTree> popTrees = new ArrayList<>();
        try {
            FieldContextMenuPopTree menuPopTree = new FieldContextMenuPopTree("fieldmenu", "字段按钮");
            for (TreeRowMenu menu : TreeRowMenu.values()) {
                FieldContextMenuPopTree item = new FieldContextMenuPopTree(esdsearchpattern, menu);
                item.addTagVar("sourceClassName", sourceClassName);
                item.addTagVar("methodName", methodName);
                item.addTagVar("viewInstId", viewInstId);
                item.addTagVar("feildname", feildname);
                menuPopTree.addChild(item);
            }
            popTrees.add(menuPopTree);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addContextMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addContextMenu(String sourceClassName, String methodName, String ContextMenuTree, String feildname, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) customMethodAPIBean.getView();
            if (ContextMenuTree != null && !ContextMenuTree.equals("")) {
                String[] menuIds = StringUtility.split(ContextMenuTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        FieldFormConfig formInfo = (FieldFormConfig) customFormViewBean.getFieldByName(feildname);
                        Set<FieldEvent> events = formInfo.getFieldBean().getEvent();
                    }
                }
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig.getSourceConfig());
        } catch (
                JDSException e)

        {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delContextMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delContextMenu(String sourceClassName, String methodName, String type, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomTreeViewBean customTreeViewBean = ((NavTreeViewBean) customMethodAPIBean.getView()).getTreeViewBean();
            String[] menuIds = StringUtility.split(type, ";");
            for (String menuId : menuIds) {
                TreeMenu treeMenu = TreeMenu.fromType(menuId);
                if (treeMenu != null) {
                    customTreeViewBean.getCustomMenu().remove(TreeMenu.valueOf(menuId));
                }
                TreeRowMenu rowMenu = TreeRowMenu.fromType(menuId);
                if (rowMenu != null) {
                    customTreeViewBean.getRowCmdBean().getRowMenu().remove(TreeRowMenu.valueOf(menuId));

                }

            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig.getSourceConfig());
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
