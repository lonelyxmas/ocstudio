package com.ds.dsm.view.config.nav.group.item;

import com.ds.dsm.view.config.nav.action.NavModuleItemColAction;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.bean.nav.group.GroupItemBean;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;
import com.ds.web.annotation.Pid;

@PageBar(pageCount = 100)
@RowHead(selMode = SelModeType.none, gridHandlerCaption = "排序|隐藏", rowHandlerWidth = "8em", rowNumbered = false)
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, menuClass = {NavModuleItemColAction.class})
@BottomBarMenu
@GridAnnotation(customService = {GroupItemService.class}, editable = true, bottombarMenu = {GridMenu.Reload, GridMenu.SubmitForm})
public class GroupItemGrid {

    @Pid
    String viewInstId;

    @Pid
    String domainId;

    @Pid
    String groupName;

    @Pid
    String sourceClassName;

    @Pid
    String sourceMethodName;

    @Pid
    String entityClassName;

    @Pid
    String fieleName;


    @CustomAnnotation(caption = "方法名", readonly = true, uid = true)
    private String methodName;


    @CustomAnnotation(caption = "中文名")
    private String caption;


    @CustomAnnotation(caption = "视图类型")
    private ModuleViewType viewType;

    @CustomAnnotation(caption = "默认折叠")
    Boolean iniFold;

    @CustomAnnotation(caption = "延迟注入")
    Boolean lazyAppend;


    @CustomAnnotation(caption = "URL", readonly = true)
    private String url;


    public GroupItemGrid(GroupItemBean groupItemBean) {
        this.viewInstId = groupItemBean.getViewInstId();
        this.domainId = groupItemBean.getDomainId();

        this.methodName = groupItemBean.getMethodName();
        this.iniFold = groupItemBean.getIniFold();
        this.lazyAppend = groupItemBean.getLazyAppend();
        this.groupName = groupItemBean.getId();
        this.entityClassName = groupItemBean.getEntityClassName();
        this.sourceMethodName = groupItemBean.getSourceMethodName();
        this.sourceClassName = groupItemBean.getSourceClassName();
        CustomView customView = groupItemBean.getMethodConfig().getView();
        viewType = customView.getModuleViewType();
        this.caption = groupItemBean.getCaption();
        this.sourceClassName = groupItemBean.getSourceClassName();
        if (caption == null || caption.equals("")) {
            caption = methodName;
        }
        this.fieleName = groupItemBean.getMethodConfig().getFieldName();
        this.url = groupItemBean.getMethodConfig().getUrl();
    }

    public String getFieleName() {
        return fieleName;
    }

    public void setFieleName(String fieleName) {
        this.fieleName = fieleName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Boolean getIniFold() {
        return iniFold;
    }

    public void setIniFold(Boolean iniFold) {
        this.iniFold = iniFold;
    }

    public Boolean getLazyAppend() {
        return lazyAppend;
    }

    public void setLazyAppend(Boolean lazyAppend) {
        this.lazyAppend = lazyAppend;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public ModuleViewType getViewType() {
        return viewType;
    }

    public void setViewType(ModuleViewType viewType) {
        this.viewType = viewType;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
