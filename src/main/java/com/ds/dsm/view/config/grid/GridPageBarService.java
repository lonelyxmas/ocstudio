package com.ds.dsm.view.config.grid;

import com.alibaba.fastjson.JSONObject;
import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.PageBarView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.PageBarBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/grid/")
public class GridPageBarService {


    @RequestMapping(method = RequestMethod.POST, value = "PageBarView")
    @FormViewAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(imageClass = "spafont spa-icon-rendermode", caption = "分页栏配置")
    @ResponseBody
    public ResultModel<PageBarView> getPageBarView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<PageBarView> result = new ResultModel<PageBarView>();

        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomViewBean customViewBean = methodAPIBean.getView();

            if (customViewBean instanceof CustomGridViewBean) {
                result.setData(new PageBarView((CustomGridViewBean) customViewBean));
            } else if (customViewBean instanceof CustomGalleryViewBean) {
                result.setData(new PageBarView((CustomGalleryViewBean) customViewBean));
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "编辑配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updatePageBar")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updatePageBar(@RequestBody PageBarView config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getViewClassName();
            if (className != null && !className.equals("")) {
                String json = JSONObject.toJSONString(config);
                PageBarBean pageBarBean = JSONObject.parseObject(json, PageBarBean.class);
                ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(config.getSourceClassName(), config.getDomainId());
                MethodConfig customMethodAPIBean = classConfig.getMethodByName(config.getSourceMethodName());
                CustomViewBean customViewBean = customMethodAPIBean.getView();
                if (customViewBean instanceof CustomGridViewBean) {
                    ((CustomGridViewBean) customViewBean).setPageBar(pageBarBean);
                } else if (customViewBean instanceof CustomGalleryViewBean) {
                    ((CustomGalleryViewBean) customViewBean).setPageBar(pageBarBean);
                }
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

}
