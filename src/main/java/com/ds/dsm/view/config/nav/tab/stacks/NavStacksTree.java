package com.ds.dsm.view.config.nav.tab.stacks;

import com.ds.dsm.view.config.form.field.service.FormModuleService;
import com.ds.dsm.view.config.service.ViewLayoutConfigService;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.bean.nav.stacks.NavStacksViewBean;
import com.ds.esd.custom.layout.CustomLayoutItemBean;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class NavStacksTree extends TreeListItem {

    @Pid
    String sourceClassName;
    @Pid
    String sourceMethodName;
    @Pid
    String viewInstId;
    @Pid
    String methodName;
    @Pid
    String domainId;
    @Pid
    String fieldname;


    public NavStacksTree(NavStacksViewBean navStacksViewBean) {
        this.caption = "Tab页配置";
        this.setImageClass(ModuleViewType.NavStacksConfig.getImageClass());
        this.setId("DSMStacksRoot_" + navStacksViewBean.getMethodName());
        this.domainId = navStacksViewBean.getDomainId();
        this.viewInstId = navStacksViewBean.getViewInstId();
        this.sourceClassName = navStacksViewBean.getSourceClassName();
        this.sourceMethodName = navStacksViewBean.getMethodName();

    }


    @TreeItemAnnotation(bindService = ViewLayoutConfigService.class)
    public NavStacksTree(CustomLayoutItemBean layoutItemBean, String methodName, String sourceMethodName, String domainId, String viewInstId) {
        this.caption = "Layout配置";
        this.imageClass = ModuleViewType.NavStacksConfig.getImageClass();
        this.id = ("DSMStacksRoot_" + methodName);
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceMethodName;
        this.sourceMethodName = methodName;

    }

    @TreeItemAnnotation(bindService = FormModuleService.class)
    public NavStacksTree(FieldModuleConfig fieldFormInfo, String sourceMethodName) {
        this.caption = fieldFormInfo.getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        this.imageClass = fieldFormInfo.getImageClass();
        this.id = fieldFormInfo.getSourceClassName() + "_" + sourceMethodName + "_" + fieldFormInfo.getFieldname();
        this.domainId = fieldFormInfo.getDomainId();
        this.viewInstId = fieldFormInfo.getDomainId();
        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = sourceMethodName;
        this.fieldname = fieldFormInfo.getFieldname();
    }

    public NavStacksTree(FieldFormConfig fieldFormInfo, String sourceClassName, String sourceMethodName, String methodName) {
        this.caption = fieldFormInfo.getAggConfig().getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        if (fieldFormInfo.getWidgetConfig() instanceof ComboBoxBean) {
            ComboBoxBean comboBoxBean = (ComboBoxBean) fieldFormInfo.getWidgetConfig();
            this.imageClass = comboBoxBean.getInputType().getImageClass();
            this.setEuClassName("dsm.view.config.form.field.combo.Field" + comboBoxBean.getInputType().getType());
        } else {
            this.imageClass = fieldFormInfo.getWidgetConfig().getComponentType().getImageClass();
            this.setEuClassName("dsm.view.config.form.field.Field" + fieldFormInfo.getWidgetConfig().getComponentType().getType());
        }

        this.id = fieldFormInfo.getSourceClassName() + "_" + sourceMethodName + "_" + fieldFormInfo.getFieldname();
        this.domainId = fieldFormInfo.getDomainId();
        this.viewInstId = fieldFormInfo.getDomainId();
        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = sourceMethodName;
        this.fieldname = fieldFormInfo.getFieldname();

    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
