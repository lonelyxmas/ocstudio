package com.ds.dsm.view.config.tree.contextmenu;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.view.config.tree.contextmenu.custom.CustomTreeContextMenuGridView;
import com.ds.dsm.view.config.tree.contextmenu.menuclass.ContextTreeMenuGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.RightContextMenuBean;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/view/config/tree/contextmenu/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "按钮配置", imageClass = "spafont spa-icon-c-databinder")
public class ContextTreeMenuMetaView {
    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;

    @RequestMapping(method = RequestMethod.POST, value = "CustomContextMenu")
    @ModuleAnnotation(caption = "常用按钮", imageClass = "spafont spa-icon-c-toolbar", dock = Dock.fill)
    @CustomAnnotation(index = 0)
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<CustomTreeContextMenuGridView>> getCustomContextMenu(String sourceClassName, String methodName, String domainId, String childViewId) {
        ListResultModel<List<CustomTreeContextMenuGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }
            ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
            resultModel = PageUtil.getDefaultPageList(childTreeViewBean.getContextMenu(), CustomTreeContextMenuGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "MenuClass")
    @ModuleAnnotation(caption = "扩展菜单", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @CustomAnnotation(index = 1)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<ContextTreeMenuGridView>> getMenuClass(String domainId, String methodName, String sourceClassName, String childViewId) {
        ListResultModel<List<ContextTreeMenuGridView>> result = new ListResultModel();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }
            ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
            List<ESDClass> esdClassList = new ArrayList<>();
            RightContextMenuBean barMenuBean = childTreeViewBean.getContextMenuBean();
            if (barMenuBean != null && barMenuBean.getMenuClass() != null) {
                Class[] clazzs = barMenuBean.getMenuClass();
                for (Class clazz : clazzs) {
                    esdClassList.add(DSMFactory.getInstance().getClassManager().getAggEntityByName(clazz.getName(), domainId, false));
                }
            }

            result = PageUtil.getDefaultPageList(esdClassList, ContextTreeMenuGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
