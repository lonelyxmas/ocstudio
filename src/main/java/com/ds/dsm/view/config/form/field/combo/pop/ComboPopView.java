package com.ds.dsm.view.config.form.field.combo.pop;

import com.ds.dsm.view.config.form.field.combo.service.ComboPopService;
import com.ds.dsm.view.config.form.field.module.ModuleBindEntityService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.ComboPopAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.field.combo.ComboPopFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.CommandBtnType;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 2, customService = ComboPopService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ComboPopView {
    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String id;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;
    @CustomAnnotation(caption = "父级ID", hidden = true)
    String parentID;
    @CustomAnnotation(caption = "是否缓存")
    Boolean cachePopWnd;
    @CustomAnnotation(caption = "动态加载")
    Boolean dynLoad;

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CommandBtnType.class)
    @CustomAnnotation(caption = "操作按钮")
    String commandBtn;
    @CustomAnnotation(caption = "宽度")
    String width;
    @CustomAnnotation(caption = "高度")
    String height;
    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "链接")
    String src;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "绑定服务")
    @ComboPopAnnotation(bindClass = PopBindEntityService.class)
    Class bindClass;


    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    public ComboPopView() {

    }

    public ComboPopView(FieldFormConfig<ComboInputFieldBean, ComboPopFieldBean> config) {
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getSourceMethodName();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();
        this.parentID = config.getId();
        ComboPopFieldBean comboColorFieldBean = config.getComboConfig();
        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(comboColorFieldBean));

        if ( comboColorFieldBean.getBindClass() != null && ! comboColorFieldBean.getBindClass().equals(Void.class)) {
            this.bindClass =  comboColorFieldBean.getBindClass();
        }
    }

    public Class getBindClass() {
        return bindClass;
    }

    public void setBindClass(Class bindClass) {
        this.bindClass = bindClass;
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public Boolean getCachePopWnd() {
        return cachePopWnd;
    }

    public void setCachePopWnd(Boolean cachePopWnd) {
        this.cachePopWnd = cachePopWnd;
    }

    public String getCommandBtn() {
        return commandBtn;
    }

    public void setCommandBtn(String commandBtn) {
        this.commandBtn = commandBtn;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
