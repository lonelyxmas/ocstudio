package com.ds.dsm.view.config.form.field.item;

import com.ds.dsm.view.config.form.field.contextmenu.ContextFieldService;
import com.ds.dsm.view.config.form.field.event.FieldEventService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum FieldMenuItems implements TreeItem {
    FieldRightist("右键菜单", "spafont spa-icon-c-menu", ContextFieldService.class, false, false, false),
    FieldEventList("常用事件", "spafont spa-icon-event", FieldEventService.class, false, false, false);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    FieldMenuItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
