package com.ds.dsm.view.config.form.field.combo;

import com.ds.dsm.view.config.form.field.combo.service.ComboListService;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.ListFieldBean;
import com.ds.esd.custom.field.RadioBoxFieldBean;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.field.combo.ComboListBoxFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.*;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 2, customService = ComboListService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ComboListView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;
    @CustomAnnotation(caption = "宽度")
    String width;
    @CustomAnnotation(caption = "高度")
    String height;
    @CustomAnnotation(caption = "标签值")
    String labelCaption;
    @CustomAnnotation(caption = "最大高度")
    Integer maxHeight;
    @CustomAnnotation(caption = "选择方式")
    SelModeType selMode;
    @CustomAnnotation(caption = "边框类型")
    BorderType borderType;
    @CustomAnnotation(caption = "Ctrl多选")
    Boolean noCtrlKey;

    @CustomAnnotation(caption = "行集合")
    ItemRow itemRow;

    @CustomAnnotation(caption = "按钮")
    String optBtn;

    @CustomAnnotation(caption = "命令按钮")
    String tagCmds;

    @CustomAnnotation(caption = "按钮位置")
    TagCmdsAlign tagCmdsAlign;


    public ComboListView() {

    }

    public ComboListView(FieldFormConfig<ComboInputFieldBean, ?> config) {
        ComboBoxBean comboBoxBean = config.getComboConfig();
        ListFieldBean listFieldBean = null;
        if (comboBoxBean instanceof ComboListBoxFieldBean) {
            listFieldBean = ((ComboListBoxFieldBean) comboBoxBean).getListBean();
        } else if (comboBoxBean instanceof RadioBoxFieldBean) {
            listFieldBean = ((RadioBoxFieldBean) comboBoxBean).getListBean();
        }

        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(listFieldBean));
        this.methodName = config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }

    public BorderType getBorderType() {
        return borderType;
    }

    public void setBorderType(BorderType borderType) {
        this.borderType = borderType;
    }

    public Boolean getNoCtrlKey() {
        return noCtrlKey;
    }

    public void setNoCtrlKey(Boolean noCtrlKey) {
        this.noCtrlKey = noCtrlKey;
    }


    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Integer getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(Integer maxHeight) {
        this.maxHeight = maxHeight;
    }

    public ItemRow getItemRow() {
        return itemRow;
    }

    public void setItemRow(ItemRow itemRow) {
        this.itemRow = itemRow;
    }

    public String getOptBtn() {
        return optBtn;
    }

    public void setOptBtn(String optBtn) {
        this.optBtn = optBtn;
    }

    public String getTagCmds() {
        return tagCmds;
    }

    public void setTagCmds(String tagCmds) {
        this.tagCmds = tagCmds;
    }

    public TagCmdsAlign getTagCmdsAlign() {
        return tagCmdsAlign;
    }

    public void setTagCmdsAlign(TagCmdsAlign tagCmdsAlign) {
        this.tagCmdsAlign = tagCmdsAlign;
    }


    public String getLabelCaption() {
        return labelCaption;
    }

    public void setLabelCaption(String labelCaption) {
        this.labelCaption = labelCaption;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
