package com.ds.dsm.view.config.form.field.combo;

import com.ds.dsm.view.config.form.field.combo.service.ComboImageService;
import com.ds.dsm.view.config.form.field.combo.service.ComboListBoxService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.field.combo.ComboListBoxFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 2, customService = ComboListBoxService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ComboListBoxView {
    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String id;
    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;

    @CustomAnnotation(caption = "返回KEY")
    String listKey;
    @CustomAnnotation(caption = "拖动显示图标")
    String dropImageClass;
    @CustomAnnotation(caption = "拖动占位宽度")
    Integer dropListWidth;
    @CustomAnnotation(caption = "拖动占位高度")
    Integer dropListHeight;


    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    public ComboListBoxView() {

    }

    public ComboListBoxView(FieldFormConfig<ComboInputFieldBean,ComboListBoxFieldBean> config) {
        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();
        ComboListBoxFieldBean inputFieldBean = config.getComboConfig();
        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(inputFieldBean));
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getListKey() {
        return listKey;
    }

    public void setListKey(String listKey) {
        this.listKey = listKey;
    }

    public String getDropImageClass() {
        return dropImageClass;
    }

    public void setDropImageClass(String dropImageClass) {
        this.dropImageClass = dropImageClass;
    }

    public Integer getDropListWidth() {
        return dropListWidth;
    }

    public void setDropListWidth(Integer dropListWidth) {
        this.dropListWidth = dropListWidth;
    }

    public Integer getDropListHeight() {
        return dropListHeight;
    }

    public void setDropListHeight(Integer dropListHeight) {
        this.dropListHeight = dropListHeight;
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }


    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
