package com.ds.dsm.view.config.grid;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.grid.field.FieldGridInfo;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavButtonViewsViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;

import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping(path = "/dsm/view/config/grid/")
@MethodChinaName(cname = "列表配置")
@ButtonViewsAnnotation(barLocation = BarLocationType.top, sideBarStatus = SideBarStatusType.expand, barSize = "3em")
public class GridConfigView {


    @CustomAnnotation(pid = true, hidden = true)
    private String className;

    @CustomAnnotation(uid = true, hidden = true)
    private String methodName;

    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;


    public GridConfigView() {

    }


    @RequestMapping(method = RequestMethod.POST, value = "GridView")
    @NavGroupViewAnnotation(reSetUrl = "clear", saveUrl = "updateGridView", autoSave = true)
    @ModuleAnnotation(dock = Dock.fill, caption = "列表配置", imageClass = "spafont spa-icon-project")
    @CustomAnnotation( index = 0)
    @ResponseBody
    public ResultModel<GridInfoGroup> getGridInfoGroup(String sourceClassName, String methodName,String domainId, String viewInstId) {
        ResultModel<GridInfoGroup> result = new ResultModel<GridInfoGroup>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldGridList")
    @GridViewAnnotation
    @ModuleAnnotation( imageClass = "spafont spa-icon-c-comboinput", caption = "列配置")
    @CustomAnnotation( index = 1)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<FieldGridInfo>> getFieldGrids(String sourceClassName, String methodName,String domainId, String viewInstId) {
        ListResultModel<List<FieldGridInfo>> cols = new ListResultModel();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig classConfig = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = classConfig.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            List<String> fieldNames = customGridViewBean.getDisplayFieldNames();
            List<FieldGridConfig> fields = new ArrayList<>();
            for (String fieldname : fieldNames) {
                FieldGridConfig fieldGridConfig = (FieldGridConfig) customGridViewBean.getFieldConfigMap().get(fieldname);
                if (fieldGridConfig != null) {
                    fieldGridConfig.setSourceClassName(sourceClassName);
                    fields.add(fieldGridConfig);
                }
            }

            cols = PageUtil.getDefaultPageList(fields, FieldGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldHiddenGridList")
    @GridViewAnnotation
    @ModuleAnnotation( imageClass = "spafont spa-icon-c-comboinput", caption = "隐藏列")
    @CustomAnnotation( index = 2)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<FieldGridInfo>> geFieldHiddenGrids(String sourceClassName, String methodName,String domainId, String viewInstId) {
        ListResultModel<List<FieldGridInfo>> cols = new ListResultModel();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,viewInstId);
            ApiClassConfig classConfig = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = classConfig.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            List<String> fieldNames = customGridViewBean.getHiddenFieldNames();
            List<FieldGridConfig> fields = new ArrayList<>();
            for (String fieldname : fieldNames) {
                FieldGridConfig fieldGridConfig = (FieldGridConfig) customGridViewBean.getFieldConfigMap().get(fieldname);
                if (fieldGridConfig != null) {
                    fieldGridConfig.setSourceClassName(sourceClassName);

                    fields.add(fieldGridConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @RequestMapping(method = RequestMethod.POST, value = "GridDataGroup")
   @DynLoadAnnotation
    @NavButtonViewsViewAnnotation
    @ModuleAnnotation( caption = "接口配置", imageClass = "spafont spa-icon-values", dock = Dock.fill)
    @CustomAnnotation( index = 3)
    @ResponseBody
    public ResultModel<GridDataGroup> getGridDataGroup(String sourceClassName, String methodName,String domainId, String viewInstId) {
        ResultModel<GridDataGroup> result = new ResultModel<GridDataGroup>();
        return result;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}
