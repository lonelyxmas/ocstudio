package com.ds.dsm.view.config.tree;

import com.ds.dsm.manager.view.BuildViewMenu;
import com.ds.dsm.view.config.nav.tree.NavTreeConfigService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu(menuClass = BuildViewMenu.class)
@FormAnnotation(col = 2, bottombarMenu = {CustomFormMenu.Save}, customService = NavTreeConfigService.class)
public class TreeBaseView {


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    @CustomAnnotation(caption = "显示名称")
    String caption;


    @CustomAnnotation(caption = "选中方式")
    SelModeType selMode;


    @CustomAnnotation(caption = "分组显示")
    Boolean group;


    @CustomAnnotation(caption = "占位符")
    Boolean togglePlaceholder;


    @CustomAnnotation(caption = "帮助工具栏")
    Boolean heplBar;

    @CustomAnnotation(caption = "单击展开")
    Boolean singleOpen;


    @CustomAnnotation(caption = "默认折叠")
    Boolean iniFold;


    @CustomAnnotation(caption = "装载消息")
    Boolean animCollapse;

    @CustomAnnotation(caption = "动态销毁")
    Boolean dynDestory;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "配置按钮")
    String optBtn;


    public TreeBaseView() {

    }

    public TreeBaseView(CustomTreeViewBean treeConfig) {
        this.viewInstId = treeConfig.getViewInstId();
        this.domainId = treeConfig.getDomainId();
        this.sourceClassName = treeConfig.getSourceClassName();
        this.methodName = treeConfig.getMethodName();
        this.animCollapse = treeConfig.getAnimCollapse();
        this.iniFold = treeConfig.getIniFold();
        this.dynDestory = treeConfig.getDynDestory();
        this.togglePlaceholder = treeConfig.getTogglePlaceholder();
        this.heplBar = treeConfig.getHeplBar();

        this.group = treeConfig.getGroup();
        this.optBtn = treeConfig.getOptBtn();
        this.singleOpen = treeConfig.getSingleOpen();
        this.selMode = treeConfig.getSelMode();
        this.caption = treeConfig.getCaption();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public Boolean getIniFold() {
        return iniFold;
    }

    public void setIniFold(Boolean iniFold) {
        this.iniFold = iniFold;
    }

    public Boolean getAnimCollapse() {
        return animCollapse;
    }

    public void setAnimCollapse(Boolean animCollapse) {
        this.animCollapse = animCollapse;
    }

    public Boolean getDynDestory() {
        return dynDestory;
    }

    public void setDynDestory(Boolean dynDestory) {
        this.dynDestory = dynDestory;
    }

    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }

    public Boolean getTogglePlaceholder() {
        return togglePlaceholder;
    }

    public void setTogglePlaceholder(Boolean togglePlaceholder) {
        this.togglePlaceholder = togglePlaceholder;
    }


    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }

    public String getOptBtn() {
        return optBtn;
    }

    public void setOptBtn(String optBtn) {
        this.optBtn = optBtn;
    }


    public Boolean getSingleOpen() {
        return singleOpen;
    }

    public void setSingleOpen(Boolean singleOpen) {
        this.singleOpen = singleOpen;
    }


    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
