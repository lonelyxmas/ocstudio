package com.ds.dsm.view.config.nav.group;

import com.ds.dsm.view.config.nav.group.item.GroupItemMethodService;
import com.ds.dsm.view.config.service.ViewLayoutConfigService;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.bean.nav.group.GroupItemBean;
import com.ds.esd.custom.layout.CustomLayoutItemBean;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class NavGroupTree extends TreeListItem {

    @Pid
    String sourceClassName;
    @Pid
    String sourceMethodName;
    @Pid
    String entityClassName;
    @Pid
    String viewInstId;
    @Pid
    String methodName;
    @Pid
    String domainId;
    @Pid
    String groupId;
    @Pid
    String fieldname;


    @TreeItemAnnotation(customItems = NavGroupItems.class)
    public NavGroupTree(NavGroupItems groupNavItems, String sourceClassName, String sourceMethodName, String domainId, String viewInstId) {
        this.caption = groupNavItems.getName();
        this.bindClassName = groupNavItems.getBindClass().getName();
        this.dynLoad = groupNavItems.isDynLoad();
        this.dynDestory = groupNavItems.isDynDestory();
        this.iniFold = groupNavItems.isIniFold();
        this.imageClass = groupNavItems.getImageClass();
        this.id = groupNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;

    }


    @TreeItemAnnotation(bindService = ViewLayoutConfigService.class)
    public NavGroupTree(CustomLayoutItemBean layoutItemBean, String methodName, String sourceClassName, String sourceMethodName, String domainId, String viewInstId) {
        this.caption = "Layout配置";
        this.imageClass = ModuleViewType.NavGroupConfig.getImageClass();
        this.id = ("DSMGroupTabsRoot_" + methodName);
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;
        this.sourceMethodName = sourceMethodName;

    }



    @TreeItemAnnotation(bindService = GroupItemMethodService.class)
    public NavGroupTree(GroupItemBean groupItemBean) {
        this.caption = groupItemBean.getCaption();
        this.groupId = groupItemBean.getId();
        if (caption == null || caption.equals("")) {
            caption = groupItemBean.getMethodName();
        }
        this.methodName = groupItemBean.getMethodName();
        this.imageClass = groupItemBean.getImageClass();

        this.id = groupItemBean.getSourceClassName() + "_" + sourceMethodName + "_" + groupItemBean.getMethodName();
        this.domainId = groupItemBean.getDomainId();
        this.groupId = groupItemBean.getId();
        this.viewInstId = groupItemBean.getViewInstId();
        this.methodName = groupItemBean.getMethodName();
        this.sourceMethodName = groupItemBean.getSourceMethodName();
        this.entityClassName = groupItemBean.getEntityClassName();
        this.sourceClassName = groupItemBean.getSourceClassName();
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

}
