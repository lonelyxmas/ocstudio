package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.form.field.combo.service.ComboDateService;
import com.ds.dsm.view.config.form.field.service.FormElementService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.ElementFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 2, customService =FormElementService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FieldElementView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;

    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;

    @CustomAnnotation(caption = "节点类型")
    public String nodeName;

    @CustomAnnotation(caption = "属性")
    public String attributes;

    @CustomAnnotation(caption = "是否可选中")
    Boolean selectable;
    @CustomAnnotation(caption = "宽")
    String width;
    @CustomAnnotation(caption = "高")
    String height;
    @CustomAnnotation(caption = "tab选中")
    Boolean selectabl;


    @CustomAnnotation(caption = "html")
    String html;

    @CustomAnnotation(caption = "TAB顺序")
    Integer tabindex;


    public FieldElementView() {

    }

    public FieldElementView(FieldFormConfig<ElementFieldBean,?> config) {
        ElementFieldBean inputFieldBean = config.getWidgetConfig();
        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(inputFieldBean));
        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Boolean getSelectabl() {
        return selectabl;
    }

    public void setSelectabl(Boolean selectabl) {
        this.selectabl = selectabl;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public Integer getTabindex() {
        return tabindex;
    }

    public void setTabindex(Integer tabindex) {
        this.tabindex = tabindex;
    }
}
