package com.ds.dsm.view.config.form;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/form/")
public class ViewFormService {


//    @RequestMapping(method = RequestMethod.POST, value = "FormViewItems")
//    @ModuleAnnotation(dynLoad = true, caption = "动作事件", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
//    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
//    @ResponseBody
//    public TreeListResultModel<List<FormNavTree>> getFormViewItems(String domainId, String sourceClassName, String methodName, String viewInstId) {
//        TreeListResultModel<List<FormNavTree>> result = new TreeListResultModel<>();
//        result = TreePageUtil.getTreeList(Arrays.asList(FormViewConfigItems.values()), FormNavTree.class);
//        return result;
//    }



    @RequestMapping(method = RequestMethod.POST, value = "FormBaseView")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-values", caption = "基础信息")
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<FormBaseView> getFormBaseView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<FormBaseView> result = new ResultModel<FormBaseView>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
            result.setData(new FormBaseView(formViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
