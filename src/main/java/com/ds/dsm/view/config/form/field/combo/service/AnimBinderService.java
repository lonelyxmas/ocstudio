package com.ds.dsm.view.config.form.field.combo.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.combo.AnimBinderView;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.AnimBinderBean;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/anim/")
public class AnimBinderService {
    @RequestMapping(method = RequestMethod.POST, value = "AnimBinder")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "动画绑定", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<AnimBinderView> getAnimBinder(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<AnimBinderView> result = new ResultModel<AnimBinderView>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new AnimBinderView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "updateAnimBinder")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateAnimBinder(@RequestBody AnimBinderView comboListView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(comboListView.getSourceClassName(), comboListView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(comboListView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboListView.getFieldname());
            AnimBinderBean widgetBean = (AnimBinderBean) formInfo.getWidgetConfig();
            Map<String, Object> configMap = BeanMap.create(widgetBean);
            configMap.putAll(BeanMap.create(comboListView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetAnimBinder")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetAnimBinder(@RequestBody AnimBinderView comboListView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(comboListView.getSourceClassName(), comboListView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(comboListView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboListView.getFieldname());
            formInfo.setWidgetConfig(null);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
