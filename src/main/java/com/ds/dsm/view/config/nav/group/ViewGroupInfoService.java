package com.ds.dsm.view.config.nav.group;

import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/group/")
public class ViewGroupInfoService {


    @RequestMapping(method = RequestMethod.POST, value = "GroupInfoGroup")
    @NavGroupViewAnnotation(reSetUrl = "clear", saveUrl = "updateGridView", autoSave = true)
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, caption = "列表配置",  imageClass = "spafont spa-icon-project")
    @ResponseBody
    public ResultModel<NavGroupInfoGroup> getGroupInfoGroup(String sourceClassName, String sourceMethodName, String domainId, String viewInstId) {
        ResultModel<NavGroupInfoGroup> result = new ResultModel<NavGroupInfoGroup>();
        return result;
    }


}
