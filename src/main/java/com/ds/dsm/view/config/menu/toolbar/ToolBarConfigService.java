package com.ds.dsm.view.config.menu.toolbar;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.ToolBarMenuBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.jds.core.esb.util.OgnlUtil;
import com.ds.web.util.AnnotationUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/menu/toolbar/")
@MethodChinaName(cname = "工具栏菜单管理", imageClass = "spafont spa-icon-c-gallery")

public class ToolBarConfigService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "菜单配置")
    @RequestMapping(method = RequestMethod.POST, value = "ToolMenuInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "菜单配置", imageClass = "spafont spa-icon-c-grid")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ToolBarNav> getToolMenuInfo(String domainId, String sourceClassName, String methodName) {
        ResultModel<ToolBarNav> result = new ResultModel<ToolBarNav>();
        return result;
    }


    @MethodChinaName(cname = "保存控制接口")
    @RequestMapping(value = {"saveToolMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveToolMenu(@RequestBody ToolBarConfigView menuConfigView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ApiClassConfig tableConfig = null;
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(menuConfigView.getSourceClassName(), domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(menuConfigView.getMethodName());

            ToolBarMenuBean barMenuBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                if (customTreeViewBean != null) {
                    barMenuBean = customTreeViewBean.getToolBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(ToolBarMenu.class, new ToolBarMenuBean());
                    }
                    OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                    customTreeViewBean.setToolBar(barMenuBean);

                } else {
                    barMenuBean = viewBean.getToolBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(ToolBarMenu.class, new ToolBarMenuBean());
                    }
                    OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                    viewBean.setToolBar(barMenuBean);
                }

            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                CustomTreeViewBean treeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                barMenuBean = treeViewBean.getToolBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(ToolBarMenu.class, new ToolBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                treeViewBean.setToolBar(barMenuBean);
            } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                barMenuBean = formViewBean.getToolBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(ToolBarMenu.class, new ToolBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                formViewBean.setToolBar(barMenuBean);

            } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                barMenuBean = customGridViewBean.getToolBar();

                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(ToolBarMenu.class, new ToolBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                customGridViewBean.setToolBar(barMenuBean);
            }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();

                barMenuBean = customGalleryViewBean.getToolBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(ToolBarMenu.class, new ToolBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                customGalleryViewBean.setToolBar(barMenuBean);
            } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                barMenuBean = customGalleryViewBean.getToolBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(ToolBarMenu.class, new ToolBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                customGalleryViewBean.setToolBar(barMenuBean);
            } else if (methodAPIBean.getView() instanceof NavTabsViewBean) {
                NavTabsViewBean navTabsViewBean = (NavTabsViewBean) methodAPIBean.getView();
                barMenuBean = navTabsViewBean.getToolBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(ToolBarMenu.class, new ToolBarMenuBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                navTabsViewBean.setToolBar(barMenuBean);
            }



            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;

    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
