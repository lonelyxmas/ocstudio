package com.ds.dsm.view.config.nav;

import com.ds.enums.IconEnumstype;
import com.ds.esd.custom.bean.enums.ModuleViewType;

public enum ModuleViewServiceType implements IconEnumstype {
    GridConfig("列表", "spafont spa-icon-c-grid", ModuleViewType.GridConfig),
    GalleryConfig("画廊", "xui-icon-dialog", ModuleViewType.GalleryConfig),
    FormConfig("表单", "xui-icon-dialog", ModuleViewType.FormConfig),
    NavGroupConfig("导航", "xui-icon-dialog", ModuleViewType.NavGroupConfig),
    DYNConfig("动态", "xui-icon-dialog", ModuleViewType.DYNConfig),
    none("无界面", "spafont spa-icon-empty", ModuleViewType.none),
    UploadConfig("文件上传", "spafont spa-icon-empty", ModuleViewType.UploadConfig),
    NavGalleryConfig("画廊导航", "xui-icon-dialog", ModuleViewType.NavGalleryConfig),
    LayoutConfig("布局", "spafont spa-icon-c-layout", ModuleViewType.LayoutConfig),
    NavMenuBarConfig("菜单导航", "spafont spa-icon-c-tabs", ModuleViewType.NavMenuBarConfig),
    NavFoldingTreeConfig("Tree导航", "spafont spa-icon-shukongjian", ModuleViewType.NavFoldingTreeConfig),
    NavFoldingTabsConfig("NavTabs导航", "spafont spa-icon-shukongjian", ModuleViewType.NavFoldingTabsConfig),
    NavButtonViewsConfig("按钮导航", "spafont spa-icon-c-buttonviews", ModuleViewType.NavButtonViewsConfig),
    NavStacksConfig("按钮导航", "spafont spa-icon-c-stacks", ModuleViewType.NavStacksConfig),
    NavTabsConfig("TAB导航", "spafont spa-icon-c-tabs", ModuleViewType.NavTabsConfig),
    NavTreeConfig("Tree导航", "spafont spa-icon-shukongjian", ModuleViewType.NavTreeConfig),
    TreeConfig("树形", "spafont spa-icon-shukongjian", ModuleViewType.TreeConfig),
    PopTreeConfig("弹出树", "spafont spa-icon-shukongjian", ModuleViewType.PopTreeConfig);


    private final String name;
    private final String imageClass;
    ModuleViewType moduleViewType;

    ModuleViewServiceType(String name, String imageClass, ModuleViewType moduleViewType) {
        this.name = name;
        this.imageClass = imageClass;
        this.moduleViewType = moduleViewType;
    }

    public static ModuleViewServiceType getServiceTypeByModuleType(ModuleViewType viewType) {
        for (ModuleViewServiceType serviceType : ModuleViewServiceType.values()) {
            if (serviceType.moduleViewType.equals(viewType)) {
                return serviceType;
            }
        }
        return none;

    }


    public ModuleViewType getModuleViewType() {
        return moduleViewType;
    }

    public void setModuleViewType(ModuleViewType moduleViewType) {
        this.moduleViewType = moduleViewType;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
