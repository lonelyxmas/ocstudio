package com.ds.dsm.view.config.nav.action;

import com.ds.common.JDSException;
import com.ds.common.util.ClassUtility;
import com.ds.config.ListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.api.enums.ResponsePathEnum;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.component.Component;
import com.ds.esd.tool.ui.component.form.ComboInputComponent;
import com.ds.esd.tool.ui.component.form.ComboInputProperties;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.event.enums.ComboInputEventEnum;
import com.ds.esd.tool.ui.module.ModuleComponent;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/custom/")
@MethodChinaName(cname = "通用服务")
public class CustomServiceService {


    @RequestMapping(value = "onChange.dyn")
    @APIEventAnnotation(customResponseData = ResponsePathEnum.expression, customRequestData = RequestPathEnum.form, autoRun = true, bindMenu = {CustomMenuItem.formReSet})
    @ResponseBody
    public ListResultModel<List<Component>> onChange(String viewInstId, String sourceClassName, ModuleComponent moduleComponent) {
        ListResultModel<List<Component>> listResultModel = new ListResultModel();
        List<Component> components = new ArrayList<>();
        List<ComboInputComponent> comboInputComponents = new ArrayList<>();
        List<ESDClass> apiConfigs = new ArrayList<>();
        try {
            try {
                apiConfigs = DSMFactory.getInstance().getViewManager().findAPIConfig(sourceClassName, viewInstId);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            ComboInputComponent<ComboInputProperties, ComboInputEventEnum> inputComponent = (ComboInputComponent) moduleComponent.getRealModuleComponent().findComponentByAlias("serviceClassName");
            List items = inputComponent.getProperties().getItems();
            if (items != null && items.size() > 0) {
                inputComponent.getProperties().getItems().clear();
            }
            Object value = inputComponent.getProperties().getValue();
            if (value != null && !value.equals("")) {
                inputComponent.getProperties().setCaption(value.toString());
            }

            for (ESDClass apiConfig : apiConfigs) {
                try {
                    Class clazz = ClassUtility.loadClass(apiConfig.getClassName());
                    if (clazz != null) {
                        inputComponent.getProperties().addItem(new TreeListItem(apiConfig.getClassName(), apiConfig.getName()));
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
            comboInputComponents.add(inputComponent);
            components.addAll(comboInputComponents);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        listResultModel.setData(components);
        return listResultModel;
    }


}
