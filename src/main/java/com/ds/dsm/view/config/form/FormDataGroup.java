package com.ds.dsm.view.config.form;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.aggregation.api.method.APIMethodBaseFormView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.form.CustomFormDataBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/form/")
@ButtonViewsAnnotation(barLocation = BarLocationType.top, sideBarStatus = SideBarStatusType.expand, barSize = "3em")
public class FormDataGroup {
    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;
    @CustomAnnotation(uid = true, hidden = true)
    private String methodName;

    public FormDataGroup() {
    }

    @MethodChinaName(cname = "接口信息")
    @RequestMapping(method = RequestMethod.POST, value = "APIMethodBaseView")
    @FormViewAnnotation()
    @ModuleAnnotation(caption = "接口信息", imageClass = "spafont spa-icon-c-webapi", dock = Dock.top)
    @UIAnnotation(height = "150")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<APIMethodBaseFormView> getAPIMethodBaseView(String viewInstId, String methodName, String domainId, String sourceClassName) {
        ResultModel<APIMethodBaseFormView> result = new ResultModel<APIMethodBaseFormView>();
        try {
            ViewEntityConfig esdClassConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId);
            MethodConfig apiCallBean = esdClassConfig.getSourceConfig().getMethodByName(methodName);
            result.setData(new APIMethodBaseFormView(apiCallBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "视图绑定配置")
    @RequestMapping(method = RequestMethod.POST, value = "FormDataView")
    @FormViewAnnotation(reSetUrl = "clearData", saveUrl = "updateFormData")
    @ModuleAnnotation(imageClass = "spafont spa-icon-values", caption = "视图绑定配置", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<FormDataView> getFormDataView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<FormDataView> result = new ResultModel<FormDataView>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId);
           // DSMFactory.getInstance().getViewManager().updateViewEntityConfig(classConfig);
            MethodConfig methodAPIBean = classConfig.getSourceConfig().getMethodByName(methodName);
            CustomFormDataBean formViewBean = (CustomFormDataBean) methodAPIBean.getDataBean();
            result.setData(new FormDataView(formViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}
