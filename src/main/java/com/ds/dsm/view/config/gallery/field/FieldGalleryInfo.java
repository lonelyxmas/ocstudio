package com.ds.dsm.view.config.gallery.field;

import com.ds.dsm.view.config.action.FieldAction;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.grid.GridColItemBean;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.view.field.FieldGalleryConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;

@PageBar(pageCount = 100)
@RowHead(selMode = SelModeType.none, gridHandlerCaption = "排序|隐藏", rowHandlerWidth = "8em", rowNumbered = false)
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, menuClass = {FieldAction.class})
@BottomBarMenu
@GridAnnotation(customService = {GalleryItemService.class}, editable = true, customMenu = {GridMenu.Reload, GridMenu.SubmitForm})
public class FieldGalleryInfo {

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "列标题")
    private String title;

    @CustomAnnotation(caption = "隐藏")
    Boolean colHidden;

    @CustomAnnotation(caption = "自动延伸")
    Boolean flexSize;

    @CustomAnnotation(caption = "可变列宽")
    Boolean colResizer;

    @CustomAnnotation(caption = "列可编辑")
    Boolean editable;

    @CustomAnnotation(caption = "宽度")
    String width;

    @CustomAnnotation(caption = "字段类型")
    public ComboInputType inputType;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    public FieldGalleryInfo() {

    }

    public FieldGalleryInfo(FieldGalleryConfig esdField) {
        this.viewInstId = esdField.getDomainId();
        this.domainId = esdField.getDomainId();
        this.title = esdField.getCaption();
        this.colHidden = esdField.getColHidden();

        this.fieldname = esdField.getFieldname();
        this.viewClassName = esdField.getViewClassName();
        this.methodName=esdField.getMethodName();
        this.sourceClassName = esdField.getSourceClassName();


    }

    public Boolean getColHidden() {
        return colHidden;
    }

    public void setColHidden(Boolean colHidden) {
        this.colHidden = colHidden;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public Boolean getFlexSize() {
        return flexSize;
    }

    public void setFlexSize(Boolean flexSize) {
        this.flexSize = flexSize;
    }

    public Boolean getColResizer() {
        return colResizer;
    }

    public void setColResizer(Boolean colResizer) {
        this.colResizer = colResizer;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


    public ComboInputType getInputType() {
        return inputType;
    }

    public void setInputType(ComboInputType inputType) {
        this.inputType = inputType;
    }
}
