package com.ds.dsm.view.config.form.field.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.FieldSVGPagerView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/svgpaper/")
@MethodChinaName(cname = "SVG画布", imageClass = "spafont spa-icon-c-comboinput")

public class FormSVGPaperService {


    @RequestMapping(method = RequestMethod.POST, value = "FieldSVGPagerInfo")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "SVG画布", dock = Dock.top)
    @UIAnnotation(height = "380")
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.treeNodeEditor})
    @ResponseBody
    public ResultModel<FieldSVGPagerView> getFieldSVGPagerInfo(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<FieldSVGPagerView> result = new ResultModel<FieldSVGPagerView>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);
               result.setData(new FieldSVGPagerView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
