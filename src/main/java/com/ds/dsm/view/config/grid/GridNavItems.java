package com.ds.dsm.view.config.grid;

import com.ds.esd.custom.tree.enums.TreeItem;

public enum GridNavItems implements TreeItem {
    GridView("模型配置", "spafont spa-icon-config", ViewGridInfoService.class, false, false, false),
    GridActionGroup("动作事件", "spafont spa-icon-action", ViewGridActionService.class, false, false, false),
    FieldGridList("显示列", "spafont spa-icon-c-comboinput", ViewGridFieldsService.class, true, true, true),
    FieldHiddenGridList("隐藏列", "spafont spa-icon-c-hiddeninput", ViewGridHiddensService.class, true, true, true);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    GridNavItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
