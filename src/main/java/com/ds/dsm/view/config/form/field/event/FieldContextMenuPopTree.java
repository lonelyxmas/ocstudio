package com.ds.dsm.view.config.form.field.event;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.tree.enums.TreeMenu;
import com.ds.esd.custom.tree.enums.TreeRowMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, caption = "添加按钮", customService = {FieldContextMenuService.class}, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Close, CustomFormMenu.Save})
public class FieldContextMenuPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String euClassName;

    @CustomAnnotation(pid = true)
    String viewInstId;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String bar;


    public FieldContextMenuPopTree(String id, String caption) {
        super(id, caption);
    }

    public FieldContextMenuPopTree(String pattern, CustomFormMenu menu) throws JDSException {
        super(menu.getType(), menu.getType() + "(" + menu.getCaption() + ")", menu.getImageClass());
    }

    public FieldContextMenuPopTree(String pattern, TreeMenu menu) throws JDSException {
        super(menu.getType(), menu.getType() + "(" + menu.getCaption() + ")", menu.getImageClass());
    }

    public FieldContextMenuPopTree(String pattern, TreeRowMenu menu) throws JDSException {
        super(menu.getType(), menu.getType() + "(" + menu.getCaption() + ")", menu.getImageClass());
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }
}
