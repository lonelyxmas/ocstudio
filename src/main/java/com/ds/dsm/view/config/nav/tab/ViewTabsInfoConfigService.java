package com.ds.dsm.view.config.nav.tab;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavTabsBaseViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/tabs/")
public class ViewTabsInfoConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "NavTabsBarView")
    @FormViewAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, imageClass = "spafont spa-icon-rendermode", caption = "导航栏配置")
    @ResponseBody
    public ResultModel<NavTabsBarView> getNavTabsBarView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<NavTabsBarView> result = new ResultModel<NavTabsBarView>();

        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            NavTabsViewBean tabsViewBean = null;
            if (customMethodAPIBean.getView() instanceof NavTabsBaseViewBean) {
                NavTabsBaseViewBean navTreeViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
                tabsViewBean = navTreeViewBean.getTabsViewBean();
            } else {
                tabsViewBean = (NavTabsViewBean) customMethodAPIBean.getView();
            }
            result.setData(new NavTabsBarView(tabsViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}