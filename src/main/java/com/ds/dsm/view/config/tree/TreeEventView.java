package com.ds.dsm.view.config.tree;

import com.ds.dsm.aggregation.event.TreeEventService;
import com.ds.esd.custom.annotation.CodeEditorAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.custom.tree.enums.CustomTreeEvent;
import com.ds.esd.tool.ui.enums.ComponentType;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {TreeEventService.class})
public class TreeEventView {

    @CustomAnnotation(caption = "事件名称", uid = true)
    String eventName;

    @CustomAnnotation(caption = "监听事件")
    String eventKey;

    @CustomAnnotation(caption = "描述")
    public String desc;

    @CustomAnnotation(caption = "表达式")
    public String expression;

    @CodeEditorAnnotation
    @FieldAnnotation(colSpan = -1, rowHeight = "150", componentType = ComponentType.CodeEditor)
    @CustomAnnotation(caption = "脚本")
    public String script;

    public TreeEventView() {

    }


    public TreeEventView(CustomTreeEvent event, MethodConfig methodConfig) {
        this.eventKey = event.getEventEnum().getEvent();
        this.desc = event.getName();
        this.expression = event.getExpression();
        this.eventName = event.name();


    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
