package com.ds.dsm.view.config.container;

import com.alibaba.fastjson.JSONObject;
import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ContainerBean;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavTabsBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.field.DisabledBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/container/")
public class CustomDisabledService {

    @RequestMapping(method = RequestMethod.POST, value = "CustomDisabledConfig")
    @FormViewAnnotation(autoSave = true)
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "基础信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<CustomDisabledView> getCustomDisabledConfig(String sourceClassName, String sourceMethodName, String domainId, String viewInstId) {
        ResultModel<CustomDisabledView> result = new ResultModel<CustomDisabledView>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            NavTabsBaseViewBean tabsBaseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
            CustomViewBean  viewBean = tabsBaseViewBean.getCurrViewBean();
            result.setData(new CustomDisabledView(viewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "updateCustomDisabled")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateCustomDisabled(@RequestBody CustomDisabledView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(comboView.getSourceMethodName());
            NavTabsBaseViewBean tabsBaseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
            CustomViewBean  viewBean = tabsBaseViewBean.getCurrViewBean();
            String json = JSONObject.toJSONString(comboView);
            DisabledBean disabledBean= JSONObject.parseObject(json, DisabledBean.class);
            ContainerBean containerBean = viewBean.getContainerBean();
            if (containerBean == null) {
                containerBean = new ContainerBean();
            }
            containerBean.setDisabledBean(disabledBean);

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetCustomDisabled")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetCustomDisabled(@RequestBody CustomDisabledView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(comboView.getSourceMethodName());
            NavTabsBaseViewBean tabsBaseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
            CustomViewBean  viewBean = tabsBaseViewBean.getCurrViewBean();
            ContainerBean containerBean = viewBean.getContainerBean();
            if (containerBean == null) {
                containerBean = new ContainerBean();
            }
            containerBean.setDisabledBean(new DisabledBean());
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
