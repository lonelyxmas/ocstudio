package com.ds.dsm.view.config.form.field.service;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.item.FieldNavItem;
import com.ds.dsm.view.config.form.field.item.FormFieldNavTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/item/")
public class FieldItemsService {


    @RequestMapping(method = RequestMethod.POST, value = "ChildFieldConfig")
    @ModuleAnnotation(dynLoad = true, caption = "字段子域")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<FormFieldNavTree>> getChildFieldConfig(String domainId, String fieldname, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<FormFieldNavTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(FieldNavItem.values()), FormFieldNavTree.class);
        return result;
    }

}
