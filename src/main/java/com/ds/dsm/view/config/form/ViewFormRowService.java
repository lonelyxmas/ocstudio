package com.ds.dsm.view.config.form;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/form/")
public class ViewFormRowService {


    @RequestMapping(method = RequestMethod.POST, value = "FormRowView")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-dialog", caption = "行属性配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<FormRowView> getFormRowView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<FormRowView> result = new ResultModel<FormRowView>();
        try {

            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
            result.setData(new FormRowView(formViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
