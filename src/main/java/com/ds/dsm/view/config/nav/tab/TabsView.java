package com.ds.dsm.view.config.nav.tab;

import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.form.TreeDataBaseBean;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.*;

public class TabsView {


    public String expression;

    public String entityClassName;

    public String name;

    public String value;

    public Integer index = -1;

    public String id;

    public String caption;

    public String imageClass;


    String viewInstId;

    String domainId;

    String sourceClassName;

    String methodName;


    Boolean activeLast;

    String message;

    Integer maxHeight;

    SelModeType selMode;

    Boolean lazyAppend;

    Boolean closeBtn;

    Boolean popBtn;

    Boolean formField;

    Boolean noHandler;

    Boolean iniFold;

    Boolean autoReload;


    Boolean animCollapse;

    Boolean dynDestory;

    Boolean togglePlaceholder;

    TagCmdsAlign tagCmdsAlign;

    Boolean group;

    Boolean autoSave;

    String optBtn;

    String valueSeparator;

    Boolean singleOpen = true;

    BarLocationType barLocation;

    HAlignType barHAlign;

    VAlignType barVAlign;

    String barSize;

    String sideBarSize;

    SideBarStatusType sideBarStatus;


    public TabsView() {

    }

    public TabsView(MethodConfig<NavTabsViewBean, TreeDataBaseBean> methodConfig) {
        NavTabsViewBean viewConfig = methodConfig.getView();

        this.viewInstId = viewConfig.getViewInstId();
        this.domainId = viewConfig.getDomainId();
        this.sourceClassName = viewConfig.getSourceClassName();
        this.animCollapse = viewConfig.getAnimCollapse();
        this.iniFold = viewConfig.getIniFold();
        this.dynDestory = viewConfig.getDynDestory();
        this.togglePlaceholder = viewConfig.getTogglePlaceholder();
        this.group = viewConfig.getGroup();
        this.optBtn = viewConfig.getOptBtn();
        this.singleOpen = viewConfig.getSingleOpen();
        this.selMode = viewConfig.getSelMode();
        this.caption = viewConfig.getCaption();
        this.formField = viewConfig.getFormField();
        this.valueSeparator = viewConfig.getValueSeparator();
        this.methodName = methodConfig.getMethodName();
        this.activeLast = viewConfig.getActiveLast();
        this.message = viewConfig.getMessage();
        this.maxHeight = viewConfig.getMaxHeight();
        this.lazyAppend = viewConfig.getLazyAppend();
        this.closeBtn = viewConfig.getCloseBtn();
        this.noHandler = viewConfig.getNoHandler();
        this.autoReload = viewConfig.getAutoReload();
        this.tagCmdsAlign = viewConfig.getTagCmdsAlign();
        this.autoSave = viewConfig.getAutoSave();
        this.barLocation = viewConfig.getBarLocation();
        this.barHAlign = viewConfig.getBarHAlign();
        this.barVAlign = viewConfig.getBarVAlign();
        this.barSize = viewConfig.getBarSize();
        this.sideBarSize = viewConfig.getSideBarSize();
        this.sideBarStatus = viewConfig.getSideBarStatus();


    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Boolean getActiveLast() {
        return activeLast;
    }

    public void setActiveLast(Boolean activeLast) {
        this.activeLast = activeLast;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(Integer maxHeight) {
        this.maxHeight = maxHeight;
    }

    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }

    public Boolean getLazyAppend() {
        return lazyAppend;
    }

    public void setLazyAppend(Boolean lazyAppend) {
        this.lazyAppend = lazyAppend;
    }

    public Boolean getCloseBtn() {
        return closeBtn;
    }

    public void setCloseBtn(Boolean closeBtn) {
        this.closeBtn = closeBtn;
    }

    public Boolean getPopBtn() {
        return popBtn;
    }

    public void setPopBtn(Boolean popBtn) {
        this.popBtn = popBtn;
    }

    public Boolean getFormField() {
        return formField;
    }

    public void setFormField(Boolean formField) {
        this.formField = formField;
    }

    public Boolean getNoHandler() {
        return noHandler;
    }

    public void setNoHandler(Boolean noHandler) {
        this.noHandler = noHandler;
    }

    public Boolean getIniFold() {
        return iniFold;
    }

    public void setIniFold(Boolean iniFold) {
        this.iniFold = iniFold;
    }

    public Boolean getAutoReload() {
        return autoReload;
    }

    public void setAutoReload(Boolean autoReload) {
        this.autoReload = autoReload;
    }

    public Boolean getAnimCollapse() {
        return animCollapse;
    }

    public void setAnimCollapse(Boolean animCollapse) {
        this.animCollapse = animCollapse;
    }

    public Boolean getDynDestory() {
        return dynDestory;
    }

    public void setDynDestory(Boolean dynDestory) {
        this.dynDestory = dynDestory;
    }

    public Boolean getTogglePlaceholder() {
        return togglePlaceholder;
    }

    public void setTogglePlaceholder(Boolean togglePlaceholder) {
        this.togglePlaceholder = togglePlaceholder;
    }

    public TagCmdsAlign getTagCmdsAlign() {
        return tagCmdsAlign;
    }

    public void setTagCmdsAlign(TagCmdsAlign tagCmdsAlign) {
        this.tagCmdsAlign = tagCmdsAlign;
    }

    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }

    public Boolean getAutoSave() {
        return autoSave;
    }

    public void setAutoSave(Boolean autoSave) {
        this.autoSave = autoSave;
    }

    public String getOptBtn() {
        return optBtn;
    }

    public void setOptBtn(String optBtn) {
        this.optBtn = optBtn;
    }

    public String getValueSeparator() {
        return valueSeparator;
    }

    public void setValueSeparator(String valueSeparator) {
        this.valueSeparator = valueSeparator;
    }

    public Boolean getSingleOpen() {
        return singleOpen;
    }

    public void setSingleOpen(Boolean singleOpen) {
        this.singleOpen = singleOpen;
    }

    public BarLocationType getBarLocation() {
        return barLocation;
    }

    public void setBarLocation(BarLocationType barLocation) {
        this.barLocation = barLocation;
    }

    public HAlignType getBarHAlign() {
        return barHAlign;
    }

    public void setBarHAlign(HAlignType barHAlign) {
        this.barHAlign = barHAlign;
    }

    public VAlignType getBarVAlign() {
        return barVAlign;
    }

    public void setBarVAlign(VAlignType barVAlign) {
        this.barVAlign = barVAlign;
    }

    public String getBarSize() {
        return barSize;
    }

    public void setBarSize(String barSize) {
        this.barSize = barSize;
    }

    public String getSideBarSize() {
        return sideBarSize;
    }

    public void setSideBarSize(String sideBarSize) {
        this.sideBarSize = sideBarSize;
    }

    public SideBarStatusType getSideBarStatus() {
        return sideBarStatus;
    }

    public void setSideBarStatus(SideBarStatusType sideBarStatus) {
        this.sideBarStatus = sideBarStatus;
    }
}
