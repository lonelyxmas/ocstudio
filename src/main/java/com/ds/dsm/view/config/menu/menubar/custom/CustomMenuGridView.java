package com.ds.dsm.view.config.menu.menubar.custom;

import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.CustomMenu;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.CustomImageType;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = CustomMenuBarService.class)
public class CustomMenuGridView {
    @CustomAnnotation(caption = "按钮名称")
    private String menucaption;

    @CustomAnnotation(caption = "类型", uid = true)
    private String type;

    @CustomAnnotation(caption = "位置")
    private String tag;


    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    private String imageClass;

    @CustomAnnotation(caption = "表达式")
    private String expression;

    @CustomAnnotation(caption = "类型")
    public ComboInputType itemType = ComboInputType.button;


    public CustomMenuGridView(CustomMenu customMenu, MethodConfig methodConfig) {
        this.type = customMenu.type();
        this.menucaption = customMenu.caption();
        this.expression = customMenu.expression();
        this.imageClass = customMenu.imageClass();
        this.itemType = customMenu.itemType();


    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMenucaption() {
        return menucaption;
    }

    public void setMenucaption(String menucaption) {
        this.menucaption = menucaption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public ComboInputType getItemType() {
        return itemType;
    }

    public void setItemType(ComboInputType itemType) {
        this.itemType = itemType;
    }

}
