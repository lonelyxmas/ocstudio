package com.ds.dsm.view.config.form.field.custom;

import com.alibaba.fastjson.JSONObject;
import com.ds.enums.service.HttpMethod;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.ContainerBean;
import com.ds.esd.custom.field.InputFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.*;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu
@FormAnnotation(col = 2, customService = ContainerService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ContainerView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    public String fieldname;
    @ComboColorAnnotation
    @CustomAnnotation(caption = "背景颜色")
    public String panelBgClr;
    @CustomAnnotation(caption = "背景图片")
    public String panelBgImg;
    @CustomAnnotation(caption = "背景")
    public String panelBgImgPos;
    @CustomAnnotation(caption = "背景平铺")
    public AttachmentType panelBgImgAttachment;

    @CustomAnnotation(caption = "溢出设定")
    public OverflowType overflow;

    @FieldAnnotation(colSpan = -1)
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = BgRepeatType.class)
    @CustomAnnotation(caption = "背景图裁切")
    public String panelBgImgRepeat;

    @SliderAnnotation()
    @CustomAnnotation(caption = "列数")
    public Integer conLayoutColumns;

    @FieldAnnotation(colSpan = -1)
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = ThemesType.class)
    @CustomAnnotation(caption = "沙箱样式")
    public String sandboxTheme;

    @CustomAnnotation(caption = "提交方法")
    public HttpMethod formMethod;
    @CustomAnnotation(caption = "目标")
    public String formTarget;
    @CustomAnnotation(caption = "提交地址")
    public String formDataPath;
    @CustomAnnotation(caption = "动作")
    public String formAction;
    @CustomAnnotation(caption = "加密方式")
    public String formEnctype;
    @CustomAnnotation(caption = "样式表")
    public String className;
    @CustomAnnotation(caption = "拖动(dropKeys)")
    public String dropKeys;
    @CustomAnnotation(caption = "拖动dragKey")
    public String dragKey;
    @CustomAnnotation(caption = "iframeAutoLoad")
    public String iframeAutoLoad;
    @CustomAnnotation(caption = "ajaxAutoLoad")
    public String ajaxAutoLoad;

    @CustomAnnotation(caption = "是否可选中")
    public Boolean selectable;

    @CustomAnnotation(caption = "set数据")
    public String set;
    @CustomAnnotation(caption = "自动提示")
    public String autoTips;
    @CustomAnnotation(caption = "显示单位")
    public SpaceUnitType spaceUnit;
    @CustomAnnotation(caption = "默认焦点")
    public Boolean defaultFocus;
    @FieldAnnotation(colSpan = -1)
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = HoverPopType.class)
    @CustomAnnotation(caption = "经过显示")
    public String hoverPop;
    @CustomAnnotation(caption = "显示动画")
    public String showEffects;
    @CustomAnnotation(caption = "隐藏动画")
    public String hideEffects;
    @CustomAnnotation(caption = "提示")
    public String tips;
    @CustomAnnotation(caption = "放大倍数")
    public Integer rotate;
    @CustomAnnotation(caption = "动画配置")
    public CustomAnimType activeAnim;

    @RichEditorAnnotation
    @FieldAnnotation(rowHeight = "200", colSpan = -1)
    @CustomAnnotation(caption = "html")
    public String html;

    public ContainerView() {

    }


    public ContainerView(FieldFormConfig<InputFieldBean,?> config) {
        ContainerBean containerBean = config.getContainerBean();
        if (containerBean == null) {
            containerBean = new ContainerBean();
        }
        String json = JSONObject.toJSONString(containerBean);
        BeanMap.create(this).putAll(JSONObject.parseObject(json));
        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();


    }

    public Boolean getDefaultFocus() {
        return defaultFocus;
    }

    public void setDefaultFocus(Boolean defaultFocus) {
        this.defaultFocus = defaultFocus;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getPanelBgClr() {
        return panelBgClr;
    }

    public void setPanelBgClr(String panelBgClr) {
        this.panelBgClr = panelBgClr;
    }

    public String getPanelBgImg() {
        return panelBgImg;
    }

    public void setPanelBgImg(String panelBgImg) {
        this.panelBgImg = panelBgImg;
    }

    public String getPanelBgImgPos() {
        return panelBgImgPos;
    }

    public void setPanelBgImgPos(String panelBgImgPos) {
        this.panelBgImgPos = panelBgImgPos;
    }

    public AttachmentType getPanelBgImgAttachment() {
        return panelBgImgAttachment;
    }

    public void setPanelBgImgAttachment(AttachmentType panelBgImgAttachment) {
        this.panelBgImgAttachment = panelBgImgAttachment;
    }

    public Integer getConLayoutColumns() {
        return conLayoutColumns;
    }

    public void setConLayoutColumns(Integer conLayoutColumns) {
        this.conLayoutColumns = conLayoutColumns;
    }

    public String getSandboxTheme() {
        return sandboxTheme;
    }

    public void setSandboxTheme(String sandboxTheme) {
        this.sandboxTheme = sandboxTheme;
    }

    public HttpMethod getFormMethod() {
        return formMethod;
    }

    public void setFormMethod(HttpMethod formMethod) {
        this.formMethod = formMethod;
    }

    public String getFormTarget() {
        return formTarget;
    }

    public void setFormTarget(String formTarget) {
        this.formTarget = formTarget;
    }

    public String getFormDataPath() {
        return formDataPath;
    }

    public void setFormDataPath(String formDataPath) {
        this.formDataPath = formDataPath;
    }

    public String getFormAction() {
        return formAction;
    }

    public void setFormAction(String formAction) {
        this.formAction = formAction;
    }

    public String getFormEnctype() {
        return formEnctype;
    }

    public void setFormEnctype(String formEnctype) {
        this.formEnctype = formEnctype;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDropKeys() {
        return dropKeys;
    }

    public void setDropKeys(String dropKeys) {
        this.dropKeys = dropKeys;
    }

    public String getDragKey() {
        return dragKey;
    }

    public void setDragKey(String dragKey) {
        this.dragKey = dragKey;
    }

    public String getIframeAutoLoad() {
        return iframeAutoLoad;
    }

    public void setIframeAutoLoad(String iframeAutoLoad) {
        this.iframeAutoLoad = iframeAutoLoad;
    }

    public String getAjaxAutoLoad() {
        return ajaxAutoLoad;
    }

    public void setAjaxAutoLoad(String ajaxAutoLoad) {
        this.ajaxAutoLoad = ajaxAutoLoad;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    public OverflowType getOverflow() {
        return overflow;
    }

    public void setOverflow(OverflowType overflow) {
        this.overflow = overflow;
    }

    public String getPanelBgImgRepeat() {
        return panelBgImgRepeat;
    }

    public void setPanelBgImgRepeat(String panelBgImgRepeat) {
        this.panelBgImgRepeat = panelBgImgRepeat;
    }

    public String getSet() {
        return set;
    }

    public void setSet(String set) {
        this.set = set;
    }

    public String getAutoTips() {
        return autoTips;
    }

    public void setAutoTips(String autoTips) {
        this.autoTips = autoTips;
    }

    public SpaceUnitType getSpaceUnit() {
        return spaceUnit;
    }

    public void setSpaceUnit(SpaceUnitType spaceUnit) {
        this.spaceUnit = spaceUnit;
    }

    public String getHoverPop() {
        return hoverPop;
    }

    public void setHoverPop(String hoverPop) {
        this.hoverPop = hoverPop;
    }

    public String getShowEffects() {
        return showEffects;
    }

    public void setShowEffects(String showEffects) {
        this.showEffects = showEffects;
    }

    public String getHideEffects() {
        return hideEffects;
    }

    public void setHideEffects(String hideEffects) {
        this.hideEffects = hideEffects;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public Integer getRotate() {
        return rotate;
    }

    public void setRotate(Integer rotate) {
        this.rotate = rotate;
    }

    public CustomAnimType getActiveAnim() {
        return activeAnim;
    }

    public void setActiveAnim(CustomAnimType activeAnim) {
        this.activeAnim = activeAnim;
    }
}
