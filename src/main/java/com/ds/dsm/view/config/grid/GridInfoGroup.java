package com.ds.dsm.view.config.grid;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.PageBarView;
import com.ds.dsm.view.config.action.CustomBuildAction;
import com.ds.dsm.view.config.service.GridConfigService;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/grid/group/")
@BottomBarMenu(menuClass = CustomBuildAction.class)
@NavGroupAnnotation(
        bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet}, customService = GridConfigService.class)
public class GridInfoGroup {


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;


    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    public GridInfoGroup() {

    }


    @MethodChinaName(cname = "基础信息配置")
    @RequestMapping(method = RequestMethod.POST, value = "GridBaseView")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-values", caption = "列表信息", dock = Dock.top)
    @APIEventAnnotation(autoRun = true)
    @UIAnnotation(height = "150")
    @ResponseBody
    public ResultModel<GridBaseView> getGridBaseView(String sourceClassName, String methodName, String domainId, String viewInstId) {

        ResultModel<GridBaseView> result = new ResultModel<GridBaseView>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomGridViewBean gridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            result.setData(new GridBaseView(gridViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "GridRowView")
    @FormViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ModuleAnnotation(dock = Dock.fill, imageClass = "spafont spa-icon-rendermode", caption = "行头信息配置")
    @ResponseBody
    public ResultModel<GridRowHead> getGridRowView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<GridRowHead> result = new ResultModel<GridRowHead>();

        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomGridViewBean gridViewBean = (CustomGridViewBean) methodAPIBean.getView();

            result.setData(new GridRowHead(gridViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "PageBarView")
    @FormViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @UIAnnotation(height = "260")
    @ModuleAnnotation(dock = Dock.bottom, imageClass = "spafont spa-icon-rendermode", caption = "分页栏配置")
    @ResponseBody
    public ResultModel<PageBarView> getPageBarView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<PageBarView> result = new ResultModel<PageBarView>();

        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
            CustomGridViewBean gridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            result.setData(new PageBarView(gridViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
