package com.ds.dsm.view.config.custom.panel.div.ui;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.nav.PanelItemBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/custom/ui/")
public class UIService {


    @RequestMapping(method = RequestMethod.POST, value = "UIConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<UIView> getUIConfig(String sourceClassName, String sourceMethodName, String groupId, String domainId, String viewInstId) {
        ResultModel<UIView> resultModel = new ResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(sourceMethodName);
            NavBaseViewBean baseViewBean = (NavBaseViewBean) customMethodAPIBean.getView();
            PanelItemBean panelItemBean = baseViewBean.getItemBean(groupId);

            resultModel.setData(new UIView(panelItemBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
