package com.ds.dsm.view.config.grid;

import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/grid/group/")
public class ViewGridInfoService {


    @RequestMapping(method = RequestMethod.POST, value = "GridView")
    @NavGroupViewAnnotation(reSetUrl = "clear", saveUrl = "updateGridView", autoSave = true)
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, caption = "列表配置",  imageClass = "spafont spa-icon-project")
    @ResponseBody
    public ResultModel<GridInfoGroup> getGridInfoGroup(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<GridInfoGroup> result = new ResultModel<GridInfoGroup>();
        return result;
    }


}
