package com.ds.dsm.view.config.form.field.custom;

import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.enums.AppendType;
import com.ds.esd.custom.field.InputFieldBean;
import com.ds.esd.custom.field.combo.ComboPopFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.web.annotation.Required;

@FormAnnotation(col = 2)
public class FieldBaseView {

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "显示名称", readonly = true)
    private String caption;

    @ComboListBoxAnnotation()
    @Required
    @CustomListAnnotation(filter = "source.isAbsValue()")
    @CustomAnnotation(caption = "控件类型")
    private ComponentType componentType;

    @CustomAnnotation(caption = "展现形式")
    private ComboInputType type;


    @CustomAnnotation(caption = "主键", readonly = true)
    Boolean uid;

    @CustomAnnotation(caption = "禁用")
    Boolean disabled;

    @CustomAnnotation(caption = "只读")
    Boolean readonly;

    @CustomAnnotation(caption = "动态装载")
    Boolean dynLoad;

    @CustomAnnotation(caption = "隐藏")
    Boolean hidden;

    @ComboInputAnnotation(inputType = ComboInputType.spin)
    @CustomAnnotation(caption = "数字精度")
    Integer fractions;

    @CustomAnnotation(caption = "引用方式")
    AppendType append;

    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "链接地址")
    String src;

    @FieldAnnotation( colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "默认值公式")
    String expression;
    @FieldAnnotation( colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "过滤表达式")
    String filter;
    @FieldAnnotation( colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "选项表达式")
    String itemsExpression;


    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    public FieldBaseView() {

    }

    public FieldBaseView(FieldFormConfig<ComboPopFieldBean,?> config) {
        this.uid = getUid();

        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();

        this.disabled = config.getAggConfig().getDisabled();

        this.componentType = config.getComponentType();
        this.caption = config.getAggConfig().getCaption();
        this.fieldname = config.getFieldname();

        this.hidden = config.getColHidden();
        this.expression = config.getAggConfig().getExpression();
        this.uid = config.getUid();

        this.readonly = config.getAggConfig().getReadonly();
        this.domainId = config.getDomainId();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getItemsExpression() {
        return itemsExpression;
    }

    public void setItemsExpression(String itemsExpression) {
        this.itemsExpression = itemsExpression;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public Integer getFractions() {
        return fractions;
    }

    public void setFractions(Integer fractions) {
        this.fractions = fractions;
    }


    public AppendType getAppend() {
        return append;
    }

    public void setAppend(AppendType append) {
        this.append = append;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Boolean getUid() {
        return uid;
    }

    public void setUid(Boolean uid) {
        this.uid = uid;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public ComboInputType getType() {
        return type;
    }

    public void setType(ComboInputType type) {
        this.type = type;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

}
