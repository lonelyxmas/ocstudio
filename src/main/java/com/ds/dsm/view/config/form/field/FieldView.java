package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.form.service.FormFieldService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComboType;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(col = 1, customService = FormFieldService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class FieldView {
    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String id;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @Required
    @CustomAnnotation(caption = "字段名")
    String fieldname;

    @CustomAnnotation(caption = "组件类型")
    @CustomListAnnotation(filter = "source.isAbsValue()")
    @ComboListBoxAnnotation
    ComponentType componentType = ComponentType.ComboInput;

    @CustomAnnotation(caption = "组合类型")
    ComboType comboType = ComboType.input;

    @CustomAnnotation(caption = "数据类型")
    @ComboListBoxAnnotation
    @CustomListAnnotation(filter = "source.comboType.name()==comboType", dynLoad = true)
    ComboInputType inputType;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    public FieldView() {

    }

    public ComboType getComboType() {
        return comboType;
    }

    public void setComboType(ComboType comboType) {
        this.comboType = comboType;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public ComboInputType getInputType() {
        return inputType;
    }

    public void setInputType(ComboInputType inputType) {
        this.inputType = inputType;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }


    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
