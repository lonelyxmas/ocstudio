package com.ds.dsm.view.config.nav.tree;

import com.ds.dsm.view.config.form.field.service.FormModuleService;
import com.ds.dsm.view.config.tree.ViewChildTreeConfigService;
import com.ds.dsm.view.config.tree.ViewTreeConfigService;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.form.pop.PopTreeViewBean;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class PopTreeTree extends TreeListItem {

    @Pid
    String domainId;
    @Pid
    String methodName;
    @Pid
    String viewInstId;
    @Pid
    String sourceMethodName;
    @Pid
    String sourceClassName;
    @Pid
    String fieldname;


    @TreeItemAnnotation(bindService = ViewTreeConfigService.class)
    public PopTreeTree(PopTreeViewBean customTreeViewBean) {
        this.caption = "导航树配置";
        this.setImageClass(ModuleViewType.TreeConfig.getImageClass());
        this.setId("DSMTreeTabsRoot_" + customTreeViewBean.getMethodName());
        this.domainId = customTreeViewBean.getDomainId();
        this.viewInstId = customTreeViewBean.getViewInstId();
        this.sourceClassName = customTreeViewBean.getSourceClassName();
        this.sourceMethodName = customTreeViewBean.getMethodName();
    }

    @TreeItemAnnotation(bindService = ViewChildTreeConfigService.class)
    public PopTreeTree(ChildTreeViewBean childTreeViewBean, String sourceClassName, String methodName) {
        this.caption = "导航树配置";
        this.setImageClass(ModuleViewType.TreeConfig.getImageClass());
        this.setId("DSMTreeTabsRoot_" + childTreeViewBean.getMethodName());
        this.domainId = childTreeViewBean.getDomainId();
        this.viewInstId = childTreeViewBean.getViewInstId();
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
    }


    @TreeItemAnnotation(bindService = FormModuleService.class)
    public PopTreeTree(FieldFormConfig fieldFormInfo, String sourceMethodName) {
        this.caption = fieldFormInfo.getAggConfig().getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldFormInfo.getFieldname();
        }
        if (fieldFormInfo.getWidgetConfig() instanceof ComboBoxBean) {
            ComboBoxBean comboBoxBean = (ComboBoxBean) fieldFormInfo.getWidgetConfig();
            this.imageClass = comboBoxBean.getInputType().getImageClass();
        } else {
            this.imageClass = fieldFormInfo.getWidgetConfig().getComponentType().getImageClass();
        }

        this.domainId = fieldFormInfo.getDomainId();
        this.viewInstId = fieldFormInfo.getDomainId();
        this.sourceClassName = fieldFormInfo.getSourceClassName();
        this.sourceMethodName = sourceMethodName;
        this.fieldname = fieldFormInfo.getFieldname();

    }
}
