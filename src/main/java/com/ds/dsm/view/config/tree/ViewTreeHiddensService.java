package com.ds.dsm.view.config.tree;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.grid.field.HiddenFieldGridInfo;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldTreeConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tree/hidden/")
public class ViewTreeHiddensService {


    @RequestMapping(method = RequestMethod.POST, value = "HiddenTreeList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "列配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<HiddenFieldGridInfo>> getHiddenTreeList(String sourceClassName,  String methodName, String domainId, String viewInstId) {
        ListResultModel<List<HiddenFieldGridInfo>> cols = new ListResultModel();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }
            List<String> fieldNames = customTreeViewBean.getHiddenFieldNames();
            List<FieldTreeConfig> fields = new ArrayList<>();
            for (String fieldname : fieldNames) {
                FieldTreeConfig fieldGridConfig = (FieldTreeConfig) customTreeViewBean.getFieldConfigMap().get(fieldname);
                if (fieldGridConfig != null) {
                    fieldGridConfig.setSourceClassName(sourceClassName);
                    fields.add(fieldGridConfig);
                }
            }

            cols = PageUtil.getDefaultPageList(fields, HiddenFieldGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ChildViewTreeHidden")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<CustomTreeTree>> getChildViewTreeHidden(String domainId, String sourceClassName, String sourceMethodName, String viewInstId) {
        TreeListResultModel<List<CustomTreeTree>> result = new TreeListResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(sourceMethodName);
            List<FieldTreeConfig> fieldFormConfigs = new ArrayList<>();
            if (methodAPIBean!=null){
                CustomTreeViewBean customTreeViewBean = null;
                if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                    NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                    customTreeViewBean = baseViewBean.getTreeView();
                } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                    customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                }

                List<String> fieldNames = customTreeViewBean.getHiddenFieldNames();

                for (String fieldname : fieldNames) {
                    FieldTreeConfig fieldFormConfig = (FieldTreeConfig) customTreeViewBean.getFieldConfigMap().get(fieldname);
                    if (fieldFormConfig != null && fieldFormConfig.getColHidden()) {

                        fieldFormConfigs.add(fieldFormConfig);
                    }
                }
            }

            result = TreePageUtil.getTreeList(fieldFormConfigs, CustomTreeTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
