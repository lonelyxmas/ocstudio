package com.ds.dsm.view.config.nav.group;

import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityConfigTree;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.dsm.view.config.nav.items.ModuleItemGroup;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/group/module/")
public class ViewGroupModuleService {


    @RequestMapping(method = RequestMethod.POST, value = "ChildModuleInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(caption = "字段信息")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.treeNodeEditor})
    @ResponseBody
    public ResultModel<ModuleItemGroup> getChildModuleInfo(String sourceClassName, String projectId, String methodName, String domainId, String fieldname) {
        return new ResultModel<>();
    }

    @RequestMapping(method = RequestMethod.POST, value = "ChildModuleTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggEntityConfigTree>> getChildModuleTree(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggEntityConfigTree>> resultModel = TreePageUtil.getTreeList(Arrays.asList(AggEntityNavItem.values()), AggEntityConfigTree.class);
        return resultModel;
    }


}
