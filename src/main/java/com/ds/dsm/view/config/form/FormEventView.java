package com.ds.dsm.view.config.form;

import com.ds.dsm.aggregation.api.method.action.bind.BindActionFormView;
import com.ds.dsm.aggregation.event.FormEventService;
import com.ds.esd.custom.annotation.CustomAction;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.form.enums.CustomFormEvent;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.component.event.Event;
import com.ds.esd.tool.ui.component.event.EventKey;
import com.ds.esd.tool.ui.enums.ComponentType;

import java.util.ArrayList;
import java.util.List;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {FormEventService.class})
public class FormEventView {


    @CustomAnnotation(caption = "事件名称", uid = true)
    String eventName;

    @CustomAnnotation(caption = "监听事件")
    String eventKey;
    @CustomAnnotation(caption = "描述")
    public String desc;
    @CustomAnnotation(caption = "表达式")
    public String expression;
    @FieldAnnotation( colSpan = -1, rowHeight = "150", componentType = ComponentType.CodeEditor)
    @CustomAnnotation(caption = "脚本")
    public String script;

    @CustomAnnotation(caption = "动作", hidden = true)
    public List<BindActionFormView> actions = new ArrayList<>();

    public FormEventView() {

    }

    public FormEventView(Event event, EventKey eventKey,MethodConfig methodConfig) {
        this.expression = event.getEventReturn();
        this.script = event.getScript();
        this.eventName = eventKey.getEvent();
        this.eventKey = eventKey.getEvent();
        this.desc = event.getDesc();
        List<Action> actionList = event.getActions();
        for (Action action : actionList) {
            actions.add(new BindActionFormView(action,methodConfig));
        }
    }

    public FormEventView(CustomFormEvent event,MethodConfig methodConfig) {
        this.eventKey = event.getEventEnum().getEvent();
        this.desc = event.getName();
        this.eventName = event.name();
        this.expression = event.getExpression();
        CustomAction[] actionList = event.getActions(true);
        for (CustomAction action : actionList) {
            actions.add(new BindActionFormView(new Action(action),methodConfig));
        }

    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<BindActionFormView> getActions() {
        return actions;
    }

    public void setActions(List<BindActionFormView> actions) {
        this.actions = actions;
    }
}
