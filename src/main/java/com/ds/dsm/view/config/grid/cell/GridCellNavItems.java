package com.ds.dsm.view.config.grid.cell;

import com.ds.esd.custom.tree.enums.TreeItem;

public enum GridCellNavItems implements TreeItem {
    GridCellInfo("单元格配置", "spafont spa-icon-c-toolbar", GridCellInfoService.class, false, false, false),
    GridCellEevent("单元格事件", "spafont spa-icon-rendermode", ViewCellActionService.class, false, false, false);
    //  FieldHiddenGridList("环境变量", "spafont spa-icon-c-hiddeninput", ViewGridHiddensService.class, true, true, true);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    GridCellNavItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
