package com.ds.dsm.view.config.gallery.item;

import com.ds.dsm.aggregation.event.GridEventService;
import com.ds.dsm.view.config.grid.cell.contextmenu.ContextCellService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum GalleryCellMenuItems implements TreeItem {
    GridCellRightist("右键菜单", "spafont spa-icon-c-menu", ContextCellService.class, false, false, false),
    GridCellEventList("常用事件", "spafont spa-icon-event", GridEventService.class, false, false, false);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    GalleryCellMenuItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
