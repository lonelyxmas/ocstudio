package com.ds.dsm.view.config.menu.toolbar.custom;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.menu.bottommenu.custom.CustomBottomPopTree;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping(path = "/dsm/view/config/form/toolbar/")
public class CustomToolBarService {


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "表单按钮")
    @RequestMapping("CustomToolBarTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<CustomToolbarPopTree>> getCustomToolBarTree(String sourceClassName, String methodName, String domainId, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<CustomToolbarPopTree>> model = new TreeListResultModel<>();
        List<CustomToolbarPopTree> popTrees = new ArrayList<>();
        CustomToolbarPopTree menuPopTree = new CustomToolbarPopTree("toolbar", "常用按钮", "ToolBarTree");
        List<CustomToolbarPopTree> menuTrees = TreePageUtil.fillObjs(Arrays.asList(CustomFormMenu.values()), CustomToolbarPopTree.class);

        ApiClassConfig tableConfig = null;
         try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                menuTrees = TreePageUtil.fillObjs(Arrays.asList(GridMenu.values()), CustomToolbarPopTree.class);
            } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                menuTrees = TreePageUtil.fillObjs(Arrays.asList(GridMenu.values()), CustomToolbarPopTree.class);
            } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                menuTrees = TreePageUtil.fillObjs(Arrays.asList(GridMenu.values()), CustomToolbarPopTree.class);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }


        menuPopTree.setSub(menuTrees);
        popTrees.add(menuPopTree);
        model.setData(popTrees);
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addCustomToolBar")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addCustomToolBar(String sourceClassName, String methodName, String CustomToolBarTree, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            if (CustomToolBarTree != null && !CustomToolBarTree.equals("")) {
                String[] menuIds = StringUtility.split(CustomToolBarTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                            NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                            NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                            CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                            if (customTreeViewBean != null) {
                                customTreeViewBean.getToolBarMenu().add(CustomFormMenu.valueOf(menuId));
                            } else {
                                viewBean.getToolBarMenu().add(CustomFormMenu.valueOf(menuId));
                            }
                        } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                            CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                            formViewBean.getToolBarMenu().add(CustomFormMenu.valueOf(menuId));
                        } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                            CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                            customTreeViewBean.getToolBarMenu().add(CustomFormMenu.valueOf(menuId));
                        } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                            customGridViewBean.getToolBarMenu().add(GridMenu.valueOf(menuId));
                        }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                            CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();

                            customGalleryViewBean.getToolBarMenu().add(GridMenu.valueOf(menuId));
                        } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                            CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                            customGalleryViewBean.getToolBarMenu().add(GridMenu.valueOf(menuId));

                        }
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delCustomToolBar")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delCustomToolBar(String sourceClassName, String methodName, String type, String bar, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            String[] menuIds = StringUtility.split(type, ";");
            for (String menuId : menuIds) {
                if (menuId != null && !menuIds.equals("")) {
                    if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                        NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                        NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                        CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                        if (customTreeViewBean != null) {
                            customTreeViewBean.getToolBarMenu().remove(CustomFormMenu.valueOf(menuId));
                        } else {
                            viewBean.getToolBarMenu().remove(CustomFormMenu.valueOf(menuId));
                        }
                    } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                        CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                        formViewBean.getToolBarMenu().remove(CustomFormMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                        CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                        customTreeViewBean.getToolBarMenu().remove(CustomFormMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                        CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                        customGridViewBean.getToolBarMenu().remove(GridMenu.valueOf(menuId));
                    }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                        CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
                        customGalleryViewBean.getToolBarMenu().remove(GridMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                        CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                        customGalleryViewBean.getToolBarMenu().remove(GridMenu.valueOf(menuId));
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
