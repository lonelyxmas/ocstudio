package com.ds.dsm.view.config.nav;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/")
public class ViewSourceClassService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewConfig")
    @ModuleAnnotation(dynLoad = true, caption = "资源域", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> getViewConfig(String domainId, String sourceClassName, String viewInstId) {
        TreeListResultModel<List<ViewConfigTree>> result = new TreeListResultModel<>();
        try {
            List<CustomView> customViews = new ArrayList<>();
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId).getCurrConfig();
            List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
            for (MethodConfig methodAPIBean : customMethods) {
                if (methodAPIBean.getView() != null) {
                    customViews.add(methodAPIBean.getView());
                }
            }
            result = TreePageUtil.getTreeList(customViews, ViewConfigTree.class);
            return result;
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }



}
