package com.ds.dsm.view.config.form.field.list;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.combo.ComboFileView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.WidgetBean;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/list/")
@MethodChinaName(cname = "选择框", imageClass = "spafont spa-icon-c-checkbox")

public class FormCheckBoxService {


    @RequestMapping(method = RequestMethod.POST, value = "CheckBox")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass ="spafont spa-icon-c-checkbox", caption = "选择框", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<FieldCheckBoxView> getCheckBox(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<FieldCheckBoxView> result = new ResultModel<FieldCheckBoxView>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);
            result.setData(new FieldCheckBoxView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "updateCheckBox")
    @APIEventAnnotation( bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateCheckBox(@RequestBody FieldCheckBoxView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(comboView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboView.getFieldname());
            WidgetBean widgetBean = formInfo.getWidgetConfig();
            Map<String, Object> configMap = BeanMap.create(widgetBean);
            configMap.putAll(BeanMap.create(comboView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetCheckBox")
    @APIEventAnnotation( bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetCheckBox(@RequestBody FieldCheckBoxView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(comboView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboView.getFieldname());
            formInfo.setWidgetConfig(null);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
