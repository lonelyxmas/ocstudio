package com.ds.dsm.view.config.form.field.combo.service;

import com.ds.enums.db.MethodChinaName;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/combo/")
@MethodChinaName(cname = "组合字段", imageClass = "spafont spa-icon-c-comboinput")

public class ComboNoneService {


}
