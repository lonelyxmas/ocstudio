package com.ds.dsm.view.config.nav.layout;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavTabsBaseViewBean;
import com.ds.esd.custom.bean.nav.foldingtree.NavFoldingTreeViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.layout.CustomLayoutViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.web.util.PageUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/layout/")
public class LayoutService {

    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateLayoutCnfig")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formSave)
    public @ResponseBody
    ResultModel<Boolean> updateLayoutCnfig(@RequestBody NavLayoutConfigView configView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(configView.getSourceClassName(), configView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(configView.getMethodName());
            CustomLayoutViewBean layoutViewBean = null;

            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                layoutViewBean = ((NavTreeViewBean) customMethodAPIBean.getView()).getLayoutViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean navTreeViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                layoutViewBean = navTreeViewBean.getLayoutViewBean();

            } else if (customMethodAPIBean.getView() instanceof NavTabsBaseViewBean) {
                NavTabsBaseViewBean baseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
                layoutViewBean = baseViewBean.getLayoutViewBean();
            }
            if (layoutViewBean != null) {
                BeanMap.create(layoutViewBean).putAll(BeanMap.create(configView));
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "LayoutConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf", caption = "布局信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<NavLayoutConfigView> getLayoutConfig(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<NavLayoutConfigView> resultModel = new ResultModel();
        try {
            ViewEntityConfig config = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getSourceConfig().getMethodByName(methodName);
            CustomLayoutViewBean layoutConfigView = null;

            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                NavTreeViewBean navTreeViewBean = (NavTreeViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTreeViewBean.getLayoutViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean navTreeViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTreeViewBean.getLayoutViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavGalleryViewBean) {
                NavGalleryViewBean navTabViewBean = (NavGalleryViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTabViewBean.getLayoutViewBean();
            }
            resultModel.setData(new NavLayoutConfigView(layoutConfigView));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "布局配置")
    @RequestMapping("ItemConfigView")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-layout", caption = "布局配置")
    @ResponseBody
    public ListResultModel<List<NavItemConfigView>> getItemConfigViews(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<NavItemConfigView>> resultModel = new ListResultModel();
        try {
            ViewEntityConfig config = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getSourceConfig().getMethodByName(methodName);
            CustomLayoutViewBean layoutConfigView = null;

            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                NavTreeViewBean navTreeViewBean = (NavTreeViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTreeViewBean.getLayoutViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean navTreeViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTreeViewBean.getLayoutViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavGalleryViewBean) {
                NavGalleryViewBean navTabViewBean = (NavGalleryViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTabViewBean.getLayoutViewBean();
            }

            resultModel = PageUtil.getDefaultPageList(layoutConfigView.getItems(), NavItemConfigView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }


    @MethodChinaName(cname = "清空列表配置")
    @RequestMapping(method = RequestMethod.POST, value = "clearData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formReSet)
    public @ResponseBody
    ResultModel<Boolean> clearData(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(methodName);
            CustomLayoutViewBean layoutViewBean = null;

            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                layoutViewBean = ((NavTreeViewBean) customMethodAPIBean.getView()).getLayoutViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean navTreeViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                layoutViewBean = navTreeViewBean.getLayoutViewBean();

            } else if (customMethodAPIBean.getView() instanceof NavTabsBaseViewBean) {
                NavTabsBaseViewBean baseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
                layoutViewBean = baseViewBean.getLayoutViewBean();
            }
            if (layoutViewBean != null) {
                BeanMap.create(layoutViewBean).putAll(BeanMap.create(new CustomLayoutViewBean(customMethodAPIBean)));
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }
}
