package com.ds.dsm.view.config.tree.child;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.aggregation.api.method.APIMethodBaseGridView;
import com.ds.dsm.aggregation.config.entity.info.AggEntityConfigView;
import com.ds.dsm.aggregation.config.entity.info.AggEntityRefGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.AggEntityConfig;
import com.ds.esd.dsm.aggregation.ref.AggEntityRef;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/agg/entity/ref/child/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "实体子对象", imageClass = "spafont spa-icon-c-databinder")
public class AggChildEntityMetaView {
    @CustomAnnotation(uid = true, hidden = true)
    private String serviceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;

    @RequestMapping(method = RequestMethod.POST, value = "AggEntityConfig")
    @ModuleAnnotation(caption = "视图", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @CustomAnnotation(index = 0)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<AggEntityConfigView>> getAggEntityConfig(String serviceClassName, String viewInstId, String domainId) {
        ListResultModel<List<AggEntityConfigView>> result = new ListResultModel<List<AggEntityConfigView>>();
        try {
            AggEntityConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(serviceClassName, domainId);
            List<FieldModuleConfig> attMethods = tableConfig.getModuleMethods();
            result = PageUtil.getDefaultPageList(attMethods, AggEntityConfigView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AttMethod")
    @ModuleAnnotation(caption = "方法", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @CustomAnnotation(index = 1)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<APIMethodBaseGridView>> getAttMethod(String serviceClassName, String domainId, String viewInstId) {
        ListResultModel<List<APIMethodBaseGridView>> result = new ListResultModel<List<APIMethodBaseGridView>>();
        try {
            AggEntityConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getAggEntityConfig(serviceClassName, domainId);
            List<MethodConfig> attMethods = new ArrayList<>();
            for (MethodConfig apiBean : tableConfig.getAllMethods()) {
                if (!apiBean.isModule() && apiBean.getApi() != null) {
                    attMethods.add(apiBean);
                }
            }
            result = PageUtil.getDefaultPageList(attMethods, APIMethodBaseGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "EsdClassRefs")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-databinder", caption = "关联关系")
    @CustomAnnotation(index = 2)
    @ResponseBody
    public ListResultModel<List<AggEntityRefGridView>> getEsdClassRefs(String serviceClassName, String domainId) {
        ListResultModel<List<AggEntityRefGridView>> tables = new ListResultModel();
        try {
            DSMFactory factory = DSMFactory.getInstance(ESDFacrory.getESDClient().getSpace());
            List<AggEntityRef> dsmRefs = factory.getAggregationManager().getEntityRefByName(serviceClassName, domainId);
            tables = PageUtil.getDefaultPageList(dsmRefs, AggEntityRefGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return tables;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getServiceClassName() {
        return serviceClassName;
    }

    public void setServiceClassName(String serviceClassName) {
        this.serviceClassName = serviceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
