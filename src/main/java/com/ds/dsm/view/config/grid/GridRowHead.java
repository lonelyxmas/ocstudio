package com.ds.dsm.view.config.grid;

import com.ds.dsm.view.config.action.CustomBuildAction;
import com.ds.dsm.view.config.service.GridConfigService;
import com.ds.esd.custom.annotation.CustomAnnotation;

import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.RowHeadBean;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.ActiveModeType;
import com.ds.esd.custom.grid.enums.EditModeType;
import com.ds.esd.custom.grid.enums.HotRowModeType;
import com.ds.esd.custom.grid.enums.TreeModeType;
import com.ds.esd.tool.ui.enums.CmdTPosType;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;
import com.ds.web.util.AnnotationUtil;


@BottomBarMenu(menuClass = CustomBuildAction.class)
@FormAnnotation(col = 1,
        bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet}, customService = GridConfigService.class)
public class GridRowHead {


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    @FieldAnnotation( colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "行头标题")
    String gridHandlerCaption;

    @CustomAnnotation(caption = "行头手柄")
    Boolean rowHandler;

    @CustomAnnotation(caption = "显示行号")
    Boolean rowNumbered;

    @CustomAnnotation(caption = "行头宽度")
    String rowHandlerWidth;

    @CustomAnnotation(caption = "选中方式")
    SelModeType selMode;





    public GridRowHead(CustomGridViewBean gridConfig) {
        if (gridConfig == null) {
            gridConfig = new CustomGridViewBean();
        }
        this.viewInstId = gridConfig.getViewInstId();
        this.domainId=gridConfig.getDomainId();
        this.viewClassName = gridConfig.getViewClassName();
        this.sourceClassName=gridConfig.getSourceClassName();

        this.methodName = gridConfig.getMethodName();


        RowHeadBean headBean = gridConfig.getRowHead();
        if (headBean == null) {
            headBean = AnnotationUtil.fillDefaultValue(RowHead.class, new RowHeadBean());
        }
        this.rowHandler = headBean.getRowHandler();
        this.rowNumbered = headBean.getRowNumbered();
        this.gridHandlerCaption = headBean.getGridHandlerCaption();
        this.rowHandlerWidth = headBean.getRowHandlerWidth();
        this.selMode = headBean.getSelMode();
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }
    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getGridHandlerCaption() {
        return gridHandlerCaption;
    }

    public void setGridHandlerCaption(String gridHandlerCaption) {
        this.gridHandlerCaption = gridHandlerCaption;
    }

    public Boolean getRowHandler() {
        return rowHandler;
    }

    public void setRowHandler(Boolean rowHandler) {
        this.rowHandler = rowHandler;
    }

    public Boolean getRowNumbered() {
        return rowNumbered;
    }

    public void setRowNumbered(Boolean rowNumbered) {
        this.rowNumbered = rowNumbered;
    }

    public String getRowHandlerWidth() {
        return rowHandlerWidth;
    }

    public void setRowHandlerWidth(String rowHandlerWidth) {
        this.rowHandlerWidth = rowHandlerWidth;
    }

    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }

}
