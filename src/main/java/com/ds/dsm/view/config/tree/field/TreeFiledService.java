package com.ds.dsm.view.config.tree.field;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.FormNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.ESDFieldConfig;
import com.ds.esd.dsm.view.field.FieldTreeConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.jds.core.esb.util.OgnlUtil;
import com.ds.web.json.JSONData;
import com.ds.web.util.PageUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/tree/field/")
@MethodChinaName(cname = "字段配置", imageClass = "spafont spa-icon-c-comboinput")

public class TreeFiledService {


    @RequestMapping(method = RequestMethod.POST, value = "TreeFieldsModule")
    @ModuleAnnotation(dynLoad = true, caption = "获取字段列", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<FormNavTree>> getTreeFieldsModule(String domainId, String sourceClassName, String sourceMethodName, String viewInstId) {
        TreeListResultModel<List<FormNavTree>> result = new TreeListResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(sourceMethodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }
            List<ESDFieldConfig> esdFieldConfigs = new ArrayList<>();
            List<String> fieldNames = customTreeViewBean.getDisplayFieldNames();
            for (String fieldName : fieldNames) {
                FieldTreeConfig fieldFormConfig = (FieldTreeConfig) customTreeViewBean.getFieldConfigMap().get(fieldName);
                if (fieldFormConfig != null) {
                    esdFieldConfigs.add(fieldFormConfig);
                }
            }
            result = TreePageUtil.getTreeList(esdFieldConfigs, FormNavTree.class);


        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldGridList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "列配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<FieldTreeGridInfo>> getFieldGrids(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldTreeGridInfo>> cols = new ListResultModel();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig classConfig = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = classConfig.getMethodByName(methodName);
            CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            List<String> fieldNames = customTreeViewBean.getDisplayFieldNames();
            List<FieldTreeConfig> fields = new ArrayList<>();
            for (String fieldName : fieldNames) {
                FieldTreeConfig fieldTreeConfig = (FieldTreeConfig) customTreeViewBean.getFieldConfigMap().get(fieldName);
                if (fieldTreeConfig != null) {
                    fieldTreeConfig.setSourceClassName(sourceClassName);
                    fields.add(fieldTreeConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldTreeGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }

    @MethodChinaName(cname = "编辑字段配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFieldTree")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateFieldForm(@RequestBody FieldTreeInfo config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getSourceClassName();
            if (className != null && !className.equals("")) {
                ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(className, config.getDomainId());
                MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(config.getSourceClassName());
                CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                FieldTreeConfig fieldConfig = (FieldTreeConfig) customTreeViewBean.getFieldConfigMap().get(config.getFieldname());
                Map<String, Object> configMap = BeanMap.create(config);
                OgnlUtil.setProperties(configMap, fieldConfig, false, false);
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(customESDClassAPIBean);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldNav")
    @FormViewAnnotation
    @ModuleAnnotation(caption = "字段信息")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor, CustomMenuItem.add}, autoRun = true)
    @ResponseBody
    public ResultModel<FieldTreeInfo> getFieldNav(String sourceClassName, String methodName, String domainId, String viewInstId, String fieldname) {
        ResultModel<FieldTreeInfo> result = new ResultModel<FieldTreeInfo>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            FieldTreeConfig fieldConfig = (FieldTreeConfig) customTreeViewBean.getFieldConfigMap().get(fieldname);
            result.setData(new FieldTreeInfo(fieldConfig));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "批量保存")
    @RequestMapping(method = RequestMethod.POST, value = "updateAllFieldTree")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.saveAllRow)
    public @ResponseBody
    ResultModel<Boolean> updateAllFieldGrid(@JSONData List<FieldTreeGridInfo> rows, String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomTreeViewBean gridViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            for (FieldTreeGridInfo config : rows) {
                FieldTreeConfig fieldConfig = (FieldTreeConfig) gridViewBean.getFieldConfigMap().get(config.getFieldname());
                fieldConfig.setColHidden(config.getColHidden());
                fieldConfig.setCaptionField(config.getCaptionField());
                fieldConfig.setUid(config.getUid());
                fieldConfig.setPid(config.getPid());
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(customESDClassAPIBean);

        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
