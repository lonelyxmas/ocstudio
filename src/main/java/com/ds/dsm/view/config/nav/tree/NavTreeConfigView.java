package com.ds.dsm.view.config.nav.tree;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.FieldFormGridInfo;
import com.ds.dsm.view.config.tree.TreeDataGroup;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavButtonViewsViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/tree/")
@MethodChinaName(cname = "树形配置")
@ButtonViewsAnnotation(barLocation = BarLocationType.top, sideBarStatus = SideBarStatusType.expand, barSize = "3em")
public class NavTreeConfigView {


    @CustomAnnotation(hidden = true, pid = true)
    public String entityClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;
    @CustomAnnotation(uid = true, hidden = true)
    private String methodName;

    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    public NavTreeConfigView() {

    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "表单字段")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<FieldFormGridInfo>> getFieldList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldFormGridInfo>> cols = new ListResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = config.getMethodByName(methodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }

            Set<String> fieldNames = customTreeViewBean.getFieldNames();
            List<FieldFormConfig> fields = new ArrayList<>();
            for (String fieldName : fieldNames) {
                FieldFormConfig fieldFormConfig = (FieldFormConfig) customTreeViewBean.getFieldConfigMap().get(fieldName);
                if (fieldFormConfig != null) {

                    fields.add(fieldFormConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldFormGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @RequestMapping(method = RequestMethod.POST, value = "TreeDataGroup")
    @DynLoadAnnotation
    @NavButtonViewsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "窗体配置", imageClass = "spafont spa-icon-values")
    @ResponseBody
    public ResultModel<TreeDataGroup> getTreeDataGroup(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<TreeDataGroup> result = new ResultModel<TreeDataGroup>();
        return result;

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}
