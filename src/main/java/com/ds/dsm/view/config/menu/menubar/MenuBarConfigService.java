package com.ds.dsm.view.config.menu.menubar;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.editor.extmenu.MenuBarBean;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.jds.core.esb.util.OgnlUtil;
import com.ds.web.util.AnnotationUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/menu/menubar/")
@MethodChinaName(cname = "工具栏菜单管理", imageClass = "spafont spa-icon-c-gallery")

public class MenuBarConfigService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "菜单配置")
    @RequestMapping(method = RequestMethod.POST, value = "MenuBarInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "菜单配置", imageClass = "spafont spa-icon-c-grid")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<MenuBarNav> getMenuBarInfo(String domainId, String sourceClassName, String methodName) {
        ResultModel<MenuBarNav> result = new ResultModel<MenuBarNav>();
        return result;
    }


    @MethodChinaName(cname = "保存控制接口")
    @RequestMapping(value = {"saveMenuBarInfo"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveMenuBarInfo(@RequestBody MenuBarConfigView menuConfigView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ApiClassConfig tableConfig = null;
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(menuConfigView.getSourceClassName(), domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(menuConfigView.getMethodName());
            MenuBarBean barMenuBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                if (customTreeViewBean != null) {
                    barMenuBean = customTreeViewBean.getMenuBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(MenuBarMenu.class, new MenuBarBean());
                    }
                    OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                    customTreeViewBean.setMenuBar(barMenuBean);

                } else {
                    barMenuBean = viewBean.getMenuBar();
                    if (barMenuBean == null) {
                        barMenuBean = AnnotationUtil.fillDefaultValue(MenuBarMenu.class, new MenuBarBean());
                    }
                    OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                    viewBean.setMenuBar(barMenuBean);
                }

            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                CustomTreeViewBean treeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                barMenuBean = treeViewBean.getMenuBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(MenuBarMenu.class, new MenuBarBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                treeViewBean.setMenuBar(barMenuBean);
            } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                barMenuBean = formViewBean.getMenuBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(MenuBarMenu.class, new MenuBarBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                formViewBean.setMenuBar(barMenuBean);

            } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                barMenuBean = customGridViewBean.getMenuBar();

                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(MenuBarMenu.class, new MenuBarBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                customGridViewBean.setMenuBar(barMenuBean);
            }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
                barMenuBean = customGalleryViewBean.getMenuBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(MenuBarMenu.class, new MenuBarBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                customGalleryViewBean.setMenuBar(barMenuBean);
            } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                barMenuBean = customGalleryViewBean.getMenuBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(MenuBarMenu.class, new MenuBarBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                customGalleryViewBean.setMenuBar(barMenuBean);
            } else if (methodAPIBean.getView() instanceof NavTabsViewBean) {
                NavTabsViewBean tabsViewBean = (NavTabsViewBean) methodAPIBean.getView();
                barMenuBean = tabsViewBean.getMenuBar();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(MenuBarMenu.class, new MenuBarBean());
                }
                OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
                tabsViewBean.setMenuBar(barMenuBean);
            }


            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;

    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
