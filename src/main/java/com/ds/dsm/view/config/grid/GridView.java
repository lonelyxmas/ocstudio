package com.ds.dsm.view.config.grid;

import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;

public class GridView {

    String nextMark;
    String prevMark;
    String pageCaption;
    String viewInstId;
    String domainId;
    String viewClassName;
    String sourceClassName;
    String methodName;
    String gridHandlerCaption;
    String rowHandlerWidth;
    String rowHeight;
    Boolean pageBar = true;
    Boolean showMoreBtns;
    Integer pageCount = 20;
    Boolean showHeader;
    Boolean rowHandler;
    Boolean rowNumbered;
    Boolean autoTips = false;

    Boolean altRowsBg;
    Boolean rowHead = true;
    TagCmdsAlign tagCmdsAlign;
    SelModeType selMode;
    String parentID;
    String uriTpl;
    String height;
    Dock dock;



    Boolean disabled = false;

    Boolean readonly = false;

    String textTpl = "*";

    String value = "1:1:1";



    public GridView() {

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public Boolean getAutoTips() {
        return autoTips;
    }

    public void setAutoTips(Boolean autoTips) {
        this.autoTips = autoTips;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public String getTextTpl() {
        return textTpl;
    }

    public void setTextTpl(String textTpl) {
        this.textTpl = textTpl;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public String getUriTpl() {
        return uriTpl;
    }

    public void setUriTpl(String uriTpl) {
        this.uriTpl = uriTpl;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }

    public Boolean getRowHead() {
        return rowHead;
    }

    public void setRowHead(Boolean rowHead) {
        this.rowHead = rowHead;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getNextMark() {
        return nextMark;
    }

    public void setNextMark(String nextMark) {
        this.nextMark = nextMark;
    }

    public String getPrevMark() {
        return prevMark;
    }

    public void setPrevMark(String prevMark) {
        this.prevMark = prevMark;
    }

    public String getPageCaption() {
        return pageCaption;
    }

    public void setPageCaption(String pageCaption) {
        this.pageCaption = pageCaption;
    }

    public TagCmdsAlign getTagCmdsAlign() {
        return tagCmdsAlign;
    }

    public void setTagCmdsAlign(TagCmdsAlign tagCmdsAlign) {
        this.tagCmdsAlign = tagCmdsAlign;
    }

    public String getGridHandlerCaption() {
        return gridHandlerCaption;
    }

    public void setGridHandlerCaption(String gridHandlerCaption) {
        this.gridHandlerCaption = gridHandlerCaption;
    }

    public String getRowHandlerWidth() {
        return rowHandlerWidth;
    }

    public void setRowHandlerWidth(String rowHandlerWidth) {
        this.rowHandlerWidth = rowHandlerWidth;
    }

    public String getRowHeight() {
        return rowHeight;
    }

    public void setRowHeight(String rowHeight) {
        this.rowHeight = rowHeight;
    }

    public Boolean getAltRowsBg() {
        return altRowsBg;
    }

    public void setAltRowsBg(Boolean altRowsBg) {
        this.altRowsBg = altRowsBg;
    }

    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }
    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
    public Boolean getPageBar() {
        return pageBar;
    }

    public void setPageBar(Boolean pageBar) {
        this.pageBar = pageBar;
    }


    public Boolean getShowHeader() {
        return showHeader;
    }

    public void setShowHeader(Boolean showHeader) {
        this.showHeader = showHeader;
    }

    public Boolean getRowHandler() {
        return rowHandler;
    }

    public void setRowHandler(Boolean rowHandler) {
        this.rowHandler = rowHandler;
    }

    public Boolean getRowNumbered() {
        return rowNumbered;
    }

    public void setRowNumbered(Boolean rowNumbered) {
        this.rowNumbered = rowNumbered;
    }

    public Boolean getShowMoreBtns() {
        return showMoreBtns;
    }

    public void setShowMoreBtns(Boolean showMoreBtns) {
        this.showMoreBtns = showMoreBtns;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
