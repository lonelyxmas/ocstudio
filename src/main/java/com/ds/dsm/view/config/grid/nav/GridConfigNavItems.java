package com.ds.dsm.view.config.grid.nav;

import com.ds.dsm.view.config.grid.cell.GridCellListService;
import com.ds.dsm.view.config.grid.row.GridRowService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum GridConfigNavItems implements TreeItem {
    GridNavConfig("模块配置", "spafont spa-icon-values", GridConfigNavService.class, false, false, false),
    FormEventGroup("动作事件", "spafont spa-icon-event", GridNavEventService.class, false, false, false),
    GridRowList("行子域", "spafont spa-icon-c-slider", GridRowService.class, true, true, true),
    GridCellList("单元格子域", "spafont spa-icon-c-menu", GridCellListService.class, true, true, true);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    GridConfigNavItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
