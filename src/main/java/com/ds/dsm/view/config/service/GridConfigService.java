package com.ds.dsm.view.config.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.grid.GridDataView;
import com.ds.dsm.view.config.grid.GridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.grid.CustomGridDataBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.PageBarBean;
import com.ds.esd.custom.grid.RowHeadBean;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.web.util.AnnotationUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/grid/")

public class GridConfigService {


    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateGridView")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formSave)
    public @ResponseBody
    ResultModel<Boolean> updateGridBase(@RequestBody GridView gridConfig) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(gridConfig.getSourceClassName(),gridConfig.getViewInstId());

            MethodConfig methodAPIBean = config.getMethodByName(gridConfig.getMethodName());
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            if (gridConfig.getPageBar()) {
                PageBarBean pageBarBean = customGridViewBean.getPageBar();
                if (pageBarBean == null) {
                    pageBarBean = AnnotationUtil.fillDefaultValue(PageBar.class, new PageBarBean());
                }
                pageBarBean.setPageCaption(gridConfig.getPageCaption());
                pageBarBean.setPrevMark(gridConfig.getPrevMark());
                pageBarBean.setPageCount(gridConfig.getPageCount());
                pageBarBean.setNextMark(gridConfig.getNextMark());
                pageBarBean.setShowMoreBtns(gridConfig.getShowMoreBtns());
                pageBarBean.setParentID(gridConfig.getParentID());
                pageBarBean.setUriTpl(gridConfig.getUriTpl());
                pageBarBean.setHeight(gridConfig.getHeight());
                pageBarBean.setAutoTips(gridConfig.getAutoTips());
                pageBarBean.setDock(gridConfig.getDock());
                pageBarBean.setDisabled(gridConfig.getDisabled());
                pageBarBean.setReadonly(gridConfig.getReadonly());
                pageBarBean.setTextTpl(gridConfig.getTextTpl());
                pageBarBean.setValue(gridConfig.getValue());
                customGridViewBean.setPageBar(pageBarBean);


            } else {
                customGridViewBean.setPageBar(null);
            }

            //customGridViewBean.setTagCmdsAlign(gridConfig.getTagCmdsAlign());
            customGridViewBean.setShowHeader(gridConfig.getShowHeader());
            customGridViewBean.setRowHeight(gridConfig.getRowHeight());
            customGridViewBean.setAltRowsBg(gridConfig.getAltRowsBg());
            customGridViewBean.setViewInstId(gridConfig.getViewInstId());
            customGridViewBean.setDomainId(gridConfig.getDomainId());
            customGridViewBean.setSourceClassName(gridConfig.getSourceClassName());
            customGridViewBean.setViewClassName(gridConfig.getViewClassName());
            customGridViewBean.setMethodName(gridConfig.getMethodName());


            if (gridConfig.getRowHead()) {
                RowHeadBean rowHeadBean = customGridViewBean.getRowHead();
                if (rowHeadBean == null) {
                    rowHeadBean = AnnotationUtil.fillDefaultValue(RowHead.class, new RowHeadBean());
                }
                rowHeadBean.setGridHandlerCaption(gridConfig.getGridHandlerCaption());
                rowHeadBean.setRowHandlerWidth(gridConfig.getRowHandlerWidth());
                rowHeadBean.setSelMode(gridConfig.getSelMode());
                rowHeadBean.setRowHandler(gridConfig.getRowHandler());
                rowHeadBean.setRowNumbered(gridConfig.getRowNumbered());
                customGridViewBean.setRowHead(rowHeadBean);
            } else {
                customGridViewBean.setRowHead(null);
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "编辑数据信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateGridData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateGridData(@RequestBody GridDataView bean) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(bean.getSourceClassName(),  bean.getViewInstId());

            MethodConfig methodAPIBean = config.getMethodByName(bean.getMethodName());
            CustomGridDataBean customGridDataBean = (CustomGridDataBean) methodAPIBean.getDataBean();
            BeanMap.create(customGridDataBean).putAll(BeanMap.create(bean));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "编辑数据信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateGridWinData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateGridWinData(@RequestBody CustomModuleBean bean) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(bean.getSourceClassName(),  bean.getViewInstId());
            MethodConfig methodAPIBean =config.getMethodByName(bean.getMethodName());
            CustomModuleBean customModuleBean = methodAPIBean.getModuleBean();
            BeanMap.create(customModuleBean).putAll(BeanMap.create(bean));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "清空列表配置")
    @RequestMapping(method = RequestMethod.POST, value = "clearData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formReSet)
    public @ResponseBody
    ResultModel<Boolean> clearData(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);

            MethodConfig methodAPIBean = config.getMethodByName(methodName);
            ViewEntityConfig beanConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(methodAPIBean.getViewClassName(),  viewInstId);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
