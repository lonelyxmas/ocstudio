package com.ds.dsm.view.config.form.field.combo.pop;

import com.ds.dsm.view.config.form.field.combo.pop.service.FieldPopSelectTreeService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@BottomBarMenu()
@TreeAnnotation(heplBar = true, lazyLoad = true, selMode = SelModeType.singlecheckbox, customService = PopBindEntityService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class PopBindEntityTree extends TreeListItem {

    @Pid
    String domainId;

    @Pid
    String parentId;
    @Pid
    String projectName;
    @Pid
    String packageName;
    @Pid
    String euPackageName;

    String viewInstId;


    @TreeItemAnnotation(bindService = FieldPopSelectTreeService.class, imageClass = "spafont spa-icon-package")
    public PopBindEntityTree(EUPackage euPackage, String euPackageName, String domainId, String viewInstId) {
        this.imageClass = euPackage.getImageClass();
        this.packageName = euPackage.getPackageName();
        this.caption = euPackage.getDesc().indexOf(packageName) > -1 ? euPackage.getDesc() : packageName + "(" + euPackage.getDesc() + ")";
        this.id = euPackage.getFolder().getID();
        this.parentId = euPackage.getId();
        this.euPackageName = euPackageName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;

    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-page")
    public PopBindEntityTree(ESDClass esdClass, String domainId, String viewInstId) {
        this.id = esdClass.getClassName();
        this.domainId = domainId;
        this.caption = esdClass.getDesc() + "(" + esdClass.getClassName() + ")";
        this.viewInstId = viewInstId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getEuPackageName() {
        return euPackageName;
    }

    public void setEuPackageName(String euPackageName) {
        this.euPackageName = euPackageName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

}
