package com.ds.dsm.view.config.menu.service;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.menu.APIBIndPopTree;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.enums.CustomFormEvent;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping(path = "/dsm/agg/api/config/")
public class APIBindButtonService {


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true,  caption = "綁定按钮")
    @RequestMapping("BindButtonTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<APIBIndPopTree>> getBindButtonTree(String sourceClassName, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<APIBIndPopTree>> model = new TreeListResultModel<>();
        List<APIBIndPopTree> popTrees = new ArrayList<>();
        try {
            APIBIndPopTree eventPopTree = new APIBIndPopTree("formEvent", "常用事件", viewInstId, sourceClassName);
            for (CustomMenuItem customMenuItem : CustomMenuItem.values()) {
                APIBIndPopTree item = new APIBIndPopTree(customMenuItem, viewInstId, sourceClassName);
                item.addTagVar("sourceClassName", sourceClassName);
                item.addTagVar("viewInstId", viewInstId);
                eventPopTree.addChild(item);
            }
            popTrees.add(eventPopTree);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addBindButton")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addBindButton(String sourceClassName, String methodName, String FormEventTree, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {

            ViewEntityConfig config = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getSourceConfig().getMethodByName(methodName);
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) customMethodAPIBean.getView();
            if (FormEventTree != null && !FormEventTree.equals("")) {
                String[] menuIds = StringUtility.split(FormEventTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        customFormViewBean.getEvent().add(CustomFormEvent.valueOf(menuId));
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config.getSourceConfig());
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delBindButton")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delBindButton(String sourceClassName, String methodName, String id, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ViewEntityConfig config = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId);
            MethodConfig customMethodAPIBean = config.getSourceConfig().getMethodByName(methodName);
            CustomFormViewBean customFormViewBean = (CustomFormViewBean) customMethodAPIBean.getView();
            String[] menuIds = StringUtility.split(id, ";");
            for (String menuId : menuIds) {
                customFormViewBean.getEvent().remove(CustomFormEvent.valueOf(menuId));
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config.getSourceConfig());
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
