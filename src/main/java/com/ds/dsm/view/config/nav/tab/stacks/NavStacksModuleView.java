package com.ds.dsm.view.config.nav.tab.stacks;

import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.esd.tool.ui.enums.SelModeType;

@FormAnnotation(col = 2)
public class NavStacksModuleView {


    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;

    @CustomAnnotation(caption = "显示名称")
    String caption;

    @CustomAnnotation(caption = "选中方式")
    SelModeType selMode;

    @CustomAnnotation(caption = "占位符")
    Boolean togglePlaceholder;

    @CustomAnnotation(caption = "装载消息")
    Boolean animCollapse;

    @CustomAnnotation(caption = "动态销毁")
    Boolean dynDestory;

    String expression;
    @CustomAnnotation(caption = "默认值")
    String value;

    @CustomAnnotation(caption = "装载消息")
    String message;

    @CustomAnnotation(caption = "最大高度")
    Integer maxHeight;

    @CustomAnnotation(caption = "延迟注入")
    Boolean lazyAppend;

    @CustomAnnotation(caption = "表单提交")
    Boolean formField;

    @CustomAnnotation(caption = "值分割符")
    String valueSeparator;


    Class bindService;

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    public NavStacksModuleView() {

    }

    public NavStacksModuleView(NavTabsViewBean config) {
        this.viewInstId = config.getViewInstId();
        this.domainId=config.getDomainId();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName=config.getSourceClassName();
        this.methodName = config.getMethodName();
        this.imageClass = config.getImageClass();
        this.caption = config.getCaption();
        this.selMode = config.getSelMode();
        this.togglePlaceholder = config.getTogglePlaceholder();
        this.animCollapse = config.getAnimCollapse();
        this.dynDestory = config.getDynDestory();
        this.expression = config.getExpression();
        this.value = config.getValue();
        this.message = config.getMessage();
        this.maxHeight = config.getMaxHeight();
        this.lazyAppend = config.getLazyAppend();
        this.formField = config.getFormField();
        this.valueSeparator = config.getValueSeparator();
        this.bindService = config.getBindService();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }
    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }

    public Boolean getTogglePlaceholder() {
        return togglePlaceholder;
    }

    public void setTogglePlaceholder(Boolean togglePlaceholder) {
        this.togglePlaceholder = togglePlaceholder;
    }


    public Boolean getAnimCollapse() {
        return animCollapse;
    }

    public void setAnimCollapse(Boolean animCollapse) {
        this.animCollapse = animCollapse;
    }

    public Boolean getDynDestory() {
        return dynDestory;
    }

    public void setDynDestory(Boolean dynDestory) {
        this.dynDestory = dynDestory;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(Integer maxHeight) {
        this.maxHeight = maxHeight;
    }

    public Boolean getLazyAppend() {
        return lazyAppend;
    }

    public void setLazyAppend(Boolean lazyAppend) {
        this.lazyAppend = lazyAppend;
    }


    public Boolean getFormField() {
        return formField;
    }

    public void setFormField(Boolean formField) {
        this.formField = formField;
    }

    public String getValueSeparator() {
        return valueSeparator;
    }

    public void setValueSeparator(String valueSeparator) {
        this.valueSeparator = valueSeparator;
    }


    public Class getBindService() {
        return bindService;
    }

    public void setBindService(Class bindService) {
        this.bindService = bindService;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
