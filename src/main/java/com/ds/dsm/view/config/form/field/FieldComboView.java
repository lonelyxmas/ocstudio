package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.form.field.combo.service.ComboColorService;
import com.ds.dsm.view.config.form.field.service.FormComboBoxService;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.CustomImageType;

@BottomBarMenu
@FormAnnotation(col = 2, customService =FormComboBoxService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FieldComboView {

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "显示名称")
    Boolean selectable;


    @CustomAnnotation(caption = "背景图片尺寸")
    String imageBgSize;
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;
    @CustomAnnotation(caption = "字体图标")
    String iconFontCode;

    @CustomAnnotation(caption = "单元")
    String unit;
    @CustomAnnotation(caption = "单元集合")
    String units;

    @CustomAnnotation(caption = "输入类型")
    ComboInputType inputType;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;

    public FieldComboView() {

    }

    public FieldComboView(FieldFormConfig<ComboInputFieldBean,?> config) {
        ComboInputFieldBean inputFieldBean = config.getWidgetConfig();
        this.methodName = config.getSourceMethodName();
        this.viewClassName= config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.domainId = config.getDomainId();
        this.viewInstId = config.getDomainId();
        this.fieldname = config.getFieldname();


        this.imageBgSize = inputFieldBean.getImageBgSize();

        this.imageClass = inputFieldBean.getImageClass();

        this.iconFontCode = inputFieldBean.getIconFontCode();


        this.unit = inputFieldBean.getUnit();

        this.units = inputFieldBean.getUnits();


        this.inputType = inputFieldBean.getInputType();


    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getImageBgSize() {
        return imageBgSize;
    }

    public void setImageBgSize(String imageBgSize) {
        this.imageBgSize = imageBgSize;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getIconFontCode() {
        return iconFontCode;
    }

    public void setIconFontCode(String iconFontCode) {
        this.iconFontCode = iconFontCode;
    }


    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public ComboInputType getInputType() {
        return inputType;
    }

    public void setInputType(ComboInputType inputType) {
        this.inputType = inputType;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
