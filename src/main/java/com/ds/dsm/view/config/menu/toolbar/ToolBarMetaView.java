package com.ds.dsm.view.config.menu.toolbar;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.view.config.menu.toolbar.custom.CustomToolGridView;
import com.ds.dsm.view.config.menu.toolbar.menuclass.ToolBarGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.CustomMenu;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.ToolBarMenuBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RequestMapping("/dsm/view/config/menu/toolbar/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "按钮配置", imageClass = "spafont spa-icon-c-databinder")
public class ToolBarMetaView {
    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;

    @RequestMapping(method = RequestMethod.POST, value = "CustomToolBar")
    @ModuleAnnotation(caption = "常用按钮", imageClass = "spafont spa-icon-c-toolbar", dock = Dock.fill)
    @CustomAnnotation(index = 0)
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<CustomToolGridView>> getCustomToolBar(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<CustomToolGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);

            List<CustomMenu> customMenu = new ArrayList<>();

            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                if (customTreeViewBean != null) {
                    customMenu = customTreeViewBean.getToolBarMenu();
                } else {
                    customMenu = viewBean.getToolBarMenu();
                }
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                CustomTreeViewBean treeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                customMenu = treeViewBean.getToolBarMenu();
            } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                customMenu = formViewBean.getToolBarMenu();
            } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                customMenu = customGridViewBean.getToolBarMenu();
            }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();

                customMenu = customGalleryViewBean.getToolBarMenu();
            } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                customMenu = customGalleryViewBean.getToolBarMenu();
            } else if (methodAPIBean.getView() instanceof NavTabsViewBean) {
                NavTabsViewBean navTabsViewBean = (NavTabsViewBean) methodAPIBean.getView();
                customMenu = navTabsViewBean.getToolBarMenu();
            }

            resultModel = PageUtil.getDefaultPageList(customMenu, CustomToolGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "MenuClass")
    @ModuleAnnotation(caption = "扩展菜单", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @CustomAnnotation(index = 1)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<ToolBarGridView>> getMenuClass(String domainId, String methodName, String sourceClassName) {
        ListResultModel<List<ToolBarGridView>> result = new ListResultModel();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);

            List<ESDClass> esdClassList = new ArrayList<>();
            ToolBarMenuBean barMenuBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                if (customTreeViewBean != null) {
                    barMenuBean = customTreeViewBean.getToolBar();
                } else {
                    barMenuBean = viewBean.getToolBar();
                }
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                CustomTreeViewBean treeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                barMenuBean = treeViewBean.getToolBar();
            } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                barMenuBean = formViewBean.getToolBar();
            } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                barMenuBean = customGridViewBean.getToolBar();
            }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();

                barMenuBean = customGalleryViewBean.getToolBar();
            } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                barMenuBean = customGalleryViewBean.getToolBar();
            } else if (methodAPIBean.getView() instanceof NavTabsViewBean) {
                NavTabsViewBean navTabsViewBean = (NavTabsViewBean) methodAPIBean.getView();
                barMenuBean = navTabsViewBean.getToolBar();
            }


            if (barMenuBean != null && barMenuBean.getMenuClasses() != null) {
                Class[] clazzs = barMenuBean.getMenuClasses();
                for (Class clazz : clazzs) {
                    esdClassList.add(DSMFactory.getInstance().getClassManager().getAggEntityByName(clazz.getName(), domainId, false));
                }
            }
            result = PageUtil.getDefaultPageList(esdClassList, ToolBarGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
