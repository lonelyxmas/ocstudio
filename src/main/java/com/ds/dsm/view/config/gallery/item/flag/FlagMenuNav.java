package com.ds.dsm.view.config.gallery.item.flag;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.manager.view.BuildViewMenu;
import com.ds.dsm.view.config.grid.row.rowcmd.GridRowMenuConfigView;
import com.ds.dsm.view.config.grid.row.rowcmd.GridRowMenuMetaView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.gallery.GalleryFlagCmdBean;
import com.ds.esd.custom.gallery.annotation.GalleryFlagCmd;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.GridRowCmdBean;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@BottomBarMenu(menuClass = BuildViewMenu.class)
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save}, customService = {FlagService.class})
@RequestMapping("/dsm/view/config/gallery/flag/")
public class FlagMenuNav {

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;
    @CustomAnnotation(hidden = true, uid = true)
    public String sourceClassName;

    public FlagMenuNav() {

    }

    @MethodChinaName(cname = "基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "FlagConfig")
    @FormViewAnnotation
    @UIAnnotation(dock = Dock.top, height = "220")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<FlagMenuConfigView> getFlagConfig(String domainId, String sourceClassName, String methodName) {
        ResultModel<FlagMenuConfigView> resultModel = new ResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGalleryViewBean customGalleryViewBean=null;
            if (methodAPIBean.getView() instanceof NavGalleryViewBean){
                customGalleryViewBean=((NavGalleryViewBean)methodAPIBean.getView()).getGalleryViewBean();
            }else if(methodAPIBean.getView() instanceof NavGalleryViewBean){
                customGalleryViewBean= (CustomGalleryViewBean) methodAPIBean.getView();
            }


        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }


    @MethodChinaName(cname = "详细信息")
    @RequestMapping(method = RequestMethod.POST, value = "FlagMenuMetaView")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill)
    @ResponseBody
    public ResultModel<FlagMenuMetaView> getFlagMenuMetaView(String sourceClassName, String methodName, String domainId) {
        ResultModel<FlagMenuMetaView> result = new ResultModel<FlagMenuMetaView>();
        return result;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}

