package com.ds.dsm.view.config.form.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.FieldFormBean;
import com.ds.dsm.view.config.form.field.FieldView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.BaseFieldInfo;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.WidgetBean;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomFieldBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.field.combo.SimpleComboBean;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.json.JSONData;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/")
@MethodChinaName(cname = "字段管理", imageClass = "spafont spa-icon-c-comboinput")

public class FormFieldService {


    @RequestMapping(method = RequestMethod.POST, value = "AddField")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "添加字段", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.add}, callback = {CustomCallBack.Close, CustomCallBack.ReloadParent})
    @DialogAnnotation(width = "600", height = "380")
    @ResponseBody
    public ResultModel<FieldView> addField(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<FieldView> result = new ResultModel<FieldView>();
        FieldView moduleView = new FieldView();
        moduleView.setSourceClassName(sourceClassName);
        moduleView.setMethodName(methodName);
        moduleView.setDomainId(domainId);
        result.setData(moduleView);
        return result;
    }

    @MethodChinaName(cname = "清空配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "clearFieldForm")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clearFieldForm(String serviceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (serviceClassName != null && !serviceClassName.equals("")) {
                ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(serviceClassName, domainId);
                MethodConfig methodAPIBean = esdClassConfig.getMethodByName(methodName);
                CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                formViewBean.getFieldConfigMap().remove(fieldname);
                DSMFactory.getInstance().getViewManager().reSetViewEntityConfig(serviceClassName, domainId, viewInstId);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }


    @MethodChinaName(cname = "编辑字段配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFieldForm")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateFieldForm(@RequestBody FieldFormBean config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getSourceClassName();
            if (className != null && !className.equals("")) {

                ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(className, config.getDomainId());
                MethodConfig methodAPIBean = esdClassConfig.getMethodByName(config.getMethodName());
                CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                this.updateField(formViewBean, config);

                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    private void updateField(CustomFormViewBean formViewBean, FieldFormBean config) throws JDSException {
        //  CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
        FieldFormConfig fieldConfig = (FieldFormConfig) formViewBean.getFieldConfigMap().get(config.getFieldname());
        if (fieldConfig == null) {
            fieldConfig = formViewBean.createField(config.getFieldname(), config.getComponentType(), config.getInputType());
        }
        ComboInputType inputType = config.getInputType();
        ComponentType componentType = config.getComponentType();
        if (componentType.equals(ComponentType.ComboInput)) {
            WidgetBean widgetBean = fieldConfig.getWidgetConfig();
            if (widgetBean instanceof ComboInputFieldBean) {
                ComboBoxBean comboBoxBean = fieldConfig.getComboConfig();
                if (comboBoxBean.getInputType() == null || (inputType != null && !comboBoxBean.getInputType().equals(inputType))) {
                    SimpleComboBean fieldBean = new SimpleComboBean();
                    fieldBean.setInputType(inputType);
                    fieldConfig.setComboConfig(fieldBean);
                }
            } else {
                SimpleComboBean fieldBean = new SimpleComboBean();
                fieldBean.setInputType(inputType);
                fieldConfig.setComboConfig(fieldBean);
            }

        } else if (!fieldConfig.getComponentType().equals(componentType)) {
            BaseFieldInfo baseFieldInfo = (BaseFieldInfo) fieldConfig.getEsdField();
            baseFieldInfo.setComponentType(componentType);
            baseFieldInfo.initCombo(null);
            fieldConfig.setWidgetConfig(baseFieldInfo.getWidgetConfig());
        }
        fieldConfig.getFieldBean().setComponentType(componentType);
        fieldConfig.setComponentType(componentType);
        fieldConfig.getAggConfig().setCaption(config.getCaption());
        fieldConfig.getAggConfig().setComponentType(componentType);
        CustomFieldBean customFieldBean = fieldConfig.getCustomBean();
        if (customFieldBean == null) {
            customFieldBean = AnnotationUtil.fillDefaultValue(CustomAnnotation.class, new CustomFieldBean());
        }
        customFieldBean.setDisabled(config.getDisabled());
        customFieldBean.setHidden(config.getColHidden());
        customFieldBean.setReadonly(config.getReadonly());
        fieldConfig.setCustomBean(customFieldBean);
    }


    @MethodChinaName(cname = "批量保存")
    @RequestMapping(method = RequestMethod.POST, value = "updateAllFieldForm")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.saveAllRow)
    public @ResponseBody
    ResultModel<Boolean> updateAllFieldForm(@JSONData List<FieldFormBean> rows, String sourceClassName, String domainId, String viewInstId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig esdClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = esdClassConfig.getMethodByName(methodName);
            CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
            for (FieldFormBean config : rows) {
                this.updateField(formViewBean, config);
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(esdClassConfig);


        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

}
