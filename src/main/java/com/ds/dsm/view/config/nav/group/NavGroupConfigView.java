package com.ds.dsm.view.config.nav.group;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.FieldFormGridInfo;
import com.ds.dsm.view.config.nav.items.ModuleItemsInfo;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.enums.VAlignType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/group/")
@MethodChinaName(cname = "配置信息")
@ButtonViewsAnnotation(barLocation = BarLocationType.left, barVAlign = VAlignType.top)
public class NavGroupConfigView {
    @CustomAnnotation(uid = true, hidden = true)
    private String esdclass;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    public NavGroupConfigView() {

    }


    @RequestMapping(method = RequestMethod.POST, value = "GroupInfoGroup")
    @NavGroupViewAnnotation(reSetUrl = "clear", saveUrl = "updateGroupBase", autoSave = true)
    @ModuleAnnotation(dock = Dock.fill, caption = "Folding配置",  imageClass = "spafont spa-icon-project")
    @CustomAnnotation( index = 0)
    @ResponseBody
    public ResultModel<NavGroupInfoGroup> getGroupInfoGroup(String sourceClassName, String methodName, String viewInstId) {
        ResultModel<NavGroupInfoGroup> result = new ResultModel<NavGroupInfoGroup>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldFormList")
    @GridViewAnnotation
    @ModuleAnnotation( imageClass = "spafont spa-icon-c-comboinput", caption = "表单字段")
    @APIEventAnnotation(autoRun = true)
    @CustomAnnotation( index = 1)
    @ResponseBody
    public ListResultModel<List<FieldFormGridInfo>> getFieldFormList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldFormGridInfo>> cols = new ListResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            NavGroupViewBean navTabsViewBean = (NavGroupViewBean) customMethodAPIBean.getView();
            Set<String> fieldNames = navTabsViewBean.getFieldNames();
            List<FieldFormConfig> fields = new ArrayList<>();
            for (String fieldName : fieldNames) {
                FieldFormConfig fieldFormConfig = (FieldFormConfig) navTabsViewBean.getFieldConfigMap().get(fieldName);
                if (fieldFormConfig != null) {

                    fields.add(fieldFormConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldFormGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }

    @RequestMapping(method = RequestMethod.POST, value = "ModuleList")
    @GridViewAnnotation
    @ModuleAnnotation( imageClass = "spafont spa-icon-conf", caption = "模块列表")
    @CustomAnnotation( index = 2)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<ModuleItemsInfo>> getModuleList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<ModuleItemsInfo>> cols = new ListResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName,  viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            NavGroupViewBean navTabsViewBean = (NavGroupViewBean) customMethodAPIBean.getView();
            List<FieldModuleConfig> fields = new ArrayList<>();
            List<FieldModuleConfig> items = navTabsViewBean.getItems();
            for (FieldModuleConfig fieldFormConfig : items) {
                if (fieldFormConfig != null) {

                    fields.add(fieldFormConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, ModuleItemsInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getEsdclass() {
        return esdclass;
    }

    public void setEsdclass(String esdclass) {
        this.esdclass = esdclass;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
}
