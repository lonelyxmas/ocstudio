package com.ds.dsm.view.config.grid.row.rowcmd.custom;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.GridRowCmdBean;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.enums.GridRowMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.AnnotationUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping(path = "/dsm/view/config/grid/rowmenu/")
@MethodChinaName(cname = "常用行按钮", imageClass = "spafont spa-icon-c-databinder")
public class CustomGridRowMenuService {

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "常用行按钮")
    @RequestMapping("CustomGridRowMenuTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<CustomGridRowMenuPopTree>> getCustomGridRowMenuTree(String sourceClassName, String methodName, String domainId, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<CustomGridRowMenuPopTree>> model = new TreeListResultModel<>();
        List<CustomGridRowMenuPopTree> popTrees = new ArrayList<>();
        CustomGridRowMenuPopTree menuPopTree = new CustomGridRowMenuPopTree("rowMenu", "常用按钮", "rowMenu");
        List<CustomGridRowMenuPopTree> menuTrees = TreePageUtil.fillObjs(Arrays.asList(GridRowMenu.values()), CustomGridRowMenuPopTree.class);
        menuPopTree.setSub(menuTrees);
        popTrees.add(menuPopTree);
        model.setData(popTrees);

        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addCustomRowMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addCustomRowMenu(String sourceClassName, String methodName, String CustomGridRowMenuTree, String domainId, String childViewId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);

            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            GridRowCmdBean barMenuBean = customGridViewBean.getRowCmdBean();
            if (CustomGridRowMenuTree != null && !CustomGridRowMenuTree.equals("")) {
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(GridRowCmd.class, new GridRowCmdBean());
                }
                String[] menuIds = StringUtility.split(CustomGridRowMenuTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        barMenuBean.getRowMenu().add(GridRowMenu.valueOf(menuId));
                    }
                }
                customGridViewBean.setRowCmdBean(barMenuBean);
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(customESDClassAPIBean);
            }

        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;

    }


    @RequestMapping("delCustomContextMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delCustomContextMenu(String sourceClassName, String methodName, String type, String bar, String domainId, String childViewId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            GridRowCmdBean barMenuBean = customGridViewBean.getRowCmdBean();
            if (barMenuBean != null) {
                String[] menuIds = StringUtility.split(type, ";");
                for (String menuId : menuIds) {
                    barMenuBean.getRowMenu().remove(GridRowMenu.valueOf(menuId));
                }
            }

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
