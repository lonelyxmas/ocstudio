package com.ds.dsm.view.config.custom.panel;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.nav.PanelItemBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/custom/panel/")
public class PanelService {


    @RequestMapping(method = RequestMethod.POST, value = "PanelConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<PanelView> getPanelConfig(String sourceClassName, String sourceMethodName, String groupId, String domainId, String viewInstId) {
        ResultModel<PanelView> resultModel = new ResultModel();
        try {
            ViewEntityConfig config = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getSourceConfig().getMethodByName(sourceMethodName);
            NavBaseViewBean baseViewBean = (NavBaseViewBean) customMethodAPIBean.getView();
            PanelItemBean panelItemBean = baseViewBean.getItemBean(groupId);
            resultModel.setData(new PanelView(panelItemBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadPanelItem")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<PanelConfigTree>> loadPanelItem(String viewInstId, String domainId, String sourceClassName, String sourceMethodName, String groupId) {
        TreeListResultModel<List<PanelConfigTree>> resultModel = new TreeListResultModel<List<PanelConfigTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(PanelConfigItems.values()), PanelConfigTree.class);
        return resultModel;
    }

}
