package com.ds.dsm.view.config.form;

import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/form/action/")
public class ViewFormActionService {


    @RequestMapping(method = RequestMethod.POST, value = "FormEventMenuItems")
    @ModuleAnnotation(dynLoad = true, caption = "动作事件", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<FormNavTree>> getFormEventMenuItems(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<FormNavTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(FormEventMenuItems.values()), FormNavTree.class);
        return result;
    }

}
