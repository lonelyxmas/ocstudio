package com.ds.dsm.view.config.tree.rowcmd.custom;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.tree.enums.TreeMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {CustomTreeRowMenuService.class}, selMode = SelModeType.multibycheckbox)
@PopTreeAnnotation(caption = "添加按钮")
public class CustomTreeRowMenuPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String childViewId;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String bar;

    @CustomAnnotation(pid = true)
    String euClassName;


    @CustomAnnotation(pid = true)
    String sourceClassName;

    @CustomAnnotation(pid = true)
    String methodName;

    public CustomTreeRowMenuPopTree(String id, String caption, String bar) {
        super(id, caption);
        this.bar = bar;

    }

    @TreeItemAnnotation()
    public CustomTreeRowMenuPopTree(TreeMenu menu, String domainId, String childViewId, String sourceClassName, String methodName) {
        super(menu.getType(), menu.getType() + "(" + menu.getCaption() + ")", menu.getImageClass());
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;
        this.domainId = domainId;
        this.childViewId = childViewId;


    }

    @Override
    public String getEuClassName() {
        return euClassName;
    }

    @Override
    public void setEuClassName(String euClassName) {
        this.euClassName = euClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getChildViewId() {
        return childViewId;
    }

    public void setChildViewId(String childViewId) {
        this.childViewId = childViewId;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }


}
