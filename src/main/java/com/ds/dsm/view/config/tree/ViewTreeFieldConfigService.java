package com.ds.dsm.view.config.tree;

import com.ds.config.ResultModel;
import com.ds.dsm.repository.entity.FieldModuleView;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/tree/module/")
public class ViewTreeFieldConfigService {

    @RequestMapping(method = RequestMethod.POST, value = "TreeFieldModule")
    @NavGroupViewAnnotation(autoSave = true)
    @ModuleAnnotation(caption = "子模块信息")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<FieldModuleView> getTreeFieldModule(String sourceClassName, String projectId, String fieldname) {
        return new ResultModel<>();
    }

}
