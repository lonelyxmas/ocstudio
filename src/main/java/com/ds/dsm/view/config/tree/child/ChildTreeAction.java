package com.ds.dsm.view.config.tree.child;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.foldingtree.NavFoldingTreeViewBean;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.form.pop.PopTreeViewBean;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/tree/child/item/")
public class ChildTreeAction {


    @MethodChinaName(cname = "向上")
    @RequestMapping(method = RequestMethod.POST, value = "moveUP")
    @CustomAnnotation(imageClass = "spafont spa-icon-move-up", index = 0)
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> moveUP(String sourceClassName, String childViewId, String id, String sourceMethodName, String domainId, String viewInstId) {

        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = config.getMethodByName(sourceMethodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }

            if (childViewId != null) {
                customTreeViewBean = customTreeViewBean.getChildBean(childViewId);
            }
            ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(id);

            List<ChildTreeViewBean> customTreeViewBeans = customTreeViewBean.getChildTreeBeans();

            Integer index = customTreeViewBeans.indexOf(childTreeViewBean);

            childTreeViewBean.setIndex(index - 1);

            ChildTreeViewBean lastTreeBean = customTreeViewBeans.get(index - 1);
            lastTreeBean.setIndex(index);

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (Exception e) {
            ErrorResultModel errorResult = new ErrorResultModel();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;

    }


    @MethodChinaName(cname = "向下")
    @RequestMapping(method = RequestMethod.POST, value = "moveDown")
    @CustomAnnotation(imageClass = "spafont spa-icon-move-down", index = 1)
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> moveDown(String sourceClassName, String childViewId, String id, String sourceMethodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = tableConfig.getMethodByName(sourceMethodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                NavTreeViewBean baseViewBean = (NavTreeViewBean) customMethodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean baseViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeViewBean();
            } else if (customMethodAPIBean.getView() instanceof PopTreeViewBean) {
                PopTreeViewBean baseViewBean = (PopTreeViewBean) customMethodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeViewBean();
            } else if (customMethodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) customMethodAPIBean.getView();
            }


            if (childViewId != null) {
                customTreeViewBean = customTreeViewBean.getChildBean(childViewId);
            }
            ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(id);

            List<ChildTreeViewBean> customTreeViewBeans = customTreeViewBean.getChildTreeBeans();

            Integer index = customTreeViewBeans.indexOf(childTreeViewBean);
            childTreeViewBean.setIndex(index + 1);

            ChildTreeViewBean lastTreeBean = customTreeViewBeans.get(index + 1);
            lastTreeBean.setIndex(index);

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}

