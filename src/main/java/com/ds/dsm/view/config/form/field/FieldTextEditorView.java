package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.form.field.service.FormSVGPaperService;
import com.ds.dsm.view.config.form.field.service.FormTextEditorService;
import com.ds.esd.custom.annotation.CodeEditorAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.TextEditorFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 2, customService =FormTextEditorService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FieldTextEditorView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;

    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;
    @CustomAnnotation(caption = "延迟加载")
    Boolean dynLoad;
    @CustomAnnotation(caption = "是否可选中")
    Boolean selectable;
    @CustomAnnotation(caption = "值")
    String value;
    @CustomAnnotation(caption = "宽")
    String width;
    @CustomAnnotation(caption = "高")
    String height;
    @CodeEditorAnnotation(codeType="css")
    @CustomAnnotation(caption = "容器样式")
    String frameStyle;
    @CustomAnnotation(caption = "命令按钮集合")
    String cmdList;
    @CustomAnnotation(caption = "命令按钮过滤")
    String cmdFilter;
    @CustomAnnotation(caption = "文本类型")
    String codeType;

    @CustomAnnotation(caption = "标签间距")
    Integer labelGap;


    public FieldTextEditorView() {

    }

    public FieldTextEditorView(FieldFormConfig<TextEditorFieldBean,?> config) {
        TextEditorFieldBean inputFieldBean = config.getWidgetConfig();
        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(inputFieldBean));
        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId=config.getDomainId();

        this.fieldname = config.getFieldname();

    }



    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getFrameStyle() {
        return frameStyle;
    }

    public void setFrameStyle(String frameStyle) {
        this.frameStyle = frameStyle;
    }

    public String getCmdList() {
        return cmdList;
    }

    public void setCmdList(String cmdList) {
        this.cmdList = cmdList;
    }

    public String getCmdFilter() {
        return cmdFilter;
    }

    public void setCmdFilter(String cmdFilter) {
        this.cmdFilter = cmdFilter;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public Integer getLabelGap() {
        return labelGap;
    }

    public void setLabelGap(Integer labelGap) {
        this.labelGap = labelGap;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
