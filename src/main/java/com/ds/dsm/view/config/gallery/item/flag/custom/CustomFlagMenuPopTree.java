package com.ds.dsm.view.config.gallery.item.flag.custom;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.enums.GalleryFlagMenu;
import com.ds.esd.custom.gallery.enums.GalleryMenu;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {CustomFlagMenuService.class}, selMode = SelModeType.multibycheckbox)
@PopTreeAnnotation(caption = "添加按钮")
public class CustomFlagMenuPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String bar;

    @CustomAnnotation(pid = true)
    String euClassName;


    @CustomAnnotation(pid = true)
    String sourceClassName;

    @CustomAnnotation(pid = true)
    String methodName;

    public CustomFlagMenuPopTree(String id, String caption, String bar) {
        super(id, caption);
        this.bar = bar;

    }

    @TreeItemAnnotation()
    public CustomFlagMenuPopTree(GalleryFlagMenu menu, String domainId, String sourceClassName, String methodName) {
        super(menu.getType(), menu.getType() + "(" + menu.getCaption() + ")", menu.getImageClass());
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;
        this.domainId = domainId;


    }

    @Override
    public String getEuClassName() {
        return euClassName;
    }

    @Override
    public void setEuClassName(String euClassName) {
        this.euClassName = euClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }


}
