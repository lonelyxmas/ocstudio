package com.ds.dsm.view.config.menu.bottommenu.custom;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.menu.menubar.custom.CustomMenubarPopTree;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping(path = "/dsm/view/config/form/bottom/")
public class CustomBottomMenuService {


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "表单按钮")
    @RequestMapping("CustomBottomMenuTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<CustomBottomPopTree>> getCustomBottomMenuTree(String sourceClassName, String methodName, String domainId, String viewInstId) {
        TreeListResultModel<List<CustomBottomPopTree>> model = new TreeListResultModel<>();
        List<CustomBottomPopTree> popTrees = new ArrayList<>();
        CustomBottomPopTree menuPopTree = new CustomBottomPopTree("bottombar", "表单按钮", "FormBottomTree");
        ApiClassConfig tableConfig = null;
        List<CustomBottomPopTree> menuTrees = TreePageUtil.fillObjs(Arrays.asList(CustomFormMenu.values()), CustomBottomPopTree.class);
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                menuTrees = TreePageUtil.fillObjs(Arrays.asList(GridMenu.values()), CustomBottomPopTree.class);
            } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                menuTrees = TreePageUtil.fillObjs(Arrays.asList(GridMenu.values()), CustomBottomPopTree.class);
            } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                menuTrees = TreePageUtil.fillObjs(Arrays.asList(GridMenu.values()), CustomBottomPopTree.class);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }
        menuPopTree.setSub(menuTrees);
        popTrees.add(menuPopTree);
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addCustomBottomMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addCustomBottomMenu(String sourceClassName, String methodName, String CustomBottomMenuTree, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            String[] menuIds = StringUtility.split(CustomBottomMenuTree, ";");
            for (String menuId : menuIds) {
                if (menuId != null && !menuIds.equals("")) {
                    if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                        NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                        NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                        CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                        if (customTreeViewBean != null) {
                            customTreeViewBean.getBottombarMenu().add(CustomFormMenu.valueOf(menuId));
                        } else {
                            viewBean.getBottombarMenu().add(CustomFormMenu.valueOf(menuId));
                        }
                    } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                        CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                        formViewBean.getBottombarMenu().add(CustomFormMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                        CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                        customTreeViewBean.getBottombarMenu().add(CustomFormMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                        CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                        customGridViewBean.getBottombarMenu().add(GridMenu.valueOf(menuId));
                    }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                        CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
                        customGalleryViewBean.getBottombarMenu().add(GridMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                        CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                        customGalleryViewBean.getBottombarMenu().add(GridMenu.valueOf(menuId));

                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (
                JDSException e)

        {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delCustomBottomMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delCustomBottomMenu(String sourceClassName, String methodName, String type, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            String[] menuIds = StringUtility.split(type, ";");
            for (String menuId : menuIds) {
                if (menuId != null && !menuIds.equals("")) {
                    if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                        NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                        NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                        CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                        if (customTreeViewBean != null) {
                            customTreeViewBean.getBottombarMenu().remove(CustomFormMenu.valueOf(menuId));
                        } else {
                            viewBean.getBottombarMenu().remove(CustomFormMenu.valueOf(menuId));
                        }
                    } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                        CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                        formViewBean.getBottombarMenu().remove(CustomFormMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                        CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                        customTreeViewBean.getBottombarMenu().remove(CustomFormMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                        CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                        customGridViewBean.getBottombarMenu().remove(CustomFormMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                        CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                        customGalleryViewBean.getBottombarMenu().remove(CustomFormMenu.valueOf(menuId));
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
