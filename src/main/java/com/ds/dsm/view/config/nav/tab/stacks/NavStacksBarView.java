package com.ds.dsm.view.config.nav.tab.stacks;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;
import com.ds.esd.tool.ui.enums.VAlignType;

@FormAnnotation(col = 2)
public class NavStacksBarView {

    @CustomAnnotation(caption = "居中")
    TagCmdsAlign tagCmdsAlign;

    @CustomAnnotation(caption = "分组显示")
    Boolean group;


    @CustomAnnotation(caption = "单击展开")
    Boolean singleOpen;

    @CustomAnnotation(caption = "默认折叠")
    Boolean iniFold;

    @CustomAnnotation(caption = "配置按钮")
    String optBtn;

    @CustomAnnotation(caption = "默认选中最后")
    Boolean activeLast;

    @CustomAnnotation(caption = "关闭按钮")
    Boolean closeBtn;

    @CustomAnnotation(caption = "弹出按钮")
    Boolean popBtn;

    @CustomAnnotation(caption = "对齐方式")
    BarLocationType barLocation;

    @CustomAnnotation(caption = "横向对齐")
    HAlignType barHAlign;

    @CustomAnnotation(caption = "纵向对齐")
    VAlignType barVAlign;

    @CustomAnnotation(caption = "工具栏大小")
    String barSize;

    @CustomAnnotation(caption = "侧栏大小")
    String sideBarSize;

    @CustomAnnotation(caption = "是否展开")
    SideBarStatusType sideBarStatus;

    Class bindService;

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    public NavStacksBarView() {

    }

    public NavStacksBarView(NavTabsViewBean config) {
        this.viewInstId = config.getViewInstId();
        this.domainId=config.getDomainId();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getMethodName();

        this.tagCmdsAlign = config.getTagCmdsAlign();
        this.group = config.getGroup();

        this.singleOpen = config.getSingleOpen();
        this.iniFold = config.getIniFold();
        this.optBtn = config.getOptBtn();

        this.activeLast = config.getActiveLast();

        this.closeBtn = config.getCloseBtn();
        this.popBtn = config.getPopBtn();


        this.barHAlign = config.getBarHAlign();
        this.barVAlign = config.getBarVAlign();
        this.barSize = config.getBarSize();
        this.sideBarSize = config.getSideBarSize();
        this.sideBarStatus = config.getSideBarStatus();
        this.bindService = config.getBindService();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public TagCmdsAlign getTagCmdsAlign() {
        return tagCmdsAlign;
    }

    public void setTagCmdsAlign(TagCmdsAlign tagCmdsAlign) {
        this.tagCmdsAlign = tagCmdsAlign;
    }

    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }


    public Boolean getSingleOpen() {
        return singleOpen;
    }

    public void setSingleOpen(Boolean singleOpen) {
        this.singleOpen = singleOpen;
    }

    public Boolean getIniFold() {
        return iniFold;
    }

    public void setIniFold(Boolean iniFold) {
        this.iniFold = iniFold;
    }

    public String getOptBtn() {
        return optBtn;
    }

    public void setOptBtn(String optBtn) {
        this.optBtn = optBtn;
    }


    public Boolean getActiveLast() {
        return activeLast;
    }

    public void setActiveLast(Boolean activeLast) {
        this.activeLast = activeLast;
    }

    public Boolean getCloseBtn() {
        return closeBtn;
    }

    public void setCloseBtn(Boolean closeBtn) {
        this.closeBtn = closeBtn;
    }

    public Boolean getPopBtn() {
        return popBtn;
    }

    public void setPopBtn(Boolean popBtn) {
        this.popBtn = popBtn;
    }


    public BarLocationType getBarLocation() {
        return barLocation;
    }

    public void setBarLocation(BarLocationType barLocation) {
        this.barLocation = barLocation;
    }

    public HAlignType getBarHAlign() {
        return barHAlign;
    }

    public void setBarHAlign(HAlignType barHAlign) {
        this.barHAlign = barHAlign;
    }

    public VAlignType getBarVAlign() {
        return barVAlign;
    }

    public void setBarVAlign(VAlignType barVAlign) {
        this.barVAlign = barVAlign;
    }

    public String getBarSize() {
        return barSize;
    }

    public void setBarSize(String barSize) {
        this.barSize = barSize;
    }

    public String getSideBarSize() {
        return sideBarSize;
    }

    public void setSideBarSize(String sideBarSize) {
        this.sideBarSize = sideBarSize;
    }

    public SideBarStatusType getSideBarStatus() {
        return sideBarStatus;
    }

    public void setSideBarStatus(SideBarStatusType sideBarStatus) {
        this.sideBarStatus = sideBarStatus;
    }

    public Class getBindService() {
        return bindService;
    }

    public void setBindService(Class bindService) {
        this.bindService = bindService;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

}
