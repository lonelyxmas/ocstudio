package com.ds.dsm.view.config.custom.panel.btn;

import com.alibaba.fastjson.JSONObject;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.RichEditorAnnotation;
import com.ds.esd.custom.bean.CustomDivBean;
import com.ds.esd.custom.bean.nav.BtnBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.nav.PanelItemBean;
import com.ds.esd.tool.ui.enums.OverflowType;
import net.sf.cglib.beans.BeanMap;

@FormAnnotation(col = 2)
public class BtnView {

    @CustomAnnotation(caption = "Info按钮")
    Boolean infoBtn;
    @CustomAnnotation(caption = "设置按钮")
    Boolean optBtn;
    @CustomAnnotation(caption = "切换按钮")
    Boolean toggleBtn;
    @CustomAnnotation(caption = "刷新按钮")
    Boolean refreshBtn;
    @CustomAnnotation(caption = "关闭按钮")
    Boolean closeBtn;
    @CustomAnnotation(caption = "弹出按钮")
    Boolean popBtn;

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String entityClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    @CustomAnnotation(hidden = true, uid = true)
    String sourceMethodName;

    public BtnView() {

    }

    public BtnView(PanelItemBean config) {

        BtnBean btnBean = config.getPanelBean().getBtnBean();
        if (btnBean == null) {
            btnBean = new BtnBean();
        }
        String json = JSONObject.toJSONString(btnBean);
        BeanMap.create(this).putAll(JSONObject.parseObject(json));
        this.viewInstId = config.getViewInstId();
        this.domainId = config.getDomainId();
        this.entityClassName = config.getEntityClassName();
        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getSourceMethodName();
        this.sourceMethodName = config.getSourceMethodName();


    }

    public Boolean getInfoBtn() {
        return infoBtn;
    }

    public void setInfoBtn(Boolean infoBtn) {
        this.infoBtn = infoBtn;
    }

    public Boolean getOptBtn() {
        return optBtn;
    }

    public void setOptBtn(Boolean optBtn) {
        this.optBtn = optBtn;
    }

    public Boolean getToggleBtn() {
        return toggleBtn;
    }

    public void setToggleBtn(Boolean toggleBtn) {
        this.toggleBtn = toggleBtn;
    }

    public Boolean getRefreshBtn() {
        return refreshBtn;
    }

    public void setRefreshBtn(Boolean refreshBtn) {
        this.refreshBtn = refreshBtn;
    }

    public Boolean getCloseBtn() {
        return closeBtn;
    }

    public void setCloseBtn(Boolean closeBtn) {
        this.closeBtn = closeBtn;
    }

    public Boolean getPopBtn() {
        return popBtn;
    }

    public void setPopBtn(Boolean popBtn) {
        this.popBtn = popBtn;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

}
