package com.ds.dsm.view.config.menu;

import com.ds.common.JDSException;
import com.ds.dsm.view.config.menu.service.APIBindButtonService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = {APIBindButtonService.class}, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@PopTreeAnnotation(caption = "绑定按钮")
public class APIBIndPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String euClassName;

    @CustomAnnotation(pid = true)
    String domainId;


    public APIBIndPopTree(String id, String caption, String viewInstId, String className) {
        super(id, caption);
        for (CustomMenuItem item : CustomMenuItem.values()) {
            try {
                this.addChild(new APIBIndPopTree(item, viewInstId, className));
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
    }

    public APIBIndPopTree(CustomMenuItem item, String domainId, String euClassName) throws JDSException {
        super(item.name(), item.getMethodName() + "(" + item.getMenu().caption() + ")");

        this.addTagVar("euClassName", euClassName);
        this.addTagVar("domainId", domainId);
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

}
