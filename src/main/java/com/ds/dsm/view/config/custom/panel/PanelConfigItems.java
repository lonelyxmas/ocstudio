package com.ds.dsm.view.config.custom.panel;

import com.ds.dsm.view.config.custom.panel.btn.BtnService;
import com.ds.dsm.view.config.custom.panel.div.DivService;
import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.esd.tool.ui.enums.CustomImageType;

public enum PanelConfigItems implements TreeItem {
    Div("Div层信息", CustomImageType.div.getImageClass(), DivService.class, false, false, false),
    Btn("面板按钮", CustomImageType.button.getImageClass(), BtnService.class, false, false, false);

    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    PanelConfigItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
