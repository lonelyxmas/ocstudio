package com.ds.dsm.view.config.form.field.list;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.field.RadioBoxFieldBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/list/")
@MethodChinaName(cname = "RadioBox集合", imageClass = "spafont spa-icon-c-radiobox")

public class FormRadioService {

    @RequestMapping(method = RequestMethod.POST, value = "Radio")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-radiobox", caption = "RadioBox集合", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<FieldRadioView> getRadio(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<FieldRadioView> result = new ResultModel<FieldRadioView>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig<RadioBoxFieldBean, ?> formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new FieldRadioView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
