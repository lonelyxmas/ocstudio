package com.ds.dsm.view.config.form;

import com.ds.dsm.view.config.service.FormConfigService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.StretchType;

@BottomBarMenu()
@FormAnnotation(
        bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet}, customService = FormConfigService.class)
public class FormRowView {

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    @CustomAnnotation(readonly = true, caption = "方法名称")
    String methodName;

    @CustomAnnotation(caption = "默认行高")
    Integer defaultRowHeight;

    @CustomAnnotation(caption = "延伸行高方式")
    StretchType stretchHeight;

    @CustomAnnotation(caption = "默认行大小")
    Integer defaultRowSize;


    @CustomAnnotation(caption = "列头宽度")
    Integer rowHeaderWidth;


    @CustomAnnotation(caption = "显示行头")
    Boolean floatHandler;

    public FormRowView() {

    }

    public FormRowView(CustomFormViewBean formConfig) {
        this.viewClassName = formConfig.getViewClassName();
        this.sourceClassName = formConfig.getSourceClassName();
        this.defaultRowHeight = formConfig.getDefaultRowHeight();
        this.defaultRowSize = formConfig.getDefaultRowSize();
        this.stretchHeight = formConfig.getStretchHeight();
        this.floatHandler = formConfig.getFloatHandler();
        this.rowHeaderWidth = formConfig.getRowHeaderWidth();
        this.viewInstId = formConfig.getViewInstId();
        this.domainId = formConfig.getDomainId();
        this.methodName = formConfig.getMethodName();

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public Boolean getFloatHandler() {
        return floatHandler;
    }

    public Integer getDefaultRowHeight() {
        return defaultRowHeight;
    }

    public void setDefaultRowHeight(Integer defaultRowHeight) {
        this.defaultRowHeight = defaultRowHeight;
    }

    public Integer getRowHeaderWidth() {
        return rowHeaderWidth;
    }

    public void setRowHeaderWidth(Integer rowHeaderWidth) {
        this.rowHeaderWidth = rowHeaderWidth;
    }

    public Integer getDefaultRowSize() {
        return defaultRowSize;
    }

    public void setDefaultRowSize(Integer defaultRowSize) {
        this.defaultRowSize = defaultRowSize;
    }

    public StretchType getStretchHeight() {
        return stretchHeight;
    }

    public void setStretchHeight(StretchType stretchHeight) {
        this.stretchHeight = stretchHeight;
    }

    public Boolean isFloatHandler() {
        return floatHandler;
    }

    public void setFloatHandler(Boolean floatHandler) {
        this.floatHandler = floatHandler;
    }
}
