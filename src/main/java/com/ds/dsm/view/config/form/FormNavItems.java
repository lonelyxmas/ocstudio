package com.ds.dsm.view.config.form;

import com.ds.esd.custom.tree.enums.TreeItem;

public enum FormNavItems implements TreeItem {
    FormInfo("模块配置", "spafont spa-icon-c-cssbox", ViewFormInfoService.class, false, false, false),
    FormEventGroup("动作事件", "spafont spa-icon-event", ViewFormActionService.class, false, false, false),
    FieldFormList("字段子域", "spafont spa-icon-c-comboinput", ViewFormFieldsService.class, true, true, true),
    ModuleList("子模块", "spafont spa-icon-conf", ViewFormModulesService.class, true, true, true);
    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    FormNavItems(String name, String imageClass, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
