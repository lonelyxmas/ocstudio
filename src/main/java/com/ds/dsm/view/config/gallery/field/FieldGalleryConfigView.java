package com.ds.dsm.view.config.gallery.field;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldGalleryConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(col = 2, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet}, customService = GalleryItemService.class)
public class FieldGalleryConfigView {

    @Pid
    String viewInstId;

    @Pid
    String domainId;
    @Pid
    String methodName;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;


    @Pid
    String sourceClassName;

    @CustomAnnotation(caption = "字段名", uid = true)
    String fieldname;

    @CustomAnnotation(caption = "显示名称", readonly = true)
    String caption;
    @Required
    @CustomAnnotation(caption = "数据类型")
    ComboInputType inputType;

    @CustomAnnotation(caption = "是否主键")
    Boolean uid;


    @CustomAnnotation(caption = "隐藏")
    Boolean hidden;


    public FieldGalleryConfigView() {

    }

    public FieldGalleryConfigView(FieldGalleryConfig config) {

        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getSourceMethodName();
        this.viewClassName= config.getViewClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();

        this.caption = config.getCaption();
        this.fieldname = config.getFieldname();
        this.hidden = config.getColHidden();
        this.uid = config.getUid();



    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public Boolean getUid() {
        return uid;
    }

    public void setUid(Boolean uid) {
        this.uid = uid;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public ComboInputType getInputType() {
        return inputType;
    }

    public void setInputType(ComboInputType inputType) {
        this.inputType = inputType;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }


}
