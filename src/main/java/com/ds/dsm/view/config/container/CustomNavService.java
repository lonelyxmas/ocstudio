package com.ds.dsm.view.config.container;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.gallery.GalleryConfigTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/container/")
public class CustomNavService {


    @RequestMapping(method = RequestMethod.POST, value = "containerNav")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<GalleryConfigTree>> getViewGalleryNavConfig(String domainId, String sourceClassName, String sourceMethodName, String methodName, String viewInstId) {
        TreeListResultModel<List<GalleryConfigTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(CustomViewItems.values()), GalleryConfigTree.class);
        return result;
    }

}
