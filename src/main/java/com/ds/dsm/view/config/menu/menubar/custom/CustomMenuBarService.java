package com.ds.dsm.view.config.menu.menubar.custom;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping(path = "/dsm/view/config/form/menu/")
public class CustomMenuBarService {


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "通用菜单项")
    @RequestMapping("CustomMenuBarTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<CustomMenubarPopTree>> getCustomMenuBarTree(String sourceClassName, String methodName, String domainId, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<CustomMenubarPopTree>> model = new TreeListResultModel<>();
        List<CustomMenubarPopTree> popTrees = new ArrayList<>();
        CustomMenubarPopTree menuPopTree = new CustomMenubarPopTree("formMenu", "常用按钮", "FormMenuTree");
        ApiClassConfig tableConfig = null;
        List<CustomMenubarPopTree> menuTrees = TreePageUtil.fillObjs(Arrays.asList(CustomFormMenu.values()), CustomMenubarPopTree.class);
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                menuTrees = TreePageUtil.fillObjs(Arrays.asList(GridMenu.values()), CustomMenubarPopTree.class);
            } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                menuTrees = TreePageUtil.fillObjs(Arrays.asList(GridMenu.values()), CustomMenubarPopTree.class);
            } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                menuTrees = TreePageUtil.fillObjs(Arrays.asList(GridMenu.values()), CustomMenubarPopTree.class);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }


        menuPopTree.setSub(menuTrees);
        popTrees.add(menuPopTree);
        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addCustomMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addCustomMenu(String sourceClassName, String methodName, String CustomMenuBarTree, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            if (CustomMenuBarTree != null && !CustomMenuBarTree.equals("")) {
                MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
                String[] menuIds = StringUtility.split(CustomMenuBarTree, ";");
                for (String menuId : menuIds) {
                    if (menuId != null && !menuIds.equals("")) {
                        if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                            NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                            NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                            CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                            if (customTreeViewBean != null) {
                                customTreeViewBean.getCustomMenu().add(CustomFormMenu.valueOf(menuId));
                            } else {
                                viewBean.getCustomMenu().add(CustomFormMenu.valueOf(menuId));
                            }

                        } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                            CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                            formViewBean.getCustomMenu().add(CustomFormMenu.valueOf(menuId));
                        } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                            CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                            customTreeViewBean.getCustomMenu().add(CustomFormMenu.valueOf(menuId));
                        } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                            customGridViewBean.getCustomMenu().add(GridMenu.valueOf(menuId));
                        } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                            CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
                            customGalleryViewBean.getCustomMenu().add(GridMenu.valueOf(menuId));
                        } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                            CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                            customGalleryViewBean.getCustomMenu().add(GridMenu.valueOf(menuId));

                        }
                    }
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delCustomMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delCustomMenu(String sourceClassName, String methodName, String type, String bar, String domainId, String viewInstId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            String[] menuIds = StringUtility.split(type, ";");
            for (String menuId : menuIds) {
                if (menuId != null && !menuIds.equals("")) {
                    if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                        NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                        NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                        CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                        if (customTreeViewBean != null) {
                            customTreeViewBean.getCustomMenu().remove(CustomFormMenu.valueOf(menuId));
                        } else {
                            viewBean.getCustomMenu().remove(CustomFormMenu.valueOf(menuId));
                        }
                    } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                        CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                        formViewBean.getCustomMenu().remove(CustomFormMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                        CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                        customTreeViewBean.getCustomMenu().remove(CustomFormMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                        CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                        customGridViewBean.getCustomMenu().remove(GridMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                        CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
                        customGalleryViewBean.getCustomMenu().add(GridMenu.valueOf(menuId));
                    } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                        CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                        customGalleryViewBean.getCustomMenu().remove(CustomFormMenu.valueOf(menuId));
                    }
                }
            }


            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
