package com.ds.dsm.view.config.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.module.ModuleView;
import com.ds.dsm.view.config.nav.tab.stacks.NavStacksTree;
import com.ds.enums.CustomBean;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.stacks.NavStacksViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.layout.CustomLayoutItemBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/stacks/")
public class ViewNavStacksConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewStackConfig")
    @ModuleAnnotation(dynLoad = true, caption = "获取字段列", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<NavStacksTree>> getViewStackConfig(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<NavStacksTree>> result = new TreeListResultModel<>();

        try {
            List<CustomBean> customBeans = new ArrayList<>();
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            NavStacksViewBean navTabsViewBean = (NavStacksViewBean) methodAPIBean.getView();
            CustomLayoutItemBean layoutItemBean = navTabsViewBean.getMethodConfig().getLayoutItem();
            customBeans.add(layoutItemBean);
            List<String> fieldNames = navTabsViewBean.getDisplayFieldNames();
            for (String fieldname : fieldNames) {
                FieldFormConfig fieldFormConfig = (FieldFormConfig) navTabsViewBean.getFieldConfigMap().get(fieldname);
                if (fieldFormConfig != null) {
                    customBeans.add(fieldFormConfig);
                }
            }
            List<String> itemNames = navTabsViewBean.getModuleFields();
            for (String fieldname : itemNames) {
                FieldModuleConfig fieldFormConfig = navTabsViewBean.getFieldModuleConfig(fieldname);
                if (fieldFormConfig != null) {
                    customBeans.add(fieldFormConfig);
                }
            }
            result = TreePageUtil.getTreeList(customBeans, NavStacksTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "WinConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ModuleView> getWinConfig(String sourceClassName, String methodName, String domainId) {
        ResultModel<ModuleView> resultModel = new ResultModel();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            if (classConfig != null) {
                MethodConfig methodAPIBean = classConfig.getMethodByName(methodName);
                resultModel.setData(new ModuleView(methodAPIBean.getModuleBean()));
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
