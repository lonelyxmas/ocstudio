package com.ds.dsm.view.config.form.field.service;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.item.FieldMenuItems;
import com.ds.dsm.view.config.form.field.item.FormFieldNavTree;

import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/")
public class FieldItemEventService {


    @RequestMapping(method = RequestMethod.POST, value = "FieldMenuItems")
    @ModuleAnnotation(dynLoad = true, caption = "单元格事件")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<FormFieldNavTree>> getFieldMenuItems(String domainId, String sourceClassName, String methodName, String fieldname, String viewInstId) {
        TreeListResultModel<List<FormFieldNavTree>> result = TreePageUtil.getTreeList(Arrays.asList(FieldMenuItems.values()), FormFieldNavTree.class);
        return result;
    }

}
