package com.ds.dsm.view.config.menu.bottommenu;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.toolbar.BottomBarMenuBean;
import com.ds.esd.tool.ui.enums.*;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@BottomBarMenu
@FormAnnotation(col = 2)
public class BottomMenuConfigView {

    @Pid
    String domainId;
    @Uid
    String id;
    @Pid
    String sourceClassName;
    @Pid
    String entityClassName;
    @Pid
    String methodName;


    @CustomAnnotation(caption = "容器位置")
    Dock barDock;
    @CustomAnnotation(caption = "容器高度")
    String height;
    @CustomAnnotation(caption = "按钮对齐方式")
    AlignType itemAlign;

    @CustomAnnotation(caption = "容器边框")
    BorderType barBorderType;


    @CustomAnnotation(caption = "按钮类型")
    StatusItemType itemType;

    @CustomAnnotation(caption = "按钮位置")
    Dock dock;

    @CustomAnnotation(caption = "按钮对齐")
    TagCmdsAlign tagCmdsAlign;

    @CustomAnnotation(caption = "按钮边框")
    BorderType borderType;

    @CustomAnnotation(caption = "自动对齐")
    Boolean connected;

    @CustomAnnotation(caption = "按钮间距")
    String itemPadding;
    @CustomAnnotation(caption = "按钮边距")
    String itemMargin;
    @CustomAnnotation(caption = "按钮宽度")
    String itemWidth;
    @CustomAnnotation(caption = "按钮高度")
    String itemHeight;


    public BottomMenuConfigView() {

    }


    public BottomMenuConfigView(BottomBarMenuBean menuBarBean, String domainId, String sourceClassName, String entityClassName, String methodName) {
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.entityClassName = entityClassName;
        this.methodName = methodName;
        this.itemPadding = menuBarBean.getItemPadding();
        this.itemMargin = menuBarBean.getItemMargin();
        this.itemWidth = menuBarBean.getItemWidth();
        this.height = menuBarBean.getHeight();
        this.dock = menuBarBean.getDock();
        this.barDock = menuBarBean.getBarDock();
        this.itemHeight = menuBarBean.getItemHeight();
        this.itemAlign = menuBarBean.getItemAlign();
        this.itemType = menuBarBean.getItemType();
        this.connected = menuBarBean.getConnected();
        this.borderType = menuBarBean.getBorderType();
        this.barBorderType = menuBarBean.getBarBorderType();
        this.tagCmdsAlign = menuBarBean.getTagCmdsAlign();

    }

    public BorderType getBarBorderType() {
        return barBorderType;
    }

    public void setBarBorderType(BorderType barBorderType) {
        this.barBorderType = barBorderType;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public StatusItemType getItemType() {
        return itemType;
    }

    public void setItemType(StatusItemType itemType) {
        this.itemType = itemType;
    }

    public String getItemPadding() {
        return itemPadding;
    }

    public void setItemPadding(String itemPadding) {
        this.itemPadding = itemPadding;
    }

    public String getItemMargin() {
        return itemMargin;
    }

    public void setItemMargin(String itemMargin) {
        this.itemMargin = itemMargin;
    }

    public String getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(String itemWidth) {
        this.itemWidth = itemWidth;
    }

    public String getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(String itemHeight) {
        this.itemHeight = itemHeight;
    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }

    public Dock getBarDock() {
        return barDock;
    }

    public void setBarDock(Dock barDock) {
        this.barDock = barDock;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public AlignType getItemAlign() {
        return itemAlign;
    }

    public void setItemAlign(AlignType itemAlign) {
        this.itemAlign = itemAlign;
    }

    public Boolean getConnected() {
        return connected;
    }

    public void setConnected(Boolean connected) {
        this.connected = connected;
    }

    public BorderType getBorderType() {
        return borderType;
    }

    public void setBorderType(BorderType borderType) {
        this.borderType = borderType;
    }

    public TagCmdsAlign getTagCmdsAlign() {
        return tagCmdsAlign;
    }

    public void setTagCmdsAlign(TagCmdsAlign tagCmdsAlign) {
        this.tagCmdsAlign = tagCmdsAlign;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
