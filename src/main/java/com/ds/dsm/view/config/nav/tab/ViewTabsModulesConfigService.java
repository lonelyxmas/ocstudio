package com.ds.dsm.view.config.nav.tab;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavTabsBaseViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.bean.nav.tab.TabItemBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tabs/module/")
public class ViewTabsModulesConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "TabModuleList")
    @GridViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "获取子项", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<NavTabItemsView>> getTabModuleList(String domainId, String sourceClassName, String methodName, String groupName, String viewInstId) {
        ListResultModel<List<NavTabItemsView>> result = new TreeListResultModel<>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            List<TabItemBean> childTabViewBeans = new ArrayList<>();
            if (methodAPIBean != null && methodAPIBean.getView() != null) {
                NavTabsViewBean navTabsViewBean = null;
                if (methodAPIBean.getView() instanceof NavTabsBaseViewBean) {
                    NavTabsBaseViewBean baseViewBean = (NavTabsBaseViewBean) methodAPIBean.getView();
                    navTabsViewBean = baseViewBean.getTabsViewBean();
                } else if (methodAPIBean.getView() instanceof NavTabsViewBean) {
                    navTabsViewBean = (NavTabsViewBean) methodAPIBean.getView();
                }
                childTabViewBeans = navTabsViewBean.getItemBeans();
            }

            result = PageUtil.getDefaultPageList(childTabViewBeans, NavTabItemsView.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "TabsChildItems")
    @ModuleAnnotation(dynLoad = true, caption = "获取子项", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<NavTabsTree>> getTabsChildItems(String domainId, String sourceClassName, String methodName, String groupName, String viewInstId) {
        TreeListResultModel<List<NavTabsTree>> result = new TreeListResultModel<>();
        try {

            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            List<TabItemBean> childTabViewBeans = new ArrayList<>();
            if (methodAPIBean != null && methodAPIBean.getView() != null) {
                NavTabsViewBean navTabsViewBean = null;
                if (methodAPIBean.getView() instanceof NavTabsBaseViewBean) {
                    NavTabsBaseViewBean baseViewBean = (NavTabsBaseViewBean) methodAPIBean.getView();
                    navTabsViewBean = baseViewBean.getTabsViewBean();
                } else if (methodAPIBean.getView() instanceof NavTabsViewBean) {
                    navTabsViewBean = (NavTabsViewBean) methodAPIBean.getView();
                }
                childTabViewBeans = navTabsViewBean.getItemBeans();
            }

            result = TreePageUtil.getTreeList(childTabViewBeans, NavTabsTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
