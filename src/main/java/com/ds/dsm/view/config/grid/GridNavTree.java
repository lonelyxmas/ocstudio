package com.ds.dsm.view.config.grid;

import com.ds.dsm.view.config.grid.cell.GridCellMenuItems;
import com.ds.dsm.view.config.grid.cell.GridCellNavItems;
import com.ds.dsm.view.config.grid.nav.GridConfigItems;
import com.ds.dsm.view.config.grid.nav.GridConfigNavItems;
import com.ds.dsm.view.config.grid.nav.GridEventConfigItems;
import com.ds.dsm.view.config.grid.row.GridRowMenuItems;
import com.ds.dsm.view.config.grid.row.GridRowNavItems;
import com.ds.dsm.view.config.nav.gallery.GalleryNavItems;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true, lazyLoad = true, dynDestory = true)
public class GridNavTree extends TreeListItem {
    @Pid
    String sourceClassName;
    @Pid
    String viewInstId;
    @Pid
    String methodName;
    @Pid
    String sourceMethodName;
    @Pid
    String domainId;
    @Pid
    String fieldname;

    @TreeItemAnnotation(customItems = GridConfigNavItems.class)
    public GridNavTree(GridConfigNavItems girdNavItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = girdNavItems.getName();
        this.bindClassName = girdNavItems.getBindClass().getName();
        this.imageClass = girdNavItems.getImageClass();
        this.id = girdNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = GalleryNavItems.class)
    public GridNavTree(GalleryNavItems girdNavItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = girdNavItems.getName();
        this.bindClassName = girdNavItems.getBindClass().getName();
        this.imageClass = girdNavItems.getImageClass();
        this.id = girdNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = GridEventConfigItems.class)
    public GridNavTree(GridEventConfigItems girdNavItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = girdNavItems.getName();
        this.bindClassName = girdNavItems.getBindClass().getName();
        this.imageClass = girdNavItems.getImageClass();
        this.id = girdNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(customItems = GridConfigItems.class)
    public GridNavTree(GridConfigItems girdNavItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = girdNavItems.getName();
        this.bindClassName = girdNavItems.getBindClass().getName();
        this.imageClass = girdNavItems.getImageClass();
        this.id = girdNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(customItems = GridRowNavItems.class)
    public GridNavTree(GridRowNavItems girdNavItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = girdNavItems.getName();
        this.bindClassName = girdNavItems.getBindClass().getName();
        this.imageClass = girdNavItems.getImageClass();
        this.id = girdNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = GridCellNavItems.class)
    public GridNavTree(GridCellNavItems girdNavItems, String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        this.caption = girdNavItems.getName();
        this.bindClassName = girdNavItems.getBindClass().getName();
        this.imageClass = girdNavItems.getImageClass();
        this.id = girdNavItems.getType() + "_" + sourceClassName + "_" + methodName + "_" + fieldname;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.fieldname = fieldname;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(customItems = GridEventMenuItems.class)
    public GridNavTree(GridEventMenuItems girdNavItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = girdNavItems.getName();
        this.bindClassName = girdNavItems.getBindClass().getName();
        this.imageClass = girdNavItems.getImageClass();
        this.id = girdNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = GridRowMenuItems.class)
    public GridNavTree(GridRowMenuItems girdNavItems, String sourceClassName, String methodName, String domainId, String viewInstId) {
        this.caption = girdNavItems.getName();
        this.bindClassName = girdNavItems.getBindClass().getName();
        this.imageClass = girdNavItems.getImageClass();
        this.id = girdNavItems.getType() + "_" + sourceClassName + "_" + methodName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(customItems = GridCellMenuItems.class)
    public GridNavTree(GridCellMenuItems girdNavItems, String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        this.caption = girdNavItems.getName();
        this.fieldname = fieldname;
        this.bindClassName = girdNavItems.getBindClass().getName();
        this.imageClass = girdNavItems.getImageClass();
        this.id = girdNavItems.getType() + "_" + sourceClassName + "_" + methodName + "_" + fieldname;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    @TreeItemAnnotation(bindService = ViewGridFieldsService.class, lazyLoad = true, dynDestory = true)
    public GridNavTree(MethodConfig customMethodAPIBean) {
        this.caption = customMethodAPIBean.getName();
        this.imageClass = customMethodAPIBean.getImageClass();
        this.id = customMethodAPIBean.getMethodName() + "_" + sourceClassName + "_" + methodName;
        this.domainId = customMethodAPIBean.getDomainId();
        this.viewInstId = customMethodAPIBean.getViewInstId();
        this.sourceClassName = customMethodAPIBean.getSourceClassName();
        this.sourceMethodName = customMethodAPIBean.getMethodName();
        this.methodName = customMethodAPIBean.getMethodName();
        ;

    }


    @TreeItemAnnotation(bindService = ViewGridFieldService.class)
    public GridNavTree(FieldGridConfig fieldGridInfo, String sourceClassName, String methodName) {
        this.caption = fieldGridInfo.getCaption();
        if (caption == null || caption.equals("")) {
            caption = fieldGridInfo.getFieldname();
        }
        if (fieldGridInfo.getGridColItemBean() != null && fieldGridInfo.getGridColItemBean().getInputType() != null) {
            this.imageClass = fieldGridInfo.getGridColItemBean().getInputType().getImageClass();
        }

        this.id = fieldGridInfo.getFieldname() + "_" + sourceClassName + "_" + methodName;
        this.domainId = fieldGridInfo.getDomainId();
        this.viewInstId = fieldGridInfo.getDomainId();
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;
        this.fieldname = fieldGridInfo.getFieldname();
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }
}
