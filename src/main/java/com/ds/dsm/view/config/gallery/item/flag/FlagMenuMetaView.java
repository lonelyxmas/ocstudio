package com.ds.dsm.view.config.gallery.item.flag;


import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.dsm.view.config.gallery.item.flag.custom.CustomFlagMenuGridView;
import com.ds.dsm.view.config.gallery.item.flag.menuclass.FlagMenuGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.gallery.GalleryFlagCmdBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/dsm/view/config/gallery/flag/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "状态按钮配置", imageClass = "spafont spa-icon-c-databinder")
public class FlagMenuMetaView {
    @CustomAnnotation(uid = true, hidden = true)
    private String sourceClassName;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;

    @RequestMapping(method = RequestMethod.POST, value = "CustomFlagMenuList")
    @ModuleAnnotation(caption = "常用按钮", imageClass = "spafont spa-icon-c-toolbar", dock = Dock.fill)
    @CustomAnnotation(index = 0)
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<CustomFlagMenuGridView>> getCustomFlagMenuList(String sourceClassName, String methodName, String domainId) {
        ListResultModel<List<CustomFlagMenuGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGalleryViewBean customGalleryViewBean = null;
            if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
            } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
            }


        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "FlagMenuClassList")
    @ModuleAnnotation(caption = "自定义状态", imageClass = "spafont spa-icon-project", dock = Dock.fill)
    @CustomAnnotation(index = 1)
    @GridViewAnnotation
    @ResponseBody
    public ListResultModel<List<FlagMenuGridView>> getFlagMenuClassList(String domainId, String methodName, String sourceClassName) {
        ListResultModel<List<FlagMenuGridView>> result = new ListResultModel();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            CustomGalleryViewBean customGalleryViewBean = null;
            if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
            } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
            }


            List<ESDClass> esdClassList = new ArrayList<>();

            result = PageUtil.getDefaultPageList(esdClassList, FlagMenuGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
