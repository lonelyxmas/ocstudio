package com.ds.dsm.view.config.form.field.combo.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.FieldDatePickerView;
import com.ds.dsm.view.config.form.field.combo.ComboFileView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.WidgetBean;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.Dock;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/combo/")
@MethodChinaName(cname = "选择文件", imageClass ="spafont spa-icon-c-fileinput")

public class ComboFileService {


    @RequestMapping(method = RequestMethod.POST, value = "ComboFile")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass ="spafont spa-icon-c-fileinput", caption = "选择文件", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ComboFileView> getComboFile(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<ComboFileView> result = new ResultModel<ComboFileView>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new ComboFileView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "updateComboFile")
    @APIEventAnnotation( bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateComboFile(@RequestBody ComboFileView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean =config.getMethodByName(comboView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboView.getFieldname());
            ComboBoxBean comboConfig = formInfo.getComboConfig();
            Map<String, Object> configMap = BeanMap.create(comboConfig);
            configMap.putAll(BeanMap.create(comboView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetComboFile")
    @APIEventAnnotation( bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetComboFile(@RequestBody ComboFileView comboView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(comboView.getSourceClassName(), comboView.getViewInstId());
            MethodConfig customMethodAPIBean = config.getMethodByName(comboView.getMethodName());
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(comboView.getFieldname());
            formInfo.setWidgetConfig(null);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
