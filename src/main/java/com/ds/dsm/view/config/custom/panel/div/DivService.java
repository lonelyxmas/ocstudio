package com.ds.dsm.view.config.custom.panel.div;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.custom.panel.PanelConfigTree;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.nav.PanelItemBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/custom/div/")
public class DivService {


    @RequestMapping(method = RequestMethod.POST, value = "DivConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<DivView> getDivConfig(String sourceClassName, String sourceMethodName, String groupId, String domainId, String viewInstId) {
        ResultModel<DivView> resultModel = new ResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(sourceMethodName);
            NavBaseViewBean baseViewBean = (NavBaseViewBean) customMethodAPIBean.getView();
            PanelItemBean panelItemBean = baseViewBean.getItemBean(groupId);
            resultModel.setData(new DivView(panelItemBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadDivItem")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<PanelConfigTree>> loadDivItem(String viewInstId, String domainId, String sourceClassName, String sourceMethodName, String groupId) {
        TreeListResultModel<List<PanelConfigTree>> resultModel = new TreeListResultModel<List<PanelConfigTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(DivConfigItems.values()), PanelConfigTree.class);
        return resultModel;
    }

}
