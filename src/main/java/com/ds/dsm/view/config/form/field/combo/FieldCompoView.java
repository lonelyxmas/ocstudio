package com.ds.dsm.view.config.form.field.combo;

import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComponentType;

@FormAnnotation(col = 2)
public class FieldCompoView {

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "显示名称", readonly = true)
    private String caption;

    @CustomAnnotation(caption = "控件类型", hidden = true)
    private ComponentType componentType;

    @CustomAnnotation(caption = "展现形式", hidden = true)
    private ComboInputType type;


    @CustomAnnotation(caption = "主键", readonly = true, hidden = true)
    Boolean uid;

    @CustomAnnotation(caption = "禁用")
    Boolean disabled;


    @CustomAnnotation(caption = "只读", hidden = true)
    Boolean readonly;

    @CustomAnnotation(caption = "动态装载")
    Boolean dynLoad;

    @CustomAnnotation(caption = "隐藏")
    Boolean hidden;


    @CustomAnnotation(caption = "默认值公式", hidden = true)
    String expression;

    @CustomAnnotation(caption = "过滤表达式", hidden = true)
    String filter;

    @CustomAnnotation(caption = "选项表达式", hidden = true)
    String itemsExpression;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    public FieldCompoView() {

    }

    public FieldCompoView(FieldFormConfig<ComboInputFieldBean,ComboBoxBean> config) {
        this.uid = config.getUid();
        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.disabled = config.getAggConfig().getDisabled();
        this.componentType = config.getComponentType();

        this.caption = config.getAggConfig().getCaption();
        this.fieldname = config.getFieldname();

        this.hidden = config.getColHidden();
        this.expression = config.getAggConfig().getExpression();
        this.uid = config.getUid();

        this.readonly = config.getAggConfig().getReadonly();

        this.domainId = config.getDomainId();


    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getItemsExpression() {
        return itemsExpression;
    }

    public void setItemsExpression(String itemsExpression) {
        this.itemsExpression = itemsExpression;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getUid() {
        return uid;
    }

    public void setUid(Boolean uid) {
        this.uid = uid;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public ComboInputType getType() {
        return type;
    }

    public void setType(ComboInputType type) {
        this.type = type;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

}
