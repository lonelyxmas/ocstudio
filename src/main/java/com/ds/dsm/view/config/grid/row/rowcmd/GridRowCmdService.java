package com.ds.dsm.view.config.grid.row.rowcmd;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.GridRowCmdBean;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.jds.core.esb.util.OgnlUtil;
import com.ds.web.util.AnnotationUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/grid/rowmenu/")
@MethodChinaName(cname = "行菜单", imageClass = "spafont spa-icon-c-gallery")

public class GridRowCmdService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "菜单配置")
    @RequestMapping(method = RequestMethod.POST, value = "RowMenuInfo")
    @NavGroupViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, caption = "菜单配置", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<GridRowMenuNav> getRomMenuInfo(String domainId, String sourceClassName, String methodName) {
        ResultModel<GridRowMenuNav> result = new ResultModel<GridRowMenuNav>();
        return result;
    }


    @MethodChinaName(cname = "保存菜单配置")
    @RequestMapping(value = {"saveRowMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveRowMenu(@RequestBody GridRowMenuConfigView menuConfigView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ApiClassConfig tableConfig = null;
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(menuConfigView.getSourceClassName(), domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(menuConfigView.getMethodName());
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            GridRowCmdBean barMenuBean = customGridViewBean.getRowCmdBean();
            if (barMenuBean == null) {
                barMenuBean = AnnotationUtil.fillDefaultValue(GridRowCmd.class, new GridRowCmdBean());
            }
            OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
            customGridViewBean.setRowCmdBean(barMenuBean);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;

    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
