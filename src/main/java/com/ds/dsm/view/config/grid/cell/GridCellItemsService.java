package com.ds.dsm.view.config.grid.cell;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.grid.GridNavTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/grid/cell/")
public class GridCellItemsService {


    @RequestMapping(method = RequestMethod.POST, value = "ChildCellConfig")
    @ModuleAnnotation(dynLoad = true, caption = "单元格行子域")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<GridNavTree>> getChildCell(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<GridNavTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(GridCellNavItems.values()), GridNavTree.class);
        return result;
    }

}
