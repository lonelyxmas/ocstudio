package com.ds.dsm.view.config.tree.child;

import com.ds.dsm.repository.db.action.ColAction;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.tool.ui.enums.CustomImageType;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@PageBar(pageCount = 100)
@RowHead(selMode = SelModeType.none, gridHandlerCaption = "排序|隐藏", rowHandlerWidth = "8em", rowNumbered = false)
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, menuClass = {ColAction.class})
@GridAnnotation(bottombarMenu = {GridMenu.Save, GridMenu.Reload}, customService = ChildTreeAction.class)
public class ChildTreeGrid {


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @Uid
    String childTreeInfoId;


    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @Pid
    String sourceClassName;
    @Pid
    String sourceMethodName;
    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    @CustomAnnotation(caption = "显示名称")
    String caption;

    @CustomAnnotation(caption = "选中方式")
    SelModeType selMode;

    @CustomAnnotation(caption = "分组显示")
    Boolean group;

    @CustomAnnotation(caption = "占位符")
    Boolean togglePlaceholder;

    @CustomAnnotation(caption = "单击展开")
    Boolean singleOpen;


    @CustomAnnotation(caption = "默认折叠")
    Boolean iniFold;


    @CustomAnnotation(caption = "装载消息")
    Boolean animCollapse;

    @CustomAnnotation(caption = "动态销毁")
    Boolean dynDestory;

    @FieldAnnotation(colSpan = 2)
    @CustomAnnotation(caption = "绑定服务")
    Class bindClass;

    @CustomAnnotation(caption = "组名称")
    String groupName;

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    String imageClass;

    @CustomAnnotation(caption = "延迟加载")
    Boolean lazyLoad = false;


    @CustomAnnotation(caption = "排序")
    Integer index = 1;


    public ChildTreeGrid() {

    }

    public ChildTreeGrid(ChildTreeViewBean treeConfig, String sourceClassName, String sourceMethodName) {
        this.childTreeInfoId = treeConfig.getId();
        this.viewInstId = treeConfig.getViewInstId();
        this.domainId = treeConfig.getDomainId();
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.methodName = treeConfig.getMethodName();
        this.groupName = treeConfig.getGroupName();
        this.imageClass = treeConfig.getImageClass();
        this.lazyLoad = treeConfig.getLazyLoad();
        this.index = treeConfig.getIndex();
        this.animCollapse = treeConfig.getAnimCollapse();
        this.iniFold = treeConfig.getIniFold();
        this.dynDestory = treeConfig.getDynDestory();
        this.togglePlaceholder = treeConfig.getTogglePlaceholder();
        this.group = treeConfig.getGroup();
        this.singleOpen = treeConfig.getSingleOpen();
        this.selMode = treeConfig.getSelMode();
        this.caption = treeConfig.getCaption();

        if (treeConfig.getBindService() != null && !treeConfig.getBindService().equals(Void.class)) {
            this.bindClass = treeConfig.getBindService();
        }

    }


    public ChildTreeGrid(CustomTreeViewBean treeConfig, String sourceClassName, String sourceMethodName) {
        this.childTreeInfoId = treeConfig.getId();
        this.viewInstId = treeConfig.getViewInstId();
        this.domainId = treeConfig.getDomainId();
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = sourceMethodName;
        this.methodName = treeConfig.getMethodName();
        this.groupName = treeConfig.getClassName();
        this.imageClass = treeConfig.getImageClass();
        this.lazyLoad = treeConfig.getLazyLoad();
        this.index = treeConfig.getIndex();
        this.animCollapse = treeConfig.getAnimCollapse();
        this.iniFold = treeConfig.getIniFold();
        this.dynDestory = treeConfig.getDynDestory();
        this.togglePlaceholder = treeConfig.getTogglePlaceholder();
        this.group = treeConfig.getGroup();
        this.singleOpen = treeConfig.getSingleOpen();
        this.selMode = treeConfig.getSelMode();
        this.caption = treeConfig.getCaption();
        this.bindClass = treeConfig.getBindService();
        if (treeConfig.getBindService() != null && !treeConfig.getBindService().equals(Void.class)) {
            this.bindClass = treeConfig.getBindService();
        }

    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public Boolean getLazyLoad() {
        return lazyLoad;
    }

    public void setLazyLoad(Boolean lazyLoad) {
        this.lazyLoad = lazyLoad;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }


    public String getChildTreeInfoId() {
        return childTreeInfoId;
    }

    public void setChildTreeInfoId(String childTreeInfoId) {
        this.childTreeInfoId = childTreeInfoId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public Boolean getIniFold() {
        return iniFold;
    }

    public void setIniFold(Boolean iniFold) {
        this.iniFold = iniFold;
    }

    public Boolean getAnimCollapse() {
        return animCollapse;
    }

    public void setAnimCollapse(Boolean animCollapse) {
        this.animCollapse = animCollapse;
    }

    public Boolean getDynDestory() {
        return dynDestory;
    }

    public void setDynDestory(Boolean dynDestory) {
        this.dynDestory = dynDestory;
    }

    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }

    public Boolean getTogglePlaceholder() {
        return togglePlaceholder;
    }

    public void setTogglePlaceholder(Boolean togglePlaceholder) {
        this.togglePlaceholder = togglePlaceholder;
    }


    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }


    public Boolean getSingleOpen() {
        return singleOpen;
    }

    public void setSingleOpen(Boolean singleOpen) {
        this.singleOpen = singleOpen;
    }

    public Class getBindClass() {
        return bindClass;
    }

    public void setBindClass(Class bindClass) {
        this.bindClass = bindClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
