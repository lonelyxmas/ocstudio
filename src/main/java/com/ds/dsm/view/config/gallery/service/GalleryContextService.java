package com.ds.dsm.view.config.gallery.service;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.gallery.GalleryConfigTree;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldGalleryConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/gallery/context/")
public class GalleryContextService {


    @RequestMapping(method = RequestMethod.POST, value = "FieldList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "列配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<FieldGalleryConfig>> getFieldList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldGalleryConfig>> cols = new ListResultModel();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig classConfig = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = classConfig.getMethodByName(methodName);
            CustomGalleryViewBean customGalleryViewBean = null;
            if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
            } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
            }
            List<String> fieldNames = customGalleryViewBean.getHiddenFieldNames();
            List<FieldGalleryConfig> fields = new ArrayList<>();
            for (String fieldName : fieldNames) {
                FieldGalleryConfig fieldGalleryConfig = (FieldGalleryConfig) customGalleryViewBean.getFieldConfigMap().get(fieldName);
                if (fieldGalleryConfig != null) {
                    fieldGalleryConfig.setSourceClassName(sourceClassName);
                    fields.add(fieldGalleryConfig);
                }
            }

            cols = PageUtil.getDefaultPageList(fields, FieldGalleryConfig.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ChildFieldConfig")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<GalleryConfigTree>> getChildFieldConfig(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<GalleryConfigTree>> result = new TreeListResultModel<>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGalleryViewBean customGalleryViewBean = null;
            if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
            } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
            }
            List<String> fieldNames = customGalleryViewBean.getHiddenFieldNames();
            List<FieldGalleryConfig> fieldFormConfigs = new ArrayList<>();
            for (String fieldName : fieldNames) {
                FieldGalleryConfig fieldFormConfig = (FieldGalleryConfig) customGalleryViewBean.getFieldConfigMap().get(fieldName);
                if (fieldFormConfig != null) {

                    fieldFormConfigs.add(fieldFormConfig);

                }
            }
            result = TreePageUtil.getTreeList(fieldFormConfigs, GalleryConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
