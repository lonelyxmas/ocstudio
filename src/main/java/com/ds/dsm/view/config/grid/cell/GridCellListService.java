package com.ds.dsm.view.config.grid.cell;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.grid.GridNavTree;
import com.ds.dsm.view.config.grid.field.FieldGridInfo;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/grid/cell/")
public class GridCellListService {


    @RequestMapping(method = RequestMethod.POST, value = "CellList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "列选项")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<FieldGridInfo>> getCellList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldGridInfo>> cols = new ListResultModel();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            List<FieldGridConfig> fieldGridConfigs = customGridViewBean.getCustomFields();
            List<FieldGridConfig> fields = new ArrayList<>();
            for (FieldGridConfig fieldGridConfig : fieldGridConfigs) {
                if (fieldGridConfig != null) {
                    fieldGridConfig.setSourceClassName(sourceClassName);
                    fieldGridConfig.setEntityClassName(customGridViewBean.getViewClassName());

                    fieldGridConfig.setDomainId(domainId);
                    fields.add(fieldGridConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }

    @RequestMapping(method = RequestMethod.POST, value = "ChildFieldConfig")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<GridNavTree>> getChildFieldConfig(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<GridNavTree>> result = new TreeListResultModel<>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridViewBean viewBean = (CustomGridViewBean) methodAPIBean.getView();
            List<FieldGridConfig> customFields = viewBean.getCustomFields();
            List<FieldGridConfig> fieldFormConfigs = new ArrayList<>();
            for (FieldGridConfig fieldGridConfig : customFields) {
                if (fieldGridConfig != null && !fieldGridConfig.getColHidden()) {
                    fieldGridConfig.setSourceClassName(sourceClassName);
                    fieldGridConfig.setEntityClassName(viewBean.getViewClassName());
                    fieldFormConfigs.add(fieldGridConfig);

                }
            }
            result = TreePageUtil.getTreeList(fieldFormConfigs, GridNavTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

}
