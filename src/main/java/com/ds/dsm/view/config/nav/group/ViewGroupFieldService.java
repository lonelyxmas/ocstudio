package com.ds.dsm.view.config.nav.group;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.grid.field.FieldGridConfigView;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/view/config/group/field/")
public class ViewGroupFieldService {


    @RequestMapping(method = RequestMethod.POST, value = "FieldNav")
    @FormViewAnnotation
    @ModuleAnnotation(caption = "字段信息")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor, autoRun = true)
    @ResponseBody
    public ResultModel<FieldGridConfigView> getFieldNav(String sourceClassName, String methodName, String domainId, String viewInstId, String fieldname) {
        ResultModel<FieldGridConfigView> result = new ResultModel<FieldGridConfigView>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            NavGroupViewBean customGridViewBean = (NavGroupViewBean) methodAPIBean.getView();
            FieldGridConfig fieldConfig = (FieldGridConfig) customGridViewBean.getFieldConfigMap().get(fieldname);
            fieldConfig.setSourceClassName(sourceClassName);
            fieldConfig.setEntityClassName(customGridViewBean.getViewClassName());
            result.setData(new FieldGridConfigView(fieldConfig));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
