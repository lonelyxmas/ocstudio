package com.ds.dsm.view.config.gallery.item.flag;

import com.ds.dsm.view.config.gallery.item.flag.menuclass.FlagMenuService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.gallery.GalleryFlagCmdBean;
import com.ds.esd.custom.grid.GridRowCmdBean;
import com.ds.esd.tool.ui.enums.CmdButtonType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@BottomBarMenu
@FormAnnotation(col = 2, customService = FlagMenuService.class)
public class FlagMenuConfigView {
    @Pid
    String domainId;

    @Pid
    String sourceClassName;


    @Pid
    String methodName;

    @Uid
    String id;


    @CustomAnnotation(caption = "按钮位置")
    TagCmdsAlign tagCmdsAlign;
    @CustomAnnotation(caption = "按钮类型")
    CmdButtonType buttonType;

    @CustomAnnotation(caption = "标题")
    String caption;

    @CustomAnnotation(caption = "提示")
    String tips;
    @CustomAnnotation(caption = "禁用")
    Boolean disabled;


    @CustomAnnotation(caption = "动态加载")
    Boolean dynLoad = false;

    @CustomAnnotation(caption = "延迟加载")
    Boolean lazy;

    @CustomAnnotation(caption = "菜单项Style")
    public String itemStyle;


    public FlagMenuConfigView() {

    }


    public FlagMenuConfigView(GalleryFlagCmdBean menuBarBean, String domainId, String sourceClassName, String methodName) {
        this.domainId = domainId;
        this.sourceClassName = sourceClassName;
        this.methodName = methodName;

        this.dynLoad = menuBarBean.getDynLoad();
        this.lazy = menuBarBean.getLazy();
        this.lazy = menuBarBean.getLazy();
        this.tagCmdsAlign = menuBarBean.getTagCmdsAlign();
        this.buttonType = menuBarBean.getButtonType();
        this.tips = menuBarBean.getTips();
        this.disabled = menuBarBean.getDisabled();

    }

    public TagCmdsAlign getTagCmdsAlign() {
        return tagCmdsAlign;
    }

    public void setTagCmdsAlign(TagCmdsAlign tagCmdsAlign) {
        this.tagCmdsAlign = tagCmdsAlign;
    }

    public CmdButtonType getButtonType() {
        return buttonType;
    }

    public void setButtonType(CmdButtonType buttonType) {
        this.buttonType = buttonType;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public Boolean getLazy() {
        return lazy;
    }

    public void setLazy(Boolean lazy) {
        this.lazy = lazy;
    }


    public String getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(String itemStyle) {
        this.itemStyle = itemStyle;
    }

}
