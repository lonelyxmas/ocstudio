package com.ds.dsm.view.config.grid.row;

import com.ds.dsm.view.config.grid.event.GridEventNavService;
import com.ds.dsm.view.config.grid.row.rowcmd.GridRowCmdService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum GridRowMenuItems implements TreeItem {
    GridRowButtonList("行操作按鈕", "spafont spa-icon-c-toolbar", GridRowCmdService.class, false, false, false),
    //  GridRowRithtButtonList("右键菜单", "spafont spa-icon-c-menu", ViewGridRowButtonService.class, false, false, false),
    GridRowEventList("常用事件", "spafont spa-icon-event", GridEventNavService.class, false, false, false);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    GridRowMenuItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
