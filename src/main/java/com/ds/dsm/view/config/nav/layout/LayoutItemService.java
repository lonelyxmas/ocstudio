package com.ds.dsm.view.config.nav.layout;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.foldingtree.NavFoldingTreeViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.layout.CustomLayoutItemBean;
import com.ds.esd.custom.layout.CustomLayoutViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/layout/item/")
public class LayoutItemService {


    @MethodChinaName(cname = "布局配置")
    @RequestMapping("ItemConfigView")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass =  "spafont spa-icon-c-layout", caption = "布局配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<NavItemConfigView> getItemConfigViews(String sourceClassName, String methodName, String domainId, String viewInstId, String id) {
        ResultModel<NavItemConfigView> resultModel = new ResultModel<NavItemConfigView>();
        try {
            ViewEntityConfig config = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName,  viewInstId);
            MethodConfig customMethodAPIBean = config.getSourceConfig().getMethodByName(methodName);
            CustomLayoutViewBean layoutConfigView = null;

            if (customMethodAPIBean.getView() instanceof NavTreeViewBean) {
                NavTreeViewBean navTreeViewBean = (NavTreeViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTreeViewBean.getLayoutViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavFoldingTreeViewBean) {
                NavFoldingTreeViewBean navTreeViewBean = (NavFoldingTreeViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTreeViewBean.getLayoutViewBean();
            } else if (customMethodAPIBean.getView() instanceof NavGalleryViewBean) {
                NavGalleryViewBean navTabViewBean = (NavGalleryViewBean) customMethodAPIBean.getView();
                layoutConfigView = navTabViewBean.getLayoutViewBean();
            }

            List<CustomLayoutItemBean> layoutItemBeanList = layoutConfigView.getItems();
            if (id != null) {
                for (CustomLayoutItemBean itemBean : layoutItemBeanList) {
                    if (itemBean.getId().equals(id)) {
                        resultModel.setData(new NavItemConfigView(itemBean));
                    }
                }
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }

}
