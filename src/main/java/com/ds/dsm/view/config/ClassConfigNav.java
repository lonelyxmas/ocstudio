package com.ds.dsm.view.config;

import com.ds.enums.db.MethodChinaName;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = {"/dsm/view/config/"})
@MethodChinaName(cname = "视图配置", imageClass = "spafont spa-icon-c-gallery")
public class ClassConfigNav {
}
