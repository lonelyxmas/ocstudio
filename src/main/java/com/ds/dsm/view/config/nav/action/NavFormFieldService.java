package com.ds.dsm.view.config.nav.action;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.FieldFormBean;
import com.ds.dsm.view.config.nav.items.ModuleItemGroup;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/action/")
@MethodChinaName(cname = "配置管理")

public class NavFormFieldService {


    @MethodChinaName(cname = "清空配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "clearFieldTabs")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formReSet)
    public @ResponseBody
    ResultModel<Boolean> clearFieldTabs(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (sourceClassName != null && !sourceClassName.equals("")) {
                ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
                MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
                NavBaseViewBean formViewBean = (NavBaseViewBean) methodAPIBean.getView();
                formViewBean.getFieldConfigMap().remove(fieldname);
                DSMFactory.getInstance().getViewManager().reSetViewEntityConfig(sourceClassName, domainId, viewInstId);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }


    @MethodChinaName(cname = "编辑字段配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFieldTabs")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateFieldTabs(@RequestBody FieldFormBean config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getSourceClassName();
            if (className != null && !className.equals("")) {
                ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(className, config.getDomainId());
                MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(config.getMethodName());
                NavBaseViewBean formViewBean = (NavBaseViewBean) methodAPIBean.getView();
                FieldFormConfig fieldConfig = (FieldFormConfig) formViewBean.getFieldConfigMap().get(config.getFieldname());
                Map<String, Object> configMap = BeanMap.create(fieldConfig);
                configMap.putAll(BeanMap.create(config));
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig.getSourceConfig());
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "TabsNav")
    @NavGroupViewAnnotation
    @ModuleAnnotation(caption = "字段信息")
    @DialogAnnotation
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor, CustomMenuItem.add, CustomMenuItem.treeNodeEditor})
    @ResponseBody
    public ResultModel<ModuleItemGroup> getFieldNav(String sourceClassName, String projectId, String methodName, String domainId, String fieldname) {
        return new ResultModel<>();
    }
}
