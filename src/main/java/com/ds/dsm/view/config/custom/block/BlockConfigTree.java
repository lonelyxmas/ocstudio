package com.ds.dsm.view.config.custom.block;


import com.ds.dsm.view.config.custom.block.container.ContainerConfigItems;
import com.ds.dsm.view.config.custom.panel.div.DivConfigItems;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(heplBar = true)
public class BlockConfigTree extends TreeListItem {

    @Pid
    String sourceClassName;
    @Pid
    String sourceMethodName;
    @Pid
    String viewInstId;
    @Pid
    String rootClassName;
    @Pid
    String methodName;
    @Pid
    String groupId;
    @Pid
    String domainId;


    @TreeItemAnnotation(customItems = BlockConfigItems.class)
    public BlockConfigTree(BlockConfigItems blockNavItems, String sourceClassName, String groupId, String methodName, String domainId, String viewInstId) {
        this.caption = blockNavItems.getName();
        this.bindClassName = blockNavItems.getBindClass().getName();
        this.dynLoad = blockNavItems.isDynLoad();
        this.dynDestory = blockNavItems.isDynDestory();
        this.iniFold = blockNavItems.isIniFold();
        this.imageClass = blockNavItems.getImageClass();
        this.id = blockNavItems.getType() + "_" + groupId;
        this.domainId = domainId;
        this.groupId = groupId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }

    @TreeItemAnnotation(customItems = ContainerConfigItems.class)
    public BlockConfigTree(ContainerConfigItems containerConfig, String sourceClassName, String groupId, String methodName, String domainId, String viewInstId) {
            this.caption = containerConfig.getName();
            this.bindClassName = containerConfig.getBindClass().getName();
            this.dynLoad = containerConfig.isDynLoad();
        this.dynDestory = containerConfig.isDynDestory();
        this.iniFold = containerConfig.isIniFold();
        this.imageClass = containerConfig.getImageClass();
        this.id = containerConfig.getType() + "_" + groupId;
        this.domainId = domainId;
        this.groupId = groupId;
        this.viewInstId = viewInstId;
        this.sourceClassName = sourceClassName;
        this.sourceMethodName = methodName;
        this.methodName = methodName;

    }


    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getRootClassName() {
        return rootClassName;
    }

    public void setRootClassName(String rootClassName) {
        this.rootClassName = rootClassName;
    }
}
