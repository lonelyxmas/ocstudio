package com.ds.dsm.view.config.grid.cell;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.grid.field.FieldGridConfigView;
import com.ds.dsm.view.config.grid.field.FieldGridInfo;
import com.ds.dsm.view.config.grid.field.HiddenFieldGridInfo;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.GridColItemBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.jds.core.esb.util.OgnlUtil;
import com.ds.web.json.JSONData;
import com.ds.web.util.PageUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/view/config/grid/cell/")
@MethodChinaName(cname = "列表字段", imageClass = "spafont spa-icon-c-comboinput")

public class GridCellService {


    @RequestMapping(method = RequestMethod.POST, value = "FieldGridList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "可见列")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<FieldGridInfo>> getFieldGridList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldGridInfo>> cols = new ListResultModel();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            List<FieldGridConfig> fieldGridConfigs = customGridViewBean.getCustomFields();
            List<FieldGridConfig> fields = new ArrayList<>();
            for (FieldGridConfig fieldGridConfig : fieldGridConfigs) {
                if (fieldGridConfig != null && !fieldGridConfig.getColHidden()) {
                    fieldGridConfig.setSourceClassName(sourceClassName);
                    fieldGridConfig.setEntityClassName(customGridViewBean.getViewClassName());

                    fieldGridConfig.setDomainId(domainId);

                    fields.add(fieldGridConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }

    @RequestMapping(method = RequestMethod.POST, value = "FieldHiddenGridList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "隐藏列")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<HiddenFieldGridInfo>> getFieldHiddenGrids(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<HiddenFieldGridInfo>> cols = new ListResultModel();
        try {

            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            Set<String> fieldNames = customGridViewBean.getFieldNames();
            List<FieldGridConfig> fields = new ArrayList<>();
            for (String fieldName : fieldNames) {
                FieldGridConfig fieldGridConfig = (FieldGridConfig) customGridViewBean.getFieldConfigMap().get(fieldName);
                if (fieldGridConfig != null && fieldGridConfig.getColHidden()) {
                    fieldGridConfig.setSourceClassName(sourceClassName);
                    fieldGridConfig.setEntityClassName(customGridViewBean.getViewClassName());
                    fields.add(fieldGridConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, HiddenFieldGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @MethodChinaName(cname = "编辑字段配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFieldGrid")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateFieldForm(@RequestBody FieldGridConfigView config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getSourceClassName();
            if (className != null && !className.equals("")) {
                ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(className, config.getDomainId());
                MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(config.getMethodName());
                CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                FieldGridConfig fieldConfig = (FieldGridConfig) customGridViewBean.getFieldConfigMap().get(config.getFieldname());
                Map<String, Object> configMap = BeanMap.create(config);
                OgnlUtil.setProperties(configMap, fieldConfig,false,false);
                OgnlUtil.setProperties(configMap, fieldConfig.getGridColItemBean(),false,false);
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(customESDClassAPIBean);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldNav")
    @FormViewAnnotation
    @ModuleAnnotation(caption = "字段信息")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor, CustomMenuItem.add}, autoRun = true)
    @ResponseBody
    public ResultModel<FieldGridConfigView> getFieldNav(String sourceClassName, String methodName, String domainId, String viewInstId, String fieldname) {
        ResultModel<FieldGridConfigView> result = new ResultModel<FieldGridConfigView>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            FieldGridConfig fieldConfig = (FieldGridConfig) customGridViewBean.getFieldConfigMap().get(fieldname);
            fieldConfig.setSourceClassName(sourceClassName);
            fieldConfig.setEntityClassName(customGridViewBean.getViewClassName());
            result.setData(new FieldGridConfigView(fieldConfig));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "批量保存")
    @RequestMapping(method = RequestMethod.POST, value = "updateAllFieldGrid")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.saveAllRow)
    public @ResponseBody
    ResultModel<Boolean> updateAllFieldGrid(@JSONData List<FieldGridInfo> rows, String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridViewBean gridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            for (FieldGridInfo config : rows) {
                FieldGridConfig fieldConfig = (FieldGridConfig) gridViewBean.getFieldConfigMap().get(config.getFieldname());
                Map<String, Object> configMap = BeanMap.create(config);

                OgnlUtil.setProperties(configMap, fieldConfig,false,false);
                OgnlUtil.setProperties(configMap, fieldConfig.getGridColItemBean(),false,false);
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(customESDClassAPIBean);

        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
