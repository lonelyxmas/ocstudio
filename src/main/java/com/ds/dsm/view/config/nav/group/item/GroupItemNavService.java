package com.ds.dsm.view.config.nav.group.item;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.nav.group.NavGroupTree;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.group.GroupItemBean;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/dsm/view/config/group/item/")
public class GroupItemNavService {


    @RequestMapping(method = RequestMethod.POST, value = "GroupItemList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "子模块")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<GroupItemGrid>> getGroupItemList(String sourceClassName, String sourceMethodName, String domainId, String viewInstId) {
        ListResultModel<List<GroupItemGrid>> cols = new ListResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(sourceMethodName);
            NavGroupViewBean groupViewBean = (NavGroupViewBean) customMethodAPIBean.getView();
            List<GroupItemBean> groupItemBeans = groupViewBean.getItemBeans();
            cols = PageUtil.getDefaultPageList(groupItemBeans, GroupItemGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadGroupItem")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<NavGroupTree>> loadGroupItem(String viewInstId, String domainId, String sourceClassName, String sourceMethodName, String groupName) {
        TreeListResultModel<List<NavGroupTree>> resultModel = new TreeListResultModel<List<NavGroupTree>>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(sourceMethodName);
            NavGroupViewBean groupViewBean = (NavGroupViewBean) customMethodAPIBean.getView();
            List<GroupItemBean> groupItemBeans = groupViewBean.getItemBeans();
            resultModel = TreePageUtil.getTreeList(groupItemBeans, NavGroupTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return resultModel;
    }

}
