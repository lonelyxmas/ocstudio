package com.ds.dsm.view.config.nav.tab;

import com.ds.config.TreeListResultModel;

import com.ds.dsm.view.config.nav.tree.NavTreeButtonItems;
import com.ds.dsm.view.config.tree.CustomTreeTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tabs/")
public class ViewTabsButtonService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewTabsButtons")
    @ModuleAnnotation(dynLoad = true, caption = "工具栏", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<NavTabsTree>> getViewTreeButtons(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<NavTabsTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(NavTabsButtonItems.values()), NavTabsTree.class);
        return result;
    }


}
