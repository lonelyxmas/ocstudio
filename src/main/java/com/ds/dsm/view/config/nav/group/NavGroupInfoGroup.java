package com.ds.dsm.view.config.nav.group;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.action.CustomBuildAction;
import com.ds.esd.custom.annotation.BlockAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/group/")
@BottomBarMenu(menuClass = CustomBuildAction.class)
@NavGroupAnnotation(
        bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet}, customService = NavGroupConfigService.class)
public class NavGroupInfoGroup {


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;


    @CustomAnnotation(hidden = true, uid = true)
    String sourceMethodName;

    public NavGroupInfoGroup() {

    }

    @RequestMapping(method = RequestMethod.POST, value = "NavGroupModuleView")
    @FormViewAnnotation(reSetUrl = "clear", saveUrl = "updateGroupBase")
    @ModuleAnnotation(imageClass = "spafont spa-icon-values", caption = "模块信息", dock = Dock.top)
    @UIAnnotation( height = "300")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<NavGroupModuleView> getNavGroupModuleView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<NavGroupModuleView> result = new ResultModel<NavGroupModuleView>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            NavGroupViewBean tabsViewBean = (NavGroupViewBean) customMethodAPIBean.getView();
            result.setData(new NavGroupModuleView(tabsViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "NavGroupBarView")
    @FormViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ModuleAnnotation(dock = Dock.fill, imageClass = "spafont spa-icon-rendermode", caption = "导航栏配置")
    @ResponseBody
    public ResultModel<NavGroupBarView> getNavFoldingBarView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<NavGroupBarView> result = new ResultModel<NavGroupBarView>();

        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            NavGroupViewBean tabsViewBean = (NavGroupViewBean) customMethodAPIBean.getView();
            result.setData(new NavGroupBarView(tabsViewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String className) {
        this.sourceClassName = className;
    }
}
