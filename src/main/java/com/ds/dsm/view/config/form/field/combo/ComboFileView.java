package com.ds.dsm.view.config.form.field.combo;

import com.ds.dsm.view.config.form.field.combo.service.ComboFileService;
import com.ds.esd.custom.ComboBoxBean;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.combo.ComboFileFieldBean;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 2, customService = ComboFileService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class ComboFileView {
    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String id;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;
    @CustomAnnotation(caption = "后缀过滤")
    String fileAccept;
    @CustomAnnotation(caption = "是否多选")
    Boolean fileMultiple;



    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;
    public ComboFileView() {

    }

    public ComboFileView(FieldFormConfig<ComboInputFieldBean,ComboFileFieldBean> config) {
        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId=config.getDomainId();
        this.fieldname = config.getFieldname();

        ComboFileFieldBean comboPopFieldBean = config.getComboConfig();
        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(comboPopFieldBean));
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileAccept() {
        return fileAccept;
    }

    public void setFileAccept(String fileAccept) {
        this.fileAccept = fileAccept;
    }

    public Boolean getFileMultiple() {
        return fileMultiple;
    }

    public void setFileMultiple(Boolean fileMultiple) {
        this.fileMultiple = fileMultiple;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
