package com.ds.dsm.view.config.nav.tree;


import com.ds.dsm.view.config.tree.ViewChildTreeConfigService;
import com.ds.dsm.view.config.tree.ViewChildTreesConfigService;
import com.ds.dsm.view.config.tree.ViewTreeHiddensService;
import com.ds.dsm.view.config.tree.field.TreeFiledService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum NavTreeConfigItems implements TreeItem {
    TreeInfoGroup("树形视图配置", "spafont spa-icon-c-treebar", ViewChildTreeConfigService.class, true, true, true),
    TreeButtons("工具栏", "spafont spa-icon-c-toolbar", ViewTreeButtonService.class, true, false, false),
    FieldHiddenGridList("环境变量", "spafont spa-icon-c-hiddeninput", ViewTreeHiddensService.class, true, true, true),
    FieldFormList("字段列表", "spafont spa-icon-c-hiddeninput", TreeFiledService.class, true, true, true),
    TreeItemList("子项列表", "spafont spa-icon-conf", ViewChildTreesConfigService.class, true, true, true);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    NavTreeConfigItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
