package com.ds.dsm.view.config.nav;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.FieldFormGridInfo;
import com.ds.dsm.view.config.nav.tab.NavTabsBarView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavButtonViewsViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.foldingtree.NavFoldingTreeViewBean;
import com.ds.esd.custom.bean.nav.tree.NavTreeViewBean;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.form.pop.PopTreeViewBean;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/")
@MethodChinaName(cname = "树形配置")
@ButtonViewsAnnotation(barLocation = BarLocationType.top, sideBarStatus = SideBarStatusType.expand, barSize = "3em")
public class NavConfigView {


    @CustomAnnotation(hidden = true, pid = true)
    public String entityClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;
    @CustomAnnotation(uid = true, hidden = true)
    private String methodName;

    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    private String domainId;


    public NavConfigView() {

    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "表单字段")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<FieldFormGridInfo>> getFieldList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldFormGridInfo>> cols = new ListResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName,  domainId);
            MethodConfig methodAPIBean =config.getMethodByName(methodName);

            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }
            Set<String> fieldNames = customTreeViewBean.getFieldNames();
            List<FieldFormConfig> fields = new ArrayList<>();
            for (String fieldname : fieldNames) {
                FieldFormConfig fieldFormConfig = (FieldFormConfig) customTreeViewBean.getFieldConfigMap().get(fieldname);
                if (fieldFormConfig != null) {

                    fields.add(fieldFormConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldFormGridInfo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cols;
    }



    @RequestMapping(method = RequestMethod.POST, value = "NavTabsBarView")
    @FormViewAnnotation
    @APIEventAnnotation(autoRun = true)
    @ModuleAnnotation(dock = Dock.fill, imageClass = "spafont spa-icon-rendermode", caption = "导航栏配置")
    @ResponseBody
    public ResultModel<NavTabsBarView> getNavTabsBarView(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<NavTabsBarView> result = new ResultModel<NavTabsBarView>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName,  viewInstId);
            MethodConfig methodAPIBean = config.getMethodByName(methodName);
            NavTreeViewBean treeViewBean = (NavTreeViewBean) methodAPIBean.getView();
            result.setData(new NavTabsBarView(treeViewBean.getTabsViewBean()));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

}
