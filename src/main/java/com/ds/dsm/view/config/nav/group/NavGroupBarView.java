package com.ds.dsm.view.config.nav.group;

import com.alibaba.fastjson.JSONObject;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboColorAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.bean.CustomBlockBean;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BorderType;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

@FormAnnotation(col = 2)
public class NavGroupBarView {


    @CustomAnnotation(caption = "是否可改变大小")
    Boolean resizer;

    @CustomAnnotation(caption = "工具栏大小")
    String barSize;

    @CustomAnnotation(caption = "边框类型")
    BorderType borderType;

    Map<String, Object> resizerProp;
    @CustomAnnotation(caption = "侧栏标题")
    String sideBarCaption;

    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = SideBarStatusType.class)
    @CustomAnnotation(caption = "侧栏类型")
    String sideBarType;

    @CustomAnnotation(caption = "侧栏大小")
    String sideBarSize;


    @CustomAnnotation(caption = "是否展开")
    SideBarStatusType sideBarStatus;

    @ComboColorAnnotation()
    @CustomAnnotation(caption = "背景颜色")
    String background;


    Class bindService;

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    public NavGroupBarView() {

    }

    public NavGroupBarView(NavGroupViewBean config) {
        CustomBlockBean blockBean = config.getBlockBean();
        if (blockBean==null){
            blockBean=new CustomBlockBean();
        }

        NavGroupBarView view = JSONObject.parseObject(JSONObject.toJSONString(blockBean), this.getClass());
        BeanMap.create(this).putAll(BeanMap.create(view));


        this.viewInstId = config.getViewInstId();
        this.domainId = config.getDomainId();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getMethodName();
        this.bindService = config.getBindService();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public Boolean getResizer() {
        return resizer;
    }

    public void setResizer(Boolean resizer) {
        this.resizer = resizer;
    }

    public BorderType getBorderType() {
        return borderType;
    }

    public void setBorderType(BorderType borderType) {
        this.borderType = borderType;
    }

    public Map<String, Object> getResizerProp() {
        return resizerProp;
    }

    public void setResizerProp(Map<String, Object> resizerProp) {
        this.resizerProp = resizerProp;
    }

    public String getSideBarCaption() {
        return sideBarCaption;
    }

    public void setSideBarCaption(String sideBarCaption) {
        this.sideBarCaption = sideBarCaption;
    }

    public String getSideBarType() {
        return sideBarType;
    }

    public void setSideBarType(String sideBarType) {
        this.sideBarType = sideBarType;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getBarSize() {
        return barSize;
    }

    public void setBarSize(String barSize) {
        this.barSize = barSize;
    }

    public String getSideBarSize() {
        return sideBarSize;
    }

    public void setSideBarSize(String sideBarSize) {
        this.sideBarSize = sideBarSize;
    }

    public SideBarStatusType getSideBarStatus() {
        return sideBarStatus;
    }

    public void setSideBarStatus(SideBarStatusType sideBarStatus) {
        this.sideBarStatus = sideBarStatus;
    }

    public Class getBindService() {
        return bindService;
    }

    public void setBindService(Class bindService) {
        this.bindService = bindService;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }


}
