package com.ds.dsm.view.config.nav.items;

import com.ds.esd.custom.enums.AppendType;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.field.combo.ComboModuleFieldBean;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.Dock;

public class ModuleItemsBean {

    String viewInstId;

    String domainId;

    String fieldname;

    String methodName;

    String caption;

    ComponentType componentType;

    Boolean hidden;

    AppendType append;

    String src;

    String expression;

    String viewClassName;

    String sourceClassName;


    Dock dock;

    String imageClass;

    String height;

    String width;

    Integer colSpan;

    Integer rowSpan;

    public ModuleItemsBean() {

    }

    public ModuleItemsBean(FieldFormConfig<ComboInputFieldBean,ComboModuleFieldBean> config) {
        ComboModuleFieldBean fieldBean = config.getComboConfig();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.append = fieldBean.getAppend();
        this.src = fieldBean.getSrc();
        this.componentType = config.getComponentType();
        this.caption = config.getAggConfig().getCaption();
        this.fieldname = config.getFieldname();
        this.hidden = config.getColHidden();
        this.expression = config.getAggConfig().getExpression();
        this.methodName = config.getSourceMethodName();
        this.colSpan = config.getFieldBean().getColSpan();

        this.height = config.getFieldBean().getRowHeight();
        this.width = config.getFieldBean().getColWidth();
        this.imageClass = config.getAggConfig().getImageClass();
        if (imageClass == null || imageClass.equals("")) {
            if (config.getComponentType() != null) {
                imageClass = config.getComponentType().getImageClass();
            }
        }

        this.rowSpan = config.getFieldBean().getRowSpan();
        this.fieldname = config.getFieldname();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public AppendType getAppend() {
        return append;
    }

    public void setAppend(AppendType append) {
        this.append = append;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public Integer getColSpan() {
        return colSpan;
    }

    public void setColSpan(Integer colSpan) {
        this.colSpan = colSpan;
    }

    public Integer getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(Integer rowSpan) {
        this.rowSpan = rowSpan;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }


    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
