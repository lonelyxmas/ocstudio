package com.ds.dsm.view.config.nav.tab;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.module.ModuleView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/tabs/")
public class ViewNavTabsConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewTabsItemsConfig")
    @ModuleAnnotation(dynLoad = true, caption = "获取目录", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<NavTabsTree>> getViewTabsItemsConfig(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<NavTabsTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(NavTabsItems.values()), NavTabsTree.class);
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "WinConfig")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "面板配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<ModuleView> getWinConfig(String sourceClassName, String methodName, String domainId) {
        ResultModel<ModuleView> resultModel = new ResultModel();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            if (classConfig != null) {
                MethodConfig methodAPIBean = classConfig.getMethodByName(methodName);
                resultModel.setData(new ModuleView(methodAPIBean.getModuleBean()));
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

}
