package com.ds.dsm.view.config.tree.contextmenu.custom;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.custom.tree.enums.TreeMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping(path = "/dsm/view/config/tree/contextmenu/")
@MethodChinaName(cname = "右键菜单常用按钮", imageClass = "spafont spa-icon-c-databinder")
public class CustomTreeContextMenuService {

    @RequestMapping(method = RequestMethod.POST, value = "TreeRightMenu")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "右键菜单常用按钮",
            imageClass = "spafont spa-icon-c-toolbar")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<CustomTreeContextMenuGridView>> getTreeRightMenu(String sourceClassName, String sourceMethodName, String domainId, String childViewId) {
        ListResultModel<List<CustomTreeContextMenuGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(sourceMethodName);
            CustomTreeViewBean customTreeViewBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                customTreeViewBean = baseViewBean.getTreeView();
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
            }
            if (childViewId != null) {
                ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                resultModel = PageUtil.getDefaultPageList(Arrays.asList(childTreeViewBean.getContextMenu()), CustomTreeContextMenuGridView.class);
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "右键菜单常用按钮")
    @RequestMapping("CustomContextMenuTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<CustomTreeContextMenuPopTree>> getCustomContextMenuTree(String sourceClassName, String methodName, String domainId, String viewInstId, String esdsearchpattern) {
        TreeListResultModel<List<CustomTreeContextMenuPopTree>> model = new TreeListResultModel<>();
        List<CustomTreeContextMenuPopTree> popTrees = new ArrayList<>();
        CustomTreeContextMenuPopTree menuPopTree = new CustomTreeContextMenuPopTree("contextMenu", "常用按钮", "contextMenu");
        List<CustomTreeContextMenuPopTree> menuTrees = TreePageUtil.fillObjs(Arrays.asList(TreeMenu.values()), CustomTreeContextMenuPopTree.class);
        menuPopTree.setSub(menuTrees);
        popTrees.add(menuPopTree);
        model.setData(popTrees);

        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addCustomContextMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addCustomContextMenu(String sourceClassName, String methodName, String CustomContextMenuTree, String domainId, String childViewId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            try {
                if (childViewId != null) {
                    if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                        NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                        CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                        ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                        String[] menuIds = StringUtility.split(CustomContextMenuTree, ";");
                        for (String menuId : menuIds) {
                            if (menuId != null && !menuIds.equals("")) {
                                childTreeViewBean.getContextMenu().add(TreeMenu.valueOf(menuId));
                            }
                        }
                    } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {

                        CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                        ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                        if (CustomContextMenuTree != null && !CustomContextMenuTree.equals("")) {
                            String[] menuIds = StringUtility.split(CustomContextMenuTree, ";");
                            for (String menuId : menuIds) {
                                if (menuId != null && !menuIds.equals("")) {
                                    childTreeViewBean.getContextMenu().add(TreeMenu.valueOf(menuId));
                                }
                            }
                        }
                    }
                }
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(customESDClassAPIBean);
            } catch (JDSException e) {
                model = new ErrorResultModel();
                ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
            }
            return model;
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delCustomContextMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delCustomContextMenu(String sourceClassName, String methodName, String type, String bar, String domainId, String childViewId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                String[] menuIds = StringUtility.split(type, ";");
                for (String menuId : menuIds) {
                    childTreeViewBean.getContextMenu().remove(TreeMenu.valueOf(menuId));
                }
            } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                ChildTreeViewBean childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                String[] menuIds = StringUtility.split(type, ";");
                for (String menuId : menuIds) {
                    childTreeViewBean.getContextMenu().remove(TreeMenu.valueOf(menuId));
                }
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
