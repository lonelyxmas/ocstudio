package com.ds.dsm.view.config.nav.items;

import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.enums.AppendType;
import com.ds.esd.custom.field.combo.ComboInputFieldBean;
import com.ds.esd.custom.field.combo.ComboModuleFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComponentType;

@FormAnnotation(col = 2)
public class ModuleBaseItemsView {

    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "显示名称", readonly = true)
    private String caption;

    @FieldAnnotation(required = true)
    @CustomListAnnotation(filter = "source.isAbsValue()")
    @CustomAnnotation(caption = "控件类型")
    private ComponentType componentType;


    @CustomAnnotation(caption = "禁用")
    Boolean disabled;

    @CustomAnnotation(caption = "输入校验")
    Boolean dynCheck;


    @CustomAnnotation(caption = "隐藏")
    Boolean hidden;

    @CustomAnnotation(caption = "引用方式")
    AppendType append;


    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "链接地址")
    String src;

    @FieldAnnotation(colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "默认值公式")
    String expression;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    public ModuleBaseItemsView() {

    }

    public ModuleBaseItemsView(FieldFormConfig<ComboInputFieldBean,ComboModuleFieldBean> config) {
        ComboModuleFieldBean fieldBean = config.getComboConfig();

        this.viewClassName = config.getViewClassName();
        this.methodName = config.getSourceMethodName();;
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.append = fieldBean.getAppend();
        this.src = fieldBean.getSrc();
        this.disabled = config.getAggConfig().getDisabled();
        this.dynCheck = config.getFieldBean().getDynCheck();
        this.componentType = config.getComponentType();

        this.caption = config.getAggConfig().getCaption();
        this.fieldname = config.getFieldname();

        this.hidden = config.getColHidden();
        this.expression = config.getAggConfig().getExpression();

        this.domainId = config.getDomainId();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getDynCheck() {
        return dynCheck;
    }

    public void setDynCheck(Boolean dynCheck) {
        this.dynCheck = dynCheck;
    }


    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }


    public AppendType getAppend() {
        return append;
    }

    public void setAppend(AppendType append) {
        this.append = append;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }


    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

}
