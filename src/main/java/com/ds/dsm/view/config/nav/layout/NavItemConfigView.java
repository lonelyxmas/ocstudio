package com.ds.dsm.view.config.nav.layout;

import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.ComboColorAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.layout.CustomLayoutItemBean;
import com.ds.esd.tool.ui.enums.AttachmentType;
import com.ds.esd.tool.ui.enums.OverflowType;
import com.ds.esd.tool.ui.enums.PosType;
import org.springframework.stereotype.Controller;

@Controller
@FormAnnotation(col = 2, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@MethodChinaName(cname = "布局", imageClass = "spafont spa-icon-c-gallery")
public class NavItemConfigView {


    @CustomAnnotation(pid = true, hidden = true)
    private String sourceClassName;

    @CustomAnnotation(uid = true, hidden = true)
    private String methodName;

    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @CustomAnnotation(pid = true, hidden = true)
    private String viewInstId;

    @CustomAnnotation(uid = true, hidden = true)
    public String projectVersionName;


    @CustomAnnotation(caption = "表达式")
    public String expression;
    @CustomAnnotation(caption = "大小")
    public Integer size;
    @CustomAnnotation(caption = "最小值")
    public Integer min;
    @CustomAnnotation(caption = "最大值")
    public Integer max;
    @CustomAnnotation(caption = "默认折叠")
    public Boolean folded;
    @CustomAnnotation(caption = "命令按钮")
    public Boolean cmd;
    @CustomAnnotation(caption = "位置")
    public PosType pos;
    @CustomAnnotation(caption = "锁定")
    Boolean locked;
    @CustomAnnotation(caption = "隐藏")
    Boolean hidden;
    @CustomAnnotation(caption = "显示关闭按钮")
    Boolean cmdDisplay;
    @CustomAnnotation(caption = "显示拖动条")
    Boolean moveDisplay;
    @ComboColorAnnotation
    @CustomAnnotation(caption = "背景颜色")
    public String panelBgClr;
    @CustomAnnotation(caption = "背景图片")
    public String panelBgImg;
    @CustomAnnotation(caption = "背景")
    public String panelBgImgPos;
    @CustomAnnotation(caption = "背景平铺")
    public AttachmentType panelBgImgAttachment;
    @CustomAnnotation(caption = "样式Class")
    String itemClass;
    @CustomAnnotation(caption = "Url")
    String url;
    @CustomAnnotation(caption = "自动平铺")
    Boolean flexSize;
    @CustomAnnotation(caption = "背景透明")
    Boolean transparent;
    @CustomAnnotation(caption = "标题")
    String title;
    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "HTML")
    public String html;
    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "滚动条")
    public OverflowType overflow;
    @CustomAnnotation(uid = true, hidden = true)
    public String id;

    public NavItemConfigView(CustomLayoutItemBean itemBean) {
        this.expression = itemBean.getExpression();
        this.size = itemBean.getSize();
        this.max = itemBean.getMax();
        this.min = itemBean.getMin();
        this.folded = itemBean.getFolded();
        this.cmd = itemBean.getCmd();
        this.pos = itemBean.getPos();
        this.overflow = itemBean.getOverflow();
        this.id = itemBean.getId();
        this.locked = itemBean.getLocked();
        this.flexSize = itemBean.getFlexSize();
        this.panelBgClr = itemBean.getPanelBgClr();
        this.panelBgImgAttachment = itemBean.getPanelBgImgAttachment();
        this.panelBgImg = itemBean.getPanelBgImg();
        this.panelBgImgPos = itemBean.getPanelBgImgPos();
        this.transparent = itemBean.getTransparent();
        this.title = itemBean.getTitle();
        this.cmdDisplay = itemBean.getCmdDisplay();
        this.moveDisplay = itemBean.getMoveDisplay();
        this.itemClass = itemBean.getItemClass();
        this.url = itemBean.getUrl();
        this.hidden = itemBean.getHidden();


    }


    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Boolean getFolded() {
        return folded;
    }

    public void setFolded(Boolean folded) {
        this.folded = folded;
    }

    public Boolean getCmd() {
        return cmd;
    }

    public void setCmd(Boolean cmd) {
        this.cmd = cmd;
    }

    public PosType getPos() {
        return pos;
    }

    public void setPos(PosType pos) {
        this.pos = pos;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public OverflowType getOverflow() {
        return overflow;
    }

    public void setOverflow(OverflowType overflow) {
        this.overflow = overflow;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }


    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
