package com.ds.dsm.view.config.form;

import com.ds.esd.tool.ui.enums.FormLayModeType;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.tool.ui.enums.StretchType;

public class FormBean {

    String viewInstId;
    String domainId;
    String sourceClassName;
    String methodName;
    Integer col;

    Integer defaultColumnSize;

    StretchType stretchH;

    FormLayModeType mode;

    Integer columnHeaderHeight;

    HAlignType textAlign = HAlignType.center;

    Boolean solidGridlines;

    Integer defaultRowHeight;

    StretchType stretchHeight;

    Integer defaultRowSize;

    Integer rowHeaderWidth;

    Boolean floatHandler;


    public FormBean() {

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public Integer getCol() {
        return col;
    }

    public void setCol(Integer col) {
        this.col = col;
    }

    public Integer getDefaultColumnSize() {
        return defaultColumnSize;
    }

    public void setDefaultColumnSize(Integer defaultColumnSize) {
        this.defaultColumnSize = defaultColumnSize;
    }

    public StretchType getStretchH() {
        return stretchH;
    }

    public void setStretchH(StretchType stretchH) {
        this.stretchH = stretchH;
    }

    public FormLayModeType getMode() {
        return mode;
    }

    public void setMode(FormLayModeType mode) {
        this.mode = mode;
    }

    public Integer getColumnHeaderHeight() {
        return columnHeaderHeight;
    }

    public void setColumnHeaderHeight(Integer columnHeaderHeight) {
        this.columnHeaderHeight = columnHeaderHeight;
    }

    public HAlignType getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(HAlignType textAlign) {
        this.textAlign = textAlign;
    }

    public Boolean getSolidGridlines() {
        return solidGridlines;
    }

    public void setSolidGridlines(Boolean solidGridlines) {
        this.solidGridlines = solidGridlines;
    }

    public Integer getDefaultRowHeight() {
        return defaultRowHeight;
    }

    public void setDefaultRowHeight(Integer defaultRowHeight) {
        this.defaultRowHeight = defaultRowHeight;
    }

    public StretchType getStretchHeight() {
        return stretchHeight;
    }

    public void setStretchHeight(StretchType stretchHeight) {
        this.stretchHeight = stretchHeight;
    }

    public Integer getDefaultRowSize() {
        return defaultRowSize;
    }

    public void setDefaultRowSize(Integer defaultRowSize) {
        this.defaultRowSize = defaultRowSize;
    }

    public Integer getRowHeaderWidth() {
        return rowHeaderWidth;
    }

    public void setRowHeaderWidth(Integer rowHeaderWidth) {
        this.rowHeaderWidth = rowHeaderWidth;
    }

    public Boolean getFloatHandler() {
        return floatHandler;
    }

    public void setFloatHandler(Boolean floatHandler) {
        this.floatHandler = floatHandler;
    }
}
