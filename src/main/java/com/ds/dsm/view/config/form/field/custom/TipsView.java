package com.ds.dsm.view.config.form.field.custom;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.field.InputFieldBean;
import com.ds.esd.custom.field.TipsBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import net.sf.cglib.beans.BeanMap;

@BottomBarMenu
@FormAnnotation(col = 1, customService = TipsService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class TipsView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    public String fieldname;


    @FieldAnnotation(rowHeight = "50")
    @CustomAnnotation(caption = "错误提示")
    String tipsErr;
    @FieldAnnotation(rowHeight = "50")
    @CustomAnnotation(caption = "成功提示")
    String tipsOK;
    @FieldAnnotation(rowHeight = "50")
    @CustomAnnotation(caption = "提示绑定")
    String tipsBinder;
    @FieldAnnotation(rowHeight = "50")
    @CustomAnnotation(caption = "预装载提示")
    String mask;
    @FieldAnnotation(rowHeight = "50")
    @CustomAnnotation(caption = "提示")
    String tips;
    @FieldAnnotation(rowHeight = "50")
    @CustomAnnotation(caption = "空白信息替换")
    String placeholder;


    public TipsView() {

    }


    public TipsView(FieldFormConfig<InputFieldBean,?> config) {
        TipsBean tipsBean = config.getTipsBean();
        if (tipsBean == null) {
            tipsBean = new TipsBean();
        }
        BeanMap.create(this).putAll(BeanMap.create(tipsBean));
        this.methodName=config.getMethodName();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();


    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getTipsErr() {
        return tipsErr;
    }

    public void setTipsErr(String tipsErr) {
        this.tipsErr = tipsErr;
    }

    public String getTipsOK() {
        return tipsOK;
    }

    public void setTipsOK(String tipsOK) {
        this.tipsOK = tipsOK;
    }

    public String getTipsBinder() {
        return tipsBinder;
    }

    public void setTipsBinder(String tipsBinder) {
        this.tipsBinder = tipsBinder;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
