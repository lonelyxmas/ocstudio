package com.ds.dsm.view.config;

import com.ds.dsm.view.config.action.CustomBuildAction;
import com.ds.dsm.view.config.grid.GridPageBarService;
import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.PageBarBean;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.AnnotationUtil;

@BottomBarMenu(menuClass = CustomBuildAction.class)
@FormAnnotation(col = 1,
        bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet}, customService = GridPageBarService.class)
public class PageBarView {


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceMethodName;


    @FieldAnnotation(colWidth = "160")
    @CustomAnnotation(caption = "是否显示分页栏")
    Boolean pageBar;

    @ComboInputAnnotation(inputType = ComboInputType.spin)
    @FieldAnnotation(componentType = ComponentType.ComboInput, colSpan = -1)
    @CustomAnnotation(caption = "默认页数")
    Integer pageCount;

    @CustomAnnotation(caption = "禁用")
    Boolean disabled = false;

    @CustomAnnotation(caption = "平铺方式")
    Dock dock;


    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "标题名称")
    String pageCaption;

    @CustomAnnotation(caption = "上一页")
    String prevMark;

    @CustomAnnotation(caption = "下一页")
    String nextMark;


    @CustomAnnotation(caption = "自动提示")
    Boolean autoTips = false;

    @CustomAnnotation(caption = "分页多级按钮")
    Boolean showMoreBtns = true;

    @CustomAnnotation(caption = "高度")
    String height = "2.5em";

    @CustomAnnotation(caption = "URL模板")
    String uriTpl = "#*";

    @CustomAnnotation(caption = "文本模板")
    String textTpl = "*";

    @CustomAnnotation(caption = "显示值")
    String value = "1:1:1";

    @CustomAnnotation(caption = "只读", hidden = true)
    Boolean readonly = false;

    @CustomAnnotation(caption = "父级ID", hidden = true)
    String parentID;

    public PageBarView() {

    }



    public PageBarView(CustomGridViewBean gridConfig) {
        if (gridConfig == null) {
            gridConfig = new CustomGridViewBean();
        }
        this.viewInstId = gridConfig.getViewInstId();
        this.domainId = gridConfig.getDomainId();
        this.viewClassName = gridConfig.getViewClassName();
        this.sourceClassName = gridConfig.getSourceClassName();
        this.sourceMethodName = gridConfig.getMethodName();

        PageBarBean pageBarBean = gridConfig.getPageBar();

        if (pageBarBean == null) {
            pageBarBean = AnnotationUtil.fillDefaultValue(PageBar.class, new PageBarBean());
            pageBar = false;
        } else {
            pageBar = true;
        }

        init(pageBarBean);

    }

    void init(PageBarBean pageBarBean) {
        this.disabled = pageBarBean.getDisabled();
        this.readonly = pageBarBean.getReadonly();
        this.autoTips = pageBarBean.getAutoTips();
        this.textTpl = pageBarBean.getTextTpl();
        this.value = pageBarBean.getValue();
        this.parentID = pageBarBean.getParentID();
        this.uriTpl = pageBarBean.getUriTpl();
        this.height = pageBarBean.getHeight();
        this.dock = pageBarBean.getDock();
        this.pageCaption = pageBarBean.getPageCaption();
        this.prevMark = pageBarBean.getPrevMark();
        this.nextMark = pageBarBean.getNextMark();
        this.pageCount = pageBarBean.getPageCount();
        this.showMoreBtns = pageBarBean.getShowMoreBtns();


    }


    public PageBarView(CustomGalleryViewBean gridConfig) {
        if (gridConfig == null) {
            gridConfig = new CustomGalleryViewBean();
        }
        this.viewInstId = gridConfig.getViewInstId();
        this.domainId = gridConfig.getDomainId();
        this.sourceClassName = gridConfig.getSourceClassName();
        this.viewClassName = gridConfig.getViewClassName();
        this.sourceMethodName = gridConfig.getMethodName();
        PageBarBean pageBarBean = gridConfig.getPageBar();
        if (pageBarBean == null) {
            pageBarBean = AnnotationUtil.fillDefaultValue(PageBar.class, new PageBarBean());
        }
        init(pageBarBean);


    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Boolean getAutoTips() {
        return autoTips;
    }

    public void setAutoTips(Boolean autoTips) {
        this.autoTips = autoTips;
    }

    public String getTextTpl() {
        return textTpl;
    }

    public void setTextTpl(String textTpl) {
        this.textTpl = textTpl;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getNextMark() {
        return nextMark;
    }

    public void setNextMark(String nextMark) {
        this.nextMark = nextMark;
    }

    public String getPageCaption() {
        return pageCaption;
    }

    public void setPageCaption(String pageCaption) {
        this.pageCaption = pageCaption;
    }

    public String getPrevMark() {
        return prevMark;
    }

    public void setPrevMark(String prevMark) {
        this.prevMark = prevMark;
    }

    public Boolean getPageBar() {
        return pageBar;
    }

    public void setPageBar(Boolean pageBar) {
        this.pageBar = pageBar;
    }


    public Boolean getShowMoreBtns() {
        return showMoreBtns;
    }

    public void setShowMoreBtns(Boolean showMoreBtns) {
        this.showMoreBtns = showMoreBtns;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }
    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public String getUriTpl() {
        return uriTpl;
    }

    public void setUriTpl(String uriTpl) {
        this.uriTpl = uriTpl;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }
}
