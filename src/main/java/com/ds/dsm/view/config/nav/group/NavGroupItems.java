package com.ds.dsm.view.config.nav.group;

import com.ds.dsm.view.config.form.ViewFormActionService;
import com.ds.dsm.view.config.nav.group.item.GroupItemNavService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum NavGroupItems implements TreeItem {
    GroupInfoGroup("基础信息", "spafont spa-icon-config", ViewGroupInfoService.class, false, false, false),
    GroupEventGroup("动作事件", "spafont spa-icon-event", ViewFormActionService.class, false, false, false),
    FieldFormList("参数列表", "spafont spa-icon-c-comboinput", ViewGroupHiddensService.class, false, false, false),
    NavLayoutView("导航布局配置", "spafont spa-icon-c-layout", ViewGroupLayoutService.class, false, false, false),
    GroupItemList("组子项列表", "spafont spa-icon-conf", GroupItemNavService.class, true, true, true);
    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    NavGroupItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
