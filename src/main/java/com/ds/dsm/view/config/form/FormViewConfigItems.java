package com.ds.dsm.view.config.form;

import com.ds.esd.custom.tree.enums.TreeItem;

public enum FormViewConfigItems implements TreeItem {
    FormViewConfig("表格布局配置", "spafont spa-icon-c-layout", ViewFormService.class, false, false, false),
    RowConfig("表格行头配置", "spafont spa-icon-c-grid", ViewFormRowService.class, false, false, false),
    HiddenFields("环境变量", "spafont spa-icon-c-hiddeninput", ViewFormHiddenFieldsService.class, true, true, true);
    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    FormViewConfigItems(String name, String imageClass, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
