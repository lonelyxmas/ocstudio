package com.ds.dsm.view.config.gallery.item.flag.custom;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.gallery.GalleryFlagCmdBean;
import com.ds.esd.custom.gallery.annotation.GalleryFlagCmd;
import com.ds.esd.custom.gallery.enums.GalleryFlagMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.AnnotationUtil;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping(path = "/dsm/view/config/gallery/flag/")
@MethodChinaName(cname = "常用状态", imageClass = "spafont spa-icon-c-gallery")
public class CustomFlagMenuService {

    @RequestMapping(method = RequestMethod.POST, value = "CustomFlagMenu")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "CustomFlagMenu",
            imageClass = "spafont spa-icon-c-toolbar")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<CustomFlagMenuGridView>> getCustomFlagMenu(String sourceClassName, String sourceMethodName, String domainId) {
        ListResultModel<List<CustomFlagMenuGridView>> resultModel = new ListResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(sourceMethodName);
            CustomGalleryViewBean customGalleryViewBean = null;

            if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
            } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "选择常用状态")
    @RequestMapping("CustomFlagTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<CustomFlagMenuPopTree>> getCustomRowMenuTree(String sourceClassName, String methodName, String domainId) {
        TreeListResultModel<List<CustomFlagMenuPopTree>> model = new TreeListResultModel<>();
        List<CustomFlagMenuPopTree> popTrees = new ArrayList<>();
        CustomFlagMenuPopTree menuPopTree = new CustomFlagMenuPopTree("flagMenu", "选择常用状态", "flagMenu");
        List<CustomFlagMenuPopTree> menuTrees = TreePageUtil.fillObjs(Arrays.asList(GalleryFlagMenu.values()), CustomFlagMenuPopTree.class);
        menuPopTree.setSub(menuTrees);
        popTrees.add(menuPopTree);
        model.setData(popTrees);

        model.setData(popTrees);
        return model;
    }


    @RequestMapping("addCustomFlagMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addCustomFlagMenu(String sourceClassName, String methodName, String CustomFlagTree, String domainId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGalleryViewBean customGalleryViewBean = null;
            if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
            } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
            }


        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;

    }


    @RequestMapping("delCustomContextMenu")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delCustomContextMenu(String sourceClassName, String methodName, String type, String bar, String domainId) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            CustomGalleryViewBean customGalleryViewBean = null;
            if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();
            } else if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
            }


            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


}
