package com.ds.dsm.view.config.nav;

import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.*;

public class CustomNavViewBean {
    @ComboListBoxAnnotation
    @CustomListAnnotation(bindClass = CustomImageType.class)
    @CustomAnnotation(caption = "图标")
    String imageClass;

    @CustomAnnotation(caption = "显示名称")
    String caption;

    @CustomAnnotation(caption = "选中方式")
    SelModeType selMode;

    @CustomAnnotation(caption = "占位符")
    Boolean togglePlaceholder;

    @CustomAnnotation(caption = "装载消息")
    Boolean animCollapse;

    @CustomAnnotation(caption = "动态销毁")
    Boolean dynDestory;

    String expression;
    @CustomAnnotation(caption = "默认值")
    String value;

    @CustomAnnotation(caption = "装载消息")
    String message;

    @CustomAnnotation(caption = "最大高度")
    Integer maxHeight;

    @CustomAnnotation(caption = "延迟注入")
    Boolean lazyAppend;

    @CustomAnnotation(caption = "表单提交")
    Boolean formField;

    @CustomAnnotation(caption = "值分割符")
    String valueSeparator;


    @CustomAnnotation(caption = "居中")
    TagCmdsAlign tagCmdsAlign;

    @CustomAnnotation(caption = "分组显示")
    Boolean group;


    @CustomAnnotation(caption = "单击展开")
    Boolean singleOpen;

    @CustomAnnotation(caption = "默认折叠")
    Boolean iniFold;

    @CustomAnnotation(caption = "配置按钮")
    String optBtn;

    @CustomAnnotation(caption = "默认选中最后")
    Boolean activeLast;

    @CustomAnnotation(caption = "关闭按钮")
    Boolean closeBtn;

    @CustomAnnotation(caption = "弹出按钮")
    Boolean popBtn;

    @CustomAnnotation(caption = "对齐方式")
    BarLocationType barLocation;

    @CustomAnnotation(caption = "横向对齐")
    HAlignType barHAlign;

    @CustomAnnotation(caption = "纵向对齐")
    VAlignType barVAlign;

    @CustomAnnotation(caption = "工具栏大小")
    String barSize;

    @CustomAnnotation(caption = "侧栏大小")
    String sideBarSize;

    @CustomAnnotation(caption = "是否展开")
    SideBarStatusType sideBarStatus;

    String methodName;

    String viewInstId;

    String domainId;

    String sourceClassName;

    Class bindService;

    public CustomNavViewBean() {

    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }

    public Boolean getTogglePlaceholder() {
        return togglePlaceholder;
    }

    public void setTogglePlaceholder(Boolean togglePlaceholder) {
        this.togglePlaceholder = togglePlaceholder;
    }

    public Boolean getAnimCollapse() {
        return animCollapse;
    }

    public void setAnimCollapse(Boolean animCollapse) {
        this.animCollapse = animCollapse;
    }

    public Boolean getDynDestory() {
        return dynDestory;
    }

    public void setDynDestory(Boolean dynDestory) {
        this.dynDestory = dynDestory;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(Integer maxHeight) {
        this.maxHeight = maxHeight;
    }

    public Boolean getLazyAppend() {
        return lazyAppend;
    }

    public void setLazyAppend(Boolean lazyAppend) {
        this.lazyAppend = lazyAppend;
    }

    public Boolean getFormField() {
        return formField;
    }

    public void setFormField(Boolean formField) {
        this.formField = formField;
    }

    public String getValueSeparator() {
        return valueSeparator;
    }

    public void setValueSeparator(String valueSeparator) {
        this.valueSeparator = valueSeparator;
    }

    public TagCmdsAlign getTagCmdsAlign() {
        return tagCmdsAlign;
    }

    public void setTagCmdsAlign(TagCmdsAlign tagCmdsAlign) {
        this.tagCmdsAlign = tagCmdsAlign;
    }

    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }

    public Boolean getSingleOpen() {
        return singleOpen;
    }

    public void setSingleOpen(Boolean singleOpen) {
        this.singleOpen = singleOpen;
    }

    public Boolean getIniFold() {
        return iniFold;
    }

    public void setIniFold(Boolean iniFold) {
        this.iniFold = iniFold;
    }

    public String getOptBtn() {
        return optBtn;
    }

    public void setOptBtn(String optBtn) {
        this.optBtn = optBtn;
    }

    public Boolean getActiveLast() {
        return activeLast;
    }

    public void setActiveLast(Boolean activeLast) {
        this.activeLast = activeLast;
    }

    public Boolean getCloseBtn() {
        return closeBtn;
    }

    public void setCloseBtn(Boolean closeBtn) {
        this.closeBtn = closeBtn;
    }

    public Boolean getPopBtn() {
        return popBtn;
    }

    public void setPopBtn(Boolean popBtn) {
        this.popBtn = popBtn;
    }

    public BarLocationType getBarLocation() {
        return barLocation;
    }

    public void setBarLocation(BarLocationType barLocation) {
        this.barLocation = barLocation;
    }

    public HAlignType getBarHAlign() {
        return barHAlign;
    }

    public void setBarHAlign(HAlignType barHAlign) {
        this.barHAlign = barHAlign;
    }

    public VAlignType getBarVAlign() {
        return barVAlign;
    }

    public void setBarVAlign(VAlignType barVAlign) {
        this.barVAlign = barVAlign;
    }

    public String getBarSize() {
        return barSize;
    }

    public void setBarSize(String barSize) {
        this.barSize = barSize;
    }

    public String getSideBarSize() {
        return sideBarSize;
    }

    public void setSideBarSize(String sideBarSize) {
        this.sideBarSize = sideBarSize;
    }

    public SideBarStatusType getSideBarStatus() {
        return sideBarStatus;
    }

    public void setSideBarStatus(SideBarStatusType sideBarStatus) {
        this.sideBarStatus = sideBarStatus;
    }

    public Class getBindService() {
        return bindService;
    }

    public void setBindService(Class bindService) {
        this.bindService = bindService;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }
    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
