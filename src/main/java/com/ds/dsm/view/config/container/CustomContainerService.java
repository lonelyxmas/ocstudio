package com.ds.dsm.view.config.container;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ContainerBean;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavTabsBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/container/")
public class CustomContainerService {

    @RequestMapping(method = RequestMethod.POST, value = "CustomContainerConfig")
    @FormViewAnnotation(autoSave = true)
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "基础信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<CustomContainerView> getCustomContainerConfig(String sourceClassName, String sourceMethodName, String domainId, String viewInstId) {
        ResultModel<CustomContainerView> result = new ResultModel<CustomContainerView>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            NavTabsBaseViewBean tabsBaseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
            CustomViewBean  viewBean = tabsBaseViewBean.getCurrViewBean();
            result.setData(new CustomContainerView(viewBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "updateCustomContainer")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    @ResponseBody
    public ResultModel<Boolean> updateCustomContainer(@RequestBody CustomContainerView containerView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(containerView.getSourceClassName(), containerView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(containerView.getMethodName());
            NavTabsBaseViewBean viewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
            ContainerBean containerBean = viewBean.getCurrViewBean().getContainerBean();
            Map<String, Object> configMap = BeanMap.create(containerBean);
            configMap.putAll(BeanMap.create(containerView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "reSetCustomContainer")
    @APIEventAnnotation(bindMenu = CustomMenuItem.formReSet)
    @ResponseBody
    public ResultModel<Boolean> reSetCustomContainer(@RequestBody CustomContainerView containerView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(containerView.getSourceClassName(), containerView.getViewInstId());
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(containerView.getMethodName());
            NavTabsBaseViewBean tabsBaseViewBean = (NavTabsBaseViewBean) customMethodAPIBean.getView();
            CustomViewBean  viewBean = tabsBaseViewBean.getCurrViewBean();
            viewBean.setContainerBean(new ContainerBean());
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
