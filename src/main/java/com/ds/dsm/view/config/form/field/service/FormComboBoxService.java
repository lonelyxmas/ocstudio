package com.ds.dsm.view.config.form.field.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.combo.FieldCompoView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/combo/")
@MethodChinaName(cname = "组合输入框", imageClass = "spafont spa-icon-c-comboinput")

public class FormComboBoxService {

//
//    @RequestMapping(method = RequestMethod.POST, value = "ComboConfig")
//    @ModuleAnnotation(dynLoad = true, caption = "字段子域", index = 0)
//    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
//    @ResponseBody
//    public TreeListResultModel<List<FormFieldNavTree>> getComboConfig(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
//        TreeListResultModel<List<FormFieldNavTree>> result = new TreeListResultModel<>();
//        ViewEntityConfig classConfig = null;
//        List<ComboWidgetNavItem> fieldWidgetNavItems = new ArrayList<>();
//        try {
//            classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
//            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
//            CustomView viewBean = customMethodAPIBean.getView();
//            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);
//            ComboBoxBean comboBoxBean = (ComboBoxBean) formInfo.getWidgetConfig();
//            ComboInputType inputType = comboBoxBean.getInputType();
//            List<ComboWidgetNavItem> fieldInputItems = ComboWidgetNavItem.getInputByComponentType(inputType);
//            for (ComboWidgetNavItem fieldInputItem : fieldInputItems) {
//
//                fieldWidgetNavItems.add(fieldInputItem);
//
//            }
//
//
//        } catch (JDSException e) {
//            e.printStackTrace();
//        }
//
//        result = TreePageUtil.getTreeList(fieldWidgetNavItems, FormFieldNavTree.class);
//        return result;
//    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldComboView")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass ="spafont spa-icon-c-comboinput", caption = "组合输入框")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<FieldCompoView> getFieldComboView(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<FieldCompoView> result = new ResultModel<FieldCompoView>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new FieldCompoView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}
