package com.ds.dsm.view.config.nav.buttonviews;

import com.ds.esd.custom.tree.enums.TreeItem;

public enum NavButtonViewsItems implements TreeItem {
    TabsInfoGroup("基础信息", "spafont spa-icon-config", ViewNavButtonViewsConfigService.class, true, true, true),
    TabItemList("Tab页配置", "spafont spa-icon-conf", ButtonViewsModulesConfigService.class, true, true, true),
    FieldFormList("参数列表", "spafont spa-icon-c-comboinput", ButtonViewsFieldsConfigService.class, true, true, true);

    private final String name;
    private final String imageClass;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    NavButtonViewsItems(String name, String imageClass, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
