package com.ds.dsm.view.config.gallery.item.contextmenu;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.grid.cell.contextmenu.ContextCellMenuConfigView;
import com.ds.dsm.view.config.grid.cell.contextmenu.ContextCellMenuNav;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.RightContextMenuBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldGalleryConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.jds.core.esb.util.OgnlUtil;
import com.ds.web.util.AnnotationUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/gallery/contextmenu/")
@MethodChinaName(cname = "右键菜单", imageClass = "spafont spa-icon-c-gallery")

public class ContextGalleryService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "右键菜单")
    @RequestMapping(method = RequestMethod.POST, value = "ContextMenuInfo")
    @NavGroupViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(dock = Dock.fill, caption = "单元格菜单", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<ContextGalleryMenuNav> getContextMenuInfo(String domainId, String sourceClassName, String methodName) {
        ResultModel<ContextGalleryMenuNav> result = new ResultModel<ContextGalleryMenuNav>();
        return result;
    }


    @MethodChinaName(cname = "保存右键菜单")
    @RequestMapping(value = {"saveContextMenuInfo"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveCsaveContextMenuInfo(@RequestBody ContextCellMenuConfigView menuConfigView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ApiClassConfig tableConfig = null;
        try {
            tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(menuConfigView.getSourceClassName(), domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(menuConfigView.getMethodName());
            CustomGalleryViewBean galleryViewBean=null;

            if (methodAPIBean.getView() instanceof NavGalleryViewBean){
                galleryViewBean=((NavGalleryViewBean)methodAPIBean.getView()).getGalleryViewBean();
            }else if(methodAPIBean.getView() instanceof NavGalleryViewBean){
                galleryViewBean= (CustomGalleryViewBean) methodAPIBean.getView();
            }

             RightContextMenuBean barMenuBean = galleryViewBean.getContextMenuBean();
            if (barMenuBean == null) {
                barMenuBean = AnnotationUtil.fillDefaultValue(RightContextMenu.class, new RightContextMenuBean());
            }
            OgnlUtil.setProperties(BeanMap.create(menuConfigView), barMenuBean, false, false);
            galleryViewBean.setContextMenuBean(barMenuBean);

            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;

    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
