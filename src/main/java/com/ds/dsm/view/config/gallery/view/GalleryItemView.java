package com.ds.dsm.view.config.gallery.view;

import com.ds.dsm.view.config.gallery.service.GalleryItemInfoService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.tool.ui.enums.BorderType;
import com.ds.esd.tool.ui.enums.Dock;

@BottomBarMenu
@FormAnnotation(col = 2, customService = GalleryItemInfoService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class GalleryItemView {


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;


    @CustomAnnotation(caption = "自适应大小")
    Boolean resizer;
    @CustomAnnotation(caption = "背景图片")
    String bgimg;
    @CustomAnnotation(caption = "字体大小")
    String iconFontSize;
    @CustomAnnotation(caption = "边框")
    BorderType borderType;
    @CustomAnnotation(caption = "子项边距")
    String itemPadding;
    @CustomAnnotation(caption = "子项间隔")
    String itemMargin;
    @CustomAnnotation(caption = "宽度")
    String itemWidth;
    @CustomAnnotation(caption = "高度")
    String itemHeight;
    @CustomAnnotation(caption = "图片宽度")
    String imgWidth;
    @CustomAnnotation(caption = "图片高度")
    String imgHeight;

    @CustomAnnotation(caption = "平铺")
    Dock dock;


    public GalleryItemView() {

    }

    public GalleryItemView(CustomGalleryViewBean gridConfig) {
        if (gridConfig == null) {
            gridConfig = new CustomGalleryViewBean();
        }
        this.viewClassName = gridConfig.getViewClassName();
        this.sourceClassName = gridConfig.getSourceClassName();
        this.methodName = gridConfig.getMethodName();
        this.viewInstId = gridConfig.getViewInstId();
        this.domainId = gridConfig.getDomainId();
        this.resizer = gridConfig.getResizer();
        this.bgimg = gridConfig.getBgimg();
        this.iconFontSize = gridConfig.getIconFontSize();
        this.borderType = gridConfig.getBorderType();
        this.itemPadding = gridConfig.getItemPadding();
        this.itemMargin = gridConfig.getItemMargin();
        this.itemHeight = gridConfig.getItemHeight();
        this.itemWidth = gridConfig.getItemWidth();
        this.imgHeight = gridConfig.getImgHeight();
        this.imgWidth = gridConfig.getImgWidth();
        this.dock = gridConfig.getDock();

    }

    public Boolean getResizer() {
        return resizer;
    }

    public void setResizer(Boolean resizer) {
        this.resizer = resizer;
    }

    public String getBgimg() {
        return bgimg;
    }

    public void setBgimg(String bgimg) {
        this.bgimg = bgimg;
    }

    public String getIconFontSize() {
        return iconFontSize;
    }

    public void setIconFontSize(String iconFontSize) {
        this.iconFontSize = iconFontSize;
    }

    public BorderType getBorderType() {
        return borderType;
    }

    public void setBorderType(BorderType borderType) {
        this.borderType = borderType;
    }

    public String getItemPadding() {
        return itemPadding;
    }

    public void setItemPadding(String itemPadding) {
        this.itemPadding = itemPadding;
    }

    public String getItemMargin() {
        return itemMargin;
    }

    public void setItemMargin(String itemMargin) {
        this.itemMargin = itemMargin;
    }

    public String getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(String itemWidth) {
        this.itemWidth = itemWidth;
    }

    public String getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(String itemHeight) {
        this.itemHeight = itemHeight;
    }

    public String getImgWidth() {
        return imgWidth;
    }

    public void setImgWidth(String imgWidth) {
        this.imgWidth = imgWidth;
    }

    public String getImgHeight() {
        return imgHeight;
    }

    public void setImgHeight(String imgHeight) {
        this.imgHeight = imgHeight;
    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }
    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


}
