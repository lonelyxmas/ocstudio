package com.ds.dsm.view.config.form.field.combo.pop;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.module.ModuleBindEntityTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/combo/pop/")
@MethodChinaName(cname = "弹出配置", imageClass = "spafont spa-icon-c-gallery")
public class PopBindEntityService {


    @MethodChinaName(cname = "弹出配置")
    @RequestMapping(value = {"ModuleBindEntityTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "弹出配置", dynLoad = true, dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.fieldNodeEditor)
    @ResponseBody
    public TreeListResultModel<List<PopBindEntityTree>> getAggEntityTree(String domainId, String projectName, String euPackageName) {
        TreeListResultModel<List<PopBindEntityTree>> result = new TreeListResultModel<List<PopBindEntityTree>>();
        try {
            List<EUPackage> childs = new ArrayList<>();
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
           // List<EUPackage> packages = ESDFacrory.getESDClient().getAllPackage(version.getVersionName());
            List<EUPackage> packages = ESDFacrory.getESDClient().getAllPackage("DSMdsm");
            List<String> esdClassNameList = new ArrayList<>();
            for (EUPackage euPackage : packages) {
                if (euPackageName == null || euPackageName.equals("") || euPackageName.startsWith(euPackage.getPackageName())) {
                    Set<EUModule> moduleList = euPackage.listModules();
                    for (EUModule module : moduleList) {
                        if (module.getRealClassName().equals(module.getClassName())) {
                            MethodConfig methodConfig = module.getComponent().getMethodAPIBean();
                            if (methodConfig != null && methodConfig.isModule()) {
                                if (methodConfig.getBindMenus().contains(CustomMenuItem.index) || methodConfig.getBindMenus().contains(CustomMenuItem.treeNodeEditor)) {
                                    esdClassNameList.add(methodConfig.getSourceClassName());
                                    childs.add(euPackage);
                                }
                            }
                        }
                    }
                }
            }


            Collections.sort(childs, new Comparator<EUPackage>() {
                public int compare(EUPackage o1, EUPackage o2) {
                    return o1.getPackageName().compareTo(o2.getPackageName());
                }
            });

            result = TreePageUtil.getTreeList(childs, PopBindEntityTree.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "saveTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save, callback = {CustomCallBack.Close, CustomCallBack.ReloadParent})
    @ResponseBody
    public ResultModel<Boolean> saveTree(String ModuleBindEntityTree) {
        return new ResultModel<>();
    }

}
