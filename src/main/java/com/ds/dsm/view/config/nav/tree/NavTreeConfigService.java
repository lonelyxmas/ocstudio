package com.ds.dsm.view.config.nav.tree;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.tree.TreeBaseDataView;
import com.ds.dsm.view.config.tree.TreeView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.TreeDataBaseBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/tree/")
public class NavTreeConfigService {
    @MethodChinaName(cname = "编辑基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateTreeView")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.formSave)
    public @ResponseBody
    ResultModel<Boolean> updateTreeBase(@RequestBody TreeView configView) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(configView.getSourceClassName(), configView.getDomainId());
            MethodConfig methodConfig = classConfig.getMethodByName(configView.getMethodName());
            CustomTreeViewBean viewConfig = null;
            if (methodConfig.getView() instanceof CustomTreeViewBean) {
                viewConfig = (CustomTreeViewBean) methodConfig.getView();
            } else if (methodConfig.getView() instanceof NavBaseViewBean) {
                viewConfig = ((NavBaseViewBean) methodConfig.getView()).getTreeView();
            }

            TreeDataBaseBean dataConfig = (TreeDataBaseBean) methodConfig.getDataBean();
            viewConfig.setViewInstId(configView.getViewInstId());
            viewConfig.setDomainId(configView.getDomainId());
            viewConfig.setSourceClassName(configView.getSourceClassName());
            viewConfig.setAnimCollapse(configView.getAnimCollapse());
            viewConfig.setIniFold(configView.getIniFold());
            viewConfig.setDynDestory(configView.getDynDestory());
            viewConfig.setTogglePlaceholder(configView.getTogglePlaceholder());
            if (viewConfig.getRowCmdBean() != null) {
                viewConfig.getRowCmdBean().setTagCmdsAlign(configView.getTagCmdsAlign());
            }

            viewConfig.setGroup(configView.getGroup());
            if (configView.getOptBtn() != null) {
                viewConfig.setOptBtn(configView.getOptBtn());
            }
            if (configView.getSingleOpen() != null) {
                viewConfig.setSingleOpen(configView.getSingleOpen());
            }
            viewConfig.setSelMode(configView.getSelMode());
            viewConfig.setCaption(configView.getCaption());
            if (configView.getFormField() != null) {
                viewConfig.setFormField(configView.getFormField());
            }

            if (configView.getHeplBar() != null) {
                viewConfig.setHeplBar(configView.getHeplBar());
            }

            viewConfig.setValueSeparator(configView.getValueSeparator());
            dataConfig.setFieldCaption(configView.getFieldCaption());
            dataConfig.setFieldId(configView.getFieldId());
            dataConfig.setRootId(configView.getRootId());
            dataConfig.setMethodName(configView.getMethodName());
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "编辑数据信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateTreeWinData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateTreeWinData(@RequestBody CustomModuleBean bean) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(bean.getSourceClassName(), bean.getViewInstId());
            MethodConfig methodAPIBean = config.getMethodByName(bean.getMethodName());
            CustomModuleBean customModuleBean = methodAPIBean.getModuleBean();
            BeanMap.create(customModuleBean).putAll(BeanMap.create(bean));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "清空列表配置")
    @RequestMapping(method = RequestMethod.POST, value = "clearData")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clearData(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig methodConfig = config.getMethodByName(methodName);
            DSMFactory.getInstance().getViewManager().delViewEntity(methodConfig.getViewClassName(), viewInstId);

        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "清空列表配置")
    @RequestMapping(method = RequestMethod.POST, value = "clear")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clear(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = config.getMethodByName(methodName);
            customMethodAPIBean.setView(null);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(config);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateTreeDataBase")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> updateTreeDataBase(@RequestBody TreeBaseDataView dataView) {

        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(dataView.getSourceClassName(), dataView.getDomainId());
            MethodConfig methodConfig = classConfig.getMethodByName(dataView.getMethodName());
            CustomTreeViewBean customTreeViewBean = ((NavBaseViewBean) methodConfig.getView()).getTreeView();
            BeanMap.create(customTreeViewBean).putAll(BeanMap.create(dataView));
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
