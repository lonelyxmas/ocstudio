package com.ds.dsm.view.config.nav.group.item;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomModuleBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.group.GroupItemBean;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.web.json.JSONData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/group/item/")
@MethodChinaName(cname = "Group子对象", imageClass = "spafont spa-icon-c-comboinput")
public class GroupItemService {
    @RequestMapping(method = RequestMethod.POST, value = "GroupItemInfo")
    @FormViewAnnotation
    @ModuleAnnotation(caption = "组信息")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor, autoRun = true)
    @ResponseBody
    public ResultModel<GroupItemView> getGroupItemInfo(String sourceClassName, String sourceMethodName, String entityClassName, String methodName, String domainId, String viewInstId, String groupId) {
        ResultModel<GroupItemView> result = new ResultModel<GroupItemView>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(sourceMethodName);
            NavGroupViewBean groupViewBean = (NavGroupViewBean) methodAPIBean.getView();
            GroupItemBean groupItemBean = (GroupItemBean) groupViewBean.getItemBean(groupId);
            result.setData(new GroupItemView(groupItemBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "批量保存")
    @RequestMapping(method = RequestMethod.POST, value = "updateAllGroupInfo")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.saveAllRow)
    public @ResponseBody
    ResultModel<Boolean> updateAllGroupInfo(@JSONData List<GroupItemGrid> rows, String sourceClassName, String viewInstId, String groupId, String sourceMethodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig apiClassConfig=tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = apiClassConfig.getMethodByName(sourceMethodName);
            for (GroupItemGrid config : rows) {
                NavGroupViewBean groupViewBean = (NavGroupViewBean) methodAPIBean.getView();
                GroupItemBean groupItemBean = (GroupItemBean) groupViewBean.getItemBean(groupId);
                groupItemBean.setIniFold(config.getIniFold());
                groupItemBean.setLazyAppend(config.getLazyAppend());
                groupItemBean.getMethodConfig().setCaption(config.getCaption());
                CustomModuleBean moduleBean=groupItemBean.getMethodConfig().getModuleBean();
                if (!config.getViewType().equals(moduleBean.getModuleViewType())) {
                    moduleBean.setModuleViewType(config.getViewType());
                    groupItemBean.getMethodConfig().setView(null);
                }

            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(apiClassConfig);


        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

}
