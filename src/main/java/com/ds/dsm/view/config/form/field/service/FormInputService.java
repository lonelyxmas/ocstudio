package com.ds.dsm.view.config.form.field.service;

import com.alibaba.fastjson.JSONObject;
import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.FieldInputView;
import com.ds.dsm.view.config.form.field.custom.FieldLayoutView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.field.InputFieldBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/input/")
@MethodChinaName(cname = "普通输入域", imageClass = "spafont spa-icon-c-comboinput")

public class FormInputService {


    @RequestMapping(method = RequestMethod.POST, value = "FieldInputInfo")
    @FormViewAnnotation(autoSave = true)
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "普通输入域", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.treeNodeEditor})
    @ResponseBody
    public ResultModel<FieldInputView> getFieldInputInfo(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<FieldInputView> result = new ResultModel<FieldInputView>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new FieldInputView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "编辑字段配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateInputInfo")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateInputInfo(@RequestBody FieldLayoutView config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getViewClassName();
            if (className != null && !className.equals("")) {
                String json = JSONObject.toJSONString(config);
                InputFieldBean fieldBean = JSONObject.parseObject(json, InputFieldBean.class);
                ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(config.getSourceClassName(), config.getDomainId());
                MethodConfig customMethodAPIBean = classConfig.getMethodByName(config.getMethodName());
                CustomView viewBean = customMethodAPIBean.getView();
                FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(config.getFieldname());
                formInfo.setWidgetConfig(fieldBean);
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
