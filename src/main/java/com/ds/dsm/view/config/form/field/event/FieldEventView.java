package com.ds.dsm.view.config.form.field.event;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.form.enums.FieldEvent;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComponentType;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {FieldEventService.class})
public class FieldEventView {

    @CustomAnnotation(pid = true)
    String className;

    @CustomAnnotation(pid = true)
    String viewInstId;

    @CustomAnnotation(pid = true)
    String domainId;

    @CustomAnnotation(pid = true)
    String fieldname;


    @CustomAnnotation(pid = true)
    String methodName;

    @CustomAnnotation(pid = true)
    String viewClassName;

    @CustomAnnotation(caption = "事件名称", uid = true)
    String eventName;

    @CustomAnnotation(caption = "监听事件")
    String eventKey;

    @CustomAnnotation(caption = "描述")
    public String desc;

    @CustomAnnotation(caption = "表达式")
    public String expression;

    @FieldAnnotation(colSpan = -1, rowHeight = "150", componentType = ComponentType.CodeEditor)
    @CustomAnnotation(caption = "脚本")
    public String script;

    public FieldEventView() {

    }


    public FieldEventView(FieldEvent event, FieldFormConfig formInfo) {
        this.eventKey = event.getEventEnum().getEvent();
        this.desc = event.getName();
        this.expression = event.getExpression();
        this.eventName = event.name();
        this.domainId = formInfo.getDomainId();
        this.viewInstId = formInfo.getDomainId();
        this.methodName = formInfo.getMethodName();
        this.viewClassName= formInfo.getViewClassName();
        this.fieldname = formInfo.getFieldname();


    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
