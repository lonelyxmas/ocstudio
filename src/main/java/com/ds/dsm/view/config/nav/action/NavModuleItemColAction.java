package com.ds.dsm.view.config.nav.action;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/nav/action/")
@Aggregation(type = AggregationType.menu, rootClass = NavModuleItemColAction.class)
public class NavModuleItemColAction {

    @MethodChinaName(cname = "向上")
    @RequestMapping(method = RequestMethod.POST, value = "moveUP")
    @CustomAnnotation(imageClass = "spafont spa-icon-move-up", index = 0)
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> moveUP(String sourceClassName, String sourceMethodName, String fieldname, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            NavBaseViewBean customFormViewBean = (NavBaseViewBean) customMethodAPIBean.getView();
            List<String> fieldNames = new ArrayList<>();
            List<String> displayFieldNames = customFormViewBean.getModuleFieldNames();
            for (String displayName : displayFieldNames) {
                displayFieldNames.add(displayName);
            }

            int k = displayFieldNames.indexOf(fieldname);
            if (k < displayFieldNames.size()) {
                displayFieldNames.add(k - 1, displayFieldNames.remove(k));
            }

            fieldNames.addAll(displayFieldNames);
            fieldNames.addAll(customFormViewBean.getHiddenFieldNames());

            LinkedHashSet<String> setNames = new LinkedHashSet<>();
            for (String displayName : fieldNames) {
                setNames.add(displayName);
            }

            customFormViewBean.setFieldNames(setNames);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "向下")
    @RequestMapping(method = RequestMethod.POST, value = "moveDown")
    @CustomAnnotation(imageClass = "spafont spa-icon-move-down", index = 1)
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> moveDown(String sourceClassName, String sourceMethodName, String fieldname, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            MethodConfig customMethodAPIBean = classConfig.getSourceConfig().getMethodByName(sourceMethodName);
            NavBaseViewBean customFormViewBean = (NavBaseViewBean) customMethodAPIBean.getView();
            List<String> fieldNames = new ArrayList<>();
            List<String> displayFieldNames = customFormViewBean.getModuleFieldNames();
            int k = displayFieldNames.indexOf(fieldname);
            if (k < displayFieldNames.size()) {
                displayFieldNames.add(k + 1, displayFieldNames.remove(k));
            }

            fieldNames.addAll(displayFieldNames);
            fieldNames.addAll(customFormViewBean.getHiddenFieldNames());

            LinkedHashSet<String> setNames = new LinkedHashSet<>();

            for (String displayName : fieldNames) {
                setNames.add(displayName);
            }

            customFormViewBean.setFieldNames(setNames);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(classConfig.getSourceConfig());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}

