package com.ds.dsm.view.config.form.field;

import com.ds.esd.custom.enums.AppendType;
import com.ds.esd.custom.field.InputFieldBean;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.*;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;

public class FieldFormBean {

    String domainId;

    String fieldname;

    String methodName;

    String sourceClassName;

    String viewClassName;

    ComponentType componentType;
    InputType type;
    String caption;
    ComboInputType inputType;

    Boolean uid;
    Boolean required;
    Boolean disabled;
    Boolean dynCheck;
    Boolean readonly;
    Boolean colHidden;
    Boolean multiLines;
    Boolean selectOnFocus;
    Boolean cover;
    Boolean controls;
    PreloadType preload;
    Boolean loop;
    Boolean muted;
    Boolean autoplay;
    Boolean dynLoad;
    Boolean closeBtn;
    Boolean barDisplay;
    Boolean advance;
    Boolean timeInput;
    Boolean hideWeekLabels;
    Boolean prepareFormData;
    Boolean noCtrlKey;
    Boolean checkBox;
    Boolean showIncreaseHandle;
    Boolean showDecreaseHandle;
    Boolean shadow;
    Boolean scaleChildren;
    Boolean selectable;
    Boolean isRange;
    Boolean connected;


    AppendType append;
    Dock dock;
    LabelPos labelPos;
    HAlignType labelHAlign;
    VAlignType labelVAlign;
    HAlignType hAlign;
    VAlignType vAlign;
    ImagePos imagePos;
    ImagePos iconPos;
    LayoutType layoutType;
    ButtonType buttonType;
    AlignType itemAlign;
    StatusItemType itemType;
    SelModeType selMode;
    AlignType align;
    BorderType borderType;
    TagCmdsAlign tagCmdsAlign;
    ItemRow itemRow;
    OverflowType overflow;


    Integer fractions;
    Integer colSpan;
    Integer rowSpan;
    Integer dropListWidth;
    Integer dropListHeight;
    Integer maxlength;
    Integer autoexpand;
    Integer volume;
    Integer firstDayOfWeek;
    Integer tabindex;
    Integer maxWidth;
    Integer maxHeight;
    Integer steps;
    Integer precision;
    Integer graphicZIndex;


    String src;
    String expression;
    String imageClass;
    String height;
    String width;
    String tipsErr;
    String tipsOK;
    String placeholder;
    String labelSize;
    String image;
    String labelGap;
    String valueFormat;
    String mask;
    String value;
    String tipsBinder;
    String html;
    String imageBgSize;
    String iconFontCode;
    String fontColor;
    String fontSize;
    String fontWeight;
    String fontFamily;
    String frameStyle;
    String cmdList;
    String cmdFilter;
    String codeType;
    String offDays;
    String dateInputFormat;
    String uploadUrl;
    String nodeName;
    String alt;
    String clock;
    String optBtn;
    String tagCmds;
    String labelCaption;
    String itemsExpression;
    String filter;
    String commandBtn;
    String captionTpl;
    String fillBG;
    String textType;
    String numberTpl;
    String itemMargin;
    String itemPadding;
    String itemWidth;
    String iframeAutoLoad;
    String poster;


    public FieldFormBean() {

    }

    public FieldFormBean(FieldFormConfig<InputFieldBean,?> config) {
        this.uid = getUid();
        this.sourceClassName = config.getSourceClassName();
        this.viewClassName = config.getViewClassName();
        this.domainId = config.getDomainId();
        this.disabled = config.getAggConfig().getDisabled();
        this.dynCheck = config.getFieldBean().getDynCheck();
        this.componentType = config.getComponentType();
        this.caption = config.getAggConfig().getCaption();
        this.fieldname = config.getFieldname();
        this.colHidden = config.getColHidden();
        this.expression = config.getAggConfig().getExpression();
        this.uid = config.getUid();

        this.readonly = config.getAggConfig().getReadonly();
        this.methodName = config.getSourceMethodName();
        this.colSpan = config.getFieldBean().getColSpan();
        this.height = config.getFieldBean().getRowHeight();
        this.imageClass = config.getAggConfig().getImageClass();
        if (imageClass == null || imageClass.equals("")) {
            if (config.getComponentType() != null) {
                imageClass = config.getComponentType().getImageClass();
            }
        }

        this.rowSpan = config.getFieldBean().getRowSpan();
        this.fieldname = config.getFieldname();

        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(config.getWidgetConfig()));

    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public void setType(InputType type) {
        this.type = type;
    }

    public ComboInputType getInputType() {
        return inputType;
    }

    public void setInputType(ComboInputType inputType) {
        this.inputType = inputType;
    }

    public Boolean getCover() {
        return cover;
    }

    public void setCover(Boolean cover) {
        this.cover = cover;
    }

    public Boolean getControls() {
        return controls;
    }

    public void setControls(Boolean controls) {
        this.controls = controls;
    }

    public PreloadType getPreload() {
        return preload;
    }

    public void setPreload(PreloadType preload) {
        this.preload = preload;
    }

    public Boolean getLoop() {
        return loop;
    }

    public void setLoop(Boolean loop) {
        this.loop = loop;
    }

    public Boolean getMuted() {
        return muted;
    }

    public void setMuted(Boolean muted) {
        this.muted = muted;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Boolean getAutoplay() {
        return autoplay;
    }

    public void setAutoplay(Boolean autoplay) {
        this.autoplay = autoplay;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public ImagePos getImagePos() {
        return imagePos;
    }

    public void setImagePos(ImagePos imagePos) {
        this.imagePos = imagePos;
    }

    public String getImageBgSize() {
        return imageBgSize;
    }

    public void setImageBgSize(String imageBgSize) {
        this.imageBgSize = imageBgSize;
    }

    public String getIconFontCode() {
        return iconFontCode;
    }

    public void setIconFontCode(String iconFontCode) {
        this.iconFontCode = iconFontCode;
    }

    public VAlignType getvAlign() {
        return vAlign;
    }

    public void setvAlign(VAlignType vAlign) {
        this.vAlign = vAlign;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public String getFontSize() {
        return fontSize;
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    public String getFontWeight() {
        return fontWeight;
    }

    public void setFontWeight(String fontWeight) {
        this.fontWeight = fontWeight;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public ButtonType getButtonType() {
        return buttonType;
    }

    public void setButtonType(ButtonType buttonType) {
        this.buttonType = buttonType;
    }

    public ImagePos getIconPos() {
        return iconPos;
    }

    public void setIconPos(ImagePos iconPos) {
        this.iconPos = iconPos;
    }

    public Boolean getDynLoad() {
        return dynLoad;
    }

    public void setDynLoad(Boolean dynLoad) {
        this.dynLoad = dynLoad;
    }

    public String getFrameStyle() {
        return frameStyle;
    }

    public void setFrameStyle(String frameStyle) {
        this.frameStyle = frameStyle;
    }

    public String getCmdList() {
        return cmdList;
    }

    public void setCmdList(String cmdList) {
        this.cmdList = cmdList;
    }

    public String getCmdFilter() {
        return cmdFilter;
    }

    public void setCmdFilter(String cmdFilter) {
        this.cmdFilter = cmdFilter;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public Boolean getCloseBtn() {
        return closeBtn;
    }

    public void setCloseBtn(Boolean closeBtn) {
        this.closeBtn = closeBtn;
    }

    public Boolean getBarDisplay() {
        return barDisplay;
    }

    public void setBarDisplay(Boolean barDisplay) {
        this.barDisplay = barDisplay;
    }

    public Boolean getAdvance() {
        return advance;
    }

    public void setAdvance(Boolean advance) {
        this.advance = advance;
    }

    public Boolean getTimeInput() {
        return timeInput;
    }

    public void setTimeInput(Boolean timeInput) {
        this.timeInput = timeInput;
    }

    public Integer getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    public void setFirstDayOfWeek(Integer firstDayOfWeek) {
        this.firstDayOfWeek = firstDayOfWeek;
    }

    public String getOffDays() {
        return offDays;
    }

    public void setOffDays(String offDays) {
        this.offDays = offDays;
    }

    public Boolean getHideWeekLabels() {
        return hideWeekLabels;
    }

    public void setHideWeekLabels(Boolean hideWeekLabels) {
        this.hideWeekLabels = hideWeekLabels;
    }

    public String getDateInputFormat() {
        return dateInputFormat;
    }

    public void setDateInputFormat(String dateInputFormat) {
        this.dateInputFormat = dateInputFormat;
    }

    public Boolean getPrepareFormData() {
        return prepareFormData;
    }

    public void setPrepareFormData(Boolean prepareFormData) {
        this.prepareFormData = prepareFormData;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public Integer getTabindex() {
        return tabindex;
    }

    public void setTabindex(Integer tabindex) {
        this.tabindex = tabindex;
    }

    public Boolean getShadow() {
        return shadow;
    }

    public void setShadow(Boolean shadow) {
        this.shadow = shadow;
    }

    public Integer getMaxWidth() {
        return maxWidth;
    }

    public void setMaxWidth(Integer maxWidth) {
        this.maxWidth = maxWidth;
    }

    public Integer getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(Integer maxHeight) {
        this.maxHeight = maxHeight;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getClock() {
        return clock;
    }

    public void setClock(String clock) {
        this.clock = clock;
    }

    public SelModeType getSelMode() {
        return selMode;
    }

    public void setSelMode(SelModeType selMode) {
        this.selMode = selMode;
    }

    public BorderType getBorderType() {
        return borderType;
    }

    public void setBorderType(BorderType borderType) {
        this.borderType = borderType;
    }

    public Boolean getNoCtrlKey() {
        return noCtrlKey;
    }

    public void setNoCtrlKey(Boolean noCtrlKey) {
        this.noCtrlKey = noCtrlKey;
    }

    public ItemRow getItemRow() {
        return itemRow;
    }

    public void setItemRow(ItemRow itemRow) {
        this.itemRow = itemRow;
    }

    public String getOptBtn() {
        return optBtn;
    }

    public void setOptBtn(String optBtn) {
        this.optBtn = optBtn;
    }

    public String getTagCmds() {
        return tagCmds;
    }

    public void setTagCmds(String tagCmds) {
        this.tagCmds = tagCmds;
    }

    public TagCmdsAlign getTagCmdsAlign() {
        return tagCmdsAlign;
    }

    public void setTagCmdsAlign(TagCmdsAlign tagCmdsAlign) {
        this.tagCmdsAlign = tagCmdsAlign;
    }

    public String getLabelCaption() {
        return labelCaption;
    }

    public void setLabelCaption(String labelCaption) {
        this.labelCaption = labelCaption;
    }

    public String getItemsExpression() {
        return itemsExpression;
    }

    public void setItemsExpression(String itemsExpression) {
        this.itemsExpression = itemsExpression;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getCommandBtn() {
        return commandBtn;
    }

    public void setCommandBtn(String commandBtn) {
        this.commandBtn = commandBtn;
    }

    public String getCaptionTpl() {
        return captionTpl;
    }

    public void setCaptionTpl(String captionTpl) {
        this.captionTpl = captionTpl;
    }

    public String getFillBG() {
        return fillBG;
    }

    public void setFillBG(String fillBG) {
        this.fillBG = fillBG;
    }

    public LayoutType getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(LayoutType layoutType) {
        this.layoutType = layoutType;
    }

    public Boolean getCheckBox() {
        return checkBox;
    }

    public void setCheckBox(Boolean checkBox) {
        this.checkBox = checkBox;
    }

    public String getTextType() {
        return textType;
    }

    public void setTextType(String textType) {
        this.textType = textType;
    }

    public Integer getSteps() {
        return steps;
    }

    public void setSteps(Integer steps) {
        this.steps = steps;
    }

    public Integer getPrecision() {
        return precision;
    }

    public void setPrecision(Integer precision) {
        this.precision = precision;
    }

    public String getNumberTpl() {
        return numberTpl;
    }

    public void setNumberTpl(String numberTpl) {
        this.numberTpl = numberTpl;
    }

    public Boolean getRange() {
        return isRange;
    }

    public void setRange(Boolean range) {
        isRange = range;
    }

    public Boolean getShowIncreaseHandle() {
        return showIncreaseHandle;
    }

    public void setShowIncreaseHandle(Boolean showIncreaseHandle) {
        this.showIncreaseHandle = showIncreaseHandle;
    }

    public Boolean getShowDecreaseHandle() {
        return showDecreaseHandle;
    }

    public void setShowDecreaseHandle(Boolean showDecreaseHandle) {
        this.showDecreaseHandle = showDecreaseHandle;
    }

    public AlignType getAlign() {
        return align;
    }

    public void setAlign(AlignType align) {
        this.align = align;
    }

    public String getItemMargin() {
        return itemMargin;
    }

    public void setItemMargin(String itemMargin) {
        this.itemMargin = itemMargin;
    }

    public String getItemPadding() {
        return itemPadding;
    }

    public void setItemPadding(String itemPadding) {
        this.itemPadding = itemPadding;
    }

    public String getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(String itemWidth) {
        this.itemWidth = itemWidth;
    }

    public AlignType getItemAlign() {
        return itemAlign;
    }

    public void setItemAlign(AlignType itemAlign) {
        this.itemAlign = itemAlign;
    }

    public StatusItemType getItemType() {
        return itemType;
    }

    public void setItemType(StatusItemType itemType) {
        this.itemType = itemType;
    }

    public Boolean getConnected() {
        return connected;
    }

    public void setConnected(Boolean connected) {
        this.connected = connected;
    }

    public String getIframeAutoLoad() {
        return iframeAutoLoad;
    }

    public void setIframeAutoLoad(String iframeAutoLoad) {
        this.iframeAutoLoad = iframeAutoLoad;
    }

    public OverflowType getOverflow() {
        return overflow;
    }

    public void setOverflow(OverflowType overflow) {
        this.overflow = overflow;
    }

    public Boolean getScaleChildren() {
        return scaleChildren;
    }

    public void setScaleChildren(Boolean scaleChildren) {
        this.scaleChildren = scaleChildren;
    }

    public Integer getGraphicZIndex() {
        return graphicZIndex;
    }

    public void setGraphicZIndex(Integer graphicZIndex) {
        this.graphicZIndex = graphicZIndex;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    public String getTipsErr() {
        return tipsErr;
    }

    public void setTipsErr(String tipsErr) {
        this.tipsErr = tipsErr;
    }

    public String getTipsOK() {
        return tipsOK;
    }

    public void setTipsOK(String tipsOK) {
        this.tipsOK = tipsOK;
    }

    public Boolean getSelectOnFocus() {
        return selectOnFocus;
    }

    public void setSelectOnFocus(Boolean selectOnFocus) {
        this.selectOnFocus = selectOnFocus;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getLabelSize() {
        return labelSize;
    }

    public void setLabelSize(String labelSize) {
        this.labelSize = labelSize;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public LabelPos getLabelPos() {
        return labelPos;
    }

    public void setLabelPos(LabelPos labelPos) {
        this.labelPos = labelPos;
    }

    public String getLabelGap() {
        return labelGap;
    }

    public void setLabelGap(String labelGap) {
        this.labelGap = labelGap;
    }

    public HAlignType getLabelHAlign() {
        return labelHAlign;
    }

    public void setLabelHAlign(HAlignType labelHAlign) {
        this.labelHAlign = labelHAlign;
    }

    public VAlignType getLabelVAlign() {
        return labelVAlign;
    }

    public void setLabelVAlign(VAlignType labelVAlign) {
        this.labelVAlign = labelVAlign;
    }

    public String getValueFormat() {
        return valueFormat;
    }

    public void setValueFormat(String valueFormat) {
        this.valueFormat = valueFormat;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public HAlignType gethAlign() {
        return hAlign;
    }

    public void sethAlign(HAlignType hAlign) {
        this.hAlign = hAlign;
    }

    public Integer getDropListWidth() {
        return dropListWidth;
    }

    public void setDropListWidth(Integer dropListWidth) {
        this.dropListWidth = dropListWidth;
    }

    public Integer getDropListHeight() {
        return dropListHeight;
    }

    public void setDropListHeight(Integer dropListHeight) {
        this.dropListHeight = dropListHeight;
    }


    public Integer getMaxlength() {
        return maxlength;
    }

    public void setMaxlength(Integer maxlength) {
        this.maxlength = maxlength;
    }

    public Boolean getMultiLines() {
        return multiLines;
    }

    public void setMultiLines(Boolean multiLines) {
        this.multiLines = multiLines;
    }

    public Integer getAutoexpand() {
        return autoexpand;
    }

    public void setAutoexpand(Integer autoexpand) {
        this.autoexpand = autoexpand;
    }

    public String getTipsBinder() {
        return tipsBinder;
    }

    public void setTipsBinder(String tipsBinder) {
        this.tipsBinder = tipsBinder;
    }

    public AppendType getAppend() {
        return append;
    }

    public void setAppend(AppendType append) {
        this.append = append;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public Integer getColSpan() {
        return colSpan;
    }

    public void setColSpan(Integer colSpan) {
        this.colSpan = colSpan;
    }

    public Integer getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(Integer rowSpan) {
        this.rowSpan = rowSpan;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public Integer getFractions() {
        return fractions;
    }

    public void setFractions(Integer fractions) {
        this.fractions = fractions;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getDynCheck() {
        return dynCheck;
    }

    public void setDynCheck(Boolean dynCheck) {
        this.dynCheck = dynCheck;
    }

    public Boolean getUid() {
        return uid;
    }

    public void setUid(Boolean uid) {
        this.uid = uid;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public InputType getType() {
        return type;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Boolean getColHidden() {
        return colHidden;
    }

    public void setColHidden(Boolean colHidden) {
        this.colHidden = colHidden;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
