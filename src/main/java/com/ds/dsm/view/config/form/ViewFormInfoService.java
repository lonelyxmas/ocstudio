package com.ds.dsm.view.config.form;

import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/form/")
public class ViewFormInfoService {


    @RequestMapping(method = RequestMethod.POST, value = "FormViewItems")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<FormNavTree>> getFormViewItems(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<FormNavTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(FormViewConfigItems.values()), FormNavTree.class);
        return result;
    }
}
