package com.ds.dsm.view.config.nav.group;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.FieldFormGridInfo;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.ESDFieldConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/group/hidden/")
public class ViewGroupHiddensService {


    @RequestMapping(method = RequestMethod.POST, value = "HiddenGroupList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "列配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<FieldFormGridInfo>> getHiddenGroupList(String sourceClassName, String sourceMethodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldFormGridInfo>> cols = new ListResultModel();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig classConfig = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = classConfig.getMethodByName(sourceMethodName);
            NavGroupViewBean customGroupViewBean = (NavGroupViewBean) methodAPIBean.getView();
            List<String> fieldNames = customGroupViewBean.getHiddenFieldNames();
            List<ESDFieldConfig> fieldFormConfigs = new ArrayList<>();
            for (String fieldname : fieldNames) {
                ESDFieldConfig fieldFormConfig = (ESDFieldConfig) customGroupViewBean.getFieldConfigMap().get(fieldname);
                if (fieldFormConfig != null) {
                    fieldFormConfigs.add(fieldFormConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fieldFormConfigs, FieldFormGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ViewGroupHiddenConfig")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<NavGroupTree>> getHiddenGridConfig(String domainId, String sourceClassName, String sourceMethodName, String viewInstId) {
        TreeListResultModel<List<NavGroupTree>> result = new TreeListResultModel<>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(sourceMethodName);
            NavGroupViewBean customGridViewBean = (NavGroupViewBean) methodAPIBean.getView();
            List<String> fieldNames = customGridViewBean.getHiddenFieldNames();
            List<ESDFieldConfig> fieldFormConfigs = new ArrayList<>();
            for (String fieldname : fieldNames) {
                ESDFieldConfig fieldFormConfig = (ESDFieldConfig) customGridViewBean.getFieldConfigMap().get(fieldname);
                if (fieldFormConfig != null) {
                    fieldFormConfigs.add(fieldFormConfig);

                }
            }
            result = TreePageUtil.getTreeList(fieldFormConfigs, NavGroupTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
