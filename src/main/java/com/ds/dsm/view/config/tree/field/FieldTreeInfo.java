package com.ds.dsm.view.config.tree.field;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.dsm.view.field.FieldTreeConfig;

@FormAnnotation(customService = {TreeFiledService.class})
public class FieldTreeInfo {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;


    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;

    @CustomAnnotation(caption = "标题字段")
    Boolean captionField;

    @CustomAnnotation(caption = "隐藏")
    Boolean hidden;

    @CustomAnnotation(caption = "主键")
    Boolean uid;

    @CustomAnnotation(caption = "父级参数")
    Boolean pid;


    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;

    public FieldTreeInfo() {

    }

    public FieldTreeInfo(FieldTreeConfig esdField) {

        this.domainId = esdField.getDomainId();
        this.hidden = esdField.getColHidden();
        this.captionField = esdField.getCaptionField();
        this.uid = esdField.getUid();
        this.pid = esdField.getPid();
        this.fieldname = esdField.getFieldname();
        this.methodName=esdField.getMethodName();

        this.viewClassName = esdField.getViewClassName();
        this.sourceClassName = esdField.getSourceClassName();


    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public Boolean getCaptionField() {
        return captionField;
    }

    public void setCaptionField(Boolean captionField) {
        this.captionField = captionField;
    }

    public Boolean getUid() {
        return uid;
    }

    public void setUid(Boolean uid) {
        this.uid = uid;
    }

    public Boolean getPid() {
        return pid;
    }

    public void setPid(Boolean pid) {
        this.pid = pid;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


}
