package com.ds.dsm.view.config.tree;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.TreeDataBaseBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(col = 2, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class TreeDataView {

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, uid = true)
    String sourceClassName;
    @CustomAnnotation(hidden = true, uid = true)
    String methodName;
    @Required
    @CustomAnnotation(caption = "窗体标题")
    String caption;

    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "保存操作（URL）")
    String saveUrl;
    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "数据装载")
    String dataUrl;
    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "编辑节点")
    String editorPath;
    @FieldAnnotation( colSpan = -1)
    @CustomAnnotation(caption = "延迟加载地址")
    String loadChildUrl;
    @FieldAnnotation( colSpan = -1, rowHeight = "150", componentType = ComponentType.CodeEditor)
    @CustomAnnotation(caption = "表达式")
    String expression;


    public TreeDataView() {

    }

    public TreeDataView(TreeDataBaseBean dataBean) {
        this.viewInstId = dataBean.getViewInstId();
        this.domainId = dataBean.getDomainId();
        this.sourceClassName = dataBean.getSourceClassName();
        this.dataUrl = dataBean.getDataUrl();
        this.saveUrl = dataBean.getSaveUrl();
        this.editorPath = dataBean.getEditorPath();
        this.expression = dataBean.getExpression();
        this.loadChildUrl = dataBean.getLoadChildUrl();


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getSaveUrl() {
        return saveUrl;
    }

    public void setSaveUrl(String saveUrl) {
        this.saveUrl = saveUrl;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDataUrl() {
        return dataUrl;
    }

    public void setDataUrl(String dataUrl) {
        this.dataUrl = dataUrl;
    }

    public String getEditorPath() {
        return editorPath;
    }

    public void setEditorPath(String editorPath) {
        this.editorPath = editorPath;
    }

    public String getLoadChildUrl() {
        return loadChildUrl;
    }

    public void setLoadChildUrl(String loadChildUrl) {
        this.loadChildUrl = loadChildUrl;
    }
}
