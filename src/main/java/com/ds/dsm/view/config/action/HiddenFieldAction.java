package com.ds.dsm.view.config.action;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/view/config/field/action/")
public class HiddenFieldAction {


    @RequestMapping(method = RequestMethod.POST, value = "showField")
    @CustomAnnotation(imageClass = "spafont spa-icon-c-hiddeninput", index = 3, caption = "取消隐藏")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> showFormField(String sourceClassName, String methodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ViewEntityConfig classConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = classConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomViewBean customViewBean = (CustomViewBean) methodAPIBean.getView();
            FieldFormConfig fieldFormConfig = (FieldFormConfig) customViewBean.getFieldConfigMap().get(fieldname);
            fieldFormConfig.getAggConfig().setColHidden(false);
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(customESDClassAPIBean);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


}

