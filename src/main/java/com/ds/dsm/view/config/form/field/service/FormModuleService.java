package com.ds.dsm.view.config.form.field.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.form.field.combo.ComboModuleView;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.CustomView;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.field.combo.ComboModuleFieldBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/module/")
@MethodChinaName(cname = "模块配置", imageClass = "spafont spa-icon-c-comboinput")

public class FormModuleService {


    @RequestMapping(method = RequestMethod.POST, value = "FieldModuleInfo")
    @FormViewAnnotation
    @ModuleAnnotation(imageClass = "xui-icon-dialog", caption = "基础信息", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.treeNodeEditor})
    @ResponseBody
    public ResultModel<ComboModuleView> getFieldModuleInfo(String sourceClassName, String sourceMethodName, String fieldname, String domainId, String viewInstId) {
        ResultModel<ComboModuleView> result = new ResultModel<ComboModuleView>();
        try {
            ApiClassConfig classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(sourceMethodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);

            result.setData(new ComboModuleView(formInfo));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadChildModule")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> loadChildModule(String viewInstId, String domainId, String sourceClassName, String fieldname, String methodName) {
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<List<ViewConfigTree>>();
        ApiClassConfig classConfig = null;
        try {
            classConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig customMethodAPIBean = classConfig.getMethodByName(methodName);
            CustomView viewBean = customMethodAPIBean.getView();
            FieldFormConfig formInfo = (FieldFormConfig) viewBean.getFieldByName(fieldname);
            MethodConfig fieldMethodConfig = null;
            ComboModuleFieldBean fieldBean = (ComboModuleFieldBean) formInfo.getWidgetConfig();
            if (fieldBean.getSrc() != null && fieldBean.getSrc().equals("")) {
                fieldMethodConfig = classConfig.getMethodByName(fieldBean.getSrc());
            } else if (fieldBean.getBindClass() != null && !fieldBean.getBindClass().equals(Void.class)) {
                ApiClassConfig bindConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(fieldBean.getBindClass().getName(), domainId);
                fieldMethodConfig = bindConfig.getMethodByItem(CustomMenuItem.index);
            } else {
                ApiClassConfig bindConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(formInfo.getViewClassName(), domainId);
                fieldMethodConfig = bindConfig.getMethodByName(formInfo.getMethodName());
            }
            List<Object> obj = new ArrayList<>();
            obj.add(fieldMethodConfig);
            if (fieldMethodConfig.getView() != null) {
                obj.add(fieldMethodConfig.getView());
            }
            resultModel = TreePageUtil.getTreeList(obj, ViewConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }


        return resultModel;
    }


}
