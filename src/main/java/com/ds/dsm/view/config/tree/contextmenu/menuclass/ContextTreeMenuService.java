package com.ds.dsm.view.config.tree.contextmenu.menuclass;

import com.ds.common.JDSException;
import com.ds.common.util.ClassUtility;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.menu.tree.AggMenuConfigTree;
import com.ds.dsm.aggregation.config.menu.tree.AggMenuMainNavItem;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.RightContextMenuBean;
import com.ds.esd.custom.tree.ChildTreeViewBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/tree/contextmenu/")
@MethodChinaName(cname = "右键菜单自定义按钮", imageClass = "spafont spa-icon-c-databinder")
public class ContextTreeMenuService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "导入菜单动作")
    @RequestMapping(value = {"ContextMenuTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "导入菜单动作", dynLoad = true, dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<AllContextMenuTree>> getContextMenuTree(String domainId, String sourceClassName, String childViewId, String methodName) {
        TreeListResultModel<List<AllContextMenuTree>> result = new TreeListResultModel<List<AllContextMenuTree>>();
        try {
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            List<String> ids = Arrays.asList(bean.getAggMenuNames().toArray(new String[]{}));
            result = TreePageUtil.getTreeList(Arrays.asList(CustomMenuType.values()), AllContextMenuTree.class, ids);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "保存菜单")
    @RequestMapping(value = {"saveContextMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveContextMenu(String domainId, String sourceClassName, String methodName, String childViewId, String ContextMenuTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (ContextMenuTree != null) {
            try {
                ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
                MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);

                String[] esdClassNames = StringUtility.split(ContextMenuTree, ";");
                List<Class> menus = new ArrayList<>();
                for (String esdClassName : esdClassNames) {
                    try {
                        menus.add(ClassUtility.loadClass(esdClassName));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                RightContextMenuBean barMenuBean = null;
                ChildTreeViewBean childTreeViewBean = null;
                if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                    NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                    CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                    childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                    CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                    childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                }

                barMenuBean = childTreeViewBean.getContextMenuBean();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(RightContextMenu.class, new RightContextMenuBean());
                }
                barMenuBean.setMenuClass(menus.toArray(new Class[]{}));
                childTreeViewBean.setContextMenuBean(barMenuBean);

                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除实体关系")
    @RequestMapping(value = {"delContextMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delContextMenu(String domainId, String sourceClassName, String methodName, String childViewId, String menuClass) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (domainId != null) {
            try {
                String[] esdClassNames = StringUtility.split(menuClass, ";");
                List<Class> menus = new ArrayList<>();
                for (String esdClassName : esdClassNames) {
                    try {
                        menus.add(ClassUtility.loadClass(esdClassName));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
                MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);

                RightContextMenuBean barMenuBean = null;
                ChildTreeViewBean childTreeViewBean = null;
                if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                    NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                    CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                    childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                    CustomTreeViewBean customTreeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                    childTreeViewBean = customTreeViewBean.getChildTreeBean(childViewId);
                }
                barMenuBean = childTreeViewBean.getContextMenuBean();
                Class[] menuClassList = barMenuBean.getMenuClass();
                List<Class> classList = new ArrayList<>();
                classList.addAll(Arrays.asList(menuClassList));
                for (Class clazz : menus) {
                    classList.remove(clazz);
                }

                barMenuBean.setMenuClass(classList.toArray(new Class[]{}));
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @RequestMapping(method = RequestMethod.POST, value = "AggConfigTree")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "900", height = "680")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "菜单配置")
    @ResponseBody
    public TreeListResultModel<List<AggMenuConfigTree>> getAggConfigTree(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggMenuConfigTree>> resultModel = new TreeListResultModel<List<AggMenuConfigTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(AggMenuMainNavItem.values()), AggMenuConfigTree.class);
        return resultModel;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
