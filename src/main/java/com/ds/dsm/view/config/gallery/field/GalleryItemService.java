package com.ds.dsm.view.config.gallery.field;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.grid.field.FieldGridConfigView;
import com.ds.dsm.view.config.grid.field.FieldGridInfo;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.grid.GridColItemBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.jds.core.esb.util.OgnlUtil;
import com.ds.web.json.JSONData;
import com.ds.web.util.PageUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/gallery/item/")
@MethodChinaName(cname = "字段配置", imageClass = "spafont spa-icon-c-comboinput")

public class GalleryItemService {


    @RequestMapping(method = RequestMethod.POST, value = "FieldGalleryList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "可见列")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<FieldGalleryInfo>> getFieldGridList(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldGalleryInfo>> cols = new ListResultModel();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            List<FieldGalleryInfo> fieldGridConfigs = customGridViewBean.getCustomFields();
            List<FieldGalleryInfo> fields = new ArrayList<>();
            for (FieldGalleryInfo fieldGalleryInfo : fieldGridConfigs) {
                if (fieldGalleryInfo != null && !fieldGalleryInfo.getColHidden()) {

                    fields.add(fieldGalleryInfo);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldGalleryInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @MethodChinaName(cname = "编辑字段配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFieldGrid")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateFieldForm(@RequestBody FieldGridConfigView config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getSourceClassName();
            if (className != null && !className.equals("")) {
                ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(className, config.getDomainId());
                MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(config.getMethodName());
                CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                FieldGridConfig fieldConfig = (FieldGridConfig) customGridViewBean.getFieldConfigMap().get(config.getFieldname());
                Map<String, Object> configMap = BeanMap.create(config);
                OgnlUtil.setProperties(configMap, fieldConfig, false, false);
                OgnlUtil.setProperties(configMap, fieldConfig.getGridColItemBean(), false, false);
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(customESDClassAPIBean);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FieldNav")
    @FormViewAnnotation
    @ModuleAnnotation(caption = "字段信息")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor, CustomMenuItem.add}, autoRun = true)
    @ResponseBody
    public ResultModel<FieldGridConfigView> getFieldNav(String sourceClassName, String methodName, String domainId, String viewInstId, String fieldname) {
        ResultModel<FieldGridConfigView> result = new ResultModel<FieldGridConfigView>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            FieldGridConfig fieldConfig = (FieldGridConfig) customGridViewBean.getFieldConfigMap().get(fieldname);
            fieldConfig.setSourceClassName(sourceClassName);
            fieldConfig.setEntityClassName(customGridViewBean.getViewClassName());
            result.setData(new FieldGridConfigView(fieldConfig));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "批量保存")
    @RequestMapping(method = RequestMethod.POST, value = "updateAllFieldGrid")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.saveAllRow)
    public @ResponseBody
    ResultModel<Boolean> updateAllFieldGrid(@JSONData List<FieldGridInfo> rows, String sourceClassName, String domainId, String methodName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            CustomGridViewBean gridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            for (FieldGridInfo config : rows) {
                FieldGridConfig fieldConfig = (FieldGridConfig) gridViewBean.getFieldConfigMap().get(config.getFieldname());
                GridColItemBean gridItemBean = fieldConfig.getGridColItemBean();
                if (gridItemBean == null) {
                    gridItemBean = new GridColItemBean();
                }
                gridItemBean.setFlexSize(config.getFlexSize());
                gridItemBean.setColResizer(config.getColResizer());
                gridItemBean.setWidth(config.getWidth());
                gridItemBean.setEditable(config.getEditable());
                gridItemBean.setInputType(config.getInputType());
                gridItemBean.setTitle(config.getTitle());
                fieldConfig.setGridColItemBean(gridItemBean);
            }
            DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(customESDClassAPIBean);

        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
