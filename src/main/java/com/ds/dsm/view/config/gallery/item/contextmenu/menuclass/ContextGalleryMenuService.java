package com.ds.dsm.view.config.gallery.item.contextmenu.menuclass;

import com.ds.common.JDSException;
import com.ds.common.util.ClassUtility;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.menu.tree.AggMenuConfigTree;
import com.ds.dsm.aggregation.config.menu.tree.AggMenuMainNavItem;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.RightContextMenuBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/gallery/contextmenu/")
@MethodChinaName(cname = "画廊右键菜单", imageClass = "spafont spa-icon-c-gallery")
public class ContextGalleryMenuService {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @MethodChinaName(cname = "导入右键菜单动作")
    @RequestMapping(value = {"GalleryMenuTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "导入菜单动作", dynLoad = true, dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<AllGalleryContextMenuTree>> getCellMenuTree(String domainId, String sourceClassName, String methodName) {
        TreeListResultModel<List<AllGalleryContextMenuTree>> result = new TreeListResultModel<List<AllGalleryContextMenuTree>>();
        try {
            DomainInst bean = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            List<String> ids = Arrays.asList(bean.getAggMenuNames().toArray(new String[]{}));
            result = TreePageUtil.getTreeList(Arrays.asList(CustomMenuType.values()), AllGalleryContextMenuTree.class, ids);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "保存右键菜单")
    @RequestMapping(value = {"saveGalleryMenuClass"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveGalleryMenuClass(String domainId, String sourceClassName, String methodName, String GalleryMenuTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (GalleryMenuTree != null) {
            try {
                ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
                MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
                String[] esdClassNames = StringUtility.split(GalleryMenuTree, ";");
                List<Class> menus = new ArrayList<>();
                for (String esdClassName : esdClassNames) {
                    try {
                        menus.add(ClassUtility.loadClass(esdClassName));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                CustomGalleryViewBean galleryViewBean=null;

                if (methodAPIBean.getView() instanceof NavGalleryViewBean){
                    galleryViewBean=((NavGalleryViewBean)methodAPIBean.getView()).getGalleryViewBean();
                }else if(methodAPIBean.getView() instanceof NavGalleryViewBean){
                    galleryViewBean= (CustomGalleryViewBean) methodAPIBean.getView();
                }

                RightContextMenuBean barMenuBean = galleryViewBean.getContextMenuBean();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(RightContextMenu.class, new RightContextMenuBean());
                }

                barMenuBean.setMenuClass(menus.toArray(new Class[]{}));
                galleryViewBean.setContextMenuBean(barMenuBean);

                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除右键菜单")
    @RequestMapping(value = {"delGalleryMenu"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delContextMenu(String domainId, String sourceClassName, String methodName, String menuClass) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (domainId != null) {
            try {
                String[] esdClassNames = StringUtility.split(menuClass, ";");
                List<Class> menus = new ArrayList<>();
                for (String esdClassName : esdClassNames) {
                    try {
                        menus.add(ClassUtility.loadClass(esdClassName));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
                MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
                CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();

                RightContextMenuBean barMenuBean = customGalleryViewBean.getContextMenuBean();
                if (barMenuBean == null) {
                    barMenuBean = AnnotationUtil.fillDefaultValue(RightContextMenu.class, new RightContextMenuBean());
                }
                Class[] menuClassList = barMenuBean.getMenuClass();
                List<Class> classList = new ArrayList<>();
                classList.addAll(Arrays.asList(menuClassList));
                for (Class clazz : menus) {
                    classList.remove(clazz);
                }

                barMenuBean.setMenuClass(classList.toArray(new Class[]{}));
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @RequestMapping(method = RequestMethod.POST, value = "AggConfigTree")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "900", height = "680")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "菜单配置")
    @ResponseBody
    public TreeListResultModel<List<AggMenuConfigTree>> getAggConfigTree(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggMenuConfigTree>> resultModel = new TreeListResultModel<List<AggMenuConfigTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(AggMenuMainNavItem.values()), AggMenuConfigTree.class);
        return resultModel;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
