package com.ds.dsm.view.config.form.field.custom;

import com.ds.dsm.view.config.form.field.service.FieldLayoutService;
import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.ComboNumberAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.FieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.ComboInputType;

@BottomBarMenu
@FormAnnotation(col = 2, customService = FieldLayoutService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FieldLayoutView {

    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(hidden = true, uid = true)
    String fieldname;
    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;


    @CustomAnnotation(caption = "高度")
    String rowHeight;

    @CustomAnnotation(caption = "宽度")
    String colWidth;


    @CustomAnnotation(caption = "是否必填")
    Boolean required;

    @CustomAnnotation(caption = "脏读标志")
    Boolean dirtyMark;

    @CustomAnnotation(caption = "动态检查")
    Boolean dynCheck;

    @CustomAnnotation(caption = "是否显示标签")
    Boolean haslabel;


    @ComboNumberAnnotation(increment = "1", precision = 0)
    @ComboInputAnnotation(inputType = ComboInputType.spin)
    @CustomAnnotation(caption = "列合并")
    Integer colSpan;

    @ComboNumberAnnotation(increment = "1", precision = 0)
    @ComboInputAnnotation(inputType = ComboInputType.spin)
    @CustomAnnotation(caption = "行合并")
    Integer rowSpan;


    public FieldLayoutView() {

    }

    public FieldLayoutView(FieldFormConfig config) {
        FieldBean fieldBean = config.getFieldBean();
        this.viewClassName = config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.methodName = config.getSourceMethodName();
        this.fieldname = config.getFieldname();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();

        this.colSpan = fieldBean.getColSpan();
        this.rowSpan = fieldBean.getRowSpan();
        this.required = fieldBean.getRequired();
        this.rowHeight = fieldBean.getRowHeight();
        this.colWidth = fieldBean.getColWidth();
        this.rowSpan = fieldBean.getRowSpan();
        this.haslabel = fieldBean.getHaslabel();
        this.dynCheck = fieldBean.getDynCheck();
        this.dirtyMark = fieldBean.getDirtyMark();
        this.required = fieldBean.getRequired();

    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Boolean getDirtyMark() {
        return dirtyMark;
    }

    public void setDirtyMark(Boolean dirtyMark) {
        this.dirtyMark = dirtyMark;
    }

    public Boolean getDynCheck() {
        return dynCheck;
    }

    public void setDynCheck(Boolean dynCheck) {
        this.dynCheck = dynCheck;
    }

    public Boolean getHaslabel() {
        return haslabel;
    }

    public void setHaslabel(Boolean haslabel) {
        this.haslabel = haslabel;
    }


    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getRowHeight() {
        return rowHeight;
    }

    public void setRowHeight(String rowHeight) {
        this.rowHeight = rowHeight;
    }

    public String getColWidth() {
        return colWidth;
    }

    public void setColWidth(String colWidth) {
        this.colWidth = colWidth;
    }


    public Integer getColSpan() {
        return colSpan;
    }

    public void setColSpan(Integer colSpan) {
        this.colSpan = colSpan;
    }

    public Integer getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(Integer rowSpan) {
        this.rowSpan = rowSpan;
    }


    public void setCommandBtn(String commandBtn) {
        commandBtn = commandBtn;
    }

}
