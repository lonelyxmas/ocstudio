package com.ds.dsm.view.config.form.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.dsm.view.config.form.field.FieldFormBean;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;

import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/view/config/form/field/combo/")
@MethodChinaName(cname = "清空配置信息", imageClass = "spafont spa-icon-c-comboinput")

public class FieldComboBoxService {


    @MethodChinaName(cname = "清空配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "clearFieldForm")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> clearFieldForm(String serviceClassName, String methodName, String fieldname,String domainId, String viewInstId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            if (serviceClassName != null && !serviceClassName.equals("")) {
                ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(serviceClassName,viewInstId);
                MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(methodName);
                CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                formViewBean.getFieldConfigMap().remove(fieldname);
                DSMFactory.getInstance().getViewManager().reSetViewEntityConfig(serviceClassName,domainId, viewInstId);
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }


    @MethodChinaName(cname = "编辑字段配置信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateFieldForm")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateFieldForm(@RequestBody FieldFormBean config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String className = config.getSourceClassName();
            if (className != null && !className.equals("")) {
                ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(className, config.getDomainId());
                MethodConfig methodAPIBean = tableConfig.getSourceConfig().getMethodByName(config.getMethodName());
                CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                FieldFormConfig fieldConfig = (FieldFormConfig) formViewBean.getFieldConfigMap().get(config.getFieldname());
                Map<String, Object> configMap = BeanMap.create(fieldConfig);
                configMap.putAll(BeanMap.create(config));
                Map<String, Object> widgetMap = BeanMap.create(fieldConfig.getWidgetConfig());
                widgetMap.putAll(BeanMap.create(config));
                DSMFactory.getInstance().getAggregationManager().updateApiClassConfig(tableConfig.getSourceConfig());
            }
        } catch (JDSException e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


}
