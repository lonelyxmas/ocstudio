package com.ds.dsm.view.config.grid;

import com.ds.dsm.view.config.action.CustomBuildAction;
import com.ds.dsm.view.config.service.GridConfigService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.grid.CustomGridViewBean;


@BottomBarMenu(menuClass = CustomBuildAction.class)
@FormAnnotation(col = 2,
        bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet}, customService = GridBaseInfoService.class)
public class GridBaseView {


    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;

    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    String viewClassName;

    @CustomAnnotation(hidden = true, uid = true)
    String methodName;

    @FieldAnnotation(rowHeight = "50")
    @CustomAnnotation(caption = "行高")
    String rowHeight;


    @CustomAnnotation(caption = "显示表头")
    Boolean showHeader;

    @CustomAnnotation(caption = "分色显示")
    Boolean altRowsBg;

    @CustomAnnotation(caption = "列可排序")
    Boolean colSortable;

    @CustomAnnotation(caption = "是否可编辑")
    Boolean editable;
    @CustomAnnotation(caption = "可否展开")
    Boolean iniFold;
    @CustomAnnotation(caption = "展开动画")
    Boolean animCollapse;
    @CustomAnnotation(caption = "可变行高")
    Boolean rowResizer;
    @CustomAnnotation(caption = "可隐藏列")
    Boolean colHidable;
    @CustomAnnotation(caption = "列大小")
    Boolean colResizer;
    @CustomAnnotation(caption = "列可移动")
    Boolean colMovable;
    @CustomAnnotation(caption = "启用CtrlKey")
    Boolean noCtrlKey;
    @CustomAnnotation(caption = "解冻列")
    Integer freezedColumn;
    @CustomAnnotation(caption = "解冻行")
    Integer freezedRow;
    @CustomAnnotation(caption = "值分隔符")
    String valueSeparator;
    @CustomAnnotation(caption = "uidColumn")
    String uidColumn;
    @CustomAnnotation(caption = "货币模版")
    String currencyTpl;
    @CustomAnnotation(caption = "数字模版")
    String numberTpl;


    public GridBaseView() {

    }

    public GridBaseView(CustomGridViewBean gridConfig) {
        if (gridConfig == null) {
            gridConfig = new CustomGridViewBean();

        }
        this.methodName = gridConfig.getMethodName();
        this.viewClassName = gridConfig.getViewClassName();
        this.sourceClassName = gridConfig.getSourceClassName();
        this.viewInstId = gridConfig.getViewInstId();
        this.domainId = gridConfig.getDomainId();
        this.showHeader = gridConfig.getShowHeader();
        this.rowHeight = gridConfig.getRowHeight();
        this.altRowsBg = gridConfig.getAltRowsBg();
        this.altRowsBg = gridConfig.getAltRowsBg();
        this.rowHeight = gridConfig.getRowHeight();
        this.colSortable = gridConfig.getColSortable();
        this.showHeader = gridConfig.getShowHeader();
        this.editable = gridConfig.getEditable();
        this.iniFold = gridConfig.getIniFold();
        this.animCollapse = gridConfig.getAnimCollapse();
        this.rowResizer = gridConfig.getRowResizer();
        this.colHidable = gridConfig.getColHidable();
        this.colMovable = gridConfig.getColMovable();
        this.noCtrlKey = gridConfig.getNoCtrlKey();
        this.freezedColumn = gridConfig.getFreezedColumn();
        this.freezedRow = gridConfig.getFreezedRow();
        this.showHeader = gridConfig.getShowHeader();
        this.editable = gridConfig.getEditable();
        this.uidColumn = gridConfig.getUidColumn();
        this.valueSeparator = gridConfig.getValueSeparator();
        this.currencyTpl = gridConfig.getCurrencyTpl();
        this.numberTpl = gridConfig.getNumberTpl();


    }

    public String getUidColumn() {
        return uidColumn;
    }

    public void setUidColumn(String uidColumn) {
        this.uidColumn = uidColumn;
    }

    public Boolean getColSortable() {
        return colSortable;
    }

    public void setColSortable(Boolean colSortable) {
        this.colSortable = colSortable;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Boolean getIniFold() {
        return iniFold;
    }

    public void setIniFold(Boolean iniFold) {
        this.iniFold = iniFold;
    }

    public Boolean getAnimCollapse() {
        return animCollapse;
    }

    public void setAnimCollapse(Boolean animCollapse) {
        this.animCollapse = animCollapse;
    }

    public Boolean getRowResizer() {
        return rowResizer;
    }

    public void setRowResizer(Boolean rowResizer) {
        this.rowResizer = rowResizer;
    }

    public Boolean getColHidable() {
        return colHidable;
    }

    public void setColHidable(Boolean colHidable) {
        this.colHidable = colHidable;
    }

    public Boolean getColResizer() {
        return colResizer;
    }

    public void setColResizer(Boolean colResizer) {
        this.colResizer = colResizer;
    }

    public Boolean getColMovable() {
        return colMovable;
    }

    public void setColMovable(Boolean colMovable) {
        this.colMovable = colMovable;
    }

    public Boolean getNoCtrlKey() {
        return noCtrlKey;
    }

    public void setNoCtrlKey(Boolean noCtrlKey) {
        this.noCtrlKey = noCtrlKey;
    }

    public Integer getFreezedColumn() {
        return freezedColumn;
    }

    public void setFreezedColumn(Integer freezedColumn) {
        this.freezedColumn = freezedColumn;
    }

    public Integer getFreezedRow() {
        return freezedRow;
    }

    public void setFreezedRow(Integer freezedRow) {
        this.freezedRow = freezedRow;
    }

    public String getValueSeparator() {
        return valueSeparator;
    }

    public void setValueSeparator(String valueSeparator) {
        this.valueSeparator = valueSeparator;
    }

    public String getCurrencyTpl() {
        return currencyTpl;
    }

    public void setCurrencyTpl(String currencyTpl) {
        this.currencyTpl = currencyTpl;
    }

    public String getNumberTpl() {
        return numberTpl;
    }

    public void setNumberTpl(String numberTpl) {
        this.numberTpl = numberTpl;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getRowHeight() {
        return rowHeight;
    }

    public void setRowHeight(String rowHeight) {
        this.rowHeight = rowHeight;
    }

    public Boolean getAltRowsBg() {
        return altRowsBg;
    }

    public void setAltRowsBg(Boolean altRowsBg) {
        this.altRowsBg = altRowsBg;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public Boolean getShowHeader() {
        return showHeader;
    }

    public void setShowHeader(Boolean showHeader) {
        this.showHeader = showHeader;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

}
