package com.ds.dsm.view.config.menu.toolbar;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.manager.view.BuildViewMenu;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.NavBaseViewBean;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.bean.nav.tab.NavTabsViewBean;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.gallery.CustomGalleryViewBean;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.ToolBarMenuBean;
import com.ds.esd.custom.tree.CustomTreeViewBean;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.AnnotationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@BottomBarMenu(menuClass = BuildViewMenu.class)
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save}, customService = {ToolBarConfigService.class})
@RequestMapping("/dsm/view/config/menu/toolbar/")
public class ToolBarNav {

    @CustomAnnotation(hidden = true, pid = true)
    String domainId;
    @CustomAnnotation(hidden = true, pid = true)
    String viewInstId;
    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;
    @CustomAnnotation(hidden = true, uid = true)
    public String sourceClassName;

    public ToolBarNav() {

    }

    @MethodChinaName(cname = "基础信息")
    @RequestMapping(method = RequestMethod.POST, value = "ToolBarConfig")
    @FormViewAnnotation
    @UIAnnotation(dock = Dock.top, height = "220")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<ToolBarConfigView> getToolBarConfig(String domainId, String sourceClassName, String entityClassName, String methodName) {
        ResultModel<ToolBarConfigView> result = new ResultModel<ToolBarConfigView>();
        try {
            ApiClassConfig tableConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = tableConfig.getMethodByName(methodName);
            ToolBarMenuBean barMenuBean = null;
            if (methodAPIBean.getView() instanceof NavBaseViewBean) {
                NavBaseViewBean viewBean = (NavBaseViewBean) methodAPIBean.getView();
                NavBaseViewBean baseViewBean = (NavBaseViewBean) methodAPIBean.getView();
                CustomTreeViewBean customTreeViewBean = baseViewBean.getTreeView();
                if (customTreeViewBean != null) {
                    barMenuBean = customTreeViewBean.getToolBar();
                } else {
                    barMenuBean = viewBean.getToolBar();
                }
            } else if (methodAPIBean.getView() instanceof CustomTreeViewBean) {
                CustomTreeViewBean treeViewBean = (CustomTreeViewBean) methodAPIBean.getView();
                barMenuBean = treeViewBean.getToolBar();
            } else if (methodAPIBean.getView() instanceof CustomFormViewBean) {
                CustomFormViewBean formViewBean = (CustomFormViewBean) methodAPIBean.getView();
                barMenuBean = formViewBean.getToolBar();
            } else if (methodAPIBean.getView() instanceof CustomGridViewBean) {
                CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
                barMenuBean = customGridViewBean.getToolBar();
            }else  if (methodAPIBean.getView() instanceof NavGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = ((NavGalleryViewBean) methodAPIBean.getView()).getGalleryViewBean();

                barMenuBean = customGalleryViewBean.getToolBar();
            } else if (methodAPIBean.getView() instanceof CustomGalleryViewBean) {
                CustomGalleryViewBean customGalleryViewBean = (CustomGalleryViewBean) methodAPIBean.getView();
                barMenuBean = customGalleryViewBean.getToolBar();
            } else if (methodAPIBean.getView() instanceof NavTabsViewBean) {
                NavTabsViewBean navTabsViewBean = (NavTabsViewBean) methodAPIBean.getView();
                barMenuBean = navTabsViewBean.getToolBar();
            }

            if (barMenuBean == null) {
                barMenuBean = AnnotationUtil.fillDefaultValue(ToolBarMenu.class, new ToolBarMenuBean());
            }
            ToolBarConfigView menuConfigView = new ToolBarConfigView(barMenuBean, domainId, sourceClassName, methodName);
            result.setData(menuConfigView);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;

    }


    @MethodChinaName(cname = "详细信息")
    @RequestMapping(method = RequestMethod.POST, value = "ToolBarMetaView")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill)
    @ResponseBody
    public ResultModel<ToolBarMetaView> getToolBarMetaView(String sourceClassName, String methodName, String viewInstId, String domainId) {
        ResultModel<ToolBarMetaView> result = new ResultModel<ToolBarMetaView>();
        return result;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}

