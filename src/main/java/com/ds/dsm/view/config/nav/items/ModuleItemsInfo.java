package com.ds.dsm.view.config.nav.items;

import com.ds.common.util.StringUtility;
import com.ds.dsm.view.config.action.FieldAction;
import com.ds.dsm.view.config.form.service.FormFieldService;
import com.ds.dsm.view.config.nav.action.NavModuleItemColAction;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;

@PageBar(pageCount = 100)
@RowHead(selMode = SelModeType.none, gridHandlerCaption = "排序|隐藏", rowHandlerWidth = "8em", rowNumbered = false)
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left,menuClass = {NavModuleItemColAction.class})
@GridAnnotation(   customService = {FormFieldService.class}, customMenu = {GridMenu.Reload})
public class ModuleItemsInfo {

    @CustomAnnotation(pid = true, hidden = true)
    String domainId;

    @CustomAnnotation(pid = true, hidden = true)
    String methodName;

    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    private String fieldname;

    @CustomAnnotation(caption = "中文名", readonly = true)
    private String caption;

    @CustomAnnotation(caption = "URL")
    private String url;


    @CustomAnnotation(caption = "类名", pid = true, hidden = true)
    private String moduleName;

    @CustomAnnotation(hidden = true, pid = true)
    String sourceClassName;

    @CustomAnnotation(hidden = true, pid = true)
    String viewClassName;




    public ModuleItemsInfo(FieldModuleConfig esdField) {

        this.domainId = esdField.getDomainId();
        this.methodName = esdField.getMethodName();
        this.caption = esdField.getCaption();
        this.fieldname = esdField.getFieldname();
        this.sourceClassName = esdField.getSourceClassName();
        this.viewClassName=esdField.getViewClassName();
        if (caption == null || caption.equals("")) {
            caption = fieldname;
        }

        this.url = esdField.getUrl();
        String url = esdField.getUrl();
        if (url.startsWith("/")) {
            url = url.substring(1);
        }
        this.moduleName = StringUtility.replace(url, "/", ".");

    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }



    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }
}
