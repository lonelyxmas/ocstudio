package com.ds.dsm.view.config.form.field;

import com.ds.dsm.view.config.form.field.service.FormSliderService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.FileUploadFieldBean;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.tool.ui.enums.LabelPos;
import com.ds.esd.tool.ui.enums.LayoutType;
import com.ds.esd.tool.ui.enums.VAlignType;
import net.sf.cglib.beans.BeanMap;

import java.util.Map;
@BottomBarMenu
@FormAnnotation(col = 2, customService =FormSliderService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.ReSet})
public class FieldSliderView {


    @CustomAnnotation(pid = true, hidden = true)
    String viewInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String domainId;
    @CustomAnnotation(caption = "字段名", readonly = true, uid = true)
    String fieldname;

    @CustomAnnotation(hidden = true, pid = true)
    public String sourceClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String viewClassName;
    @CustomAnnotation(hidden = true, pid = true)
    public String methodName;


    @CustomAnnotation(caption = "宽度")
    String width;
    @CustomAnnotation(caption = "高度")
    String height;
    @CustomAnnotation(caption = "精度")
    Integer precision;
    @CustomAnnotation(caption = "数字模板")
    String numberTpl;
    @CustomAnnotation(caption = "步进值")
    Integer steps;

    @CustomAnnotation(caption = "默认值")
    String value;
    @CustomAnnotation(caption = "布局")
    LayoutType layoutType;
    @CustomAnnotation(caption = "是否环绕")
    Boolean isRange;
    @CustomAnnotation(caption = "显示增加按钮")
    Boolean showIncreaseHandle;
    @CustomAnnotation(caption = "显示减少按钮")
    Boolean showDecreaseHandle;
    @CustomAnnotation(caption = "标签大小")
    Integer labelSize;
    @CustomAnnotation(caption = "标签位置")
    LabelPos labelPos;
    @CustomAnnotation(caption = "标签间隙")
    Integer labelGap;
    @CustomAnnotation(caption = "标签名称")
    String labelCaption;
    @CustomAnnotation(caption = "垂直对齐")
    HAlignType labelHAlign;
    @CustomAnnotation(caption = "左右对齐")
    VAlignType labelVAlign;


    public FieldSliderView() {

    }

    public FieldSliderView(FieldFormConfig<FileUploadFieldBean,?> config) {
        FileUploadFieldBean inputFieldBean = config.getWidgetConfig();
        Map<String, Object> configMap = BeanMap.create(this);
        configMap.putAll(BeanMap.create(inputFieldBean));
        this.methodName = config.getSourceMethodName();
        this.viewClassName= config.getViewClassName();
        this.sourceClassName = config.getSourceClassName();
        this.viewInstId = config.getDomainId();
        this.domainId = config.getDomainId();
        this.fieldname = config.getFieldname();
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Integer getPrecision() {
        return precision;
    }

    public void setPrecision(Integer precision) {
        this.precision = precision;
    }

    public String getNumberTpl() {
        return numberTpl;
    }

    public void setNumberTpl(String numberTpl) {
        this.numberTpl = numberTpl;
    }

    public Integer getSteps() {
        return steps;
    }

    public void setSteps(Integer steps) {
        this.steps = steps;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LayoutType getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(LayoutType layoutType) {
        this.layoutType = layoutType;
    }

    public Boolean getRange() {
        return isRange;
    }

    public void setRange(Boolean range) {
        isRange = range;
    }

    public Boolean getShowIncreaseHandle() {
        return showIncreaseHandle;
    }

    public void setShowIncreaseHandle(Boolean showIncreaseHandle) {
        this.showIncreaseHandle = showIncreaseHandle;
    }

    public Boolean getShowDecreaseHandle() {
        return showDecreaseHandle;
    }

    public void setShowDecreaseHandle(Boolean showDecreaseHandle) {
        this.showDecreaseHandle = showDecreaseHandle;
    }

    public Integer getLabelSize() {
        return labelSize;
    }

    public void setLabelSize(Integer labelSize) {
        this.labelSize = labelSize;
    }

    public LabelPos getLabelPos() {
        return labelPos;
    }

    public void setLabelPos(LabelPos labelPos) {
        this.labelPos = labelPos;
    }

    public Integer getLabelGap() {
        return labelGap;
    }

    public void setLabelGap(Integer labelGap) {
        this.labelGap = labelGap;
    }

    public String getLabelCaption() {
        return labelCaption;
    }

    public void setLabelCaption(String labelCaption) {
        this.labelCaption = labelCaption;
    }

    public HAlignType getLabelHAlign() {
        return labelHAlign;
    }

    public void setLabelHAlign(HAlignType labelHAlign) {
        this.labelHAlign = labelHAlign;
    }

    public VAlignType getLabelVAlign() {
        return labelVAlign;
    }

    public void setLabelVAlign(VAlignType labelVAlign) {
        this.labelVAlign = labelVAlign;
    }
}
