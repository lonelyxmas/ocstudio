package com.ds.dsm.view.config.nav.group.item;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.group.GroupItemBean;
import com.ds.esd.custom.bean.nav.group.NavGroupViewBean;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/view/config/group/item/")
@MethodChinaName(cname = "Group子对象", imageClass = "spafont spa-icon-c-comboinput")

public class GroupItemMethodService {


    @RequestMapping(method = RequestMethod.POST, value = "MethodInfo")
    @FormViewAnnotation
    @ModuleAnnotation(caption = "组信息")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor, autoRun = true)
    @ResponseBody
    public ResultModel<GroupItemView> getGroupItemInfo(String sourceClassName, String sourceMethodName, String domainId, String viewInstId, String groupId) {
        ResultModel<GroupItemView> result = new ResultModel<GroupItemView>();
        try {
            ViewEntityConfig tableConfig = DSMFactory.getInstance().getViewManager().getViewEntityConfig(sourceClassName, viewInstId);
            ApiClassConfig customESDClassAPIBean = tableConfig.getSourceConfig();
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(sourceMethodName);
            NavGroupViewBean groupViewBean = (NavGroupViewBean) methodAPIBean.getView();
            GroupItemBean groupItemBean = (GroupItemBean) groupViewBean.getItemBean(groupId);
            result.setData(new GroupItemView(groupItemBean));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "loadGroupMethod")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> loadGroupItem(String viewInstId, String domainId, String sourceClassName, String entityClassName, String methodName, String sourceMethodName, String groupId) {
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<List<ViewConfigTree>>();
        JDSActionContext.getActionContext().getContext().put("sourceClassName", entityClassName);
        List<Object> obj = new ArrayList<>();
        ApiClassConfig customESDClassAPIBean = null;
        try {
            customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(entityClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            obj.add(methodAPIBean);
            if (methodAPIBean.getView() != null) {
                obj.add(methodAPIBean.getView());
            }
            resultModel = TreePageUtil.getTreeList(obj, ViewConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        JDSActionContext.getActionContext().getContext().put("sourceClassName", sourceClassName);
        return resultModel;
    }

}
