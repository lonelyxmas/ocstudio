package com.ds.dsm.view.config.grid;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.grid.field.FieldGridInfo;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.grid.CustomGridViewBean;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewEntityConfig;
import com.ds.esd.dsm.view.field.FieldGridConfig;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/view/config/grid/field/")
public class ViewGridFieldsService {


    @RequestMapping(method = RequestMethod.POST, value = "FieldGridList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "列配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<FieldGridInfo>> getFieldGrids(String sourceClassName, String methodName, String domainId, String viewInstId) {
        ListResultModel<List<FieldGridInfo>> cols = new ListResultModel();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, viewInstId);

            MethodConfig methodAPIBean = config.getMethodByName(methodName);
            CustomGridViewBean customGridViewBean = (CustomGridViewBean) methodAPIBean.getView();
            List<String> fieldNames = customGridViewBean.getDisplayFieldNames();
            List<FieldGridConfig> fields = new ArrayList<>();
            for (String fieldName : fieldNames) {
                FieldGridConfig fieldGridConfig = (FieldGridConfig) customGridViewBean.getFieldConfigMap().get(fieldName);
                if (fieldGridConfig != null) {
                    fieldGridConfig.setSourceClassName(sourceClassName);
                    fields.add(fieldGridConfig);
                }
            }
            cols = PageUtil.getDefaultPageList(fields, FieldGridInfo.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ViewGridFieldConfig")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<GridNavTree>> getViewGridFieldConfig(String domainId, String sourceClassName, String methodName, String viewInstId) {
        TreeListResultModel<List<GridNavTree>> result = new TreeListResultModel<>();
        try {
            ApiClassConfig config = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName,  viewInstId);

            MethodConfig methodAPIBean = config.getMethodByName(methodName);
            CustomGridViewBean viewBean = (CustomGridViewBean) methodAPIBean.getView();
            List<FieldGridConfig> customFields = viewBean.getCustomFields();
            List<FieldGridConfig> fieldFormConfigs = new ArrayList<>();
            for (FieldGridConfig fieldGridConfig : customFields) {
                if (fieldGridConfig != null && !fieldGridConfig.getColHidden()) {
                    fieldGridConfig.setSourceClassName(sourceClassName);
                    fieldGridConfig.setEntityClassName(viewBean.getViewClassName());
                    fieldFormConfigs.add(fieldGridConfig);
                }
            }
            result = TreePageUtil.getTreeList(fieldFormConfigs, GridNavTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }
}
