package com.ds.dsm.repository.url;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.repository.entity.EntityTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/manager/repository/url/")
@MethodChinaName(cname = "远程服务管理", imageClass = "spafont spa-icon-c-gallery")

public class RemoteUrlService {

    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;

    @MethodChinaName(cname = "添加远程服务")
    @RequestMapping(value = {"AddUrlTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation()
    @DialogAnnotation(width = "400")
    @ModuleAnnotation( caption = "添加远程服务", dynLoad = true, dock = Dock.fill, name = "AddUrlTree")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<UrlTree>> addUrlTree(String projectVersionName) {
        List<Object> entities = Arrays.asList(new String[]{projectVersionName});
        TreeListResultModel<List<UrlTree>> result = TreePageUtil.getTreeList(entities, UrlTree.class);
        return result;
    }

    @MethodChinaName(cname = "保存远程服务")
    @RequestMapping(value = {"saveUrls"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveUrls(String projectVersionName, String AddUrlTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (AddUrlTree != null) {
            try {
                RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                if (repositoryInst != null) {
                    String[] urls = StringUtility.split(AddUrlTree, ";");
                    DSMFactory.getInstance().getRepositoryManager().addUrls(projectVersionName, urls);
                }

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除远程服务")
    @RequestMapping(value = {"delUrl"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delUrl(String projectVersionName, String url) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (projectVersionName != null) {
            try {
                RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                Set<String> urlNames = repositoryInst.getUrlNames();
                String[] urls = StringUtility.split(url, ";");
                for (String urlName : urls) {
                    urlNames.remove(urlName);
                }
                DSMFactory.getInstance().getRepositoryManager().updateRepositoryInst(repositoryInst, true);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public TreeListResultModel<List<EntityTree>> loadChild(String projectVersionName) {
        TreeListResultModel<List<EntityTree>> result = new TreeListResultModel<List<EntityTree>>();
        try {
            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            List<EntityTree> treeViews = new ArrayList<>();

            result.setData(treeViews);
            result.setIds(Arrays.asList(repositoryInst.getUrlNames().toArray(new String[]{})));
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "UrlNav")
    @NavGroupViewAnnotation
    @ModuleAnnotation(caption = "远程服务")
    @DialogAnnotation
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<UrlNav> getUrlNav(String projectVersionName, String projectId, String url) {
        ResultModel<UrlNav> result = new ResultModel<UrlNav>();
        return result;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }
}
