package com.ds.dsm.repository.url;

import com.ds.dsm.repository.entity.EntityService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Pid;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = EntityService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class UrlTree extends TreeListItem {

    @Pid
    String projectVersionName;

    @Pid
    String esdClassName;


    @TreeItemAnnotation(imageClass = "spafont spa-icon-coin",caption = "资源库信息",bindService = EntityService.class)
    public UrlTree(String projectVersionName) {
        this.projectVersionName = projectVersionName;
        this.id = "all";
    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-coin", iniFold = false)
    public UrlTree(ESDClass esdClass, String projectVersionName) {
        this.caption = esdClass.getName() + "(" + esdClass.getDesc() + ")";
        this.id = esdClass.getClassName();
        this.esdClassName = esdClass.getClassName();
        this.projectVersionName = projectVersionName;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getEsdClassName() {
        return esdClassName;
    }

    public void setEsdClassName(String esdClassName) {
        this.esdClassName = esdClassName;
    }
}
