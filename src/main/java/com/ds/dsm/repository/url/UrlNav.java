package com.ds.dsm.repository.url;

import com.ds.config.ResultModel;
import com.ds.dsm.repository.entity.EntityMetaView;
import com.ds.dsm.repository.entity.EntityService;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.BlockAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@BottomBarMenu
@RequestMapping(path = "/dsm/manager/repository/urls/")
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = EntityService.class)
@MethodChinaName(cname = "远程信息管理", imageClass = "spafont spa-icon-c-gallery")
public class UrlNav {

    @CustomAnnotation(pid = true, hidden = true)
    public String projectVersionName;

    @CustomAnnotation(uid = true, hidden = true)
    public String projectId;


    @MethodChinaName(cname = "远程服务信息")
    @RequestMapping(method = RequestMethod.POST, value = "UrlInfo")
    @FormViewAnnotation()
    @ModuleAnnotation(caption = "远程服务信息", dock = Dock.top)
    @UIAnnotation(height = "180")
    @ResponseBody
    public ResultModel<RemoteUrlFormView> getUrlInfo(String projectVersionName, String projectId, String sourceClassName) {
        ResultModel<RemoteUrlFormView> result = new ResultModel<RemoteUrlFormView>();
        return result;
    }

    @MethodChinaName(cname = "详细信息")
    @RequestMapping(method = RequestMethod.POST, value = "UrlMeta")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, imageClass = "spafont spa-icon-c-databinder")
    @ResponseBody
    public ResultModel<EntityMetaView> getEntityMetaInfo(String projectVersionName, String projectId, String sourceClassName) {
        ResultModel<EntityMetaView> result = new ResultModel<EntityMetaView>();
        return result;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
