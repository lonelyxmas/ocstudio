package com.ds.dsm.repository.url;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.dsm.repository.entity.EntityMethodView;
import com.ds.dsm.repository.entity.FieldModuleView;
import com.ds.dsm.repository.entity.ref.EntityRefGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.config.EntityConfig;
import com.ds.esd.dsm.repository.entity.EntityRef;
import com.ds.esd.dsm.view.field.FieldModuleConfig;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/dsm/manager/repository/url/")
@MethodChinaName(cname = "资源库配置")
public class UrlMetaView {

    @CustomAnnotation(pid = true, hidden = true)
    public String projectVersionName;

    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;

    @CustomAnnotation(hidden = true, uid = true)
    public String sourceClassName;

    public UrlMetaView() {

    }

    @RequestMapping(method = RequestMethod.POST, value = "MethodList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-function",caption = "函数")
    @CustomAnnotation( index = 0)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<EntityMethodView>> getMethodLists(String sourceClassName, String methodName) {
        ListResultModel<List<EntityMethodView>> resultModel = new ListResultModel();
        try {
            EntityConfig repository = DSMFactory.getInstance().getRepositoryManager().getEntityConfig(sourceClassName, true);
            List<MethodConfig> fieldEntityConfigs = repository.getOtherMethodsList();
            resultModel = PageUtil.getDefaultPageList(fieldEntityConfigs, EntityMethodView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ModuleList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-values",  caption = "关联实体")
    @CustomAnnotation( index = 1)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<FieldModuleView>> getModuleList(String sourceClassName, String methodName, String projectVersionName) {
        ListResultModel<List<FieldModuleView>> resultModel = new ListResultModel();
        try {
            EntityConfig repository = DSMFactory.getInstance().getRepositoryManager().getEntityConfig(sourceClassName, true);
            List<FieldModuleConfig> fieldEntityConfigs = repository.getModuleMethods();
            resultModel = PageUtil.getDefaultPageList(fieldEntityConfigs, FieldModuleView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "关联关系")
    @RequestMapping(method = RequestMethod.POST, value = "Refs")
    @GridViewAnnotation()
    @CustomAnnotation( index = 2)
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-databinder", caption = "关联关系")
    @ResponseBody
    public ListResultModel<List<EntityRefGridView>> getTableRefList(String sourceClassName, String projectVersionName) {
        ListResultModel<List<EntityRefGridView>> tables = new ListResultModel();
        try {
            DSMFactory factory = DSMFactory.getInstance(ESDFacrory.getESDClient().getSpace());
            List<EntityRef> tableInfos = factory.getRepositoryManager().getEntityRefByName(sourceClassName, projectVersionName);
            tables = PageUtil.getDefaultPageList(tableInfos, EntityRefGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return tables;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}

