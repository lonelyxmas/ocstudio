package com.ds.dsm.repository.entity;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {EntityService.class}, event = CustomGridEvent.editor)
public class EntityMethodView {


    @CustomAnnotation(caption = "方法名称")
    String desc;
    @CustomAnnotation(caption = "接口信息")
    String methodInfo;
    @CustomAnnotation(caption = "方法名称")
    String methodName;


    public EntityMethodView(MethodConfig methodConfig) {

        this.methodName = methodConfig.getMethodName();
        this.methodInfo = methodConfig.getMetaInfo();
        this.desc = methodConfig.getCaption();
    }

    public String getMethodInfo() {
        return methodInfo;
    }

    public void setMethodInfo(String methodInfo) {
        this.methodInfo = methodInfo;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}


