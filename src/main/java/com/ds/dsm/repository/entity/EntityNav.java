package com.ds.dsm.repository.entity;

import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.BlockAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@BottomBarMenu
@RequestMapping(path = "/dsm/manager/repository/entity/")
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = EntityService.class)
@MethodChinaName(cname = "实体管理", imageClass = "spafont spa-icon-c-gallery")
public class EntityNav {

    @CustomAnnotation(uid = true, hidden = true)
    public String sourceClassName;

    @CustomAnnotation(pid = true, hidden = true)
    public String projectVersionName;

    @CustomAnnotation(uid = true, hidden = true)
    public String projectId;


    @MethodChinaName(cname = "实体信息")
    @RequestMapping(method = RequestMethod.POST, value = "EntityInfo")
    @FormViewAnnotation()
    @ModuleAnnotation(caption = "实体信息", dock = Dock.top)
    @UIAnnotation(height = "180")
    @ResponseBody
    public ResultModel<EntityFormView> getEntityInfo(String projectVersionName, String projectId, String sourceClassName) {
        ResultModel<EntityFormView> result = new ResultModel<EntityFormView>();
        return result;
    }

    @MethodChinaName(cname = "关联信息")
    @RequestMapping(method = RequestMethod.POST, value = "EntityMeta")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, imageClass = "spafont spa-icon-c-databinder")
    @ResponseBody
    public ResultModel<EntityMetaView> getEntityMetaInfo(String projectVersionName, String projectId, String sourceClassName) {
        ResultModel<EntityMetaView> result = new ResultModel<EntityMetaView>();
        return result;

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
