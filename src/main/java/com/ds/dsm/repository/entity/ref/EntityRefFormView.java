package com.ds.dsm.repository.entity.ref;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.repository.entity.EntityRef;
import com.ds.web.annotation.RefType;

@BottomBarMenu
@FormAnnotation(col = 1, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {EntityRefService.class})
public class EntityRefFormView {


    @CustomAnnotation(uid = true, hidden = true)
    String refId;

    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;

    @CustomAnnotation(caption = "表名", pid = true)
    String className;

    @CustomAnnotation(caption = "关联对象")
    String otherClassName;

    @FieldAnnotation( required = true)
    @CustomAnnotation(caption = "引用关系")
    RefType ref;


    EntityRefFormView() {

    }


    public EntityRefFormView(EntityRef ref) {
        this.projectVersionName = ref.getProjectVersionName();
        this.ref = ref.getRefBean().getRef();
        this.className = ref.getClassName();
        this.otherClassName = ref.getOtherClassName();
        this.refId = ref.getRefId();


    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getOtherClassName() {
        return otherClassName;
    }

    public void setOtherClassName(String otherClassName) {
        this.otherClassName = otherClassName;
    }

    public RefType getRef() {
        return ref;
    }

    public void setRef(RefType ref) {
        this.ref = ref;
    }

}
