package com.ds.dsm.repository.entity.ref;

import com.ds.common.JDSException;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.ESDField;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

import java.util.Set;

@BottomBarMenu()
@TreeAnnotation(selMode = SelModeType.singlecheckbox, customService = {EntityRefService.class}, bottombarMenu = {CustomFormMenu.Close, CustomFormMenu.Save})
public class EntityFieldItem extends TreeListItem {


    public EntityFieldItem(String projectVersionName) throws JDSException {
        super("allClass", "所有实体");
        this.setIniFold(false);

        RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
        Set<String> classNames = repositoryInst.getEntityNames();
        for (String className : classNames) {
            ESDClass esdClass = DSMFactory.getInstance().getClassManager().getRepositoryClass(className, true);
            if (esdClass != null) {
                this.addChild(new EntityFieldItem(esdClass));
            }

        }
    }


    public EntityFieldItem(ESDClass esdClass) throws JDSException {
        super(esdClass.getClassName(), esdClass.getDesc() + "(" + esdClass.getName() + ")");
        for (ESDField colInfo : esdClass.getFieldList()) {
            this.addChild(new EntityFieldItem(colInfo));
        }
    }

    public EntityFieldItem(ESDField esdField) throws JDSException {
        super(esdField.getESDClass().getClassName() + "_" + esdField.getName(), esdField.getCaption());
    }


}
