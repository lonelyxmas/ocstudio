package com.ds.dsm.repository.entity;


import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.web.annotation.Required;

@FormAnnotation()
public class EntityFormView {


    @CustomAnnotation(uid = true, hidden = true)
    public String className;

    @FieldAnnotation(required = true, colSpan = -1)
    @CustomAnnotation(caption = "名称")
    public String name;
    @Required
    @CustomAnnotation(caption = "包名")
    public String packageName;
    @Required
    @CustomAnnotation(caption = "UID")
    private String uid;
    @FieldAnnotation(colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "描述", hidden = true)
    private String desc;

    public EntityFormView(ESDClass esdClass) {
        this.name = esdClass.getName();

        this.className = esdClass.getCtClass().getName();
        this.packageName = esdClass.getCtClass().getPackage().getName();
        this.uid = esdClass.getUid();
        this.desc = esdClass.getDesc();

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
