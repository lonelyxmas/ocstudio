package com.ds.dsm.repository.entity;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/manager/repository/entity/")
@MethodChinaName(cname = "实体管理", imageClass = "spafont spa-icon-c-gallery")

public class EntityService {

    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;
    @CustomAnnotation(pid = true, hidden = true)
    String projectId;


    @MethodChinaName(cname = "添加实体对象")
    @RequestMapping(value = {"AddEntityTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation()
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "添加实体对象", dynLoad = true, dock = Dock.fill, name = "AddEntityTree")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<EntityTree>> addEntityTree(String projectVersionName) {
        TreeListResultModel<List<EntityTree>> result = new TreeListResultModel<>();

        try {
            List<Object> entities = Arrays.asList(new String[]{projectVersionName});
            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            result = TreePageUtil.getTreeList(entities, EntityTree.class, Arrays.asList(repositoryInst.getEntityNames().toArray(new String[]{})));
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }

    @MethodChinaName(cname = "保存实体关系")
    @RequestMapping(value = {"saveEntityClass"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveEntityClass(String projectVersionName, String AddEntityTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (AddEntityTree != null) {
            try {
                RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                if (repositoryInst != null) {
                    String[] esdClassNames = StringUtility.split(AddEntityTree, ";");
                    DSMFactory.getInstance().getRepositoryManager().addEntity(projectVersionName, esdClassNames);
                }

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @MethodChinaName(cname = "删除实体关系")
    @RequestMapping(value = {"delEntityClass"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delEntityClass(String projectVersionName, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (projectVersionName != null) {
            try {
                RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                Set<String> classNameList = repositoryInst.getEntityNames();
                String[] classNames = StringUtility.split(sourceClassName, ";");
                for (String esdClassName : classNames) {
                    classNameList.remove(esdClassName);
                    DSMFactory.getInstance().getRepositoryManager().delEntity(esdClassName, projectVersionName);
                }
                DSMFactory.getInstance().getRepositoryManager().updateRepositoryInst(repositoryInst, true);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    public TreeListResultModel<List<EntityTree>> loadChild(String projectVersionName) {
        TreeListResultModel<List<EntityTree>> result = new TreeListResultModel<List<EntityTree>>();
        try {
            RepositoryInst repositoryInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            List<EntityTree> treeViews = new ArrayList<>();
            DSMFactory.getInstance().reload();
            List<ESDClass> esdClasses = DSMFactory.getInstance().getClassManager().getRepositoryClassList();
            if (esdClasses.size() > 0) {
                for (ESDClass child : esdClasses) {
                    treeViews.add(new EntityTree(child, projectVersionName));
                }
            }
            result.setData(treeViews);
            result.setIds(Arrays.asList(repositoryInst.getEntityNames().toArray(new String[]{})));
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "EntityNav")
    @NavGroupViewAnnotation
    @ModuleAnnotation(caption = "实体信息")
    @DialogAnnotation
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<EntityNav> getEntityNav(String projectVersionName, String projectId, String sourceClassName) {
        ResultModel<EntityNav> result = new ResultModel<EntityNav>();
        return result;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
