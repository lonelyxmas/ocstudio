package com.ds.dsm.repository.entity.ref;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.gallery.annotation.GalleryAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.dsm.repository.entity.EntityRef;
import com.ds.web.annotation.RefType;

@PageBar
@GalleryAnnotation()
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {EntityRefService.class}, event = CustomGridEvent.editor)
public class EntityRefGridView {


    @CustomAnnotation(uid = true, hidden = true)
    String refId;

    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;

    @CustomAnnotation(caption = "关联表", hidden = true, pid = true)
    String className;

    @CustomAnnotation(caption = "名称", pid = true)
    String name;

    @FieldAnnotation(  required = true)
    @CustomAnnotation(caption = "引用关系")
    RefType ref;

    @CustomAnnotation(caption = "关联字段", pid = true)
    String methodName;

    @CustomAnnotation(caption = "关联对象")
    String otherClassName;


    EntityRefGridView() {

    }


    public EntityRefGridView(EntityRef ref) {

        this.projectVersionName = ref.getProjectVersionName();
        this.ref = ref.getRefBean().getRef();
        this.name = ref.getRefBean().getRef().name();
        this.className = ref.getClassName();
        this.otherClassName = ref.getOtherClassName();
        this.methodName = ref.getMethodName();
        this.refId = ref.getRefId();


    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getOtherClassName() {
        return otherClassName;
    }

    public void setOtherClassName(String otherClassName) {
        this.otherClassName = otherClassName;
    }

    public RefType getRef() {
        return ref;
    }

    public void setRef(RefType ref) {
        this.ref = ref;
    }

}
