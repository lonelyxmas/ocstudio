package com.ds.dsm.repository.entity;


import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.web.annotation.Required;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {EntityService.class}, event = CustomGridEvent.editor)
public class EntityGridView {

    @CustomAnnotation(hidden = true, pid = true)
    public String viewInstId;

    @CustomAnnotation(hidden = true, pid = true)
    public String domainId;

    @CustomAnnotation(uid = true, hidden = true)
    public String sourceClassName;

    @FieldAnnotation(required = true, colSpan = -1)
    @CustomAnnotation(caption = "名称")
    public String name;

    @Required
    @CustomAnnotation(caption = "包名")
    public String packageName;

    @Required
    @CustomAnnotation(caption = "UID")
    private String uid;

    @FieldAnnotation( colSpan = -1, rowHeight = "50")
    @CustomAnnotation(caption = "描述", hidden = true)
    private String desc;

    public EntityGridView(ESDClass esdClass) {
        this.name = esdClass.getName();
        this.viewInstId = esdClass.getViewInstId();
        this.domainId = esdClass.getDomainId();

        this.sourceClassName = esdClass.getCtClass().getName();
        this.packageName = esdClass.getCtClass().getPackage().getName();
        this.uid = esdClass.getUid();
        this.desc = esdClass.getDesc();

    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
