package com.ds.dsm.repository.db.ref;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.database.ref.TableRef;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping(path = "/dsm/repository/ref/")
@MethodChinaName(cname = "关联表信息", imageClass = "spafont spa-icon-c-databinder")

public class RefService {
    @RequestMapping(method = RequestMethod.POST, value = "RefInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "300", height = "350")
    @ModuleAnnotation( caption = "关联表信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<RefFormView> getRefInfo(String refId, String projectVersionName) {
        ResultModel<RefFormView> result = new ResultModel<RefFormView>();
        try {
            TableRef ref = DSMFactory.getInstance().getRepositoryManager().getTableRefById(refId, projectVersionName);
            result.setData(new RefFormView(ref));
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "CreateRefInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "300", height = "350")
    @ModuleAnnotation( caption = "关联表信息")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public ResultModel<RefFormView> createRefInfo(String tablename, String refId, String projectVersionName) {
        ResultModel<RefFormView> result = new ResultModel<RefFormView>();
        TableRef ref = new TableRef();
        ref.setTablename(tablename);

        ref.setProjectVersionName(projectVersionName);
        refId = UUID.randomUUID().toString();
        ref.setRefId(refId);
        result.setData(new RefFormView(ref));
        return result;

    }

    @MethodChinaName(cname = "更新外键关系")
    @RequestMapping(value = {"updateRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save, CustomMenuItem.saveRow})
    public @ResponseBody
    ResultModel<Boolean> updateRef(@RequestBody TableRef ref) {
        ResultModel result = new ResultModel<>();
        try {
            DSMFactory factory = DSMFactory.getInstance(ESDFacrory.getESDClient().getSpace());
            String otherTableName = null;
            if (ref.getFkField() == null || ref.getFkField().equals("")) {
                throw new JDSException("外键不能为空！");
            }
            if (ref.getPkField() == null || ref.getPkField().equals("")) {
                throw new JDSException("主键不能为空！");
            }

            if (ref.getRef() == null) {
                throw new JDSException("关系不能为空！");
            }
            String mainTableName = ref.getTablename();

            String pk = ref.getPkField();
            if (pk.indexOf(".") > -1) {
                String tableName = StringUtility.split(pk, ".")[0];
                if (tableName.equals(mainTableName)) {
                    String fk = ref.getFkField();
                    if (fk.indexOf(".") > -1) {
                        otherTableName = StringUtility.split(fk, ".")[0];
                        ref.setOtherTableName(otherTableName);
                    }

                }

            }
            factory.getRepositoryManager().updateTableRef(ref);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }

        return result;

    }

    @MethodChinaName(cname = "删除关联信息")
    @RequestMapping(value = {"deleteRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> deleteRef(String refId, String projectVersionName) {
        ResultModel result = new ResultModel<>();
        try {
            DSMFactory factory = DSMFactory.getInstance(ESDFacrory.getESDClient().getSpace());
            factory.getRepositoryManager().delTableRef(refId, projectVersionName);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }

        return new ResultModel<>();

    }


    @MethodChinaName(cname = "添加外键关系")
    @RequestMapping(value = {"AddFkField"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation(fieldId = "fkField",
            fieldCaption = "fkField",
            saveUrl = "updateFk")
    @DialogAnnotation(width = "300", height = "350")
    @ModuleAnnotation(dynLoad = true, caption = "添加外键关系", dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<FieldItem>> addAddFkField(String tablename, String projectId, String projectVersionName) {
        TreeListResultModel<List<FieldItem>> result = new TreeListResultModel<List<FieldItem>>();
        try {
            List<FieldItem> list = new ArrayList<>();
            list.add(new FieldItem(projectVersionName));
            result.setData(list);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(value = {"AddPkField"}, method = {RequestMethod.GET, RequestMethod.POST})

    @PopTreeViewAnnotation(
            fieldId = "pkField",
            fieldCaption = "pkField",
            saveUrl = "updatePk")
    @DialogAnnotation( width = "350", height = "400")
    @ModuleAnnotation(caption = "添加主键", dynLoad = true,  dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<FieldItem>> addPkField(String tablename, String projectVersionName, String projectId) {
        TreeListResultModel<List<FieldItem>> result = new TreeListResultModel<List<FieldItem>>();
        try {
            List<FieldItem> list = new ArrayList<>();
            list.add(new FieldItem(projectVersionName));
            result.setData(list);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(value = {"AddCaptionField"}, method = {RequestMethod.GET, RequestMethod.POST})

    @PopTreeViewAnnotation(
            fieldId = "mainCaption",
            fieldCaption = "mainCaption",
            saveUrl = "updateCaption"
    )
    @DialogAnnotation(width = "350", height = "400")
    @ModuleAnnotation(dynLoad = true, caption = "添加显示选项", dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<FieldItem>> addCaptionField(String tablename, String projectVersionName, String projectId) {
        TreeListResultModel<List<FieldItem>> result = new TreeListResultModel<List<FieldItem>>();
        try {
            List<FieldItem> list = new ArrayList<>();
            list.add(new FieldItem(projectVersionName));
            result.setData(list);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(value = {"AddOtherCaptionField"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation(
            fieldId = "otherCaption",
            fieldCaption = "otherCaption",
            saveUrl = "updateOtherCaption")
    @DialogAnnotation(width = "350", height = "400")
    @ModuleAnnotation(dynLoad = true, caption = "添加显示选项", dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<FieldItem>> addOtherCaptionField(String tablename, String projectVersionName, String projectId) {
        TreeListResultModel<List<FieldItem>> result = new TreeListResultModel<List<FieldItem>>();
        try {
            List<FieldItem> list = new ArrayList<>();
            list.add(new FieldItem(projectVersionName));
            result.setData(list);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "更新外键关系")
    @RequestMapping(value = {"updateFk"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> updateFk(String projectVersionName, String tablename, String AddFkFieldTree) {
        return new ResultModel<>();

    }


    @MethodChinaName(cname = "更新外键关系")
    @RequestMapping(value = {"updatePk"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> updatePk(String tablename, String AddPkFieldTree) {
        return new ResultModel<>();

    }

    @MethodChinaName(cname = "更新库表关系")
    @RequestMapping(value = {"updateCaption"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> updateCaption(String tablename, String AddCaptionFieldTree) {
        return new ResultModel<>();

    }

    @MethodChinaName(cname = "更新外键库表关系")
    @RequestMapping(value = {"updateOtherCaption"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> updateOtherCaption(String tablename, String AddCaptionFieldTree) {
        return new ResultModel<>();
    }

}
