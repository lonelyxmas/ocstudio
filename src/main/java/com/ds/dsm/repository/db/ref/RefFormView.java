package com.ds.dsm.repository.db.ref;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.ComboPopAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.database.proxy.DSMTableProxy;
import com.ds.esd.dsm.repository.database.ref.TableRef;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.web.annotation.RefType;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(col = 1, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {RefService.class})
public class RefFormView {


    @CustomAnnotation(uid = true, hidden = true)
    String refId;

    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @CustomAnnotation(caption = "表名", pid = true)
    String tablename;


    @CustomAnnotation(caption = "关联表", hidden = true)
    String otherTableName;

    @Required
    @CustomAnnotation(caption = "关系")
    RefType ref;

    @ComboPopAnnotation(src = "AddPkField")
    @ComboInputAnnotation(inputType = ComboInputType.getter)
    @Required
    @CustomAnnotation(caption = "主键")
    String pkField;

    @ComboPopAnnotation(src = "AddFkField")
    @ComboInputAnnotation(inputType = ComboInputType.getter)
    @Required
    @CustomAnnotation(caption = "外键")
    String fkField;

    @ComboPopAnnotation(src = "AddCaptionField")
    @ComboInputAnnotation(inputType = ComboInputType.getter)
    @Required
    @CustomAnnotation(caption = "主键显示字段")
    String mainCaption;

    @ComboPopAnnotation(src = "AddOtherCaptionField")
    @ComboInputAnnotation(inputType = ComboInputType.getter)
    @Required
    @CustomAnnotation(caption = "外键显示字段")
    String otherCaption;


    RefFormView() {

    }


    public RefFormView(TableRef ref) {

        this.projectVersionName = ref.getProjectVersionName();
        this.ref = ref.getRef();
        this.tablename = ref.getTablename();
        this.otherTableName = ref.getOtherTableName();
        this.otherCaption = ref.getOtherCaption();
        this.fkField = ref.getFkField();
        this.pkField = ref.getPkField();
        this.mainCaption = ref.getMainCaption();
        DSMTableProxy mainTable = null;
        try {
            mainTable = DSMFactory.getInstance().getRepositoryManager().getTableProxyByName(tablename, projectVersionName);
            if (pkField == null || pkField.equals("")) {
                pkField = mainTable.getTableName() + "." + mainTable.getPkName();
            }

            if (mainCaption == null || mainCaption.equals("")) {
                if (mainTable.getCaptionField() != null) {
                    mainCaption = mainTable.getTableName() + "." + mainTable.getCaptionField().getFieldName();
                } else {
                    mainCaption = mainTable.getTableName() + "." + mainTable.getPkFieldName();
                }

            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

        this.refId = ref.getRefId();


    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getMainCaption() {
        return mainCaption;
    }

    public void setMainCaption(String mainCaption) {
        this.mainCaption = mainCaption;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getOtherTableName() {
        return otherTableName;
    }

    public void setOtherTableName(String otherTableName) {
        this.otherTableName = otherTableName;
    }

    public RefType getRef() {
        return ref;
    }

    public void setRef(RefType ref) {
        this.ref = ref;
    }

    public String getPkField() {
        return pkField;
    }

    public void setPkField(String pkField) {
        this.pkField = pkField;
    }

    public String getFkField() {
        return fkField;
    }

    public void setFkField(String fkField) {
        this.fkField = fkField;
    }

    public String getOtherCaption() {
        return otherCaption;
    }

    public void setOtherCaption(String otherCaption) {
        this.otherCaption = otherCaption;
    }
}
