package com.ds.dsm.repository.db.ref;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.dsm.repository.database.proxy.DSMColProxy;
import com.ds.esd.dsm.repository.database.proxy.DSMTableProxy;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

import java.util.Set;

@BottomBarMenu()
@TreeAnnotation(selMode = SelModeType.singlecheckbox, customService = {RefService.class}, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class FieldItem extends TreeListItem {

    String projectId;

    public FieldItem(String projectVersionName) throws JDSException {
        super("allTable", "关联库表");
        this.setIniFold(false);

        RepositoryInst dsmInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
        Set<String> tableNames = dsmInst.getTableNames();
        for (String tableName : tableNames) {
            DSMTableProxy proxy = DSMFactory.getInstance().getRepositoryManager().getTableProxyByName(tableName, projectVersionName);
            this.addChild(new FieldItem(proxy));
        }
    }


    public FieldItem(DSMTableProxy tableProxy) throws JDSException {
        super(tableProxy.getFieldName(), tableProxy.getCnname() + "(" + tableProxy.getFieldName() + ")");
        for (DSMColProxy colInfo : tableProxy.getDSMColList()) {
            this.addChild(new FieldItem(colInfo));
        }
    }

    public FieldItem(DSMColProxy colInfo) throws JDSException {
        super(colInfo.getDbcol().getTablename() + "." + colInfo.getDbcol().getName(), colInfo.getTableFieldInfo().getDesc());
        this.addTagVar("dsmId", colInfo.getProjectVersionName());
    }


}
