package com.ds.dsm.repository.db.table;


import com.ds.common.database.metadata.TableInfo;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;

@BottomBarMenu
@FormAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class TableRefFormView {

    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;

    @CustomAnnotation(hidden = true, pid = true)
    public String projectVersionName;

    @CustomAnnotation(uid = true, hidden = true)
    public String tablename;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "表名")
    public String name;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "注解")
    public String cnname;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "数据库标识")
    public String configKey;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "主键")
    private String pkName;

    @CustomAnnotation(caption = "连接串", hidden = true)
    private String url;

    public TableRefFormView(TableInfo info, String projectVersionName) {
        init(info);
        this.projectVersionName = projectVersionName;
    }

    void init(TableInfo info) {
        this.name = info.getName();
        this.cnname = info.getCnname();
        this.pkName = info.getPkName();
        this.configKey = info.getConfigKey();
        this.url = info.getUrl();
        this.tablename = info.getName();
    }

    public TableRefFormView(TableInfo info) {
        init(info);
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getPkName() {
        return pkName;
    }

    public void setPkName(String pkName) {
        this.pkName = pkName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

}
