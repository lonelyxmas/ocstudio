package com.ds.dsm.repository.db.action;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(path = "/dsm/repository/action/row/")
public class DBRowCMDAction {


    @MethodChinaName(cname = "清空")
    @RequestMapping(method = RequestMethod.POST, value = "clearProject")
    @CustomAnnotation(imageClass = "fa fa-lg fa-circle-o-notch")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> clearProject(String projectVersionName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();

        try {
            DSMFactory.getInstance().clearProject(projectVersionName);

            reload();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "构建")
    @RequestMapping(method = RequestMethod.POST, value = "buildRepository")
    @CustomAnnotation(imageClass = "xuicon spafont spa-icon-debug1")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> buildRepository(String projectVersionName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();

        try {
            RepositoryInst dsmInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            DSMFactory.getInstance().buildProject(dsmInst, getCurrChromeDriver());
            getCurrChromeDriver().printLog("重新装载服务...", true);
            reload();
            getCurrChromeDriver().printLog("编译完成！", true);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "编译")
    @RequestMapping(method = RequestMethod.POST, value = "compileRepositoryInst")
    @CustomAnnotation(imageClass = "spafont spa-icon-c-databinder")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> compileRepositoryInst(String projectVersionName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();

        try {
            RepositoryInst dsmInst = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            DSMFactory.getInstance().compileRepositoryInst(dsmInst, getCurrChromeDriver());
            getCurrChromeDriver().printLog("重新装载服务...", true);
            reload();
            getCurrChromeDriver().printLog("编译完成！", true);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    void rebuildCustomModule(String projectId, String packageName, String esdPackageName) {
        try {
            Map map = new HashMap();
            map.put("projectId", projectId);
            ESDChrome chrome = getCurrChromeDriver();
            ESDFacrory.getESDClient().buildCustomModule(projectId, packageName, esdPackageName, map, chrome);

        } catch (JDSException e) {
            e.printStackTrace();
        }

    }


    ResultModel<Boolean> reload() {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        ESDChrome chrome = getCurrChromeDriver();
        chrome.printLog("正在装载编译，请稍后！", true);
        try {
            ESDFacrory.getInstance().dyReload(null);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;

    }


    public ESDChrome getCurrChromeDriver() {
        ESDChrome chrome = JDSActionContext.getActionContext().Par("$currChromeDriver", ESDChrome.class);
        return chrome;
    }


}

