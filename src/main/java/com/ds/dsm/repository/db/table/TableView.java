package com.ds.dsm.repository.db.table;

import com.ds.common.database.metadata.TableInfo;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.enums.StretchType;

@FormAnnotation(stretchHeight = StretchType.last)
public class TableView {

    @CustomAnnotation(hidden = true, pid = true)
    public String projectId;

    @CustomAnnotation(hidden = true, pid = true)
    public String projectVersionName;

    @CustomAnnotation(uid = true, hidden = true)
    public String tablename;


    @CustomAnnotation(caption = "表名", readonly = true)
    public String name;

    @CustomAnnotation(caption = "注解", readonly = true)
    public String cnname;

    @CustomAnnotation(caption = "数据库标识", readonly = true)
    public String configKey;


    @CustomAnnotation(caption = "主键", readonly = true)
    private String pkName;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "连接串")
    private String url;

    public TableView(TableInfo info) {
        this.name = info.getName();
        this.cnname = info.getCnname();
        this.pkName = info.getPkName();
        this.configKey = info.getConfigKey();
        this.url = info.getUrl();
        this.tablename = info.getName();
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getPkName() {
        return pkName;
    }

    public void setPkName(String pkName) {
        this.pkName = pkName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

}
