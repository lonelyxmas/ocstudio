package com.ds.dsm.repository.db.service;

import com.ds.common.JDSException;
import com.ds.common.database.metadata.ProviderConfig;
import com.ds.common.database.metadata.TableInfo;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.manager.repository.SelectTableTopTree;
import com.ds.dsm.repository.db.table.TableNav;
import com.ds.dsm.repository.db.table.TableRefGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.RepositoryInst;
import com.ds.esd.dsm.repository.database.FDTFactory;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(path = "/dsm/repository/")
@MethodChinaName(cname = "数据库表", imageClass = "spafont spa-icon-c-gallery")

public class DSMTableService {


    @CustomAnnotation(uid = true, hidden = true)
    String projectVersionName;
    @CustomAnnotation(pid = true, hidden = true)
    String projectId;


    @RequestMapping(method = RequestMethod.POST, value = "TableList")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-buttonview", caption = "数据库表")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public ListResultModel<List<TableRefGridView>> getTableList(String projectVersionName) {
        ListResultModel<List<TableRefGridView>> result = new ListResultModel();
        try {
            List<TableRefGridView> tableList = new ArrayList<>();
            RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            if (bean != null) {
                for (String tableName : bean.getTableNames()) {
                    TableInfo tableInfo = FDTFactory.getInstance().getTableInfoByFullName(tableName);
                    if (tableInfo != null) {
                        tableList.add(new TableRefGridView(tableInfo, projectVersionName));
                    }
                }
                result = PageUtil.getDefaultPageList(tableList);
            }
        } catch (Exception e) {
            result = new ErrorListResultModel<>();

            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @MethodChinaName(cname = "保存数据库表")
    @RequestMapping(value = {"saveTempTable"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.treeSave)
    public @ResponseBody
    ResultModel<Boolean> saveTempTable(String projectVersionName, String AddTempTableTree) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (AddTempTableTree != null) {
            try {
                RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                if (bean != null) {
                    Set<String> tableNameList = bean.getTableNames();
                    String[] tableNames = StringUtility.split(AddTempTableTree, ";");
                    for (String tableName : tableNames) {
                        if (!tableNameList.contains(tableName)) {
                            tableNameList.add(tableName);
                        }
                    }
                    bean.getTableNames().addAll(tableNameList);
                    DSMFactory.getInstance().getRepositoryManager().updateRepositoryInst(bean, true);
                }

            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    @MethodChinaName(cname = "删除数据库表关系")
    @RequestMapping(value = {"delTempTable"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delTempTable(String projectVersionName, String tablename) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (projectVersionName != null) {
            try {
                RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
                Set<String> tableNameList = bean.getTableNames();
                String[] tableNames = StringUtility.split(tablename, ";");
                for (String tableName : tableNames) {
                    tableNameList.remove(tableName);
                    DSMFactory.getInstance().getRepositoryManager().delEntity(tableName, projectVersionName);
                }
                DSMFactory.getInstance().getRepositoryManager().updateRepositoryInst(bean, true);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    @MethodChinaName(cname = "添加数据库表")
    @RequestMapping(value = {"AddTempTable"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "添加数据库表", dynLoad = true, dock = Dock.fill)
    @APIEventAnnotation(isAllform = true, bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<SelectTableTopTree>> getTableTreeView(String projectVersionName) {
        TreeListResultModel<List<SelectTableTopTree>> result = new TreeListResultModel<List<SelectTableTopTree>>();
        try {
            RepositoryInst bean = DSMFactory.getInstance().getRepositoryManager().getProjectRepository(projectVersionName);
            List<ProviderConfig> configs = ESDFacrory.getESDClient().getAllDbConfig();
            result = TreePageUtil.getTreeList(configs, SelectTableTopTree.class);
            result.setIds(Arrays.asList(bean.getTableNames().toArray(new String[]{})));
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "TableNav")
    @NavGroupViewAnnotation
    @ModuleAnnotation(caption = "库表信息")
    @DialogAnnotation
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<TableNav> getTableNav(String projectVersionName, String projectId, String tablename) {
        ResultModel<TableNav> result = new ResultModel<TableNav>();
        return result;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
