package com.ds.dsm.repository.db.table;

import com.ds.common.JDSException;
import com.ds.common.database.metadata.ColInfo;
import com.ds.common.database.metadata.TableInfo;
import com.ds.config.ListResultModel;
import com.ds.dsm.repository.db.col.ColView;
import com.ds.dsm.repository.db.ref.RefGridView;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.repository.database.ref.TableRef;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/dsm/repository/table/ref/")
@Controller
@ButtonViewsAnnotation()
@MethodChinaName(cname = "关联数据库")
public class TableMetaView {
    @CustomAnnotation(uid = true, hidden = true)
    private String tablename;
    @CustomAnnotation(uid = true, hidden = true)
    private String dmsId;

    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @RequestMapping(method = RequestMethod.POST, value = "Cols")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", caption = "字段列表")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<ColView>> getCols(String tablename, String projectId, String projectVersionName) {
        ListResultModel<List<ColView>> cols = new ListResultModel();
        try {
            TableInfo tableInfo = ESDFacrory.getESDClient().getTableInfoByFullName(tablename);
            List<ColInfo> colInfos = tableInfo.getColList();
            cols = PageUtil.getDefaultPageList(colInfos, ColView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    @MethodChinaName(cname = "关联关系")
    @RequestMapping(method = RequestMethod.POST, value = "Refs")
    @GridViewAnnotation()
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-databinder", caption = "关联关系")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<RefGridView>> getTableRefList(String tablename, String projectVersionName) {
        ListResultModel<List<RefGridView>> tables = new ListResultModel();
        try {
            DSMFactory factory = DSMFactory.getInstance(ESDFacrory.getESDClient().getSpace());
            List<TableRef> tableInfos = factory.getRepositoryManager().getTableRefByName(tablename, projectVersionName);
            tables = PageUtil.getDefaultPageList(tableInfos, RefGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return tables;
    }

    public String getDmsId() {
        return dmsId;
    }

    public void setDmsId(String dmsId) {
        this.dmsId = dmsId;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
