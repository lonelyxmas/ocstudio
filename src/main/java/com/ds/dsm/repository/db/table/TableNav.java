package com.ds.dsm.repository.db.table;

import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.BlockAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/dsm/repository/table/")
@NavGroupAnnotation(bottombarMenu = CustomFormMenu.Close)
@MethodChinaName(cname = "数据库表", imageClass = "spafont spa-icon-c-gallery")
public class TableNav {

    @CustomAnnotation(uid = true, hidden = true)
    public String tablename;

    @CustomAnnotation(pid = true, hidden = true)
    public String projectVersionName;

    @CustomAnnotation(uid = true, hidden = true)
    public String projectId;


    @MethodChinaName(cname = "库表信息")
    @RequestMapping(method = RequestMethod.POST, value = "TableInfo")
    @FormViewAnnotation(saveUrl = "db.table.updateTable")
    @UIAnnotation( height = "180")
    @ModuleAnnotation(caption = "库表信息", dock = Dock.top)
    @ResponseBody
    public ResultModel<TableView> getTableInfo(String projectVersionName, String projectId, String tablename) {
        ResultModel<TableView> result = new ResultModel<TableView>();
        return result;
    }

    @MethodChinaName(cname = "关联信息")
    @RequestMapping(method = RequestMethod.POST, value = "TableMeta")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, imageClass = "spafont spa-icon-c-databinder")
    @ResponseBody
    public ResultModel<TableMetaView> getDSMTableMetaInfo(String projectVersionName, String projectId, String tablename) {
        ResultModel<TableMetaView> result = new ResultModel<TableMetaView>();
        return result;

    }


    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
