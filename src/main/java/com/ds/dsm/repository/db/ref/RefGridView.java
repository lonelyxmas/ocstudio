package com.ds.dsm.repository.db.ref;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.custom.grid.enums.GridRowMenu;
import com.ds.esd.dsm.repository.database.ref.TableRef;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;
import com.ds.web.annotation.RefType;
import com.ds.web.annotation.Required;

@PageBar
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, rowMenu = {GridRowMenu.Delete})
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {RefService.class}, event = CustomGridEvent.editor)
public class RefGridView {


    @CustomAnnotation(uid = true, hidden = true)
    String refId;

    @CustomAnnotation(pid = true, hidden = true)
    String projectVersionName;


    @CustomAnnotation(caption = "表名", pid = true)
    String tablename;


    @CustomAnnotation(caption = "关联表", hidden = true)
    String otherTableName;
    @Required
    @CustomAnnotation(caption = "关系")
    RefType ref;

    @Required
    @CustomAnnotation(caption = "主键")
    String pkField;
    @Required
    @CustomAnnotation(caption = "外键")
    String fkField;
    @Required
    @CustomAnnotation(caption = "主键显示字段")
    String mainCaption;
    @Required
    @CustomAnnotation(caption = "外键显示字段")
    String otherCaption;


    RefGridView() {

    }


    public RefGridView(TableRef ref) {

        this.projectVersionName = ref.getProjectVersionName();
        this.ref = ref.getRef();
        this.tablename = ref.getTablename();
        this.otherTableName = ref.getOtherTableName();
        this.otherCaption = ref.getOtherCaption();
        this.fkField = ref.getFkField();
        this.pkField = ref.getPkField();
        this.refId = ref.getRefId();
        this.mainCaption = ref.getMainCaption();

    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getMainCaption() {
        return mainCaption;
    }

    public void setMainCaption(String mainCaption) {
        this.mainCaption = mainCaption;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getOtherTableName() {
        return otherTableName;
    }

    public void setOtherTableName(String otherTableName) {
        this.otherTableName = otherTableName;
    }

    public RefType getRef() {
        return ref;
    }

    public void setRef(RefType ref) {
        this.ref = ref;
    }

    public String getPkField() {
        return pkField;
    }

    public void setPkField(String pkField) {
        this.pkField = pkField;
    }

    public String getFkField() {
        return fkField;
    }

    public void setFkField(String fkField) {
        this.fkField = fkField;
    }

    public String getOtherCaption() {
        return otherCaption;
    }

    public void setOtherCaption(String otherCaption) {
        this.otherCaption = otherCaption;
    }
}
