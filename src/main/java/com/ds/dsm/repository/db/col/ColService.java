package com.ds.dsm.repository.db.col;

import com.ds.common.database.metadata.ColInfo;
import com.ds.common.database.metadata.MetadataFactory;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/repository/col/")
@MethodChinaName(cname = "字段配置", imageClass = "spafont spa-icon-c-comboinput")
public class ColService {


    @MethodChinaName(cname = "字段信息")
    @RequestMapping(method = RequestMethod.POST, value = "ColInfo")
    @FormViewAnnotation
    @DialogAnnotation( width = "480", height = "400")
    @ModuleAnnotation(caption = "字段信息")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor, CustomMenuItem.add})
    @ResponseBody
    public ResultModel<ColBaseView> getColInfo(String tableName, String projectId, String colname) {
        return new ResultModel<>();
    }
    @RequestMapping(method = RequestMethod.POST, value = "ColInfo")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.saveRow})
    @ResponseBody
    public ResultModel<Boolean> saveColInfo(@RequestBody ColInfo col) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            List<ColInfo> cols = new ArrayList<ColInfo>();
            cols.add(col);
            String configKey = col.getConfigKey();
            if (configKey == null || configKey.equals("")) {
                configKey = col.getUrl();
            }

            MetadataFactory factory = ESDFacrory.getESDClient().getDbFactory(configKey);
            factory.modifyTableCols(col.getTablename(), cols);
        } catch (Exception e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;

    }


}
