package com.ds.dsm.config.service;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.vfs.FileInfo;
import com.ds.vfs.Folder;
import com.ds.vfs.ct.CtVfsFactory;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Controller
@RequestMapping(value = {"/dsm/domain/config/package/"})
@MenuBarMenu(menuType = CustomMenuType.component, caption = "DSM菜单配置")
@Aggregation(type = AggregationType.menu, rootClass = AggPackageMenu.class)
public class AggPackageMenu {


    @CustomAnnotation(imageClass = "xuicon xui-icon-minus", index = 4, caption = "删除")
    @RequestMapping(value = {"delAggPackage"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    ResultModel<Boolean> delAggEntity(String domainId, String packageName, String folderPath) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (domainId != null) {
            try {
                Folder folder = CtVfsFactory.getCtVfsService().getFolderByPath(folderPath);
                List<FileInfo> fileInfoList = folder.getFileList();
                String[] esdClassNames = StringUtility.split(packageName, ";");
                Set classNameSet = new HashSet();
                for (FileInfo fileInfo : fileInfoList) {
                    classNameSet.add(fileInfo.getDescrition());
                }
                DSMFactory.getInstance().getAggregationManager().delAggEntity(domainId, classNameSet, true);
                CtVfsFactory.getCtVfsService().deleteFolder(folder.getID());
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
