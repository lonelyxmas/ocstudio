package com.ds.dsm.config;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityConfigTree;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.dsm.config.entity.AggDomainEntityConfigTree;
import com.ds.esd.custom.CustomViewFactory;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/domain/config/")
public class AggNavConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "DomainConfig")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "950", height = "680")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "领域配置")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<AggDsmConfigTree>> loadDomainList(String projectName) {
        TreeListResultModel<List<AggDsmConfigTree>> result = new TreeListResultModel<>();
        try {
            List<DomainInst> domainInstList = DSMFactory.getInstance().getAggregationManager().getDomainInstList(projectName);
            domainInstList.add(DSMFactory.getInstance().getAggregationManager().getDomainInstById(CustomViewFactory.DSMdsm));
            result = TreePageUtil.getTreeList(domainInstList, AggDsmConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;

    }




    @RequestMapping(method = RequestMethod.POST, value = "DomainEntityConfig")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "950", height = "680")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "实体配置")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public TreeListResultModel<List<AggDomainEntityConfigTree>> loadDomainEntityConfig(String projectName) {
        TreeListResultModel<List<AggDomainEntityConfigTree>> result = new TreeListResultModel<>();
        try {
            List<DomainInst> domainInstList = DSMFactory.getInstance().getAggregationManager().getDomainInstList(projectName);
            domainInstList.add(DSMFactory.getInstance().getAggregationManager().getDomainInstById(CustomViewFactory.DSMdsm));
            result = TreePageUtil.getTreeList(domainInstList, AggDomainEntityConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;

    }

}
