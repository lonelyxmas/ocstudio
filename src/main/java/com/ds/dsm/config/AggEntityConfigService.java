package com.ds.dsm.config;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityConfigTree;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/domain/config/entity/")
public class AggEntityConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "loadChildEntity")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggEntityConfigTree>> loadChildEntity(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<AggEntityConfigTree>> resultModel = TreePageUtil.getTreeList(Arrays.asList(AggEntityNavItem.values()), AggEntityConfigTree.class);
        return resultModel;
    }


}
