package com.ds.dsm.config.api;

import com.ds.config.TreeListResultModel;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.dsm.config.AggDsmConfigTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/domain/config/")
public class APIEntityConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "entityConfig")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggDsmConfigTree>> entityConfig(String sourceClassName, String domainId) {
        TreeListResultModel<List<AggDsmConfigTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(AggEntityNavItem.values()), AggDsmConfigTree.class);
        return result;
    }
}
