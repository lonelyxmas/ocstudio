package com.ds.dsm.config.api;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/domain/config/")
public class ViewEntityConfigService {


    @RequestMapping(method = RequestMethod.POST, value = "viewConfig")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> viewConfig(String sourceClassName, String domainId, String id) {
        TreeListResultModel<List<ViewConfigTree>> result = new TreeListResultModel<>();
        ESDClass esdClass = null;
        try {
            esdClass = DSMFactory.getInstance().getClassManager().getAggEntityByName(sourceClassName, domainId, false);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        result = TreePageUtil.getTreeList(Arrays.asList(esdClass), ViewConfigTree.class);
        return result;
    }

}
