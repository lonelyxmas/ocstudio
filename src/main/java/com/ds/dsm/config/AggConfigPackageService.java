package com.ds.dsm.config;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import com.ds.vfs.FileInfo;
import com.ds.vfs.Folder;
import com.ds.vfs.ct.CtVfsFactory;
import com.ds.web.APIConfig;
import com.ds.web.APIConfigFactory;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("/dsm/domain/config/package/")
public class AggConfigPackageService {


    @RequestMapping(method = RequestMethod.POST, value = "loadConfigFile")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<AggDsmConfigTree>> loadConfigFile(String folderPath, String domainId) {
        TreeListResultModel<List<AggDsmConfigTree>> result = new TreeListResultModel<>();
        try {
            Folder folder = CtVfsFactory.getCtVfsService().getFolderByPath(folderPath);
            List<APIConfig> objs = new ArrayList<>();
            for (FileInfo fileInfo : folder.getFileList()) {
                String className = fileInfo.getDescrition();
                if (className != null) {
                    try {
                        APIConfig apiConfig = APIConfigFactory.getInstance().getAPIConfig(className);
                        if (apiConfig != null) {
                            objs.add(apiConfig);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            result = TreePageUtil.getTreeList(objs, AggDsmConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "PackageEntityList")
    @GridViewAnnotation
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf", caption = "聚合实体")
    @ResponseBody
    public ListResultModel<List<AggEntityGridView>> getPackageEntityList(String folderPath, String packageName, String domainId) {
        ListResultModel<List<AggEntityGridView>> result = new ListResultModel();
        try {
            List<ESDClass> esdClassList = new ArrayList<>();
            Folder folder = CtVfsFactory.getCtVfsService().getFolderByPath(folderPath);
            for (FileInfo fileInfo : folder.getFileList()) {
                String className = packageName + "." + fileInfo.getName();
                ESDClass esdClass = DSMFactory.getInstance().getClassManager().getAggEntityByName(className, domainId, false);
                esdClassList.add(esdClass);
            }

            Collections.sort(esdClassList, new Comparator<ESDClass>() {
                public int compare(ESDClass o1, ESDClass o2) {
                    return o1.getClassName().compareTo(o2.getClassName());
                }
            });

            result = PageUtil.getDefaultPageList(esdClassList, AggEntityGridView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "删除实体关系")
    @RequestMapping(value = {"delAggEntity"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delAggEntity(String domainId, String sourceClassName) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        if (domainId != null) {
            try {
                String[] esdClassNames = StringUtility.split(sourceClassName, ";");
                Set classNameSet = new HashSet();
                for (String esdClassName : esdClassNames) {
                    if (esdClassName.indexOf(":") > -1) {
                        String className = StringUtility.split(esdClassName, ":")[0];
                        classNameSet.add(className);
                    } else {
                        classNameSet.add(esdClassName);
                    }
                }
                DSMFactory.getInstance().getAggregationManager().delAggEntity(domainId, classNameSet,true);
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


}
