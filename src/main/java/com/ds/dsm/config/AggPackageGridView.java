package com.ds.dsm.config;


import com.ds.common.util.StringUtility;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.vfs.Folder;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Delete}, customService = {AggDomainNavService.class})
public class AggPackageGridView {


    @Uid
    public String path;
    @Pid
    public String domainId;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "名称")
    public String name;

    @FieldAnnotation(required = true)
    @CustomAnnotation(caption = "包名")
    public String packageName;


    @CustomAnnotation(caption = "描述", hidden = true)
    private String desc;


    public AggPackageGridView(Folder folder, String parentPath, String domainId) {
        this.name = folder.getName();
        this.path = folder.getPath();
        this.desc = folder.getDescrition();
        this.packageName = StringUtility.replace(path, parentPath, "");
        packageName = packageName.substring(0, packageName.length() - 1);
        packageName = StringUtility.replace(packageName, "/", ".");
        this.domainId = domainId;


    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
