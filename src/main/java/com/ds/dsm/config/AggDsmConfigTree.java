package com.ds.dsm.config;

import com.ds.common.util.StringUtility;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityNavItem;
import com.ds.dsm.config.service.AggFileMenu;
import com.ds.dsm.config.service.AggPackageMenu;
import com.ds.dsm.editor.agg.menu.JavaAggToolBar;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.vfs.FileInfo;
import com.ds.vfs.Folder;
import com.ds.web.APIConfig;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@ToolBarMenu(menuClasses = JavaAggToolBar.class)
@TreeAnnotation(caption = "领域配置",
        size = 300, lazyLoad = true, heplBar = true)
public class AggDsmConfigTree extends TreeListItem {
    @Pid
    String projectVersionName;
    @Pid
    String domainId;
    @Pid
    String folderPath;
    @Pid
    String packageName;
    @Pid
    DSMType dsmType;
    @Pid
    String sourceClassName;


    @TreeItemAnnotation(imageClass = "spafont spa-icon-project", deepSearch = true, iniFold = false, dynLoad = true, lazyLoad = true, bindService = AggDomainNavService.class, dynDestory = true)
    public AggDsmConfigTree(DomainInst domainInst) {
        this.domainId = domainInst.getDomainId();
        this.caption = domainInst.getName();
        this.id = domainInst.getDomainId();
        this.dsmType = domainInst.getDsmType();
    }

    @RightContextMenu(menuClass = AggPackageMenu.class)
    @TreeItemAnnotation(imageClass = "spafont spa-icon-package", deepSearch = true, bindService = AggConfigPackageService.class, iniFold = true, lazyLoad = true, dynDestory = true)
    public AggDsmConfigTree(Folder folder, String parentPath, String domainId) {
        this.domainId = domainId;
        this.folderPath = folder.getPath();
        this.packageName = StringUtility.replace(folderPath, parentPath, "");
        packageName = packageName.substring(0, packageName.length() - 1);
        packageName = StringUtility.replace(packageName, "/", ".");
        this.caption = packageName;
        this.id = folder.getID();
    }


    @RightContextMenu(menuClass = AggFileMenu.class)
    @TreeItemAnnotation(imageClass = "spafont spa-icon-json-file", bindService = AggConfigFileService.class, lazyLoad = true, dynDestory = true)
    public AggDsmConfigTree(FileInfo fileInfo, String domainId) {
        this.caption = fileInfo.getName();
        this.sourceClassName = fileInfo.getDescrition();
        this.id = fileInfo.getID();
        this.domainId = domainId;
    }

    @TreeItemAnnotation(bindService = AggEntityConfigService.class, imageClass = "spafont spa-icon-class", lazyLoad = true, dynDestory = true, iniFold = true, caption = "视图配置")
    public AggDsmConfigTree(APIConfig apiConfig, String domainId) {
        this.id = domainId + "_" + apiConfig.getClassName();
        this.domainId = domainId;
        this.caption = apiConfig.getDesc() + "(" + apiConfig.getClassName() + ")";
        this.imageClass = apiConfig.getImageClass() == null ? "spafont spa-icon-class" : apiConfig.getImageClass();
        this.sourceClassName = apiConfig.getClassName();
    }


    @RightContextMenu(menuClass = AggFileMenu.class)
    @TreeItemAnnotation(customItems = AggEntityNavItem.class, lazyLoad = true, dynDestory = true)
    public AggDsmConfigTree(AggEntityNavItem aggEntityNavItem, String sourceClassName, String domainId) {
        this.sourceClassName = sourceClassName;
        this.domainId = domainId;
        this.id = aggEntityNavItem.name();
        this.caption = aggEntityNavItem.getName();
        this.imageClass = aggEntityNavItem.getImageClass();
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

}
