package com.ds.dsm.config.entity;

import com.ds.common.util.StringUtility;
import com.ds.dsm.config.service.AggPackageMenu;
import com.ds.dsm.editor.agg.menu.JavaAggToolBar;
import com.ds.dsm.view.config.service.ViewConfigNavService;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.vfs.Folder;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@ToolBarMenu(menuClasses = JavaAggToolBar.class)
@TreeAnnotation(caption = "领域配置",
        size = 300, lazyLoad = true, heplBar = true)
public class AggDomainEntityConfigTree extends TreeListItem {
    @Pid
    String projectVersionName;
    @Pid
    String domainId;
    @Pid
    String folderPath;
    @Pid
    String rootPath;
    @Pid
    String packageName;
    @Pid
    DSMType dsmType;
    @Pid
    String sourceClassName;


    @TreeItemAnnotation(imageClass = "spafont spa-icon-project", deepSearch = true, iniFold = false, dynLoad = true, lazyLoad = true, bindService = AggDomainEntityNavService.class, dynDestory = true)
    public AggDomainEntityConfigTree(DomainInst domainInst, String rootPath) {
        this.domainId = domainInst.getDomainId();
        this.caption = domainInst.getName();
        this.id = domainInst.getDomainId();
        this.dsmType = domainInst.getDsmType();
        this.rootPath = rootPath;
    }

    @RightContextMenu(menuClass = AggPackageMenu.class)
    @TreeItemAnnotation(imageClass = "spafont spa-icon-package", deepSearch = true, bindService = AggEntityPackageService.class, iniFold = true, lazyLoad = true, dynDestory = true)
    public AggDomainEntityConfigTree(Folder folder, String rootPath, String domainId) {
        this.domainId = domainId;
        this.folderPath = folder.getPath();
        this.rootPath = rootPath;
        this.packageName = StringUtility.replace(folderPath, rootPath, "");
        packageName = packageName.substring(0, packageName.length() - 1);
        packageName = StringUtility.replace(packageName, "/", ".");
        this.caption = packageName;
        this.id = folder.getID();
    }


    @TreeItemAnnotation(bindService = ViewConfigNavService.class, imageClass = "spafont spa-icon-class", lazyLoad = true, dynDestory = true, iniFold = true, caption = "视图配置")
    public AggDomainEntityConfigTree(ESDClass esdClass, String domainId) {
        this.id = domainId + "_" + esdClass.getClassName();
        this.domainId = domainId;
        this.caption = esdClass.getDesc() + "(" + esdClass.getClassName() + ")";
        this.imageClass = esdClass.getImageClass() == null ? "spafont spa-icon-class" : esdClass.getImageClass();
        this.sourceClassName = esdClass.getClassName();

    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }


    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public DSMType getDsmType() {
        return dsmType;
    }

    public void setDsmType(DSMType dsmType) {
        this.dsmType = dsmType;
    }

    public String getProjectVersionName() {
        return projectVersionName;
    }

    public void setProjectVersionName(String projectVersionName) {
        this.projectVersionName = projectVersionName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

}
