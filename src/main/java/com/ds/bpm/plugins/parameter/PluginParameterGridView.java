package com.ds.bpm.plugins.parameter;

import com.ds.config.CParameter;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {BPDParameterManager.class}, event = CustomGridEvent.editor)
public class PluginParameterGridView {

    @CustomAnnotation(uid = true, hidden = true)
    private String parameterId;
    @CustomAnnotation(pid = true, hidden = true)
    private String pluginId;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;


    @MethodChinaName(cname = "显示名称")
    private String desc;

    @MethodChinaName(cname = "参数名称")
    private String name;

    @MethodChinaName(cname = "参数值")
    private String parameterValue;


    public PluginParameterGridView() {

    }

    public PluginParameterGridView(CParameter parameter) {
        this.projectId = parameter.getProjectId();
        this.pluginId = parameter.getPluginId();
        this.parameterId = parameter.getParameterId();
        this.name = parameter.getName();
        this.desc = parameter.getDesc();
        this.parameterValue = parameter.getParameterValue();
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getParameterId() {
        return parameterId;
    }

    public void setParameterId(String parameterId) {
        this.parameterId = parameterId;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public String getParameterValue() {
        return parameterValue;
    }

}
