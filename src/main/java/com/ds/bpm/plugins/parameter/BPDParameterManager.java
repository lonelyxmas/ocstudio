package com.ds.bpm.plugins.parameter;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

;

@Controller
@RequestMapping(path = "/admin/bpd/parameter/")
@MethodChinaName(cname = "扩展参数", imageClass = "spafont spa-icon-tools")

public class BPDParameterManager {

    @RequestMapping(method = RequestMethod.POST, value = "AddParameters")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add})
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350")
    @ModuleAnnotation( caption = "添加参数")
    @ResponseBody
    public ResultModel<PluginParameterFormView> addParamter(String projectId, String pluginId) {
        ResultModel<PluginParameterFormView> result = new ResultModel<PluginParameterFormView>();
        try {
            BPDPlugin plugin = this.getPluginById(projectId, pluginId);
            CParameter cParameter = new CParameter();
            cParameter.setPluginId(pluginId);
            cParameter.setProjectId(projectId);
            cParameter.setParameterId(UUID.randomUUID().toString());
            cParameter.setPluginId(pluginId);
            plugin.putParameter(cParameter.getParameterId(), cParameter);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ParameterInfo")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor})
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350")
    @ModuleAnnotation( caption = "编辑参数")
    @ResponseBody
    public ResultModel<PluginParameterFormView> getParamterInfo(String projectId, String pluginId, String parameterId) {
        ResultModel<PluginParameterFormView> result = new ResultModel<PluginParameterFormView>();

        return result;
    }


    @MethodChinaName(cname = "保存参数")
    @RequestMapping(method = RequestMethod.POST, value = "saveParameters")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save})
    @ResponseBody
    public ResultModel<Boolean> saveParamter(@RequestBody PluginParameterFormView parameter) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        String pluginId = parameter.getPluginId();
        String parameterId = parameter.getParameterId();
        BPDPlugin plugin = null;
        try {
            plugin = this.getPluginById(parameter.getProjectId(), parameter.getPluginId());
            CParameter cParameter = new CParameter();
            cParameter.setPluginId(parameter.getPluginId());
            cParameter.setProjectId(parameter.getProjectId());
            if (parameterId == null || parameterId.equals("")) {
                cParameter.setParameterId(UUID.randomUUID().toString());
            } else {
                cParameter.setParameterId(parameterId);
            }
            cParameter.setPluginId(pluginId);
            cParameter.setName(parameter.getName());
            cParameter.setParameterValue(parameter.getParameterValue());
            cParameter.setDesc(parameter.getDesc());
            plugin.putParameter(cParameter.getParameterId(), cParameter);

            BPDProjectConfig bpdProjectConfig =   BPMFactory.getInstance().getProjectConfig(plugin.getProjectId());
            BPMFactory.getInstance().updateProjectConfig(bpdProjectConfig);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }


        return result;
    }


    @MethodChinaName(cname = "删除参数")
    @RequestMapping(method = RequestMethod.POST, value = "delParameters")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = {CustomMenuItem.delete})
    @ResponseBody
    public ResultModel<Boolean> deleteParams(String parameterId, String projectId, String pluginId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        String[] idarr = StringUtility.split(parameterId, ";");
        BPDPlugin plugin = null;
        try {
            plugin = this.getPluginById(projectId, pluginId);
            for (String id : idarr) {
                plugin.getParameters().remove(id);
            }
            BPDProjectConfig bpdProjectConfig =  BPMFactory.getInstance().getProjectConfig(plugin.getProjectId());
            BPMFactory.getInstance().updateProjectConfig(bpdProjectConfig);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }


        return result;
    }


    BPDPlugin getPluginById(String projectId, String pluginId) throws JDSException {
        BPDProjectConfig config =  BPMFactory.getInstance().getProjectConfig(projectId);
        BPDPlugin plugin = config.getPluginById(pluginId);
        return plugin;
    }

}
