package com.ds.bpm.plugins;


import com.ds.bpm.plugins.bpd.PluginInfoFormView;
import com.ds.bpm.plugins.bpd.PluginInfoGridView;
import com.ds.bpm.plugins.bpd.PluginNav;
import com.ds.bpm.plugins.view.ProjectListView;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.project.enums.ProjectDefAccess;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

;

@Controller
@RequestMapping(path = "/admin/bpd/")
@MethodChinaName(cname = "工作流配置", imageClass = "bpmfont bpm-gongzuoliu-moxing")
public class PluginIndex {


    @MethodChinaName(cname = "工作流插件")
    @RequestMapping("Plugins")
    @GridViewAnnotation(editorPath = "PluginNav", delPath = "deletePlugins")
    @ModuleAnnotation(imageClass = "bpmfont bpm-gongzuoliu-moxing", caption = "工作流插件")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<PluginInfoGridView>> getProcessPlugins(String projectName, PluginType pluginType) {
        BPDProjectConfig config = null;
        ListResultModel<List<PluginInfoGridView>> result = new ListResultModel<List<PluginInfoGridView>>();
        List<BPDPlugin> bpdElements = new ArrayList<BPDPlugin>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            config = this.getConfig(version.getProject().getId());
            if (config != null) {
                List<BPDPlugin> cbpdElements = config.getBpdElementsList();
                for (BPDPlugin plugin : cbpdElements) {
                    if (plugin.getPluginType() != null && plugin.getPluginType().equals(pluginType)) {
                        plugin.setProjectId(version.getProject().getId());
                        bpdElements.add(plugin);
                    }
                }
                result = PageUtil.getDefaultPageList(bpdElements, PluginInfoGridView.class);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }

    BPDProjectConfig getConfig(String projectId) throws JDSException {
        BPDProjectConfig config = BPMFactory.getInstance().getProjectConfig(projectId);
        return config;
    }


    @RequestMapping(method = RequestMethod.POST, value = "index")
    @GridViewAnnotation(editorPath = "ConfigNav")
    @ModuleAnnotation(caption = "导航页")
    @ResponseBody
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.index)
    public ListResultModel<List<ProjectListView>> projectList(String projectName) {
        ListResultModel<List<ProjectListView>> result = new ListResultModel<List<ProjectListView>>();
        List<ProjectListView> views = new ArrayList<ProjectListView>();
        try {
            try {
                BPMFactory.getInstance().initFactory();

            } catch (JDSException e) {
                e.printStackTrace();
            }
            List<Project> projects = ESDFacrory.getESDClient().getAllProject(ProjectDefAccess.Public);
            result = PageUtil.getDefaultPageList(projects, ProjectListView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }


    @MethodChinaName(cname = "插件信息")
    @RequestMapping(method = RequestMethod.POST, value = "PluginNav")
    @NavGroupViewAnnotation(saveUrl = "updatePlugsInfo")
    @ModuleAnnotation(caption = "插件信息")
    @DialogAnnotation
    @ResponseBody
    public ResultModel<PluginNav> pluginInfo(String projectId, String pluginId) {
        ResultModel<PluginNav> result = new ResultModel<PluginNav>();
        return result;
    }


    @MethodChinaName(cname = "更新插件信息")
    @RequestMapping(method = RequestMethod.POST, value = "updatePlugsInfo")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<PluginInfoFormView> updatePlugsInfo(@RequestBody PluginInfoFormView pluginView) {
        String pluginId = pluginView.getPluginId();
        ResultModel<PluginInfoFormView> result = new ResultModel<PluginInfoFormView>();
        String projectId = pluginView.getProjectId();
        try {
            BPDProjectConfig bpdProjectConfig =  BPMFactory.getInstance().getProjectConfig(projectId);
            BPDPlugin plugin = null;
            if (pluginId == null || pluginId.equals("")) {
                plugin = new BPDPlugin();
                plugin.setPluginId(UUID.randomUUID().toString());
                bpdProjectConfig.getBpdElementsList().add(plugin);
            } else {
                plugin = bpdProjectConfig.getPluginById(pluginId);
                if (plugin == null) {
                    plugin = new BPDPlugin();
                    plugin.setPluginId(pluginId);
                    bpdProjectConfig.getBpdElementsList().add(plugin);
                }
            }

            plugin.setName(pluginView.getName());
            plugin.setDisplayname(pluginView.getDisplayname());
            plugin.setHeight(pluginView.getPanelHeight());
            plugin.setWidth(pluginView.getWidth());
            plugin.setImplementation(pluginView.getImplementation());
            plugin.setPluginType(pluginView.getPluginType());
            plugin.setActivityType(pluginView.getActivityType());
            plugin.setStatus(pluginView.getStatus());
            plugin.setProjectId(pluginView.getProjectId());
            BPMFactory.getInstance().updateProjectConfig(bpdProjectConfig);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }

    @MethodChinaName(cname = "deletePlugins")
    @APIEventAnnotation(callback = {CustomCallBack.Reload})
    @RequestMapping(method = RequestMethod.POST, value = "deletePlugins")
    @ResponseBody
    public ResultModel<Boolean> deletePlugins(String projectId, String pluginId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            BPDProjectConfig bpdProjectConfig =  BPMFactory.getInstance().getProjectConfig(projectId);
            List<String> idarr = Arrays.asList(StringUtility.split(pluginId, ";"));
            List<BPDPlugin> plugins = new ArrayList<>();
            for (BPDPlugin plugin : bpdProjectConfig.getBpdElementsList()) {
                if (!idarr.contains(plugin.getPluginId())) {
                    plugins.add(plugin);
                }
            }
            bpdProjectConfig.setBpdElementsList(plugins);
            BPMFactory.getInstance().updateProjectConfig(bpdProjectConfig);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }


        return result;
    }


    BPDPlugin getPluginById(String projectId, String pluginId) throws JDSException {
        BPDProjectConfig config = BPMFactory.getInstance().getProjectConfig(projectId);
        BPDPlugin plugin = config.getPluginById(pluginId);
        return plugin;
    }

}
