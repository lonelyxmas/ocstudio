package com.ds.bpm.plugins.listener;

import com.ds.config.CListener;
import com.ds.config.ListenerType;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {ListenerManager.class}, event = CustomGridEvent.editor)
public class ListenerGridView {


    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;


    @CustomAnnotation(uid = true, hidden = true)
    private String listenerId;


    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "监听器类型", disabled = true)
    private ListenerType listenerType;

    @MethodChinaName(cname = "监听器名称")
    private String name;


    @MethodChinaName(cname = "实现类")
    private String implementation;


    public String getListenerId() {
        return listenerId;
    }

    public void setListenerId(String listenerId) {
        this.listenerId = listenerId;
    }

    public ListenerGridView() {

    }

    public ListenerGridView(CListener listener) {
        this.listenerId = listener.getListenerId();
        this.projectId = listener.getProjectId();
        this.name = listener.getName();
        this.implementation = listener.getImplementation();
    }

    public ListenerType getListenerType() {
        return listenerType;
    }

    public void setListenerType(ListenerType listenerType) {
        this.listenerType = listenerType;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImplementation() {
        return implementation;
    }

    public void setImplementation(String implementation) {
        this.implementation = implementation;
    }
}
