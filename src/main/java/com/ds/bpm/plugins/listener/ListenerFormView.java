package com.ds.bpm.plugins.listener;

import com.ds.config.CListener;
import com.ds.config.ListenerType;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(col = 1, stretchHeight = StretchType.last, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {ListenerManager.class})
public class ListenerFormView {


    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;


    @CustomAnnotation(uid = true, hidden = true)
    private String listenerId;


    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation( caption = "监听器类型", disabled = true)
    private ListenerType listenerType;

    @MethodChinaName(cname = "监听器名称")
    @Required
    @CustomAnnotation(uid = true )
    private String name;


    @MethodChinaName(cname = "实现类")
    @CustomAnnotation()
    @Required
    private String implementation;


    public String getListenerId() {
        return listenerId;
    }

    public void setListenerId(String listenerId) {
        this.listenerId = listenerId;
    }

    public ListenerFormView() {

    }

    public ListenerFormView(CListener listener) {
        this.listenerId = listener.getListenerId();
        this.projectId = listener.getProjectId();
        this.name = listener.getName();
        this.implementation = listener.getImplementation();
    }

    public ListenerType getListenerType() {
        return listenerType;
    }

    public void setListenerType(ListenerType listenerType) {
        this.listenerType = listenerType;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImplementation() {
        return implementation;
    }

    public void setImplementation(String implementation) {
        this.implementation = implementation;
    }
}
