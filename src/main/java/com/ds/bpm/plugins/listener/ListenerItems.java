package com.ds.bpm.plugins.listener;

import com.ds.config.ListenerType;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum ListenerItems implements TreeItem {
    EXPRESSION(ListenerType.EXPRESSION, ListenerManager.class, false, false, false),
    ACTIVITY(ListenerType.ACTIVITY, ListenerManager.class, false, false, false),
    PROCESS(ListenerType.PROCESS, ListenerManager.class, true, true, true),
    RIGHT(ListenerType.RIGHT, ListenerManager.class, false, false, false);
    private final String imageClass;
    private final String name;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;
    private final ListenerType listenerType;

    ListenerItems(ListenerType listenerType, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.listenerType = listenerType;
        this.name = listenerType.getName();
        this.imageClass = listenerType.getImageClass();
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    public ListenerType getListenerType() {
        return listenerType;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
