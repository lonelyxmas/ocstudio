package com.ds.bpm.plugins.bpd;


import com.ds.common.JDSException;
import com.ds.config.BPDPlugin;
import com.ds.config.BPDProjectConfig;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.GroupItemAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

@RequestMapping("/admin/bpd/")
@Controller
@BottomBarMenu
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class PluginNav {

    @CustomAnnotation(uid = true, hidden = true)
    private String pluginId;

    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;


    public PluginNav() {


    }


    @RequestMapping(method = RequestMethod.POST, value = "PluginInfo")
    @FormViewAnnotation(saveUrl = "updatePlugsInfo")
    @ModuleAnnotation(caption = "基本信息", dock = Dock.top)
    @UIAnnotation(height = "160")
    @ResponseBody
    @APIEventAnnotation(autoRun = true)
    public ResultModel<PluginInfoFormView> getPluginInfo(String projectId, String name) {
        ResultModel<PluginInfoFormView> result = new ResultModel<PluginInfoFormView>();
        try {
            BPDProjectConfig config = this.getConfig(projectId);
            BPDPlugin element = null;
            if (name != null && !name.equals("")) {
                element = config.getElementByName(name);
            } else {
                element = new BPDPlugin();
                element.setPluginId(UUID.randomUUID().toString());
            }
            PluginInfoFormView info = new PluginInfoFormView(element);
            result.setData(info);
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value = "PluginMetaInfo")
    @DynLoadAnnotation
    @NavTabsViewAnnotation
    @ModuleAnnotation(caption = "详细信息", dock = Dock.fill)
    @ResponseBody
    public ResultModel<PluginMetaView> getPluginMetaInfo(String projectId, String pluginId) {
        ResultModel<PluginMetaView> result = new ResultModel<PluginMetaView>();

        return result;

    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    BPDProjectConfig getConfig(String projectId) throws JDSException {
        BPDProjectConfig config =  BPMFactory.getInstance().getProjectConfig(projectId);
        return config;
    }

}
