package com.ds.bpm.plugins.bpd;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.config.ActivityDefImpl;
import com.ds.config.BPDPlugin;
import com.ds.config.PluginType;
import com.ds.enums.ServiceStatus;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(customService = BPDPluginService.class)
public class PluginInfoFormView {


    @CustomAnnotation(uid = true, hidden = true)
    private String pluginId;

    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @MethodChinaName(cname = "名称")
    @CustomAnnotation()
    @Required
    private String name;

    @MethodChinaName(cname = "状态")
    @CustomAnnotation()
    @Required
    private ServiceStatus status;

    @MethodChinaName(cname = "类型")
    @CustomAnnotation()
    @Required
    private ActivityDefImpl activityType;


    @MethodChinaName(cname = "插件类型")
    @CustomAnnotation()
    @Required
    private PluginType pluginType;

    @MethodChinaName(cname = "显示名称")
    @CustomAnnotation()
    @Required
    private String displayname;

    @MethodChinaName(cname = "窗口高度")
    @JSONField(name = "panelHeight")
    private Integer panelHeight = 350;

    @MethodChinaName(cname = "窗口宽度")
    private Integer width = 400;


    @MethodChinaName(cname = "实现类")
    @CustomAnnotation()
    @Required
    private String implementation;

    public PluginInfoFormView() {

    }

    public PluginInfoFormView(BPDPlugin element) {
        this.projectId = element.getProjectId();
        this.pluginId = element.getPluginId();
        this.name = element.getName();
        this.activityType = element.getActivityType() == null ? ActivityDefImpl.Process : element.getActivityType();
        this.implementation = element.getImplementation();
        this.pluginType = element.getPluginType();
        this.displayname = element.getDisplayname() == null ? element.getName() : element.getDisplayname();
        this.panelHeight = element.getHeight();
        this.width = element.getWidth();
        this.status = element.getStatus();
    }

    public ServiceStatus getStatus() {
        return status;
    }

    public void setStatus(ServiceStatus status) {
        this.status = status;
    }

    public PluginType getPluginType() {
        return pluginType;
    }

    public void setPluginType(PluginType pluginType) {
        this.pluginType = pluginType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ActivityDefImpl getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityDefImpl activityType) {
        this.activityType = activityType;
    }

    public String getImplementation() {
        return implementation;
    }

    public void setImplementation(String implementation) {
        this.implementation = implementation;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public Integer getPanelHeight() {
        return panelHeight;
    }

    public void setPanelHeight(Integer panelHeight) {
        this.panelHeight = panelHeight;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }
}
