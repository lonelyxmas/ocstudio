package com.ds.bpm.plugins.bpd;

import com.ds.config.PluginType;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum PluginsItems implements TreeItem {
    Process(PluginType.Process, BPDPluginService.class, false, false, false),
    Activity(PluginType.Activity, BPDPluginService.class, false, false, false),
    Classification(PluginType.Classification, BPDPluginService.class, true, true, true),
    Route(PluginType.Route, BPDPluginService.class, false, false, false);
    private final String imageClass;
    private final String name;
    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;
    private final PluginType pluginType;

    PluginsItems(PluginType pluginType, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.pluginType = pluginType;
        this.name = pluginType.getName();
        this.imageClass = pluginType.getImageClass();
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    public PluginType getPluginType() {
        return pluginType;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
