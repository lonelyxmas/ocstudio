package com.ds.bpm.plugins.bpd;

import com.ds.config.PluginType;
import com.ds.esd.custom.annotation.NavTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation()
@TreeAnnotation(caption = "工作流插件")
@NavTreeAnnotation(caption = "")
public class PluginTypeTree extends TreeListItem {

    @Pid
    String projectName;

    @Pid
    PluginType pluginType;

    @TreeItemAnnotation(customItems = PluginsItems.class, iniFold = false)
    public PluginTypeTree(PluginsItems pluginsItem, String projectName, String className) {
        this.caption = pluginsItem.getName();
        this.imageClass = pluginsItem.getImageClass();
        this.pluginType = pluginsItem.getPluginType();
        this.id = pluginsItem.getType();
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public PluginType getPluginType() {
        return pluginType;
    }

    public void setPluginType(PluginType pluginType) {
        this.pluginType = pluginType;
    }
}
