package com.ds.bpm.plugins.bpd;

import com.ds.bpm.plugins.extendatt.ExtendAttributeGridView;
import com.ds.bpm.plugins.formula.BPDFormulaGridView;
import com.ds.bpm.plugins.formula.BPDFormulaView;
import com.ds.bpm.plugins.parameter.PluginParameterGridView;
import com.ds.common.JDSException;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RequestMapping("/admin/bpd/")
@Controller
@ButtonViewsAnnotation()
public class PluginMetaView {


    @CustomAnnotation(uid = true, hidden = true)
    private String pluginId;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    public PluginMetaView() {

    }

    @MethodChinaName(cname = "公式面板")
    @RequestMapping("PluginFormulas")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-function", caption = "公式面板")
    @CustomAnnotation(index = 0)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<BPDFormulaGridView>> getPluginFormulas(String projectId, String pluginId) {

        ListResultModel<List<BPDFormulaGridView>> result = new ListResultModel<List<BPDFormulaGridView>>();
        List<CFormula> bpdElements = new ArrayList<CFormula>();
        try {
            BPDPlugin plugin = this.getPluginById(projectId, pluginId);
            if (plugin != null) {
                Map<String, CFormula> cbpdElementMap = plugin.getFormulaTypeMap();
                Set<String> keySet = cbpdElementMap.keySet();
                for (String key : keySet) {
                    CFormula element = cbpdElementMap.get(key);
                    bpdElements.add(element);
                }
                result = PageUtil.getDefaultPageList(bpdElements, BPDFormulaGridView.class);

            }
        } catch (JDSException e) {
            result = new ErrorListResultModel();
            ((ErrorListResultModel<List<BPDFormulaGridView>>) result).setErrdes(e.getMessage());
        }

        return result;

    }

    @MethodChinaName(cname = "扩展属性")
    @RequestMapping("ExtendAttributes")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-app", caption = "扩展属性")
    @CustomAnnotation(index = 1)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<ExtendAttributeGridView>> getExtendAttributes(String projectId, String pluginId) {
        ListResultModel<List<ExtendAttributeGridView>> result = new ListResultModel<List<ExtendAttributeGridView>>();
        List<CExtendedAttribute> bpdElements = new ArrayList<CExtendedAttribute>();
        try {
            BPDPlugin plugin = this.getPluginById(projectId, pluginId);
            if (plugin != null) {
                Map<String, CExtendedAttribute> cbpdElementMap = plugin.getExtendedAttributes();
                Set<String> keySet = cbpdElementMap.keySet();
                for (String key : keySet) {
                    CExtendedAttribute element = cbpdElementMap.get(key);
                    bpdElements.add(element);
                }
                result = PageUtil.getDefaultPageList(bpdElements, ExtendAttributeGridView.class);

            }
        } catch (JDSException e) {
            result = new ErrorListResultModel();
            ((ErrorListResultModel<List<ExtendAttributeGridView>>) result).setErrdes(e.getMessage());
        }

        return result;

    }


    @MethodChinaName(cname = "参数")
    @RequestMapping("Parameters")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-com3", caption = "参数")
    @CustomAnnotation(index = 2)
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<PluginParameterGridView>> getParameters(String projectId, String pluginId) {

        ListResultModel<List<PluginParameterGridView>> result = new ListResultModel<List<PluginParameterGridView>>();
        List<CParameter> bpdElements = new ArrayList<CParameter>();
        try {
            BPDPlugin plugin = this.getPluginById(projectId, pluginId);
            if (plugin != null) {
                Map<String, CParameter> cbpdElementMap = plugin.getParameters();
                Set<String> keySet = cbpdElementMap.keySet();
                for (String key : keySet) {
                    CParameter element = cbpdElementMap.get(key);
                    bpdElements.add(element);
                }
                result = PageUtil.getDefaultPageList(bpdElements, PluginParameterGridView.class);

            }
        } catch (JDSException e) {
            result = new ErrorListResultModel();
            ((ErrorListResultModel<List<PluginParameterGridView>>) result).setErrdes(e.getMessage());
        }

        return result;

    }


    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }


    BPDPlugin getPluginById(String projectId, String pluginId) throws JDSException {
        BPDProjectConfig config =   BPMFactory.getInstance().getProjectConfig(projectId);
        BPDPlugin plugin = config.getPluginById(pluginId);
        return plugin;
    }

}
