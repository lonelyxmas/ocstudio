package com.ds.bpm.plugins.extendatt;


import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

;

@Controller
@RequestMapping(path = "/admin/bpd/attribute/")
@MethodChinaName(cname = "扩展属性", imageClass = "spafont spa-icon-tools")
public class ExtendAttributeManager {


    @RequestMapping(method = RequestMethod.POST, value = "AddExtendAttributes")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add})
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350")
    @ModuleAnnotation(caption = "添加扩展属性")
    @ResponseBody
    public ResultModel<ExtendAttributeFormView> addAttribute(String projectId, String pluginId) {
        ResultModel<ExtendAttributeFormView> result = new ResultModel<ExtendAttributeFormView>();

        CExtendedAttribute cExtendedAttribute = new CExtendedAttribute();
        cExtendedAttribute.setPluginId(pluginId);
        cExtendedAttribute.setProjectId(projectId);
        cExtendedAttribute.setAttributeId(UUID.randomUUID().toString());
        result.setData(new ExtendAttributeFormView(cExtendedAttribute));

        return result;

    }


    @MethodChinaName(cname = "保存扩展属性")
    @RequestMapping(method = RequestMethod.POST, value = "saveAttribute")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save})
    @ResponseBody
    public ResultModel<Boolean> saveAttribute(@RequestBody ExtendAttributeFormView attribute) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        String pluginId = attribute.getPluginId();
        String cExtendedAttributeId = attribute.getAttributeId();
        BPDPlugin plugin = null;
        try {
            plugin = this.getPluginById(attribute.getProjectId(), attribute.getPluginId());
            CExtendedAttribute cExtendedAttribute = new CExtendedAttribute();
            cExtendedAttribute.setPluginId(attribute.getPluginId());
            cExtendedAttribute.setProjectId(attribute.getProjectId());
            if (cExtendedAttributeId == null || cExtendedAttributeId.equals("")) {
                cExtendedAttribute.setAttributeId(UUID.randomUUID().toString());
            } else {
                cExtendedAttribute.setAttributeId(cExtendedAttributeId);
            }
            cExtendedAttribute.setType(attribute.getAttType());
            cExtendedAttribute.setName(attribute.getName());
            cExtendedAttribute.setValue(attribute.getAttValue());

            plugin.getExtendedAttributes().put(cExtendedAttribute.getAttributeId(), cExtendedAttribute);
            BPDProjectConfig bpdProjectConfig = BPMFactory.getInstance().getProjectConfig(plugin.getProjectId());
            BPMFactory.getInstance().updateProjectConfig(bpdProjectConfig);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }


        return result;

    }

    @RequestMapping(method = RequestMethod.POST, value = "ExtendAttributeInfo")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor})
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350")
    @ModuleAnnotation(caption = "编辑扩展属性")
    @ResponseBody
    public ResultModel<ExtendAttributeFormView> getAttributeInfo() {
        ResultModel<ExtendAttributeFormView> result = new ResultModel<ExtendAttributeFormView>();
        return result;
    }


    @MethodChinaName(cname = "delExtendAttributes")
    @RequestMapping(method = RequestMethod.POST, value = "delExtendAttributes")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = {CustomMenuItem.delete})
    @ResponseBody
    public ResultModel<Boolean> delExtendAttributes(String attributeId, String projectId, String pluginId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        String[] idarr = StringUtility.split(attributeId, ";");
        BPDPlugin plugin = null;
        try {
            plugin = this.getPluginById(projectId, pluginId);
            for (String id : idarr) {
                plugin.getExtendedAttributes().remove(id);
            }
            BPDProjectConfig bpdProjectConfig =  BPMFactory.getInstance().getProjectConfig(plugin.getProjectId());
            BPMFactory.getInstance().updateProjectConfig(bpdProjectConfig);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }

        return result;
    }


    BPDPlugin getPluginById(String projectId, String pluginId) throws JDSException {
        BPDProjectConfig config =  BPMFactory.getInstance().getProjectConfig(projectId);
        BPDPlugin plugin = config.getPluginById(pluginId);
        return plugin;
    }

}
