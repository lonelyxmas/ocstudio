package com.ds.bpm.plugins.extendatt;

import com.ds.config.CExtendedAttribute;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.web.annotation.Required;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, event = CustomGridEvent.editor, customService = {ExtendAttributeManager.class})
public class ExtendAttributeGridView {

    @CustomAnnotation(uid = true, hidden = true)
    private String attributeId;
    @CustomAnnotation(pid = true, hidden = true)
    private String pluginId;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @MethodChinaName(cname = "属性名称")
    private String name;

    @MethodChinaName(cname = "属性值")
    private String attValue;

    @MethodChinaName(cname = "属性类型")
    private String attType;

    ExtendAttributeGridView() {

    }

    public ExtendAttributeGridView(CExtendedAttribute attribute) {
        this.projectId = attribute.getProjectId();
        this.pluginId = attribute.getPluginId();
        this.attributeId = attribute.getAttributeId();
        this.name = attribute.getName();
        this.attValue = attribute.getValue();
        this.attType = attribute.getType();
    }


    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getAttType() {
        return attType;
    }

    public void setAttType(String attType) {
        this.attType = attType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getAttValue() {
        return attValue;
    }

    public void setAttValue(String attValue) {
        this.attValue = attValue;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }
}
