package com.ds.bpm.plugins.nav;

import com.ds.bpm.plugins.listener.ListenerItems;
import com.ds.config.ListenerType;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.NavFoldingTreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.web.annotation.Pid;

@TabsAnnotation()
@TreeAnnotation()
@NavFoldingTreeAnnotation(caption = "流程监听器")
public class ListenerTypeTree extends TreeListItem {
    @Pid
    String projectName;

    @Pid
    ListenerType listenerType;

    @TreeItemAnnotation(customItems = ListenerItems.class)
    public ListenerTypeTree(ListenerItems items, String projectName, String className) {
        this.caption = items.getName();
        this.imageClass = items.getImageClass();
        this.listenerType=items.getListenerType();
        this.projectName = projectName;
        this.setClassName(className);
        this.setId(items.getType());
        this.setIniFold(false);
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public ListenerType getListenerType() {
        return listenerType;
    }

    public void setListenerType(ListenerType listenerType) {
        this.listenerType = listenerType;
    }

}
