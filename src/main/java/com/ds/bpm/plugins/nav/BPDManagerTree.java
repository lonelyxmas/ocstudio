package com.ds.bpm.plugins.nav;

import com.ds.bpm.plugins.bpd.PluginTypeTree;
import com.ds.bpm.plugins.bpd.PluginsItems;
import com.ds.bpm.plugins.listener.ListenerItems;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.TreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutItemAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.PosType;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/admin/bpd/")
@MethodChinaName(cname = "流程定义配置", imageClass = "bpmfont bpm-gongzuoliu-moxing")
@LayoutAnnotation(transparent = false, items = {@LayoutItemAnnotation(size = 260, pos = PosType.before, locked = true, moveDisplay = false, cmd = false)})
public class BPDManagerTree {
    @RequestMapping(method = RequestMethod.POST, value = "PluginNavTree")
    @ModuleAnnotation(dynLoad = true, caption = "工作流插件", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @TreeViewAnnotation
    @ResponseBody
    public TreeListResultModel<List<PluginTypeTree>> getPlugins(String id, String projectVersionName) {
        TreeListResultModel<List<PluginTypeTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(PluginsItems.values()), PluginTypeTree.class);
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ListenerNavTree")
    @TreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-conf1", caption = "监听器")
    @ResponseBody
    public TreeListResultModel<List<ListenerTypeTree>> getListeners(String id, String projectVersionName) {
        TreeListResultModel<List<ListenerTypeTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(ListenerItems.values()), ListenerTypeTree.class);

        return result;
    }


}
