package com.ds.bpm.plugins.view;

import com.ds.config.CBPDBrowserElement;
import com.ds.config.CExtendedAttribute;
import com.ds.config.CParameter;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, event = CustomGridEvent.editor)
public class BrowserGridView {

    @CustomAnnotation(uid = true, hidden = true)
    private String browserId;

    @CustomAnnotation(pid = true, hidden = true)
    private String pluginId;

    @CustomAnnotation(pid = true, hidden = true)
    private String projectId;

    @MethodChinaName(cname = "插件名称")
    @CustomAnnotation(uid = true)
    private String name;

    @MethodChinaName(cname = "域名")
    private String baseurl;
    @MethodChinaName(cname = "属性名称")
    private String toxml;
    @MethodChinaName(cname = "窗体名称")
    private String displayname;

    @MethodChinaName(cname = "参数")
    private List<CParameter> parameters = new ArrayList<CParameter>();

    @MethodChinaName(cname = "远程页面扩展")
    private List<CBPDBrowserElement> browserElements = new ArrayList<CBPDBrowserElement>();

    @MethodChinaName(cname = "自定义扩展属性")
    private List<CExtendedAttribute> attributes = new ArrayList<CExtendedAttribute>();

    BrowserGridView() {

    }

    public BrowserGridView(CBPDBrowserElement element) {
        this.pluginId = element.getPluginId();
        this.browserId = element.getBrowserId();
        this.name = element.getName();
        this.projectId = element.getProjectId();
        this.displayname = element.getDisplayname();
        baseurl = element.getBaseurl();
        toxml = element.getToxml();

        Map<String, CParameter> parameterMap = element.getParameters();
        Set<String> keySet = parameterMap.keySet();
        for (String key : keySet) {
            CParameter parameter = parameterMap.get(key);
            if (parameter != null) {
                this.parameters.add(parameterMap.get(key));
            }
        }

        Map<String, CExtendedAttribute> attributeMap = element.getExtendedAttributes();
        Set<String> attributeSet = attributeMap.keySet();
        for (String attkey : attributeSet) {
            CExtendedAttribute parameter = attributeMap.get(attkey);
            if (parameter != null) {
                this.attributes.add(attributeMap.get(attkey));
            }
        }


    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getBrowserId() {
        return browserId;
    }

    public void setBrowserId(String browserId) {
        this.browserId = browserId;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getBaseurl() {
        return baseurl;
    }

    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
    }


    public String getToxml() {
        return toxml;
    }

    public void setToxml(String toxml) {
        this.toxml = toxml;
    }

    public List<CParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<CParameter> parameters) {
        this.parameters = parameters;
    }

    public List<CBPDBrowserElement> getBrowserElements() {
        return browserElements;
    }

    public void setBrowserElements(List<CBPDBrowserElement> browserElements) {
        this.browserElements = browserElements;
    }

    public List<CExtendedAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<CExtendedAttribute> attributes) {
        this.attributes = attributes;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
