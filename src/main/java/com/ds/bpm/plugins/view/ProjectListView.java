package com.ds.bpm.plugins.view;

import com.ds.common.JDSException;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
@PageBar
@GridAnnotation(event = CustomGridEvent.editor)
public class ProjectListView {

    @CustomAnnotation(uid = true,hidden = true)
    String projectId;

    @CustomAnnotation(caption = "工程名称")
    String desc;

    @CustomAnnotation(caption = "工程标识")
    String projectName;

    @CustomAnnotation(caption = "用户组织")
    String spaceName;


    public ProjectListView(Project project) {
        this.projectName = project.getProjectName();

        try {
            this.spaceName = ESDFacrory.getESDClient().getSpace().getDesc();
        } catch (JDSException e) {
            e.printStackTrace();
        }
        this.projectId = project.getId();
        this.desc = project.getDesc();

    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }


}
