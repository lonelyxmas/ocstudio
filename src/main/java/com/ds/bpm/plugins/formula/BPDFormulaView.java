package com.ds.bpm.plugins.formula;

import com.ds.config.CFormula;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.web.annotation.Required;

@MenuBarMenu
@FormAnnotation(col = 1, stretchHeight = StretchType.last, customMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {BPDFormulaManager.class})
public class BPDFormulaView {

    @CustomAnnotation(uid = true, hidden = true)
    private String parameterId;
    @CustomAnnotation(pid = true, hidden = true)
    private String pluginId;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectName;

    @MethodChinaName(cname = "公式类型")
    @CustomAnnotation()
    @Required
    private FormulaType parameterValue;

    @MethodChinaName(cname = "属性值")
    @CustomAnnotation()
    @Required
    private String name;

    @MethodChinaName(cname = "显示名称")
    @CustomAnnotation()
    @Required
    private String desc;


    public BPDFormulaView() {

    }


    public BPDFormulaView(CFormula formula) {
        this.projectName = formula.getProjectName();
        this.pluginId = formula.getPluginId();
        this.parameterId = formula.getParameterId();
        this.name = formula.getName();
        this.desc = formula.getDesc();
        this.parameterValue = formula.getParameterValue();
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getParameterId() {
        return parameterId;
    }

    public void setParameterId(String parameterId) {
        this.parameterId = parameterId;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setParameterValue(FormulaType parameterValue) {
        this.parameterValue = parameterValue;
    }

    public FormulaType getParameterValue() {
        return parameterValue;
    }

}
