package com.ds.bpm.plugins.formula;


import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

;

@Controller
@RequestMapping(path = "/admin/bpd/formula/")
@MethodChinaName(cname = "公式管理", imageClass = "spafont spa-icon-function")

public class BPDFormulaManager {


    @RequestMapping(method = RequestMethod.POST, value = "AddFormulas")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add})
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350")
    @ModuleAnnotation( caption = "添加公式面板")
    @ResponseBody
    public ResultModel<BPDFormulaView> addFormula(String projectName, String pluginId) {
        ResultModel<BPDFormulaView> result = new ResultModel<BPDFormulaView>();
        CFormula cParameter = new CFormula();
        cParameter.setPluginId(pluginId);
        cParameter.setProjectName(projectName);
        cParameter.setPluginId(pluginId);
        result.setData(new BPDFormulaView(cParameter));

        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FormulaInfo")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor})
    @FormViewAnnotation
    @DialogAnnotation(height = "260", width = "350")
    @ModuleAnnotation( caption = "编辑公式面板")
    @ResponseBody
    public ResultModel<BPDFormulaView> getFormulaInfo(String projectName, String pluginId) {
        ResultModel<BPDFormulaView> result = new ResultModel<BPDFormulaView>();
        CFormula cParameter = new CFormula();
        cParameter.setPluginId(pluginId);
        cParameter.setProjectName(projectName);
        cParameter.setPluginId(pluginId);
        result.setData(new BPDFormulaView(cParameter));
        return result;
    }


    @MethodChinaName(cname = "保存公式")
    @RequestMapping(method = RequestMethod.POST, value = "saveFormulas")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save})
    @ResponseBody
    public ResultModel<Boolean> saveFormula(@RequestBody BPDFormulaView parameter) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        String pluginId = parameter.getPluginId();
        String parameterId = parameter.getParameterId();
        BPDPlugin plugin = null;
        try {
            plugin = this.getPluginById(parameter.getProjectName(), parameter.getPluginId());
            CFormula cParameter = new CFormula();
            cParameter.setPluginId(parameter.getPluginId());
            cParameter.setProjectName(parameter.getProjectName());

            if (parameterId == null || parameterId.equals("")) {
                cParameter.setParameterId(UUID.randomUUID().toString());
            } else {
                cParameter.setParameterId(parameterId);
            }
            cParameter.setPluginId(pluginId);
            cParameter.setName(parameter.getName());
            cParameter.setParameterValue(parameter.getParameterValue());
            cParameter.setDesc(parameter.getDesc());
            plugin.putFormula(cParameter.getParameterId(), cParameter);
            BPDProjectConfig bpdProjectConfig = BPMFactory.getInstance().getProjectConfig(plugin.getProjectId());
            BPMFactory.getInstance().updateProjectConfig(bpdProjectConfig);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }


        return result;
    }


    @MethodChinaName(cname = "删除公式")
    @RequestMapping(method = RequestMethod.POST, value = "delFormulas")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = {CustomMenuItem.delete})
    @ResponseBody
    public ResultModel<Boolean> delFormulas(String parameterId, String projectId, String pluginId) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        String[] idarr = StringUtility.split(parameterId, ";");
        BPDPlugin plugin = null;
        try {
            plugin = this.getPluginById(projectId, pluginId);
            for (String id : idarr) {
                plugin.getFormulaTypeMap().remove(id);
            }
            BPDProjectConfig bpdProjectConfig = BPMFactory.getInstance().getProjectConfig(plugin.getProjectId());
            BPMFactory.getInstance().updateProjectConfig(bpdProjectConfig);
        } catch (JDSException e) {
            result = new ErrorResultModel<>();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }


        return result;
    }


    BPDPlugin getPluginById(String projectId, String pluginId) throws JDSException {
        BPDProjectConfig config = BPMFactory.getInstance().getProjectConfig(projectId);
        BPDPlugin plugin = config.getPluginById(pluginId);
        return plugin;
    }

}
