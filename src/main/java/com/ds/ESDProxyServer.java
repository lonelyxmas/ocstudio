package com.ds;

import com.ds.common.JDSException;
import com.ds.config.JDSConfig;
import com.ds.config.UserBean;
import com.ds.esb.config.manager.EsbBeanFactory;
import com.ds.esb.config.manager.ExpressionTempBean;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.esb.util.EsbFactory;
import com.ds.jds.core.User;
import com.ds.server.JDSServer;
import com.ds.server.httpproxy.ServerProxyFactory;
import com.ds.server.httpproxy.core.ProxyHost;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.*;
import java.util.List;


public class ESDProxyServer {

    public static final String PRODUCT_NAME = "ESD EditorTools";

    public static final String PRODUCT_VERSION = "V0.5b";

    public static final String PRODUCT_COPYRIGHT = "Copyright(c)2012 - 2021 itjds.net, All Rights Reserved";

    public static final String MAIN_URL = "http://itjds.net/projectManager/index.do";

    static ESDProxyServer esdEditor;

    private User user;

    public static final String THREAD_LOCK = "Thread Lock";


    public static ESDProxyServer getInstance() {
        if (esdEditor == null) {
            synchronized (THREAD_LOCK) {
                if (esdEditor == null) {
                    esdEditor = new ESDProxyServer();
                }
            }
        }
        return esdEditor;
    }

    ESDProxyServer() {

    }


    private String getLocalPort() {
        ServerSocket s = null;
        String port = "8083";
        try {
            s = new ServerSocket(0);
            port = s.getLocalPort() + "";
            s.close();
        } catch (MalformedURLException e3) {
            e3.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return port;
    }


    public void openUrl(URL url, String sessionid) {
        EsbFactory.initBus();
        List<? extends ServiceBean> list = EsbBeanFactory.getInstance().loadAllServiceBean();
        for (int k = 0; k < list.size(); k++) {
            if (!(list.get(k) instanceof ExpressionTempBean)) {
                continue;
            }
            ExpressionTempBean bean = (ExpressionTempBean) list.get(k);
        }
        ServerProxyFactory factory = ServerProxyFactory.getInstance();
        System.out.println("------------" + JDSConfig.getAbsolutePath("/") + "server.properties");
        System.out.println("************************************************");
        try {
            ProxyHost proxyHost = factory.getWebServer().getProxyHost();
            URI uri = new URI("http://" + proxyHost.getLocalIp() + ":" + proxyHost.getPort() + "/RAD/testVVVERSION0/projectManager");
            Desktop.getDesktop().browse(uri);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


    }


    public static void main(String[] args) {
        main(args, null, null);
    }


    public void login(boolean show, UserBean bean) throws JDSException {

        this.user = JDSServer.getClusterClient().getUDPClient().clientLogin(bean);
        try {
            openUrl(new URL(MAIN_URL), JDSServer.getInstance().getAdminUser().getSessionId());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }


    public static int main(String[] args, InputStream in, PrintStream out) {
        System.out.println("************************************************");
        try {
            System.setProperty("JDSHome", new File(PathUtil.getJdsPath(), "JDSHome").getAbsolutePath());
            System.out.println("************************************************");
            System.out.println("----- 欢迎使用 JDS ESD 代理服务器");
            System.out.println(PRODUCT_NAME + " - " + PRODUCT_VERSION);
            System.out.println(PRODUCT_COPYRIGHT);
            System.out.println("************************************************");
            System.out.println("-------- ESDTools Initialization ---------");
            System.out.println("************************************************");
            System.out.println("- Start Connect Server - " + UserBean.getInstance().getServerUrl() + "*");
            System.out.println("- Connent JDSServer UserName    [" + UserBean.getInstance().getUsername() + "]*");
            ESDProxyServer.getInstance().login(false, UserBean.getInstance());
        } catch (Exception e1) {
            e1.printStackTrace();
            return -1;
        }
        return 0;

    }


    public User getUser() {
        return user;
    }


    public void setUser(User user) {
        this.user = user;
    }


}
