package com.ds;

import com.ds.common.expression.function.AbstractFunction;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.esb.config.EsbBeanAnnotation;
import org.openqa.selenium.chrome.ChromeDriver;

@EsbBeanAnnotation(id = "currChromeDriver")
public class GetChromeDriver extends AbstractFunction {

    public ESDChrome perform() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }
}