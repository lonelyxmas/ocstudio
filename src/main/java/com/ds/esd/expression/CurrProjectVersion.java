package com.ds.esd.expression;

import com.ds.common.JDSException;
import com.ds.common.expression.function.AbstractFunction;
import com.ds.context.JDSActionContext;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;

@EsbBeanAnnotation(id = "CurrProjectVersion")
public class CurrProjectVersion extends AbstractFunction {

    public ProjectVersion perform() {
        String projectVersionName = (String) JDSActionContext.getActionContext().getParams("projectName");
        ProjectVersion version = null;
        try {
            version = ESDFacrory.getESDClient().getProjectVersionByName(projectVersionName);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return version;
    }


}
