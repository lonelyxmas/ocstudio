package com.ds.esd;

import com.ds.bpm.plugins.nav.BPDManagerTree;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavFoldingTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.index.left.left.IndexLeftTopComponent;
import com.ds.esd.custom.layout.annotation.LayoutAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutItemAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.LayoutType;
import com.ds.esd.tool.ui.enums.PosType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin/")
@MethodChinaName(cname = "管理员控制台")
@LayoutAnnotation(transparent = false, type = LayoutType.vertical, items = {})
public class IndexLeftNav {


    @LayoutItemAnnotation(panelBgClr = "#3498DB", size = 55, pos = PosType.before, locked = true, moveDisplay = false, cmd = false)
    @ModuleAnnotation
    public ResultModel<IndexLeftTopComponent> getTopBar() {
        return new ResultModel<IndexLeftTopComponent>();
    }


    @MethodChinaName(cname = "流程配置")
    @RequestMapping(method = RequestMethod.POST, value = "LeftBPDConfig")
    @NavFoldingTreeViewAnnotation
    @LayoutItemAnnotation(pos = PosType.main)
    @APIEventAnnotation(index = 2)
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", caption = "流程配置", dynLoad = true, cache = false)
    @ResponseBody
    public ResultModel<BPDManagerTree> getLeftBPDConfig() {
        return new ResultModel<BPDManagerTree>();
    }


}
