package com.ds.esd.bpm.custom.perform;

import com.ds.esd.bpm.custom.PerformServiceImpl;
import com.ds.esd.bpm.form.PerformService;
import com.ds.esd.custom.annotation.nav.TabItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.dsm.repository.database.proxy.DSMTableProxy;
import com.ds.esd.tool.ui.component.item.TabListItem;
import com.ds.web.annotation.Pid;

@MenuBarMenu(menuClasses = PerformServiceImpl.class)
@TabsAnnotation(autoReload = false)
public class FormTabs extends TabListItem {

    @Pid
    String activityInstId;
    @Pid
    String processInstId;
    @Pid
    String configKey;
    @Pid
    String tableName;
    @Pid
    String formname;

    @TabItemAnnotation(imageClass = "spafont spa-icon-coin", bindService = PerformService.class)
    public FormTabs(DSMTableProxy tableProxy, String activityInstId, String processInstId) {
        this.tableName = tableProxy.getTableName();
        this.id = tableName;
        this.caption = tableProxy.getCnname();
        this.activityInstId = activityInstId;
        this.processInstId = processInstId;
        this.configKey = tableProxy.getConfigKey();
        this.formname = tableProxy.getFieldName();
    }

    public String getActivityInstId() {
        return activityInstId;
    }

    public void setActivityInstId(String activityInstId) {
        this.activityInstId = activityInstId;
    }

    public String getProcessInstId() {
        return processInstId;
    }

    public void setProcessInstId(String processInstId) {
        this.processInstId = processInstId;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFormname() {
        return formname;
    }

    public void setFormname(String formname) {
        this.formname = formname;
    }
}
