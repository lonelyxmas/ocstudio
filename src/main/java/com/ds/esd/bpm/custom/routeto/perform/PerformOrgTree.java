package com.ds.esd.bpm.custom.routeto.perform;

import com.ds.bpm.enums.right.PerformGroupEnums;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.org.Org;
import com.ds.org.Person;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;


@TreeAnnotation(heplBar = true, caption = "选择人员", selMode = SelModeType.singlecheckbox)
public class PerformOrgTree extends TreeListItem {
    @Pid
    String orgId;

    @Pid
    String parentOrgId;

    @Uid
    String personId;

    @Pid
    PerformGroupEnums performGroup;
    @Pid
    String activityInstId;

    @Pid
    String nextActivityDefId;


    @TreeItemAnnotation(imageClass = "bpmfont bpm-gongzuoliu-moxing", bindService = PerformPersonTreeService.class, iniFold = false)
    public PerformOrgTree(Org org, String activityInstId, String nextActivityDefId) {
        this.id = org.getOrgId();
        this.caption = org.getName();
        this.orgId = org.getOrgId();
        this.parentOrgId=orgId;
        this.performGroup = PerformGroupEnums.performs;
        this.activityInstId = activityInstId;
        this.nextActivityDefId = nextActivityDefId;

    }


    @TreeItemAnnotation(imageClass = "bpmfont bpmgongzuoliu")
    public PerformOrgTree(Person person) {
        this.caption = person.getName();
        this.id = person.getID();
        this.orgId = person.getOrgId();
        this.personId = person.getID();
    }

    public String getParentOrgId() {
        return parentOrgId;
    }

    public void setParentOrgId(String parentOrgId) {
        this.parentOrgId = parentOrgId;
    }

    public String getActivityInstId() {
        return activityInstId;
    }

    public void setActivityInstId(String activityInstId) {
        this.activityInstId = activityInstId;
    }

    public String getNextActivityDefId() {
        return nextActivityDefId;
    }

    public void setNextActivityDefId(String nextActivityDefId) {
        this.nextActivityDefId = nextActivityDefId;
    }

    public PerformGroupEnums getPerformGroup() {
        return performGroup;
    }

    public void setPerformGroup(PerformGroupEnums performGroup) {
        this.performGroup = performGroup;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
