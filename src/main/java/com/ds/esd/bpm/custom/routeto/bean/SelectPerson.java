package com.ds.esd.bpm.custom.routeto.bean;

import com.ds.bpm.client.Perform;

public class SelectPerson {

    public SelectPerson() {

    }

    Perform performs = new Perform();

    Perform readers = new Perform();

    Perform insteadSigns = new Perform();

    public Perform getPerforms() {
        return performs;
    }

    public void setPerforms(Perform performs) {
        this.performs = performs;
    }

    public Perform getReaders() {
        return readers;
    }

    public void setReaders(Perform readers) {
        this.readers = readers;
    }

    public Perform getInsteadSigns() {
        return insteadSigns;
    }

    public void setInsteadSigns(Perform insteadSigns) {
        this.insteadSigns = insteadSigns;
    }
}
