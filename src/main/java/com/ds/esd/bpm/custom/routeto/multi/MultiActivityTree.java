package com.ds.esd.bpm.custom.routeto.multi;

import com.ds.bpm.client.ActivityDef;
import com.ds.bpm.client.RouteToType;
import com.ds.esd.custom.annotation.nav.TabItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.item.TabListItem;
import com.ds.web.annotation.Pid;


@TabsAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, autoReload = false, customService = MultiRoutetoService.class)
public class MultiActivityTree extends TabListItem {

    @Pid
    public String topOrgId;
    @Pid
    public RouteToType action;
    @Pid
    public String activityInstId;
    @Pid
    public String nextActivityDefId;


    @TabItemAnnotation(bindService = MultiActivityTreeService.class, caption = "选择办理人员")
    public MultiActivityTree(ActivityDef activityDef, String activityInstId, RouteToType action, String topOrgId) {
        super(activityDef.getActivityDefId(), activityDef.getName());
        this.tag = activityDef.getActivityDefId();
        this.topOrgId = topOrgId;
        this.action = action;
        this.id = activityDef.getActivityDefId();
        this.caption = activityDef.getName();
        this.activityInstId = activityInstId;
        this.nextActivityDefId = activityDef.getActivityDefId();

    }


    public String getTopOrgId() {
        return topOrgId;
    }

    public void setTopOrgId(String topOrgId) {
        this.topOrgId = topOrgId;
    }

    public RouteToType getAction() {
        return action;
    }

    public void setAction(RouteToType action) {
        this.action = action;
    }

    public String getActivityInstId() {
        return activityInstId;
    }

    public void setActivityInstId(String activityInstId) {
        this.activityInstId = activityInstId;
    }

    public String getNextActivityDefId() {
        return nextActivityDefId;
    }

    public void setNextActivityDefId(String nextActivityDefId) {
        this.nextActivityDefId = nextActivityDefId;
    }


}
