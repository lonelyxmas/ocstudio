package com.ds.esd.bpm.custom.routeto.multi;

import com.ds.bpm.client.ActivityDef;
import com.ds.bpm.client.RouteToType;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.bpm.enums.activitydef.ActivityDefPerformtype;
import com.ds.config.TreeListResultModel;
import com.ds.esd.bpm.custom.routeto.MultiPerformGroupItem;
import com.ds.esd.bpm.custom.routeto.PerformGroupItem;
import com.ds.esd.custom.annotation.nav.NavButtonViewsViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.ButtonViewPageUtil;
import com.ds.esd.util.TabPageUtil;
import com.ds.jds.core.esb.EsbUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/bpm/routeto/multi/")
public class MultiActivityTreeService {

    @NavButtonViewsViewAnnotation()
    @ModuleAnnotation(dynLoad = true)
    @APIEventAnnotation(bindMenu = CustomMenuItem.tabEditor)
    @RequestMapping(method = RequestMethod.POST, value = "performSelect")
    @ResponseBody
    public TreeListResultModel<List<MultiPerformTree>> performSelect(String activityInstId, String nextActivityDefId, RouteToType action) {
        TreeListResultModel<List<MultiPerformTree>> performTree = new TreeListResultModel<>();
        if (nextActivityDefId != null) {
            try {
                ActivityDef activityDef = this.getClient().getActivityDef(nextActivityDefId);
                if (activityDef.getRightAttribute().getPerformType().equals(ActivityDefPerformtype.MULTIPLE)) {
                    performTree = TabPageUtil.getTabList(Arrays.asList(MultiPerformGroupItem.values()), MultiPerformTree.class);
                } else {
                    performTree = ButtonViewPageUtil.getTabList(Arrays.asList(PerformGroupItem.values()), MultiPerformTree.class);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return performTree;

    }

    private WorkflowClientService getClient() {
        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);        return client;
    }

}
