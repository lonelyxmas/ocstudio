package com.ds.esd.bpm.custom.routeto;

import com.ds.bpm.enums.right.PerformGroupEnums;
import com.ds.esd.bpm.custom.routeto.insteadsign.InsteadSignGroupService;
import com.ds.esd.bpm.custom.routeto.perform.MultiPerformGroupService;
import com.ds.esd.bpm.custom.routeto.reader.ReaderGroupService;
import com.ds.esd.custom.nav.tab.enums.TabItem;

public enum MultiPerformGroupItem implements TabItem {

    performs(PerformGroupEnums.performs, MultiPerformGroupService.class, false, false, false),

    readers(PerformGroupEnums.readers, ReaderGroupService.class, false, false, false),

    insteadSigns(PerformGroupEnums.insteadSigns, InsteadSignGroupService.class, false, false, false);


    private String name;

    private String imageClass;

    private String className;

    PerformGroupEnums performGroup;

    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    MultiPerformGroupItem(PerformGroupEnums performGroupEnums, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.name = performGroupEnums.getName();
        this.performGroup = performGroupEnums;
        this.imageClass = performGroupEnums.getImageClass();
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public PerformGroupEnums getPerformGroup() {
        return performGroup;
    }

    public void setPerformGroup(PerformGroupEnums performGroup) {
        this.performGroup = performGroup;
    }

    @Override
    public String getType() {
        return name();
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    public String getImageClass() {
        return imageClass;
    }

    public static PerformGroupEnums fromType(String typeName) {
        for (PerformGroupEnums type : PerformGroupEnums.values()) {
            if (type.getType().equals(typeName)) {
                return type;
            }
        }
        return null;
    }

    public static PerformGroupEnums fromName(String name) {
        for (PerformGroupEnums type : PerformGroupEnums.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
