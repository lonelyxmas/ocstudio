package com.ds.esd.bpm.custom.routeto.bean;

import com.ds.bpm.client.RouteToType;

public class WebPerformPerson {

    public WebPerformPerson() {

    }

    RouteToType action;

    String activityInstId;

    String nextActivityDefId;

    String activityInstHistoryId;

    SelectPerson selectPerson;

    public SelectPerson getSelectPerson() {
        return selectPerson;
    }

    public void setSelectPerson(SelectPerson selectPerson) {
        this.selectPerson = selectPerson;
    }

    public String getActivityInstHistoryId() {
        return activityInstHistoryId;
    }

    public void setActivityInstHistoryId(String activityInstHistoryId) {
        this.activityInstHistoryId = activityInstHistoryId;
    }

    public String getNextActivityDefId() {
        return nextActivityDefId;
    }

    public void setNextActivityDefId(String nextActivityDefId) {
        this.nextActivityDefId = nextActivityDefId;
    }

    public RouteToType getAction() {
        return action;
    }

    public void setAction(RouteToType action) {
        this.action = action;
    }

    public String getActivityInstId() {
        return activityInstId;
    }

    public void setActivityInstId(String activityInstId) {
        this.activityInstId = activityInstId;
    }


}
