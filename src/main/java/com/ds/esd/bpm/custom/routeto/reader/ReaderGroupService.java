package com.ds.esd.bpm.custom.routeto.reader;

import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.esd.bpm.custom.routeto.perform.PerformOrgTree;
import com.ds.esd.custom.annotation.TreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.BpmDomain;
import com.ds.esd.dsm.domain.enums.BpmDomainType;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.Org;
import com.ds.server.OrgManagerFactory;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/bpm/routeto/select/")
@BpmDomain(type = BpmDomainType.bpmright)
@Aggregation(type = AggregationType.customDomain, rootClass = ReaderGroupService.class)
public class ReaderGroupService {

    @TreeViewAnnotation()
    @ModuleAnnotation(dynLoad = true, name = "readers")
    @APIEventAnnotation(bindMenu = CustomMenuItem.tabEditor)
    @RequestMapping(method = RequestMethod.POST, value = "Readers")
    @ResponseBody
    public TreeListResultModel<List<ReaderOrgTree>> getPerforms(String activityInstId, String nextActivityDefId, String topOrgId) {
        TreeListResultModel<List<ReaderOrgTree>> orgTreeResultModule = new TreeListResultModel<>();
        List<Org> orgs = OrgManagerFactory.getOrgManager().getTopOrgs(JDSActionContext.getActionContext().getSystemCode());
        orgTreeResultModule = TreePageUtil.getTreeList(orgs, ReaderOrgTree.class);
        return orgTreeResultModule;
    }

}
