package com.ds.esd.bpm.custom.routeto.multi;

import com.ds.bpm.client.RouteToType;
import com.ds.bpm.enums.right.PerformGroupEnums;
import com.ds.esd.bpm.custom.routeto.PerformGroupItem;
import com.ds.esd.custom.annotation.nav.TabItemAnnotation;
import com.ds.esd.custom.buttonviews.annotation.ButtonViewsAnnotation;
import com.ds.esd.tool.ui.component.item.TabListItem;
import com.ds.esd.tool.ui.component.tab.SideBarStatusType;
import com.ds.esd.tool.ui.enums.BarLocationType;
import com.ds.esd.tool.ui.enums.VAlignType;
import com.ds.web.annotation.Pid;


@ButtonViewsAnnotation(barLocation = BarLocationType.left, barSize = "3em", autoReload = false, barVAlign = VAlignType.top, sideBarStatus = SideBarStatusType.expand)
public class MultiPerformTree extends TabListItem {

    @Pid
    public String topOrgId;
    @Pid
    public String orgId;
    @Pid
    public RouteToType action;
    @Pid
    public String activityInstId;
    @Pid
    public String nextActivityDefId;

    @Pid
    PerformGroupEnums performGroup;


    @TabItemAnnotation(customItems = PerformGroupItem.class)
    public MultiPerformTree(PerformGroupItem performGroupItem, String activityInstId, RouteToType action, String topOrgId, String nextActivityDefId) {
        super(performGroupItem.getType(), performGroupItem.getName(), performGroupItem.getImageClass());
        this.setName(performGroupItem.getType());
        this.setDesc(performGroupItem.getName());
        this.performGroup = performGroupItem.getPerformGroup();
        this.caption = performGroupItem.getName();
        this.topOrgId = topOrgId;
        this.action = action;
        this.activityInstId = activityInstId;
        this.nextActivityDefId = nextActivityDefId;
    }

    public String getTopOrgId() {
        return topOrgId;
    }

    public void setTopOrgId(String topOrgId) {
        this.topOrgId = topOrgId;
    }

    public RouteToType getAction() {
        return action;
    }

    public void setAction(RouteToType action) {
        this.action = action;
    }

    public String getActivityInstId() {
        return activityInstId;
    }

    public void setActivityInstId(String activityInstId) {
        this.activityInstId = activityInstId;
    }

    public String getNextActivityDefId() {
        return nextActivityDefId;
    }

    public void setNextActivityDefId(String nextActivityDefId) {
        this.nextActivityDefId = nextActivityDefId;
    }

    public PerformGroupEnums getPerformGroup() {
        return performGroup;
    }

    public void setPerformGroup(PerformGroupEnums performGroup) {
        this.performGroup = performGroup;
    }

}
