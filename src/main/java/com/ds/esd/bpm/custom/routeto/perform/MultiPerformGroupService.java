package com.ds.esd.bpm.custom.routeto.perform;

import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.esd.custom.annotation.TreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.BpmDomain;
import com.ds.esd.dsm.domain.enums.BpmDomainType;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.Org;
import com.ds.server.OrgManagerFactory;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/bpm/routeto/select/")
@BpmDomain(type = BpmDomainType.bpmright)
@Aggregation(type = AggregationType.customDomain, rootClass = MultiPerformGroupService.class)
public class MultiPerformGroupService {

    @TreeViewAnnotation()
    @ModuleAnnotation(dynLoad = true, name = "performs")
    @APIEventAnnotation(bindMenu = CustomMenuItem.tabEditor)
    @RequestMapping(method = RequestMethod.POST, value = "MultiPerforms")
    @ResponseBody
    public TreeListResultModel<List<PerformOrgTree>> getMultiPerforms(String activityInstId, String nextActivityDefId, String topOrgId) {
        TreeListResultModel<List<PerformOrgTree>> orgTreeResultModule = new TreeListResultModel<>();
        List<Org> orgs = OrgManagerFactory.getOrgManager().getTopOrgs(JDSActionContext.getActionContext().getSystemCode());
        orgTreeResultModule = TreePageUtil.getTreeList(orgs, PerformOrgTree.class);
        return orgTreeResultModule;
    }

}
