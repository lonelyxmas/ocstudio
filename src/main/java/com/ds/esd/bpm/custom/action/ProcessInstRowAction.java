package com.ds.esd.bpm.custom.action;

import com.ds.bpm.engine.WorkflowClientService;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.plugins.svgview.SVGProcessInstView;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.jds.core.esb.EsbUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/bpm/porcesInst/")
@MethodChinaName(cname = "流程实例", imageClass = "bpmfont bpmshujukaifagongzuoliushujutansuozuijindakai")
public class ProcessInstRowAction {

    @MethodChinaName(cname = "查看例程")
    @RequestMapping(method = RequestMethod.POST, value = "ProcessView")
    @CustomAnnotation(imageClass = "bpmfont bpmshujukaifagongzuoliushujutansuozuijindakai", caption = "查看例程")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<SVGProcessInstView> processView(String processInstId) {
        ResultModel<SVGProcessInstView> resultModel = new ResultModel<SVGProcessInstView>();
        return resultModel;
    }


    /**
     * @return
     */
    private WorkflowClientService getClient() {

        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);

        return client;
    }
}

