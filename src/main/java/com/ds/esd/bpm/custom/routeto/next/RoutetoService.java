package com.ds.esd.bpm.custom.routeto.next;


import com.ds.bpm.client.*;
import com.ds.bpm.engine.BPMException;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.bpm.enums.activitydef.ActivityDefPerformSequence;
import com.ds.bpm.enums.activitydef.ActivityDefPerformtype;
import com.ds.bpm.enums.right.RightCtx;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.custom.routeto.bean.WebPerformPerson;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.toolbar.bpm.RouteCustomMenu;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.org.Person;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("/bpm/routeto/")
@MethodChinaName(cname = "发送动作")
public class RoutetoService {
    @MethodChinaName(cname = "发送")
    @RequestMapping(method = RequestMethod.POST, value = "RouteTo")
    @RouteCustomMenu(routeType = {RouteToType.RouteTo})
    @APIEventAnnotation(bindMenu = {CustomMenuItem.save}, isAllform = true, callback = {CustomCallBack.CloseParent, CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> routeTo(@RequestBody WebPerformPerson webPerformPerson) {
        RouteBean routeBean = new RouteBean();
        routeBean.setActivityInstId(webPerformPerson.getActivityInstId());
        routeBean.setAction(webPerformPerson.getAction());
        routeBean.setReaders(webPerformPerson.getSelectPerson().getReaders());
        routeBean.setPerforms(webPerformPerson.getSelectPerson().getPerforms());
        routeBean.setInsteadSigns(webPerformPerson.getSelectPerson().getInsteadSigns());
        routeBean.setActivityInstHistoryId(webPerformPerson.getActivityInstHistoryId());
        routeBean.setNextActivityDefId(webPerformPerson.getNextActivityDefId());
        routeBean.setInsteadSigns(webPerformPerson.getSelectPerson().getInsteadSigns());
        ResultModel resultModel = new ResultModel();
        resultModel.setData(false);
        String activityInstId = routeBean.getActivityInstId();
        try {
            List<String> activityDefIds = new ArrayList<String>();
            List<Map<RightCtx, Object>> ctxs = new ArrayList<Map<RightCtx, Object>>();
            activityDefIds.add(routeBean.getNextActivityDefId());
            Map<RightCtx, Object> ctx = this.fillCtx(routeBean);
            ctxs.add(ctx);
            getClient().routeTo(activityInstId, activityDefIds, ctxs);
            ActivityInst activityInst = this.getClient().getActivityInst(activityInstId);
            ActivityDefRight right = activityInst.getActivityDef().getRightAttribute();

//                // 当设定条件为单人办理且类型为自动签收时自动进入连续办理界面
            if (right.getPerformSequence().equals(ActivityDefPerformSequence.AUTOSIGN) && activityInst.isCanSignReceive() && right.getPerformType().equals(ActivityDefPerformtype.SINGLE)) {
                resultModel.setData(true);
            }


        } catch (BPMException e) {
            resultModel = new ErrorResultModel();
        }
        return resultModel;
    }

    private Map<RightCtx, Object> fillCtx(RouteBean routeBean) throws BPMException {
        Map<RightCtx, Object> ctx = new HashMap<RightCtx, Object>();
        ActivityDef activityDef = getClient().getActivityDef(routeBean.getNextActivityDefId());
        // 办理类型
        if (activityDef != null) {
            ActivityDefPerformtype performType = activityDef.getRightAttribute().getPerformType();
            ActivityDefPerformSequence performSequence = activityDef.getRightAttribute().getPerformSequence();
            if (performType.equals(ActivityDefPerformtype.NOSELECT) || performType.equals(ActivityDefPerformtype.NEEDNOTSELECT)) {
                List<String> readList = new ArrayList<String>();
                List<String> performList = new ArrayList<String>();
                List<Person> persons = activityDef.getRightAttribute().getPerFormPersons();
                for (Person person : persons) {
                    performList.add(person.getID());
                }
                List<Person> readpersons = activityDef.getRightAttribute().getReaderPersons();
                for (Person person : readpersons) {
                    readList.add(person.getID());
                }
                ctx.put(RightCtx.PERFORMERS, performList);
                ctx.put(RightCtx.READERS, readList);
            } else {
                String[] performArr = StringUtility.split(routeBean.getPerforms().getPerforms(), ";");
                String[] readArr = StringUtility.split(routeBean.getReaders().getReaders(), ";");
                List<String> performList = Arrays.asList(performArr);
                ctx.put(RightCtx.PERFORMERS, performList);
                ctx.put(RightCtx.READERS, Arrays.asList(readArr));

            }
        }

        return ctx;
    }


    /**
     * @return
     */
    private WorkflowClientService getClient() {
        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);        return client;
    }
}
