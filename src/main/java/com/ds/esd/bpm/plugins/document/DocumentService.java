package com.ds.esd.bpm.plugins.document;

import com.ds.bpm.client.ActivityInst;
import com.ds.bpm.client.ct.CtBPMCacheManager;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.common.JDSException;
import com.ds.common.md5.MD5InputStream;
import com.ds.common.util.StringUtility;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.plugins.attachment.AttachMentView;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.BpmDomain;
import com.ds.esd.dsm.domain.enums.BpmDomainType;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.org.conf.OrgConstants;
import com.ds.vfs.FileInfo;
import com.ds.vfs.Folder;
import com.ds.vfs.ct.CtVfsFactory;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/bpm/plugins/document/")
@MethodChinaName(cname = "正文", imageClass = "spafont spa-icon-c-textarea")
@BpmDomain(type = BpmDomainType.custom)
@Aggregation(type = AggregationType.customDomain,rootClass = DocumentService.class )
public class DocumentService {
    @MethodChinaName(cname = "正文")
    @GridViewAnnotation()
    @ModuleAnnotation(caption ="正文" )
    @RequestMapping(method = RequestMethod.POST, value = "DocumentList")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.index, CustomMenuItem.reload})
    public @ResponseBody
    ListResultModel<List<AttachMentView>> getDocumentList(String activityInstId, String formname, int pageIndex, int pageSize) {
        ListResultModel<List<AttachMentView>> resultModel = new ListResultModel<List<AttachMentView>>();
        try {
            ActivityInst inst = getClient().getActivityInst(activityInstId);
            String processInstId = null;
            if (inst != null) {
                processInstId = inst.getProcessInstId();
            } else {
                processInstId = activityInstId;
            }
            String path = processInstId + "/" + formname + "/doc";
            Folder foleder = CtVfsFactory.getCtVfsService().mkDir(path);
            List<FileInfo> fileInfos = foleder.getFileList();
            resultModel = PageUtil.getDefaultPageList(fileInfos, AttachMentView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @MethodChinaName(cname = "添加正文")
    @RequestMapping(method = RequestMethod.POST, value = "addDocument")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> addDocument(String activityInstId, String file_fullname, String formname, @RequestParam("file") MultipartFile file) {
        ResultModel<Boolean> resultModel = new ResultModel<Boolean>();
        try {
            ActivityInst inst = CtBPMCacheManager.getInstance().getActivityInst(activityInstId);
            String path = OrgConstants.CMAILROOTPATH + inst.getProcessInstId() + "/" + formname + "/doc";
            Folder foleder = CtVfsFactory.getCtVfsService().mkDir(path);
            FileInfo fileInfo = foleder.createFile(file_fullname, getClient().getConnectInfo().getUserID());
            CtVfsFactory.getCtVfsService().upload(fileInfo.getPath(), new MD5InputStream(file.getInputStream()), getClient().getConnectInfo().getUserID());
        } catch (JDSException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultModel;

    }

    @MethodChinaName(cname = "删除正文")
    @RequestMapping(method = RequestMethod.POST, value = "delDocument")
    @APIEventAnnotation(callback = CustomCallBack.Reload, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delAttacheMent(String paths) {
        ResultModel<Boolean> resultModel = new ResultModel<Boolean>();
        String[] pathArr = StringUtility.split(paths, ";");
        List<FileInfo> fileInfos = new ArrayList<FileInfo>();
        try {
            for (String path : pathArr) {
                FileInfo file = CtVfsFactory.getCtVfsService().getFileByPath(path);
                CtVfsFactory.getCtVfsService().deleteFile(file.getID());
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return resultModel;

    }

    /**
     * @return
     */
    private WorkflowClientService getClient() {

        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);

        return client;
    }
}
