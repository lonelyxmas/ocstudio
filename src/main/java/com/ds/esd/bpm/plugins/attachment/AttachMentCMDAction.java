package com.ds.esd.bpm.plugins.attachment;

import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAction;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.tool.ui.enums.event.ActionTypeEnum;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/bpm/plugins/attachment/")
public class AttachMentCMDAction {
    @MethodChinaName(cname = "下载")
    @CustomAnnotation(imageClass = "spafont spa-icon-select1")
    @RequestMapping(value = {"download"}, method = {RequestMethod.POST})
    @APIEventAnnotation(
            bindAction = {@CustomAction(method = "call", type = ActionTypeEnum.other, target = "callback", args = {"{xui.downLoad()}", "undefined", "undefined", "{args[1].path}"}, _return = false)}
    )
    @ResponseBody
    public ResultModel<Boolean> download(String activityInstId, String activityInstHistoryId, String path) {
        ResultModel resultModel = new ResultModel();
        return resultModel;
    }


}

