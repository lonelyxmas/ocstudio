package com.ds.esd.bpm.plugins.attachment;

import com.alibaba.fastjson.util.TypeUtils;
import com.ds.common.JDSException;
import com.ds.context.JDSActionContext;
import com.ds.esd.custom.action.CustomPageAction;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.component.block.BlockComponent;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.component.form.FileUploadComponent;
import com.ds.esd.tool.ui.component.form.FileUploadProperties;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.enums.event.enums.FileUploadEventEnum;
import com.ds.esd.tool.ui.module.ModuleComponent;
import com.ds.web.RequestParamBean;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class UPLoadView extends ModuleComponent {
    public UPLoadView() {

    }

    public UPLoadView(EUModule module, MethodConfig methodAPIBean, Map<String, Object> valueMap) {
        euModule = module;

        FileUploadComponent uploadComponent = new FileUploadComponent();
        uploadComponent.addEvent(FileUploadEventEnum.uploadcomplete, new Action(CustomPageAction.ReloadParent));
        FileUploadProperties fileUploadProperties = new FileUploadProperties();
        fileUploadProperties.setPrepareFormData(true);
        fileUploadProperties.setDock(Dock.fill);
        try {
            ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(methodAPIBean.getEsdClass().getClassName(), methodAPIBean.getDomainId());
            MethodConfig methodConfig = apiClassConfig.getMethodByItem(CustomMenuItem.upload);
            LinkedHashSet<RequestParamBean> paramsList = methodConfig.getParamSet();
            Map<String, Object> context = new HashMap<>();
            context.put("projectName", euModule.getProjectVersion().getVersionName());
            if (paramsList.size() > 0) {
                for (RequestParamBean paramBean : paramsList) {
                    Object value = TypeUtils.cast(JDSActionContext.getActionContext().getParams(paramBean.getParamName()), paramBean.getParamClass(), null);
                    if (value != null) {
                        context.put(paramBean.getParamName(), value);
                    }
                }
            }
            fileUploadProperties.setParams(context);

            if (methodAPIBean != null) {
                fileUploadProperties.setUploadUrl(methodConfig.getUrl());
            } else {
                fileUploadProperties.setUploadUrl("/custom/bpm/AttachUPLoad");
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        uploadComponent.setProperties(fileUploadProperties);
        BlockComponent mainComponent = new BlockComponent<>(Dock.fill, euModule.getName() + "Main");
        mainComponent.addChildren(uploadComponent);

        if (this.getDialogComponent() != null) {
            this.getDialogComponent().addChildren(mainComponent);
            this.addChildren(this.getDialogComponent());
        } else if (this.getModuleBlockComponent() != null) {
            this.getModuleBlockComponent().addChildren(mainComponent);
            this.addChildren(mainComponent);
        } else if (this.getModulePanelComponent() != null) {
            this.getModulePanelComponent().addChildren(mainComponent);
            this.addChildren(mainComponent);
        } else {
            this.addChildren(mainComponent);
        }

    }

}
