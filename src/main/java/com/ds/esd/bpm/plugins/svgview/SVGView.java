package com.ds.esd.bpm.plugins.svgview;


import com.ds.bpm.client.ActivityInst;
import com.ds.bpm.client.ActivityInstHistory;
import com.ds.bpm.client.ProcessInst;
import com.ds.bpm.engine.BPMException;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.node.*;
import com.ds.esd.bpm.view.Graph;
import com.ds.esd.bpm.view.GraphService;
import com.ds.esd.tool.ui.component.svg.SVGPaperComponent;
import com.ds.esd.tool.ui.component.svg.comb.circle.CircleProperties;
import com.ds.esd.tool.ui.component.svg.comb.connector.ConnectorProperties;
import com.ds.esd.tool.ui.component.svg.comb.rect.ActivityRectProperties;
import com.ds.jds.core.esb.EsbUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/bpm/view/")
@MethodChinaName(cname = "视图样例")
public class SVGView {

    public SVGView() {

    }

    @MethodChinaName(cname = "测试绘图")
    @RequestMapping(method = RequestMethod.GET, value = "getView")
    public @ResponseBody
    ResultModel<SVGPaperComponent> getView() {
        ResultModel<SVGPaperComponent> model = new ResultModel<SVGPaperComponent>();
        SVGPaperComponent page = new SVGPaperComponent();
        StartNode startNode = new StartNode(120, 230,"start");
        EndNode endNode = new EndNode(800, 230, "end" + 1);
        NoActiviteNode noActiviteNode1 = new NoActiviteNode(200, 230, "noActiviteNode1", "起草", "起草");
        NoActiviteNode noActiviteNode2 = new NoActiviteNode(380, 230, "noActiviteNode2", "审批", "审批");

        RouteNode startRouteNode = new RouteNode(startNode, noActiviteNode1, "startRouteNode", "发送");
        RouteNode routeNode1 = new RouteNode(noActiviteNode1, noActiviteNode2, "routeNode1", "发送");
        RouteNode endRouteNode = new RouteNode(noActiviteNode2, endNode, "endRouteNode", "发送");
        page.addChildren(startNode);
        page.addChildren(endNode);
        page.addChildren(noActiviteNode2);
        page.addChildren(noActiviteNode1);

        page.addChildren(startRouteNode);
        page.addChildren(routeNode1);
        page.addChildren(endRouteNode);
        model.setData(page);


        return model;

    }


    @MethodChinaName(cname = "历史展现视图")
    @RequestMapping(method = RequestMethod.GET, value = "processInstView")
    public @ResponseBody
    ResultModel<SVGPaperComponent> processInstView(String processInstId) throws JDSException {
        ResultModel<SVGPaperComponent> model = new ResultModel<SVGPaperComponent>();
        SVGPaperComponent page = new SVGPaperComponent();
        try {

            GraphService service = EsbUtil.parExpression(GraphService.class);

            ProcessInst processInst = getWorkflowSevice().getProcessInst(processInstId);
            String versionId = processInst.getProcessDefVersionId();
            Graph graph = service.getGraphInfo(versionId).get();
            List<RouteNode<ConnectorProperties>> routes = graph.getRouteNodes();
            for (RouteNode routeNode : routes) {
                page.addChildren(routeNode);
            }

            List<StartNode<CircleProperties>>  startNodes = graph.getStartNodes();
            for (StartNode node : startNodes) {
                page.addChildren(node);
            }

            List<EndNode<CircleProperties>> endNodes = graph.getEndNodes();
            for (EndNode node : endNodes) {
                page.addChildren(node);
            }


            Set<String> instDefSet = new LinkedHashSet();
            List<ActivityInst> actvityInstList = processInst.getActivityInstList();
            for (ActivityInst inst : actvityInstList) {
                instDefSet.add(inst.getActivityDefId());
            }

            Set<String> hisDefSet = new LinkedHashSet();
            List<ActivityInstHistory> historyList = processInst.getActivityInstHistoryListByProcessInst();
            for (ActivityInstHistory his : historyList) {
                hisDefSet.add(his.getActivityDefId());
            }


            List<NoActiviteNode<ActivityRectProperties>> activitys = graph.getActiviteNode();
            for (NoActiviteNode node : activitys) {

                if (instDefSet.contains(node.getAlias())) {
                    page.addChildren(new InstActiviteNode(node));
                } else if (hisDefSet.contains(node.getAlias())) {
                    page.addChildren(new HisActiviteNode(node));
                } else {
                    page.addChildren(new WaitActiviteNode(node));
                }


            }
            model.setData(page);

        } catch (BPMException e) {
            e.printStackTrace();
            model = new ErrorResultModel<SVGPaperComponent>();
            ((ErrorResultModel) model).setErrcode(e.getErrorCode());
            ((ErrorResultModel) model).setErrdes(e.getMessage());

        }


        return model;

    }


    @MethodChinaName(cname = "样式视图")
    @RequestMapping(method = RequestMethod.GET, value = "processDefView")
    public @ResponseBody
    ResultModel<SVGPaperComponent> processView(String processDefVersionId) {

        GraphService service = EsbUtil.parExpression(GraphService.class);

        ResultModel<SVGPaperComponent> model = new ResultModel<SVGPaperComponent>();
        try {
            Graph graph = service.getGraphInfo(processDefVersionId).get();
            SVGPaperComponent page = new SVGPaperComponent();
            List<RouteNode<ConnectorProperties>>routes = graph.getRouteNodes();
            for (RouteNode routeNode : routes) {
                page.addChildren(routeNode);
            }
            List<NoActiviteNode<ActivityRectProperties>> activitys = graph.getActiviteNode();
            for (NoActiviteNode node : activitys) {
                page.addChildren(node);
            }

            List<StartNode<CircleProperties>> startNodes = graph.getStartNodes();
            for (StartNode node : startNodes) {
                page.addChildren(node);
            }

            List<EndNode<CircleProperties>> endNodes = graph.getEndNodes();
            for (EndNode node : endNodes) {
                page.addChildren(node);
            }
            model.setData(page);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorResultModel<SVGPaperComponent>();
            ((ErrorResultModel) model).setErrcode(e.getErrorCode());
            ((ErrorResultModel) model).setErrdes(e.getMessage());
        }
        return model;

    }

    private WorkflowClientService getWorkflowSevice() {
        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);
        return client;
    }


}
