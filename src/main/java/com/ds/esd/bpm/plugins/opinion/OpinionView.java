package com.ds.esd.bpm.plugins.opinion;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.org.PersonNotFoundException;
import com.ds.server.OrgManagerFactory;
import com.ds.vfs.FileInfo;
import com.ds.vfs.ct.CtVfsFactory;

import java.util.Date;

@PageBar
@GridAnnotation(customService = OpinionService.class, customMenu = {GridMenu.Add, GridMenu.Reload, GridMenu.Delete}, event = CustomGridEvent.editor)
public class OpinionView {

    @CustomAnnotation(caption = "上传人")
    String personname;
    @CustomAnnotation(caption = "意见内容")
    String content;
    @CustomAnnotation(caption = "文件名称")
    String name;
    @CustomAnnotation(caption = "上传时间")
    Date time;
    @CustomAnnotation(caption = "上传路径")
    String path;
    @CustomAnnotation(caption = "版本号")
    String version;


    OpinionView(FileInfo fileInfo) {

        this.name = fileInfo.getDescrition();
        this.time = new Date(fileInfo.getCreateTime());
        this.path = fileInfo.getPath();
        this.version = fileInfo.getCurrentVersion().getVersionName();

        try {
            this.content = CtVfsFactory.getCtVfsService().readFileAsString(fileInfo.getPath(), "utf-8").toString();
            this.personname = OrgManagerFactory.getOrgManager().getPersonByID(fileInfo.getPersonId()).getName();
        } catch (PersonNotFoundException e) {
            e.printStackTrace();
        } catch (JDSException e) {
            e.printStackTrace();
        }

    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPersonname() {
        return personname;
    }

    public void setPersonname(String personname) {
        this.personname = personname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getTime() {
        return time;
    }

    public String getPath() {

        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


}
