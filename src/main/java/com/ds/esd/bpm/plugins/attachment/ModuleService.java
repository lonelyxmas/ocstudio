package com.ds.esd.bpm.plugins.attachment;

import com.ds.bpm.client.ActivityInst;
import com.ds.bpm.client.ProcessInst;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.bpm.enums.process.ProcessInstAtt;
import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.vfs.FileInfo;
import com.ds.vfs.Folder;
import com.ds.vfs.ct.CtVfsFactory;
import com.ds.vfs.ct.CtVfsService;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/Module/")
@MethodChinaName(cname = "附件", imageClass = "spafont spa-icon-newfile")

public class ModuleService {
    @MethodChinaName(cname = "获取附件列表")
    @RequestMapping(method = RequestMethod.POST, value = "GetAttacheMentList")
    public @ResponseBody
    ListResultModel<List<AttachMentView>> getAttacheMentList(String activityInstId, String activityInstHistoryId, String formname) {
        ListResultModel<List<AttachMentView>> resultModel = new ListResultModel<List<AttachMentView>>();
        try {
            if (activityInstId != null) {
                List<FileInfo> attachMents = new ArrayList<>();
                ActivityInst inst = getClient().getActivityInst(activityInstId);
                String processInstId = null;
                if (inst != null) {
                    processInstId = inst.getProcessInstId();
                }
                Folder processInstFolder = this.getProcessFolder(processInstId);
                if (inst.isCanPerform()) {
                    if (activityInstHistoryId != null) {
                        Folder activityInstHisFolder = (Folder) getVfsClient().getFolderByPath(processInstFolder.getPath() + activityInstId + "/" + activityInstHistoryId + "/" + formname + "/attachment");
                        if (activityInstHisFolder != null) {
                            List<FileInfo> fileInfos = activityInstHisFolder.getFileList();
                            attachMents.addAll(fileInfos);
                        }
                    }
                } else {
                    Folder activityInstFolder = getVfsClient().getFolderByPath(processInstFolder.getPath() + activityInstId + "/" + formname + "/attachment");
                    attachMents.addAll(activityInstFolder.getFileList());
                }
                resultModel = PageUtil.getDefaultPageList(attachMents, AttachMentView.class);
            }


        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }

    Folder getProcessFolder(String processInstId) throws JDSException {
        ProcessInst processInst = getClient().getProcessInst(processInstId);
        String processInstFolderId = processInst.getAttribute(ProcessInstAtt.ROOTFOLDERID.getType());
        Folder processInstFolder = null;
        if (processInstFolderId != null) {
            processInstFolder = (Folder) this.getVfsClient().getFolderById(processInstFolderId);
        }
        return processInstFolder;
    }


    public CtVfsService getVfsClient() {

        CtVfsService vfsClient = CtVfsFactory.getCtVfsService();
        return vfsClient;
    }


    /**
     * @return
     */
    private WorkflowClientService getClient() {
        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);
        return client;
    }
}
