package com.ds.esd.bpm.plugins.svgview;

import com.ds.bpm.client.ActivityInst;
import com.ds.bpm.client.ActivityInstHistory;
import com.ds.bpm.client.ProcessInst;
import com.ds.common.JDSException;
import com.ds.esd.bpm.node.*;
import com.ds.esd.bpm.view.Graph;
import com.ds.esd.bpm.view.GraphService;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.component.Component;
import com.ds.esd.tool.ui.component.svg.SVGPaperComponent;
import com.ds.esd.tool.ui.component.svg.SVGPaperProperties;
import com.ds.esd.tool.ui.component.svg.comb.circle.CircleProperties;
import com.ds.esd.tool.ui.component.svg.comb.connector.ConnectorProperties;
import com.ds.esd.tool.ui.component.svg.comb.rect.ActivityRectProperties;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.module.ModuleComponent;
import com.ds.jds.core.esb.EsbUtil;

import java.util.*;

public class SVGProcessInstView extends ModuleComponent {
    SVGProcessInstView() {
        super();
    }

    public SVGProcessInstView(EUModule module, MethodConfig methodAPIBean, Map<String, Object> map) {
        super(module);
        List<Component> components = new ArrayList<Component>();
        try {
            ProcessInst processInst = EsbUtil.parExpression("$currProcessInst", ProcessInst.class);
            this.euModule = module;
            SVGPaperComponent paperComponent = new SVGPaperComponent();
            SVGPaperProperties properties = new SVGPaperProperties();
            properties.setDock(Dock.fill);
            properties.setPanelBgClr("#FFFFFF");
            paperComponent.setProperties(properties);
            GraphService service = EsbUtil.parExpression(GraphService.class);

            String versionId = processInst.getProcessDefVersionId();
           Graph graph = service.getGraphInfo(versionId).get();
            List<RouteNode<ConnectorProperties>> routes = graph.getRouteNodes();
            for (RouteNode routeNode : routes) {
                paperComponent.addChildren(routeNode);
            }

            List<StartNode<CircleProperties>> startNodes = graph.getStartNodes();
            for (StartNode node : startNodes) {
                paperComponent.addChildren(node);
            }

            List<EndNode<CircleProperties>> endNodes = graph.getEndNodes();
            for (EndNode node : endNodes) {
                paperComponent.addChildren(node);
            }


            Set<String> instDefSet = new LinkedHashSet();
            List<ActivityInst> actvityInstList = processInst.getActivityInstList();
            for (ActivityInst inst : actvityInstList) {
                instDefSet.add(inst.getActivityDefId());
            }

            Set<String> hisDefSet = new LinkedHashSet();
            List<ActivityInstHistory> historyList = processInst.getActivityInstHistoryListByProcessInst();
            for (ActivityInstHistory his : historyList) {
                hisDefSet.add(his.getActivityDefId());
            }


            List<NoActiviteNode<ActivityRectProperties>> activitys = graph.getActiviteNode();
            for (NoActiviteNode node : activitys) {
                if (instDefSet.contains(node.getAlias())) {
                    paperComponent.addChildren(new InstActiviteNode(node));
                } else if (hisDefSet.contains(node.getAlias())) {
                    paperComponent.addChildren(new HisActiviteNode(node));
                } else {
                    paperComponent.addChildren(new WaitActiviteNode(node));
                }
            }
            this.addChildren(paperComponent);

        } catch (JDSException e) {
            e.printStackTrace();
        }
    }


}
