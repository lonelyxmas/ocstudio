package com.ds.esd.bpm.plugins.opinion;

import com.ds.bpm.client.ActivityInst;
import com.ds.bpm.client.ct.CtBPMCacheManager;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.BpmDomain;
import com.ds.esd.dsm.domain.enums.BpmDomainType;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.org.conf.OrgConstants;
import com.ds.vfs.FileInfo;
import com.ds.vfs.Folder;
import com.ds.vfs.ct.CtVfsFactory;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/bpm/plugins/opinion/")
@MethodChinaName(cname = "意见", imageClass = "spafont spa-icon-action1")
@BpmDomain(type = BpmDomainType.custom)
@Aggregation(type = AggregationType.customDomain, rootClass = OpinionService.class)
public class OpinionService {

    @MethodChinaName(cname = "获取意见列表")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "获取意见列表")
    @RequestMapping(method = RequestMethod.POST, value = "OpinionsList")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.index, CustomMenuItem.reload})
    public @ResponseBody
    ListResultModel<List<OpinionView>> getOpinionsList(String activityInstId, String id) {
        ListResultModel<List<OpinionView>> resultModel = new ListResultModel<List<OpinionView>>();
        try {
            ActivityInst inst = getClient().getActivityInst(activityInstId);
            String processInstId = null;
            if (inst != null) {
                processInstId = inst.getProcessInstId();
            } else {
                processInstId = activityInstId;
            }
            String path = OrgConstants.CMAILROOTPATH + processInstId + "/" + id + "/opinionname";
            Folder foleder = CtVfsFactory.getCtVfsService().mkDir(path);
            List<FileInfo> fileInfos = foleder.getFileList();
            resultModel = PageUtil.getDefaultPageList(fileInfos, OpinionView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }

    @MethodChinaName(cname = "添加意见")
    @RequestMapping(method = RequestMethod.POST, value = "addOpinion")
    @APIEventAnnotation(bindMenu = CustomMenuItem.save, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> addOpinion(String activityInstId, String content, String formname) {
        ResultModel<Boolean> resultModel = new ResultModel<Boolean>();
        List<FileInfo> fileInfos = new ArrayList<FileInfo>();
        try {
            ActivityInst inst = CtBPMCacheManager.getInstance().getActivityInst(activityInstId);
            String path = OrgConstants.CMAILROOTPATH + inst.getActivityInstId() + "/" + formname + "/opinion";
            Folder foleder = CtVfsFactory.getCtVfsService().mkDir(path);
            FileInfo fileInfo = foleder.createFile(formname, getClient().getConnectInfo().getUserID());
            CtVfsFactory.getCtVfsService().saveFileAsContent(fileInfo.getPath(), content, getClient().getConnectInfo().getUserID());
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }

    @MethodChinaName(cname = "删除意见")
    @RequestMapping(method = RequestMethod.POST, value = "delOpinion")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    public @ResponseBody
    ResultModel<Boolean> delOpinion(String paths) {
        ResultModel<Boolean> resultModel = new ResultModel<Boolean>();
        String[] pathArr = StringUtility.split(paths, ";");
        List<FileInfo> fileInfos = new ArrayList<FileInfo>();
        try {
            for (String path : pathArr) {
                FileInfo file = CtVfsFactory.getCtVfsService().getFileByPath(path);
                CtVfsFactory.getCtVfsService().deleteFile(file.getID());
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return resultModel;

    }

    /**
     * @return
     */
    private WorkflowClientService getClient() {
        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);
        return client;
    }
}
