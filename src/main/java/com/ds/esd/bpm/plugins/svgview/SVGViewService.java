package com.ds.esd.bpm.plugins.svgview;


import com.ds.bpm.engine.WorkflowClientService;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.BpmDomain;
import com.ds.esd.dsm.domain.enums.BpmDomainType;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/bpm/plugins/performview/")
@MethodChinaName(cname = "历程", imageClass = "bpmfont bpmgongzuoliuzhutiguizeweihuguanli")
@BpmDomain(type = BpmDomainType.route)
@Aggregation(type = AggregationType.customDomain, rootClass = SVGViewService.class)
public class SVGViewService {
    @MethodChinaName(cname = "查看历程")
    @RequestMapping(method = RequestMethod.POST, value = "ProcessView")
    @DynLoadAnnotation()
    @DialogAnnotation
    @ModuleAnnotation(caption = "查看历程", imageClass = "bpmfont bpmgongzuoliuzhutiguizeweihuguanli")
    @ResponseBody
    public ResultModel<SVGProcessInstView> processView(String activityInstId, String nextActivityDefId) {
        ResultModel<SVGProcessInstView> resultModel = new ResultModel<SVGProcessInstView>();
        return resultModel;
    }

    /**
     * @return
     */
    private WorkflowClientService getClient() {
        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);        return client;
    }
}
