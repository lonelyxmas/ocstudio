package com.ds.esd.bpm.plugins.attachment;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;
import com.ds.org.PersonNotFoundException;
import com.ds.server.OrgManagerFactory;
import com.ds.vfs.FileInfo;

import java.util.Date;

@PageBar
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.floatright, menuClass = {AttachMentCMDAction.class})
@GridAnnotation(customMenu = {GridMenu.Add, GridMenu.Reload, GridMenu.Delete}, customService = {AttachMentService.class})
public class AttachMentView {
    @CustomAnnotation(pid = true, hidden = true)
    String activityInstId;
    @CustomAnnotation(pid = true, hidden = true)
    String formname;
    @CustomAnnotation(pid = true, hidden = true)
    String fileId;
    @CustomAnnotation(caption = "文件名称")
    String fielName;
    @CustomAnnotation(caption = "上传人")
    String personname;
    @CustomAnnotation(caption = "上传时间")
    Date time;
    @CustomAnnotation(caption = "上传路径", uid = true, hidden = true)
    String path;
    @CustomAnnotation(caption = "版本号", hidden = true)
    String version;

    public AttachMentView(FileInfo fileInfo) {
        this.fielName = fileInfo.getDescrition();
        this.time = new Date(fileInfo.getCreateTime());
        this.path = fileInfo.getPath();
        this.fileId = fileInfo.getID();
        this.version = fileInfo.getCurrentVersion().getVersionName();
        try {
            this.personname = OrgManagerFactory.getOrgManager().getPersonByID(fileInfo.getPersonId()).getName();
        } catch (PersonNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getActivityInstId() {
        return activityInstId;
    }

    public void setActivityInstId(String activityInstId) {
        this.activityInstId = activityInstId;
    }

    public String getFormname() {
        return formname;
    }

    public void setFormname(String formname) {
        this.formname = formname;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getPersonname() {
        return personname;
    }

    public void setPersonname(String personname) {
        this.personname = personname;
    }

    public String getFielName() {
        return fielName;
    }

    public void setFielName(String fielName) {
        this.fielName = fielName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getTime() {
        return time;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


}
