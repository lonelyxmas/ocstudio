package com.ds.esd.bpm.service;

import com.ds.bpm.client.ct.BPMClientFactory;
import com.ds.bpm.engine.BPMException;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.common.JDSException;
import com.ds.common.expression.function.AbstractFunction;
import com.ds.server.JDSClientService;
import com.ds.server.JDSServer;

public class GetBPMClient extends AbstractFunction {

    public WorkflowClientService perform(JDSClientService clientService) throws BPMException {

        if (clientService == null) {
            try {
                clientService = JDSServer.getInstance().getAdminClient();
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }

        if (clientService == null) {
            throw new BPMException("session失效,请重新登录！", BPMException.NOTLOGINEDERROR);
        }


        WorkflowClientService client = null;
        try {
            client = BPMClientFactory.getWorkflowClient(clientService);
        } catch (JDSException e) {
            throw new BPMException(e.getMessage(), BPMException.NOTLOGINEDERROR);
        }
        if (client == null) {
            throw new BPMException(" SessionHandle:[" + clientService.getSessionHandle() + "]未获取流程授权！", BPMException.NOTLOGINEDERROR);
        }
        return client;
    }
}