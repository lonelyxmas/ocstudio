package com.ds.esd.bpm.form.demo.view;
import com.ds.common.JDSException;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.bpm.client.ActivityInst;
import com.ds.bpm.engine.BPMException;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.config.ResultModel;
import com.ds.esb.config.EsbBeanAnnotation;



@EsbBeanAnnotation
public class HaDeviceServiceImpl implements DemDeviceService {


    public ResultModel<GetHaDeviceInfoView> getHaDeviceInfo(String tableName, String formname, String configKey, String activityInstId, String processInstId)  throws JDSException{
        ResultModel<GetHaDeviceInfoView> resultModel = new ResultModel<GetHaDeviceInfoView>();
        try {
            WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);
            ActivityInst activityInst = client.getActivityInst(activityInstId);
            GetHaDeviceInfoView view = activityInst.getFormValues().getDAO(formname, GetHaDeviceInfoView.class);
            resultModel.setData(view);
        } catch (BPMException e) {
            e.printStackTrace();
            throw new JDSException(e);
        }
        return resultModel;
    }

    ;

    /**
     * @return
     */
    private WorkflowClientService getClient() {
        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);
        return client;
    }

}
