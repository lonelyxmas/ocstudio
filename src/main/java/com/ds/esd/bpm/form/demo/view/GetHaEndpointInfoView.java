package com.ds.esd.bpm.form.demo.view;


import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.web.annotation.Pid;


@MenuBarMenu
@FormAnnotation()
public interface GetHaEndpointInfoView {


    @Pid
    public String getTableName();

    public void setTableName(String name);

    @Pid
    public String getConfigKey();

    public void setConfigKey(String configKey);


    @CustomAnnotation(caption = "模块名称")
    public String getName();

    public void setName(String name);

    @CustomAnnotation(caption = "当前值")
    public String getValue();

    public void setValue(String value);

    @CustomAnnotation(caption = "传感器类型")
    public Integer getSensortype();

    public void setSensortype(Integer sensortype);

    @CustomAnnotation(caption = "EP地址")
    public String getEp();

    public void setEp(String ep);

    @CustomAnnotation(caption = "HAdevi")
    public String getHadeviceid();

    public void setHadeviceid(String hadeviceid);

    @CustomAnnotation(caption = "HA配置信息")
    public String getProfileid();

    public void setProfileid(String profileid);

    @CustomAnnotation(caption = "模块名称")
    public String getDeviceid();

    public void setDeviceid(String deviceid);

    @CustomAnnotation(caption = "无线接入地址")
    public String getIeeeaddress();

    public void setIeeeaddress(String ieeeaddress);

    @CustomAnnotation(caption = "物理短地址信")
    public String getNwkaddress();

    public void setNwkaddress(String nwkaddress);

    @CustomAnnotation(uid = true)
    public String getEndpointid();

    public void setEndpointid(String endpointid);


}
