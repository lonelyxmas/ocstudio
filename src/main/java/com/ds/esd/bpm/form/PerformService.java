package com.ds.esd.bpm.form;

import com.ds.bpm.client.ActivityInst;
import com.ds.bpm.client.ProcessDefForm;
import com.ds.bpm.client.ProcessDefVersion;
import com.ds.bpm.client.RouteToType;
import com.ds.bpm.client.data.DataMap;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.api.enums.ResponsePathEnum;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.bpm.RouteCustomMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.dsm.repository.database.proxy.DSMTableProxy;
import com.ds.jds.core.esb.EsbUtil;
import net.sf.cglib.beans.BeanMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("/bpm/custom/")
@MethodChinaName(cname = "流程流转", imageClass = "bpmfont bpmqidonggongzuoliu")
public class PerformService {

    @RequestMapping(method = RequestMethod.POST, value = "Display")
    @RouteCustomMenu(routeType = {RouteToType.Reload})
    @NavTabsViewAnnotation
    @DialogAnnotation
    @ModuleAnnotation(caption = "办理", name = "table", dynLoad = true)
    @APIEventAnnotation(bindMenu = CustomMenuItem.editor, customRequestData = RequestPathEnum.ctx, customResponseData = ResponsePathEnum.form)
    @ResponseBody
    public TreeListResultModel<List<TableFormTabs>> display(String activityInstId, String processInstId, String domainId, String projectName) {
        TreeListResultModel<List<TableFormTabs>> formModule = new TreeListResultModel<List<TableFormTabs>>();
        List<TableFormTabs> formTabs = new ArrayList<>();
        try {
            ActivityInst activityInst = this.getClient().getActivityInst(activityInstId);
            ProcessDefVersion processDefVersion = activityInst.getProcessDefVersion();
            ProcessDefForm processDefForm = processDefVersion.getFormDef();
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            if (domainId == null || domainId.equals("")) {
                domainId = project.getProjectName();
            }
            List<String> tableNams = processDefForm.getTableNames();
            DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            for (String tableName : tableNams) {
                DSMTableProxy proxy = DSMFactory.getInstance().getRepositoryManager().getTableProxyByName(tableName, projectName);
                TableFormTabs tableFormTab = new TableFormTabs();
                tableFormTab.setActivityInstId(activityInstId);
                tableFormTab.setConfigKey(proxy.getConfigKey());
                tableFormTab.setFormname(proxy.getFieldName());
                tableFormTab.setTableName(tableName);
                tableFormTab.setProcessInstId(processInstId);
                tableFormTab.setId(proxy.getFieldName());
                tableFormTab.setCaption(proxy.getCnname());
                tableFormTab.setProcessDefVersionId(processDefVersion.getProcessDefVersionId());
                Map<String, String> baseMap = new HashMap<>();
                baseMap.put("tableName", tableName);
                baseMap.put("formname", proxy.getFieldName());
                baseMap.put("configKey", proxy.getConfigKey());
                if (tableFormTab.getTagVar() == null) {
                    tableFormTab.setTagVar(new HashMap<>());
                }
                tableFormTab.getTagVar().putAll(baseMap);
                ESDClass daoService = domainInst.getDAOService(tableName);
                if (daoService != null) {
                    ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(daoService.getClassName(), domainId);
                    MethodConfig methodConfig = apiClassConfig.getMethodByItem(CustomMenuItem.tabEditor);
                    if (methodConfig == null) {
                        methodConfig = apiClassConfig.getMethodByItem(CustomMenuItem.editor);
                    }
                    if (methodConfig != null) {
                        tableFormTab.setEuClassName(methodConfig.getEUClassName());
                    }
                    formTabs.add(tableFormTab);
                }
            }
            if (formTabs.size() > 0) {
                formModule.setData(formTabs);
                formModule.setIds(Arrays.asList(formTabs.get(0).getFormname()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return formModule;
    }

    /**
     * @return
     */
    private WorkflowClientService getClient() {

        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);

        return client;
    }
}
