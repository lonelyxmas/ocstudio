package com.ds.esd.bpm.form.demo.view;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.BlockAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.api.enums.ResponsePathEnum;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@RequestMapping(value = "/bpm/demo/hadevice")
@Controller
public interface DemDeviceService {


    @APIEventAnnotation(bindMenu = {CustomMenuItem.tabEditor},autoRun = true,customRequestData = RequestPathEnum.ctx, customResponseData = ResponsePathEnum.form)
    @RequestMapping(value = "HaDeviceInfo")
    @ResponseBody
    @ModuleAnnotation
    @BlockAnnotation
    @FormViewAnnotation
    public ResultModel<GetHaDeviceInfoView> getHaDeviceInfo(String tableName, String formname, String configKey, String activityInstId, String processInstId) throws JDSException;


}
