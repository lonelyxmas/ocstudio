package com.ds.esd.bpm.form.demo.view;



import com.ds.esd.bpm.plugins.attachment.AttachMentService;
import com.ds.esd.bpm.plugins.attachment.AttachMentView;
import com.ds.esd.bpm.plugins.opinion.OpinionService;
import com.ds.esd.bpm.plugins.opinion.OpinionView;
import com.ds.esd.custom.annotation.ComboModuleAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.enums.AppendType;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.web.annotation.Pid;

import java.util.List;


@FormAnnotation()
public interface GetHaDeviceInfoView {

    @Pid
    public String getTableName();

    public void setTableName(String name);

    @Pid
    public String getConfigKey();

    public void setConfigKey(String configKey);

    @CustomAnnotation(caption = "设备名称")
    public String getName();

    public void setName(String name);

    @CustomAnnotation(caption = "工厂名称")
    public String getFactory();

    public void setFactory(String factory);

    @CustomAnnotation(caption = "渠道")
    public String getChannel();

    public void setChannel(String channel);

    @CustomAnnotation(caption = "主类型")
    public String getSensortype();

    public void setSensortype(String sensortype);

    @CustomAnnotation(caption = "当前值")
    public String getCurrvalue();

    public void setCurrvalue(String currvalue);

    @CustomAnnotation(caption = "序列号")
    public String getSerialno();

    public void setSerialno(String serialno);

    @CustomAnnotation(caption = "APP主账户")
    public String getAppaccount();

    public void setAppaccount(String appaccount);

    @CustomAnnotation(caption = "安装位置")
    public String getAreaid();

    public void setAreaid(String areaid);

    @CustomAnnotation(caption = "状态")
    public Integer getStates();

    public void setStates(Integer states);

    @CustomAnnotation(caption = "批次")
    public String getBatch();

    public void setBatch(String batch);

    @CustomAnnotation(caption = "子系统ID")
    public String getSubsyscode();

    public void setSubsyscode(String subsyscode);

    @CustomAnnotation(caption = "安装房间")
    public String getPlaceid();

    public void setPlaceid(String placeid);

    @CustomAnnotation(uid = true)
    public String getDeviceid();

    public void setDeviceid(String deviceid);

    @CustomAnnotation(caption = "电量")
    public String getBattery();

    public void setBattery(String battery);

    @CustomAnnotation(caption = "绑定账户")
    public String getBindingaccount();

    public void setBindingaccount(String bindingaccount);

    @CustomAnnotation(caption = "MAC 地址")
    public String getMacaddress();

    public void setMacaddress(String macaddress);

    @CustomAnnotation(caption = "设备类型对应")
    public Integer getDevicetype();

    public void setDevicetype(Integer devicetype);

    @CustomAnnotation(caption = "添加时间")
    public Integer getAddtime();

    public void setAddtime(Integer addtime);


    @ComboModuleAnnotation(append = AppendType.ref, bindClass = AttachMentService.class)
    @FieldAnnotation(colSpan = -1, rowHeight = "200", haslabel = false)
    @CustomAnnotation(caption = "附件列表")
    List<AttachMentView> getAttacheMentList();

    @ComboModuleAnnotation(append = AppendType.ref, bindClass = OpinionService.class)
    @FieldAnnotation(colSpan = -1, rowHeight = "300", haslabel = false)
    @CustomAnnotation(caption = "审批意见")
    List<OpinionView> getOpinionList();


}
