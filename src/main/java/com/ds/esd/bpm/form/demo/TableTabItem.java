package com.ds.esd.bpm.form.demo;

import com.ds.esd.bpm.form.demo.view.DemDeviceService;
import com.ds.esd.bpm.form.demo.view.DemoEndpointService;
import com.ds.esd.custom.nav.tab.enums.TabItem;


public enum TableTabItem implements TabItem {


    HaDevice("设备", "HA_DEVICE", "fdt", DemDeviceService.class, false, false, false),
    HaEndpoint("端点", "HA_DEVICE", "fdt", DemoEndpointService.class, false, false, false);


    private String name;

    private String tableName;

    private String caption;

    private String configKey;

    private String imageClass;


    private  Class bindClass;
    private  boolean iniFold;
    private  boolean dynDestory;
    private  boolean dynLoad;

    TableTabItem(String tableName,String domainId) {

    }



    TableTabItem(String cnName, String tableName, String configKey, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {

        this.configKey = configKey;
        this.tableName = tableName;
        this.bindClass = bindClass;
        this.caption = cnName;
        this.name = name();
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    @Override
    public String getType() {
        return name();
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    public String getImageClass() {
        return imageClass;
    }

}
