package com.ds.esd.bpm.form.demo.view;

import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.BlockAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping(value = "/bpm/demo/haendpoint")
@Controller
public interface DemoEndpointService {


    @APIEventAnnotation(bindMenu = {CustomMenuItem.tabEditor})
    @RequestMapping(value = "HaEndpointInfo")
    @ResponseBody
    @ModuleAnnotation
    @BlockAnnotation
    @FormViewAnnotation
    public ResultModel<GetHaEndpointInfoView> getHaEndpointInfo(String tableName, String formname, String configKey, String activityInstId, String processInstId);


}
