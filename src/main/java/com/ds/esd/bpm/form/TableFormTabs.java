package com.ds.esd.bpm.form;

import com.ds.esd.bpm.custom.PerformServiceImpl;
import com.ds.esd.bpm.form.demo.TableTabItem;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.tool.ui.component.item.TabListItem;
import com.ds.web.annotation.Pid;

@MenuBarMenu(menuClasses = PerformServiceImpl.class, dynLoad = true, lazy = true)
@TabsAnnotation(autoReload = false)
public class TableFormTabs extends TabListItem {

    @Pid
    String activityInstId;
    @Pid
    String processInstId;
    @Pid
    String processDefVersionId;
    @Pid
    String configKey;
    @Pid
    String tableName;
    @Pid
    String formname;

    public TableFormTabs() {

    }

    public TableFormTabs(TableTabItem tableTabItem, String activityInstId, String processInstId) {
        this.tableName = tableTabItem.getTableName();
        this.id = tableTabItem.getName();
        this.caption = tableTabItem.getCaption();
        this.activityInstId = activityInstId;
        this.processInstId = processInstId;
        this.configKey = tableTabItem.getConfigKey();
        this.formname = tableTabItem.getName();
    }

    public String getProcessDefVersionId() {
        return processDefVersionId;
    }

    public void setProcessDefVersionId(String processDefVersionId) {
        this.processDefVersionId = processDefVersionId;
    }

    public String getActivityInstId() {
        return activityInstId;
    }

    public void setActivityInstId(String activityInstId) {
        this.activityInstId = activityInstId;
    }

    public String getProcessInstId() {
        return processInstId;
    }

    public void setProcessInstId(String processInstId) {
        this.processInstId = processInstId;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFormname() {
        return formname;
    }

    public void setFormname(String formname) {
        this.formname = formname;
    }
}
