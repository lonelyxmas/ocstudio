package com.ds.esd.bpm.worklist.sevice;

import com.ds.bpm.engine.BPMException;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.form.PerformService;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.jds.core.esb.EsbUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/bpm/custom/")
public class NotStartProcessService extends PerformService {

    @MethodChinaName(cname = "删除草稿")
    @RequestMapping(method = RequestMethod.POST, value = "deleteProcessInst")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> deleteProcessInst(String activityInstId) {
        List<String> activityInstIds = Arrays.asList(StringUtility.split(activityInstId, ";"));
        for (String activityId : activityInstIds) {
            try {
                String processInstId = getClient().getActivityInst(activityId).getProcessInstId();
                getClient().deleteProcessInst(processInstId, null);
            } catch (BPMException e) {
                e.printStackTrace();
            }

        }
        return new ResultModel<>();
    }

     WorkflowClientService getClient() {

         WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);

        return client;
    }
}
