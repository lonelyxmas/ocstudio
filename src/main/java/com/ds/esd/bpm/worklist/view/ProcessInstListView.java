package com.ds.esd.bpm.worklist.view;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.bpm.client.ProcessInst;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.bpm.enums.process.ProcessInstAtt;
import com.ds.bpm.enums.process.ProcessInstStatus;
import com.ds.esd.bpm.custom.action.ProcessDefRowAction;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.org.Person;
import com.ds.server.OrgManagerFactory;

import java.util.Date;

@PageBar
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left,menuClass = {ProcessDefRowAction.class})
@GridAnnotation( customMenu = {GridMenu.Reload}, event = CustomGridEvent.editor)
public class ProcessInstListView {

    @CustomAnnotation(hidden = true)
    String processInstId;

    @CustomAnnotation(caption = "流程名称")
    String processDefName;

    @CustomAnnotation(caption = "标题")
    String title;

    @CustomAnnotation(caption = "所在步骤")
    String activityDefName;

    @CustomAnnotation(caption = "状态")
    ProcessInstStatus state;
    ;

    @CustomAnnotation(caption = "运行状态")
    ProcessInstStatus runStatus;

    @CustomAnnotation(caption = "结束时间")
    Date endTime;

    @CustomAnnotation(caption = "开始时间")
    Date startTime;

    @CustomAnnotation(caption = "发起人")
    String startPersonName;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ProcessInstListView(ProcessInst inst) {

        try {

            this.processInstId = inst.getProcessInstId();
            this.startTime = inst.getStartTime();
            this.runStatus = inst.getRunStatus();
            this.state = inst.getState();
            this.processDefName = inst.getProcessDef().getName();
            this.title = inst.getName();
            this.endTime = inst.getEndTime();
            this.activityDefName = inst.getActivityInstList().get(0).getActivityDef().getName();
            String personId = (String) inst.getRightAttribute(ProcessInstAtt.PROCESS_INSTANCE_STARTER);
            if (personId != null) {
                Person person = OrgManagerFactory.getOrgManager().getPersonByID(personId);
                startPersonName = person.getName();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void setProcessInstId(String processInstId) {
        this.processInstId = processInstId;
    }

    public void setProcessDefName(String processDefName) {
        this.processDefName = processDefName;
    }

    public void setActivityDefName(String activityDefName) {
        this.activityDefName = activityDefName;
    }

    public ProcessInstStatus getState() {
        return state;
    }

    public void setState(ProcessInstStatus state) {
        this.state = state;
    }

    public ProcessInstStatus getRunStatus() {
        return runStatus;
    }

    public void setRunStatus(ProcessInstStatus runStatus) {
        this.runStatus = runStatus;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setStartPersonName(String startPersonName) {
        this.startPersonName = startPersonName;
    }

    public String getProcessInstId() {
        return processInstId;
    }

    public String getProcessDefName() {
        return processDefName;
    }

    public String getActivityDefName() {
        return activityDefName;
    }

    public Date getEndTime() {
        return endTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public String getStartPersonName() {
        return startPersonName;
    }

    @JSONField(serialize = false)
    private WorkflowClientService getClient() {

        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);

        return client;
    }

}
