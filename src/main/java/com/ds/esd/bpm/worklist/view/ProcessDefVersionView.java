package com.ds.esd.bpm.worklist.view;

import com.ds.bpm.client.ProcessDefVersion;
import com.ds.bpm.enums.process.ProcessDefAccess;
import com.ds.bpm.enums.process.ProcessDefVersionStatus;
import com.ds.esd.bpm.custom.action.ProcessDefRowAction;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.LabelAnnotation;
import com.ds.esd.custom.grid.annotation.*;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;

import java.util.Date;

@PageBar
@RowHead(selMode = SelModeType.none, rowHandlerWidth = "12em", rowNumbered = false)
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, menuClass = {ProcessDefRowAction.class})
@GridAnnotation(customMenu = {GridMenu.Reload})
public class ProcessDefVersionView {

    @GridColItemAnnotation(width = "10em")
    @CustomAnnotation(caption = "定义名称")
    private String processDefName;

    @LabelAnnotation
    @GridColItemAnnotation(width = "6em")
    @CustomAnnotation(caption = "版本号")
    private int version;
    @GridColItemAnnotation(width = "6em")
    @CustomAnnotation(caption = "版本状态")
    private ProcessDefVersionStatus publicationStatus;
    @GridColItemAnnotation(width = "6em")
    @CustomAnnotation(caption = "创建人")
    private String creatorName;
    @GridColItemAnnotation(width = "6em", inputType = ComboInputType.datetime)
    @CustomAnnotation(caption = "激活时间")
    private Date activeTime;

    @CustomAnnotation(hidden = true, uid = true)
    private String processDefVersionId;
    @CustomAnnotation(hidden = true)
    private String processDefId;

    @CustomAnnotation(caption = "流程类型")
    private ProcessDefAccess accessLevel;


    public ProcessDefVersionView(ProcessDefVersion processDefVersion) {
        this.accessLevel = processDefVersion.getAccessLevel();
        this.processDefName = processDefVersion.getProcessDefName();
        this.processDefId = processDefVersion.getProcessDefId();
        this.activeTime = processDefVersion.getActiveTime();
        this.creatorName = processDefVersion.getCreatorName();
        this.processDefVersionId = processDefVersion.getProcessDefVersionId();
        this.publicationStatus = processDefVersion.getPublicationStatus();
        this.version = processDefVersion.getVersion();
    }


    public String getProcessDefVersionId() {
        return processDefVersionId;
    }

    public void setProcessDefVersionId(String processDefVersionId) {
        this.processDefVersionId = processDefVersionId;
    }


    public String getProcessDefId() {
        return processDefId;
    }

    public void setProcessDefId(String processDefId) {
        this.processDefId = processDefId;
    }


    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }


    public String getProcessDefName() {
        return processDefName;
    }

    public void setProcessDefName(String processDefName) {
        this.processDefName = processDefName;
    }


    public ProcessDefVersionStatus getPublicationStatus() {
        return publicationStatus;
    }

    public void setPublicationStatus(ProcessDefVersionStatus publicationStatus) {
        this.publicationStatus = publicationStatus;
    }


    public Date getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(Date activeTime) {
        this.activeTime = activeTime;
    }


    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }


    public ProcessDefAccess getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(ProcessDefAccess accessLevel) {
        this.accessLevel = accessLevel;
    }


}
