package com.ds.esd.bpm.worklist.view;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.bpm.client.ActivityInst;
import com.ds.bpm.client.ActivityInstHistory;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.bpm.enums.activityinst.ActivityInstReceiveMethod;
import com.ds.bpm.enums.activityinst.ActivityInstRightAtt;
import com.ds.bpm.enums.activityinst.ActivityInstRunStatus;
import com.ds.bpm.enums.activityinst.ActivityInstStatus;
import com.ds.bpm.enums.process.ProcessInstAtt;
import com.ds.esd.bpm.form.PerformService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.org.Person;
import com.ds.server.OrgManagerFactory;

import java.util.Date;
import java.util.List;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload}, customService = {PerformService.class}, event = CustomGridEvent.editor)
public class ActivityInstListView {
    @CustomAnnotation(hidden = true)
    String activityInstId;
    @CustomAnnotation(hidden = true)
    String processInstId;
    @CustomAnnotation(hidden = true)
    String activityInstHistoryId;

    @CustomAnnotation(caption = "流程名称")
    String processDefName;

    @CustomAnnotation(caption = "所在步骤")
    String activityDefName;

    @CustomAnnotation(caption = "状态")
    ActivityInstStatus state;

    @CustomAnnotation(caption = "标题")
    String title;

    @CustomAnnotation(caption = "运行状态")
    ActivityInstRunStatus runStatus;

    @CustomAnnotation(caption = "到达时间")
    Date arrivedTime;

    @CustomAnnotation(caption = "预警时间")
    Date alertTime;

    @CustomAnnotation(caption = "结束时间")
    Date endTime;

    @CustomAnnotation(caption = "开始时间")
    Date startTime;

    @CustomAnnotation(caption = "流程启动时间")
    Date processStartTime;

    @CustomAnnotation(caption = "办理人")
    String personName;

    @CustomAnnotation(caption = "发起人")
    String startPersonName;

    @CustomAnnotation(caption = "接收方式", hidden = true)
    ActivityInstReceiveMethod receiveMethod;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ActivityInstListView(ActivityInst inst) {

        try {
            this.activityInstId = inst.getActivityInstId();
            this.processInstId = inst.getProcessInstId();
            this.alertTime = inst.getAlertTime();
            this.arrivedTime = inst.getArrivedTime();
            this.startTime = inst.getStartTime();
            this.runStatus = inst.getRunStatus();
            this.state = inst.getState();
            this.processDefName = inst.getProcessDef().getName();
            this.activityDefName = inst.getActivityDef().getName();
            this.processStartTime = inst.getProcessInst().getStartTime();
            this.receiveMethod = inst.getReceiveMethod();
            this.title = inst.getProcessInst().getName();

            List<Person> personList = getClient().getActivityInstPersons(inst.getActivityInstId(), ActivityInstRightAtt.PERFORMER);
            if (personList == null || personList.size() == 0) {
                personList = getClient().getActivityInstPersons(inst.getActivityInstId(), ActivityInstRightAtt.READER);
                if (personList != null && personList.size() > 0) {
                    Person person = personList.get(0);
                    personName = person.getName();
                }
            } else if (personList.size() > 0) {
                Person person = personList.get(0);
                personName = person.getName();
            }


            String personId = (String) inst.getProcessInst().getRightAttribute(ProcessInstAtt.PROCESS_INSTANCE_STARTER);
            if (personId != null) {
                Person person = OrgManagerFactory.getOrgManager().getPersonByID(personId);
                startPersonName = person.getName();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public ActivityInstListView(ActivityInstHistory inst) {

        try {
            this.activityInstId = inst.getActivityInstId();
            this.processInstId = inst.getProcessInstId();
            this.arrivedTime = inst.getArrivedTime();
            this.startTime = inst.getStartTime();
            this.processStartTime = inst.getProcessInst().getStartTime();
            this.runStatus = ActivityInstRunStatus.NORMAL;
            this.state = ActivityInstStatus.completed;
            this.processDefName = inst.getActivityDef().getProcessDef().getName();
            this.activityDefName = inst.getActivityDef().getName();
            this.endTime = inst.getEndTime();
            this.receiveMethod = inst.getReceiveMethod();
            List<Person> personList = getClient().getActivityInstPersons(inst.getActivityInstId(), ActivityInstRightAtt.PERFORMER);
            if (personList == null || personList.size() == 0) {
                personList = (List<Person>) getClient().getActivityInstPersons(inst.getActivityInstId(), ActivityInstRightAtt.READER);
                if (personList != null && personList.size() > 0) {
                    Person person = personList.get(0);
                    personName = person.getName();
                }
            } else if (personList.size() > 0) {
                Person person = personList.get(0);
                personName = person.getName();
            }


            String personId = (String) inst.getProcessInst().getRightAttribute(ProcessInstAtt.PROCESS_INSTANCE_STARTER);
            if (personId != null) {
                Person person = OrgManagerFactory.getOrgManager().getPersonByID(personId);
                startPersonName = person.getName();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public String getActivityInstId() {
        return activityInstId;
    }

    public String getProcessInstId() {
        return processInstId;
    }

    public String getActivityInstHistoryId() {
        return activityInstHistoryId;
    }

    public String getProcessDefName() {
        return processDefName;
    }

    public String getActivityDefName() {
        return activityDefName;
    }

    public ActivityInstStatus getState() {
        return state;
    }

    public ActivityInstRunStatus getRunStatus() {
        return runStatus;
    }

    public Date getArrivedTime() {
        return arrivedTime;
    }

    public Date getAlertTime() {
        return alertTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getProcessStartTime() {
        return processStartTime;
    }

    public String getPersonName() {
        return personName;
    }

    public String getStartPersonName() {
        return startPersonName;
    }

    public ActivityInstReceiveMethod getReceiveMethod() {
        return receiveMethod;
    }

    @JSONField(serialize = false)
    private WorkflowClientService getClient() {

        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);

        return client;
    }

}
