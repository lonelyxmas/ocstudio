package com.ds.esd.bpm.worklist;

import com.ds.bpm.client.*;
import com.ds.bpm.engine.BPMException;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.bpm.enums.process.ProcessInstAtt;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.custom.perform.FormTabs;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.api.enums.ResponsePathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.custom.toolbar.bpm.RouteCustomMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.domain.annotation.BpmDomain;
import com.ds.esd.dsm.domain.enums.BpmDomainType;
import com.ds.esd.dsm.repository.database.proxy.DSMTableProxy;
import com.ds.esd.util.TabPageUtil;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/bpm/custom/processInst/")
@MethodChinaName(cname = "流程流转")
@BpmDomain(type = BpmDomainType.route)
@Aggregation(type = AggregationType.customDomain, rootClass = ProcessInstService.class)
public class ProcessInstService {


    @MethodChinaName(cname = "删除流程")
    @RequestMapping(method = RequestMethod.POST, value = "delProcess")
    public @ResponseBody
    ResultModel<Boolean> delProcess(String processInstIds) {
        String[] processInstIdArr = StringUtility.split(processInstIds, ";");
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            for (String processInstId : processInstIdArr) {
                getClient().deleteProcessInst(processInstId, new HashMap<>());
            }
        } catch (BPMException e) {
            result = new ErrorResultModel();
            ((ErrorResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;

    }

    @MethodChinaName(cname = "结束流程")
    @RequestMapping(method = RequestMethod.POST, value = "completeProcessInst")
    public @ResponseBody
    ResultModel<Boolean> completeProcessInst(String processInstId) {
        try {
            getClient().completeProcessInst(processInstId, null);
        } catch (BPMException e) {
            e.printStackTrace();
        }
        return new ResultModel();
    }


    @RouteCustomMenu(routeType = {RouteToType.Reload})
    @NavTabsViewAnnotation()
    @CustomAnnotation()
    @RequestMapping(method = RequestMethod.POST, value = "NewProcess")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.treegridrow, RequestPathEnum.ctx}, customResponseData = ResponsePathEnum.form)
    @DialogAnnotation
    @ModuleAnnotation(dynLoad = true, imageClass = "bpmfont bpmgongzuoliuzhutiguizeweihuguanli", caption = "新建流程")
    public @ResponseBody
    ListResultModel<List<FormTabs>> newProcess(String processDefVersionId) {
        ListResultModel<List<FormTabs>> formTabsTree = new ListResultModel<>();
        List<DSMTableProxy> tableProxyList = new ArrayList<>();
        try {
            if (processDefVersionId != null) {
                ProcessDefVersion version = getClient().getProcessDefVersion(processDefVersionId);
                ProcessInst processInst = getClient().newProcess(version.getProcessDefId(), null, null, null);
                ActivityInst activityInst = processInst.getActivityInstList().get(0);
                processInst.getRightAttribute(ProcessInstAtt.PROCESS_INSTANCE_STARTER);
                ProcessDefVersion processDefVersion = activityInst.getProcessDefVersion();
                String domainId = processDefVersion.getAttribute("domainId");
                ProcessDefForm formDef = processDefVersion.getFormDef();
                List<String> tableNames = formDef.getTableNames();
                for (String tableName : tableNames) {
                    DSMTableProxy proxy = DSMFactory.getInstance().getRepositoryManager().getTableProxyByName(tableName, domainId);
                    tableProxyList.add(proxy);
                }
                formTabsTree = TabPageUtil.getTabList(tableProxyList, FormTabs.class);
            }


        } catch (BPMException e) {
            e.printStackTrace();
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return formTabsTree;
    }


    /**
     * @return
     */
    private WorkflowClientService getClient() {
        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);
        return client;
    }
}
