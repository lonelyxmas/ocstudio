package com.ds.esd.bpm;


import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.worklist.WorkListService;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/bpm/")
public class BPMNavTree {

    @MethodChinaName(cname = "流程样例")
    @RequestMapping(method = RequestMethod.POST, value = "Index")
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", caption = "流程样例")
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.index)
    @ResponseBody
    public ResultModel<WorkListService> getBPMNav() {
        return new ResultModel<WorkListService>();
    }


}
