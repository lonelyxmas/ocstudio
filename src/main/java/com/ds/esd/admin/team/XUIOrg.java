package com.ds.esd.admin.team;

import com.ds.org.Org;
import com.ds.org.Person;

import java.util.ArrayList;
import java.util.List;

public class XUIOrg {

    public String id;

    public String name;

    public String path;

    public boolean disabled = false;

    public boolean group = false;

    public boolean draggable = true;

    public String imageClass = "spafont spa-icon-c-grid";

    public String caption;

    List<XUIOrg> sub;

    public XUIOrg(List<Org> orgs, boolean hasPerson) {
        this.caption = "组织结构";
        this.id = "00000000-0000-0000-0000-000000000000";
        this.name = "总跟";
        for (Org childOrg : orgs) {
            this.addSub(new XUIOrg(childOrg, hasPerson));
        }
    }

    public XUIOrg(Org org, boolean hasPerson) {
        this.id = org.getOrgId();
        this.name = org.getName();
        this.caption = org.getName();
        this.imageClass = "xui-icon-menu";
        List<Org> childOrgs = org.getChildrenList();
        if (childOrgs != null) {
            for (Org childOrg : childOrgs) {
                if (!childOrg.getOrgId().equals(childOrg.getParentId())) {
                    this.addSub(new XUIOrg(childOrg, hasPerson));
                }

            }
        }

        if (hasPerson) {
            this.setDisabled(true);
            List<Person> persons = org.getPersonList();
            for (Person Person : persons) {
                this.addSub(new XUIOrg(Person));
            }
        }

    }

    public XUIOrg(Person person) {
        this.id = person.getID();
        this.caption = person.getName();
        this.name = person.getName();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public void addSub(XUIOrg subNode) {
        if (sub == null) {
            sub = new ArrayList<XUIOrg>();
        }
        sub.add(subNode);
    }

    public List<XUIOrg> getSub() {
        return sub;
    }

    public void setSub(List<XUIOrg> sub) {
        this.sub = sub;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
