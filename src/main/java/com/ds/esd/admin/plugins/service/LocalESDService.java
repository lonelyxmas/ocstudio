package com.ds.esd.admin.plugins.service;

import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.engine.ConnectInfo;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.grid.simple.SimpleGridComponet;
import com.ds.esd.tool.ui.component.panel.grid.TreeGridComponent;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.org.Person;
import com.ds.org.PersonNotFoundException;
import com.ds.server.JDSClientService;
import com.ds.server.JDSServer;
import com.ds.server.OrgManagerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@MethodChinaName(cname = "本地功能调用")
@RequestMapping(value = {"/LOCALESD/"})

public class LocalESDService {

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "executeCMD")
    @MethodChinaName(cname = "执行命令")
    public @ResponseBody
    ResultModel<Object> executeCMD(String expression) {

        ResultModel<Object> resultModel = new ResultModel();
        JDSClientService clientService = EsbUtil.parExpression(JDSClientService.class);
        try {
            if (clientService.getConnectInfo() == null) {
                try {
                    Person person = OrgManagerFactory.getOrgManager().getPersonByID(JDSServer.getInstance().getAdminUser().getId());
                    ConnectInfo connectInfo = new ConnectInfo(person.getID(), person.getAccount(), person.getPassword());
                    clientService.connect(connectInfo);
                } catch (PersonNotFoundException e) {
                    e.printStackTrace();
                } catch (JDSException e) {
                    e.printStackTrace();
                }
            }

            Object obj = EsbUtil.parExpression(expression);
            resultModel.setData(obj);
        } catch (Throwable e) {
            resultModel = new ErrorResultModel<>();
            ((ErrorResultModel<Object>) resultModel).setErrdes("表达式错误！" + e.getMessage());
        }


        return resultModel;
    }

    ;

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "executeGridCMD")
    @MethodChinaName(cname = "执行命令")
    public @ResponseBody
    ResultModel<TreeGridComponent> executeGridCMD(String expression) {
        Object obj = null;

        ResultModel<TreeGridComponent> resultModel = new ResultModel();

        JDSClientService clientService = EsbUtil.parExpression(JDSClientService.class);
        if (clientService.getConnectInfo() == null) {
            try {
                Person person = OrgManagerFactory.getOrgManager().getPersonByID(JDSServer.getInstance().getAdminUser().getId());
                ConnectInfo connectInfo = new ConnectInfo(person.getID(), person.getAccount(), person.getPassword());
                clientService.connect(connectInfo);
            } catch (PersonNotFoundException e) {
                e.printStackTrace();
            } catch (JDSException e) {
                e.printStackTrace();
            }

        }

        try {


            obj = JDSActionContext.getActionContext().Par(expression);
            //obj = EsbUtil.parExpression(expression);
            TreeGridComponent gridComponent = new SimpleGridComponet(obj);
            resultModel.setData(gridComponent);
        } catch (Throwable e) {
            e.printStackTrace();
            resultModel = new ErrorResultModel<>();
            ((ErrorResultModel<TreeGridComponent>) resultModel).setErrdes("表达式错误！" + e.getMessage());
        }


        return resultModel;
    }

}
