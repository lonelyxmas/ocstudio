package com.ds.esd.admin.plugins.service;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.admin.node.XUIFile;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.project.config.ProjectConfig;
import com.ds.esd.project.enums.ProjectDefAccess;
import com.ds.esd.tool.module.EUModule;
import com.ds.vfs.Folder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(value = {"/admin/plugs/extcom/"})
@MethodChinaName(cname = "扩展组件服务")
public class ExtComService {

    @MethodChinaName(cname = "获取扩展组件服务")
    @RequestMapping(value = {"getExtCom"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<Set<EUModule>> getExtCom(@PathVariable String projectName) {
        ListResultModel<Set<EUModule>> result = new ListResultModel<Set<EUModule>>();
        Set<EUModule> modulesConfigs = new HashSet<>();
        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            modulesConfigs = projectVersion.getAllModule();
            result.setData(modulesConfigs);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<Set<EUModule>> errorResult = new ErrorListResultModel<Set<EUModule>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "添加组件")
    @RequestMapping(value = {"addExtCom"}, method = {RequestMethod.POST})
    public @ResponseBody
    ResultModel<Boolean> addComponent(String projectName, String id) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ProjectVersion version = getClient().getProjectVersionByName(projectName);
            String[] ids = StringUtility.split(id, ";");

            ProjectConfig config = version.getProject().getConfig();
            List<String> extcomIds = config.getExtcoms();
            for (String apiId : ids) {
                if (!extcomIds.contains(apiId)) {
                    extcomIds.add(apiId);
                }
            }

            config.setExtcoms(extcomIds);
            version.updateConfig(config);

        } catch (JDSException e) {
            result = new ErrorResultModel();
            ((ErrorResultModel) result).setErrcode(JDSException.APPLICATIONNOTFOUNDERROR);
            ((ErrorResultModel) result).setErrdes(e.getMessage());

        }
        return result;
    }


    @MethodChinaName(cname = "获取组件工程")
    @RequestMapping(value = {"getExtComTreeProject"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIFile>> getExtComTreeProject() {
        ListResultModel<List<XUIFile>> result = new ListResultModel<List<XUIFile>>();
        List<XUIFile> xuiFiles = new ArrayList<XUIFile>();
        List<Folder> folders = new ArrayList<Folder>();
        try {

            List<Project> projects = this.getClient().getAllProject(ProjectDefAccess.ExtCom);
            for (Project project : projects) {
                Set<EUModule> modules = null;
                ProjectVersion projectVersion = project.getActiveProjectVersion();
                try {
                    modules = projectVersion.getAllModule();
                } catch (JDSException e) {
                    e.printStackTrace();
                }

                if (modules != null && modules.size() > 0) {
                    xuiFiles.add(new XUIFile(projectVersion));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            ErrorListResultModel<List<XUIFile>> errorResult = new ErrorListResultModel<List<XUIFile>>();
            errorResult.setErrdes(e.getMessage());

            result = errorResult;
        }
        result.setData(xuiFiles);
        return result;
    }


    @MethodChinaName(cname = "删除组件")
    @RequestMapping(value = {"delExtCom"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ResultModel<Boolean> delComponent(String projectName, String id) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        List<String> fontConfigIds = new ArrayList<String>();
        try {
            Project project = getClient().getProjectByName(projectName);
            fontConfigIds = project.getConfig().getFonts();
            fontConfigIds.remove(id);
            this.getClient().updateProjectConfig(project.getId(), project.getConfig());
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            errorResult.setErrcode(e.getErrorCode());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "获取组件")
    @RequestMapping(value = {"getProjectExtComs"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<EUModule>> getProjectExtComs(String projectName) {
        ListResultModel<List<EUModule>> result = new ListResultModel<List<EUModule>>();

        try {
            Project project = getClient().getProjectByName(projectName);
            List<EUModule> fontConfigs = project.getComponents();
            result.setData(fontConfigs);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ESDClient getClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }

}
