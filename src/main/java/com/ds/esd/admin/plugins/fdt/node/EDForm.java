package com.ds.esd.admin.plugins.fdt.node;

import java.util.ArrayList;
import java.util.List;

public class EDForm {

    //生成路径 root/userspace/DA/test.js
    String path;

    //表单名称
    String name;

    //主键名称
    String pkName;

    //表单提交地址
    String ajaxUrl;

    //生成表单列数
    Integer columns = 2;

    List<EDField> colList = new ArrayList<EDField>();

    public List<EDField> getColList() {
        return colList;
    }

    public void setColList(List<EDField> colList) {
        this.colList = colList;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPkName() {
        return pkName;
    }

    public void setPkName(String pkName) {
        this.pkName = pkName;
    }

    public String getAjaxUrl() {
        return ajaxUrl;
    }

    public void setAjaxUrl(String ajaxUrl) {
        this.ajaxUrl = ajaxUrl;
    }

    public Integer getColumns() {
        return columns;
    }

    public void setColumns(Integer columns) {
        this.columns = columns;
    }
}
