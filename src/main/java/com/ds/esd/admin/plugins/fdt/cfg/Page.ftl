xui.Class('${alias}', 'xui.Module',{
   Instance:{

        Dependencies:[],
        Required:[],

        properties : ${properties.toJson()},
        initialize : function(){
        },
        iniComponents : function(){

   var host=this, children=[], properties={},intProperties=function(properties){
                xui.each(properties, function(item){
                  var child= xui.create(item.key)
                .setHost(host,item.alias)
                .setAlias(item.alias)
                .setProperties(item.properties)
                 children.push( child.get(0) );
                })
             },
                append=function(child){
                    children.push(child.get(0));
                };

            xui.merge(properties, this.properties);


            intProperties(${genChildrenJSON()});

            return children;
            // ]]Code created by CrossUI RAD Studio
        },

        // 可以自定义哪些界面控件将会被加到父容器中
        customAppend : function(parent, subId, left, top){
            return false;
        }
        /*,
        // 属性影响本模块的部分
        propSetAction : function(prop){
        },
        // 本模块中所有xui dom节点的定制CSS style
        customStyle:{}
    },
    // 制定义模块的默认属性和事件声明
    Static:{
        $DataModel:{
        },
        $EventHandlers:{
        }
    */
    }
});