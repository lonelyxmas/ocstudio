package com.ds.esd.admin.plugins.fdt.node;

public class ColPropety {

    String width="8em";

    String labelSize="4em";


    String name;

    boolean multiLines=false;

    String type;

    String labelHAlign="left";


    public ColPropety(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getLabelSize() {
        return labelSize;
    }

    public void setLabelSize(String labelSize) {
        this.labelSize = labelSize;
    }

    public boolean isMultiLines() {
        return multiLines;
    }

    public void setMultiLines(boolean multiLines) {
        this.multiLines = multiLines;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabelHAlign() {
        return labelHAlign;
    }

    public void setLabelHAlign(String labelHAlign) {
        this.labelHAlign = labelHAlign;
    }



}
