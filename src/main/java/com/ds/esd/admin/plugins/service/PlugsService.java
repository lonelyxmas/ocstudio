package com.ds.esd.admin.plugins.service;

import com.ds.common.JDSException;
import com.ds.common.database.metadata.MetadataFactory;
import com.ds.common.database.metadata.TableInfo;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.admin.node.XUIFile;
import com.ds.esd.admin.node.XUIModule;
import com.ds.esd.admin.plugins.fdt.node.UITopNode;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.gen.UIGenTools;
import com.ds.esd.dsm.repository.database.proxy.DSMTableProxy;
import com.ds.esd.editor.enums.PackageType;
import com.ds.esd.plugins.api.APIFactory;
import com.ds.esd.plugins.api.node.APIPaths;
import com.ds.esd.plugins.font.node.FontConfig;
import com.ds.esd.plugins.img.node.ImgConfig;
import com.ds.esd.project.config.DataBaseConfig;
import com.ds.esd.project.enums.ProjectDefAccess;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.node.XUIModuleFile;
import com.ds.esd.tool.node.XUISyncFile;
import com.ds.esd.tool.toolbox.UIComponentNode;
import com.ds.esd.tool.ui.component.Component;
import com.ds.esd.tool.ui.component.data.APICallerComponent;
import com.ds.esd.tool.ui.component.data.APICallerProperties;
import com.ds.esd.tool.ui.module.ModuleComponent;
import com.ds.vfs.FileInfo;
import com.ds.vfs.Folder;
import com.ds.vfs.VFSConstants;
import com.ds.vfs.ct.CtVfsFactory;
import com.ds.vfs.ct.CtVfsService;
import com.ds.web.APIConfig;
import com.ds.web.APIConfigFactory;
import com.ds.web.RequestMethodBean;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

@Controller
@RequestMapping(value = {"/RAD/{projectName}/plugs/"})
@MethodChinaName(cname = "RAD工具应用")
public class PlugsService {


    @MethodChinaName(cname = "从模板添加文件")
    @RequestMapping(value = {"addFromTpl"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ResultModel<XUIModule> addFromTpl(String className, String tempPath, @PathVariable String projectName) {
        ResultModel<XUIModule> result = new ResultModel<XUIModule>();
        XUIModule xuiModule = new XUIModule();
        List<XUIFile> modules = xuiModule.getFiles();
        try {
            FileInfo tempFile = getVfsClient().getFileByPath(tempPath);
            String json = this.getVfsClient().readFileAsString(tempFile.getPath(), VFSConstants.Default_Encoding).toString();
            this.getClient().saveModuleAsJson(projectName, className, json);

        } catch (JDSException e) {
            e.printStackTrace();
        }

        if (tempPath == null || tempPath.equals("")) {
            tempPath = "template/cn/index.js";
        }

        return result;
    }

    @MethodChinaName(cname = "获取所有XUI类")
    @RequestMapping(value = {"getAllClass"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIFile>> getAllClass(@PathVariable String projectName) {
        List<XUIFile> classList = new ArrayList<XUIFile>();
        ListResultModel<List<XUIFile>> result = new ListResultModel<List<XUIFile>>();
        Set<EUModule> modules = null;
        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            modules = projectVersion.getAllModule();
            for (EUModule module : modules) {
                if (module != null) {
                    classList.add(new XUIFile(module));
                }
            }
            result.setData(classList);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<XUIFile>> errorResult = new ErrorListResultModel<List<XUIFile>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "获取所有XUI类")
    @RequestMapping(value = {"getAllClassComponents"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIFile>> getAllClassComponents(@PathVariable String projectName) {
        List<XUIFile> classList = new ArrayList<XUIFile>();
        ListResultModel<List<XUIFile>> result = new ListResultModel<List<XUIFile>>();
        Set<EUModule> modules = null;
        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            modules = projectVersion.getAllModule();
            for (EUModule module : modules) {
                if (module != null) {
                    classList.add(new XUIFile(module));
                }
            }
            result.setData(classList);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<XUIFile>> errorResult = new ErrorListResultModel<List<XUIFile>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "字体类配置")
    @RequestMapping(value = {"getSelFont"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<FontConfig>> getSelFont(@PathVariable String projectName) {
        ListResultModel<List<FontConfig>> result = new ListResultModel<List<FontConfig>>();
        List<FontConfig> fontConfigs = new ArrayList<FontConfig>();
        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            fontConfigs = projectVersion.getProject().getFonts();
            result.setData(fontConfigs);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<FontConfig>> errorResult = new ErrorListResultModel<List<FontConfig>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "图片类配置")
    @RequestMapping(value = {"getSelImg"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<ImgConfig>> getSelImg(@PathVariable String projectName) {
        ListResultModel<List<ImgConfig>> result = new ListResultModel<List<ImgConfig>>();
        List<ImgConfig> imgConfigs = new ArrayList<ImgConfig>();
        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            imgConfigs = projectVersion.getProject().getImgs();
            result.setData(imgConfigs);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<ImgConfig>> errorResult = new ErrorListResultModel<List<ImgConfig>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "图片类配置")
    @RequestMapping(value = {"getSelImgByPath"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ResultModel<ImgConfig> getSelImgByPath(@PathVariable String projectName, String path) {
        ResultModel<ImgConfig> result = new ResultModel<ImgConfig>();

        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            ImgConfig imgConfig = getClient().getImgConfig(path);
            result.setData(imgConfig);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<ImgConfig> errorResult = new ErrorResultModel<ImgConfig>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "获取工程内图片")
    @RequestMapping(value = {"getInnerImg"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<ImgConfig>> getInnerImg(@PathVariable String projectName, String path) {
        ListResultModel<List<ImgConfig>> result = new ListResultModel<List<ImgConfig>>();
        List<ImgConfig> imgConfigs = new ArrayList<ImgConfig>();
        try {
            ImgConfig imgConfig = getClient().buildImgConfig(projectName, path);
            imgConfigs.add(imgConfig);
            result.setData(imgConfigs);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<ImgConfig>> errorResult = new ErrorListResultModel<List<ImgConfig>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @RequestMapping(value = {"API"}, method = {RequestMethod.GET})
    public ResultModel<Boolean> api(HttpServletResponse response, @PathVariable String projectName) {
        InputStream stream = null;
        FileInfo fileInfo = null;
        try {
            fileInfo = this.getVfsClient().getFileByPath("root/RAD/API/index.html");
            if (fileInfo != null) {
                stream = fileInfo.getCurrentVersonInputStream();
            }
            OutputStream os = response.getOutputStream();
            // 循环写入输出流
            byte[] b = new byte[4096];
            int length;
            while ((length = stream.read(b)) > 0) {
                os.write(b, 0, length);

            }
            try {
                os.close();
                stream.close();
            } catch (final IOException ioe) {
            }

        } catch (JDSException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResultModel<>();

    }

    @MethodChinaName(cname = "获取扩展模块库")
    @RequestMapping(value = {"getExtModuleProjectTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIFile>> getExtModuleProjectTree(@PathVariable String projectName) {
        ListResultModel<List<XUIFile>> result = new ListResultModel<List<XUIFile>>();
        List<XUIFile> xuiFiles = new ArrayList<XUIFile>();
        List<Folder> folders = new ArrayList<Folder>();
        try {
            List<Project> projects = this.getClient().getAllProject(ProjectDefAccess.Module);
            for (Project project : projects) {
                Set<EUModule> modules = null;
                ProjectVersion projectVersion = project.getActiveProjectVersion();
                try {
                    modules = projectVersion.getAllModule();
                } catch (JDSException e) {
                    e.printStackTrace();
                }

                if (modules != null && modules.size() > 0) {
                    XUIFile xuiFile = new XUIFile(projectVersion);
                    xuiFile.setCaption(projectVersion.getProject().getDesc() == null ? projectVersion.getProject().getProjectName() : projectVersion.getProject().getDesc());
                    xuiFiles.add(xuiFile);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            ErrorListResultModel<List<XUIFile>> errorResult = new ErrorListResultModel<List<XUIFile>>();
            errorResult.setErrdes(e.getMessage());

            result = errorResult;
        }
        result.setData(xuiFiles);
        return result;
    }


    @MethodChinaName(cname = "获取扩展组件库")
    @RequestMapping(value = {"getExtComProjectTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIFile>> getExtComProjectTree(@PathVariable String projectName) {
        ListResultModel<List<XUIFile>> result = new ListResultModel<List<XUIFile>>();
        List<XUIFile> xuiFiles = new ArrayList<XUIFile>();
        List<Folder> folders = new ArrayList<Folder>();
        try {
            List<Project> projects = this.getClient().getAllProject(ProjectDefAccess.Component);
            for (Project project : projects) {
                Set<EUModule> modules = null;
                ProjectVersion projectVersion = project.getActiveProjectVersion();
                try {
                    modules = projectVersion.getAllModule();
                } catch (JDSException e) {
                    e.printStackTrace();
                }

                if (modules != null && modules.size() > 0) {
                    XUIFile xuiFile = new XUIFile(projectVersion);
                    xuiFile.setCaption(projectVersion.getProject().getDesc() == null ? projectVersion.getProject().getProjectName() : projectVersion.getProject().getDesc());
                    xuiFiles.add(xuiFile);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            ErrorListResultModel<List<XUIFile>> errorResult = new ErrorListResultModel<List<XUIFile>>();
            errorResult.setErrdes(e.getMessage());

            result = errorResult;
        }
        result.setData(xuiFiles);
        return result;
    }


    @MethodChinaName(cname = "获取所有工程")
    @RequestMapping(value = {"getAllProjectTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIFile>> getAllProjectTree(@PathVariable String projectName, String tpath) {
        ListResultModel<List<XUIFile>> result = new ListResultModel<List<XUIFile>>();
        List<XUIFile> xuiFiles = new ArrayList<XUIFile>();
        List<Folder> folders = new ArrayList<Folder>();
        try {

            Project sproject = this.getClient().getProjectByName(projectName);
            ProjectDefAccess type = sproject.getProjectType();
//            if (tpath != null && tpath.startsWith("Module/")) {
//                type = ProjectDefAccess.Module;
//            }

            List<Project> projects = this.getClient().getAllProject(type);

            for (Project project : projects) {
                Set<EUModule> modules = null;
                ProjectVersion projectVersion = project.getActiveProjectVersion();
                try {
                    modules = projectVersion.getAllModule();
                } catch (JDSException e) {
                    e.printStackTrace();
                }

                if (modules != null && modules.size() > 0) {
                    XUIFile xuiFile = new XUIFile(projectVersion);
                    xuiFile.setCaption(projectVersion.getProject().getDesc() == null ? projectVersion.getProject().getProjectName() : projectVersion.getProject().getDesc());
                    xuiFile.setSub(new ArrayList<>());
                    xuiFiles.add(xuiFile);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            ErrorListResultModel<List<XUIFile>> errorResult = new ErrorListResultModel<List<XUIFile>>();
            errorResult.setErrdes(e.getMessage());

            result = errorResult;
        }
        result.setData(xuiFiles);
        return result;
    }


    @MethodChinaName(cname = "获取所有包")
    @RequestMapping(value = {"getAllClasses"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUISyncFile>> getAllClasses(@PathVariable String projectName, String className) {
        ListResultModel<List<XUISyncFile>> result = new ListResultModel<List<XUISyncFile>>();
        List<XUISyncFile> xuiFiles = new ArrayList<XUISyncFile>();
        List<Folder> folders = new ArrayList<Folder>();
        Map<String, XUISyncFile> folderMap = new HashMap<String, XUISyncFile>();

        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            EUModule euModule = projectVersion.getModule(className);

            EUPackage euPackage = projectVersion.getEUPackage(euModule.getPackageName());
            List<EUModule> allModules = euPackage.listAllModule();
            for (EUModule module : allModules) {
                if (module != null && (className == null || !className.equals(module.getClassName()))) {

                    Folder folder = null;
                    try {
                        folder = getClient().getFileByPath(module.getPath(), projectName).getFolder();
                        XUISyncFile xuiSyncFile = folderMap.get(folder.getPath());
                        if (xuiSyncFile == null) {
                            xuiSyncFile = new XUISyncFile(folder, projectVersion);
                            folderMap.put(folder.getPath(), xuiSyncFile);
                            xuiFiles.add(xuiSyncFile);
                        }
                        XUIModuleFile moduleFile = new XUIModuleFile(module);
                        moduleFile.setType("");
                        moduleFile.setCaption(module.getName());
                        xuiSyncFile.getSub().add(moduleFile);


                    } catch (JDSException e) {
                        e.printStackTrace();
                    }

                }
            }
            Collections.sort(xuiFiles, new Comparator<XUISyncFile>() {
                public int compare(XUISyncFile o1, XUISyncFile o2) {
                    return o1.getClassName().compareTo(o2.getClassName());
                }
            });


            result.setData(xuiFiles);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<XUISyncFile>> errorResult = new ErrorListResultModel<List<XUISyncFile>>();
            errorResult.setErrdes(e.getMessage());

            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "获取所有包")
    @RequestMapping(value = {"getAllPackages"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIFile>> getAllPackages(String projectName, String type, String pattern) {
        ListResultModel<List<XUIFile>> result = new ListResultModel<List<XUIFile>>();
        List<XUIFile> xuiFiles = new ArrayList<XUIFile>();
        List<Folder> folders = new ArrayList<Folder>();
        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            List<EUPackage> packages = getClient().getAllPackage(projectName, PackageType.local);

            for (EUPackage euPackage : packages) {
                if (euPackage.findModules(type, pattern).size() > 0) {
                    xuiFiles.add(new XUIFile(euPackage));
                }
            }
            Collections.sort(xuiFiles, new Comparator<XUIFile>() {
                public int compare(XUIFile o1, XUIFile o2) {
                    return o1.getClassName().compareTo(o2.getClassName());
                }
            });
            result.setData(xuiFiles);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<XUIFile>> errorResult = new ErrorListResultModel<List<XUIFile>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "获取所有包")
    @RequestMapping(value = {"getAPIPackages"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<APIPaths>> getAPIPackages(String projectName) {
        ListResultModel<List<APIPaths>> result = new ListResultModel<List<APIPaths>>();
        List<APIPaths> xuiFiles = new ArrayList<APIPaths>();
        List<Folder> folders = new ArrayList<Folder>();
        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            List<APIConfig> apiConfigs = getClient().getAPIConfigByProject(projectName);

            for (APIConfig apiConfig : apiConfigs) {
                xuiFiles.add(new APIPaths(APIFactory.LoclHostName, apiConfig));
            }
            Collections.sort(xuiFiles, new Comparator<APIPaths>() {
                public int compare(APIPaths o1, APIPaths o2) {
                    return o1.getPath().compareTo(o2.getPath());
                }
            });
            result.setData(xuiFiles);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<APIPaths>> errorResult = new ErrorListResultModel<List<APIPaths>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "指定包下组件")
    @RequestMapping(value = {"getAllComponentsByClass"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<Object>> getAllComponentsByClass(@PathVariable String projectName, String className, String type, String pattern, Boolean draggable) {
        if (draggable == null) {
            draggable = true;
        }
        ListResultModel<List<Object>> result = new ListResultModel<List<Object>>();
        List<Object> modules = new ArrayList<Object>();

        try {
            EUModule module = this.getClient().getModule(className, projectName);
            ModuleComponent component = module.getComponent();
            if (component != null) {
                for (Object subcomponent : component.findComponents(type, pattern)) {
                    modules.add(new UIComponentNode((Component) subcomponent, draggable));
                }
            }
            result.setData(modules);

        } catch (Exception e) {
            e.printStackTrace();
            ErrorListResultModel<List<Object>> errorResult = new ErrorListResultModel<List<Object>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "指定包下组件")
    @RequestMapping(value = {"getAllComponentsByPath"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<Object>> getAllComponentsByPath(String projectName, String path, String className, String type, String pattern, Boolean draggable) {
        if (draggable == null) {
            draggable = true;
        }
        ListResultModel<List<Object>> result = new ListResultModel<List<Object>>();
        List<Object> modules = new ArrayList<Object>();

        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            Folder folder = getClient().getFolderByPath(path, projectName);
            if (folder == null) {
                folder = getClient().createFolder(path, projectName);

            }
            Set<EUModule> allModules = projectVersion.getAllModule();
            for (EUModule module : allModules) {
                if (module != null) {
                    Folder mfolder = getClient().getFileByPath(module.getPath(), projectName).getFolder();
                    if (mfolder.getPath().equals(folder.getPath())) {
                        ModuleComponent component = module.getComponent();
                        if (component != null && component.findComponents(type, pattern) != null && component.findComponents(type, pattern).size() > 0) {
                            UIComponentNode node = new UIComponentNode(component, draggable);
                            // node.setCaption(component.getDesc()==null ?component.getClassName());
                            modules.add(node);
                        }
                    }
                }
            }


            result.setData(modules);

        } catch (Exception e) {
            e.printStackTrace();
            ErrorListResultModel<List<Object>> errorResult = new ErrorListResultModel<List<Object>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "检索关联动作")
    @RequestMapping(value = {"getComponents"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<Object>> getComponents(@PathVariable String projectName) {
        ListResultModel<List<Object>> result = new ListResultModel<List<Object>>();
        List<Object> modules = new ArrayList<Object>();

        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            Set<EUModule> allModules = projectVersion.getAllModule();
            for (EUModule module : allModules) {
                if (module != null) {
                    ModuleComponent component = module.getComponent();
                    if (component != null && component.getChildren() != null && component.getChildren().size() > 0) {
                        modules.add(new UIComponentNode(component, true));
                    }
                }
            }
            result.setData(modules);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<Object>> errorResult = new ErrorListResultModel<List<Object>>();
            errorResult.setErrdes(e.getMessage());

            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "获取扩展应用定义")
    @RequestMapping(value = {"getExtModules"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<Object>> getExtModules(@PathVariable String projectName) {
        ListResultModel<List<Object>> result = new ListResultModel<List<Object>>();
        List<Object> modules = new ArrayList<Object>();

        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            Set<EUModule> allModules = projectVersion.getAllModule();
            for (EUModule module : allModules) {
                if (module != null) {
                    ModuleComponent component = module.getComponent();
                    if (component != null && component.getChildren() != null && component.getChildren().size() > 0) {
                        modules.add(new UIComponentNode(component, true));
                    }
                }
            }
            result.setData(modules);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<Object>> errorResult = new ErrorListResultModel<List<Object>>();
            errorResult.setErrdes(e.getMessage());

            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "获取扩展组件定义")
    @RequestMapping(value = {"getExtComs"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<Object>> getExtComs(@PathVariable String projectName) {
        ListResultModel<List<Object>> result = new ListResultModel<List<Object>>();
        List<Object> modules = new ArrayList<Object>();

        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            Set<EUModule> allModules = projectVersion.getAllModule();
            for (EUModule module : allModules) {
                if (module != null) {
                    ModuleComponent component = module.getComponent();
                    if (component != null && component.getChildren() != null && component.getChildren().size() > 0) {
                        modules.add(new UIComponentNode(component, true));
                    }
                }
            }
            result.setData(modules);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<Object>> errorResult = new ErrorListResultModel<List<Object>>();
            errorResult.setErrdes(e.getMessage());

            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "获取所有数据库表")
    @RequestMapping(method = RequestMethod.POST, value = "fdt/GetAllTableByName")
    @ResponseBody
    public ListResultModel<List<TableInfo>> getAllTableByName(String simpleName, @PathVariable String projectName) {
        ListResultModel<List<TableInfo>> result = new ListResultModel<List<TableInfo>>();
        List<TableInfo> tables = new ArrayList<TableInfo>();
        try {
            ProjectVersion projectVersion = this.getClient().getProjectVersionByName(projectName);
            List<DataBaseConfig> configs = projectVersion.getProject().getConfig().getDbConfigs();

            for (DataBaseConfig config : configs) {
                try {
                    if (config.getConfigKey() != null && this.getClient().getDbFactory(config.getConfigKey()) != null) {
                        MetadataFactory factory = getClient().getDbFactory(config.getConfigKey());
                        List<TableInfo> alltables = factory.getTableInfos(simpleName);
                        List<String> tableNames = config.getTableName();
                        for (TableInfo table : alltables) {
                            if (tableNames == null || tableNames.contains(table.getName())) {
                                tables.add(table);
                            }
                        }
                        result = PageUtil.getDefaultPageList(tables);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


        } catch (JDSException e) {
            ErrorListResultModel<List<TableInfo>> errorResult = new ErrorListResultModel<List<TableInfo>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "获取数据库表组件")
    @RequestMapping(value = "GetTableComponents", method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    ResultModel<List<UIComponentNode>> getTableComponents(String tableName, String configKey, @PathVariable String projectName, String repositoryId) {
        ResultModel<List<UIComponentNode>> result = new ResultModel<List<UIComponentNode>>();
        List<UIComponentNode> componentNodes = new ArrayList<UIComponentNode>();
        try {
            ProjectVersion version = this.getClient().getProjectVersionByName(projectName);
            UIGenTools tools = new UIGenTools(version, configKey);
            MetadataFactory metadataFactory = this.getClient().getDbFactory(configKey);
            DSMTableProxy proxy = DSMFactory.getInstance().getRepositoryManager().getTableProxyByName(tableName, repositoryId);
            EUModule formModule = tools.genTableFormModule(tableName, null, repositoryId);

            EUModule gridLayoutModule = tools.genTableGridModule(tableName, null, repositoryId);

            APIConfig config = APIConfigFactory.getInstance().getAPIConfig("com.ds.fdt.server.service.DAOFromService");
            List<RequestMethodBean> methods = config.getMethods();

            for (RequestMethodBean methodBean : methods) {
                methodBean.setUrl(StringUtility.replace(methodBean.getUrl(), "{connfigKey}", configKey));
                methodBean.setUrl(StringUtility.replace(methodBean.getUrl(), "{className}", proxy.getClassName()));
                APICallerProperties properties = new APICallerProperties(methodBean);
                UIComponentNode apiComponent = new UIComponentNode(new APICallerComponent(methodBean.getName(), properties), true);
                componentNodes.add(apiComponent);
            }
            componentNodes.add(new UIComponentNode(formModule.getComponent(), true));
            componentNodes.add(new UIComponentNode(gridLayoutModule.getComponent(), true));
            result.setData(componentNodes);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<List<UIComponentNode>> errorResult = new ErrorResultModel<List<UIComponentNode>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }


    @MethodChinaName(cname = "获取数据库树形实例数据")
    @RequestMapping(value = "fdt/GetTableTrees", method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    ResultModel<List<UITopNode>> getTableTrees(String text, String url, @PathVariable String projectName) {
        ResultModel<List<UITopNode>> result = new ResultModel<List<UITopNode>>();
        List<UITopNode> topNodes = new ArrayList<UITopNode>();
        try {
            ProjectVersion projectVersion = this.getClient().getProjectVersionByName(projectName);
            List<DataBaseConfig> configs = projectVersion.getProject().getConfig().getDbConfigs();

            for (DataBaseConfig config : configs) {
                try {
                    if (config.getConfigKey() != null && this.getClient().getDbFactory(config.getConfigKey()) != null) {
                        topNodes.add(new UITopNode(config));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
            result.setData(topNodes);
        } catch (Exception e) {
            ErrorResultModel<List<UITopNode>> errorResult = new ErrorResultModel<List<UITopNode>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    public ESDClient getClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }

    public CtVfsService getVfsClient() {

        CtVfsService vfsClient = CtVfsFactory.getCtVfsService();
        return vfsClient;
    }
}



