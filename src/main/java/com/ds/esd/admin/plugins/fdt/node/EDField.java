package com.ds.esd.admin.plugins.fdt.node;


import com.ds.esd.tool.ui.enums.XUIType;

public class EDField {

    //排序
    Integer index;

    //中文名称（标签）
    String cnName;

    //字段名称
    String name;

    String dictJson;

    String value;

    //字段类型
    XUIType type;

    //s输入长度
    Integer length;

    //跨列
    Integer colSpan;

    //跨行
    Integer rowSpan;


    public String getDictJson() {
        return dictJson;
    }

    public void setDictJson(String dictJson) {
        this.dictJson = dictJson;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public XUIType getType() {
        return type;
    }

    public void setType(XUIType type) {
        this.type = type;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getColSpan() {
        return colSpan;
    }

    public void setColSpan(Integer colSpan) {
        this.colSpan = colSpan;
    }

    public Integer getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(Integer rowSpan) {
        this.rowSpan = rowSpan;
    }
}
