package com.ds.esd.admin.plugins.service;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.plugins.img.ImgFactory;
import com.ds.esd.plugins.img.node.ImgConfig;
import com.ds.esd.plugins.img.node.ImgNode;
import com.ds.esd.project.config.ProjectConfig;
import com.ds.esd.project.enums.ProjectResourceType;
import com.ds.vfs.Folder;
import com.ds.vfs.ct.CtVfsFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = {"/admin/plugs/img/"})
@MethodChinaName(cname = "图片服务")
public class ImgService {

    @MethodChinaName(cname = "图片类配置")
    @RequestMapping(value = {"getSelImg"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<ImgConfig>> getSelImg(@PathVariable String projectName) {
        ListResultModel<List<ImgConfig>> result = new ListResultModel<List<ImgConfig>>();
        List<ImgConfig> imgConfigs = new ArrayList<ImgConfig>();
        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            imgConfigs = projectVersion.getProject().getImgs();
            result.setData(imgConfigs);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<ImgConfig>> errorResult = new ErrorListResultModel<List<ImgConfig>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "添加图片")
    @RequestMapping(value = {"addImg"}, method = {RequestMethod.POST})
    public @ResponseBody
    ResultModel<Boolean> addImg(String projectName, String id) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ProjectVersion version = getClient().getProjectVersionByName(projectName);
            String[] ids = StringUtility.split(id, ";");
            ProjectConfig config = version.getProject().getConfig();

            List<String> imgIds = config.getImgs();
            for (String imgId : ids) {
                if (!imgIds.contains(imgId)) {
                    ImgConfig imgConfig = ImgFactory.getInstance(getClient().getSpace()).getImgConfigById(imgId);
                    Folder folder = CtVfsFactory.getCtVfsService().getFolderById(imgId);
                    if (folder != null) {
                        Folder imgFolder = (Folder) getClient().copy(projectName, folder.getPath(), "img/" + folder.getName());
                        imgIds.add(imgFolder.getID());
                    }
                }
            }
            config.setImgs(imgIds);
            version.updateConfig(config);
        } catch (JDSException e) {
            result = new ErrorResultModel();
            ((ErrorResultModel) result).setErrcode(JDSException.APPLICATIONNOTFOUNDERROR);
            ((ErrorResultModel) result).setErrdes(e.getMessage());

        }
        return result;
    }


    @MethodChinaName(cname = "图片定义")
    @RequestMapping(value = {"getImgTreeProject"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<ImgNode>> getImgTreeProject() {
        ListResultModel<List<ImgNode>> result = new ListResultModel<List<ImgNode>>();
        List<ImgNode> fontNodes = new ArrayList<ImgNode>();
        try {
            List<Project> projects = getClient().getResourceAllProject(ProjectResourceType.img);
            for (Project project : projects) {
                fontNodes.add(new ImgNode(project));
            }
            result.setData(fontNodes);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "字体类配置")
    @RequestMapping(value = {"delImg"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ResultModel<Boolean> delFont(String projectName, String id) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        List<String> fontConfigIds = new ArrayList<String>();
        try {
            Project project = getClient().getProjectByName(projectName);
            fontConfigIds = project.getConfig().getImgs();
            fontConfigIds.remove(id);
            this.getClient().updateProjectConfig(project.getId(), project.getConfig());
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            errorResult.setErrcode(e.getErrorCode());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "字体类配置")
    @RequestMapping(value = {"getProjectImgs"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<ImgConfig>> getProjectImgs(String projectName) {
        ListResultModel<List<ImgConfig>> result = new ListResultModel<List<ImgConfig>>();
        try {
            Project project = getClient().getProjectByName(projectName);
            List<ImgConfig> fontConfigs = project.getImgs();
            result.setData(fontConfigs);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ESDClient getClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }

}
