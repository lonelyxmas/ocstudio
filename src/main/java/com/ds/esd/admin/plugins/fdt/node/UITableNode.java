package com.ds.esd.admin.plugins.fdt.node;

import com.ds.common.database.metadata.TableInfo;

import java.util.ArrayList;
import java.util.List;

public class UITableNode {


    public String id;

    public String path;

    public boolean disabled = false;

    public boolean group = false;

    public boolean draggable = true;


    public String key;

    public String tablename;

    public String url;


    public String configKey;

    public String cls = "xui.Module";

    public String imageClass = "fa fa-lg fa-table";

    public String caption;


    public boolean iniFold = false;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public boolean isDraggable() {
        return draggable;
    }

    public void setDraggable(boolean draggable) {
        this.draggable = draggable;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }


    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    List<UIColNode> sub;

    public UITableNode(TableInfo table) {
        this.id = table.getName();
        this.configKey = table.getConfigKey();
        this.tablename = table.getName();
        this.url = table.getUrl();
        this.configKey = table.getConfigKey();
        this.caption = (table.getCnname() == null || table.getCnname().equals("")) ? table.getName() : table.getCnname();
        this.caption = table.getName() + "(" + this.caption + ")";
    }


    public UITableNode(String id, String text) {
        this.id = id;
        this.caption = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public void addSub(UIColNode subNode) {
        if (sub == null) {
            sub = new ArrayList<UIColNode>();
        }
        sub.add(subNode);
    }

    public List<UIColNode> getSub() {
        return sub;
    }

    public void setSub(List<UIColNode> sub) {
        this.sub = sub;
    }

    public boolean isIniFold() {
        return iniFold;
    }

    public void setIniFold(boolean iniFold) {
        this.iniFold = iniFold;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
