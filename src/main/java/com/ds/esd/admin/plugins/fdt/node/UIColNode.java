package com.ds.esd.admin.plugins.fdt.node;

import com.ds.common.database.metadata.ColInfo;
import com.ds.esd.tool.cfg.XUITypeMapping;
import com.ds.esd.tool.ui.enums.ComponentType;

import java.util.HashMap;
import java.util.Map;

public class UIColNode {


    public String id;

    public String path;


    String alias;

    public boolean disabled = false;

    public boolean group = false;

    public boolean draggable = true;

    public String key;

    public String type;

    public String cls = "xui.UI.ComboInput";

    public String imageClass = "spafot spa-icon-c-";

    public String caption;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean iniFold = false;


    public Map<String, String> tagVar = new HashMap<String, String>();

    public ColPropety iniProp;


    public UIColNode(ColInfo col) {
        this.id = col.getName();
        this.caption = col.getCnname();
        this.type = XUITypeMapping.getType(col).toString();

        this.cls = "xui.UI." + XUITypeMapping.getComponentType(col);
        if (XUITypeMapping.getComponentType(col).equals(ComponentType.ComboInput)) {
            this.imageClass = imageClass + type.toLowerCase() + "input";
        } else {
            this.imageClass = imageClass + type.toLowerCase();
        }


        this.iniProp = new ColPropety(col.getName());
        this.alias = col.getName();

        iniProp.setType(type);

        Integer length = col.getLength();
        if (length > 20) {
            length = 20;
        }
        iniProp.setWidth(length + "em");

        tagVar.put("path", "root/form/dbmanager/guanlijiemian/collist.js");
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public ColPropety getIniProp() {
        return iniProp;
    }

    public void setIniProp(ColPropety iniProp) {
        this.iniProp = iniProp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }


    public boolean isIniFold() {
        return iniFold;
    }

    public void setIniFold(boolean iniFold) {
        this.iniFold = iniFold;
    }

    public Map<String, String> getTagVar() {
        return tagVar;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public boolean isDraggable() {
        return draggable;
    }

    public void setDraggable(boolean draggable) {
        this.draggable = draggable;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public void setTagVar(Map<String, String> tagVar) {
        this.tagVar = tagVar;
    }

}
