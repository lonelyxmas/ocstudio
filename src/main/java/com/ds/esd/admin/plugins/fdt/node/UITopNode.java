package com.ds.esd.admin.plugins.fdt.node;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.common.JDSException;
import com.ds.common.database.dao.DAOException;
import com.ds.common.database.metadata.MetadataFactory;
import com.ds.common.database.metadata.TableInfo;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.project.config.DataBaseConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UITopNode {


    public String id;

    public String path;

    public boolean disabled = false;

    public boolean group = false;

    public boolean draggable = true;

    public String key;

    public String cls = "xui.Module";

    public String imageClass = "iconfont iconchucun";

    public String caption;

    public String url;

    public String configKey;

    public boolean iniFold = false;


    public Map<String, String> tagVar = new HashMap<String, String>();

    List<UITableNode> sub;

    public UITopNode(DataBaseConfig config) {
        this.id = config.getConfigKey();
        this.caption = config.getConfigKey();

        MetadataFactory metadataFactory = null;
        try {
            metadataFactory = this.getClient().getDbFactory(config.getConfigKey());
        } catch (JDSException e) {
            e.printStackTrace();
        }

        List<String> tableName = config.getTableName();
        List<TableInfo> tables = null;
        try {
            tables = metadataFactory.getTableInfos("");
            for (TableInfo table : tables) {
                if (tableName.contains(table.getName())) {
                    if (this.sub == null) {
                        this.sub = new ArrayList<UITableNode>();
                    }
                    this.sub.add(new UITableNode(table));
                }
            }

        } catch (DAOException e) {
            e.printStackTrace();
        }

    }

    public UITopNode(String id, String text, String url, String configKey) {
        this.id = id;
        this.caption = text;
        this.url = url;
        this.configKey = configKey;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public void addSub(UITableNode subNode) {
        if (sub == null) {
            sub = new ArrayList<UITableNode>();
        }
        sub.add(subNode);
    }

    public List<UITableNode> getSub() {
        return sub;
    }

    public void setSub(List<UITableNode> sub) {
        this.sub = sub;
    }

    public boolean isIniFold() {
        return iniFold;
    }

    public void setIniFold(boolean iniFold) {
        this.iniFold = iniFold;
    }

    public Map<String, String> getTagVar() {
        return tagVar;
    }

    public void setTagVar(Map<String, String> tagVar) {
        this.tagVar = tagVar;
    }


    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }


    @JSONField(serialize = false)
    public ESDClient getClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }
}
