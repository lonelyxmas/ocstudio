package com.ds.esd.admin.plugins.service;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.plugins.font.node.FontConfig;
import com.ds.esd.plugins.font.node.FontNode;
import com.ds.esd.project.config.ProjectConfig;
import com.ds.esd.project.enums.ProjectResourceType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = {"/admin/plugs/font/"})
@MethodChinaName(cname = "图标字体服务")
public class FontService {

    @MethodChinaName(cname = "字体类配置")
    @RequestMapping(value = {"getSelFont"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<FontConfig>> getSelFont(@PathVariable String projectName) {
        ListResultModel<List<FontConfig>> result = new ListResultModel<List<FontConfig>>();
        List<FontConfig> fontConfigs = new ArrayList<FontConfig>();
        try {
            ProjectVersion projectVersion = getClient().getProjectVersionByName(projectName);
            fontConfigs = projectVersion.getProject().getFonts();
            result.setData(fontConfigs);
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorListResultModel<List<FontConfig>> errorResult = new ErrorListResultModel<List<FontConfig>>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "添加字体定义")
    @RequestMapping(value = {"addFont"}, method = {RequestMethod.POST})
    public @ResponseBody
    ResultModel<Boolean> addFont(String projectName, String id) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            ProjectVersion version = getClient().getProjectVersionByName(projectName);
            String[] ids = StringUtility.split(id, ";");

            ProjectConfig config = version.getProject().getConfig();
            List<String> fontIds = config.getFonts();
            for (String apiId : ids) {
                if (!fontIds.contains(apiId)) {
                    fontIds.add(apiId);
                }
            }

            config.setFonts(fontIds);
            version.updateConfig(config);

        } catch (JDSException e) {
            result = new ErrorResultModel();
            ((ErrorResultModel) result).setErrcode(JDSException.APPLICATIONNOTFOUNDERROR);
            ((ErrorResultModel) result).setErrdes(e.getMessage());

        }
        return result;
    }


    @MethodChinaName(cname = "获取字体文件定义")
    @RequestMapping(value = {"getFontTreeProject"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<FontNode>> getFontTreeProject() {
        ListResultModel<List<FontNode>> result = new ListResultModel<List<FontNode>>();
        List<FontNode> fontNodes = new ArrayList<FontNode>();
        try {
            List<Project> projects = getClient().getResourceAllProject(ProjectResourceType.font);
            for (Project project : projects) {

                fontNodes.add(new FontNode(project));
            }
            result.setData(fontNodes);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "字体类配置")
    @RequestMapping(value = {"delFont"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ResultModel<Boolean> delFont(String projectName, String id) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        List<String> fontConfigIds = new ArrayList<String>();
        try {
            Project project = getClient().getProjectByName(projectName);
            fontConfigIds = project.getConfig().getFonts();
            fontConfigIds.remove(id);
            this.getClient().updateProjectConfig(project.getId(), project.getConfig());
        } catch (JDSException e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            errorResult.setErrcode(e.getErrorCode());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "字体类配置")
    @RequestMapping(value = {"getProjectFonts"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<FontConfig>> getProjectFonts(String projectName) {
        ListResultModel<List<FontConfig>> result = new ListResultModel<List<FontConfig>>();

        try {
            Project project = getClient().getProjectByName(projectName);
            List<FontConfig> fontConfigs = project.getFonts();
            result.setData(fontConfigs);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ESDClient getClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }

}
