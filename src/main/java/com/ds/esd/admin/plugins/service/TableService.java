package com.ds.esd.admin.plugins.service;

import com.ds.common.database.ProfiledConnectionEntry;
import com.ds.common.database.metadata.ColInfo;
import com.ds.common.database.metadata.SqlExcuteInfo;
import com.ds.common.database.metadata.SqlType;
import com.ds.common.database.metadata.TableInfo;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.esd.admin.plugins.fdt.node.UIColNode;
import com.ds.esd.admin.plugins.fdt.node.UITableNode;
import com.ds.esd.admin.plugins.fdt.node.UITopNode;

import java.util.List;

public interface TableService {

    /**
     * 添加一列
     *
     * @param col
     * @return
     */
    public ResultModel<Boolean> addCol(ColInfo col);

    /**
     * 添加数据库表
     *
     * @param tableInfo
     * @return
     */
    public ResultModel<Boolean> addTable(TableInfo tableInfo);

    /**
     * 添加数据库表
     *
     * @param tableInfo
     * @return
     */
    public ResultModel<Boolean> editTable(TableInfo tableInfo);

//
//    /**
//     * 选择目录树
//     *
//     * @param path
//     * @return
//     */
//    public ResultModel<UIFolderNode> GetFolderTree(String path, String projectName);

    /**
     * 删除数据库表
     *
     * @param tableName
     * @return
     */
    public ResultModel<Boolean> dropTable(String tableName, String configKey);

    /**
     * 获取指定表所有列信息
     *
     * @param tableName
     * @return
     */
    public ListResultModel<List<ColInfo>> getAllCollByTableName(String tableName, String configKey);


    /**
     * 根据表前缀获取所有数据库表
     *
     * @param simpleName 表名前缀过滤 如 FDT_ 则只显示，FDT开头的数据库表。
     * @return
     */
    public ListResultModel<List<TableInfo>> getAllTableByName(String simpleName, String configKey);


    /**
     * @param configKey
     * @param projectName
     * @return
     */
    public ListResultModel<List<TableInfo>> getProjectTables(String configKey, String projectName);


    /**
     * 生成数据库表
     *
     * @param info
     * @return
     */
    public ResultModel<Boolean> createTable(TableInfo info);

    /**
     * 获取数据表
     *
     * @return
     */

    public ListResultModel<List<UITopNode>> getAllTableTrees(String text, String configKey, String pattern);

    /**
     * 获取数据表
     *
     * @return
     */

    public ListResultModel<List<UITableNode>> getTableTrees(String pattern, String configKey, String projectName);

    public ListResultModel<List<UITopNode>> GetDbTrees(String projectName);

    public ListResultModel<List<UIColNode>> getColTree(String tableName, String configKey);

    public ListResultModel<List<SqlExcuteInfo>> getSqlCountInfo(String configKey);

    public ListResultModel<List<ProfiledConnectionEntry>> getSqlExcuteInfos(String configKey, SqlType type);

    public ResultModel<Boolean> startSqlMonitor(String configKey);

    public ResultModel<Boolean> stopSqlMonitor(String configKey);

    public ResultModel<Boolean> resetStatistics(String configKey);


}