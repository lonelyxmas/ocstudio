package com.ds.esd.admin.plugins.fdt.node;

import com.ds.common.database.metadata.ProviderConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UIDBConfigNode {


    public String id;

    public String path;

    public boolean disabled = false;

    public boolean group = false;

    public boolean draggable = true;

    public String key;

    public String cls = "xui.Module";

    public String imageClass = "iconfont iconchucun";

    public String caption;

    public String url;


    public boolean iniFold = false;


    public Map<String, String> tagVar = new HashMap<String, String>();

    List<UITableNode> sub;

    public UIDBConfigNode(ProviderConfig config) {
        this.id = config.getConfigKey();
        this.caption = config.getConfigKey();
        this.url = config.getServerURL();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public void addSub(UITableNode subNode) {
        if (sub == null) {
            sub = new ArrayList<UITableNode>();
        }
        sub.add(subNode);
    }

    public List<UITableNode> getSub() {
        return sub;
    }

    public void setSub(List<UITableNode> sub) {
        this.sub = sub;
    }

    public boolean isIniFold() {
        return iniFold;
    }

    public void setIniFold(boolean iniFold) {
        this.iniFold = iniFold;
    }

    public Map<String, String> getTagVar() {
        return tagVar;
    }

    public void setTagVar(Map<String, String> tagVar) {
        this.tagVar = tagVar;
    }

}
