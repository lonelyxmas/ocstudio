package com.ds.esd.admin.plugins.service;

import com.ds.cluster.ServerNode;
import com.ds.cluster.ServerNodeList;
import com.ds.cluster.service.ServerEventFactory;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.EsbFlowType;
import com.ds.esb.config.manager.EsbBean;
import com.ds.esb.config.manager.EsbBeanFactory;
import com.ds.esb.config.manager.ExpressionTempBean;
import com.ds.esd.admin.node.*;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.server.JDSServer;
import com.ds.server.SubSystem;
import com.ds.server.eumus.ConfigCode;
import com.ds.server.eumus.SystemType;
import com.ds.server.service.SysWebManager;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@MethodChinaName(cname = "集群服务")
@RequestMapping("/admin/system/")
public class SystemService {


    @MethodChinaName(cname = "获取所有系统")
    @RequestMapping(value = {"getAllSystem"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUISystem>> getAllSystem(String classification) {
        ListResultModel<List<XUISystem>> userStatusInfo = new ListResultModel<List<XUISystem>>();
        try {
            List<ServerNode> serverNodes = JDSServer.getInstance().getClusterClient().getAllServer();

            List<ServerNode> sysNodes = new ArrayList<>();

            for (ServerNode node : serverNodes) {
                SubSystem subSystem = JDSServer.getInstance().getClusterClient().getSystem(node.getId());
                if (subSystem != null && subSystem.getType() != null && subSystem.getType().equals(SystemType.system)) {
                    sysNodes.add(node);
                }
            }

            userStatusInfo = PageUtil.getDefaultPageList(sysNodes, XUISystem.class);
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }

        return userStatusInfo;
    }


    @MethodChinaName(cname = "获取所有应用配置")
    @RequestMapping(value = {"getApplications"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIApplication>> getApplications() {
        List<CApplication> applications = JDSServer.getClusterClient().getApplications();
        ListResultModel userStatusInfo = PageUtil.getDefaultPageList(applications, XUIApplication.class);
        return userStatusInfo;
    }

    @MethodChinaName(cname = "获取所有节点服务")
    @RequestMapping(value = {"getServerNodes"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIServerNode>> getServerNodes(String code) {
        ServerNodeList serverNodeList = JDSServer.getClusterClient().getServerNodeListByConfigCode(ConfigCode.fromType(code));
        ListResultModel<List<XUIServerNode>> userStatusInfo = PageUtil.getDefaultPageList(serverNodeList.getServerNodeList(), XUIServerNode.class);
        return userStatusInfo;
    }

    @MethodChinaName(cname = "获取节点信息")
    @RequestMapping(value = {"getServerNodeInfo"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ResultModel<XUIServerNode> getServerNodeInfo(String code) {
        ServerNode serverNode = JDSServer.getClusterClient().getServerNodeById(code);
        ResultModel<XUIServerNode> userStatusInfo = new ResultModel<XUIServerNode>();
        userStatusInfo.setData(new XUIServerNode(serverNode));
        return userStatusInfo;
    }

    @MethodChinaName(cname = "获取远程服务信息")
    @RequestMapping(value = {"getEsbBeanListBean"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<EsbBean>> getEsbTemp() {
        List<EsbBean> esbBeans = EsbBeanFactory.getInstance().getEsbBeanList();
        ListResultModel userStatusInfo = PageUtil.getDefaultPageList(esbBeans);
        return userStatusInfo;
    }


    @ResponseBody
    @RequestMapping(value = {"getServcieByType"}, method = {RequestMethod.POST, RequestMethod.GET})
    public ListResultModel<List<ServiceNode>> getServcieByType() {
        ListResultModel<List<ServiceNode>> module = new ListResultModel<List<ServiceNode>>();
        List<ServiceNode> serviceNodes = new ArrayList<ServiceNode>();
        EsbFlowType[] types = EsbFlowType.values();
        for (EsbFlowType type : types) {
            ServiceNode serviceNode = new ServiceNode(type);
            serviceNodes.add(serviceNode);
        }
        module.setData(serviceNodes);
        return module;
    }

    @MethodChinaName(cname = "所有消息事件信息")
    @RequestMapping(value = {"getAllEvent"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<Set<ExpressionTempBean>> getAllEvent() {
        Set<ExpressionTempBean> beanSet = ServerEventFactory.getInstance().getAllRegisterEvent();
        List<ExpressionTempBean> esbBeans = Arrays.asList(beanSet.toArray(new ExpressionTempBean[beanSet.size()]));
        ListResultModel userStatusInfo = PageUtil.getDefaultPageList(esbBeans);
        return userStatusInfo;
    }


    @MethodChinaName(cname = "获取注册服务信息")
    @RequestMapping(value = {"getBusBeanTemp"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<Set<ExpressionTempBean>> getBusBeanTemp(String flowTypes) {
        List<EsbFlowType> types = new ArrayList<EsbFlowType>();
        if (flowTypes != null) {
            if (flowTypes.equals("all")) {
                types.addAll(Arrays.asList(EsbFlowType.values()));
            } else {
                String[] flowTypeArr = StringUtility.split(flowTypes, ";");
                for (String app : flowTypeArr) {
                    types.add(EsbFlowType.fromType(app));
                }
            }


        }

        List<ExpressionTempBean> esbBeans = (List<ExpressionTempBean>) EsbBeanFactory.getInstance().getServiceBeanByFlowType(types.toArray(new EsbFlowType[]{}));
        ListResultModel userStatusInfo = PageUtil.getDefaultPageList(esbBeans);
        return userStatusInfo;
    }

    @MethodChinaName(cname = "获取远程服务信息")
    @RequestMapping(value = {"getRemoteClusterSevice"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIService>> getRemoteClusterSevice(String classification) {
        ListResultModel<List<XUIService>> userStatusInfo = new ListResultModel<List<XUIService>>();
        try {
            userStatusInfo = PageUtil.getDefaultPageList(JDSServer.getInstance().getClusterSevice(), XUIService.class);
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }

        return userStatusInfo;
    }

    @MethodChinaName(cname = "更新系统服务信息")
    @RequestMapping(value = {"updateSystemInfo"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ResultModel<Boolean> updateSystemInfo(@RequestBody SubSystem subSystem) {
        return saveSysem(subSystem);
    }

//    @RequestMapping(value = {"/enableSystem"}, method = {RequestMethod.GET, RequestMethod.POST})
//    public @ResponseBody
//    ResultModel<Boolean> enableSystem(String systemIds) {
//        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
//        try {
//           String[] systemIdArr=systemIds.split(";");
//                   for(String systemId:systemIdArr){
//                        EISubSystem system= getSysWebManager().get
//
//                            system.setGuestenable(0);
//                            saveSysem(system);
//                        }
//                   }
//
//        } catch (JDSException e) {
//            userStatusInfo = new ErrorResultModel();
//            ((ErrorResultModel) userStatusInfo).setErrcode(e.getErrorCode());
//            ((ErrorResultModel) userStatusInfo).setErrdes(e.getMessage());
//        }
//
//        return userStatusInfo;
//    }
//
//    @RequestMapping(value = {"/disableSystem"}, method = {RequestMethod.GET, RequestMethod.POST})
//    public @ResponseBody
//    ResultModel<Boolean> disableSystem(String systemIds) {
//        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
//        try {
//            String[] systemIdArr=systemIds.split(";");
//            for(String systemId:systemIdArr){
//                EISubSystem system=JDSServer.getInstance().getRemoteSystem(systemId);
//
//                if (system.getGuestenable()==null || system.getGuestenable()!=1){
//                    system.setGuestenable(-1);
//                    saveSysem(system);
//                }
//            }
//        } catch (JDSException e) {
//            userStatusInfo = new ErrorResultModel();
//            ((ErrorResultModel) userStatusInfo).setErrcode(e.getErrorCode());
//            ((ErrorResultModel) userStatusInfo).setErrdes(e.getMessage());
//        }
//
//        return userStatusInfo;
//    }


//    @RequestMapping(value = {"/getAllCache"}, method = {RequestMethod.GET, RequestMethod.POST})
//    public @ResponseBody
//    ListResultModel<List<XUISystem>> getAllCache() {
//        ListResultModel<List<XUISystem>> userStatusInfo = new ListResultModel<List<XUISystem>>();
//        List<XUISystem> modules = new ArrayList<XUISystem>();
//        List<EISubSystem> systems = null;
//        try {
//            systems = JDSServer.getInstance().getRemoteSystems();
//            for (EISubSystem subSystem : systems) {
//                modules.add(new XUISystem(subSystem));
//            }
//            ;
//            userStatusInfo.setSize(modules.size());
//            userStatusInfo.setData(modules);
//
//
//        } catch (JDSException e) {
//            userStatusInfo = new ErrorListResultModel();
//            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
//            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
//        }
//
//        return userStatusInfo;
//    }

    ResultModel<Boolean> saveSysem(SubSystem subSystem) {

        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        try {
            getSysWebManager().saveSystemInfo(subSystem).get();
        } catch (JDSException e) {
            userStatusInfo = new ErrorResultModel();
            ((ErrorResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorResultModel) userStatusInfo).setErrdes(e.getMessage());
        }

        return userStatusInfo;
    }

    SysWebManager getSysWebManager() {
        return (SysWebManager) EsbUtil.parExpression("$SysWebManager");
    }

}
