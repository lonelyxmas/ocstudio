package com.ds.esd.admin.node;

import com.ds.common.util.DateUtility;
import com.ds.msg.LogMsg;

import java.util.Date;

public class XUIDataLog {


    String msgId;

    String event;

    String sensorieee;

    String gwieee;


    String body;

    String type;

    String time;

    public XUIDataLog(LogMsg msg) {
        this.msgId = msg.getId();
        this.event = msg.getTitle();
        this.type = msg.getType();
        this.sensorieee = msg.getSensorId();
        this.gwieee = msg.getGatewayId();

        this.time = DateUtility.formatDate(new Date(msg.getEventTime() == null ? msg.getReceiveTime() : msg.getEventTime()), "yyyy-MM-dd HH:mm:ss");
        this.body = msg.getBody();

    }


    public String getSensorieee() {
        return sensorieee;
    }

    public void setSensorieee(String sensorieee) {
        this.sensorieee = sensorieee;
    }

    public String getGwieee() {
        return gwieee;
    }

    public void setGwieee(String gwieee) {
        this.gwieee = gwieee;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
