package com.ds.esd.admin.node;

import java.util.ArrayList;
import java.util.List;

public class XUIModule {

    String conf = "{imageWidth:64,imageWidth:64}";

    boolean OK = true;

    String content;

    String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    List<XUIFile> files = new ArrayList<XUIFile>();

    public String getContent() {

        return content;
    }

    public boolean isOK() {
        return OK;
    }

    public void setOK(boolean OK) {
        this.OK = OK;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public XUIModule() {
    }

    public String getConf() {
        return conf;
    }

    public void setConf(String conf) {
        this.conf = conf;
    }

    public List<XUIFile> getFiles() {
        return files;
    }

    public void setFiles(List<XUIFile> files) {
        this.files = files;
    }
}
