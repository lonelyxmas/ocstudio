package com.ds.esd.admin.node;

import com.ds.cluster.ServerNode;
import com.ds.cluster.service.ServerEventFactory;
import com.ds.common.JDSException;
import com.ds.esb.config.manager.ExpressionTempBean;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.org.OrgManager;
import com.ds.server.JDSServer;
import com.ds.server.OrgManagerFactory;
import com.ds.server.SubSystem;
import com.ds.server.eumus.SystemNodeType;
import com.ds.server.eumus.SystemStatus;

import java.util.List;

public class XUIServerNode {


    String id;
    String name;
    String desc;
    String maxconnection = "2000";
    String minconnection = "200";
    String timeout = "60";
    String url = "";
    String userexpression;
    SystemNodeType type;
    String gwmserver;
    String repeatEventKey = "";
    XUISystem xuisubSystem;

    public SystemStatus status = SystemStatus.ONLINE;
    public int currCount, maxCounts;
    public Integer checkTimes = 0;


    public XUIServerNode(ServerNode node) {
        SubSystem subSystem = null;
        try {
            subSystem = JDSServer.getInstance().getClusterClient().getSystem(node.getId());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        try {
            xuisubSystem = new XUISystem(node);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        String systemId = subSystem.getSysId();

        OrgManager orgManager = OrgManagerFactory.getOrgManager(subSystem.getConfigname());
        status = JDSServer.getClusterClient().getSystemStatus(systemId);
        this.id = node.getId();
        this.name = node.getName();
        this.desc = node.getDesc();
        this.maxconnection = node.getMaxconnection();
        this.timeout = node.getTimeout();
        this.url = node.getUrl();
        this.userexpression = node.getUserexpression();
        this.type = node.getType();
        this.checkTimes = node.getCheckTimes();
        ServerEventFactory factory = ServerEventFactory.getInstance();
        List<ExpressionTempBean> serviceBeans = factory.getRegisterEventByCode(node.getId());

        for (ServiceBean serviceBean : serviceBeans) {
            repeatEventKey = repeatEventKey + serviceBean.getId() + ",";
        }
        if (repeatEventKey.endsWith(",")) {
            repeatEventKey = repeatEventKey.substring(0, repeatEventKey.length() - 1);
        }

    }

    public String getRepeatEventKey() {
        return repeatEventKey;
    }

    public void setRepeatEventKey(String repeatEventKey) {
        this.repeatEventKey = repeatEventKey;
    }

    public XUISystem getXuisubSystem() {
        return xuisubSystem;
    }

    public void setXuisubSystem(XUISystem xuisubSystem) {
        this.xuisubSystem = xuisubSystem;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMaxconnection() {
        return maxconnection;
    }

    public void setMaxconnection(String maxconnection) {
        this.maxconnection = maxconnection;
    }

    public String getMinconnection() {
        return minconnection;
    }

    public void setMinconnection(String minconnection) {
        this.minconnection = minconnection;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserexpression() {
        return userexpression;
    }

    public void setUserexpression(String userexpression) {
        this.userexpression = userexpression;
    }

    public SystemNodeType getType() {
        return type;
    }

    public void setType(SystemNodeType type) {
        this.type = type;
    }

    public String getGwmserver() {
        return gwmserver;
    }

    public void setGwmserver(String gwmserver) {
        this.gwmserver = gwmserver;
    }


    public SystemStatus getStatus() {
        return status;
    }

    public void setStatus(SystemStatus status) {
        this.status = status;
    }

    public int getCurrCount() {
        return currCount;
    }

    public void setCurrCount(int currCount) {
        this.currCount = currCount;
    }

    public int getMaxCounts() {
        return maxCounts;
    }

    public void setMaxCounts(int maxCounts) {
        this.maxCounts = maxCounts;
    }

    public Integer getCheckTimes() {
        return checkTimes;
    }

    public void setCheckTimes(Integer checkTimes) {
        this.checkTimes = checkTimes;
    }

    @Override
    public String toString() {
        return "id=" + id + ";serverurl" + this.url;
    }


}
