package com.ds.esd.admin.node;

import com.ds.server.JDSServer;
import com.ds.server.SubSystem;
import com.ds.server.eumus.SystemStatus;
import com.ds.server.eumus.SystemType;

public class XUICache {


    String name;

    SystemStatus status;

    SystemType type;

    String url;

    String time;


    public XUICache(SubSystem subSystem){

        String systemId=  subSystem.getSysId();
        status= JDSServer.getClusterClient().getSystemStatus(systemId);
        this.name=subSystem.getName();
        this.type=subSystem.getType();
        this.url=subSystem.getUrl();
        this.time=subSystem.getAdminId();


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SystemStatus getStatus() {
        return status;
    }

    public void setStatus(SystemStatus status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public SystemType getType() {
        return type;
    }

    public void setType(SystemType type) {
        this.type = type;
    }
}
