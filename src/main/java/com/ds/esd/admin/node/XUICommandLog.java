package com.ds.esd.admin.node;

import com.ds.common.util.DateUtility;
import com.ds.msg.CommandMsg;
import com.ds.org.Person;
import com.ds.org.PersonNotFoundException;
import com.ds.server.OrgManagerFactory;

import java.util.Date;

public class XUICommandLog {
    String commandid;
    String cmd;
    String send;
    String status;
    String commandStr;
    String time;


    String sensorieee;

    String gwieee;


    public XUICommandLog(CommandMsg commandMsg) {
        this.commandid = commandMsg.getId();
        this.cmd = commandMsg.getEvent();
        Date datatime = new Date(commandMsg.getReceiveTime());
        this.time = DateUtility.formatDate(datatime, "yyyy-MM-dd HH:mm:ss");
        Person person = null;
        try {
            person = OrgManagerFactory.getOrgManager().getPersonByID(commandMsg.getFrom());
            this.send = person.getName();
        } catch (PersonNotFoundException e) {
            e.printStackTrace();
        }
        this.gwieee=commandMsg.getGatewayId();
        this.sensorieee=commandMsg.getSensorId();
        this.status = commandMsg.getResultCode().getName();
        this.commandStr = commandMsg.getBody();

    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCommandid() {
        return commandid;
    }

    public void setCommandid(String commandid) {
        this.commandid = commandid;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getSend() {
        return send;
    }

    public void setSend(String send) {
        this.send = send;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCommandStr() {
        return commandStr;
    }

    public void setCommandStr(String commandStr) {
        this.commandStr = commandStr;
    }

    public String getSensorieee() {
        return sensorieee;
    }

    public void setSensorieee(String sensorieee) {
        this.sensorieee = sensorieee;
    }

    public String getGwieee() {
        return gwieee;
    }

    public void setGwieee(String gwieee) {
        this.gwieee = gwieee;
    }

}
