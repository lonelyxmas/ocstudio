package com.ds.esd.admin.node;

public class XUIParas {

    String action;
    String hashCode;
    String path;
    String deep;
    String content;
    String pattern;
    Boolean withConfig=true;
    String curProjectPath;
    String curpath;

    Integer type;




    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getCurProjectPath() {
        return curProjectPath;
    }

    public void setCurProjectPath(String curProjectPath) {
        this.curProjectPath = curProjectPath;
    }

    public String getDeep() {
        return deep;
    }

    public void setDeep(String deep) {
        this.deep = deep;
    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getHashCode() {
        return hashCode;
    }

    public void setHashCode(String hashCode) {
        this.hashCode = hashCode;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Boolean getWithConfig() {
        return withConfig;
    }

    public void setWithConfig(Boolean withConfig) {
        this.withConfig = withConfig;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}