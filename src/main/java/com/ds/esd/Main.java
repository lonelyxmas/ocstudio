package com.ds.esd;

import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.index.IndexLeftViewAnnotation;
import com.ds.esd.custom.annotation.index.IndexViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin/")
@MethodChinaName(cname = "一级导航")
public class Main {


    @MethodChinaName(cname = "首页")
    @RequestMapping(method = RequestMethod.POST, value = "Index")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.index)
    @IndexViewAnnotation
    @ModuleAnnotation(caption = "首页")
    @ResponseBody
    public ResultModel<IndexNav> index(String projectId) {
        ResultModel<IndexNav> result = new ResultModel<IndexNav>();
        return result;
    }


    @MethodChinaName(cname = "首页2")
    @RequestMapping(method = RequestMethod.POST, value = "Index2")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.index)
    @IndexLeftViewAnnotation
    @ModuleAnnotation(caption = "首页2")
    @ResponseBody
    public ResultModel<Index2Nav> index2(String projectId) {
        ResultModel<Index2Nav> result = new ResultModel<Index2Nav>();
        return result;
    }

}
