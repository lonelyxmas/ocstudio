package com.ds.esd;

import com.ds.config.ResultModel;
import com.ds.esd.custom.index.annotation.IndexLeftAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutItemAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutViewAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.LayoutType;
import com.ds.esd.tool.ui.enums.PosType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin/")
@IndexLeftAnnotation
@LayoutAnnotation(transparent = false, type = LayoutType.horizontal)
public class Index2Nav {

    @ModuleAnnotation()
    @LayoutViewAnnotation
    @LayoutItemAnnotation(panelBgClr = "#3498DB", size = 220, pos = PosType.before, locked = true, moveDisplay = false, cmd = false)
    @RequestMapping(method = RequestMethod.POST, value = "IndexLeft")
    public ResultModel<IndexLeftNav> getIndexLeftNav() {
        return new ResultModel<IndexLeftNav>();
    }

    @ModuleAnnotation()
    @LayoutViewAnnotation
    @LayoutItemAnnotation(pos = PosType.main)
    @RequestMapping(method = RequestMethod.POST, value = "IndexMainNav")
    @ResponseBody
    public ResultModel<IndexMainNav> getIndexMainNav() {
        return new ResultModel<IndexMainNav>();
    }


}
