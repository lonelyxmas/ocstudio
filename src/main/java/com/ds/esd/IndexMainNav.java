package com.ds.esd;

import com.ds.bpm.client.ProcessDefVersion;
import com.ds.bpm.engine.WorkflowClientService;
import com.ds.bpm.engine.query.BPMCondition;
import com.ds.bpm.engine.query.BPMConditionKey;
import com.ds.bpm.enums.process.ProcessDefVersionStatus;
import com.ds.common.JDSException;
import com.ds.common.query.JoinOperator;
import com.ds.common.query.Operator;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.worklist.view.ProcessDefVersionView;
import com.ds.esd.bpm.worklist.view.ProcessInstListView;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.index.left.IndexMainBarComponent;
import com.ds.esd.custom.layout.annotation.LayoutAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutItemAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.LayoutType;
import com.ds.esd.tool.ui.enums.PosType;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.server.JDSClientService;
import com.ds.server.JDSServer;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/")
@MethodChinaName(cname = "管理员控制台")
@LayoutAnnotation(transparent = false, type = LayoutType.vertical, items = {})
public class IndexMainNav {

    @ModuleAnnotation
    @LayoutItemAnnotation(panelBgClr = "#3498DB", size = 80, pos = PosType.before, locked = true, moveDisplay = false, cmd = false)
    public ResultModel<IndexMainBarComponent> getCenterTopBar() {
        return new ResultModel<IndexMainBarComponent>();
    }


    @MethodChinaName(cname = "流程定义列表")
    @RequestMapping(method = RequestMethod.POST, value = "ProcessDefVersionList")
    @GridViewAnnotation(editorPath = "NewProcess")
    @LayoutItemAnnotation(pos = PosType.main)
    @ModuleAnnotation(imageClass = "bpmfont bpmgongzuoliuzhutiguizeweihuguanli", caption = "流程定义列表")
    public @ResponseBody
    ListResultModel<List<ProcessDefVersionView>> getProcessDefVersionList(String projectName) {
        ListResultModel result = new ListResultModel();
        try {
            BPMCondition condition = new BPMCondition(BPMConditionKey.PROCESSDEF_VERSION_PUBLICATIONSTATUS, Operator.EQUALS, ProcessDefVersionStatus.RELEASED.getType());
            BPMCondition sysCondition = new BPMCondition(BPMConditionKey.PROCESSDEF_VERSION_PROCESSDEF_ID, Operator.IN, "SELECT PROCESSDEF_ID FROM BPM_PROCESSDEF WHERE SYSTEMCODE='" + ESDFacrory.getESDClient().getSystemCode() + "'");
            condition.addCondition(sysCondition, JoinOperator.JOIN_AND);
            if (projectName != null && !projectName.equals("")) {
                Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
                BPMCondition projectCondition = new BPMCondition(BPMConditionKey.PROCESSDEF_VERSION_PROCESSDEF_ID, Operator.IN, "SELECT PROCESSDEF_ID FROM BPM_PROCESSDEF WHERE CLASSIFICATION='" + project.getProjectName() + "'");
                condition.addCondition(projectCondition, JoinOperator.JOIN_AND);
            }
            ListResultModel<List<ProcessDefVersion>> versionList = getClient().getProcessDefVersionList(condition, null, null);
            result = PageUtil.changPageList(versionList, ProcessDefVersionView.class);
        } catch (Exception e) {
            result = new ErrorListResultModel();
            ((ErrorListResultModel<List<ProcessInstListView>>) result).setErrdes(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @return
     */
    private WorkflowClientService getClient() {
        JDSClientService service = JDSActionContext.getActionContext().Par(JDSClientService.class);
        if (service == null) {
            try {
                JDSActionContext.getActionContext().getSession().put("JDSUSERID", JDSServer.getInstance().getAdminUser().getId());
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }
        WorkflowClientService client = EsbUtil.parExpression("$BPMC", WorkflowClientService.class);
        return client;
    }

}
