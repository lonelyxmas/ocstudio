package com.ds.esd;

import com.ds.esd.editor.extmenu.PluginsPostationType;
import com.ds.esd.tool.ui.component.list.TreeListItem;

import java.util.HashMap;

public class PluginMenuItem extends TreeListItem {

    public PluginsPostationType menuType = PluginsPostationType.topMenu;

    public PluginMenuItem() {
        RightPluginType[] menuTypes = RightPluginType.values();
        this.setId("rights");
        this.setCaption("权限");
        for (RightPluginType menuType : menuTypes) {
            this.addChild(new PluginMenuItem(menuType));
        }

    }

    public PluginMenuItem(RightPluginType menuType) {
        this.caption = menuType.getCaption();
        this.setImageClass(menuType.getImageClass());
        this.setId(menuType.getId());
        this.setIniFold(false);
        this.setEuClassName(menuType.getClassName());
        this.tagVar = new HashMap<>();
        this.tagVar.put("rightType", menuType.getId());

    }

    public PluginsPostationType getMenuType() {
        return menuType;
    }

    public void setMenuType(PluginsPostationType menuType) {
        this.menuType = menuType;
    }

}
