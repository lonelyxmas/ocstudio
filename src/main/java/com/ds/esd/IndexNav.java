package com.ds.esd;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.iorg.IOrgNav;
import com.ds.bpm.plugins.bpd.PluginTypeTree;
import com.ds.bpm.plugins.bpd.PluginsItems;
import com.ds.bpm.plugins.listener.ListenerItems;
import com.ds.bpm.plugins.nav.ListenerTypeTree;
import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.worklist.WorkListService;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.nav.NavButtonViewsViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavFoldingTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.index.annotation.IndexAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutItemAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.LayoutType;
import com.ds.esd.tool.ui.enums.PosType;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/admin/")
@MethodChinaName(cname = "管理员控制台")
@IndexAnnotation
@LayoutAnnotation(transparent = false, type = LayoutType.vertical, items = {@LayoutItemAnnotation(panelBgClr = "#3498DB", size = 28, pos = PosType.before, locked = true, moveDisplay = false, cmd = false)})
public class IndexNav {


    @MethodChinaName(cname = "流程实例")
    @NavButtonViewsViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", caption = "流程样例")
    @APIEventAnnotation(index = 0)
    @RequestMapping(method = RequestMethod.POST, value = "BPMNav")
    @ResponseBody
    public ResultModel<WorkListService> getBPMNav() {
        return new ResultModel<WorkListService>();
    }


    @MethodChinaName(cname = "组织权限")
    @RequestMapping(method = RequestMethod.POST, value = "OrgNav")
    @NavFoldingTreeViewAnnotation
    @APIEventAnnotation(index = 1)
    @ModuleAnnotation(imageClass = "bpmfont bpmgongzuoliu", caption = "组织权限", dynLoad = true, cache = false)
    @ResponseBody
    public ResultModel<IOrgNav> getOrgManager() {
        return new ResultModel<IOrgNav>();
    }

    @APIEventAnnotation(index = 2)
    @RequestMapping(method = RequestMethod.POST, value = "PluginNavTree")
    @ModuleAnnotation(caption = "工作流插件", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi", dynLoad = true)
    @NavTreeViewAnnotation
    @ResponseBody
    public TreeListResultModel<List<PluginTypeTree>> getPlugins(String id, String projectVersionName) {
        TreeListResultModel<List<PluginTypeTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(PluginsItems.values()), PluginTypeTree.class);
        return result;
    }

    @APIEventAnnotation(index = 3)
    @RequestMapping(method = RequestMethod.POST, value = "ListenerNavTree")
    @NavTreeViewAnnotation()
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf1", caption = "监听器", dynLoad = true)
    @ResponseBody
    public TreeListResultModel<List<ListenerTypeTree>> getListeners(String id, String projectVersionName) {
        TreeListResultModel<List<ListenerTypeTree>> result = new TreeListResultModel<>();
        result = TreePageUtil.getTreeList(Arrays.asList(ListenerItems.values()), ListenerTypeTree.class);

        return result;
    }


//
//
//    @MethodChinaName(cname = "流程配置")
//    @RequestMapping(method = RequestMethod.POST, value = "BPDConfig")
//    @NavTreeViewAnnotation
//    @APIEventAnnotation(index = 2)
//    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", caption = "流程配置", dynLoad = true, cache = false)
//    @ResponseBody
//    public ResultModel<BPDManagerTree> getBPMConfig() {
//        return new ResultModel<BPDManagerTree>();
//    }


    @JSONField(serialize = false)
    public ESDClient getEsdClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }
}
