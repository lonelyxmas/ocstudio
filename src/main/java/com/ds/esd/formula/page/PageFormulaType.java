package com.ds.esd.formula.page;


import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.esd.formula.page.service.PageActionFormulaService;
import com.ds.esd.formula.page.service.EComFormulaService;


public enum PageFormulaType implements TreeItem {

    ESDCOM(EComFormulaService.class, "页面组件", "spafont spa-icon-module", true, false, false),

    ActionSelectedID(PageActionFormulaService.class, "页面动作", "spafont spa-icon-frame", true, false, false);


    private final String name;

    private final Class bindClass;

    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    PageFormulaType(Class bindClass, String name, String imageClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {

        this.name = name;
        this.bindClass = bindClass;
        this.imageClass = imageClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;
    }


    public String getName() {
        return name;
    }

    @Override
    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String getImageClass() {
        return imageClass;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public static PageFormulaType fromName(String name) {
        for (PageFormulaType type : PageFormulaType.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    @Override
    public String getType() {
        return name();
    }

}
