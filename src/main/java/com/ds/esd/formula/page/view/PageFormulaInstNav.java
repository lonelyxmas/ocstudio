package com.ds.esd.formula.page.view;

import com.ds.bpm.formula.ExpressionParameter;
import com.ds.bpm.formula.FormulaService;
import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.CustomViewFactory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.FormulaParamsEnums;
import com.ds.esd.project.config.formula.ModuleFormulaInst;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.web.APIConfig;
import com.ds.web.APIConfigFactory;
import javassist.NotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/esd/page/module/")
@BottomBarMenu
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Close})
public class PageFormulaInstNav {

    @CustomAnnotation(pid = true, hidden = true)
    public String formulaInstId;
    @CustomAnnotation(pid = true, hidden = true)
    public String projectName;
    @CustomAnnotation(pid = true, hidden = true)
    public String className;

    @FormViewAnnotation()
    @RequestMapping(path = "FormulaInfo")
    @ModuleAnnotation(dock = Dock.top, caption = "公式信息")
    @UIAnnotation(height = "220")
    @ResponseBody
    public ResultModel<PageFormInstView> getFormulaInfo(String formulaInstId, String projectName, String className) {
        ResultModel<PageFormInstView> model = new ResultModel<>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(className, version.getVersionName());
            ModuleFormulaInst inst = getFormula(formulaInstId, euModule);
            PageFormInstView selectView = new PageFormInstView(inst);
            model.setData(selectView);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return model;
    }

    @RequestMapping(path = "ParamtersNav")
    @NavTabsViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, dynLoad = true, caption = "参数配置")
    @ResponseBody
    public TreeListResultModel<List<TreeListItem>> getParamters(String formulaInstId, String projectName, String className) {
        TreeListResultModel<List<TreeListItem>> model = new TreeListResultModel<>();
        List<TreeListItem> items = new ArrayList<>();
        try {
            if (projectName != null) {
                ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
                EUModule euModule = ESDFacrory.getESDClient().getModule(className, version.getVersionName());
                ModuleFormulaInst inst = getFormula(formulaInstId, euModule);
                ParticipantSelect select = getService().getParticipantSelect(inst.getParticipantSelectId()).get();
                List<ExpressionParameter> parameters = select.getParameterList();
                for (ExpressionParameter parameter : parameters) {
                    TreeListItem item = new TreeListItem();
                    item.setId(parameter.getParameterName());
                    item.setCaption(parameter.getParameterType().getName());
                    FormulaParamsEnums paramsEnums = FormulaParamsEnums.fromName(parameter.getParameterType().getType());
                    item.setImageClass(parameter.getParameterType().getImageClass());
                    if (paramsEnums != null) {
                        try {
                            Class clazz = paramsEnums.getClazz();

                            APIConfig apiConfig = APIConfigFactory.getInstance().getAPIConfig(clazz.getName());
                            ApiClassConfig esdClassAPIBean = new ApiClassConfig(apiConfig);
                            MethodConfig apiBean = esdClassAPIBean.getMethodByItem(CustomMenuItem.index);

                            if (!parameter.getSingle()) {
                                apiBean = esdClassAPIBean.getMethodByItem(CustomMenuItem.indexs);
                            }

                            Map<String, Object> context = JDSActionContext.getActionContext().getContext();
                            context.put("parameterCode", parameter.getParameterCode());
                            context.put("paramsType", parameter.getParameterType());
                            EUModule comModule = CustomViewFactory.getInstance().getViewByMethod(apiBean, version.getVersionName(), context);
                            if (euModule != null) {
                                item.setEuClassName(comModule.getClassName());
                                items.add(item);
                            }

                        } catch (NotFoundException e) {
                            e.printStackTrace();
                        }
                    }


                }
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        model.setData(items);

        return model;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    private ModuleFormulaInst getFormula(String formulaInstId, EUModule module) throws JDSException {
        Map<String, ModuleFormulaInst> formulaInsts = module.getComponent().getFormulas();
        return formulaInsts.get(formulaInstId);
    }

    public FormulaService getService() {
        FormulaService service = (FormulaService) EsbUtil.parExpression("$FormulaService");
        return service;
    }

}
