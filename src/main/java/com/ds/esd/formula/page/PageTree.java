package com.ds.esd.formula.page;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation
public class PageTree extends TreeListItem {


    @Pid
    String projectName;
    @Pid
    String currClassName;
    @Pid
    PageFormulaType rightFormulaType;


    @TreeItemAnnotation(customItems = PageFormulaType.class)
    public PageTree(PageFormulaType rightFormulaType, String currClassName, String projectName) {
        this.rightFormulaType = rightFormulaType;
        this.caption = rightFormulaType.getName();
        this.imageClass = rightFormulaType.getImageClass();
        this.id = rightFormulaType.getType();
        this.currClassName = currClassName;
        this.projectName = projectName;

    }

    public String getCurrClassName() {
        return currClassName;
    }

    public void setCurrClassName(String currClassName) {
        this.currClassName = currClassName;
    }


    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public PageFormulaType getRightFormulaType() {
        return rightFormulaType;
    }

    public void setRightFormulaType(PageFormulaType rightFormulaType) {
        this.rightFormulaType = rightFormulaType;
    }
}
