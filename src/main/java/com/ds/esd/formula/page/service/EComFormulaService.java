package com.ds.esd.formula.page.service;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.enums.OrgDomainType;
import com.ds.esd.formula.page.tree.EComFormulaPopTree;
import com.ds.esd.formula.page.view.EComActionGridView;
import com.ds.esd.formula.page.view.PageFormulaInstNav;
import com.ds.esd.project.config.formula.FormulaInst;
import com.ds.esd.project.config.formula.ModuleFormulaInst;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.annotation.Aggregation;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/esd/page/module/action/")
@MethodChinaName(cname = "组件切面", imageClass = "spafont spa-icon-c-imagebutton")
@Aggregation(rootClass = OrgDomainType.class)
public class EComFormulaService {
    @RequestMapping(method = RequestMethod.POST, value = "Index")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-imagebutton", caption = "组件切面")
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.reload, CustomMenuItem.index, CustomMenuItem.treeNodeEditor}, customRequestData = {RequestPathEnum.SPA_className})
    @ResponseBody
    public ListResultModel<List<EComActionGridView>> getFormulaInstList(String projectName, String currClassName) {
        ListResultModel<List<EComActionGridView>> resultModel = new ListResultModel<>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(currClassName, version.getVersionName());
            Map<String, ModuleFormulaInst> instMap = euModule.getComponent().getFormulas();
            List<FormulaInst> instList = new ArrayList<>();
            for (FormulaInst inst : instMap.values()) {
                if (inst != null && inst.getFormulaType() != null && inst.getFormulaType().equals(FormulaType.ESDCOM)) {
                    instList.add(inst);
                }
            }
            resultModel = PageUtil.getDefaultPageList(instList, EComActionGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }

    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @RequestMapping("loadChildItem")
    @ResponseBody
    public TreeListResultModel<List<EComFormulaPopTree>> loadChildItem(String currClassName, FormulaType formulaType) {
        TreeListResultModel<List<EComFormulaPopTree>> model = new TreeListResultModel<>();
        try {
            List<ParticipantSelect> selects = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulas(formulaType);
            model = TreePageUtil.getTreeList(selects, EComFormulaPopTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return model;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "添加表达式", imageClass = "spafont spa-icon-function")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add, customRequestData = {RequestPathEnum.SPA_className})
    @RequestMapping("SelectFormula")
    @DialogAnnotation(width = "300", height = "450")
    @ResponseBody
    public TreeListResultModel<List<EComFormulaPopTree>> getFormulaTree(String currClassName) {
        TreeListResultModel<List<EComFormulaPopTree>> model = new TreeListResultModel<>();
        model = TreePageUtil.getTreeList(Arrays.asList(FormulaType.ESDCOM), EComFormulaPopTree.class);
        return model;
    }

    @RequestMapping(method = RequestMethod.POST, value = "FormulaInstInfo")
    @NavGroupViewAnnotation
    @DialogAnnotation
    @ModuleAnnotation(caption = "添加参数", imageClass = "spafont spa-icon-function")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
    public @ResponseBody
    ResultModel<PageFormulaInstNav> getFormulaInstInfo(String formulaInstId, String projectName) {
        ResultModel<PageFormulaInstNav> model = new ResultModel<PageFormulaInstNav>();
        return model;
    }

    @MethodChinaName(cname = "删除表达式")
    @RequestMapping(value = {"delFormulaInst"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent}, bindMenu = {CustomMenuItem.delete}, customRequestData = {RequestPathEnum.SPA_className})
    public @ResponseBody
    ResultModel<Boolean> delFormulaInst(String formulaInstId, String projectName, String currClassName) {
        ResultModel<Boolean> resultModel = new ResultModel<>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(currClassName, version.getVersionName());
            Map<String, ModuleFormulaInst> instMap = euModule.getComponent().getFormulas();
            String[] formulaInstIdArr = StringUtility.split(formulaInstId, ";");
            for (String id : formulaInstIdArr) {
                instMap.remove(id);
            }
            ESDFacrory.getESDClient().saveModule(euModule);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
