package com.ds.esd.formula.service;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.component.RolePopTree;
import com.ds.esd.formula.component.RolesPopTree;
import com.ds.esd.formula.component.grid.RoleGridView;
import com.ds.esd.project.config.ProjectConfig;
import com.ds.esd.project.config.formula.FormulaInst;
import com.ds.esd.project.config.formula.FormulaInstParams;
import com.ds.org.RoleType;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@RequestMapping(path = "/admin/formula/component/orgrole/")
@MethodChinaName(cname = "部门角色", imageClass = "bpmfont bpm-gongzuoliu-moxing")
public class OrgRoleFService {


    @CustomAnnotation(pid = true, hidden = true)
    String formulaInstId;

    @CustomAnnotation(pid = true, hidden = true)
    String parameterCode;


    @GridViewAnnotation(addPath = "RolesPopTree")
    @RequestMapping("Indexs")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.indexs)
    @ResponseBody
    public ListResultModel<List<RoleGridView>> getRolesGrid(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<RoleGridView>> model = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst =formulas.get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, RoleGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return model;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true,caption = "添加部门角色")
    @RequestMapping("RolesPopTree")
    @DialogAnnotation( width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<RolesPopTree>> getRolesPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<RolesPopTree>> model = new TreeListResultModel<>();
        List<RolesPopTree> popTrees = new ArrayList<>();
        popTrees.add(new RolesPopTree(esdsearchpattern, RoleType.OrgRole));
        model.setData(popTrees);
        return model;
    }


    @GridViewAnnotation(addPath = "RolePopTree")
    @RequestMapping("Index")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.index)
    @ResponseBody
    public ListResultModel<List<RoleGridView>> getRoleGrid(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<RoleGridView>> model = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst =formulas.get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, RoleGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return model;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "添加部门角色")
    @RequestMapping("RolePopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<RolePopTree>> getRolePopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<RolePopTree>> model = new TreeListResultModel<>();
        List<RolePopTree> popTrees = new ArrayList<>();
        popTrees.add(new RolePopTree(esdsearchpattern, RoleType.OrgRole));
        model.setData(popTrees);
        return model;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }


}
