package com.ds.esd.formula.service;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.component.ModulePopTree;
import com.ds.esd.formula.component.ModulesPopTree;
import com.ds.esd.formula.component.grid.ComponentGridView;
import com.ds.esd.formula.component.grid.ModuleGridView;
import com.ds.esd.project.config.ProjectConfig;
import com.ds.esd.project.config.formula.FormulaInst;
import com.ds.esd.project.config.formula.FormulaInstParams;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@RequestMapping(path = "/admin/formula/component/module/")
@MethodChinaName(cname = "页面", imageClass = "spafont spa-icon-page")
public class ModuleFService {

    @GridViewAnnotation(addPath = "ModulesPopTree")
    @RequestMapping("Indexs")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.indexs)
    @ResponseBody
    public ListResultModel<List<ModuleGridView>> getIndexs(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<ModuleGridView>> model = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst =formulas.get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, ModuleGridView.class);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }

        return model;
    }


    @PopTreeViewAnnotation()
    @ModuleAnnotation(dynLoad = true, caption = "添加模块")
    @RequestMapping("ModulesPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<ModulesPopTree>> getComponentsPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<ModulesPopTree>> model = new TreeListResultModel<>();
        List<ModulesPopTree> popTrees = new ArrayList<>();
        try {
            popTrees.add(new ModulesPopTree(esdsearchpattern));
            model.setData(popTrees);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }
        return model;
    }

    @GridViewAnnotation(addPath = "ModulePopTree")
    @RequestMapping("Index")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<ModuleGridView>> getIndex(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<ModuleGridView>> model = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst =formulas.get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, ModuleGridView.class);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }

        return model;
    }


    @PopTreeViewAnnotation()
    @ModuleAnnotation(dynLoad = true,  caption = "添加模块")
    @RequestMapping("ModulePopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<ModulePopTree>> getComponentPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<ModulePopTree>> model = new TreeListResultModel<>();
        List<ModulePopTree> popTrees = new ArrayList<>();
        try {
            popTrees.add(new ModulePopTree(esdsearchpattern));
            model.setData(popTrees);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }
        return model;
    }

    @RequestMapping("addModule")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addModule(String projectName, String ModulePopTree, String ModulesPopTree, String formulaInstId, String parameterCode, FormulaParams paramsType) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst =formulas.get(formulaInstId);
            ParticipantSelect select = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulaById(inst.getParticipantSelectId());
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            if (params == null) {
                params = new FormulaInstParams();
                params.setFormulaInstId(inst.getFormulaInstId());
                params.setParameterCode(parameterCode);
                params.setParameterName(parameterCode);
                if (paramsType != null && !paramsType.equals("")) {
                    params.setParamsType(paramsType);
                }
                inst.getParamsMap().put(parameterCode, params);
            }

            if (ModulesPopTree != null && ModulesPopTree.equals("")) {
                String[] paths = StringUtility.split(ModulesPopTree, ";");

                for (String path : paths) {
                    params.getParameterValues().put(path, path);
                }
            } else if (ModulePopTree != null && ModulePopTree.equals("")) {
                params.getParameterValues().put(ModulePopTree, ModulePopTree);
            }


            ESDFacrory.getESDClient().updateFormulaConfig(project.getId(),inst);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delModule")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delModule(String formulaInstId, String parameterCode, String projectName, String id) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst =formulas.get(formulaInstId);
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            String[] componentIds = StringUtility.split(id, ";");

            for (String path : componentIds) {
                params.getParameterValues().remove(path);
            }
            ESDFacrory.getESDClient().updateFormulaConfig(project.getId(),inst);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }

    private FormulaInst getFormula(String projectId, String formulaInstId) throws JDSException {
        Project project = ESDFacrory.getESDClient().getProjectById(projectId);
        Map<String, FormulaInst> formulas = project.getFormulas();
        FormulaInst inst =formulas.get(formulaInstId);
        return inst;
    }

}


