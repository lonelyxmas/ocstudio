package com.ds.esd.formula.service;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.attribute.Attributetype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;

import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.component.ComponentPopTree;
import com.ds.esd.formula.component.ComponentsPopTree;
import com.ds.esd.formula.component.grid.ApiGridView;
import com.ds.esd.formula.component.grid.ComponentGridView;
import com.ds.esd.project.config.ProjectConfig;
import com.ds.esd.project.config.formula.FormulaInst;
import com.ds.esd.project.config.formula.FormulaInstParams;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RequestMapping(path = "/admin/formula/component/component/")
@MethodChinaName(cname = "通用组件", imageClass ="spafont spa-icon-module")

public class ComponentFService {
    @GridViewAnnotation(addPath = "ComponentPopTree")
    @RequestMapping("Index")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.index)
    @ResponseBody
    public ListResultModel<List<ComponentGridView>> getIndex(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<ComponentGridView>> model = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            FormulaInst inst =project.getFormulas().get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, ComponentGridView.class);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }

        return model;
    }


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true,  caption = "添加组件")
    @RequestMapping("ComponentPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<ComponentPopTree>> getComponentPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<ComponentPopTree>> model = new TreeListResultModel<>();
        List<ComponentPopTree> popTrees = new ArrayList<>();
        try {
            popTrees.add(new ComponentPopTree(esdsearchpattern, ComponentType.getFormComponents()));
            model.setData(popTrees);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }
        return model;
    }


    @GridViewAnnotation(addPath = "ComponentsPopTree")
    @RequestMapping("Indexs")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.indexs)
    @ResponseBody
    public ListResultModel<List<ComponentGridView>> getIndexs(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<ComponentGridView>> model = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            FormulaInst inst =project.getFormulas().get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, ComponentGridView.class);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }

        return model;
    }


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true,  caption = "添加组件")
    @RequestMapping("ComponentsPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<ComponentsPopTree>> getComponentsPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<ComponentsPopTree>> model = new TreeListResultModel<>();
        List<ComponentsPopTree> popTrees = new ArrayList<>();
        try {
            popTrees.add(new ComponentsPopTree(esdsearchpattern, ComponentType.getFormComponents()));
            model.setData(popTrees);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }
        return model;
    }


    @RequestMapping("addComponent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addComponent(String projectName, String ComponentPopTree, String ComponentsPopTree, String formulaInstId, String parameterCode, FormulaParams paramsType) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            FormulaInst inst =project.getFormulas().get(formulaInstId);
            ParticipantSelect select = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulaById(inst.getParticipantSelectId());
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            if (params == null) {
                params = new FormulaInstParams();
                params.setFormulaInstId(inst.getFormulaInstId());
                params.setParameterCode(parameterCode);
                params.setParameterName(parameterCode);
                if (paramsType != null && !paramsType.equals("")) {
                    params.setParamsType(paramsType);
                }
                inst.getParamsMap().put(parameterCode, params);
            }


            if (ComponentsPopTree != null && !ComponentsPopTree.equals("")) {
                String[] paths = StringUtility.split(ComponentsPopTree, ";");
                for (String path : paths) {
                    params.getParameterValues().put(path, path);
                }
            } else if (ComponentPopTree != null) {
                params.getParameterValues().clear();
                params.getParameterValues().put(ComponentPopTree, ComponentPopTree);
            }


            ESDFacrory.getESDClient().updateFormulaConfig(project.getId(),inst);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delComponent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delComponent(String formulaInstId, String parameterCode, String projectName, String id) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            FormulaInst inst =project.getFormulas().get(formulaInstId);
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            String[] componentIds = StringUtility.split(id, ";");

            for (String path : componentIds) {
                params.getParameterValues().remove(path);
            }
            ESDFacrory.getESDClient().updateFormulaConfig(project.getId(),inst);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }

    private FormulaInst getFormula(String projectId, String formulaInstId) throws JDSException {
        Project project = ESDFacrory.getESDClient().getProjectById(projectId);
        FormulaInst inst =project.getFormulas().get(formulaInstId);
        return inst;
    }


}
