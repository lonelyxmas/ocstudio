package com.ds.esd.formula.service;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;

import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.component.FieldPopTree;
import com.ds.esd.formula.component.FieldsPopTree;
import com.ds.esd.formula.component.grid.FieldGridView;
import com.ds.esd.project.config.ProjectConfig;
import com.ds.esd.project.config.formula.FormulaInst;
import com.ds.esd.project.config.formula.FormulaInstParams;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@RequestMapping(path = "/admin/formula/component/field/")
@MethodChinaName(cname = "字段", imageClass = "bpmfont bpmgongzuoliuzuhuzicaidan")

public class FieldFService {
    @GridViewAnnotation(addPath = "FieldsPopTree")
    @RequestMapping("Indexs")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.indexs)
    @ResponseBody
    public ListResultModel<List<FieldGridView>> getFieldsGrid(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<FieldGridView>> model = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst =formulas.get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, FieldGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }

        return model;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "添加数据库字段")
    @RequestMapping("FieldsPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<FieldsPopTree>> getFieldsPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<FieldsPopTree>> model = new TreeListResultModel<>();
        List<FieldsPopTree> popTrees = new ArrayList<>();
        try {
            popTrees.add(new FieldsPopTree(esdsearchpattern));
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }
        model.setData(popTrees);
        return model;
    }

    @GridViewAnnotation(addPath = "FieldPopTree")
    @RequestMapping("Index")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.indexs)
    @ResponseBody
    public ListResultModel<List<FieldGridView>> getFieldGrid(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<FieldGridView>> model = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst =formulas.get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, FieldGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }

        return model;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "添加数据库字段")
    @RequestMapping("FieldPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<FieldPopTree>> getFieldPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<FieldPopTree>> model = new TreeListResultModel<>();
        List<FieldPopTree> popTrees = new ArrayList<>();
        try {
            popTrees.add(new FieldPopTree(esdsearchpattern));
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }
        model.setData(popTrees);
        return model;
    }

    @RequestMapping("addField")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addField(String projectName, String FieldPopTree, String FieldsPopTree, String formulaInstId, String parameterCode, FormulaParams paramsType) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst =formulas.get(formulaInstId);
            ParticipantSelect select = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulaById(inst.getParticipantSelectId());
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            if (params == null) {
                params = new FormulaInstParams();
                params.setFormulaInstId(inst.getFormulaInstId());
                params.setParameterCode(parameterCode);
                params.setParameterName(parameterCode);
                if (paramsType != null && !paramsType.equals("")) {
                    params.setParamsType(paramsType);
                }
                inst.getParamsMap().put(parameterCode, params);
            }


            if (FieldsPopTree != null && !FieldsPopTree.equals("")) {
                String[] fieldIds = StringUtility.split(FieldsPopTree, ";");
                for (String fieldId : fieldIds) {
                    params.getParameterValues().put(fieldId, fieldId);
                }
            } else if (FieldPopTree != null && !FieldPopTree.equals("")) {
                params.getParameterValues().put(FieldPopTree, FieldPopTree);
            }


            ESDFacrory.getESDClient().updateFormulaConfig(project.getId(),inst);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delField")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delField(String formulaInstId, String parameterCode, String projectName, String id) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst =formulas.get(formulaInstId);
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            String[] roleIds = StringUtility.split(id, ";");

            for (String roleId : roleIds) {
                params.getParameterValues().remove(roleId);
            }
            ESDFacrory.getESDClient().updateFormulaConfig(project.getId(),inst);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }

    private FormulaInst getFormula(String projectId, String formulaInstId) throws JDSException {
        Project project = ESDFacrory.getESDClient().getProjectById(projectId);
        Map<String, FormulaInst> formulas = project.getFormulas();
        FormulaInst inst =formulas.get(formulaInstId);
        return inst;
    }


}
