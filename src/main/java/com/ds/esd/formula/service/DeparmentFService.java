package com.ds.esd.formula.service;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.component.DeparmentPopTree;
import com.ds.esd.formula.component.DeparmentsPopTree;
import com.ds.esd.formula.component.grid.DeparmentGridView;
import com.ds.esd.project.config.formula.FormulaInst;
import com.ds.esd.project.config.formula.FormulaInstParams;
import com.ds.org.Org;
import com.ds.org.OrgNotFoundException;
import com.ds.server.OrgManagerFactory;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@RequestMapping(path = "/admin/formula/component/deparment/")
@MethodChinaName(cname = "部门", imageClass = "bpmfont bpmgongzuoliuzuhuzicaidan")

public class DeparmentFService {
    @GridViewAnnotation(addPath = "DeparmentsPopTree")
    @RequestMapping("Indexs")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.indexs)
    @ResponseBody
    public ListResultModel<List<DeparmentGridView>> getDeparmentsGrid(String projectId, String formulaInstId, String parameterCode) {
        ListResultModel<List<DeparmentGridView>> model = new ListResultModel<>();
        try {
            Map<String, FormulaInst> formulas = ESDFacrory.getESDClient().getProjectById(projectId).getFormulas();
            FormulaInst inst = formulas.get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, DeparmentGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }

        return model;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "添加部门")
    @RequestMapping("DeparmentsPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<DeparmentsPopTree>> getDeparmentsPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<DeparmentsPopTree>> model = new TreeListResultModel<>();
        List<DeparmentsPopTree> popTrees = new ArrayList<>();
        popTrees.add(new DeparmentsPopTree(esdsearchpattern));
        model.setData(popTrees);
        return model;
    }

    @GridViewAnnotation(addPath = "DeparmentPopTree")
    @RequestMapping("Index")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.index)
    @ResponseBody
    public ListResultModel<List<DeparmentGridView>> getDeparmentGrid(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<DeparmentGridView>> model = new ListResultModel<>();
        try {
            Map<String, FormulaInst> formulas = ESDFacrory.getESDClient().getProjectByName(projectName).getFormulas();
            FormulaInst inst = formulas.get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, DeparmentGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }

        return model;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "添加部门")
    @RequestMapping("DeparmentPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<DeparmentPopTree>> getDeparmentPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<DeparmentPopTree>> model = new TreeListResultModel<>();
        List<DeparmentPopTree> popTrees = new ArrayList<>();

        popTrees.add(new DeparmentsPopTree(esdsearchpattern));

        model.setData(popTrees);
        return model;
    }

    @RequestMapping("addDeparment")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addDeparment(String projectName, String DeparmentPopTree, String DeparmentsPopTree, String formulaInstId, String parameterCode, FormulaParams paramsType) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst = formulas.get(formulaInstId);

            ParticipantSelect select = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulaById(inst.getParticipantSelectId());
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            if (params == null) {
                params = new FormulaInstParams();
                params.setFormulaInstId(inst.getFormulaInstId());
                params.setParameterCode(parameterCode);
                params.setParameterName(parameterCode);
                if (paramsType != null && !paramsType.equals("")) {
                    params.setParamsType(paramsType);
                }
                inst.getParamsMap().put(parameterCode, params);
            }

            if (DeparmentsPopTree != null && !DeparmentsPopTree.equals("")) {
                String[] orgIds = StringUtility.split(DeparmentsPopTree, ";");

                for (String orgId : orgIds) {
                    try {
                        Org org = OrgManagerFactory.getOrgManager().getOrgByID(orgId);
                        params.getParameterValues().put(orgId, org.getName());
                    } catch (OrgNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } else if (DeparmentPopTree != null) {
                params.getParameterValues().clear();
                try {
                    Org org = OrgManagerFactory.getOrgManager().getOrgByID(DeparmentPopTree);
                    params.getParameterValues().put(DeparmentPopTree, org.getName());
                } catch (OrgNotFoundException e) {
                    e.printStackTrace();
                }

            }
            ESDFacrory.getESDClient().updateFormulaConfig(project.getId(), inst);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }
        return model;
    }


    @RequestMapping("delDeparment")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delDeparment(String formulaInstId, String parameterCode, String projectName, String id) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> formulas = project.getFormulas();
            FormulaInst inst = formulas.get(formulaInstId);
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            String[] roleIds = StringUtility.split(id, ";");

            for (String roleId : roleIds) {
                params.getParameterValues().remove(roleId);
            }
            ESDFacrory.getESDClient().updateFormulaConfig(project.getId(), inst);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }

    private FormulaInst getFormula(String projectId, String formulaInstId) throws JDSException {
        Map<String, FormulaInst> formulas = ESDFacrory.getESDClient().getProjectById(projectId).getFormulas();
        FormulaInst inst = formulas.get(formulaInstId);
        return inst;
    }

}
