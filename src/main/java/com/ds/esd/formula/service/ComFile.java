package com.ds.esd.formula.service;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.editor.enums.PackagePathType;
import com.ds.esd.plugins.api.APIFactory;
import com.ds.esd.plugins.api.node.APIPaths;
import com.ds.esd.tool.enums.FileImgCssType;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.vfs.FileInfo;
import com.ds.vfs.FileVersion;
import com.ds.vfs.Folder;
import com.ds.web.APIConfig;

import java.util.ArrayList;
import java.util.List;

;

public class ComFile implements Comparable<ComFile> {

    String name;
    String id;
    Integer type;
    String location;
    String className;
    String packageName;
    String imageClass;
    String projectName;
    String caption;
    String path;
    List<ComFile> sub;
    public Boolean iniFold;


    public ComFile(ProjectVersion version) {
        this.projectName = version.getProject().getProjectName();
        this.name = projectName;
        this.location = version.getPath();
        this.id = location;
        this.imageClass = "fa fa-cubes";
        this.type = 0;
        this.caption = name;
        iniFold = true;
        path = "/";
    }

    public ComFile(EUPackage euPackage) {
        this.name = euPackage.getName();
        this.iniFold = true;
        this.location = euPackage.getPath();
        String curProjectPath = euPackage.getProjectVersion().getPath();
        if (curProjectPath != null && !curProjectPath.equals("") && location.startsWith(curProjectPath)) {
            location = location.substring(curProjectPath.length());
        }
        this.className = euPackage.getPackageName();
        packageName = euPackage.getPackageName();
        this.id = euPackage.getId();
        this.imageClass = euPackage.getImageClass();
        this.type = 1;
        this.caption = euPackage.getDesc();
        this.projectName = euPackage.getProjectVersion().getProject().getProjectName();
    }

    public ComFile(EUModule module) {
        this.name = module.getName() + ".cls";
        this.iniFold = true;
        this.location = module.getPath();
        String curProjectPath = module.getProjectVersion().getPath();
        if (curProjectPath != null && !curProjectPath.equals("") && location.startsWith(curProjectPath)) {
            location = location.substring(curProjectPath.length());
        }
        this.className = module.getClassName();
        this.id = module.getPath();
        this.imageClass = "spafont spa-icon-page";
        this.type = 1;
        this.caption = className;
        String title = module.getComponent().getTitle();
        if (title != null && !title.equals("")) {
            this.caption = caption + "(" + title + ")";
        }
        this.projectName = module.getProjectVersion().getProjectName();
    }

    public ComFile(FileVersion fileVersion, ProjectVersion version) {
        this.name = fileVersion.getFileName();
        this.location = fileVersion.getPath();
        if (version != null) {
            String curProjectPath = version.getPath();
            if (curProjectPath != null && !curProjectPath.equals("") && location.startsWith(curProjectPath)) {
                location = location.substring(curProjectPath.length());
            }
            this.projectName = version.getVersionName();
        }

        this.imageClass = imageClass;
        this.id = location;
        this.type = 1;
        this.caption = "#" + fileVersion.getIndex() + this.name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ComFile(Folder folder, ProjectVersion version) {
        iniFold = true;
        this.name = folder.getDescrition() == null ? folder.getName() : folder.getDescrition();
        String subpath = StringUtility.replace(folder.getPath(), version.getPath(), "/");
        try {
            APIPaths apiPaths = APIFactory.getInstance().getAPIPaths(subpath);
            if (apiPaths != null) {
                for (APIConfig config : apiPaths.getApiConfigs()) {
                    if (config.getChinaName() != null) {
                        this.name = config.getDesc() + "(" + config.getPackageName() + ")";
                        this.imageClass = apiPaths.getImageClass();
                    }
                }
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        this.location = folder.getPath();
        this.id = location;
        this.className = StringUtility.replace(location, version.getPath(), "");
        this.className = StringUtility.replace(this.className, "/", ".");
        if (className.endsWith(".")) {
            className = className.substring(0, className.length() - 1);
        }
        if (location.endsWith(".")) {
            className = className.substring(1, className.length());
        }
        path = location;
        PackagePathType packagePathType = PackagePathType.equalsPath(subpath);
        if (packagePathType != null) {
            this.name = packagePathType.getDesc() + "(" + className + ")";
            this.imageClass = packagePathType.getImageClass();
            this.setIniFold(true);
            if (packagePathType.equals(PackagePathType.App)) {
                List<Folder> folders = folder.getChildrenList();
                List<FileInfo> infos = folder.getFileList();
                this.setIniFold(false);
                this.sub = new ArrayList<>();
                for (Folder cfolder : folders) {
                    this.sub.add(new ComFile(cfolder, version));

                }
                for (FileInfo fileInfo : infos) {
                    if (!fileInfo.getName().endsWith(".cls")) {
                        this.sub.add(new ComFile(fileInfo, version));
                    } else {
                        EUModule module = version.getModule(fileInfo.getPath());
                        if (module != null) {
                            this.sub.add(new ComFile(module));
                        }
                    }
                }

            }
        } else {
            if (!this.name.endsWith("(" + className + ")")) {
                this.name = this.name + "(" + className + ")";
            }

        }
        this.type = 0;
        this.caption = this.name;
        //模板文件没有版本
        if (version != null) {
            String curProjectPath = version.getPath();
            if (curProjectPath != null && !curProjectPath.equals("") && location.startsWith(curProjectPath)) {
                location = location.substring(curProjectPath.length());
            }
            this.projectName = version.getVersionName();
        }

    }


    public ComFile(FileInfo fileInfo, ProjectVersion version) {
        this.name = fileInfo.getDescrition() == null ? fileInfo.getName() : fileInfo.getDescrition();
        this.location = fileInfo.getPath();
        int index = name.lastIndexOf(".");
        String mimeType = null;
        if (index > 0) {
            String fileType = name.substring(index + 1).toLowerCase();
            this.imageClass = FileImgCssType.fromType(fileType).getImageClass();
        } else {
            this.imageClass = FileImgCssType.unkown.getImageClass();
        }

        this.id = location;
        this.type = 1;
        this.caption = this.name;

        if (version != null) {
            String curProjectPath = version.getPath();
            if (curProjectPath != null && !curProjectPath.equals("") && location.startsWith(curProjectPath)) {
                location = location.substring(curProjectPath.length());
            }
            this.projectName = version.getVersionName();
        }

    }

    public Boolean getIniFold() {
        return iniFold;
    }

    public void setIniFold(Boolean iniFold) {
        this.iniFold = iniFold;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public List<ComFile> getSub() {
        return sub;
    }

    public void setSub(List<ComFile> sub) {
        this.sub = sub;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    @Override
    public int compareTo(ComFile o) {
        if (className != null && o.getClassName() != null) {
            return className.compareTo(o.getClassName());
        } else if (caption != null && o.getCaption() != null) {
            return caption.compareTo(o.getCaption());
        } else if (location != null && o.getLocation() != null) {
            return location.compareTo(o.getLocation());
        }

        return id.compareTo(o.getId());
    }
}