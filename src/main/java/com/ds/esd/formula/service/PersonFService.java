package com.ds.esd.formula.service;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.component.PersonPopTree;
import com.ds.esd.formula.component.PersonsPopTree;
import com.ds.esd.formula.component.grid.PersonGridView;
import com.ds.esd.formula.component.grid.RoleGridView;
import com.ds.esd.project.config.ProjectConfig;
import com.ds.esd.project.config.formula.FormulaInst;
import com.ds.esd.project.config.formula.FormulaInstParams;
import com.ds.org.Person;
import com.ds.org.PersonNotFoundException;
import com.ds.server.OrgManagerFactory;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RequestMapping(path = "/admin/formula/component/person/")
@MethodChinaName(cname = "人员", imageClass = "spafont spa-icon-login")
public class PersonFService {


    @GridViewAnnotation(addPath = "PersonsPopTree")
    @RequestMapping("Indexs")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.indexs)
    @ResponseBody
    public ListResultModel<List<PersonGridView>> getPersonsGrid(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<PersonGridView>> model = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            FormulaInst inst =project.getFormulas().get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, PersonGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return model;
    }

    @PopTreeViewAnnotation()
    @ModuleAnnotation(dynLoad = true,  caption = "添加人员")
    @RequestMapping("PersonsPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<PersonsPopTree>> getPersonsPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<PersonsPopTree>> model = new TreeListResultModel<>();
        List<PersonsPopTree> popTrees = new ArrayList<>();
        popTrees.add(new PersonsPopTree(esdsearchpattern));

        model.setData(popTrees);
        return model;
    }

    @GridViewAnnotation(addPath = "PersonPopTree")
    @RequestMapping("Index")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.index)
    @ResponseBody
    public ListResultModel<List<PersonGridView>> getPersonGrid(String projectName, String formulaInstId, String parameterCode) {
        ListResultModel<List<PersonGridView>> model = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            FormulaInst inst =project.getFormulas().get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, PersonGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return model;
    }

    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "添加人员")
    @RequestMapping("PersonPopTree")
    @DialogAnnotation( width = "300", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<PersonPopTree>> getPersonPopTree(String formulaInstId, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<PersonPopTree>> model = new TreeListResultModel<>();
        List<PersonPopTree> popTrees = new ArrayList<>();
        popTrees.add(new PersonPopTree(esdsearchpattern));

        model.setData(popTrees);
        return model;
    }

    @RequestMapping("addPerson")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addPerson(String projectName, String PersonPopTree, String formulaInstId, String parameterCode, FormulaParams paramsType) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            FormulaInst inst =project.getFormulas().get(formulaInstId);
            ParticipantSelect select = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulaById(inst.getParticipantSelectId());
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            if (params == null) {
                params = new FormulaInstParams();
                params.setFormulaInstId(inst.getFormulaInstId());
                params.setParameterCode(parameterCode);
                params.setParameterName(parameterCode);
                if (paramsType != null && !paramsType.equals("")) {
                    params.setParamsType(paramsType);
                }
                inst.getParamsMap().put(parameterCode, params);
            }
            String[] personIds = StringUtility.split(PersonPopTree, ";");

            for (String personId : personIds) {
                try {
                    Person person = OrgManagerFactory.getOrgManager().getPersonByID(personId);
                    params.getParameterValues().put(person.getID(), person.getName());
                } catch (PersonNotFoundException e) {
                    e.printStackTrace();
                }
            }
            ESDFacrory.getESDClient().updateFormulaConfig(project.getId(),inst);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delPerson")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delPerson(String formulaInstId, String parameterCode, String projectName, String id) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(projectName);
            FormulaInst inst =project.getFormulas().get(formulaInstId);
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            String[] roleIds = StringUtility.split(id, ";");

            for (String roleId : roleIds) {
                params.getParameterValues().remove(roleId);
            }
            ESDFacrory.getESDClient().updateFormulaConfig(project.getId(),inst);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }

    private FormulaInst getFormula(String projectId, String formulaInstId) throws JDSException {
        Project project = ESDFacrory.getESDClient().getProjectById(projectId);
        FormulaInst inst =project.getFormulas().get(formulaInstId);
        return inst;
    }


}
