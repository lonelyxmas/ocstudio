package com.ds.esd.formula.service.esd;

import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.component.ActionPopTree;
import com.ds.esd.formula.component.ActionsPopTree;
import com.ds.esd.formula.component.grid.ModuleActionGridView;
import com.ds.esd.project.config.formula.FormulaInstParams;
import com.ds.esd.project.config.formula.ModuleFormulaInst;
import com.ds.esd.tool.module.EUModule;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@RequestMapping(path = "/admin/formula/component/module/action/")
@MethodChinaName(cname = "授权按钮", imageClass ="spafont spa-icon-c-imagebutton")

public class ModuleActionFService {


    @GridViewAnnotation(addPath = "ItemsPopTree")
    @RequestMapping("Indexs")
    @ModuleAnnotation()
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.indexs)
    @ResponseBody
    public ListResultModel<List<ModuleActionGridView>> getIndexs(String projectName, String className, String formulaInstId, String parameterCode) {
        ListResultModel<List<ModuleActionGridView>> model = new ListResultModel<>();
        try {

            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(className, version.getVersionName());
            Map<String, ModuleFormulaInst> rightMap = euModule.getComponent().getFormulas();
            ModuleFormulaInst inst = rightMap.get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, ModuleActionGridView.class);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }

        return model;
    }


    @PopTreeViewAnnotation(saveUrl = "admin.formula.component.module.action.addComponent")
    @RequestMapping("ItemsPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @ModuleAnnotation(dynLoad = true, caption = "授权动作")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<ActionsPopTree>> getItemsPopTree(String projectName, String formulaInstId, String className, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<ActionsPopTree>> model = new TreeListResultModel<>();
        List<ActionsPopTree> popTrees = new ArrayList<>();
        try {

            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(className, version.getVersionName());
            popTrees.add(new ActionsPopTree(esdsearchpattern, euModule));
            model.setData(popTrees);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }
        return model;
    }


    @GridViewAnnotation(addPath = "ItemPopTree")
    @RequestMapping("Index")
    @ModuleAnnotation
    @APIEventAnnotation(autoRun = true,bindMenu = CustomMenuItem.index)
    @ResponseBody
    public ListResultModel<List<ModuleActionGridView>> getIndex(String projectName, String className, String formulaInstId, String parameterCode) {
        ListResultModel<List<ModuleActionGridView>> model = new ListResultModel<>();
        try {

            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(className, version.getVersionName());
            Map<String, ModuleFormulaInst> rightMap = euModule.getComponent().getFormulas();
            ModuleFormulaInst inst = rightMap.get(formulaInstId);
            List<FormulaInstParams> formulaParamList = new ArrayList<FormulaInstParams>();
            Collection<FormulaInstParams> formulaParams = inst.getParamsMap().values();
            for (FormulaInstParams param : formulaParams) {
                if (param.getParameterCode().equals(parameterCode)) {
                    param.getParameterValues().forEach((key, value) -> {
                        FormulaInstParams paramInst = new FormulaInstParams(param, key, value);
                        formulaParamList.add(paramInst);
                    });
                }
            }
            model = PageUtil.getDefaultPageList(formulaParamList, ModuleActionGridView.class);
        } catch (JDSException e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
        }

        return model;
    }


    @PopTreeViewAnnotation( saveUrl = "admin.formula.component.module.action.addComponent")
    @RequestMapping("ItemPopTree")
    @DialogAnnotation(width = "300", height = "450")
    @ModuleAnnotation(dynLoad = true, caption = "授权按钮")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @ResponseBody
    public TreeListResultModel<List<ActionPopTree>> getItemPopTree(String projectName, String formulaInstId, String className, String parameterCode, String esdsearchpattern) {
        TreeListResultModel<List<ActionPopTree>> model = new TreeListResultModel<>();
        List<ActionPopTree> popTrees = new ArrayList<>();

        try {

            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(className, version.getVersionName());
            popTrees.add(new ActionPopTree(esdsearchpattern, euModule));
            model.setData(popTrees);
        } catch (JDSException e) {
            e.printStackTrace();
            model = new ErrorListResultModel(e.getMessage());
        }
        return model;
    }


    @RequestMapping("addComponent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeSave, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> addComponent(String projectName, String className, String ItemPopTree, String ItemsPopTree, String formulaInstId, String parameterCode, FormulaParams paramsType) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {

            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(className, version.getVersionName());
            Map<String, ModuleFormulaInst> rightMap = euModule.getComponent().getFormulas();
            ModuleFormulaInst inst = rightMap.get(formulaInstId);
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            if (params == null) {
                params = new FormulaInstParams();
                params.setFormulaInstId(inst.getFormulaInstId());
                params.setParameterCode(parameterCode);
                params.setParameterName(parameterCode);
                if (paramsType != null && !paramsType.equals("")) {
                    params.setParamsType(paramsType);
                }
                inst.getParamsMap().put(parameterCode, params);
            }

            if (ItemsPopTree != null && !ItemsPopTree.equals("")) {
                String[] paths = StringUtility.split(ItemsPopTree, ";");
                for (String path : paths) {
                    params.getParameterValues().put(path, path);
                }
            } else if (ItemPopTree != null) {
                params.getParameterValues().clear();
                params.getParameterValues().put(ItemPopTree, ItemsPopTree);
            }

            ESDFacrory.getESDClient().saveModule(euModule);


        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


    @RequestMapping("delComponent")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delComponent(String formulaInstId, String className, String parameterCode, String projectName, String id) {
        ResultModel<Boolean> model = new ResultModel<>();
        try {

            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(className, version.getVersionName());
            Map<String, ModuleFormulaInst> rightMap = euModule.getComponent().getFormulas();
            ModuleFormulaInst inst = rightMap.get(formulaInstId);
            FormulaInstParams params = (FormulaInstParams) inst.getParamsMap().get(parameterCode);
            String[] componentIds = StringUtility.split(id, ";");
            for (String path : componentIds) {
                params.getParameterValues().remove(path);
            }
            ESDFacrory.getESDClient().saveModule(euModule);
        } catch (JDSException e) {
            model = new ErrorResultModel();
            ((ErrorResultModel<Boolean>) model).setErrdes(e.getMessage());
        }


        return model;
    }


}
