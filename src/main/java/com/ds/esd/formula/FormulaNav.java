package com.ds.esd.formula;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.TreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.manager.formula.item.FormulaTypeItem;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/admin/formula/")
@TabsAnnotation(closeBtn = true)
public class FormulaNav {

    @MethodChinaName(cname = "插件分类")
    @RequestMapping(method = RequestMethod.POST, value = "FormulaManager")
    @TreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, imageClass = "bpmfont bpmgongzuoliu")
    @ResponseBody
    public TreeListResultModel<List<FormulaTypeItem>> getFormulaManager(String id) {
        TreeListResultModel<List<FormulaTypeItem>> resultModel = new TreeListResultModel<List<FormulaTypeItem>>();
        List<FormulaTypeItem> items = new ArrayList<>();
        FormulaTypeItem root = new FormulaTypeItem();
        items.add(root);
        if (id != null && !id.equals("")) {
            String[] orgIdArr = StringUtility.split(id, ";");
            resultModel.setIds(Arrays.asList(orgIdArr));
        } else {
            resultModel.setIds(Arrays.asList(new String[]{root.getFristClassItem(root).getId()}));
        }
        resultModel.setData(items);
        return resultModel;

    }


    @JSONField(serialize = false)
    public ESDClient getEsdClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }
}
