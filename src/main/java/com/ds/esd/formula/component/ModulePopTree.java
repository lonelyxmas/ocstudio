package com.ds.esd.formula.component;


import com.ds.common.JDSException;
import com.ds.context.JDSActionContext;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.ModuleFService;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {ModuleFService.class})
@PopTreeAnnotation(caption = "添加模块")
public class ModulePopTree extends TreeListItem {

    @CustomAnnotation(pid = true)
    String pattern = "";
    @CustomAnnotation(pid = true)
    String projectId;

    @CustomAnnotation(pid = true)
    String formulaInstId;

    @CustomAnnotation(pid = true)
    String parameterCode;

    @CustomAnnotation(pid = true)
    String paramsType;

    public ModulePopTree(String pattern) throws JDSException {
        super("allComponent", "所有组件");
        this.setIniFold(false);
        Object projectId = JDSActionContext.getActionContext().getParams("projectId");
        if (projectId != null) {
            List<EUPackage> packages = ESDFacrory.getESDClient().getProjectById(projectId.toString()).getActiveProjectVersion().getAllPackage();
            for (EUPackage euPackage : packages) {
                this.addChild(new ModulesPopTree(pattern, euPackage));
            }
        }
    }

    private boolean pattern(String pattern, EUModule euModule) {
        if (pattern != null && !pattern.equals("")) {
            Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher namematcher = p.matcher(euModule.getName());
            Matcher cnnamematcher = p.matcher(euModule.getDesc());
            Matcher fieldmatcher = p.matcher(euModule.getClassName());
            if (namematcher.find() || cnnamematcher.find() || fieldmatcher.find()) {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }


    public ModulePopTree(String pattern, EUPackage euPackage) throws JDSException {
        super(euPackage.getName(), euPackage.getPackageName());
        for (EUModule euModule : euPackage.listModules()) {
            if (pattern(pattern, euModule)) {
                this.addChild(new ModulePopTree(euModule));
            }

        }
    }

    public ModulePopTree(EUModule euModule) throws JDSException {
        super(euModule.getClassName(), euModule.getDesc());
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParamsType() {
        return paramsType;
    }

    public void setParamsType(String paramsType) {
        this.paramsType = paramsType;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
