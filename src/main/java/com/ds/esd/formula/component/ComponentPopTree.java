package com.ds.esd.formula.component;


import com.ds.common.JDSException;
import com.ds.context.JDSActionContext;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.ComponentFService;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.ui.component.Component;
import com.ds.esd.tool.ui.component.Properties;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.SelModeType;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {ComponentFService.class})
@PopTreeAnnotation(caption = "添加组件")
public class ComponentPopTree extends TreeListItem {

    @CustomAnnotation(pid = true)
    String pattern = "";

    @CustomAnnotation(pid = true)
    String projectId;

    @CustomAnnotation(pid = true)
    String formulaInstId;

    @CustomAnnotation(pid = true)
    String parameterCode;

    @CustomAnnotation(pid = true)
    String paramsType;


    public ComponentPopTree(String pattern, ComponentType... componentTypes) throws JDSException {
        super("allComponent", "所有组件");
        this.setIniFold(false);
        this.iniFold = false;
        this.imageClass = "spafont spa-icon-conf";
        String projectName = (String) JDSActionContext.getActionContext().getParams("projectName");
        List<EUPackage> packages = ESDFacrory.getESDClient().getAllPackage(projectName);
        for (EUPackage euPackage : packages) {
            this.addChild(new ComponentPopTree(pattern, euPackage, componentTypes));
        }
    }


    public ComponentPopTree(String pattern, EUPackage euPackage, ComponentType... componentTypes) throws JDSException {
        super(euPackage.getPackageName(), euPackage.getPackageName());
        this.imageClass = "fa fa-cubes";
        this.iniFold = false;

        for (EUModule euModule : euPackage.listModules()) {
            this.addChild(new ComponentPopTree(pattern, euModule, componentTypes));
        }
    }

    public ComponentPopTree(String pattern, EUModule euModule, ComponentType... componentTypes) throws JDSException {
        super(euModule.getClassName(), euModule.getName());
        this.imageClass = "spafont spa-icon-page";
        List<Component> componentList = new ArrayList<>();
        if (euModule.getComponent().getChildren() != null) {
            componentList.addAll(euModule.getComponent().getChildren());
            for (Component childComponent : componentList) {
                if (hasChild(pattern, childComponent, componentTypes)) {
                    this.addChild(new ComponentPopTree(pattern, childComponent, componentTypes));
                }
            }
        }

    }

    public ComponentPopTree(String pattern, Component component, ComponentType... componentTypes) throws JDSException {
        super(component.getPath(), component.getAlias() + "(" + component.getProperties().getDesc() + ")");
        ComponentType componentType = ComponentType.fromType(component.getKey());
        this.imageClass = componentType.getImageClass();
        this.addTagVar("euClassName", component.getModuleComponent().getClassName());
        this.caption = component.getAlias() + "(" + componentType.name() + ")";
        List<Component> componentList = new ArrayList<>();
        if (component.getChildren() != null) {
            componentList.addAll(component.getChildren());
            for (Component childComponent : componentList) {

                if (hasChild(pattern, childComponent, componentTypes)) {
                    this.addChild(new ComponentPopTree(pattern, childComponent, componentTypes));
                }

            }
        }

    }


    private boolean checkChild(String pattern, Component component) {
        List<Component> components = component.getChildrenRecursivelyList();
        if (pattern(pattern, component)) {
            return true;
        }
        for (Component childComponent : components) {
            if (pattern(pattern, childComponent)) {
                return true;
            }
        }
        return false;
    }

    private boolean pattern(String pattern, Component component) {
        if (pattern != null && !pattern.equals("")) {
            Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Properties sourceProperties = component.getProperties();
            Matcher namematcher = p.matcher(sourceProperties.getName());
            Matcher descmatcher = p.matcher(sourceProperties.getDesc());
            if (namematcher.find() || descmatcher.find()) {
                return true;
            }
        } else {
            return true;
        }
        return false;

    }

    boolean hasChild(String pattern, Component component, ComponentType... componentTypes) {
        if ((componentTypes == null || componentTypes.length == 0) && pattern(pattern, component)) {
            return true;
        }
        Set<ComponentType> types = new LinkedHashSet<>();
        List<Component> componentList = new ArrayList<>();
        componentList.addAll(component.getChildrenRecursivelyList());


        for (ComponentType componentType : componentTypes) {
            if (componentType.equals(component.typeKey) && pattern(pattern, component)) {
                return true;
            }
        }

        for (Component childComponent : componentList) {
            types.add(childComponent.typeKey);
        }
        return checkIn(types, componentTypes);
    }


    boolean checkIn(Set<ComponentType> types, ComponentType... componentTypes) {
        if (componentTypes == null || componentTypes.length == 0) {
            return true;
        }
        for (ComponentType componentType : componentTypes) {
            if (componentType != null && types.contains(componentType)) {
                return true;
            }
        }
        return false;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParamsType() {
        return paramsType;
    }

    public void setParamsType(String paramsType) {
        this.paramsType = paramsType;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

}
