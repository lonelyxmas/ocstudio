package com.ds.esd.formula.component;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.plugins.api.enums.APIType;
import com.ds.esd.plugins.api.node.APIPaths;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.ApiFService;
import com.ds.esd.plugins.api.node.APIPaths;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

import java.util.HashMap;
import java.util.List;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, customService = {ApiFService.class}, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@PopTreeAnnotation(caption = "添加API")
public class ApiPopTree extends TreeListItem {
    @CustomAnnotation(pid = true)
    String pattern = "";

    @CustomAnnotation(pid = true)
    String projectId;

    @CustomAnnotation(pid = true)
    String formulaInstId;

    @CustomAnnotation(pid = true)
    String parameterCode;

    @CustomAnnotation(pid = true)
    String paramsType;


    public ApiPopTree(String pattern) throws JDSException {
        List<APIPaths> childernList = ESDFacrory.getESDClient().getAPITopPaths(pattern, APIType.userdef);
        for (APIPaths capis : childernList) {
            if (capis != null && capis.getChildren() != null && capis.getChildren().size() > 0) {
                this.addChild(new ApiPopTree(capis));
            }
        }
    }


    public ApiPopTree(APIPaths apiPath) {
        this.caption = apiPath.getName();
        this.setId(apiPath.getPath());
        this.setIniFold(false);
        this.tagVar = new HashMap<>();
        this.tagVar.put("path", apiPath.getPath());
        for (APIPaths capis : apiPath.getChildren()) {
            if (capis != null && capis.getChildren() != null && capis.getChildren().size() > 0) {
                this.addChild(new ApiPopTree(capis));
            }
        }
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParamsType() {
        return paramsType;
    }

    public void setParamsType(String paramsType) {
        this.paramsType = paramsType;
    }
}
