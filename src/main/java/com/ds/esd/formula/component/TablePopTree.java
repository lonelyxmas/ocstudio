package com.ds.esd.formula.component;


import com.ds.common.JDSException;
import com.ds.common.database.metadata.TableInfo;
import com.ds.context.JDSActionContext;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.TableFService;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {TableFService.class})
@PopTreeAnnotation(caption = "添加库表")
public class TablePopTree extends TreeListItem {

    @CustomAnnotation(pid = true)
    String pattern = "";


    @CustomAnnotation(pid = true)
    String projectId;

    @CustomAnnotation(pid = true)
    String formulaInstId;

    @CustomAnnotation(pid = true)
    String parameterCode;

    @CustomAnnotation(pid = true)
    String paramsType;


    public TablePopTree(String pattern) throws JDSException {
        super("allTable", "关联库表");
        this.setIniFold(false);
        projectId = JDSActionContext.getActionContext().getParams("projectId").toString();
        List<TableInfo> tableInfos = ESDFacrory.getESDClient().getProjectById(projectId).getTables();
        for (TableInfo tableInfo : tableInfos) {
            if (pattern(pattern, tableInfo)) {
                this.addChild(new TablePopTree(tableInfo));
            }

        }
    }


    public TablePopTree(TableInfo tableInfo) throws JDSException {
        super(tableInfo.getName(), tableInfo.getCnname() + "(" + tableInfo.getName() + ")");

    }

    private boolean pattern(String pattern, TableInfo tableInfo) {
        if (pattern != null && !pattern.equals("")) {
            Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher namematcher = p.matcher(tableInfo.getName());
            Matcher cnnamematcher = p.matcher(tableInfo.getCnname());
            if (namematcher.find() || cnnamematcher.find()) {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }


    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParamsType() {
        return paramsType;
    }

    public void setParamsType(String paramsType) {
        this.paramsType = paramsType;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
