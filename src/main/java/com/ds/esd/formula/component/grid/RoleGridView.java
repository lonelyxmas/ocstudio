package com.ds.esd.formula.component.grid;

import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.formula.service.RoleFService;
import com.ds.esd.project.config.formula.FormulaInstParams;
import com.ds.org.RoleType;
@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {RoleFService.class})
public class RoleGridView {


    @CustomAnnotation(pid = true, hidden = true)
    String formulaInstId;

    @CustomAnnotation(pid = true, hidden = true)
    RoleType roleType;

    @CustomAnnotation(uid = true, hidden = true)
    private String id;

    @CustomAnnotation(caption = "参数代码")
    private String parameterCode;

    @MethodChinaName(cname = "用户角色")
    private String parameterValue;

    @MethodChinaName(cname = "参数类型")
    FormulaParams paramsType;

    @CustomAnnotation(pid = true, hidden = true)
    Boolean single;

    public RoleGridView() {

    }

    public RoleGridView(FormulaInstParams params) {
        this.id = params.getId();
        this.parameterValue = params.getValue();
        this.paramsType = params.getParamsType();
        this.formulaInstId = params.getFormulaInstId();
        this.parameterCode = params.getParameterCode();
        this.single = params.getSingle();

    }

    public Boolean getSingle() {
        return single;
    }

    public void setSingle(Boolean single) {
        this.single = single;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public FormulaParams getParamsType() {
        return paramsType;
    }

    public void setParamsType(FormulaParams paramsType) {
        this.paramsType = paramsType;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
