package com.ds.esd.formula.component;


import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.ModuleFService;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.ui.enums.SelModeType;
@BottomBarMenu
@TreeAnnotation(heplBar = true,  selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save,CustomFormMenu.Close},  customService = {ModuleFService.class})
@PopTreeAnnotation( caption = "添加模块")
public class ModulesPopTree extends ModulePopTree {

    public ModulesPopTree(String pattern) throws JDSException {
        super(pattern);

    }

    public ModulesPopTree(String pattern, EUPackage euPackage) throws JDSException {
        super(euPackage.getName(), euPackage);

    }

    public ModulesPopTree(EUModule euModule) throws JDSException {
        super(euModule);
    }


}
