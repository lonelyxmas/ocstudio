package com.ds.esd.formula.component;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.plugins.api.node.APIPaths;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.ApiFService;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu
@TreeAnnotation(heplBar = true,  selMode = SelModeType.multibycheckbox, customService = {ApiFService.class}, bottombarMenu = {CustomFormMenu.Save,CustomFormMenu.Close})
@PopTreeAnnotation( caption = "添加API")
public class ApisPopTree extends ApiPopTree {
    public ApisPopTree(String pattern) throws JDSException {
        super(pattern);
    }

    public ApisPopTree(APIPaths apiPath) {
        super(apiPath);
    }

}
