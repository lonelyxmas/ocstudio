package com.ds.esd.formula.component;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.FolderFService;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.org.Org;
import com.ds.server.OrgManagerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {FolderFService.class})
@PopTreeAnnotation(caption = "选择文件夹")
public class FoldersPopTree extends TreeListItem {
    @CustomAnnotation(pid = true)
    String pattern = "";
    @CustomAnnotation(pid = true)
    String projectId;

    @CustomAnnotation(pid = true)
    String formulaInstId;

    @CustomAnnotation(pid = true)
    String parameterCode;

    @CustomAnnotation(pid = true)
    String paramsType;

    public FoldersPopTree(String pattern) {
        imageClass = "bpmfont bpm-gongzuoliu-moxing";
        this.caption = "跟目录";
        this.id = "allFolder";
        List<Org> orgs = OrgManagerFactory.getOrgManager().getTopOrgs();
        for (Org org : orgs) {
            if (checkChild(pattern, org)) {
                this.addChild(new FoldersPopTree(pattern, org));
            }
        }
    }

    public FoldersPopTree(String pattern, Org org) {
        this.caption = org.getName();
        this.setId(org.getOrgId());
        this.setIniFold(false);
        this.tagVar = new HashMap<>();
        this.imageClass = "spafont spa-icon-c-treeview";
        this.tagVar.put("orgId", org.getOrgId());
        if (org.getChildrenList() != null && org.getChildrenList().size() > 0) {
            for (Org childorg : org.getChildrenList()) {
                if (checkChild(pattern, childorg)) {
                    this.addChild(new FoldersPopTree(pattern, childorg));
                }

            }
        }

    }

    private boolean checkChild(String pattern, Org org) {
        if (pattern(pattern, org)) {
            return true;
        }
        List<Org> orgs = org.getChildrenRecursivelyList();
        for (Org childOrg : orgs) {
            if (pattern(pattern, childOrg)) {
                return true;
            }
        }
        return false;
    }

    private boolean pattern(String pattern, Org org) {

        if (pattern != null && !pattern.equals("")) {
            Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher namematcher = p.matcher(org.getName());
            if (namematcher.find()) {
                return true;
            }
        } else {
            return true;
        }

        return false;

    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParamsType() {
        return paramsType;
    }

    public void setParamsType(String paramsType) {
        this.paramsType = paramsType;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
