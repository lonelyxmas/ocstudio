package com.ds.esd.formula.component.grid;

import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.formula.service.ApiFService;
import com.ds.esd.project.config.formula.FormulaInstParams;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Add, GridMenu.Delete}, customService = {ApiFService.class})
public class ApiGridView {

    @CustomAnnotation(pid = true, hidden = true)
    String formulaInstId;


    @MethodChinaName(cname = "参数代码")
    private String parameterCode;

    @MethodChinaName(cname = "API")
    private String parameterValue;

    @MethodChinaName(cname = "参数名称")
    FormulaParams paramsType;


    @CustomAnnotation(pid = true, hidden = true)
    Boolean single;


    @CustomAnnotation(uid = true, hidden = true)
    private String id;


    public ApiGridView(FormulaInstParams params) {
        this.id = params.getId();
        this.parameterValue = params.getValue();
        this.paramsType = params.getParamsType();
        this.formulaInstId = params.getFormulaInstId();
        this.single = params.getSingle();
        this.parameterCode = params.getParameterCode();
    }

    public Boolean getSingle() {
        return single;
    }

    public void setSingle(Boolean single) {
        this.single = single;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FormulaParams getParamsType() {
        return paramsType;
    }

    public void setParamsType(FormulaParams paramsType) {
        this.paramsType = paramsType;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }
}
