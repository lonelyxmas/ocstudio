package com.ds.esd.formula.component;

import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.PersonFService;

import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.org.Org;
import com.ds.org.Person;
@BottomBarMenu
@TreeAnnotation(heplBar = true,  selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save,CustomFormMenu.Close},  customService = {PersonFService.class})
@PopTreeAnnotation( caption = "选择人员")
public class PersonsPopTree extends PersonPopTree {


    public PersonsPopTree(String pattern) {
        super(pattern);
    }


    public PersonsPopTree(String pattern, Org org) {
        super(pattern, org);
    }

    public PersonsPopTree(Person person) {
        super(person);
    }

}
