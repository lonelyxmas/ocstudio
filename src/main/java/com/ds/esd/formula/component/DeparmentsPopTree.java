package com.ds.esd.formula.component;

import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.DeparmentFService;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.org.Org;
@BottomBarMenu
@TreeAnnotation(heplBar = true,  selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Close, CustomFormMenu.Save},  customService = {DeparmentFService.class})
@PopTreeAnnotation( caption = "选择部门")
public class DeparmentsPopTree extends DeparmentPopTree {

    public DeparmentsPopTree(String pattern) {
        super(pattern);
    }

    public DeparmentsPopTree(String pattern, Org org) {
        super(pattern, org);

    }

}
