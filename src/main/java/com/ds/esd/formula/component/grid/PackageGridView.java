package com.ds.esd.formula.component.grid;


import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.formula.service.PackageFService;
import com.ds.esd.project.config.formula.FormulaInstParams;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {PackageFService.class})
public class PackageGridView {

    @CustomAnnotation(pid = true, hidden = true)
    String formulaInstId;

    @MethodChinaName(cname = "参数代码")
    private String parameterCode;

    @MethodChinaName(cname = "应用名称")
    private String parameterValue;

    @MethodChinaName(cname = "参数名称")
    FormulaParams paramsType;

    @CustomAnnotation(uid = true, hidden = true)
    private String id;

    @CustomAnnotation(pid = true, hidden = true)
    Boolean single;


    public PackageGridView(FormulaInstParams params) {
        this.parameterValue = params.getValue();
        this.paramsType = params.getParamsType();
        this.formulaInstId = params.getFormulaInstId();
        this.id = params.getId();
        this.parameterCode = params.getParameterCode();
        this.single = params.getSingle();
    }

    public Boolean getSingle() {
        return single;
    }

    public void setSingle(Boolean single) {
        this.single = single;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public FormulaParams getParamsType() {
        return paramsType;
    }

    public void setParamsType(FormulaParams paramsType) {
        this.paramsType = paramsType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
