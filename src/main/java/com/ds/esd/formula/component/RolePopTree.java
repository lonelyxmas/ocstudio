package com.ds.esd.formula.component;

import com.ds.common.util.CnToSpell;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.RoleFService;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.org.Role;
import com.ds.org.RoleOtherType;
import com.ds.org.RoleType;
import com.ds.server.OrgManagerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {RoleFService.class})
@PopTreeAnnotation(caption = "添加角色")
public class RolePopTree extends TreeListItem {
    @CustomAnnotation(pid = true)
    String pattern = "";
    @CustomAnnotation(pid = true)
    String projectId;


    @CustomAnnotation(pid = true)
    String formulaInstId;

    @CustomAnnotation(pid = true)
    String parameterCode;


    @CustomAnnotation(pid = true)
    String paramsType;


    public RolePopTree(String pattern) {
        this.caption = "所有角色";
        this.setId("allRoles");
        this.setIniFold(false);
        this.setImageClass("bpmfont bpmbinghangkaishi");
        sub = new ArrayList<>();
        for (RoleType roleType : RoleType.values()) {
            if (roleType.getOtherType().equals(RoleOtherType.Person)) {
                sub.add(new RolePopTree(pattern, roleType));
            }
        }
    }


    public RolePopTree(String pattern, RoleType roleType) {
        this.caption = roleType.getName();
        this.setId(roleType.getType());
        this.setImageClass(roleType.getImageClass());
        this.tagVar = new HashMap<>();
        this.tagVar.put("roleType", roleType.getType());

        this.setIniFold(false);
        List<Role> roles = OrgManagerFactory.getOrgManager().getAllRoles();
        if (roles.size() > 0) {
            sub = new ArrayList<>();
            for (Role role : roles) {
                if (role.getType().equals(roleType)) {
                    if (pattern(pattern, role)) {
                        sub.add(new RolePopTree(role));
                    }

                }
            }
        }


    }

    public RolePopTree(Role role) {
        this.caption = role.getName();
        this.setId(role.getRoleId());
        this.tagVar = new HashMap<>();
        this.tagVar.put("roleId", role.getRoleId());
        this.tagVar.put("roleType", role.getType().getType());
        this.setIniFold(false);

    }

    private boolean pattern(String pattern, Role role) {
        if (pattern != null && !pattern.equals("")) {
            Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher namematcher = p.matcher(role.getName());
            Matcher fieldmatcher = p.matcher(CnToSpell.getFullSpell(role.getName()));
            if (namematcher.find() || fieldmatcher.find()) {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }


    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getParamsType() {
        return paramsType;
    }

    public void setParamsType(String paramsType) {
        this.paramsType = paramsType;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
