package com.ds.esd.formula.component;

import com.ds.common.util.CnToSpell;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.PersonFService;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.org.Org;
import com.ds.org.Person;
import com.ds.server.OrgManagerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {PersonFService.class})
@PopTreeAnnotation(caption = "选择人员")
public class PersonPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String pattern = "";
    @CustomAnnotation(pid = true)
    String projectId;

    @CustomAnnotation(pid = true)
    String formulaInstId;

    @CustomAnnotation(pid = true)
    String parameterCode;

    @CustomAnnotation(pid = true)
    String paramsType;

    public PersonPopTree(String pattern) {
        this.imageClass = "spafont spa-icon-c-treeview";
        this.caption = "组织机构";
        this.id = "allOrg";
        List<Org> orgs = OrgManagerFactory.getOrgManager().getTopOrgs();
        for (Org org : orgs) {
            this.addChild(new PersonPopTree(pattern, org));
        }
    }

    private boolean pattern(String pattern, Person person) {
        if (pattern != null && !pattern.equals("")) {
            Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher namematcher = p.matcher(person.getName());
            Matcher fieldmatcher = p.matcher(CnToSpell.getFullSpell(person.getName()));
            if (namematcher.find() || fieldmatcher.find()) {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    public PersonPopTree(String pattern, Org org) {
        this.caption = org.getName();
        this.setId(org.getOrgId());
        imageClass = "bpmfont bpm-gongzuoliu-moxing";
        this.setIniFold(false);
        this.tagVar = new HashMap<>();
        this.tagVar.put("orgId", org.getOrgId());
        if (org.getChildrenList() != null && org.getChildrenList().size() > 0) {
            for (Org childorg : org.getChildrenList()) {
                this.addChild(new PersonPopTree(pattern, childorg));
            }
        }

        if (org.getPersonList() != null && org.getPersonList().size() > 0) {
            for (Person person : org.getPersonList()) {
                if (pattern(pattern, person)) {
                    this.addChild(new PersonPopTree(person));
                }

            }
        }
    }

    public PersonPopTree(Person person) {
        this.caption = person.getName();
        this.setId(person.getID());
        this.setIniFold(false);
        this.setImageClass("bpmfont bpmgongzuoliu");
        this.tagVar = new HashMap<>();
        this.tagVar.put("personId", person.getID());
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParamsType() {
        return paramsType;
    }

    public void setParamsType(String paramsType) {
        this.paramsType = paramsType;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
