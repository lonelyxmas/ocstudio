package com.ds.esd.formula.component.grid;


import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.formula.service.esd.ModuleFormComFService;
import com.ds.esd.project.config.formula.FormulaInstParams;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {ModuleFormComFService.class})
public class ModuleFormComGridView {


    @CustomAnnotation(pid = true, hidden = true)
    public String formulaInstId;

    @MethodChinaName(cname = "参数代码")
    public String parameterCode;

    @MethodChinaName(cname = "组件名称")
    private String parameterValue;

    @MethodChinaName(cname = "参数名称")
    public FormulaParams paramsType;

    @CustomAnnotation(uid = true, hidden = true)
    public String id;


    @CustomAnnotation(pid = true, hidden = true)
    public Boolean single;

    public ModuleFormComGridView(FormulaInstParams params) {
        this.parameterValue = params.getValue();
        this.paramsType = params.getParamsType();
        this.formulaInstId = params.getFormulaInstId();
        this.id = params.getId();
        this.single = params.getSingle();
        this.parameterCode = params.getParameterCode();
    }

    public Boolean getSingle() {
        return single;
    }

    public void setSingle(Boolean single) {
        this.single = single;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public FormulaParams getParamsType() {
        return paramsType;
    }

    public void setParamsType(FormulaParams paramsType) {
        this.paramsType = paramsType;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
