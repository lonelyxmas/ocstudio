package com.ds.esd.formula.component;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.ApiFService;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.enums.SelModeType;

@BottomBarMenu
@PopTreeAnnotation( caption = "添加")
@TreeAnnotation(heplBar = true,  selMode = SelModeType.multibycheckbox, customService = {ApiFService.class},  bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class ActionsPopTree extends ActionPopTree {


    public ActionsPopTree(String pattern, EUModule euModule) throws JDSException {
        super(pattern, euModule);

    }

}
