package com.ds.esd.formula.component;

import com.ds.common.JDSException;
import com.ds.common.util.CnToSpell;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.ApiFService;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.component.Component;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.component.event.Event;
import com.ds.esd.tool.ui.component.event.EventKey;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.SelModeType;

import java.util.ArrayList;
import java.util.List;

@BottomBarMenu
@PopTreeAnnotation(caption = "添加动作")
@TreeAnnotation(heplBar = true, selMode = SelModeType.singlecheckbox, customService = {ApiFService.class}, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class ActionPopTree extends TreeListItem {
    @CustomAnnotation(pid = true)
    String pattern = "";

    @CustomAnnotation(pid = true)
    String projectId;

    @CustomAnnotation(pid = true)
    String formulaInstId;

    @CustomAnnotation(pid = true)
    String parameterCode;

    @CustomAnnotation(pid = true)
    String paramsType;


    public ActionPopTree(String pattern, EUModule euModule) throws JDSException {
        super(euModule.getClassName(), euModule.getName());
        this.imageClass = "spafont spa-icon-page";
        List<Component> componentList = new ArrayList<>();
        if (euModule.getComponent().getChildren() != null) {
            componentList.addAll(euModule.getComponent().getChildren());
            for (Component childComponent : componentList) {
                this.addChild(new ComponentPopTree(pattern, childComponent));
            }
        }

    }

    public ActionPopTree(String pattern, Component component) throws JDSException {
        super(component.getPath(), component.getAlias() + "(" + component.getProperties().getDesc() + ")");
        ComponentType componentType = ComponentType.fromType(component.getKey());
        this.imageClass = componentType.getImageClass();

        this.addTagVar("euClassName", component.getModuleComponent().getClassName());
        this.caption = component.getAlias() + "(" + componentType.name() + ")";
        List<Component> componentList = new ArrayList<>();
        if (component.getChildren() != null) {
            componentList.addAll(component.getChildren());
            for (Component childComponent : componentList) {
                this.addChild(new ComponentPopTree(pattern, childComponent));
            }
        }

    }


    public ActionPopTree(Component component, EventKey key, Event event) {
        this.caption = event.getDesc();
        this.setId(component.getAlias() + "_" + key);
        this.setIniFold(false);
        for (Action action : event.getActions()) {
            this.addChild(new ActionPopTree(component, key, event, action));
        }
    }


    public ActionPopTree(Component component, EventKey key, Event event, Action action) {
        this.caption = action.getDesc();
        this.setId(component.getAlias() + "_" + key + "_" + CnToSpell.getShortSpell(action.getDesc()));
        this.setIniFold(false);

    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParamsType() {
        return paramsType;
    }

    public void setParamsType(String paramsType) {
        this.paramsType = paramsType;
    }
}
