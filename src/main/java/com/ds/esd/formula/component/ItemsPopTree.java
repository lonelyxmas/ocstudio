package com.ds.esd.formula.component;


import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.ComponentFService;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.ui.component.Component;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.enums.SelModeType;
@BottomBarMenu
@TreeAnnotation(heplBar = true,  selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save,CustomFormMenu.Close}, customService = {ComponentFService.class})
@PopTreeAnnotation( caption = "添加子对象")
public class ItemsPopTree extends ItemPopTree {

    public ItemsPopTree(String pattern, ComponentType... componentTypes) throws JDSException {
        super(pattern, componentTypes);

    }

    public ItemsPopTree(String pattern, EUPackage euPackage, ComponentType... componentTypes) throws JDSException {
        super(pattern, euPackage, componentTypes);
    }

    public ItemsPopTree(String pattern, EUModule euModule, ComponentType... componentTypes) throws JDSException {
        super(pattern, euModule, componentTypes);

    }

    public ItemsPopTree(String pattern, Component component, ComponentType... componentTypes) throws JDSException {
        super(pattern, component, componentTypes);

    }


}
