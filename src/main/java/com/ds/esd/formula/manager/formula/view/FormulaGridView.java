package com.ds.esd.formula.manager.formula.view;

import com.ds.config.CFormula;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.formula.manager.formula.LocalFormulaService;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {LocalFormulaService.class}, event = CustomGridEvent.editor)
public class FormulaGridView {

    @CustomAnnotation(uid = true, hidden = true)
    private String parameterId;
    @CustomAnnotation(pid = true, hidden = true)
    private String pluginId;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectName;


    @MethodChinaName(cname = "公式类型")
    private FormulaType parameterValue;

    @MethodChinaName(cname = "属性值")
    @CustomAnnotation()
    private String name;

    @MethodChinaName(cname = "显示名称")
    private String desc;


    public FormulaGridView() {

    }


    public FormulaGridView(CFormula formula) {
        this.projectName = formula.getProjectName();
        this.pluginId = formula.getPluginId();
        this.parameterId = formula.getParameterId();
        this.name = formula.getName();
        this.desc = formula.getDesc();
        this.parameterValue = formula.getParameterValue();
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getParameterId() {
        return parameterId;
    }

    public void setParameterId(String parameterId) {
        this.parameterId = parameterId;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setParameterValue(FormulaType parameterValue) {
        this.parameterValue = parameterValue;
    }

    public FormulaType getParameterValue() {
        return parameterValue;
    }

}
