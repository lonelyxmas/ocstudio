package com.ds.esd.formula.manager.formula;

import com.ds.bpm.formula.ExpressionParameter;
import com.ds.bpm.formula.FormulaService;
import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.formula.CtExpressionParameter;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.manager.formula.view.ParameterFormView;
import com.ds.esd.formula.manager.formula.view.ParameterGridView;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/formula/")
@MethodChinaName(cname = "宏公式管理", imageClass = "spafont spa-icon-function")
public class FormulaParameterService {
    @RequestMapping(method = RequestMethod.POST, value = "Parameters")
    @MethodChinaName(cname = "获取宏公式参数")
    @GridViewAnnotation
    @ModuleAnnotation()
    @APIEventAnnotation(bindMenu = {CustomMenuItem.index})
    public @ResponseBody
    ListResultModel<List<ParameterGridView>> getParameters(String participantSelectId) {
        ListResultModel<List<ParameterGridView>> parameterListResultModel = new ListResultModel<List<ParameterGridView>>();
        try {
            if (participantSelectId != null && !participantSelectId.equals("")) {
                ParticipantSelect select = getService().getParticipantSelect(participantSelectId).get();
                List<ExpressionParameter> parameters = select.getParameterList();
                if (parameters != null) {
                    parameterListResultModel = PageUtil.getDefaultPageList(parameters, ParameterGridView.class);
                }

            } else {
                return new ErrorListResultModel<>("type not be null!");

            }

        } catch (JDSException e) {
            e.printStackTrace();
            return new ErrorListResultModel<>(e.getMessage());
        }
        return parameterListResultModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "ParameterInfo")
    @MethodChinaName(cname = "获取宏公式参数")
    @FormViewAnnotation
    @DialogAnnotation(width = "600", height = "280")
    @ModuleAnnotation(caption = "添加参数")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor})
    public @ResponseBody
    ResultModel<ParameterFormView> getFormulaParameter(String parameterId) {
        ResultModel<ParameterFormView> model = new ResultModel<ParameterFormView>();
        ParameterFormView select = null;
        try {
            ExpressionParameter rselect = getService().getFormulaParameter(parameterId).get();
            select = new ParameterFormView(rselect);
            model.setData(select);
        } catch (JDSException e) {
            e.printStackTrace();
            return new ErrorResultModel<>(e.getMessage());
        }

        return model;

    }


    @RequestMapping(method = RequestMethod.POST, value = "AddParameters")
    @MethodChinaName(cname = "添加宏公式参数")
    @DialogAnnotation
    @ModuleAnnotation(caption = "添加宏公式参数")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    @FormViewAnnotation
    public @ResponseBody
    ResultModel<Boolean> addFormulaParameters(@RequestBody CtExpressionParameter parameter) {
        return getService().addFormulaParameters(parameter);
    }


    @RequestMapping(method = RequestMethod.POST, value = "DelParameters")
    @MethodChinaName(cname = "删除宏公式参数")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delParameters(String parameterId) {
        return getService().delParameters(parameterId);
    }


    public FormulaService getService() {
        FormulaService service = (FormulaService) EsbUtil.parExpression("$FormulaService");
        return service;
    }

}
