package com.ds.esd.formula.manager.formula.item;

import com.ds.enums.attribute.Attributetype;
import com.ds.esb.config.formula.EngineType;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.editor.extmenu.PluginsPostationType;
import com.ds.esd.tool.ui.component.list.TreeListItem;

import java.util.HashMap;

@TreeAnnotation(size = 280, heplBar = true)
@TabsAnnotation(closeBtn = true)
public class FormulaTypeItem extends TreeListItem {

    public PluginsPostationType menuType = PluginsPostationType.topMenu;


    public FormulaTypeItem() {
        this.caption = "公式类型";
        this.setId("allFormulaType");
        this.setIniFold(false);
        EngineType[] types = EngineType.values();
        for (EngineType type : types) {
            FormulaTypeItem engineType = new FormulaTypeItem(type);
            if (engineType.getSub() != null && engineType.getSub().size() > 0) {
                this.addChild(engineType);
            }

        }
    }

    public FormulaTypeItem(Attributetype baseType) {
        this.caption = baseType.getName();
        this.setId(baseType.getType());
        this.setIniFold(false);
        this.setImageClass(baseType.getImageClass());
        FormulaType[] formulaTypes = FormulaType.values();
        for (FormulaType formulaType : formulaTypes) {
            if (formulaType.getBaseType().equals(baseType)) {
                FormulaTypeItem formulaTypeItem = new FormulaTypeItem(formulaType);
                this.addChild(formulaTypeItem);

            }
        }

    }

    public FormulaTypeItem(FormulaType formulaType) {
        this.caption = formulaType.getName();
        this.setImageClass(formulaType.getImageClass());
        this.setId(formulaType.getType());
        this.setIniFold(false);
        this.setCloseBtn(true);
        this.setEuClassName("admin.formula.Formulas");
        this.tagVar = new HashMap<>();
        this.tagVar.put("formulaType", formulaType.getType());

    }

    public FormulaTypeItem(EngineType engineType) {
        this.caption = engineType.getName();
        this.setImageClass(engineType.getImageClass());
        this.setId(engineType.getType());
        this.setIniFold(false);
        Attributetype[] attributetypes = Attributetype.values();
        for (Attributetype attributetype : attributetypes) {
            if (attributetype.getEngineType().equals(engineType)) {
                FormulaTypeItem formulaTypeItem = new FormulaTypeItem(attributetype);
                if (formulaTypeItem.getSub() != null && formulaTypeItem.getSub().size() > 0) {
                    this.addChild(formulaTypeItem);
                }
            }
        }

    }

    public PluginsPostationType getMenuType() {
        return menuType;
    }

    public void setMenuType(PluginsPostationType menuType) {
        this.menuType = menuType;
    }
}
