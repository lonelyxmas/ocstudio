package com.ds.esd.formula.manager.formula.view;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.bpm.formula.ExpressionParameter;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaParams;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.TextEditorAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.manager.formula.FormulaParameterService;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(stretchHeight = StretchType.last, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {FormulaParameterService.class})
public class ParameterFormView implements ExpressionParameter {
    @CustomAnnotation(hidden = true, uid = true)
    private String parameterId;

    @CustomAnnotation(hidden = true, pid = true)
    @JSONField(name = "participantSelectId")
    private String participantSelectId;

    @CustomAnnotation(hidden = true)
    private String parameterenName;

    @MethodChinaName(cname = "参数代码")
    @Required
    @CustomAnnotation()
    private String parameterCode;
    @Required
    @MethodChinaName(cname = "参数名称")
    @CustomAnnotation()
    private String parameterName;
    @Required
    @MethodChinaName(cname = "参数类型")
    @CustomAnnotation()
    private FormulaParams parameterType;

    @MethodChinaName(cname = "是否多选")
    @CustomAnnotation()
    private Boolean single = false;


    @MethodChinaName(cname = "默认值")
    @TextEditorAnnotation
    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation()
    private String parameterValue;

    @MethodChinaName(cname = "参数描述")
    @TextEditorAnnotation
    @FieldAnnotation(colSpan = -1)
    private String parameterDesc;

    public ParameterFormView() {

    }

    public ParameterFormView(ExpressionParameter expressionParameter) {
        this.parameterValue = expressionParameter.getParameterValue();
        this.parameterId = expressionParameter.getParameterId();
        this.participantSelectId = expressionParameter.getParticipantSelectId();
        this.parameterCode = expressionParameter.getParameterCode();
        this.parameterName = expressionParameter.getParameterName();
        this.parameterenName = expressionParameter.getParameterenName();
        this.parameterDesc = expressionParameter.getParameterDesc();
        this.parameterType = expressionParameter.getParameterType();
        this.single = expressionParameter.getSingle();

    }

    /**
     * @return Returns the parameterCode.
     */
    public String getParameterCode() {
        return parameterCode;
    }

    /**
     * @param parameterCode The parameterCode to set.
     */
    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    /**
     * @return Returns the parameterDesc.
     */
    public String getParameterDesc() {
        return parameterDesc;
    }

    /**
     * @param parameterDesc The parameterDesc to set.
     */
    public void setParameterDesc(String parameterDesc) {
        this.parameterDesc = parameterDesc;
    }

    /**
     * @return Returns the parameterId.
     */
    public String getParameterId() {
        return parameterId;
    }

    /**
     * @param parameterId The parameterId to set.
     */
    public void setParameterId(String parameterId) {
        this.parameterId = parameterId;
    }

    @Override
    public Boolean getSingle() {
        return single;
    }

    @Override
    public void setSingle(Boolean single) {
        this.single = single;
    }

    /**
     * @return Returns the parameterName.
     */
    public String getParameterName() {
        return parameterName;
    }

    /**
     * @param parameterName The parameterName to set.
     */
    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    /**
     * @return Returns the parameterType.
     */
    public FormulaParams getParameterType() {
        return parameterType;
    }

    /**
     * @param parameterType The parameterType to set.
     */
    public void setParameterType(FormulaParams parameterType) {
        this.parameterType = parameterType;
    }

    @Override
    public String getParameterValue() {
        return parameterValue;
    }

    @Override
    public void setParameterValue(String parameterValue) {

        this.parameterValue = parameterValue;
    }

    /**
     * @return Returns the participantId.
     */
    public String getParticipantSelectId() {
        return participantSelectId;
    }

    /**
     * @param participantSelectId The participantId to set.
     */
    public void setParticipantSelectId(String participantSelectId) {
        this.participantSelectId = participantSelectId;

    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o instanceof com.ds.esd.bpm.formula.CtExpressionParameter) {
            com.ds.esd.bpm.formula.CtExpressionParameter parameter = (com.ds.esd.bpm.formula.CtExpressionParameter) o;
            if (this.parameterCode == null) {
                return false;
            } else {
                return this.parameterCode.equalsIgnoreCase(parameter.getParameterCode());
            }
        } else if (o instanceof String) {
            String id = (String) o;
            if (this.parameterCode == null) {
                return false;
            } else {
                return this.parameterCode.equalsIgnoreCase(id);
            }
        } else {
            return false;
        }
    }

    public String getParameterenName() {
        return parameterenName;
    }

    public void setParameterenName(String parameterenName) {
        this.parameterenName = parameterenName;
    }
}
