package com.ds.esd.formula.manager.formula;

import com.ds.bpm.formula.FormulaService;
import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.bpm.formula.CtParticipantSelect;
import com.ds.esd.custom.annotation.ContainerAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.manager.formula.view.ParticipantSelectFormView;
import com.ds.esd.formula.manager.formula.view.ParticipantSelectGridView;
import com.ds.esd.tool.ui.enums.ThemesType;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/admin/formula/")
public class LocalFormulaService {

    @RequestMapping(method = RequestMethod.POST, value = "Formulas")
    @MethodChinaName(cname = "获取表达式信息")
    @GridViewAnnotation()
    @ModuleAnnotation()
    @APIEventAnnotation(bindMenu = CustomMenuItem.reload)
    @ContainerAnnotation(sandboxTheme = ThemesType.webflat)
    public @ResponseBody
    ListResultModel<List<ParticipantSelectGridView>> getFormulas(String formulaType) {
        return PageUtil.changPageList(getService().getFormulas(FormulaType.fromType(formulaType)), ParticipantSelectGridView.class);
    }


    @RequestMapping(method = RequestMethod.POST, value = "ParticipantInfo")
    @FormViewAnnotation()
    @ModuleAnnotation(caption = "编辑表达式信息")
    @DialogAnnotation
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor}, autoRun = true)
    @ContainerAnnotation(sandboxTheme = ThemesType.webflat)
    public @ResponseBody
    ResultModel<ParticipantSelectFormView> getParticipantSelect(String participantSelectId, String formulaType) {
        ResultModel<ParticipantSelectFormView> model = new ResultModel<ParticipantSelectFormView>();
        ParticipantSelect select = null;
        try {
            select = getService().getParticipantSelect(participantSelectId).get();
            ParticipantSelectFormView selectView = new ParticipantSelectFormView(select);
            model.setData(selectView);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return model;
    }

    @RequestMapping(method = RequestMethod.POST, value = "CreateParticipant")
    @MethodChinaName(cname = "添加表达式")
    @FormViewAnnotation()
    @DialogAnnotation
    @ModuleAnnotation(caption = "添加表达式")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add}, autoRun = true)
    @ContainerAnnotation(sandboxTheme = ThemesType.webflat)
    public @ResponseBody
    ResultModel<ParticipantSelectFormView> createParticipantSelect(String formulaType) {
        ResultModel<ParticipantSelectFormView> model = new ResultModel<ParticipantSelectFormView>();
        ParticipantSelect participantSelect = new CtParticipantSelect();
        participantSelect.setParticipantSelectId(UUID.randomUUID().toString());

        if (formulaType != null && formulaType.equals("")) {
            participantSelect.setFormulaType(FormulaType.fromType(formulaType));
        }
        ParticipantSelectFormView selectVeiw = new ParticipantSelectFormView(participantSelect);
        model.setData(selectVeiw);
        return model;
    }

    @RequestMapping(method = RequestMethod.POST, value = "saveParticipant")
    @MethodChinaName(cname = "添加表达式")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> addParticipantSelect(@RequestBody ParticipantSelect participantSelect) {
        return getService().addParticipantSelect(participantSelect);
    }


    @RequestMapping(method = RequestMethod.POST, value = "DelParticipant")
    @MethodChinaName(cname = "删除表达式")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delParticipantSelect(String participantSelectId) {
        return getService().delParticipantSelect(participantSelectId);
    }

    public FormulaService getService() {
        FormulaService service = (FormulaService) EsbUtil.parExpression("$FormulaService");
        return service;
    }

}
