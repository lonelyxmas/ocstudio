package com.ds.esd.formula.manager;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.TextEditorAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.web.annotation.Required;


@BottomBarMenu
@FormAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class FormulaSelectFormView {

    @Required
    @CustomAnnotation(caption = "公式名称")
    private String selectName;

    @CustomAnnotation(caption = "类型")
    @MethodChinaName(cname = "类型")
    private FormulaType formulaType;

    @MethodChinaName(cname = "公式名称")
    @CustomAnnotation(hidden = true)
    private String selectenName;

    @MethodChinaName(cname = "公式")
    @TextEditorAnnotation
    @FieldAnnotation(rowHeight = "60", colSpan = -1, required = true)
    @CustomAnnotation()
    private String formula;


    @MethodChinaName(cname = "描述")
    @TextEditorAnnotation
    @FieldAnnotation(rowHeight = "75", colSpan = -1)
    @CustomAnnotation()
    private String selectDesc;

    private String participantSelectId;

    public FormulaSelectFormView() {

    }

    public FormulaSelectFormView(ParticipantSelect select) {
        this.participantSelectId = select.getParticipantSelectId();
        this.selectName = select.getSelectName();
        this.formulaType = select.getFormulaType();
        this.formula = select.getFormula();
        this.selectDesc = select.getSelectDesc();
    }


    public String getSelectDesc() {
        return selectDesc;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public FormulaType getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }

    public String getSelectenName() {
        return selectenName;
    }

    public void setSelectenName(String selectenName) {
        this.selectenName = selectenName;
    }

    public String getSelectName() {
        return selectName;
    }

    public void setSelectName(String selectName) {
        this.selectName = selectName;
    }

    public String getParticipantSelectId() {
        return participantSelectId;
    }

    public void setParticipantSelectId(String participantSelectId) {
        this.participantSelectId = participantSelectId;
    }

    public void setSelectDesc(String selectDesc) {
        this.selectDesc = selectDesc;
    }

}
