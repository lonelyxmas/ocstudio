package com.ds.esd.formula.manager.formula.view;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.CodeEditorAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.TextEditorAnnotation;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.custom.toolbar.CodeEditorTools;
import com.ds.esd.formula.manager.formula.LocalFormulaService;

@PageBar
@GridAnnotation(rowHeight = "4em", customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {LocalFormulaService.class}, event = CustomGridEvent.editor)
public class FormulaSelectGridView {

    private String selectName;

    @CustomAnnotation(caption = "类型")
    private FormulaType formulaType;


    @CustomAnnotation(caption = "公式名称")
    private String selectenName;

    @MethodChinaName(cname = "公式")
    @CodeEditorAnnotation
    @ToolBarMenu(menuClasses = CodeEditorTools.class)
    @CustomAnnotation()
    private String formula;


    @MethodChinaName(cname = "描述")
    @TextEditorAnnotation
    @CustomAnnotation()
    private String selectDesc;

    private String participantSelectId;

    public FormulaSelectGridView() {

    }

    public FormulaSelectGridView(ParticipantSelect select) {
        this.participantSelectId = select.getParticipantSelectId();
        this.selectName = select.getSelectName();
        this.formulaType = select.getFormulaType();
        this.formula = select.getFormula();
        this.selectDesc = select.getSelectDesc();
    }


    public String getSelectDesc() {
        return selectDesc;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public FormulaType getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }

    public String getSelectenName() {
        return selectenName;
    }

    public void setSelectenName(String selectenName) {
        this.selectenName = selectenName;
    }

    public String getSelectName() {
        return selectName;
    }

    public void setSelectName(String selectName) {
        this.selectName = selectName;
    }

    public String getParticipantSelectId() {
        return participantSelectId;
    }

    public void setParticipantSelectId(String participantSelectId) {
        this.participantSelectId = participantSelectId;
    }

    public void setSelectDesc(String selectDesc) {
        this.selectDesc = selectDesc;
    }

}
