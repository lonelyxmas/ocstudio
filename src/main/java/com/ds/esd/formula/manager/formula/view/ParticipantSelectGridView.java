package com.ds.esd.formula.manager.formula.view;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.enums.attribute.Attributetype;
import com.ds.esb.config.formula.EngineType;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.TextEditorAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.formula.manager.formula.LocalFormulaService;
import com.ds.esd.tool.ui.enums.ComboInputType;
@PageBar
@GridAnnotation(rowHeight = "4em", customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {LocalFormulaService.class}, event = CustomGridEvent.editor)
public class ParticipantSelectGridView {

    @CustomAnnotation(caption = "公式名称")
    private String selectName;

    @CustomAnnotation(caption = "引擎类型")
    private EngineType engineType;

    @CustomAnnotation(caption = "基础类型")
    private Attributetype baseType;

    @CustomAnnotation(caption = "公式分类")
    private FormulaType formulaType;

    @CustomAnnotation(hidden = true)
    private String selectenName;

    @TextEditorAnnotation
    @CustomAnnotation(  caption = "公式")
    private String formula;
    @TextEditorAnnotation
    @CustomAnnotation( caption = "描述")
    private String selectDesc;

    @CustomAnnotation(hidden = true, uid = true)
    private String participantSelectId;


    public ParticipantSelectGridView() {

    }

    public ParticipantSelectGridView(ParticipantSelect select) {
        this.participantSelectId = select.getParticipantSelectId();
        this.formula = select.getFormula();
        this.selectName = select.getSelectName();
        this.formulaType = select.getFormulaType();
        if (formulaType != null) {
            this.baseType = formulaType.getBaseType();
        }
        if (baseType != null) {
            this.engineType = baseType.getEngineType();
        }
        this.formula = select.getFormula();
        this.selectDesc = select.getSelectDesc();

    }


    public EngineType getEngineType() {
        return engineType;
    }

    public void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    public Attributetype getBaseType() {
        return baseType;
    }

    public void setBaseType(Attributetype baseType) {
        this.baseType = baseType;
    }

    public String getSelectDesc() {
        return selectDesc;
    }


    public String getFormula() {
        return formula;
    }


    public void setFormula(String formula) {
        this.formula = formula;
    }


    public FormulaType getFormulaType() {
        return formulaType;
    }


    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }


    public String getSelectenName() {
        return selectenName;
    }


    public void setSelectenName(String selectenName) {

        this.selectenName = selectenName;
    }


    public String getSelectName() {
        return selectName;
    }


    public void setSelectName(String selectName) {
        this.selectName = selectName;
    }


    public String getParticipantSelectId() {
        return participantSelectId;
    }


    public void setParticipantSelectId(String participantSelectId) {
        this.participantSelectId = participantSelectId;
    }


    public void setSelectDesc(String selectDesc) {
        this.selectDesc = selectDesc;
    }

}
