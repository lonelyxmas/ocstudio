package com.ds.esd.formula.manager.formula.view;


import com.ds.bpm.formula.ExpressionParameter;
import com.ds.bpm.formula.ParticipantSelect;
import com.ds.enums.attribute.Attributetype;
import com.ds.esb.config.formula.EngineType;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.enums.AppendType;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.toolbar.CodeEditorTools;
import com.ds.esd.formula.manager.formula.FormulaParameterService;
import com.ds.esd.formula.manager.formula.LocalFormulaService;

import java.util.List;

@BottomBarMenu
@FormAnnotation(col = 3, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {LocalFormulaService.class})
public class ParticipantSelectFormView implements ParticipantSelect {

    @CustomAnnotation(caption = "公式名称")
    @FieldAnnotation(colSpan = -1)
    private String selectName;

    @CustomAnnotation(caption = "引擎类型", readonly = true)
    private EngineType engineType = EngineType.CUSTOM;

    @ComboListBoxAnnotation
    @CustomListAnnotation(filter = "source.engineType.toString()==engineType", dynLoad = true)
    @CustomAnnotation(caption = "基础类型")
    private Attributetype baseType;

    @ComboListBoxAnnotation
    @CustomListAnnotation(filter = "source.baseType.toString()==baseType", dynLoad = true)
    @CustomAnnotation(caption = "公式分类")
    private FormulaType formulaType;

    @CustomAnnotation(hidden = true)
    private String selectenName;

    @CodeEditorAnnotation
    @ToolBarMenu(menuClasses = CodeEditorTools.class)
    @FieldAnnotation(rowHeight = "60", colSpan = -1)
    @CustomAnnotation(caption = "公式")
    private String formula;

    @TextEditorAnnotation
    @FieldAnnotation(rowHeight = "75", colSpan = -1)
    @CustomAnnotation(caption = "描述")
    private String selectDesc;

    @CustomAnnotation(hidden = true, uid = true)
    private String participantSelectId;

    @ComboModuleAnnotation(append = AppendType.ref, bindClass = FormulaParameterService.class)
    @FieldAnnotation(colSpan = -1, rowHeight = "300")
    @CustomAnnotation(caption = "参数列表")
    private List<ExpressionParameter> parameterList;


    public ParticipantSelectFormView() {

    }


    public ParticipantSelectFormView(ParticipantSelect select) {
        this.participantSelectId = select.getParticipantSelectId();
        this.parameterList = select.getParameterList();
        this.selectName = select.getSelectName();
        this.formulaType = select.getFormulaType();
        this.formula = select.getFormula();

        if (formulaType != null) {
            this.baseType = formulaType.getBaseType();
        }
        if (baseType != null) {
            this.engineType = baseType.getEngineType();
        }
        this.selectDesc = select.getSelectDesc();

    }

    public EngineType getEngineType() {
        return engineType;
    }

    public void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    public Attributetype getBaseType() {
        return baseType;
    }

    public void setBaseType(Attributetype baseType) {
        this.baseType = baseType;
    }

    @Override
    public String getSelectDesc() {
        return selectDesc;
    }

    @Override
    public String getFormula() {
        return formula;
    }

    @Override
    public void setFormula(String formula) {
        this.formula = formula;
    }

    @Override
    public FormulaType getFormulaType() {
        return formulaType;
    }

    @Override
    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }

    @Override
    public String getSelectenName() {
        return selectenName;
    }

    @Override
    public void setSelectenName(String selectenName) {
        this.selectenName = selectenName;
    }

    @Override
    public String getSelectName() {
        return selectName;
    }

    @Override
    public void setSelectName(String selectName) {
        this.selectName = selectName;
    }

    @Override
    public String getParticipantSelectId() {
        return participantSelectId;
    }

    @Override
    public void setParticipantSelectId(String participantSelectId) {
        this.participantSelectId = participantSelectId;
    }


    @Override
    public void setParameterList(List<ExpressionParameter> parameterList) {
        this.parameterList = parameterList;
    }

    @Override
    public void setSelectDesc(String selectDesc) {
        this.selectDesc = selectDesc;
    }

    @Override
    public List<ExpressionParameter> getParameterList() {
        return parameterList;
    }

}
