package com.ds.esd.formula.manager.formula;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.formula.manager.formula.view.ParticipantSelectGridView;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/admin/formula/")

public class FormulaSearchService {


    @CustomAnnotation(pid = true, hidden = true)
    public String projectId;


    @FormViewAnnotation(searchUrl = "Search")
    @ModuleAnnotation(caption = "查找公式")
    @RequestMapping(path = "SearchView")
    @MethodChinaName(cname = "查找公式")
    @DialogAnnotation
    @APIEventAnnotation(bindMenu = {CustomMenuItem.reload})
    @ResponseBody
    public ResultModel<ParticipantSelectGridView> fullSearch() {
        ResultModel<ParticipantSelectGridView> model = new ResultModel<ParticipantSelectGridView>();
        try {
            model.setData(new ParticipantSelectGridView());
        } catch (Exception e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
            e.printStackTrace();
        }
        return model;
    }


    @GridViewAnnotation()
    @RequestMapping(path = "Search")
    @MethodChinaName(cname = "公式列表")
    @ModuleAnnotation()
    @APIEventAnnotation(bindMenu = {CustomMenuItem.search})
    @ResponseBody
    public ListResultModel<List<ParticipantSelectGridView>> search(@RequestBody ParticipantSelectGridView formulaSearchView) {
        ListResultModel model = new ListResultModel();
        try {
            List<ParticipantSelect> ctParticipantSelects = new ArrayList<>();
            FormulaType type = formulaSearchView.getFormulaType();

            ListResultModel<List<ParticipantSelect>> selects = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).formulaSearch(type, formulaSearchView.getFormula(), formulaSearchView.getSelectName());
            //  List<ParticipantSelect> foumulas = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulas(type);
            model = PageUtil.changPageList(selects, ParticipantSelectGridView.class);
        } catch (Exception e) {
            model = new ErrorListResultModel();
            ((ErrorListResultModel) model).setErrdes(e.getMessage());
            e.printStackTrace();
        }
        return model;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }


}
