package com.ds.esd.formula;


import com.ds.enums.attribute.AttributeName;
import com.ds.esb.config.formula.FormulaParams;
import com.ds.esd.formula.service.*;
import com.ds.esd.formula.service.esd.ModuleBarFService;
import com.ds.esd.formula.service.esd.ModuleFormComFService;
import com.ds.esd.formula.service.esd.ModuleItemFService;
import com.ds.esd.formula.service.esd.ModuleUIComFService;


public enum FormulaParamsEnums implements AttributeName {

    PERSON(FormulaParams.PERSON, PersonFService.class, "Index"),
    //
    ORG(FormulaParams.ORG, DeparmentFService.class, "Index"),
    //
    TABLE(FormulaParams.TABLE, TableFService.class, "Index"),
    //

    ESDCOM(FormulaParams.ESDCOM, ComponentService.class, "Index"),

    CURRUICOM(FormulaParams.CURRUICOM, ModuleUIComFService.class, "Index"),

    CURRITEM(FormulaParams.CURRITEM, ModuleItemFService.class, "Index"),

    CURRBAR(FormulaParams.CURRBAR, ModuleBarFService.class, "Index"),

    CURRTAB(FormulaParams.CURRTAB, ModuleBarFService.class, "Index"),

    CURRACTION(FormulaParams.CURRACTION, ModuleBarFService.class, "Index"),

    CURRFORMCOM(FormulaParams.CURRFORMCOM, ModuleFormComFService.class, "Index"),

    PACKAGE(FormulaParams.PACKAGE, PackageFService.class, "Index"),

    FOLDER(FormulaParams.FOLDER, FolderFService.class, "Index"),

    FIELD(FormulaParams.FIELD, FieldFService.class, "Index"),

    ESDUICOM(FormulaParams.ESDUICOM, UIComponentFService.class, "Index"),

    ESDFORMCOM(FormulaParams.ESDFORMCOM, FormComponentFService.class, "Index"),

    CLASSNAME(FormulaParams.CLASSNAME, ModuleFService.class, "Index"),
    //
    API(FormulaParams.API, ApiFService.class, "Index"),

    ORGROLE(FormulaParams.ORGROLE, OrgRoleFService.class, "Index"),

    ORGLEVEL(FormulaParams.ORGLEVEL, OrgLevelFService.class, "Index"),

    PERSONGROUP(FormulaParams.PERSONGROUP, PersonGroupFService.class, "Index"),

    PERSONROLE(FormulaParams.PERSONROLE, PersonRoleFService.class, "Index"),

    PERSONPOSTTION(FormulaParams.PERSONPOSTTION, PersonPositionFService.class, "Index"),

    PERSONDUTY(FormulaParams.PERSONDUTY, PersonDutyFService.class, "Index"),

    PERSONLEVEL(FormulaParams.PERSONLEVEL, PersonLevelFService.class, "Index"),

    UNKNOW(FormulaParams.UNKNOW, Object.class, "Index"),;

    private String name;
    private Class clazz;
    private String displayName;
    private String methodName;


    FormulaParamsEnums(FormulaParams params, Class clazz, String methodName) {
        this.name = params.getType();
        this.displayName = params.getName();
        this.clazz = clazz;
        this.methodName = methodName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public static FormulaParamsEnums fromName(String name) {
        for (FormulaParamsEnums type : FormulaParamsEnums.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    @Override
    public String getType() {
        return name;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
