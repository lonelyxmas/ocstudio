package com.ds.esd.formula.action;

import com.ds.common.database.metadata.ColInfo;
import com.ds.common.database.metadata.MetadataFactory;
import com.ds.common.database.metadata.TableInfo;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/admin/formula/")
@Aggregation(type=AggregationType.menu)
public class ParamsAction {

    @MethodChinaName(cname = "向上")
    @RequestMapping(method = RequestMethod.POST, value = "moveUP")
    @CustomAnnotation(imageClass = "spafont spa-icon-move-up")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> moveUP(String configKey, String tablename, String fieldname) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            MetadataFactory factory = ESDFacrory.getESDClient().getDbFactory(configKey);
            TableInfo tableInfo = factory.getTableInfo(tablename);
            List<ColInfo> colInfoList = tableInfo.getColList();
            ColInfo colInfo = tableInfo.getCoInfoByName(fieldname);
            int k = colInfoList.indexOf(colInfo);
            colInfoList.add(k - 1, colInfoList.remove(k));
            ColInfo nextColInfo = colInfoList.get(k - 2);
            factory.sortColIndex(tablename, colInfo, nextColInfo.getFieldname());
        } catch (Exception e) {
            ErrorResultModel errorResult = new ErrorResultModel();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }


    @MethodChinaName(cname = "向下")
    @RequestMapping(method = RequestMethod.POST, value = "moveDown")
    @CustomAnnotation(imageClass = "spafont spa-icon-move-down")
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = RequestPathEnum.treegridrow)
    public @ResponseBody
    ResultModel<Boolean> moveDown(String configKey, String tablename, String fieldname) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            MetadataFactory factory = ESDFacrory.getESDClient().getDbFactory(configKey);
            TableInfo tableInfo = factory.getTableInfo(tablename);
            List<ColInfo> colInfoList = tableInfo.getColList();
            ColInfo colInfo = tableInfo.getCoInfoByName(fieldname);
            int k = colInfoList.indexOf(colInfo);
            if (k < colInfoList.size()) {
                colInfoList.add(k+1, colInfoList.remove(k));
            }
            ColInfo nextColInfo = colInfoList.get(k - 1);
            factory.sortColIndex(tablename, colInfo, nextColInfo.getFieldname());
        } catch (Exception e) {
            ErrorResultModel errorResult = new ErrorResultModel();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }
//
//    @MethodChinaName(cname = "关联关系")
//    @RequestMapping(method = RequestMethod.POST, value = "refInfo")
//    @CustomAnnotation(imageClass = "spafont spa-icon-c-databinder")
//    @APIEventAnnotation(callback = {CustomCallBack.Reload}, customRequestData = UrlPathEnum.treegridrow)
//    public @ResponseBody
//    ResultModel<Boolean> refInfo(String configKey, String tablename, String fieldname) {
//        ResultModel<Boolean> result = new ResultModel<Boolean>();
//        try {
//            MetadataFactory factory = ESDFacrory.getESDClient().getDbFactory(configKey);
//            TableInfo tableInfo = factory.getTableInfo(tablename);
//            List<ColInfo> colInfoList = tableInfo.getColList();
//            ColInfo colInfo = tableInfo.getCoInfoByName(fieldname);
//            int k = colInfoList.indexOf(colInfo);
//            if (k < colInfoList.size()) {
//                colInfoList.add(k, colInfoList.remove(k));
//            }
//            ColInfo nextColInfo = colInfoList.get(k - 1);
//            factory.sortColIndex(tablename, colInfo, nextColInfo.getFieldname());
//        } catch (JDSException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }

}

