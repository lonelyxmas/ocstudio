package com.ds.esd.formula;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.TreeListResultModel;
import com.ds.enums.attribute.Attributetype;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.EngineType;
import com.ds.esb.config.formula.FormulaType;

import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;

import com.ds.esd.formula.manager.formula.item.FormulaTypeItem;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/admin/formula/")
@MethodChinaName(cname = "公式管理", imageClass = "bpmfont bpmgongzuoliu")

public class FormulaIndex {

    @RequestMapping(method = RequestMethod.POST, value = "Index")
    @NavTreeViewAnnotation
    @ModuleAnnotation( dynLoad = true, imageClass = "bpmfont bpmgongzuoliu", caption = "公式管理")
    @DialogAnnotation(width = "600", height = "450")
    @APIEventAnnotation(bindMenu = CustomMenuItem.index)
    @ResponseBody
    public TreeListResultModel<List<FormulaTypeItem>> getIndex(String id, Attributetype baseType, EngineType engineType, FormulaType formulaType) {
        TreeListResultModel<List<FormulaTypeItem>> resultModel = new TreeListResultModel<List<FormulaTypeItem>>();
        List<FormulaTypeItem> items = new ArrayList<>();
        FormulaTypeItem root = null;
        if (baseType != null) {
            root = new FormulaTypeItem(baseType);
        } else if (formulaType != null) {
            root = new FormulaTypeItem(formulaType);
        } else if (engineType != null) {
            root = new FormulaTypeItem(engineType);
        } else {
            root = new FormulaTypeItem();
        }
        items.add(root);
        if (id != null && !id.equals("")) {
            String[] orgIdArr = StringUtility.split(id, ";");
            resultModel.setIds(Arrays.asList(orgIdArr));
        } else {
            resultModel.setIds(Arrays.asList(new String[]{root.getFristClassItem(root).getId()}));
        }
        resultModel.setData(items);
        return resultModel;

    }

    @MethodChinaName(cname = "授权")
    @RequestMapping(method = RequestMethod.POST, value = "CustomFormula")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "600", height = "450")
    @ModuleAnnotation( dynLoad = true, imageClass = "xui-uicmd-cmdbox", caption = "授权")
    @ResponseBody
    public TreeListResultModel<List<FormulaTypeItem>> getRightFormula(String id, Attributetype baseType) {
        return getIndex(id, baseType, null, null);
    }


    @JSONField(serialize = false)
    public ESDClient getEsdClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }
}
