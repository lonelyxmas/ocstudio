package com.ds.esd;

import com.ds.esd.tool.ui.enums.ItemType;

public enum RightPluginType implements ItemType {
    comright("comright", "组件授权", "spafont spa-icon-astext", "esd.right.ComRight"),
    moduleright("moduleright", "模块授权", "spafont spa-icon-moveforward", "esd.right.ModuleRight"),
    bpmright("bpmright", "流程授权", "bpmfont bpmgongzuoliuzhutiguizeweihuguanli", "esd.right.BpmRight");

    private String id;
    private String caption;
    private String imageClass;
    private String className;


    RightPluginType(String id, String caption, String imageClass, String className) {
        this.id = id;
        this.caption = caption;
        this.imageClass = imageClass;
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getImageClass() {
        return imageClass;
    }

    @Override
    public String getCaption() {
        return caption;
    }

}
