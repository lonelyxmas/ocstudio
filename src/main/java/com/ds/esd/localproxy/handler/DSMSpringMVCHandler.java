package com.ds.esd.localproxy.handler;


import com.alibaba.fastjson.JSONObject;
import com.ds.common.JDSConstants;
import com.ds.common.logging.Log;
import com.ds.common.logging.LogFactory;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.context.JDSActionContext;
import com.ds.esd.custom.CustomViewFactory;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.enums.ModuleViewType;
import com.ds.esd.custom.form.CustomFormViewBean;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.util.GridPageUtil;
import com.ds.esd.util.TabPageUtil;
import com.ds.esd.util.TreePageUtil;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.jds.core.esb.util.OgnlUtil;
import com.ds.server.httpproxy.core.ConfigOption;
import com.ds.server.httpproxy.core.HttpRequest;
import com.ds.server.httpproxy.core.HttpResponse;
import com.ds.server.httpproxy.core.Server;
import com.ds.web.RequestMethodBean;
import com.ds.web.util.JSONGenUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


public class DSMSpringMVCHandler extends AbstractRADHandler {
    public static final ConfigOption RULE_OPTION = new ConfigOption("rule", true, "Regular expression for matching URLs.");
    private static final Log logger = LogFactory.getLog(JDSConstants.CONFIG_KEY, DSMSpringMVCHandler.class);
    Pattern rule;

    public boolean initialize(String handlerName, Server server) {
        super.initialize(handlerName, server);
        rule = Pattern.compile(RULE_OPTION.getProperty(server, handlerName));
        return true;
    }


    protected boolean handleBody(HttpRequest request, HttpResponse response) throws IOException {
        boolean ruleMatches = rule.matcher(request.getUrl()).matches();
        if (!ruleMatches) {
            return false;
        }
        logger.info("request url " + request.getUrl());
        RequestMethodBean methodBean = this.getRequestMethodBean(request);

        if (methodBean == null) {
            return false;
        }

        try {
            Object object = null;
            String contentType = this.getContentType(request);
            MethodConfig methodConfig = (MethodConfig) JDSActionContext.getActionContext().getContext().get(CustomViewFactory.MethodBeanKey);
            Class iClass = JSONGenUtil.getInnerReturnType(methodBean.getSourceMethod());
            if (methodConfig != null && methodConfig.isModule()) {
                ModuleViewType moduleViewType = methodConfig.getModuleViewType();
                object = this.invokMethod(methodBean, request, response);
                if (moduleViewType != null && !(object instanceof ResultModel)) {
                    switch (moduleViewType) {
                        case GalleryConfig:
                            if (Collection.class.isAssignableFrom(object.getClass())) {
                                object = GridPageUtil.getDefaultPageList(Arrays.asList(((Collection) object).toArray()), iClass);
                            }
                            break;
                        case TreeConfig:
                            if (Collection.class.isAssignableFrom(object.getClass())) {
                                object = TreePageUtil.getDefaultTreeList(Arrays.asList(((Collection) object).toArray()), iClass);
                            }
                            break;
                        case NavTabsConfig:
                            if (Collection.class.isAssignableFrom(object.getClass())) {
                                object = TabPageUtil.getTabList(Arrays.asList(((Collection) object).toArray()), iClass);
                            }
                            break;
                        case PopTreeConfig:
                            if (Collection.class.isAssignableFrom(object.getClass())) {
                                object = TreePageUtil.getDefaultTreeList(Arrays.asList(((Collection) object).toArray()), iClass);
                            }
                            break;
                        case NavGalleryConfig:
                            break;
                        case GridConfig:
                            if (Collection.class.isAssignableFrom(object.getClass())) {
                                object = GridPageUtil.getDefaultPageList(Arrays.asList(((Collection) object).toArray()), iClass);
                            }
                            break;
                        case NavButtonViewsConfig:
                            break;
                        case FormConfig:
                            ResultModel resultModel = new ResultModel<>();
                            CustomFormViewBean formViewBean = (CustomFormViewBean) methodConfig.getView();
                            List<FieldFormConfig> fieldFormConfigs = formViewBean.getAllFields();
                            for (FieldFormConfig fieldFormConfig : fieldFormConfigs) {
                                if (fieldFormConfig.getCustomBean() != null
                                        && fieldFormConfig.getAggConfig().getExpression() != null
                                        && !fieldFormConfig.getAggConfig().getExpression().equals("")) {
                                    Map<String, Object> objectMap = JDSActionContext.getActionContext().getContext();
                                    String expression = fieldFormConfig.getAggConfig().getExpression();
                                    Object obj = EsbUtil.parExpression(expression, objectMap, object, fieldFormConfig.getEsdField().getReturnType());
                                    OgnlUtil.setProperty(fieldFormConfig.getEsdField().getName(), obj, object, objectMap);
                                }


                            }


                            resultModel.setData(object);
                            object = resultModel;
                            break;
                        case NavMenuBarConfig:
                            break;
                    }
                }

                String json = object.toString();
                if (methodBean.getResponseBody() != null) {
                    json = JSONObject.toJSONString(object);
                }
                return sendJSON(methodBean, response, json);
            } else {
                return false;
            }


        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel resultModel = new ErrorResultModel();
            resultModel.setErrdes(e.getMessage());
            return sendJSON(methodBean, response, JSONObject.toJSONString(resultModel, false));
        }

    }


}
