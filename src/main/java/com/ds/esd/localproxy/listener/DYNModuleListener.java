package com.ds.esd.localproxy.listener;

import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.module.ModuleComponent;

public interface DYNModuleListener<T> {

    public void before();

    public <T extends ModuleComponent> EUModule<T> after();

    public <T extends ModuleComponent> EUModule<T>  callBack();

}
