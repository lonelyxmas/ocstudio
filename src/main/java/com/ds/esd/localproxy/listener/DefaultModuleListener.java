package com.ds.esd.localproxy.listener;

import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.module.ModuleComponent;

import java.util.Map;

public class DefaultModuleListener<T> implements DYNModuleListener<T> {


    private final Class<T> customClass;

    private final Map<String,?> valueMap;

    public DefaultModuleListener(Class<T> customClass, Map<String, ?> valueMap) {
        this.customClass=customClass;
        this.valueMap=valueMap;
    }

    @Override
    public void before() {

    }

    @Override
    public <T extends ModuleComponent> EUModule<T> after() {
        return null;
    }

    @Override
    public <T extends ModuleComponent> EUModule<T> callBack() {
        return null;
    }
}
