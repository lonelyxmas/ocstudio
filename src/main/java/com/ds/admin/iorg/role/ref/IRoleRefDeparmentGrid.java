package com.ds.admin.iorg.role.ref;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Required;


@PageBar
@GridAnnotation(customService = IRoleRefDeparmentService.class, customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete})
public interface IRoleRefDeparmentGrid {


    @CustomAnnotation(pid = true, caption = "角色ID")
    public String getRoleId();

    @CustomAnnotation(caption = "描述")
    public String getBrief();

    @Pid
    public String getOrgId();

    @Required
    @CustomAnnotation(caption = "部门名称", captionField = true)
    public String getName();

    @Pid
    public String getParentId();


}
