package com.ds.admin.iorg.role;

import com.ds.admin.iorg.department.role.IAddDeparmentRolePopTree;
import com.ds.admin.iorg.department.role.IDeparmentPopTree;
import com.ds.admin.org.department.role.DeparmentPopTree;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.OrgNotFoundException;
import com.ds.org.OrgRoleType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/ref/role/")
@MethodChinaName(cname = "角色与部门关系")
public interface IRoleRefOrgService {


    @MethodChinaName(cname = "部门列表")
    @RequestMapping(method = RequestMethod.POST, value = "RefOrgs")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "部门列表")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public <M extends IRoleRefOrgGrid> ListResultModel<List<M>> getRefOrgs(String roleId, OrgRoleType roleType);


    @MethodChinaName(cname = "添加部门到角色")
    @RequestMapping(value = {"saveOrg2RolRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveOrg2RolRef(String roleId, String RoleRefOrgPopTree);


    @MethodChinaName(cname = "给角色添加部门")
    @RequestMapping(value = {"RoleRefOrgPopTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation()
    @DialogAnnotation(width = "300", height = "450")
    @ModuleAnnotation(dynLoad = true, caption = "给角色添加部门")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = {CustomMenuItem.add})
    @ResponseBody
    public <N extends IDeparmentPopTree> TreeListResultModel<List<N>> getRoleRefOrgPopTree(String roleId, String id);

    @MethodChinaName(cname = "删除角色部门关系")
    @RequestMapping(value = {"delRoleOrgRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = CustomCallBack.Reload, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delOrgRoleRef(String orgId, String roleId) throws OrgNotFoundException;

}
