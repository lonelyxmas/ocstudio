package com.ds.admin.iorg.role;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.org.OrgRoleType;
import com.ds.org.PersonRoleType;
import com.ds.org.RoleOtherType;
import com.ds.web.annotation.Caption;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation(lazyLoad = true)
public interface IRoleTree {


    @Caption
    public String getName();

    @Pid
    public RoleOtherType getRoleOtherType();

    @Uid
    public OrgRoleType getOrgRoleType();

    @Uid
    public PersonRoleType getPersonRoleType();


}
