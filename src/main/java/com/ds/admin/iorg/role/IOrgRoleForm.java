package com.ds.admin.iorg.role;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.org.RoleType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Required;
import com.ds.web.annotation.Uid;


@FormAnnotation(col = 1, customService = IOrgRoleService.class)
public interface IOrgRoleForm {


    @Pid
    public String getOrgId();

    @Pid
    public String getSysId();

    @Uid
    public String getRoleId();


    @CustomAnnotation(caption = "角色名称")
    @Required
    public String getName();
    @Required
    @CustomAnnotation(pid = true, caption = "角色类型")
    public RoleType getOrgRoleType();

    ;
}
