package com.ds.admin.iorg.role;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.web.annotation.Pid;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Add, GridMenu.Delete, GridMenu.Reload}, customService = IRoleRefOrgService.class)
public interface IRoleRefOrgGrid {

    @Pid
    public String getRoleId();

    @CustomAnnotation(caption = "描述")
    public String getBrief();

    @CustomAnnotation(uid = true, hidden = true)
    public String getOrgId();

    @CustomAnnotation(caption = "部门名称", captionField = true)
    public String getName();

    @CustomAnnotation(pid = true, hidden = true)
    public String getParentId();

}
