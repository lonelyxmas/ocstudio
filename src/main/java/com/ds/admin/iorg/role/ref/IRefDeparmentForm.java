package com.ds.admin.iorg.role.ref;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Required;


@BottomBarMenu
@FormAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public interface IRefDeparmentForm {


    @CustomAnnotation(pid = true, caption = "角色ID")
    public String getRoleId();

    @CustomAnnotation(caption = "描述")
    public String getBrief();

    @Pid
    public String getOrgId();

    @Required
    @CustomAnnotation(caption = "部门名称", captionField = true)
    public String getName();

    @Pid
    public String getParentId();


}
