package com.ds.admin.iorg.role;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.org.RoleType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {IOrgRoleAPI.class}, event = CustomGridEvent.editor)
public interface IOrgRoleGrid {


    @Pid
    public String getOrgId();

    @Pid
    public String getSysId();

    @Uid
    public String getRoleId();

    @CustomAnnotation(caption = "角色名称")
    public String getName();

    @CustomAnnotation(caption = "角色类型")
    public RoleType getOrgRoleType();
}
