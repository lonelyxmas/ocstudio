package com.ds.admin.iorg.role;

import com.ds.admin.iorg.role.person.IRoleRefPersonGrid;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.GroupItemAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

;

@Controller
@RequestMapping(path = "/admin/org/role/")
@NavGroupAnnotation(customService = IRoleService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@MethodChinaName(cname = "人员角色信息")
@BottomBarMenu
public interface IRoleNav {

    @RequestMapping(method = RequestMethod.POST, value = "RoleBaseInfo")
    @FormViewAnnotation
    @ModuleAnnotation(dock = Dock.top, caption = "角色信息")
    @UIAnnotation(height = "120")
    @ResponseBody
    public ResultModel<IRoleForm> getRoleBaseInfo(String roleId);


    @MethodChinaName(cname = "人员列表")
    @RequestMapping(method = RequestMethod.POST, value = "RefPersons")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "人员列表", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public ListResultModel<List<IRoleRefPersonGrid>> getRefPersons(String roleId, String roleType);


}
