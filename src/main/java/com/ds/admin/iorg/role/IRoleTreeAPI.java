package com.ds.admin.iorg.role;

import com.ds.config.ListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.PersonRoleType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/role/")
@MethodChinaName(cname = "人员角色树接口", imageClass = "bpmfont bpm-gongzuoliu-moxing")
public interface IRoleTreeAPI {

    @RequestMapping(method = RequestMethod.POST, value = "RoleList")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(caption = "角色管理")
    @GridViewAnnotation()
    @ResponseBody
    public <T extends IRoleGrid> ListResultModel<List<T>> getRoleList(PersonRoleType personRoleType);

    ;
}
