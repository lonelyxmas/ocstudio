package com.ds.admin.iorg.role;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.org.RoleType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Required;
import com.ds.web.annotation.Uid;


@FormAnnotation(col = 1, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = IOrgRoleService.class)
public interface IAddOrgRoleForm {



    @Pid
    public String getSysId();

    @Uid
    public String getRoleId();

    @Required
    @CustomAnnotation(caption = "角色名称")
    public String getName();
    @Required
    @CustomAnnotation(pid = true, caption = "角色类型")
    public RoleType getOrgRoleType();

    ;
}
