package com.ds.admin.iorg.role.person;

import com.ds.admin.iorg.person.role.IAddRolePersonPopTree;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/ref/role/")
@MethodChinaName(cname = "角色与人员关系")
public interface IRoleRefPersonService {


    @MethodChinaName(cname = "人员列表")
    @RequestMapping(method = RequestMethod.POST, value = "RefPersons")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "人员列表")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public <M extends IRoleRefPersonGrid> ListResultModel<List<M>> getRefPersons(String roleId, String roleType);


    @MethodChinaName(cname = "添加人员到角色")
    @RequestMapping(value = {"savePerson2RolRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> savePerson2RolRef(String roleId, String RoleRefPersonPopTree);


    @MethodChinaName(cname = "给角色添加人员")
    @RequestMapping(value = {"RoleRefPersonPopTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation()
    @DialogAnnotation(width = "300", height = "450")
    @ModuleAnnotation(dynLoad = true, caption = "给角色添加人员")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = {CustomMenuItem.add})
    @ResponseBody
    public <N extends IAddRolePersonPopTree> TreeListResultModel<List<N>> getRoleRefPersonPopTree(String roleId, String id);

    @MethodChinaName(cname = "删除角色人员关系")
    @RequestMapping(value = {"delRolePersonRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = CustomCallBack.Reload, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delPersonRoleRef(String personId, String roleId);

}
