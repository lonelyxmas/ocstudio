package com.ds.admin.iorg.role;

import com.ds.config.ListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.OrgRoleType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/role/deparment/")
@MethodChinaName(cname = "部门角色树接口", imageClass = "bpmfont bpm-gongzuoliu-moxing")
public interface IOrgRoleTreeAPI {

    @RequestMapping(method = RequestMethod.POST, value = "OrgRoleList")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @ModuleAnnotation(caption = "角色管理")
    @GridViewAnnotation()
    @ResponseBody
    public <T extends IOrgRoleGrid> ListResultModel<List<T>> getOrgRoleList(OrgRoleType orgRoleType);

}
