package com.ds.admin.iorg.role.ref;


import com.ds.admin.iorg.department.role.IDeparmentPopTree;
import com.ds.admin.iorg.role.IRolePopTree;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.OrgNotFoundException;
import com.ds.org.OrgRoleType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/ref/")
@MethodChinaName(cname = "关联关系", imageClass = "spafont spa-icon-c-databinder")
public interface IRoleRefDeparmentService {
    @MethodChinaName(cname = "部门列表")
    @RequestMapping(method = RequestMethod.POST, value = "RefRoleDeparments")
    @GridViewAnnotation(delPath = "delDeparmentRoleRef", addPath = "AddDeparment2RoleTree")
    @ModuleAnnotation(caption = "部门列表")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public <T extends IRoleRefDeparmentGrid> ListResultModel<List<T>> getRefDeparments(String roleId);

    @MethodChinaName(cname = "保存部门到角色")
    @RequestMapping(value = {"saveDeparment2RoleRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveDeparment2RoleRef(String roleId, String AddDeparment2RoleTree);


    @RequestMapping(value = {"AddDeparment2RoleTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation(saveUrl = "saveDeparment2RoleRef")
    @DialogAnnotation(width = "300", height = "450")
    @ModuleAnnotation( dynLoad = true, caption = "添加部门到角色")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = {CustomMenuItem.add})
    @ResponseBody
    public <M extends IDeparmentPopTree> TreeListResultModel<List<M>> getAddDeparment2RoleTree(String roleId, String id);

    @MethodChinaName(cname = "删除角色部门关系")
    @RequestMapping(value = {"delDeparmentRoleRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = CustomCallBack.Reload, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delDeparmentRoleRef(String orgId, String roleId) throws OrgNotFoundException;


}
