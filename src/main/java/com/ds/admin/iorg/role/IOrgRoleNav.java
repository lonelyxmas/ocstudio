package com.ds.admin.iorg.role;

import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.org.OrgRoleType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

;

@Controller
@RequestMapping(path = "/admin/org/role/")
@MethodChinaName(cname = "部门角色信息")
@NavGroupAnnotation(customService = IRoleService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@BottomBarMenu
public interface IOrgRoleNav {

    @RequestMapping(method = RequestMethod.POST, value = "OrgRoleBaseInfo")
    @FormViewAnnotation
    @ModuleAnnotation(dock = Dock.top, caption = "角色信息")
    @UIAnnotation(height = "120")
    @ResponseBody
    public <T extends IOrgRoleForm> ResultModel<T> getRoleBaseInfo(String roleId);


    @MethodChinaName(cname = "部门列表")
    @RequestMapping(method = RequestMethod.POST, value = "OrgRefOrgs")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "部门列表", dock = Dock.fill)
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public <R extends IRoleRefOrgGrid> ListResultModel<List<R>> getRefOrgs(String roleId, OrgRoleType orgRoleType);


}
