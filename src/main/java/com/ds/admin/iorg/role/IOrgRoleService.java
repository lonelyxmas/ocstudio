package com.ds.admin.iorg.role;

import com.ds.common.JDSException;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.OrgDomain;
import com.ds.esd.dsm.domain.enums.OrgDomainType;
import com.ds.org.Role;
import com.ds.org.RoleNotFoundException;
import com.ds.org.RoleType;
import com.ds.web.annotation.Aggregation;

import java.util.List;

@Aggregation(sourceClass = IOrgRoleService.class, rootClass = Role.class)
@OrgDomain(type = OrgDomainType.role)
public interface IOrgRoleService {
    @MethodChinaName(cname = "保存角色信息")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public void saveRole(IOrgRoleForm role);


    @MethodChinaName(cname = "删除角色")
    @APIEventAnnotation(callback = CustomCallBack.Reload, bindMenu = CustomMenuItem.delete)
    public void delRole(String roleId);

    @DialogAnnotation( width = "800", height = "480")
    @ModuleAnnotation(caption = "编辑角色信息")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor})
    public Role getRoleInfo(String roleId, String orgRoleType) throws RoleNotFoundException;

    @DialogAnnotation(width = "200", height = "150")
    @ModuleAnnotation(caption = "添加角色信息")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add})
    public Role addRole(RoleType orgRoleType, String sysId);


    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ModuleAnnotation(caption = "角色管理")
    public List<Role> getRoleList(RoleType orgRoleType) throws JDSException;

    ;
}
