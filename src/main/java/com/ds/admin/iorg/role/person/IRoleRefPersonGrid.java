package com.ds.admin.iorg.role.person;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.InputAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.InputType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Add, GridMenu.Delete, GridMenu.Reload}, customService = IRoleRefPersonService.class)
public interface IRoleRefPersonGrid {

    @Pid
    public String getRoleId();

    @Uid
    public String getPersonId();

    @CustomAnnotation(caption = "部门名称")
    public String getOrgName();

    @CustomAnnotation(caption = "邮箱")
    public String getEmail();

    @CustomAnnotation(caption = "账户信息")
    public String getAccount();

    @InputAnnotation(inputType = InputType.password)
    @CustomAnnotation(caption = "密码")
    public String getPassword();

    @CustomAnnotation(caption = "用户名称")
    public String getName();

    @CustomAnnotation(caption = "手机")
    public String getMobile();

}
