package com.ds.admin.iorg.role;

import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.Role;
import com.ds.org.RoleType;
import com.ds.web.annotation.Aggregation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin/org/role/")
@MethodChinaName(cname = "角色聚合根", imageClass = "bpmfont bpm-gongzuoliu-moxing")
@Aggregation(sourceClass = IRoleService.class, rootClass = Role.class)
public interface IRoleAPI {


    @MethodChinaName(cname = "保存角色信息")
    @RequestMapping(value = {"saveRole"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    <T extends IRoleForm> ResultModel<Boolean> saveRole(@RequestBody T role);


    @MethodChinaName(cname = "删除角色")
    @RequestMapping(value = {"delRole"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = CustomCallBack.Reload, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delRole(String roleId);


    @RequestMapping(method = RequestMethod.POST, value = "RoleInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(caption = "编辑角色信息")
    @DialogAnnotation(height = "480")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor})
    @ResponseBody
    public ResultModel<IRoleNav> getRoleInfo(String roleId, String personRoleType);

    @RequestMapping(method = RequestMethod.POST, value = "AddRole")
    @FormViewAnnotation
    @ModuleAnnotation(caption = "添加角色信息")
    @DialogAnnotation(width = "200", height = "150")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add})
    @ResponseBody
    public ResultModel<IAddRoleForm> addRole(RoleType personRoleType, String sysId);

    ;
}
