package com.ds.admin.iorg.role;

import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.org.RoleOtherType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/role/")
@MethodChinaName(cname = "角色管理", imageClass = "bpmfont bpm-gongzuoliu-moxing")
public interface IRoleTypeTreeAPI {


    @RequestMapping(method = RequestMethod.POST, value = "loadChild")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.loadChild})
    @ResponseBody
    public <K extends IRoleTree> TreeListResultModel<List<K>> loadChild(RoleOtherType roleOtherType);

    ;
}
