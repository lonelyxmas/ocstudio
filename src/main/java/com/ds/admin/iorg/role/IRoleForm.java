package com.ds.admin.iorg.role;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.org.RoleType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Readonly;
import com.ds.web.annotation.Required;
import com.ds.web.annotation.Uid;


@FormAnnotation(col = 1, customService = IRoleService.class)
public interface IRoleForm {


    @Pid
    public String getOrgId();

    @Pid
    public String getSysId();

    @Uid
    public String getRoleId();

    @Pid
    public String getPersonId();


    @CustomAnnotation(caption = "角色名称")
    @Required
    public String getName();

    @Readonly
    @CustomAnnotation(caption = "角色类型")
    public RoleType getPersonRoleType();


    ;
}
