package com.ds.admin.iorg.department.view;

import com.ds.admin.iorg.department.IDeparmentAPI;
import com.ds.admin.iorg.person.IPersonTreeAPI;
import com.ds.esd.custom.annotation.ComboPopAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.TextEditorAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation(customService = {IDeparmentAPI.class}, col = 1)
public interface IDeparmentForm {


    @Required
    @CustomAnnotation(caption = "部门名称", captionField = true)
    public String getName();


    @ComboPopAnnotation(bindClass = IPersonTreeAPI.class)
    @CustomAnnotation(caption = "部门负责人")
    public String getLeaderId();


    @TextEditorAnnotation()
    @FieldAnnotation(rowHeight = "100")
    @CustomAnnotation(caption = "描述")
    public String getBrief();



    @CustomAnnotation(uid = true, hidden = true)
    public String getOrgId();


    @CustomAnnotation(pid = true, hidden = true)
    public String getParentId();


}
