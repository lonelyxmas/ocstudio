package com.ds.admin.iorg.department;


import com.ds.admin.iorg.department.role.IDeparmentRoleRefTabs;
import com.ds.admin.iorg.department.view.IDeparmentForm;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/admin/org/deparment/")
@MethodChinaName(cname = "部门详细信息", imageClass = "bpmfont bpmgongzuoliuzuhuzicaidan")
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@BottomBarMenu
public interface IDeparmentNav {
    @RequestMapping(method = RequestMethod.POST, value = "DeparmentBaseInfo")
    @FormViewAnnotation()
    @ModuleAnnotation(caption = "部门信息", dock = Dock.top)
    @UIAnnotation(height = "150")
    @ResponseBody
    public <R extends IDeparmentForm> ResultModel<R> getDeparmentBaseInfo(String orgId);

    @RequestMapping(method = RequestMethod.POST, value = "OrgRoles")
    @ModuleAnnotation(caption = "部门角色", dynLoad = true, dock = Dock.fill)
    @NavTabsViewAnnotation()
    @ResponseBody
    public <T extends IDeparmentRoleRefTabs> TreeListResultModel<List<T>> getOrgRoles(String id, String orgId);
}
