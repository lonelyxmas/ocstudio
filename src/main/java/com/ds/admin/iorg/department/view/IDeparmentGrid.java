package com.ds.admin.iorg.department.view;

import com.ds.admin.iorg.department.IDeparmentAPI;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;


@PageBar
@GridAnnotation(event = CustomGridEvent.editor,
        customService = IDeparmentAPI.class,
        customMenu = {GridMenu.Add, GridMenu.Delete, GridMenu.Reload})
public interface IDeparmentGrid {

    @CustomAnnotation(caption = "描述")
    public String getBrief();

    @CustomAnnotation(uid = true, hidden = true)
    public String getOrgId();

    @CustomAnnotation(caption = "部门名称", captionField = true)
    public String getName();

    @CustomAnnotation(pid = true, hidden = true)
    public String getParentId();


}
