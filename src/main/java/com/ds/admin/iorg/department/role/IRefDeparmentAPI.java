package com.ds.admin.iorg.department.role;


import com.ds.admin.iorg.role.IRolePopTree;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.OrgRoleType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/admin/org/ref/deparment/")
@MethodChinaName(cname = "关联关系", imageClass = "spafont spa-icon-c-databinder")
public interface IRefDeparmentAPI {


    @RequestMapping(method = RequestMethod.POST, value = "loadChildOrgRefOrg")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.loadChild})
    @ResponseBody
    public <T extends IRolePopTree> TreeListResultModel<List<T>> loadChildOrg(String personId, OrgRoleType orgRoleType);


    @RequestMapping(method = RequestMethod.POST, value = "RefOrgRoles")
    @GridViewAnnotation(delPath = "delDeparmentRoleRef", addPath = "AddRole2DeparmentTree")
    @ModuleAnnotation(caption = "角色列表")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.tabEditor)
    @ResponseBody
    public <K extends IRefDeparmentRoleGrid> ListResultModel<List<K>> getRefOrgRoles(String orgId, String roleType);


}
