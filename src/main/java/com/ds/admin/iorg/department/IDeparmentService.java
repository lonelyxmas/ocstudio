package com.ds.admin.iorg.department;

import com.ds.common.JDSException;
import com.ds.common.org.CtOrg;
import com.ds.config.ListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.dsm.domain.annotation.OrgDomain;
import com.ds.esd.dsm.domain.enums.OrgDomainType;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.Org;
import com.ds.org.OrgNotFoundException;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Aggregation(sourceClass = IDeparmentService.class, rootClass = Org.class)
@OrgDomain(type = OrgDomainType.deparment)
public interface IDeparmentService {

    @ModuleAnnotation(caption = "查找部门")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.search})
    @ResponseBody
    public ListResultModel<List<Org>> findOrgs(String patten) throws JDSException ;


    @MethodChinaName(cname = "部门信息")
    @DialogAnnotation( width = "600", height = "480")
    @ModuleAnnotation(caption = "编辑部门信息")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
    @ResponseBody
    public Org getOrgInfo(String orgId) throws OrgNotFoundException ;
    @MethodChinaName(cname = "添加部门")
    @DialogAnnotation( width = "600", height = "480")
    @ModuleAnnotation(caption = "添加部门")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add}, autoRun = true)
    public Org addDeparment(String parentId) ;


    @MethodChinaName(cname = "保存机构信息")
    @RequestMapping(value = {"saveOrg"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save})
    public void saveOrg(CtOrg org) throws JDSException ;

    @ModuleAnnotation(caption = "子部门列表")
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.reload})
    public List<Org> getChildrenOrgList(String parentId) throws JDSException, OrgNotFoundException ;

    @MethodChinaName(cname = "删除部门")
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent}, bindMenu = {CustomMenuItem.delete})
    public void delOrg(String orgId);

}
