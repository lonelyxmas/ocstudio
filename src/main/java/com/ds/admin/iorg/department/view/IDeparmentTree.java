package com.ds.admin.iorg.department.view;

import com.ds.admin.iorg.department.IDeparmentAPI;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.web.annotation.Caption;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@TreeAnnotation(lazyLoad = true, customService = IDeparmentAPI.class)
public interface IDeparmentTree {

    @Caption
    public String getName();

    @Pid
    public String getParentId();

    @Uid
    public String getOrgId();

    ;
}
