package com.ds.admin.iorg.department.child;

import com.ds.admin.iorg.department.IDeparmentService;
import com.ds.admin.iorg.department.view.IDeparmentGrid;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.Org;
import com.ds.web.annotation.Aggregation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/deparment/")
@MethodChinaName(cname = "部门导航树服务", imageClass = "bpmfont bpmgongzuoliuzuhuzicaidan")
@Aggregation(sourceClass = IDeparmentService.class, rootClass = Org.class)
public interface IChildDeparmentTreeAPI {

    @RequestMapping(method = RequestMethod.POST, value = "ChildOrgs")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "子部门列表")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.treeNodeEditor})
    @ResponseBody
    public <T extends IDeparmentGrid> ListResultModel<List<T>> getChildrenOrgList(String parentId);


    @RequestMapping(method = RequestMethod.POST, value = "loadTop")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.loadTops})
    @ResponseBody
    public <T extends IChildDeparmentTree> TreeListResultModel<List<T>> loadTop();

    @RequestMapping(method = RequestMethod.POST, value = "loadChildDeparment")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.loadChild})
    @ResponseBody
    public <T extends IChildDeparmentTree> TreeListResultModel<List<T>> loadChildDeparment(String orgId);

    ;
}
