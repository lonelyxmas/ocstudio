package com.ds.admin.iorg.department.child;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.web.annotation.Caption;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@TreeAnnotation(customService = IChildDeparmentTreeAPI.class, lazyLoad = true)
public interface IChildDeparmentTree {

    @Caption
    public String getName();

    @Pid
    public String getParentId();

    @Uid
    public String getOrgId();

    ;
}
