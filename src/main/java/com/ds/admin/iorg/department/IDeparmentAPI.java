package com.ds.admin.iorg.department;

import com.ds.admin.iorg.department.view.IAddDeparmentForm;
import com.ds.admin.iorg.department.view.IDeparmentTree;
import com.ds.admin.iorg.person.view.IPersonGrid;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.Org;
import com.ds.web.annotation.Aggregation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/deparment/")
@MethodChinaName(cname = "部门聚合根服务", imageClass = "bpmfont bpmgongzuoliuzuhuzicaidan")
@Aggregation(sourceClass = IDeparmentService.class, rootClass = Org.class)
public interface IDeparmentAPI<M extends IAddDeparmentForm> {
    @RequestMapping(method = RequestMethod.POST, value = "Persons")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "人员列表")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.treeNodeEditor})
    @ResponseBody
    public <K extends IPersonGrid> ListResultModel<List<K>> getPersons(String orgId);

    @RequestMapping(method = RequestMethod.POST, value = "loadChild")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.loadChild})
    @ResponseBody
    public <T extends IDeparmentTree> ListResultModel<List<T>> loadChild(String parentId);

    @MethodChinaName(cname = "部门信息")
    @RequestMapping(method = RequestMethod.POST, value = "DeparmentInfo")
    @NavGroupViewAnnotation()
    @DialogAnnotation(width = "600", height = "520")
    @ModuleAnnotation(caption = "编辑部门信息")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
    @ResponseBody
    public <K extends IDeparmentNav> ResultModel<K> getDeparmentInfo(String orgId);

    @MethodChinaName(cname = "添加部门")
    @RequestMapping(method = RequestMethod.POST, value = "addDeparment")
    @FormViewAnnotation()
    @DialogAnnotation(width = "350", height = "260")
    @ModuleAnnotation(caption = "添加部门")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add}, autoRun = true)
    @ResponseBody
    public ResultModel<M> addDeparment(String parentId);

    @MethodChinaName(cname = "保存机构信息")
    @RequestMapping(value = {"saveOrg"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.save})
    public @ResponseBody
    ResultModel<Boolean> saveOrg(@RequestBody M deparmentForm);

    @MethodChinaName(cname = "删除部门")
    @RequestMapping(value = {"delOrg"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent}, bindMenu = {CustomMenuItem.delete})
    public @ResponseBody
    ResultModel<Boolean> delOrg(String orgId);

    ;
}
