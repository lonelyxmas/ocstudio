package com.ds.admin.iorg.department.role;

import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Caption;
import com.ds.web.annotation.Pid;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, lazyLoad = true, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, bindService = IDeparmentRefRoleService.class)
@PopTreeAnnotation(caption = "添加角色")
public interface IAddDeparmentRolePopTree {


    @Pid
    public String getOrgId();

    @Caption
    public String getName();

    @Pid
    public String getRoleId();

}
