package com.ds.admin.iorg.department.role;

import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.org.OrgRoleType;
import com.ds.org.RoleType;
import com.ds.web.annotation.Pid;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = IDeparmentRefRoleService.class)
public interface IRefDeparmentRoleGrid {

    @CustomAnnotation(pid = true, hidden = true)
    public String getOrgId();

    @CustomAnnotation(uid = true, hidden = true)
    public String getRoleId();


    @Pid
    @CustomAnnotation(uid = true, hidden = true)
    OrgRoleType getOrgRoleType();

    @CustomAnnotation(caption = "角色名称")
    public String getName();

    @CustomAnnotation(caption = "角色类型")
    public RoleType getRoleType();

}
