package com.ds.admin.iorg.department.role;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.org.OrgRoleType;
import com.ds.web.annotation.Caption;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@TabsAnnotation(bindService = IRefDeparmentAPI.class)
@TreeAnnotation()
public interface IDeparmentRoleRefTabs {

    @Uid
    public String getRoleId();

    @Caption
    public String getName();

    @Pid
    public String getOrgId();

    @Pid
    public OrgRoleType getOrgRoleType();


}
