package com.ds.admin.iorg.department.role;

import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.OrgRoleType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/ref/deparment/")
@MethodChinaName(cname = "角色与部门关系")
public interface IDeparmentRefRoleService {


    @MethodChinaName(cname = "角色列表")
    @RequestMapping(method = RequestMethod.POST, value = "RefRoles")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "角色列表")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public <M extends IRefDeparmentRoleGrid> ListResultModel<List<M>> getRefRoles(String orgId, OrgRoleType roleType);



    @MethodChinaName(cname = "添加角色到部门")
    @RequestMapping(value = {"saveRol2OrgRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveRol2OrgRef(String roleId, String RoleRefOrgPopTree);




    @MethodChinaName(cname = "给部门添加角色")
    @RequestMapping(value = {"OrgRefRolePopTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation()
    @DialogAnnotation(width = "300", height = "450")
    @ModuleAnnotation(dynLoad = true, caption = "给部门添加角色")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = {CustomMenuItem.add})
    @ResponseBody
    public <N extends IAddDeparmentRolePopTree> TreeListResultModel<List<N>> getOrgRefRolePopTree(String orgId, OrgRoleType orgRoleType);

    @MethodChinaName(cname = "删除角色部门关系")
    @RequestMapping(value = {"delRoleOrgRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = CustomCallBack.Reload, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delOrgRoleRef(String orgId, String roleId);

}
