package com.ds.admin.iorg.department.role;

import com.ds.admin.iorg.role.IRoleRefOrgService;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Caption;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox,
        lazyLoad = true, bindService = IRoleRefOrgService.class,
        bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@PopTreeAnnotation(caption = "角色添加部门")
public interface IDeparmentPopTree {

    @Uid
    public String getPersonId();

    @Pid
    public String getOrgId();

    @Pid
    public String getRoleId();

    @Caption
    public String getName();


}
