package com.ds.admin.iorg.person.role;

import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.OrgDomain;
import com.ds.esd.dsm.domain.enums.OrgDomainType;
import com.ds.org.Person;
import com.ds.org.PersonRoleType;
import com.ds.web.annotation.Aggregation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Aggregation(
        sourceClass = IPersonRoleAPI.class,
        rootClass = Person.class)
@OrgDomain(type = OrgDomainType.person)
@RequestMapping("/admin/org/ref/")
public interface IPersonRoleAPI {


    @RequestMapping(method = RequestMethod.POST, value = "loadChildPersonRefOrg")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.loadChild})
    @ResponseBody
    public <T extends IAddRolePersonPopTree> TreeListResultModel<List<IAddRolePersonPopTree>> loadChildOrg(String orgId, PersonRoleType personRoleType);

    @RequestMapping(method = RequestMethod.POST, value = "TabRefPersonRoles")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "角色列表")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.tabEditor)
    @ResponseBody
    public <R extends IPersonRefRoleGrid>ListResultModel<List<IPersonRefRoleGrid>> getTabRefPersonRoles(String personId, PersonRoleType personRoleType);

    ;
}
