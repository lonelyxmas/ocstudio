package com.ds.admin.iorg.person.view;

import com.ds.admin.iorg.person.IPersonTreeAPI;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Caption;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@BottomBarMenu
@TreeAnnotation(lazyLoad = true, bindService = IPersonTreeAPI.class, selMode = SelModeType.singlecheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public interface IPersonTree {

    @Caption
    public String getName();

    @Pid
    public String getPersonId();

    @Pid
    public String getParentId();

    @Uid
    public String getOrgId();

    ;
}
