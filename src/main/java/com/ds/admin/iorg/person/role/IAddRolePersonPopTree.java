package com.ds.admin.iorg.person.role;

import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.annotation.Caption;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, lazyLoad = true, caption = "添加角色", bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@PopTreeAnnotation()
public interface IAddRolePersonPopTree {


    @Uid
    public String getPersonId();

    @Pid
    public String getOrgId();

    @Caption
    public String getName();

    @Pid
    public String getRoleId();

}
