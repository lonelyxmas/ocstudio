package com.ds.admin.iorg.person.view;

import com.ds.admin.iorg.person.IPersonAPI;
import com.ds.esd.custom.annotation.ComboNumberAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.InputAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.enums.InputType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Required;
import com.ds.web.annotation.Uid;

@FormAnnotation(customService = IPersonAPI.class, col = 2)
public interface IPersonForm {

    @Uid
    public String getPersonId();

    @Pid
    public String getRoleId();

    @Pid
    public String getOrgId();

    @CustomAnnotation(caption = "部门名称")
    public String getOrgName();


    @CustomAnnotation(caption = "邮箱")
    public String getEmail();

    @Required
    @CustomAnnotation(caption = "账户信息")
    public String getAccount();

    @Required
    @InputAnnotation(inputType = InputType.password)
    @CustomAnnotation(caption = "密码")
    public String getPassword();

    @Required
    @CustomAnnotation(caption = "用户名")
    public String getName();

    @ComboNumberAnnotation
    @CustomAnnotation(caption = "手机")
    public String getMobile();


}
