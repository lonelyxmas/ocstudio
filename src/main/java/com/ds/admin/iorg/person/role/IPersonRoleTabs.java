package com.ds.admin.iorg.person.role;

import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.org.PersonRoleType;
import com.ds.org.RoleType;
import com.ds.web.annotation.Caption;
import com.ds.web.annotation.Pid;

@TabsAnnotation(bindService = IPersonRoleAPI.class)
public interface IPersonRoleTabs {

    @Caption
    public String getName();

    @Pid
    PersonRoleType getPersonRoleType();

    @Pid
    public String getPersonId();

    @Pid
    public RoleType getRoleType();


}
