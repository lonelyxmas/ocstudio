package com.ds.admin.iorg.person;

import com.ds.admin.iorg.person.view.IPersonTree;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/person/")
@MethodChinaName(cname = "人员选择树", imageClass = "spafont spa-icon-login")
public interface IPersonTreeAPI {

    @RequestMapping(method = RequestMethod.POST, value = "loadTop")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.loadTops})
    @ResponseBody
    public <T extends IPersonTree> TreeListResultModel<List<T>> loadTop();

    @RequestMapping(method = RequestMethod.POST, value = "loadChildDeparment")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.loadChild})
    @ResponseBody
    public <T extends IPersonTree> TreeListResultModel<List<T>> loadChildDeparment(String parentId);


    @MethodChinaName(cname = "选择部门领导")
    @RequestMapping(value = {"PersonTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation()
    @DialogAnnotation(width = "300", height = "450")
    @ModuleAnnotation(dynLoad = true, caption = "选择部门领导")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.fieldNodeEditor)
    @ResponseBody
    public <N extends IPersonTree> TreeListResultModel<List<N>> getPersonTree(String orgId);


    @RequestMapping(value = {"updateLeader"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.save, callback = CustomCallBack.Close)
    @ResponseBody
    public ResultModel<Boolean> updateLeader();


}
