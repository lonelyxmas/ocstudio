package com.ds.admin.iorg.person.view;

import com.ds.admin.iorg.person.IPersonAPI;
import com.ds.esd.custom.annotation.ComboNumberAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.InputAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.InputType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@PageBar
@GridAnnotation(event = CustomGridEvent.editor,
        customService = IPersonAPI.class,
        customMenu = {GridMenu.Add, GridMenu.Delete, GridMenu.Reload})
public interface IPersonGrid {
    @Uid
    public String getPersonId();
   @Pid
    public String getRoleId();
    @CustomAnnotation(pid = true, hidden = true)
    public String getOrgId();

    @CustomAnnotation(caption = "邮箱")
    public String getEmail();

    @CustomAnnotation(caption = "账户信息")
    public String getAccount();

    @InputAnnotation(inputType = InputType.password)
    @CustomAnnotation(caption = "密码")
    public String getPassword();

    @CustomAnnotation(pid = true, hidden = true)
    public String getName();

    @ComboNumberAnnotation
    @CustomAnnotation(caption = "手机")
    public String getMobile();

    @CustomAnnotation(caption = "部门名称")
    public String getOrgName();


}
