package com.ds.admin.iorg.person.role;

import com.ds.admin.iorg.role.person.IRoleRefPersonService;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.org.PersonRoleType;
import com.ds.org.RoleType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = IPersonRefRoleService.class)
public interface IPersonRefRoleGrid {


    @CustomAnnotation(caption = "角色名称")
    public String getName();

    @CustomAnnotation(caption = "角色类型", pid = true)
    public RoleType getRoleType();

    @CustomAnnotation(caption = "角色类型", pid = true)
    public PersonRoleType getPersonRoleType();

    @Uid
    public String getRoleId();

    @Pid
    public String getPersonId();


}
