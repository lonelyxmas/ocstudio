package com.ds.admin.iorg.person.role;

import com.ds.admin.iorg.role.IRolePopTree;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.PersonRoleType;
import com.ds.org.RoleType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/person/")
@MethodChinaName(cname = "人员角色关系", imageClass = "bpmfont bpm-gongzuoliu-moxing")
public interface IPersonRefRoleService {


    @RequestMapping(method = RequestMethod.POST, value = "loadRoleChild")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.loadChild})
    @ResponseBody
    public <T extends IRolePopTree> TreeListResultModel<List<IRolePopTree>> loadRoleChild(String personId, PersonRoleType personRoleType);


    @RequestMapping(method = RequestMethod.POST, value = "RefPersonRoles")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "角色列表")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.reload)
    @ResponseBody
    public <K extends IPersonRefRoleGrid> ListResultModel<List<K>> getRefPersonRoles(String personId, RoleType roleType);


    @RequestMapping(value = {"PersonRolePopTree"}, method = {RequestMethod.GET, RequestMethod.POST})
    @PopTreeViewAnnotation()
    @DialogAnnotation(width = "300", height = "450")
    @ModuleAnnotation(dynLoad = true, caption = "添加角色到人员")
    @APIEventAnnotation(isAllform = true, autoRun = true, bindMenu = {CustomMenuItem.add})
    @ResponseBody
    public <R extends IRolePopTree> TreeListResultModel<List<R>> getRolePersonPopTree(String personId, PersonRoleType personRoleType, String id);


    @MethodChinaName(cname = "添加角色到人员")
    @RequestMapping(value = {"saveRol2PersonRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> saveRol2PersonRef(String personId, String PersonRolePopTree);


    @MethodChinaName(cname = "删除角色人员关系")
    @RequestMapping(value = {"delPersonRoleRef"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = CustomCallBack.Reload, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delPersonRoleRef(String personId, String roleId);


}
