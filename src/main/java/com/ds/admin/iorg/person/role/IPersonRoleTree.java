package com.ds.admin.iorg.person.role;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.org.PersonRoleType;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@TreeAnnotation()
public interface IPersonRoleTree {

    @Pid
    String getPersonId();

    @Pid
    PersonRoleType getPersonRoleType();

    @Uid
    String getRoleId();

}
