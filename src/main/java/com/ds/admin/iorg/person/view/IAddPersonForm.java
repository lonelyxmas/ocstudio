package com.ds.admin.iorg.person.view;

import com.ds.admin.iorg.person.IPersonAPI;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;

@BottomBarMenu
@FormAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = IPersonAPI.class, col = 1)
public interface IAddPersonForm extends IPersonForm {


}
