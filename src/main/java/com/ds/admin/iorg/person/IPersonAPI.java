package com.ds.admin.iorg.person;

import com.ds.admin.iorg.person.view.IAddPersonForm;
import com.ds.admin.iorg.person.view.IPersonForm;
import com.ds.admin.iorg.person.view.IPersonGrid;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/person/")
@MethodChinaName(cname = "人员聚合根操作", imageClass = "spafont spa-icon-login")
public interface IPersonAPI {

    @RequestMapping(method = RequestMethod.POST, value = "Persons")
    @GridViewAnnotation()
    @ModuleAnnotation(caption = "人员列表")
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.reload})
    @ResponseBody
    public <T extends IPersonGrid> ListResultModel<List<T>> getPersons(String orgId);

    @RequestMapping(method = RequestMethod.POST, value = "PersonInfo")
    @NavGroupViewAnnotation()
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
    @DialogAnnotation(caption = "编辑人员信息", height = "550")
    @ModuleAnnotation()
    @ResponseBody
    public <M extends IPersonNav> ResultModel<M> getPersonInfo(String personId);


    @RequestMapping(method = RequestMethod.POST, value = "AddPersonView")
    @FormViewAnnotation
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add}, autoRun = true)
    @DialogAnnotation(width = "370", height = "260", caption = "添加人员信息")
    @ModuleAnnotation()
    @ResponseBody
    public <T extends IAddPersonForm> ResultModel<T> AddPerson(String orgId);


    @MethodChinaName(cname = "保存成员信息")
    @RequestMapping(value = {"savePerson"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    <R extends IPersonForm> ResultModel<Boolean> savePerson(@RequestBody R person);

    @MethodChinaName(cname = "删除人员")
    @RequestMapping(value = {"delPerson"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload}, bindMenu = CustomMenuItem.delete)
    public @ResponseBody
    ResultModel<Boolean> delPerson(String personId);

}
