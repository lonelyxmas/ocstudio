package com.ds.admin.iorg.person;

import com.ds.admin.iorg.person.role.IPersonRoleTabs;
import com.ds.admin.iorg.person.view.IPersonForm;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.nav.NavTabsViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/admin/org/person/")
@MethodChinaName(cname = "人员详细信息", imageClass = "spafont spa-icon-login")
@BottomBarMenu
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = IPersonAPI.class)
public interface IPersonNav {


    @RequestMapping(method = RequestMethod.POST, value = "PersonBaseInfo")
    @FormViewAnnotation
    @ModuleAnnotation(caption = "用户信息", dock = Dock.top)
    @UIAnnotation(height = "160")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public <T extends IPersonForm> ResultModel<IPersonForm> getPersonBaseInfo(String personId);


    @MethodChinaName(cname = "用户角色")
    @RequestMapping(method = RequestMethod.POST, value = "PersonRoles")
    @ModuleAnnotation(dynLoad = true, caption = "用户角色", dock = Dock.fill)
    @NavTabsViewAnnotation()
    @ResponseBody
    public <T extends IPersonRoleTabs> TreeListResultModel<List<T>> getPersonRoles(String personId);
}
