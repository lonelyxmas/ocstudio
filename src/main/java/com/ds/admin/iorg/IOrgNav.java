package com.ds.admin.iorg;

import com.ds.admin.iorg.department.child.IChildDeparmentTree;
import com.ds.admin.iorg.department.view.IDeparmentTree;
import com.ds.admin.iorg.role.IRoleTree;
import com.ds.config.TreeListResultModel;
import com.ds.esd.custom.annotation.TreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutItemAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.PosType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/org/")
@LayoutAnnotation(transparent = false, items = {@LayoutItemAnnotation(size = 260, pos = PosType.before, locked = true, moveDisplay = false, cmd = false)})
public interface IOrgNav {


    @RequestMapping(method = RequestMethod.POST, value = "PersonManager")
    @APIEventAnnotation(autoRun = true)
    @TreeViewAnnotation()
    @ModuleAnnotation(caption = "人员管理", imageClass = "bpmfont bpmgongzuoliu")
    @ResponseBody
    public <K extends IDeparmentTree> TreeListResultModel<List<K>> getPersonManager(String id);

    @RequestMapping(method = RequestMethod.POST, value = "DepartmentManager")
    @APIEventAnnotation()
    @TreeViewAnnotation
    @ModuleAnnotation(caption = "部门管理", imageClass = "bpmfont bpm-gongzuoliu-moxing")
    @ResponseBody
    public <T extends IChildDeparmentTree> TreeListResultModel<List<T>> getDepartmentManager(String id);


    @RequestMapping(method = RequestMethod.POST, value = "RoleManager")
    @APIEventAnnotation()
    @TreeViewAnnotation
    @ModuleAnnotation(caption = "角色管理", imageClass = "bpmfont bpmbinghangkaishi")
    @ResponseBody
    public <M extends IRoleTree> TreeListResultModel<List<M>> getRoleManager(String id);

    ;
}
