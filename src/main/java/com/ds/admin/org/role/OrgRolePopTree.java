package com.ds.admin.org.role;

import com.ds.admin.iorg.role.IOrgRolePopTree;
import com.ds.admin.iorg.role.IRolePopTree;
import com.ds.admin.org.person.role.PersonRefRoleService;
import com.ds.admin.org.role.ref.RoleRefDeparmentService;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.org.OrgRoleType;
import com.ds.org.PersonRoleType;
import com.ds.org.Role;

@EsbBeanAnnotation
public class OrgRolePopTree extends TreeListItem implements IOrgRolePopTree {

    String personId;

    String orgId;

    PersonRoleType personRoleType;

    OrgRoleType orgRoleType;

    String roleId;

    @TreeItemAnnotation(bindService = RoleRefDeparmentService.class, lazyLoad = true, dynDestory = true)
    public OrgRolePopTree(OrgRoleType orgRoleType, String orgId) {
        this.caption = orgRoleType.getName();
        this.orgRoleType = orgRoleType;
        this.roleId = orgRoleType.getType();
        this.orgId = orgId;
        this.imageClass = orgRoleType.getImageClass();
    }


    @TreeItemAnnotation(bindService = PersonRefRoleService.class, lazyLoad = true, dynDestory = true)
    public OrgRolePopTree(PersonRoleType personRoleType, String personId) {
        this.caption = personRoleType.getName();
        this.personRoleType = personRoleType;
        this.roleId = personRoleType.getType();
        this.personId = personId;
        this.imageClass = personRoleType.getImageClass();
    }

    @TreeItemAnnotation()
    public OrgRolePopTree(Role role, String personId) {
        this.caption = role.getName();
        this.roleId = role.getRoleId();
        this.personId = personId;
        this.imageClass = role.getType().getImageClass();
        this.personRoleType = PersonRoleType.valueOf(role.getType().name());

    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public OrgRoleType getOrgRoleType() {
        return orgRoleType;
    }

    public void setOrgRoleType(OrgRoleType orgRoleType) {
        this.orgRoleType = orgRoleType;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public PersonRoleType getPersonRoleType() {
        return personRoleType;
    }

    public void setPersonRoleType(PersonRoleType personRoleType) {
        this.personRoleType = personRoleType;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
}
