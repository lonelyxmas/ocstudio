package com.ds.admin.org.role.person;

import com.ds.admin.iorg.role.person.IRoleRefPersonService;
import com.ds.admin.org.person.role.AddRolePersonPopTree;
import com.ds.admin.org.role.RolePopTree;
import com.ds.common.JDSException;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.*;
import com.ds.server.OrgManagerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@EsbBeanAnnotation()
public class RoleRefPersonService implements IRoleRefPersonService {


    @Override
    public ListResultModel<List<RoleRefPersonGrid>> getRefPersons(String roleId, String roleType) {
        ListResultModel<List<RoleRefPersonGrid>> resultModel = new ListResultModel<List<RoleRefPersonGrid>>();
        List<RoleRefPersonGrid> roleViews = new ArrayList<>();
        try {

            Role role = this.getOrgManager().getRoleByID(roleId);

            List<Person> personList = role.getPersonList();
            for (Person person : personList) {
                if (role.getType().equals(RoleType.valueOf(roleType))) {
                    RoleRefPersonGrid view = new RoleRefPersonGrid(role, person);
                    roleViews.add(view);
                }
            }
        } catch (JDSException e) {
            e.printStackTrace();
        } catch (RoleNotFoundException e) {
            e.printStackTrace();
        }
        resultModel.setData(roleViews);
        return resultModel;
    }


    @Override
    public ResultModel<Boolean> savePerson2RolRef(String roleId, String RoleRefPersonPopTree) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] personIds = new String[]{};
        if (RoleRefPersonPopTree != null) {
            personIds = StringUtility.split(RoleRefPersonPopTree, ";");
            for (String personId : personIds) {
                try {
                    Role role = this.getOrgManager().getRoleByID(roleId);
                    CtOrgAdminManager.getInstance().addPerson2Role(personId, role);
                } catch (PersonNotFoundException e) {
                    e.printStackTrace();
                } catch (RoleNotFoundException e) {
                    e.printStackTrace();
                } catch (JDSException e) {
                    e.printStackTrace();
                }
            }
        }
        return userStatusInfo;

    }

    @Override
    public TreeListResultModel<List<AddRolePersonPopTree>> getRoleRefPersonPopTree(String roleId, String id) {
        TreeListResultModel<List<AddRolePersonPopTree>> resultModel = new TreeListResultModel<List<AddRolePersonPopTree>>();
        List<AddRolePersonPopTree> items = new ArrayList<>();
        try {
            List<Org> orgs = this.getOrgManager().getTopOrgs(JDSActionContext.getActionContext().getSystemCode());
            resultModel = TreePageUtil.getDefaultTreeList(orgs, AddRolePersonPopTree.class);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorListResultModel<List<AddRolePersonPopTree>>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }

    @Override

    public ResultModel<Boolean> delPersonRoleRef(String personId, String roleId) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] roleIds = StringUtility.split(roleId, ";");
        String[] personIds = StringUtility.split(personId, ";");

        for (String id : roleIds) {
            try {
                Role role = this.getOrgManager().getRoleByID(id);
                for (String subPersonId : personIds) {
                    if (subPersonId != null) {
                        CtOrgAdminManager.getInstance().removePerson2Role(subPersonId, role);
                    }
                }

            } catch (PersonNotFoundException e) {
                e.printStackTrace();
            } catch (RoleNotFoundException e) {
                e.printStackTrace();
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }

        return userStatusInfo;

    }


    public ESDClient getClient() throws JDSException {
        ESDClient client = ESDFacrory.getESDClient();
        return client;
    }

    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.getClient().getConfigCode());
        return orgManager;
    }
}
