package com.ds.admin.org.role;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.iorg.role.IRoleTreeAPI;
import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.OrgManager;
import com.ds.org.PersonRoleType;
import com.ds.org.Role;
import com.ds.server.OrgManagerFactory;
import com.ds.web.util.PageUtil;

import java.util.ArrayList;
import java.util.List;

@EsbBeanAnnotation
public class RoleTreeAPI implements IRoleTreeAPI {

    @Override
    public ListResultModel<List<RoleGrid>> getRoleList(PersonRoleType personRoleType) {
        ListResultModel<List<RoleGrid>> resultModel = new ListResultModel<List<RoleGrid>>();
        try {
            List<Role> roleList = new ArrayList<>();
            List<Role> roles = getOrgManager().getAllRoles();
            for (Role role : roles) {
                if (role.getType().getType().equals(personRoleType.getType())) {
                    roleList.add(role);
                }
            }
            resultModel = PageUtil.getDefaultPageList(roleList, RoleGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorListResultModel<List<RoleGrid>>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }


    @JSONField(serialize = false)
    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager();
        return orgManager;
    }
}
