package com.ds.admin.org.role;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.iorg.role.IRoleForm;
import com.ds.admin.iorg.role.IRoleNav;
import com.ds.admin.iorg.role.person.IRoleRefPersonGrid;
import com.ds.admin.org.role.person.RoleRefPersonGrid;
import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.*;
import com.ds.server.OrgManagerFactory;

import java.util.ArrayList;
import java.util.List;

@EsbBeanAnnotation()
public class RoleNav implements IRoleNav {


    String roleId;

    String sysId;

    String personId;

    String orgId;

    public RoleNav() {

    }

    public RoleNav(RoleNav nav) {
        this.roleId = nav.getRoleId();
        this.personId = nav.getPersonId();
        this.orgId = nav.getOrgId();
        this.sysId = nav.getSysId();
    }


    public ResultModel<IRoleForm> getRoleBaseInfo(String roleId) {
        ResultModel<IRoleForm> result = new ResultModel<IRoleForm>();
        return result;
    }


    public ListResultModel<List<IRoleRefPersonGrid>> getRefPersons(String roleId, String roleType) {
        ListResultModel<List<IRoleRefPersonGrid>> resultModel = new ListResultModel<List<IRoleRefPersonGrid>>();
        List<IRoleRefPersonGrid> roleViews = new ArrayList<>();
        try {

            Role role = this.getOrgManager().getRoleByID(roleId);

            List<Person> personList = role.getPersonList();
            for (Person person : personList) {
                if (role.getType().equals(RoleType.valueOf(roleType))) {
                    IRoleRefPersonGrid view = new RoleRefPersonGrid(role, person);
                    roleViews.add(view);
                }
            }
        } catch (JDSException e) {
            e.printStackTrace();
        } catch (RoleNotFoundException e) {
            e.printStackTrace();
        }
        resultModel.setData(roleViews);
        return resultModel;
    }

    @JSONField(serialize = false)
    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager();
        return orgManager;
    }


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getRoleId() {
        return roleId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }


}
