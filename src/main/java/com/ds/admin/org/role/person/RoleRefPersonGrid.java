package com.ds.admin.org.role.person;

import com.ds.admin.iorg.role.person.IRoleRefPersonGrid;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Person;
import com.ds.org.Role;

@EsbBeanAnnotation
public class RoleRefPersonGrid implements IRoleRefPersonGrid {


    String roleId;

    String personId;

    String name;

    String account;

    String email;

    String password;

    String mobile;

    String orgName;

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public RoleRefPersonGrid(Role role, Person person) {
        this.personId = person.getID();

        this.name = person.getName();
        this.account = person.getAccount();
        this.password = person.getPassword();
        this.mobile = person.getMobile();
        this.email = person.getEmail();
        this.roleId = role.getRoleId();

    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

}
