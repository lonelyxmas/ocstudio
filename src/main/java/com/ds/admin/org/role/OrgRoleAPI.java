package com.ds.admin.org.role;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.iorg.department.role.IAddDeparmentRolePopTree;
import com.ds.admin.iorg.role.IOrgRoleAPI;
import com.ds.admin.iorg.role.IOrgRoleForm;
import com.ds.admin.org.department.role.AddDeparmentRolePopTree;
import com.ds.common.JDSException;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.org.CtRole;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.*;
import com.ds.server.OrgManagerFactory;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@EsbBeanAnnotation()
public class OrgRoleAPI implements IOrgRoleAPI {
    public OrgRoleAPI() {

    }

    @Override
    public ResultModel<Boolean> saveRole(@RequestBody IOrgRoleForm role) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        CtRole ctRole = null;
        if (role.getRoleId() != null && !role.getRoleId().equals("")) {
            try {
                ctRole = (CtRole) OrgManagerFactory.getOrgManager().getRoleByID(role.getRoleId());
            } catch (RoleNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            ctRole = new CtRole();
            ctRole.setRoleId(UUID.randomUUID().toString());
        }

        if (role.getOrgRoleType() == null) {
            ctRole.setType(RoleType.Role);
        } else {
            ctRole.setType(role.getOrgRoleType());
        }

        try {
            ctRole.setSysId(ESDFacrory.getESDClient().getSpace().getSysId());
        } catch (JDSException e) {
            e.printStackTrace();
        }
        ctRole.setName(role.getName());
        CtOrgAdminManager.getInstance().saveRole(ctRole);

        return userStatusInfo;

    }


    public TreeListResultModel<List<AddDeparmentRolePopTree>> loadTop() {
        TreeListResultModel<List<AddDeparmentRolePopTree>> resultModel = new TreeListResultModel<List<AddDeparmentRolePopTree>>();
        List<Org> orgList = new ArrayList<>();
        try {
            orgList = getOrgManager().getTopOrgs();
            resultModel = TreePageUtil.getDefaultTreeList(orgList, AddDeparmentRolePopTree.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @Override
    public TreeListResultModel<List<AddDeparmentRolePopTree>> loadChild(String orgId) {
        TreeListResultModel<List<AddDeparmentRolePopTree>> resultModel = new TreeListResultModel<List<AddDeparmentRolePopTree>>();
        List<Org> orgList = new ArrayList<>();
        try {
            Org org = getOrgManager().getOrgByID(orgId);
            resultModel = TreePageUtil.getDefaultTreeList(org.getChildrenList(), AddDeparmentRolePopTree.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @Override
    public ResultModel<Boolean> delRole(String roleId) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] roleIdArr = StringUtility.split(roleId, ";");
        for (String id : roleIdArr) {
            CtOrgAdminManager.getInstance().delRole(id);
        }
        return userStatusInfo;

    }

    @Override
    public ResultModel<OrgRoleNav> getOrgRoleInfo(String roleId, String roleType) {
        ResultModel<OrgRoleNav> resultModel = new ResultModel<OrgRoleNav>();
        return resultModel;
    }

    @Override
    public ResultModel<AddOrgRoleForm> addRole(OrgRoleType orgRoleType, String sysId) {
        ResultModel<AddOrgRoleForm> resultModel = new ResultModel<AddOrgRoleForm>();
        CtRole role = new CtRole();
        role.setType(RoleType.valueOf(orgRoleType.getType()));
        role.setName("新建角色");
        role.setSysId(sysId);
        AddOrgRoleForm roleView = new AddOrgRoleForm(role);
        resultModel.setData(roleView);
        return resultModel;
    }


    @JSONField(serialize = false)
    private OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager();
        return orgManager;
    }
}
