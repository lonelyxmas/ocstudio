package com.ds.admin.org.role;

import com.ds.admin.iorg.role.IRoleForm;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Org;
import com.ds.org.Person;
import com.ds.org.Role;
import com.ds.org.RoleType;

@EsbBeanAnnotation()
public class RoleForm implements IRoleForm {

    String roleId;
    String sysId;
    String personId;
    String orgId;
    String name;
    RoleType personRoleType;

    public RoleForm() {

    }

    public RoleForm(Org org, Role role) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.orgId = org.getOrgId();
        this.sysId = role.getSysId();
        this.personRoleType = role.getType();
    }


    public RoleForm(Person person, Role role) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.personId = person.getID();
        this.sysId = role.getSysId();
        this.personRoleType = role.getType();
    }

    public RoleForm(Role role) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.sysId = role.getSysId();
        this.personRoleType = role.getType();
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getRoleId() {
        return roleId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getPersonRoleType() {
        return personRoleType;
    }

    public void setPersonRoleType(RoleType personRoleType) {
        this.personRoleType = personRoleType;
    }
}
