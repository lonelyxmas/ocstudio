package com.ds.admin.org.role;

import com.ds.admin.iorg.role.IAddOrgRoleForm;
import com.ds.admin.iorg.role.IAddRoleForm;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Role;
import com.ds.org.RoleType;

@EsbBeanAnnotation()
public class AddOrgRoleForm implements IAddOrgRoleForm {

    String roleId;
    String sysId;
    String name;
    RoleType orgRoleType;

    public AddOrgRoleForm() {

    }

    public AddOrgRoleForm(Role role) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.sysId = role.getSysId();
        this.orgRoleType = role.getType();
    }



    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getRoleId() {
        return roleId;
    }


    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getOrgRoleType() {
        return orgRoleType;
    }

    public void setOrgRoleType(RoleType orgRoleType) {
        this.orgRoleType = orgRoleType;
    }
}
