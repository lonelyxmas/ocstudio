package com.ds.admin.org.role.ref;


import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.iorg.role.ref.IRoleRefDeparmentService;
import com.ds.admin.org.department.role.DeparmentPopTree;
import com.ds.common.JDSException;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.*;
import com.ds.server.OrgManagerFactory;

import java.util.ArrayList;
import java.util.List;

@EsbBeanAnnotation
public class RoleRefDeparmentService implements IRoleRefDeparmentService {


    @Override
    public ListResultModel<List<RoleRefDeparmentGrid>> getRefDeparments(String roleId) {
        ListResultModel<List<RoleRefDeparmentGrid>> resultModel = new ListResultModel<List<RoleRefDeparmentGrid>>();
        List<RoleRefDeparmentGrid> roleViews = new ArrayList<>();
        try {
            Role role = this.getOrgManager().getRoleByID(roleId);
            List<Org> orgs = role.getOrgList();
            for (Org org : orgs) {
                RoleRefDeparmentGrid view = new RoleRefDeparmentGrid(role, org);
                roleViews.add(view);
            }
        } catch (JDSException e) {
            e.printStackTrace();

        } catch (RoleNotFoundException e) {
            e.printStackTrace();
        }
        resultModel.setData(roleViews);
        return resultModel;
    }

    @Override
    public ResultModel<Boolean> saveDeparment2RoleRef(String roleId, String AddDeparment2RoleTree) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        try {
            Role role = this.getOrgManager().getRoleByID(roleId);
            if (AddDeparment2RoleTree != null) {
                String[] orgIds = StringUtility.split(AddDeparment2RoleTree, ";");
                for (String orgId : orgIds) {
                    try {
                        CtOrgAdminManager.getInstance().addOrg2Role(orgId, role);
                    } catch (OrgNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (RoleNotFoundException e) {
            e.printStackTrace();
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return userStatusInfo;

    }

    @Override
    public TreeListResultModel<List<DeparmentPopTree>> getAddDeparment2RoleTree(String roleId, String id) {
        TreeListResultModel<List<DeparmentPopTree>> resultModel = new TreeListResultModel<List<DeparmentPopTree>>();
        try {
            List<Org> orgs = this.getOrgManager().getTopOrgs(JDSActionContext.getActionContext().getSystemCode());
            resultModel = TreePageUtil.getDefaultTreeList(orgs, DeparmentPopTree.class);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorListResultModel<List<DeparmentPopTree>>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }

    @Override
    public ResultModel<Boolean> delDeparmentRoleRef(String orgId, String roleId) throws OrgNotFoundException {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] roleIds = StringUtility.split(roleId, ";");
        try {
            for (String id : roleIds) {
                Role role = this.getOrgManager().getRoleByID(id);
                if (orgId != null) {
                    CtOrgAdminManager.getInstance().removeOrg2Role(orgId, role);
                }
            }

        } catch (RoleNotFoundException e) {
            e.printStackTrace();
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return userStatusInfo;

    }


    @JSONField(serialize = false)
    private ESDClient getClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }

    @JSONField(serialize = false)
    private OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.getClient().getConfigCode());
        return orgManager;
    }
}
