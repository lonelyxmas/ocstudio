package com.ds.admin.org.role;

import com.ds.admin.iorg.role.IRoleRefOrgService;
import com.ds.admin.org.department.role.DeparmentPopTree;
import com.ds.common.JDSException;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.GridPageUtil;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.*;
import com.ds.server.OrgManagerFactory;

import java.util.ArrayList;
import java.util.List;

@EsbBeanAnnotation()
public class RoleRefOrgService implements IRoleRefOrgService {


    @Override
    public ListResultModel<List<RoleRefOrgGrid>> getRefOrgs(String roleId, OrgRoleType roleType) {
        ListResultModel<List<RoleRefOrgGrid>> resultModel = new ListResultModel<List<RoleRefOrgGrid>>();
        List<RoleRefOrgGrid> roleViews = new ArrayList<>();
        try {
            Role role = this.getOrgManager().getRoleByID(roleId);
            List<Org> orgList = role.getOrgList();
            List<Org> orgRefList = new ArrayList();
            for (Org org : orgList) {
                if (role.getType().equals(RoleType.valueOf(roleType.getType()))) {
                    orgRefList.add(org);
                    RoleRefOrgGrid view = new RoleRefOrgGrid(org, roleId);
                    roleViews.add(view);
                }
        }
            resultModel = GridPageUtil.getDefaultPageList(orgList, RoleRefOrgGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        } catch (RoleNotFoundException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @Override
    public ResultModel<Boolean> saveOrg2RolRef(String roleId, String RoleRefOrgPopTree) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] orgIds = new String[]{};
        if (RoleRefOrgPopTree != null) {
            orgIds = StringUtility.split(RoleRefOrgPopTree, ";");
            for (String orgId : orgIds) {
                try {
                    Role role = this.getOrgManager().getRoleByID(roleId);
                    CtOrgAdminManager.getInstance().addOrg2Role(orgId, role);
                } catch (OrgNotFoundException e) {
                    e.printStackTrace();
                } catch (RoleNotFoundException e) {
                    e.printStackTrace();
                } catch (JDSException e) {
                    e.printStackTrace();
                }
            }
        }
        return userStatusInfo;

    }

    @Override
    public TreeListResultModel<List<DeparmentPopTree>> getRoleRefOrgPopTree(String roleId, String id) {
        TreeListResultModel<List<DeparmentPopTree>> resultModel = new TreeListResultModel<List<DeparmentPopTree>>();
        List<DeparmentPopTree> items = new ArrayList<>();
        try {
            List<Org> orgs = this.getOrgManager().getTopOrgs(JDSActionContext.getActionContext().getSystemCode());
            resultModel = TreePageUtil.getDefaultTreeList(orgs, DeparmentPopTree.class);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorListResultModel<List<DeparmentPopTree>>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }

    @Override

    public ResultModel<Boolean> delOrgRoleRef(String orgId, String roleId) throws OrgNotFoundException {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] roleIds = StringUtility.split(roleId, ";");
        String[] orgIds = StringUtility.split(orgId, ";");

        for (String id : roleIds) {
            try {
                Role role = this.getOrgManager().getRoleByID(id);
                for (String subOrgId : orgIds) {
                    if (subOrgId != null) {
                        CtOrgAdminManager.getInstance().removeOrg2Role(subOrgId, role);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return userStatusInfo;

    }


    public ESDClient getClient() throws JDSException {
        ESDClient client = ESDFacrory.getESDClient();
        return client;
    }

    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.getClient().getConfigCode());
        return orgManager;
    }
}
