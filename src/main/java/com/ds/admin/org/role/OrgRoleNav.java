package com.ds.admin.org.role;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.iorg.role.IOrgRoleForm;
import com.ds.admin.iorg.role.IOrgRoleNav;
import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.*;
import com.ds.server.OrgManagerFactory;

import java.util.ArrayList;
import java.util.List;

@EsbBeanAnnotation()
public class OrgRoleNav implements IOrgRoleNav {


    String roleId;

    String sysId;

    String personId;

    String orgId;

    public OrgRoleNav() {

    }
    @Override
    public ResultModel<OrgRoleForm> getRoleBaseInfo(String roleId) {
        ResultModel<OrgRoleForm> result = new ResultModel<OrgRoleForm>();
        return result;
    }

    @Override
    public ListResultModel<List<RoleRefOrgGrid>> getRefOrgs(String roleId, OrgRoleType orgRoleType) {
        ListResultModel<List<RoleRefOrgGrid>> resultModel = new ListResultModel<List<RoleRefOrgGrid>>();
        List<RoleRefOrgGrid> roleViews = new ArrayList<>();
        try {
            Role role = this.getOrgManager().getRoleByID(roleId);
            List<Org> orgList = role.getOrgList();
            for (Org org : orgList) {
                if (role.getType().equals(RoleType.valueOf(orgRoleType.getType()))) {
                    RoleRefOrgGrid view = new RoleRefOrgGrid(org, roleId);
                    roleViews.add(view);
                }
            }
        } catch (JDSException e) {
            e.printStackTrace();
        } catch (RoleNotFoundException e) {
            e.printStackTrace();
        }
        resultModel.setData(roleViews);
        return resultModel;
    }

    @JSONField(serialize = false)
    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager();
        return orgManager;
    }


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getRoleId() {
        return roleId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }


}
