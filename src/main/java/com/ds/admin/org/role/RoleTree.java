package com.ds.admin.org.role;

import com.ds.admin.iorg.role.IRoleTree;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.org.OrgRoleType;
import com.ds.org.PersonRoleType;
import com.ds.org.RoleOtherType;

@EsbBeanAnnotation
public class RoleTree<T> extends TreeListItem implements IRoleTree {


    RoleOtherType roleOtherType;

    OrgRoleType orgRoleType;

    PersonRoleType personRoleType;

    String caption;


    @TreeItemAnnotation(bindService = RoleTypeTreeAPI.class)
    public RoleTree(RoleOtherType type) {
        this.caption = type.getName();
        this.imageClass = type.getImageClass();
        this.roleOtherType = type;
    }

    @TreeItemAnnotation(bindService = RoleTreeAPI.class)
    public RoleTree(PersonRoleType personRoleType) {
        this.caption = personRoleType.getName();
        this.imageClass = personRoleType.getImageClass();
        this.personRoleType = personRoleType;
    }

    @TreeItemAnnotation(bindService = OrgRoleTreeAPI.class)
    public RoleTree(OrgRoleType orgRoleType) {
        this.caption = orgRoleType.getName();
        this.imageClass = orgRoleType.getImageClass();
        this.orgRoleType = orgRoleType;
    }

    @Override
    public OrgRoleType getOrgRoleType() {
        return orgRoleType;
    }

    public void setOrgRoleType(OrgRoleType orgRoleType) {
        this.orgRoleType = orgRoleType;
    }

    @Override
    public PersonRoleType getPersonRoleType() {
        return personRoleType;
    }

    public void setPersonRoleType(PersonRoleType personRoleType) {
        this.personRoleType = personRoleType;
    }

    public RoleOtherType getRoleOtherType() {
        return roleOtherType;
    }

    public void setRoleOtherType(RoleOtherType roleOtherType) {
        this.roleOtherType = roleOtherType;
    }


    @Override
    public String getCaption() {
        return caption;
    }

    @Override
    public void setCaption(String caption) {
        this.caption = caption;
    }
}
