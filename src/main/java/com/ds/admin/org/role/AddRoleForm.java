package com.ds.admin.org.role;

import com.ds.admin.iorg.role.IAddRoleForm;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Org;
import com.ds.org.Person;
import com.ds.org.Role;
import com.ds.org.RoleType;

@EsbBeanAnnotation()
public class AddRoleForm implements IAddRoleForm {

    String roleId;
    String sysId;
    String name;
    RoleType personRoleType;

    public AddRoleForm() {

    }

    public AddRoleForm(Person person, Role role) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.sysId = role.getSysId();
        this.personRoleType = role.getType();
    }

    public AddRoleForm(Role role) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.sysId = role.getSysId();
        this.personRoleType = role.getType();
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getPersonRoleType() {
        return personRoleType;
    }

    public void setPersonRoleType(RoleType personRoleType) {
        this.personRoleType = personRoleType;
    }
}
