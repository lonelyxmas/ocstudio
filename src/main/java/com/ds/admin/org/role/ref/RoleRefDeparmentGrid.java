package com.ds.admin.org.role.ref;

import com.ds.admin.iorg.role.ref.IRoleRefDeparmentGrid;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Org;
import com.ds.org.Role;

@EsbBeanAnnotation
public class RoleRefDeparmentGrid implements IRoleRefDeparmentGrid {

    String orgId;
    String name;
    String brief;
    String roleId;
    String parentId;

    public RoleRefDeparmentGrid() {

    }

    public RoleRefDeparmentGrid(Role role, Org org) {
        this.roleId = role.getRoleId();
        this.orgId = org.getOrgId();
        this.name = org.getName();
        this.brief = org.getBrief();
        this.parentId = org.getParentId();

    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }


}
