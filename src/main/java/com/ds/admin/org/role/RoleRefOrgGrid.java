package com.ds.admin.org.role;

import com.ds.admin.iorg.role.IRoleRefOrgGrid;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Org;

@EsbBeanAnnotation
public class RoleRefOrgGrid implements IRoleRefOrgGrid {


    String orgId;

    String roleId;

    String name;

    String brief;

    String parentId;


    public RoleRefOrgGrid(Org org,String roleId) {
        this.orgId = org.getOrgId();
        this.name = org.getName();
        this.brief = org.getBrief();
        this.parentId = org.getParentId();
        this.roleId=roleId;

    }

    @Override
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

}
