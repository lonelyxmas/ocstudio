package com.ds.admin.org.role;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.iorg.role.IRoleTypeTreeAPI;
import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.OrgManager;
import com.ds.org.OrgRoleType;
import com.ds.org.PersonRoleType;
import com.ds.org.RoleOtherType;
import com.ds.server.OrgManagerFactory;

import java.util.Arrays;
import java.util.List;

@EsbBeanAnnotation
public class RoleTypeTreeAPI implements IRoleTypeTreeAPI {


    @Override
    public TreeListResultModel<List<RoleTree>> loadChild(RoleOtherType roleOtherType) {
        TreeListResultModel<List<RoleTree>> resultModel = new TreeListResultModel<List<RoleTree>>();
        try {
            switch (roleOtherType) {
                case Person:
                    resultModel = TreePageUtil.getDefaultTreeList(Arrays.asList(PersonRoleType.values()), RoleTree.class);
                    break;
                case Org:
                    resultModel = TreePageUtil.getDefaultTreeList(Arrays.asList(OrgRoleType.values()), RoleTree.class);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @JSONField(serialize = false)
    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager();
        return orgManager;
    }
}
