package com.ds.admin.org.role;

import com.ds.admin.iorg.role.IPersonRoleGrid;
import com.ds.admin.iorg.role.IRoleGrid;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Org;
import com.ds.org.Person;
import com.ds.org.Role;
import com.ds.org.RoleType;

@EsbBeanAnnotation()
public class PersonRoleGrid implements IPersonRoleGrid {

    String roleId;
    String sysId;
    String personId;
    String name;
    RoleType roleType;

    public PersonRoleGrid() {

    }

    public PersonRoleGrid(Role role) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.sysId = role.getSysId();
        this.roleType = role.getType();
    }


    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getRoleId() {
        return roleId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
