package com.ds.admin.org.role;

import com.ds.admin.iorg.role.IOrgRoleGrid;
import com.ds.admin.iorg.role.IPersonRoleGrid;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Role;
import com.ds.org.RoleType;

@EsbBeanAnnotation()
public class OrgRoleGrid implements IOrgRoleGrid {

    String roleId;
    String sysId;
    String orgId;
    String name;
    RoleType orgRoleType;

    public OrgRoleGrid() {

    }


    public OrgRoleGrid(Role role) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.sysId = role.getSysId();
        this.orgRoleType = role.getType();

    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getRoleId() {
        return roleId;
    }


    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getOrgRoleType() {
        return orgRoleType;
    }

    public void setOrgRoleType(RoleType orgRoleType) {
        this.orgRoleType = orgRoleType;
    }
}
