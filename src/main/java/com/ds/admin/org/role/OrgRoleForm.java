package com.ds.admin.org.role;

import com.ds.admin.iorg.role.IOrgRoleForm;
import com.ds.admin.iorg.role.IRoleForm;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Org;
import com.ds.org.Person;
import com.ds.org.Role;
import com.ds.org.RoleType;

@EsbBeanAnnotation()
public class OrgRoleForm implements IOrgRoleForm {

    String roleId;
    String sysId;
    String orgId;
    String name;
    RoleType orgRoleType;

    public OrgRoleForm() {

    }

    public OrgRoleForm( Role role,String orgId) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.orgId =orgId;
        this.sysId = role.getSysId();
        this.orgRoleType = role.getType();
    }




    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getOrgRoleType() {
        return orgRoleType;
    }

    public void setOrgRoleType(RoleType orgRoleType) {
        this.orgRoleType = orgRoleType;
    }
}
