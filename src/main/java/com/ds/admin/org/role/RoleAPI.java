package com.ds.admin.org.role;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.iorg.role.IAddRoleForm;
import com.ds.admin.iorg.role.IRoleAPI;
import com.ds.admin.iorg.role.IRoleForm;
import com.ds.admin.iorg.role.IRoleNav;
import com.ds.common.JDSException;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.org.CtRole;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDFacrory;
import com.ds.org.OrgManager;
import com.ds.org.RoleNotFoundException;
import com.ds.org.RoleType;
import com.ds.server.OrgManagerFactory;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

@EsbBeanAnnotation()
public class RoleAPI implements IRoleAPI {

    public ResultModel<Boolean> saveRole(@RequestBody IRoleForm role) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        CtRole ctRole = null;
        if (role.getRoleId() != null && !role.getRoleId().equals("")) {
            try {
                ctRole = (CtRole) OrgManagerFactory.getOrgManager().getRoleByID(role.getRoleId());
            } catch (RoleNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            ctRole = new CtRole();
            ctRole.setRoleId(UUID.randomUUID().toString());
        }

        if (role.getPersonRoleType() == null) {
            ctRole.setType(RoleType.Role);
        } else {
            ctRole.setType(role.getPersonRoleType());
        }

        try {
            ctRole.setSysId(ESDFacrory.getESDClient().getSpace().getSysId());
        } catch (JDSException e) {
            e.printStackTrace();
        }
        ctRole.setName(role.getName());
        CtOrgAdminManager.getInstance().saveRole(ctRole);

        return userStatusInfo;

    }


    public ResultModel<Boolean> delRole(String roleId) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] roleIdArr = StringUtility.split(roleId, ";");
        for (String id : roleIdArr) {
            CtOrgAdminManager.getInstance().delRole(id);
        }
        return userStatusInfo;

    }


    public ResultModel<IRoleNav> getRoleInfo(String roleId, String personRoleType) {
        ResultModel<IRoleNav> resultModel = new ResultModel<IRoleNav>();
        return resultModel;
    }


    public ResultModel<IAddRoleForm> addRole(RoleType personRoleType, String sysId) {
        ResultModel<IAddRoleForm> resultModel = new ResultModel<IAddRoleForm>();
        CtRole role = new CtRole();
        role.setType(personRoleType);
        role.setName("新建角色");
        role.setSysId(sysId);
        AddRoleForm roleView = new AddRoleForm(role);
        resultModel.setData(roleView);
        return resultModel;
    }


    @JSONField(serialize = false)
    private OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager();
        return orgManager;
    }
}
