package com.ds.admin.org.department.role;

import com.ds.esd.custom.nav.tab.enums.TabItem;
import com.ds.org.OrgRoleType;


public enum OrgRoleTabItem implements TabItem {


    OrgRole(OrgRoleType.OrgRole, RefDeparmentAPI.class, false, false, false),

    OrgLevel(OrgRoleType.OrgLevel, RefDeparmentAPI.class, false, false, false);

    OrgRoleType orgRoleType;

    private String name;

    private String imageClass;

    private String className;

    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    OrgRoleTabItem(OrgRoleType orgRoleType, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.orgRoleType = orgRoleType;

        this.name = orgRoleType.getName();
        this.imageClass = orgRoleType.getImageClass();
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    public OrgRoleType getOrgRoleType() {
        return orgRoleType;
    }

    public void setOrgRoleType(OrgRoleType orgRoleType) {
        this.orgRoleType = orgRoleType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    @Override
    public String getType() {
        return name();
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    public String getImageClass() {
        return imageClass;
    }

    public static OrgRoleTabItem fromType(String typeName) {
        for (OrgRoleTabItem type : OrgRoleTabItem.values()) {
            if (type.getType().equals(typeName)) {
                return type;
            }
        }
        return null;
    }

    public static OrgRoleTabItem fromName(String name) {
        for (OrgRoleTabItem type : OrgRoleTabItem.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
