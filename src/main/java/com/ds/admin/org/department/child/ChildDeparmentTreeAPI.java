package com.ds.admin.org.department.child;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.iorg.department.child.IChildDeparmentTreeAPI;
import com.ds.admin.org.department.view.DeparmentGrid;
import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.Org;
import com.ds.org.OrgManager;
import com.ds.server.OrgManagerFactory;
import com.ds.web.util.PageUtil;

import java.util.ArrayList;
import java.util.List;

@EsbBeanAnnotation
public class ChildDeparmentTreeAPI implements IChildDeparmentTreeAPI {

    @Override
    public ListResultModel<List<DeparmentGrid>> getChildrenOrgList(String parentId) {
        ListResultModel<List<DeparmentGrid>> resultModel = new ListResultModel<List<DeparmentGrid>>();
        List<Org> orgList = new ArrayList<>();
        try {
            Org org = getOrgManager().getOrgByID(parentId);
            orgList = org.getChildrenList();
            resultModel = PageUtil.getDefaultPageList(orgList, DeparmentGrid.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @Override
    public TreeListResultModel<List<ChildDeparmentTree>> loadTop() {
        TreeListResultModel<List<ChildDeparmentTree>> resultModel = new TreeListResultModel<List<ChildDeparmentTree>>();
        List<Org> orgList = new ArrayList<>();
        try {
            orgList = getOrgManager().getTopOrgs();
            resultModel = TreePageUtil.getDefaultTreeList(orgList, ChildDeparmentTree.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @Override
    public TreeListResultModel<List<ChildDeparmentTree>> loadChildDeparment(String orgId) {
        TreeListResultModel<List<ChildDeparmentTree>> resultModel = new TreeListResultModel<List<ChildDeparmentTree>>();
        List<Org> orgList = new ArrayList<>();
        try {
            Org org = getOrgManager().getOrgByID(orgId);
            orgList = org.getChildrenList();
            resultModel = TreePageUtil.getDefaultTreeList(orgList, ChildDeparmentTree.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @JSONField(serialize = false)
    private ESDClient getClient() throws JDSException {
        ESDClient client = ESDFacrory.getESDClient();
        return client;
    }

    @JSONField(serialize = false)
    private OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.getClient().getConfigCode());
        return orgManager;
    }
}
