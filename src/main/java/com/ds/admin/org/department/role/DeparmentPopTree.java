package com.ds.admin.org.department.role;

import com.ds.admin.iorg.department.role.IDeparmentPopTree;
import com.ds.admin.org.department.child.ChildDeparmentTreeAPI;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.org.Org;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@EsbBeanAnnotation
public class DeparmentPopTree extends TreeListItem implements IDeparmentPopTree {



    String personId;


    String orgId;


    String roleId;
    public DeparmentPopTree(){}

    @TreeItemAnnotation(dynDestory = true, lazyLoad = true, bindService = ChildDeparmentTreeAPI.class)
    public DeparmentPopTree(Org org, String roleId) {
        this.caption = org.getName();
        this.roleId = roleId;
        this.orgId = org.getOrgId();
        this.personId = orgId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
}
