package com.ds.admin.org.department.role;

import com.ds.admin.iorg.department.role.IDeparmentRefRoleService;
import com.ds.admin.org.person.role.AddRolePersonPopTree;
import com.ds.common.JDSException;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.GridPageUtil;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.*;
import com.ds.server.OrgManagerFactory;

import java.util.ArrayList;
import java.util.List;

@EsbBeanAnnotation()
public class DeparmentRefRoleService implements IDeparmentRefRoleService {


    @Override
    public ListResultModel<List<RefDeparmentRoleGrid>> getRefRoles(String orgId, OrgRoleType orgRoleType) {
        ListResultModel<List<RefDeparmentRoleGrid>> resultModel = new ListResultModel<List<RefDeparmentRoleGrid>>();
        List<RefDeparmentRoleGrid> orgViews = new ArrayList<>();
        try {
            Org org = this.getOrgManager().getOrgByID(orgId);
            List<Role> roleList = org.getRoleList();
            List<Org> orgRefList = new ArrayList();
            for (Role role : roleList) {
                if (role.getType().equals(RoleType.valueOf(orgRoleType.getType()))) {
                    orgRefList.add(org);
                    RefDeparmentRoleGrid view = new RefDeparmentRoleGrid(role, orgId);
                    orgViews.add(view);
                }
            }
            resultModel = GridPageUtil.getDefaultPageList(orgViews, RefDeparmentRoleGrid.class);
        } catch (JDSException e) {
            e.printStackTrace();
        } catch (OrgNotFoundException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @Override
    public ResultModel<Boolean> saveRol2OrgRef(String orgId, String OrgRefRolePopTree) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] roleIds = new String[]{};
        try {
            Org org = this.getOrgManager().getOrgByID(orgId);
            if (OrgRefRolePopTree != null) {
                roleIds = StringUtility.split(OrgRefRolePopTree, ";");
                for (String roleId : roleIds) {
                    try {
                        CtOrgAdminManager.getInstance().addRole2Org(roleId, org);
                    } catch (RoleNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (OrgNotFoundException e) {
            e.printStackTrace();
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return userStatusInfo;

    }

    @Override
    public TreeListResultModel<List<AddDeparmentRolePopTree>> getOrgRefRolePopTree(String orgId, OrgRoleType orgRoleType) {
        TreeListResultModel<List<AddDeparmentRolePopTree>> resultModel = new TreeListResultModel<List<AddDeparmentRolePopTree>>();
        List<AddRolePersonPopTree> items = new ArrayList<>();
        try {
            List<Role> roles = this.getOrgManager().getAllRoles();
            List<Role> orgRoles = new ArrayList<>();
            for (Role role : roles) {
                if (role.getType().getType().equals(orgRoleType.getType())) {
                    orgRoles.add(role);
                }
            }

            resultModel = TreePageUtil.getDefaultTreeList(orgRoles, AddDeparmentRolePopTree.class);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorListResultModel<List<AddDeparmentRolePopTree>>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }

    @Override
    public ResultModel<Boolean> delOrgRoleRef(String orgId, String roleId) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] roleIds = StringUtility.split(roleId, ";");
        String[] personIds = StringUtility.split(orgId, ";");

        for (String id : roleIds) {
            try {
                Role role = this.getOrgManager().getRoleByID(id);
                for (String subPersonId : personIds) {
                    if (subPersonId != null) {
                        CtOrgAdminManager.getInstance().removePerson2Role(subPersonId, role);
                    }
                }

            } catch (PersonNotFoundException e) {
                e.printStackTrace();
            } catch (RoleNotFoundException e) {
                e.printStackTrace();
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }

        return userStatusInfo;

    }


    public ESDClient getClient() throws JDSException {
        ESDClient client = ESDFacrory.getESDClient();
        return client;
    }

    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.getClient().getConfigCode());
        return orgManager;
    }
}
