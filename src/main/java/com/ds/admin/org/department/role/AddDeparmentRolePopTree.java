package com.ds.admin.org.department.role;

import com.ds.admin.iorg.department.role.IAddDeparmentRolePopTree;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.org.Role;

@EsbBeanAnnotation
public class AddDeparmentRolePopTree extends TreeListItem implements IAddDeparmentRolePopTree {


    String orgId;

    String roleId;


    @TreeItemAnnotation()
    public AddDeparmentRolePopTree(Role role, String orgId) {
        this.caption = role.getName();
        this.roleId = role.getRoleId();
        this.imageClass = role.getType().getImageClass();
        this.id = roleId;
        this.orgId = orgId;
    }


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
}
