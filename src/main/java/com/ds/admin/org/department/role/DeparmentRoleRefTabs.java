package com.ds.admin.org.department.role;

import com.ds.admin.iorg.department.role.IDeparmentRoleRefTabs;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.nav.TabItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.org.OrgRoleType;

@EsbBeanAnnotation
public class DeparmentRoleRefTabs<T extends DeparmentRoleRefTabs> extends TreeListItem<T> implements IDeparmentRoleRefTabs {


    String orgId;

    OrgRoleType orgRoleType;

    String roleId;

    public DeparmentRoleRefTabs() {

    }

    @TabItemAnnotation(customItems = OrgRoleTabItem.class)
    public DeparmentRoleRefTabs(OrgRoleTabItem orgRoleType, String orgId) {
        this.caption = orgRoleType.getName();
        this.bindClassName = orgRoleType.getBindClass().getName();
        this.orgRoleType = orgRoleType.getOrgRoleType();
        this.imageClass = orgRoleType.getImageClass();
        this.orgId = orgId;
        this.roleId = orgRoleType.getType();
    }


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public OrgRoleType getOrgRoleType() {
        return orgRoleType;
    }

    public void setOrgRoleType(OrgRoleType orgRoleType) {
        this.orgRoleType = orgRoleType;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
}
