package com.ds.admin.org.department;

import com.ds.admin.iorg.department.IDeparmentService;
import com.ds.common.JDSException;
import com.ds.common.org.CtOrg;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.org.CtOrgManager;
import com.ds.common.util.CnToSpell;
import com.ds.common.util.StringUtility;
import com.ds.config.ListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDClient;
import com.ds.org.Org;
import com.ds.org.OrgNotFoundException;
import com.ds.server.OrgManagerFactory;
import com.ds.web.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@EsbBeanAnnotation()
@Service("DeparmentService")
public class DeparmentService implements IDeparmentService {

    @Autowired
    private ESDClient esdClient;
    @Autowired
    private CtOrgManager orgManager;


    public ListResultModel<List<Org>> findOrgs(String patten) throws JDSException {
        List<Org> orgs = new ArrayList<>();
        List<Org> orgList = OrgManagerFactory.getOrgManager().getOrgs();
        for (Org org : orgList) {
            if (patten != null && !patten.equals("")) {
                Pattern p = Pattern.compile(patten, Pattern.CASE_INSENSITIVE);
                Matcher namematcher = p.matcher(org.getName() == null ? "" : org.getName());
                Matcher fieldmatcher = p.matcher(org.getBrief() == null ? "" : org.getBrief());
                Matcher cnmatcher = p.matcher(CnToSpell.getFullSpell(org.getName() == null ? "" : org.getName()));
                if (namematcher.find()
                        || fieldmatcher.find()
                        || cnmatcher.find()
                        ) {
                    orgs.add(org);
                }
            } else {
                orgs.add(org);
            }
        }
        return PageUtil.getDefaultPageList(orgs);

    }


    public Org getOrgInfo(String orgId) throws OrgNotFoundException {
        Org org = orgManager.getOrgByID(orgId);
        return org;
    }


    public Org addDeparment(String parentId) {
        CtOrg org = new CtOrg();
        org.setParentId(parentId);
        org.setOrgId(UUID.randomUUID().toString());
        org.setName("新建部门");
        return org;
    }


    public void saveOrg(CtOrg org) throws JDSException {
        if (org.getOrgId() != null && org.getOrgId().equals("")) {
            org.setOrgId(UUID.randomUUID().toString());
        }
        CtOrgAdminManager.getInstance().saveOrg(org);
        orgManager.clearOrgCache(org.getParentId());
    }


    public List<Org> getChildrenOrgList(String parentId) throws OrgNotFoundException {
        List<Org> orgList = new ArrayList<>();
        Org org = OrgManagerFactory.getOrgManager(esdClient.getConfigCode()).getOrgByID(parentId);
        orgList = org.getChildrenList();
        return orgList;
    }


    public void delOrg(String orgId) {
        ;
        String[] orgIdArr = StringUtility.split(orgId, ";");
        for (String id : orgIdArr) {
            CtOrgAdminManager.getInstance().delOrg(id);
        }
    }


//    @JSONField(serialize = false)
//    ESDClient getClient() throws JDSException {
//        ESDClient client = ESDFacrory.getESDClient();
//        return client;
//    }
//
//    @JSONField(serialize = false)
//    OrgManager getOrgManager() throws JDSException {
//        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.getClient().getConfigCode());
//        return orgManager;
//    }
}
