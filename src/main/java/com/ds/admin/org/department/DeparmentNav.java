package com.ds.admin.org.department;


import com.ds.admin.iorg.department.IDeparmentNav;
import com.ds.admin.iorg.department.view.IDeparmentForm;
import com.ds.admin.org.department.role.DeparmentRoleRefTabs;
import com.ds.admin.org.department.role.OrgRoleTabItem;
import com.ds.admin.org.department.view.DeparmentForm;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.util.TabPageUtil;

import java.util.Arrays;
import java.util.List;

@EsbBeanAnnotation
public class DeparmentNav implements IDeparmentNav {

    @CustomAnnotation(uid = true, hidden = true)
    public String orgId;

    public DeparmentNav() {

    }

    public DeparmentNav(DeparmentNav nav) {
        this.orgId = nav.getOrgId();
    }


    public ResultModel<DeparmentForm> getDeparmentBaseInfo(String orgId) {
        ResultModel<DeparmentForm> result = new ResultModel<DeparmentForm>();
        return result;
    }


    public TreeListResultModel<List<DeparmentRoleRefTabs>> getOrgRoles(String id, String orgId) {
        TreeListResultModel<List<DeparmentRoleRefTabs>> resultModel = TabPageUtil.getTabList(Arrays.asList(OrgRoleTabItem.values()), DeparmentRoleRefTabs.class);
        return resultModel;

    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
