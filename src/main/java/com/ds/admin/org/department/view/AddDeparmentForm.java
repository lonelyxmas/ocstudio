package com.ds.admin.org.department.view;

import com.ds.admin.iorg.department.view.IAddDeparmentForm;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Org;

@EsbBeanAnnotation
public class AddDeparmentForm implements IAddDeparmentForm {

    String orgId;

    String name;

    String brief;

    String parentId;

    public AddDeparmentForm() {

    }

    public AddDeparmentForm(Org org) {
        this.orgId = org.getOrgId();
        this.name = org.getName();
        this.brief = org.getBrief();
        this.parentId = org.getParentId();

    }

    public AddDeparmentForm(AddDeparmentForm org) {
        this.orgId = org.getOrgId();
        this.name = org.getName();
        this.brief = org.getBrief();
        this.parentId = org.getParentId();

    }


    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }


}
