package com.ds.admin.org.department.role;

import com.ds.admin.iorg.department.role.IRefDeparmentRoleGrid;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.OrgRoleType;
import com.ds.org.Role;
import com.ds.org.RoleType;

@EsbBeanAnnotation
public class RefDeparmentRoleGrid implements IRefDeparmentRoleGrid {


    String roleId;

    String orgId;

    String name;

    RoleType roleType;

    OrgRoleType orgRoleType;


    public RefDeparmentRoleGrid() {

    }


    public RefDeparmentRoleGrid(Role role, String orgId) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.orgId = orgId;
        this.roleType = role.getType();
        this.orgRoleType = OrgRoleType.valueOf(role.getType().getType());
    }

    @Override
    public OrgRoleType getOrgRoleType() {
        return orgRoleType;
    }

    public void setOrgRoleType(OrgRoleType orgRoleType) {
        this.orgRoleType = orgRoleType;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getRoleId() {
        return roleId;
    }


    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
