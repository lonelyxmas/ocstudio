package com.ds.admin.org.department.view;

import com.ds.admin.iorg.department.view.IDeparmentForm;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Org;
import org.springframework.stereotype.Component;

@EsbBeanAnnotation
@Component
public class DeparmentForm implements IDeparmentForm {

    String orgId;

    String name;

    String leaderId;

    String brief;

    String parentId;

    public DeparmentForm(Org org) {
        this.orgId = org.getOrgId();
        this.name = org.getName();
        this.brief = org.getBrief();
        this.parentId = org.getParentId();
        this.leaderId = org.getLeaderId();

    }

    @Override
    public String getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(String leaderId) {
        this.leaderId = leaderId;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }


}
