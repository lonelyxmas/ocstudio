package com.ds.admin.org.department;

import com.ds.admin.iorg.department.IDeparmentAPI;
import com.ds.admin.org.department.view.AddDeparmentForm;
import com.ds.admin.org.department.view.DeparmentTree;
import com.ds.admin.org.person.view.PersonGrid;
import com.ds.common.JDSException;
import com.ds.common.org.CtOrg;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.org.CtOrgManager;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.Org;
import com.ds.org.OrgManager;
import com.ds.org.Person;
import com.ds.server.OrgManagerFactory;
import com.ds.web.util.PageUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@EsbBeanAnnotation
public class DeparmentAPI<M extends AddDeparmentForm> implements IDeparmentAPI<M> {


    @Override
    public ListResultModel<List<PersonGrid>> getPersons(String orgId) {
        ListResultModel<List<PersonGrid>> resultModel = new ListResultModel<List<PersonGrid>>();
        try {
            Org org = getOrgManager().getOrgByID(orgId);
            List<Person> personList = org.getPersonList();
            resultModel = PageUtil.getDefaultPageList(personList, PersonGrid.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @Override
    public ListResultModel<List<DeparmentTree>> loadChild(String parentId) {
        ListResultModel<List<DeparmentTree>> resultModel = new ListResultModel<List<DeparmentTree>>();
        try {
            Org org = getOrgManager().getOrgByID(parentId);
            List<Org> orgList = org.getChildrenList();
            if (orgList != null && orgList.size() > 0) {
                resultModel = TreePageUtil.getDefaultTreeList(orgList, DeparmentTree.class);
            } else {
                resultModel.setData(new ArrayList<DeparmentTree>());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @Override
    public ResultModel<DeparmentNav> getDeparmentInfo(String orgId) {
        ResultModel<DeparmentNav> resultModel = new ResultModel<DeparmentNav>();
        return resultModel;
    }

    @Override
    public ResultModel<M> addDeparment(String parentId) {
        ResultModel<AddDeparmentForm> resultModel = new ResultModel<AddDeparmentForm>();
        CtOrg org = new CtOrg();
        org.setParentId(parentId);
        org.setOrgId(UUID.randomUUID().toString());
        org.setName("新建部门");
        AddDeparmentForm deparmentView = new AddDeparmentForm(org);
        resultModel.setData(deparmentView);
        return (ResultModel<M>) resultModel;
    }

    @Override
    public ResultModel<Boolean> saveOrg(@RequestBody M deparmentForm) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        try {
            CtOrg ctOrg = new CtOrg();
            BeanUtils.copyProperties(deparmentForm, ctOrg);
            if (deparmentForm.getOrgId() != null && deparmentForm.getOrgId().equals("")) {
                ctOrg.setOrgId(UUID.randomUUID().toString());
            }
            CtOrgAdminManager.getInstance().saveOrg(ctOrg);
            CtOrgManager manager = (CtOrgManager) OrgManagerFactory.getOrgManager();
            manager.clearOrgCache(deparmentForm.getParentId());
        } catch (JDSException e) {
            userStatusInfo = new ErrorResultModel();
            ((ErrorResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorResultModel) userStatusInfo).setErrdes(e.getMessage());
        }
        return userStatusInfo;

    }

    @Override
    public ResultModel<Boolean> delOrg(String orgId) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] orgIdArr = StringUtility.split(orgId, ";");
        for (String id : orgIdArr) {
            CtOrgAdminManager.getInstance().delOrg(id);
        }
        return userStatusInfo;
    }


    public ESDClient getClient() throws JDSException {
        ESDClient client = ESDFacrory.getESDClient();
        return client;
    }

    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.getClient().getConfigCode());
        return orgManager;
    }
}
