package com.ds.admin.org.department.child;

import com.ds.admin.iorg.department.child.IChildDeparmentTree;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.org.Org;

@EsbBeanAnnotation
public class ChildDeparmentTree extends TreeListItem implements IChildDeparmentTree {


    String parentId;

    String orgId;

    String name;

    public ChildDeparmentTree() {
    }

    @TreeItemAnnotation(bindService = ChildDeparmentTreeAPI.class)
    public ChildDeparmentTree(Org org) {
        this.parentId = org.getOrgId();
        this.orgId = org.getOrgId();
        this.name = org.getName();

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
