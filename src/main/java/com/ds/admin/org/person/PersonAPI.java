package com.ds.admin.org.person;

import com.ds.admin.iorg.person.IPersonAPI;
import com.ds.admin.iorg.person.view.IPersonForm;
import com.ds.admin.org.person.view.AddPersonForm;
import com.ds.admin.org.person.view.PersonGrid;
import com.ds.common.org.CtPerson;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.org.Person;
import com.ds.web.util.PageUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@EsbBeanAnnotation
public class PersonAPI implements IPersonAPI {


    public ListResultModel<List<PersonGrid>> getPersons(String orgId) {
        ListResultModel<List<PersonGrid>> resultModel = new ListResultModel<List<PersonGrid>>();
        List<Person> personList = new ArrayList<>();
        try {
            personList = getService().getPersons(orgId);
            resultModel = PageUtil.getDefaultPageList(personList, PersonGrid.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    public ResultModel<PersonNav> getPersonInfo(String personId) {
        ResultModel<PersonNav> resultModel = new ResultModel<PersonNav>();
        return resultModel;


    }


    public ResultModel<AddPersonForm> AddPerson(String orgId) {
        ResultModel<AddPersonForm> resultModel = new ResultModel<AddPersonForm>();
        CtPerson person = new CtPerson();
        person.setOrgId(orgId);
        resultModel.setData(new AddPersonForm(person));
        return resultModel;


    }


    public ResultModel<Boolean> savePerson(@RequestBody IPersonForm person) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        CtPerson ctPerson = new CtPerson();
        if (person.getPersonId() != null && !person.getPersonId().equals("")) {
            ctPerson.setID(person.getPersonId());
        } else {
            ctPerson.setID(UUID.randomUUID().toString());
        }

        BeanUtils.copyProperties(person, ctPerson);
        getService().savePerson(ctPerson);
        return userStatusInfo;
    }


    public ResultModel<Boolean> delPerson(String personId) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        getService().delPerson(personId);
        return userStatusInfo;
    }

    PersonService getService() {
        return EsbUtil.parExpression(PersonService.class);
    }
}
