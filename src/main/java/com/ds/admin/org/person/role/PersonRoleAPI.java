package com.ds.admin.org.person.role;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.admin.iorg.person.role.IPersonRoleAPI;
import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.*;
import com.ds.server.OrgManagerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@EsbBeanAnnotation
public class PersonRoleAPI implements IPersonRoleAPI {

    @Override
    public TreeListResultModel<List<AddRolePersonPopTree>> loadChildOrg(String orgId, PersonRoleType personRoleType) {
        TreeListResultModel<List<AddRolePersonPopTree>> resultModel = new TreeListResultModel<List<AddRolePersonPopTree>>();
        List orgList = new ArrayList<>();
        try {
            Org org = OrgManagerFactory.getOrgManager().getOrgByID(orgId);
            if (org.getChildrenList() != null && org.getChildrenList().size() > 0) {
                for (Org childorg : org.getChildrenList()) {
                    if ((org.getChildrenList() != null && org.getChildrenList().size() > 0)
                            || (org.getPersonList() != null && org.getPersonList().size() > 0)
                            ) {
                        orgList.add(childorg);
                    }
                }
            }
            if (org.getPersonList() != null && org.getPersonList().size() > 0) {
                for (Person person : org.getPersonList()) {
                    orgList.add(person);
                }
            }
            resultModel = TreePageUtil.getDefaultTreeList(orgList, AddRolePersonPopTree.class);
        } catch (OrgNotFoundException e) {
            e.printStackTrace();
        }


        return resultModel;
    }

    @Override
    public ListResultModel<List<PersonRefRoleGrid>> getTabRefPersonRoles(String personId, PersonRoleType personRoleType) {
        ListResultModel<List<PersonRefRoleGrid>> resultModel = new ListResultModel<List<PersonRefRoleGrid>>();
        List<PersonRefRoleGrid> roleViews = new ArrayList<>();
        try {
            Person person = this.getOrgManager().getPersonByID(personId);
            Set<String> roleIds = person.getRoleIdList();
            for (String roleId : roleIds) {
                try {
                    Role role = this.getOrgManager().getRoleByID(roleId);
                    if (personRoleType != null && role.getType().getType().equals(personRoleType.getType())) {
                        PersonRefRoleGrid view = new PersonRefRoleGrid(person, role);
                        roleViews.add(view);
                    }
                } catch (RoleNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (PersonNotFoundException e) {
            e.printStackTrace();
        } catch (JDSException e) {
            e.printStackTrace();
        }
        resultModel.setData(roleViews);
        return resultModel;
    }


    @JSONField(serialize = false)
    ESDClient getClient() throws JDSException {
        ESDClient client = ESDFacrory.getESDClient();
        return client;
    }

    @JSONField(serialize = false)
    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.getClient().getConfigCode());
        return orgManager;
    }
}
