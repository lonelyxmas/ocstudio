package com.ds.admin.org.person.view;

import com.ds.admin.iorg.person.view.IPersonForm;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Org;
import com.ds.org.OrgNotFoundException;
import com.ds.org.Person;
import com.ds.server.OrgManagerFactory;

@EsbBeanAnnotation
public class PersonForm implements IPersonForm {


    String personId;

    String orgId;

    String roleId;

    String name;

    String account;

    String email;

    String password;

    String mobile;

    String orgName;

    public PersonForm() {

    }

    public PersonForm(Person person) {

        this.personId = person.getID();
        this.orgId = person.getOrgId();
        this.name = person.getName();
        this.account = person.getAccount();
        this.password = person.getPassword();
        this.mobile = person.getMobile();
        this.email = person.getEmail();
        Org org = null;
        try {
            org = OrgManagerFactory.getOrgManager().getOrgByID(person.getOrgId());
            this.orgName = org.getName();
        } catch (OrgNotFoundException e) {
            e.printStackTrace();
        }

    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
