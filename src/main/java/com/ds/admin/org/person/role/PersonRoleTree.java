package com.ds.admin.org.person.role;

import com.ds.admin.iorg.person.role.IPersonRefRoleService;
import com.ds.admin.iorg.person.role.IPersonRoleTree;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.org.PersonRoleType;
import com.ds.org.Role;

@EsbBeanAnnotation
public class PersonRoleTree<T extends PersonRoleTree> extends TreeListItem<T> implements IPersonRoleTree {


    String personId;

    PersonRoleType personRoleType;

    String roleId;


    @TreeItemAnnotation(bindService = IPersonRefRoleService.class)
    public PersonRoleTree(PersonRoleType personRoleType, String personId) {
        this.caption = personRoleType.getName();
        this.personRoleType = personRoleType;
        this.imageClass = personRoleType.getImageClass();
        this.personId = personId;
        this.roleId = personRoleType.getType();
    }

    @TreeItemAnnotation()
    public PersonRoleTree(Role role, String personId) {
        this.personRoleType = PersonRoleType.valueOf(role.getType().getType());
        this.caption = role.getName();
        this.imageClass = personRoleType.getImageClass();
        this.personId = personId;
        this.roleId = role.getRoleId();
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public PersonRoleType getPersonRoleType() {
        return personRoleType;
    }

    public void setPersonRoleType(PersonRoleType personRoleType) {
        this.personRoleType = personRoleType;
    }

}
