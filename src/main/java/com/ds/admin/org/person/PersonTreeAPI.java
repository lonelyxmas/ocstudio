package com.ds.admin.org.person;

import com.ds.admin.iorg.person.IPersonTreeAPI;
import com.ds.admin.org.person.view.PersonTree;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.util.TreePageUtil;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.org.Org;
import com.ds.org.OrgNotFoundException;
import com.ds.org.Person;
import com.ds.server.OrgManagerFactory;

import java.util.ArrayList;
import java.util.List;

@EsbBeanAnnotation
public class PersonTreeAPI implements IPersonTreeAPI {


    @Override
    public TreeListResultModel<List<PersonTree>> loadTop() {
        TreeListResultModel<List<PersonTree>> resultModel = new TreeListResultModel<List<PersonTree>>();
        List<Org> orgList = new ArrayList<>();
        try {
            orgList = getService().getOrgManager().getTopOrgs();
            resultModel = TreePageUtil.getDefaultTreeList(orgList, PersonTree.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @Override
    public TreeListResultModel<List<PersonTree>> loadChildDeparment(String orgId) {
        TreeListResultModel<List<PersonTree>> resultModel = new TreeListResultModel<List<PersonTree>>();
        try {
            List orgList = new ArrayList<>();
            try {
                Org org = OrgManagerFactory.getOrgManager().getOrgByID(orgId);
                if (org.getChildrenList() != null && org.getChildrenList().size() > 0) {
                    for (Org childorg : org.getChildrenList()) {
                        if ((org.getChildrenList() != null && org.getChildrenList().size() > 0)
                                || (org.getPersonList() != null && org.getPersonList().size() > 0)
                                ) {
                            orgList.add(childorg);
                        }
                    }
                }
                if (org.getPersonList() != null && org.getPersonList().size() > 0) {
                    for (Person person : org.getPersonList()) {
                        orgList.add(person);
                    }
                }
                resultModel = TreePageUtil.getDefaultTreeList(orgList, PersonTree.class);
            } catch (OrgNotFoundException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    @Override
    public TreeListResultModel<List<PersonTree>> getPersonTree(String orgId) {
        TreeListResultModel<List<PersonTree>> resultModel = new TreeListResultModel<List<PersonTree>>();
        List<Org> orgList = new ArrayList<>();
        try {
            orgList = getService().getOrgManager().getTopOrgs();
            resultModel = TreePageUtil.getDefaultTreeList(orgList, PersonTree.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    PersonService getService() {
        return EsbUtil.parExpression(PersonService.class);
    }


    @Override
    public ResultModel<Boolean> updateLeader() {
        return new ResultModel<>();
    }
}
