package com.ds.admin.org.person;

import com.ds.admin.iorg.person.IPersonNav;
import com.ds.admin.org.person.role.PersonRoleTabItem;
import com.ds.admin.org.person.role.PersonRoleTabs;
import com.ds.admin.org.person.view.PersonForm;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.util.TabPageUtil;
import com.ds.org.Person;
import com.ds.org.PersonNotFoundException;
import com.ds.server.OrgManagerFactory;
import com.ds.web.annotation.Pid;

import java.util.Arrays;
import java.util.List;

@EsbBeanAnnotation()
public class PersonNav implements IPersonNav {

    @Pid
    String personId;

    public PersonNav() {
    }


    @Override
    public ResultModel<PersonForm> getPersonBaseInfo(String personId) {
        ResultModel<PersonForm> result = new ResultModel<PersonForm>();
        Person person = null;
        try {
            person = OrgManagerFactory.getOrgManager().getPersonByID(personId);
        } catch (PersonNotFoundException e) {
            e.printStackTrace();
        }
        result.setData(new PersonForm(person));
        return result;
    }

    @Override
    public TreeListResultModel<List<PersonRoleTabs>> getPersonRoles(String personId) {
        return TabPageUtil.getDefaultTabList(Arrays.asList(PersonRoleTabItem.values()), PersonRoleTabs.class);
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
