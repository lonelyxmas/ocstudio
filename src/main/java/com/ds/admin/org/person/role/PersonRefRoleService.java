package com.ds.admin.org.person.role;

import com.ds.admin.iorg.person.role.IPersonRefRoleService;
import com.ds.admin.org.role.RolePopTree;
import com.ds.common.JDSException;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.*;
import com.ds.server.OrgManagerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@EsbBeanAnnotation()
public class PersonRefRoleService implements IPersonRefRoleService {


    @Override
    public TreeListResultModel<List<RolePopTree>> loadRoleChild(String personId, PersonRoleType personRoleType) {
        TreeListResultModel<List<RolePopTree>> resultModel = new TreeListResultModel<List<RolePopTree>>();
        List<Role> typeRoles = new ArrayList<>();
        List<String> selectRoleIds = new ArrayList<>();
        if (personRoleType != null) {

            Person person = null;
            try {
                person = OrgManagerFactory.getOrgManager().getPersonByID(personId);
            } catch (PersonNotFoundException e) {
                e.printStackTrace();
            }
            Set<String> roleIds = person.getRoleIdList();

            List<Role> roles = OrgManagerFactory.getOrgManager().getAllRoles();
            if (roles.size() > 0) {
                for (Role role : roles) {
                    if (role.getType().getType().equals(personRoleType.getType())) {
                        typeRoles.add(role);
                        if (roleIds.contains(role.getRoleId())) {
                            selectRoleIds.add(role.getRoleId());
                        }
                    }
                }
            }
        }
        resultModel = TreePageUtil.getTreeList(typeRoles, RolePopTree.class, selectRoleIds);
        return resultModel;
    }



    @Override
    public ListResultModel<List<PersonRefRoleGrid>> getRefPersonRoles(String personId, RoleType roleType) {
        ListResultModel<List<PersonRefRoleGrid>> resultModel = new ListResultModel<List<PersonRefRoleGrid>>();
        List<PersonRefRoleGrid> roleViews = new ArrayList<>();
        try {
            Person person = this.getOrgManager().getPersonByID(personId);
            Set<String> roleIds = person.getRoleIdList();
            for (String roleId : roleIds) {
                try {
                    Role role = this.getOrgManager().getRoleByID(roleId);
                    if (role.getType().equals(roleType)) {
                        PersonRefRoleGrid view = new PersonRefRoleGrid(person, role);
                        roleViews.add(view);
                    }
                } catch (RoleNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (PersonNotFoundException e) {
            e.printStackTrace();
        } catch (JDSException e) {
            e.printStackTrace();
        }
        resultModel.setData(roleViews);
        return resultModel;
    }

    @Override
    public TreeListResultModel<List<RolePopTree>> getRolePersonPopTree(String personId, PersonRoleType personRoleType, String id) {
        TreeListResultModel<List<RolePopTree>> resultModel = new TreeListResultModel<List<RolePopTree>>();
        try {
            resultModel = TreePageUtil.getDefaultTreeList(Arrays.asList(personRoleType), RolePopTree.class);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorListResultModel<List<RolePopTree>>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }

    @Override
    public ResultModel<Boolean> saveRol2PersonRef(String personId, String PersonRolePopTree) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] roleIds = new String[]{};
        if (PersonRolePopTree != null) {
            roleIds = StringUtility.split(PersonRolePopTree, ";");
            for (String roleId : roleIds) {
                try {
                    Role role = this.getOrgManager().getRoleByID(roleId);
                    CtOrgAdminManager.getInstance().addPerson2Role(personId, role);
                } catch (PersonNotFoundException e) {
                    e.printStackTrace();
                } catch (RoleNotFoundException e) {
                    e.printStackTrace();
                } catch (JDSException e) {
                    e.printStackTrace();
                }
            }
        }
        return userStatusInfo;

    }


    @Override
    public ResultModel<Boolean> delPersonRoleRef(String personId, String roleId) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        String[] roleIds = StringUtility.split(roleId, ";");
        String[] personIds = StringUtility.split(personId, ";");

        for (String id : roleIds) {
            try {
                Role role = this.getOrgManager().getRoleByID(id);
                for (String subPersonId : personIds) {
                    if (subPersonId != null) {
                        CtOrgAdminManager.getInstance().removePerson2Role(subPersonId, role);
                    }
                }

            } catch (PersonNotFoundException e) {
                e.printStackTrace();
            } catch (RoleNotFoundException e) {
                e.printStackTrace();
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }

        return userStatusInfo;

    }


    public ESDClient getClient() throws JDSException {
        ESDClient client = ESDFacrory.getESDClient();
        return client;
    }

    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.getClient().getConfigCode());
        return orgManager;
    }

}
