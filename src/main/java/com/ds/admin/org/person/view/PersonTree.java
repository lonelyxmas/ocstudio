package com.ds.admin.org.person.view;

import com.ds.admin.iorg.person.view.IPersonTree;
import com.ds.admin.org.person.PersonTreeAPI;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.org.Org;
import com.ds.org.Person;

@EsbBeanAnnotation
public class PersonTree extends TreeListItem implements IPersonTree {

    String personId;

    String parentId;

    String orgId;

    String name;

    public PersonTree() {
    }

    @TreeItemAnnotation(dynDestory = true, lazyLoad = true, bindService = PersonTreeAPI.class, imageClass = "spafont spa-icon-app")
    public PersonTree(Org org) {
        this.id = orgId;
        this.parentId = org.getOrgId();
        this.orgId = org.getOrgId();
        this.name = org.getName();

    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-login")
    public PersonTree(Person person) {
        this.personId = person.getID();
        this.orgId = person.getOrgId();
        this.id = personId;
        this.name = person.getName();

    }

    @Override
    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
