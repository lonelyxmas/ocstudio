package com.ds.admin.org.person.role;

import com.ds.admin.iorg.person.role.IPersonRefRoleGrid;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.org.Person;
import com.ds.org.PersonRoleType;
import com.ds.org.Role;
import com.ds.org.RoleType;

@EsbBeanAnnotation
public class PersonRefRoleGrid implements IPersonRefRoleGrid {

    String roleId;

    String personId;

    String name;

    RoleType roleType;

    PersonRoleType personRoleType;


    public PersonRefRoleGrid() {

    }

    public PersonRefRoleGrid(Person person, Role role) {
        this.roleId = role.getRoleId();
        this.name = role.getName();
        this.personId = person.getID();
        this.roleType = role.getType();
        this.personRoleType = PersonRoleType.valueOf(roleType.getType());
    }

    @Override
    public PersonRoleType getPersonRoleType() {
        return personRoleType;
    }

    public void setPersonRoleType(PersonRoleType personRoleType) {
        this.personRoleType = personRoleType;
    }

    public String getRoleId() {
        return roleId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
