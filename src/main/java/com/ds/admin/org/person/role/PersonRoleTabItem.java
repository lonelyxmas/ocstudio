package com.ds.admin.org.person.role;

import com.ds.esd.custom.nav.tab.enums.TabItem;
import com.ds.org.PersonRoleType;


public enum PersonRoleTabItem implements TabItem {


    Duty(PersonRoleType.Duty, PersonRoleAPI.class, false, false, false),

    Role(PersonRoleType.Role, PersonRoleAPI.class, false, false, false),

    PersonLevel(PersonRoleType.PersonLevel, PersonRoleAPI.class, false, false, false),

    Group(PersonRoleType.Group, PersonRoleAPI.class, false, false, false),

    Position(PersonRoleType.Position, PersonRoleAPI.class, false, false, false);

    PersonRoleType personRoleType;

    private String name;

    private String imageClass;

    private String className;

    private final Class bindClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;


    PersonRoleTabItem(PersonRoleType personRoleType, Class bindClass, boolean iniFold, boolean dynLoad, boolean dynDestory) {
        this.personRoleType = personRoleType;
        this.name = personRoleType.getName();
        this.imageClass = personRoleType.getImageClass();
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    public PersonRoleType getPersonRoleType() {
        return personRoleType;
    }

    public void setPersonRoleType(PersonRoleType personRoleType) {
        this.personRoleType = personRoleType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    @Override
    public String getType() {
        return name();
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    public String getImageClass() {
        return imageClass;
    }

    public static PersonRoleTabItem fromType(String typeName) {
        for (PersonRoleTabItem type : PersonRoleTabItem.values()) {
            if (type.getType().equals(typeName)) {
                return type;
            }
        }
        return null;
    }

    public static PersonRoleTabItem fromName(String name) {
        for (PersonRoleTabItem type : PersonRoleTabItem.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
