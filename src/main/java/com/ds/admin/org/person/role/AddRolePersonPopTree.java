package com.ds.admin.org.person.role;

import com.ds.admin.iorg.person.role.IAddRolePersonPopTree;
import com.ds.admin.iorg.person.role.IPersonRoleAPI;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.org.Org;
import com.ds.org.Person;

@EsbBeanAnnotation
public class AddRolePersonPopTree extends TreeListItem implements IAddRolePersonPopTree {

    String personId;

    String orgId;

    String roleId;


    @TreeItemAnnotation(bindService = IPersonRoleAPI.class, lazyLoad = true, dynDestory = true, imageClass = "spafont spa-icon-c-treeview")
    public AddRolePersonPopTree(Org org, String roleId) {
        this.caption = org.getName();
        this.roleId = roleId;
        this.orgId = org.getOrgId();
        this.personId = orgId;
    }

    @TreeItemAnnotation(imageClass = "bpmfont bpmgongzuoliu")
    public AddRolePersonPopTree(Person person, String roleId) {
        this.caption = person.getName();
        this.roleId = roleId;
        this.personId = person.getID();
        this.id = personId;
    }


    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
}
