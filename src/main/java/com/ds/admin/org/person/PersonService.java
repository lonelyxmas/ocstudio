package com.ds.admin.org.person;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.common.JDSException;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.org.CtOrgManager;
import com.ds.common.org.CtPerson;
import com.ds.common.util.CnToSpell;
import com.ds.common.util.StringUtility;
import com.ds.config.ListResultModel;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.dsm.domain.annotation.OrgDomain;
import com.ds.esd.dsm.domain.enums.OrgDomainType;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.org.*;
import com.ds.server.OrgManagerFactory;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@EsbBeanAnnotation()
@Aggregation(
        sourceClass = PersonService.class,
        rootClass = Person.class)
@OrgDomain(type = OrgDomainType.person)
public class PersonService {


    @ModuleAnnotation(caption = "人员列表")
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.reload})
    @ResponseBody
    public List<Person> getPersons(String orgId) throws JDSException, OrgNotFoundException {
        Org org = null;
        if (orgId == null || orgId.equals("")) {
            org = OrgManagerFactory.getOrgManager().getTopOrgs().get(0);
        } else {
            org = getOrgManager().getOrgByID(orgId);
        }
        List<Person> personList = org.getPersonList();
        return personList;
    }


    @ModuleAnnotation(caption = "查找人员")
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.search})
    @ResponseBody
    public ListResultModel<List<Person>> findPersons(String patten) throws JDSException {
        List<Person> persons = new ArrayList<>();
        List<Person> personList = OrgManagerFactory.getOrgManager().getPersons();
        for (Person person : personList) {
            if (patten != null && !patten.equals("")) {
                Pattern p = Pattern.compile(patten, Pattern.CASE_INSENSITIVE);
                Matcher namematcher = p.matcher(person.getName() == null ? "" : person.getName());
                Matcher cnnamematcher = p.matcher(person.getNickName() == null ? "" : person.getNickName());
                Matcher fieldmatcher = p.matcher(person.getMobile() == null ? "" : person.getMobile());
                Matcher captionmatcher = p.matcher(person.getAccount() == null ? "" : person.getAccount());
                Matcher cnmatcher = p.matcher(CnToSpell.getFullSpell(person.getName() == null ? "" : person.getName()));
                if (namematcher.find()
                        || cnnamematcher.find()
                        || fieldmatcher.find()
                        || captionmatcher.find()
                        || cnmatcher.find()
                        ) {
                    persons.add(person);
                }
            } else {
                persons.add(person);
            }
        }
        return PageUtil.getDefaultPageList(persons);
    }


    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
    @DialogAnnotation( width = "800", height = "550")
    @ModuleAnnotation(caption = "编辑人员信息")
    @ResponseBody
    public Person getPersonInfo(String personId) throws JDSException, PersonNotFoundException {
        Person person = getOrgManager().getPersonByID(personId);
        return person;
    }

    @APIEventAnnotation(bindMenu = {CustomMenuItem.add}, autoRun = true)
    @DialogAnnotation(width = "800", height = "550")
    @ModuleAnnotation(caption = "添加人员信息")
    @ResponseBody
    public Person addPerson(String orgId) {
        CtPerson person = new CtPerson();
        person.setOrgId(orgId);
        return person;
    }


    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public void savePerson(CtPerson person) {
        if (person.getAccount() == null || person.getAccount().equals("")) {
            person.setAccount(CnToSpell.getFullSpell(person.getName()));
        }
        CtOrgAdminManager.getInstance().savePerson(person);
        CtOrgManager manager = (CtOrgManager) OrgManagerFactory.getOrgManager();
        manager.clearOrgCache(person.getOrgId());
    }


    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent}, bindMenu = CustomMenuItem.delete)
    public void delPerson(String iD) {
        String[] personIdArr = StringUtility.split(iD, ";");
        for (String id : personIdArr) {
            CtOrgAdminManager.getInstance().delPerson(id);
        }
    }

    @JSONField(serialize = false)
    ESDClient getClient() throws JDSException {
        ESDClient client = ESDFacrory.getESDClient();
        return client;
    }

    @JSONField(serialize = false)
    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.getClient().getConfigCode());
        return orgManager;
    }
}
