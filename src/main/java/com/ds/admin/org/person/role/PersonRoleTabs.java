package com.ds.admin.org.person.role;

import com.ds.admin.iorg.person.role.IPersonRoleTabs;
import com.ds.esb.config.EsbBeanAnnotation;
import com.ds.esd.custom.annotation.nav.TabItemAnnotation;
import com.ds.esd.custom.bean.nav.tab.TabItemBean;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.org.PersonRoleType;
import com.ds.org.RoleType;
import com.ds.web.annotation.Caption;
import com.ds.web.annotation.Pid;

@EsbBeanAnnotation
public class PersonRoleTabs extends TreeListItem implements IPersonRoleTabs {

    @Pid
    String personId;
    @Pid
    RoleType roleType;
    @Pid
    PersonRoleType personRoleType;

    @Caption
    String name;

    PersonRoleTabs() {

    }

    @TabItemAnnotation(customItems = PersonRoleTabItem.class)
    public PersonRoleTabs(PersonRoleTabItem tabItemBean, String personId) {

        PersonRoleTabItem personRoleType =tabItemBean;
        this.caption = tabItemBean.getPersonRoleType().getName();
        this.roleType = RoleType.valueOf(personRoleType.getPersonRoleType().getType());
        this.imageClass = tabItemBean.getImageClass();
        this.personId = personId;
        this.id = personRoleType.getType();
        this.personRoleType = personRoleType.getPersonRoleType();
    }

    @Override
    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public PersonRoleType getPersonRoleType() {
        return personRoleType;
    }

    public void setPersonRoleType(PersonRoleType personRoleType) {
        this.personRoleType = personRoleType;
    }

    @Override
    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
