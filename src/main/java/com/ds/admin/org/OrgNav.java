package com.ds.admin.org;

import com.ds.admin.iorg.IOrgNav;
import com.ds.admin.org.department.child.ChildDeparmentTree;
import com.ds.admin.org.department.view.DeparmentTree;
import com.ds.admin.org.role.RoleTree;
import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.util.TreePageUtil;
import com.ds.org.Org;
import com.ds.org.OrgManager;
import com.ds.org.RoleOtherType;
import com.ds.server.OrgManagerFactory;

import java.util.Arrays;
import java.util.List;

public class OrgNav implements IOrgNav {

    public OrgNav() {

    }

    @Override
    public TreeListResultModel<List<ChildDeparmentTree>> getDepartmentManager(String id) {
        TreeListResultModel<List<ChildDeparmentTree>> resultModel = new TreeListResultModel<>();
        try {
            List<Org> orgs = this.getOrgManager().getTopOrgs(JDSActionContext.getActionContext().getSystemCode());
            resultModel = TreePageUtil.getTreeList(orgs, ChildDeparmentTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorListResultModel<List<ChildDeparmentTree>>) resultModel).setErrdes(e.getMessage());
        }

        return resultModel;

    }

    @Override
    public TreeListResultModel<List<DeparmentTree>> getPersonManager(String id) {
        TreeListResultModel<List<DeparmentTree>> resultModel = new TreeListResultModel<List<DeparmentTree>>();
        try {
            List<Org> orgs = this.getOrgManager().getTopOrgs(JDSActionContext.getActionContext().getSystemCode());
            resultModel = TreePageUtil.getTreeList(orgs, DeparmentTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorListResultModel<List<DeparmentTree>>) resultModel).setErrdes(e.getMessage());
        }

        return resultModel;

    }


    @Override
    public TreeListResultModel<List<RoleTree>> getRoleManager(String id) {
        TreeListResultModel<List<RoleTree>> resultModel = new TreeListResultModel<List<RoleTree>>();
        resultModel = TreePageUtil.getDefaultTreeList(Arrays.asList(RoleOtherType.values()), RoleTree.class);
        return resultModel;

    }


    OrgManager getOrgManager() throws JDSException {
        OrgManager orgManager = OrgManagerFactory.getOrgManager(ESDFacrory.getESDClient().getConfigCode());
        return orgManager;
    }
}
