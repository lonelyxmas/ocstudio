package com.ds.admin.db;


import com.ds.admin.db.nav.DBNavTreeView;
import com.ds.common.database.metadata.ColInfo;
import com.ds.common.database.metadata.MetadataFactory;
import com.ds.common.database.metadata.TableInfo;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/db/table/")
@MethodChinaName(cname = "库表管理", imageClass = "spafont spa-icon-c-grid")

public class TableService {

    @MethodChinaName(cname = "获取所有数据库表")
    @RequestMapping(method = RequestMethod.POST, value = "TableList")
    @GridViewAnnotation
    @ModuleAnnotation(caption = "获取所有数据库表")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ListResultModel<List<TableGridView>> getTableList(String configKey, String url) {
        ListResultModel<List<TableGridView>> resultModel = new ListResultModel();
        try {
            ESDFacrory.getESDClient().getDbFactory(configKey).clearAll();
            List<TableInfo> tableInfos = ESDFacrory.getESDClient().getDbFactory(configKey).getTableInfos(null);
            resultModel = PageUtil.getDefaultPageList(tableInfos, TableGridView.class);
        } catch (Exception e) {
            ErrorListResultModel errorResult = new ErrorListResultModel();
            errorResult.setErrdes(e.getMessage());
            resultModel = errorResult;
        }
        return resultModel;
    }

    @MethodChinaName(cname = "删除数据库表")
    @RequestMapping(method = RequestMethod.POST, value = "delTable")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delTable(String configKey, String tablename) {
        ResultModel<Boolean> resultModel = new ResultModel<Boolean>();
        try {
            List<String> tablenames = Arrays.asList(StringUtility.split(tablename, ";"));
            ESDFacrory.getESDClient().getDbFactory(configKey).dropTable(tablenames);
        } catch (Exception e) {
            ErrorResultModel errorResult = new ErrorResultModel();
            errorResult.setErrdes(e.getMessage());
            resultModel = errorResult;
        }
        return resultModel;
    }


    @MethodChinaName(cname = "字段列表")
    @RequestMapping(method = RequestMethod.POST, value = "Cols")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", dynLoad = true, caption = "字段列表")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<DbColGridView>> getCols(String configKey, String url, String tablename) {
        ListResultModel<List<DbColGridView>> resultModel = new ListResultModel();
        try {
            if (tablename != null && !tablename.equals("")) {
                TableInfo tableInfo = ESDFacrory.getESDClient().getTableInfoByFullName(tablename);
                if (tableInfo != null) {
                    List<ColInfo> colInfos = tableInfo.getColList();
                    resultModel = PageUtil.getDefaultPageList(colInfos, DbColGridView.class);
                }

            }

        } catch (Exception e) {
            ErrorListResultModel errorResult = new ErrorListResultModel();
            errorResult.setErrdes(e.getMessage());
            resultModel = errorResult;
        }
        return resultModel;
    }


    @MethodChinaName(cname = "编辑库表信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateTable")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateTable(@RequestBody TableInfo tableInfo) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            List<TableInfo> tableList = new ArrayList<TableInfo>();
            tableList.add(tableInfo);
            String configKey = tableInfo.getConfigKey();
            if (configKey == null || configKey.equals("")) {
                configKey = tableInfo.getUrl();
            }

            MetadataFactory factory = ESDFacrory.getESDClient().getDbFactory(tableInfo.getConfigKey());
            factory.createTableByInfo(tableInfo);
            //factory.modifyTableCnname(tableList);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "添加数据库表")
    @RequestMapping(method = RequestMethod.POST, value = "AddTable")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> addTable(@RequestBody TableInfo tableInfo) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            String configKey = tableInfo.getConfigKey();
            if (configKey == null || configKey.equals("")) {
                configKey = tableInfo.getUrl();
            }
            MetadataFactory factory = ESDFacrory.getESDClient().getDbFactory(configKey);
            factory.createTableByInfo(tableInfo);
        } catch (Exception e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }

    @MethodChinaName(cname = "增加库表")
    @RequestMapping(method = RequestMethod.POST, value = "CreateTableInfo")
    @FormViewAnnotation(saveUrl = "AddTable")
    @DialogAnnotation(width = "450", height = "220")
    @ModuleAnnotation(caption = "增加库表")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.add})
    @ResponseBody
    public ResultModel<AddTableView> createTableInfo(String configKey, String url, String tablename) {
        ResultModel<AddTableView> result = new ResultModel<AddTableView>();
        return result;
    }


    @MethodChinaName(cname = "库表信息")
    @RequestMapping(method = RequestMethod.POST, value = "TableInfo")
    @DialogAnnotation
    @NavGroupViewAnnotation(saveUrl = "db.table.updateTable")
    @ModuleAnnotation(caption = "库表信息")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
    @ResponseBody
    public ResultModel<TableNav> getTableInfo(String configKey, String url, String tablename) {
        ResultModel<TableNav> result = new ResultModel<TableNav>();
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ChildTables")
    @ModuleAnnotation(dynLoad = true, caption = "资源域", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @CustomAnnotation( index = 0)
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<DBNavTreeView>> getChildTables(String configKey, String url) {
        TreeListResultModel<List<DBNavTreeView>> result = new TreeListResultModel<>();
        try {
            ESDFacrory.getESDClient().getDbFactory(configKey).clearAll();
            List<TableInfo> tableInfos = ESDFacrory.getESDClient().getDbFactory(configKey).getTableInfos(null);
            result = TreePageUtil.getTreeList(tableInfos, DBNavTreeView.class);
        } catch (Exception e) {
            ErrorListResultModel errorResult = new ErrorListResultModel();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }
        return result;
    }
}
