package com.ds.admin.db;

import com.ds.common.database.metadata.ProviderConfig;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {ProviderService.class}, event = CustomGridEvent.editor)
public class ProviderGridView {

    @CustomAnnotation(caption = "连接名称")
    public String configName;
    @CustomAnnotation(caption = "数据库标识", hidden = true, uid = true)
    public String configKey;
    @CustomAnnotation(caption = "用户名")
    public String username;
    @CustomAnnotation(caption = "密码")
    public String password;
    @CustomAnnotation(caption = "驱动")
    public String driver;
    @CustomAnnotation(caption = "连接串")
    public String serverURL;
    @CustomAnnotation(caption = "最小连接数")
    public int minConnections = 15;
    @CustomAnnotation(caption = "最大连接数")
    public int maxConnections = 50;
    @CustomAnnotation(caption = "最大空闲时间")
    public int maxIdleTime = 60;
    @CustomAnnotation(caption = "空闲检测时间")
    public int checkIdlePeriod = 60;
    @CustomAnnotation(caption = "超时时间")
    public int checkoutTimeout = 60000;
    @CustomAnnotation(caption = "链接超时时间")
    public int connectionTimeout = 30 * 1000;
    @CustomAnnotation(caption = "是否MYSQL")
    public boolean mysqlUseUnicode = true;
    @CustomAnnotation(caption = "编码")
    public String encoding = "utf-8";

    public ProviderGridView(ProviderConfig config) {
        this.configKey = config.getConfigKey();
        this.configName = config.getConfigName();
        this.driver = config.getDriver();
        this.serverURL = config.getServerURL();
        this.username = config.getUsername();
        this.password = config.getPassword();
        this.maxConnections = config.getMaxConnections();
        this.minConnections = config.getMinConnections();
        this.maxIdleTime = config.getMaxIdleTime();
        this.checkIdlePeriod = config.getCheckIdlePeriod();
        this.checkoutTimeout = config.getCheckoutTimeout();
    }


    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public int getMinConnections() {
        return minConnections;
    }

    public void setMinConnections(int minConnections) {
        this.minConnections = minConnections;
    }

    public int getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public int getMaxIdleTime() {
        return maxIdleTime;
    }

    public void setMaxIdleTime(int maxIdleTime) {
        this.maxIdleTime = maxIdleTime;
    }

    public int getCheckIdlePeriod() {
        return checkIdlePeriod;
    }

    public void setCheckIdlePeriod(int checkIdlePeriod) {
        this.checkIdlePeriod = checkIdlePeriod;
    }

    public int getCheckoutTimeout() {
        return checkoutTimeout;
    }

    public void setCheckoutTimeout(int checkoutTimeout) {
        this.checkoutTimeout = checkoutTimeout;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public boolean isMysqlUseUnicode() {
        return mysqlUseUnicode;
    }

    public void setMysqlUseUnicode(boolean mysqlUseUnicode) {
        this.mysqlUseUnicode = mysqlUseUnicode;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }


    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getServerURL() {
        return serverURL;
    }

    public void setServerURL(String serverURL) {
        this.serverURL = serverURL;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

}
