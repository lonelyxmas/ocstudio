package com.ds.admin.db;

import com.ds.admin.db.action.DBColAction;
import com.ds.common.database.metadata.TableInfo;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;

@PageBar
@GridRowCmd(menuClass = DBColAction.class)
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {TableService.class}, event = CustomGridEvent.editor)
public class TableGridView {


    @CustomAnnotation(hidden = true, uid = true)
    public String tablename;

    
    @CustomAnnotation(caption = "表名")
    public String name;

    @CustomAnnotation(caption = "注解")
    public String cnname;

    @CustomAnnotation(caption = "主键", readonly = true)
    private String pkName;


    @CustomAnnotation(caption = "数据库标识", pid = true, readonly = true)
    public String configKey;

    @CustomAnnotation( pid = true, caption = "连接串", readonly = true)
    private String url;

    public TableGridView() {

    }

    public TableGridView(TableInfo info) {
        this.name = info.getName();
        this.cnname = info.getCnname();
        this.pkName = info.getPkName();
        this.configKey = info.getConfigKey();
        this.url = info.getUrl();
        this.tablename = info.getName();
    }


    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getPkName() {
        return pkName;
    }

    public void setPkName(String pkName) {
        this.pkName = pkName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

}
