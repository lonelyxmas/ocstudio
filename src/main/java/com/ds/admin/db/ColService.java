package com.ds.admin.db;

import com.ds.common.database.metadata.ColInfo;
import com.ds.common.database.metadata.MetadataFactory;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/db/col/")
@MethodChinaName(cname = "库表配置")
public class ColService {


    @MethodChinaName(cname = "库表信息")
    @RequestMapping(method = RequestMethod.POST, value = "TableInfo")
    @NavGroupViewAnnotation(saveUrl = "db.table.updateTable")
    @ModuleAnnotation(caption = "库表信息")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.treeNodeEditor})
    @ResponseBody
    public ResultModel<TableNav> getTableInfo(String configKey, String url, String tablename) {
        ResultModel<TableNav> result = new ResultModel<TableNav>();
        return result;
    }


    @MethodChinaName(cname = "编辑字段信息")
    @RequestMapping(method = RequestMethod.POST, value = "updateCol")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> updateCol(@RequestBody ColInfo col) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            List<ColInfo> cols = new ArrayList<ColInfo>();
            cols.add(col);
            String configKey = col.getConfigKey();
            if (configKey == null || configKey.equals("")) {
                configKey = col.getUrl();
            }

            MetadataFactory factory = ESDFacrory.getESDClient().getDbFactory(configKey);
            factory.modifyTableCols(col.getTablename(), cols);
        } catch (Exception e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            result = errorResult;
        }

        return result;
    }

    @MethodChinaName(cname = "删除字段")
    @RequestMapping(method = RequestMethod.POST, value = "delCol")
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload})
    @ResponseBody
    public ResultModel<Boolean> delCol(String configKey, String tablename, String name) {
        ResultModel<Boolean> resultModel = new ResultModel<Boolean>();
        try {
            List<String> cols = Arrays.asList(StringUtility.split(name, ";"));
            ESDFacrory.getESDClient().getDbFactory(configKey).delCols(tablename, cols);
        } catch (Exception e) {
            ErrorResultModel<Boolean> errorResult = new ErrorResultModel<Boolean>();
            errorResult.setErrdes(e.getMessage());
            resultModel = errorResult;
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AddColInfo")
    @FormViewAnnotation
    @DialogAnnotation(width = "450", height = "300")
    @ModuleAnnotation( caption = "字段信息")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.add})
    @ResponseBody
    public ResultModel<DbColView> addColInfo(String configKey, String tablename, String name) {
        ResultModel<DbColView> resultModel = new ResultModel();
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "EditorColInfo")
    @ModuleAnnotation( caption = "字段信息")
    @FormViewAnnotation
    @DialogAnnotation(width = "450", height = "300")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.editor}, autoRun = true)
    @ResponseBody
    public ResultModel<ColEditorView> editorColInfo(String configKey, String tablename, String name) {
        ResultModel<ColEditorView> resultModel = new ResultModel();
        MetadataFactory factory = null;
        try {
            factory = ESDFacrory.getESDClient().getDbFactory(configKey);
            ColInfo colInfo = factory.getTableInfo(tablename).getCoInfoByName(name);
            resultModel.setData(new ColEditorView(colInfo));
        } catch (Exception e) {
            ErrorResultModel errorResult = new ErrorResultModel();
            errorResult.setErrdes(e.getMessage());
            resultModel = errorResult;
        }
        return resultModel;
    }
}
