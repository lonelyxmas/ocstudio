package com.ds.admin.db;

import com.ds.common.JDSException;
import com.ds.common.database.metadata.ColInfo;
import com.ds.common.database.metadata.TableInfo;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/db/table/")
@NavGroupAnnotation(bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
@BottomBarMenu()
@MethodChinaName(cname = "数据库表", imageClass = "spafont spa-icon-c-gallery")
public class TableNav {

    @CustomAnnotation(uid = true, hidden = true)
    public String tablename;

    public TableNav() {

    }

    @MethodChinaName(cname = "库表信息")
    @RequestMapping(method = RequestMethod.POST, value = "TableInfoView")
    @FormViewAnnotation()
    @ModuleAnnotation(dock = Dock.top, caption = "库表信息")
    @UIAnnotation(height = "160")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ResultModel<TableView> getTableInfoView(String configKey, String tablename) {
        ResultModel<TableView> result = new ResultModel<TableView>();
        ListResultModel<List<DbColView>> cols = new ListResultModel();
        try {
            TableInfo tableInfo = ESDFacrory.getESDClient().getTableInfoByFullName(tablename);
            result.setData(new TableView(tableInfo));

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public TableNav(TableNav nav) {
        this.tablename = nav.getTablename();
    }

    @MethodChinaName(cname = "字段信息")
    @RequestMapping(method = RequestMethod.POST, value = "ColListView")
    @GridViewAnnotation()
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-comboinput", dock = Dock.fill, dynLoad = true, caption = "字段信息")
    @APIEventAnnotation(autoRun = true)
    @ResponseBody
    public ListResultModel<List<DbColView>> getCols(String tablename) {
        ListResultModel<List<DbColView>> cols = new ListResultModel();
        try {
            if (tablename != null && !tablename.equals("")) {
                TableInfo tableInfo = ESDFacrory.getESDClient().getTableInfoByFullName(tablename);
                if (tableInfo != null) {
                    List<ColInfo> colInfos = tableInfo.getColList();
                    cols = PageUtil.getDefaultPageList(colInfos, DbColView.class);
                }
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return cols;
    }


    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }


}
