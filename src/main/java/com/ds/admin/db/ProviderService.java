package com.ds.admin.db;

import com.ds.admin.db.nav.DBNavTreeView;
import com.ds.common.JDSException;
import com.ds.common.database.metadata.ProviderConfig;
import com.ds.common.util.StringUtility;
import com.ds.config.*;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.util.TreePageUtil;
import com.ds.esd.workspace.MySpace;
import com.ds.esd.workspace.MySpaceConfig;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/db/provider/")
@MethodChinaName(cname = "数据源管理", imageClass = "spafont spa-icon-class")

public class ProviderService {

    @MethodChinaName(cname = "数据源")
    @RequestMapping(method = RequestMethod.POST, value = "ProviderGallery")
    @GalleryViewAnnotation
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor, autoRun = true)
    @ModuleAnnotation(imageClass = "spafont spa-icon-class", caption = "数据源")
    @ResponseBody
    public ListResultModel<List<ProviderItemView>> getProviderList(String projectId) {
        ListResultModel<List<ProviderItemView>> result = new ListResultModel<List<ProviderItemView>>();
        try {
            List<ProviderConfig> configs = getClient().getAllDbConfig();
            result = PageUtil.getDefaultPageList(configs, ProviderItemView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel();
            ((ErrorListResultModel) result).setErrcode(JDSException.APPLICATIONNOTFOUNDERROR);
            ((ErrorListResultModel) result).setErrdes(e.getMessage());

        }
        return result;
    }


    @MethodChinaName(cname = "加载数据库配置")
    @RequestMapping(value = {"ProviderList"}, method = {RequestMethod.POST})
    @GridViewAnnotation
    @ModuleAnnotation(caption = "数据源")
    @APIEventAnnotation(bindMenu = CustomMenuItem.reload, autoRun = true)
    public @ResponseBody
    ListResultModel<List<ProviderItemView>> getDbConfigList() {
        ListResultModel<List<ProviderItemView>> result = new ListResultModel<List<ProviderItemView>>();
        try {
            List<ProviderConfig> configs = getClient().getAllDbConfig();
            result = PageUtil.getDefaultPageList(configs, ProviderItemView.class);
        } catch (JDSException e) {
            result = new ErrorListResultModel();
            ((ErrorListResultModel) result).setErrcode(JDSException.APPLICATIONNOTFOUNDERROR);
            ((ErrorListResultModel) result).setErrdes(e.getMessage());

        }
        return result;
    }


    @MethodChinaName(cname = "删除数据库配置")
    @RequestMapping(value = {"removeDBConfig"}, method = {RequestMethod.POST})
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete)

    public @ResponseBody
    ResultModel<Boolean> removeDBConfig(String configKey) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            MySpace space = getClient().getSpace();
            MySpaceConfig spaceConfig = space.getConfig();
            List<ProviderConfig> newConfigs = new ArrayList<ProviderConfig>();
            Map<String, ProviderConfig> configMap = spaceConfig.getDbConfig();
            List<String> configKeyList = Arrays.asList(StringUtility.split(configKey, ";"));
            configMap.remove("");
            for (String delkey : configKeyList) {
                configMap.remove(delkey);
            }
            getClient().updateSpaceConfig(spaceConfig);
        } catch (JDSException e) {
            result = new ErrorResultModel();
            ((ErrorResultModel) result).setErrcode(JDSException.APPLICATIONNOTFOUNDERROR);
            ((ErrorResultModel) result).setErrdes(e.getMessage());

        }
        return result;
    }

    @MethodChinaName(cname = "修改数据库配置")
    @RequestMapping(value = {"ProviderInfo"}, method = {RequestMethod.POST})
    @FormViewAnnotation
    @DialogAnnotation(width = "480", height = "350")
    @ModuleAnnotation( caption = "修改数据库配置")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.editor)
    public @ResponseBody
    ResultModel<ProviderView> getProviderInfo(String configKey) {
        ResultModel<ProviderView> result = new ResultModel<ProviderView>();
        try {
            ProviderConfig config = getClient().getDbConfig(configKey);
            result.setData(new ProviderView(config));
        } catch (JDSException e) {
            result = new ErrorResultModel();
            ((ErrorResultModel) result).setErrcode(JDSException.APPLICATIONNOTFOUNDERROR);
            ((ErrorResultModel) result).setErrdes(e.getMessage());

        }
        return result;
    }

    @MethodChinaName(cname = "添加数据库配置")
    @RequestMapping(value = {"updateDBConfig"}, method = {RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = CustomMenuItem.save)
    public @ResponseBody
    ResultModel<Boolean> addDBConfig(@RequestBody ProviderConfig config) {
        ResultModel<Boolean> result = new ResultModel<Boolean>();
        try {
            getClient().updateDbConfig(config);
        } catch (JDSException e) {
            result = new ErrorResultModel();
            ((ErrorResultModel) result).setErrcode(JDSException.APPLICATIONNOTFOUNDERROR);
            ((ErrorResultModel) result).setErrdes(e.getMessage());

        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "AllRepository")
    @ModuleAnnotation(dynLoad = true, caption = "资源域", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @CustomAnnotation( index = 0)
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<DBNavTreeView>> getAllRepository(String projectVersionName, String parentId) {
        TreeListResultModel<List<DBNavTreeView>> result = new TreeListResultModel<>();
        try {
            List<ProviderConfig> configs = ESDFacrory.getESDClient().getAllDbConfig();
            result = TreePageUtil.getTreeList(configs, DBNavTreeView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ESDClient getClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }

}
