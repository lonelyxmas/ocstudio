package com.ds.admin.db;

import com.ds.common.database.metadata.TableInfo;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.tool.ui.enums.StretchType;
import com.ds.web.annotation.Required;

@FormAnnotation(stretchHeight = StretchType.last, customService = {TableService.class})
public class TableView {


    @CustomAnnotation(hidden = true, uid = true)
    public String tablename;

    @Required
    @CustomAnnotation(caption = "表名")
    public String name;
    @Required
    @CustomAnnotation(caption = "注解")
    public String cnname;

    @CustomAnnotation(caption = "主键", readonly = true)
    private String pkName;


    @CustomAnnotation(caption = "数据库标识", readonly = true)
    public String configKey;

    @FieldAnnotation(colSpan = -1)
    @CustomAnnotation(caption = "连接串", readonly = true)
    private String url;

    public TableView() {

    }

    public TableView(TableInfo info) {
        this.name = info.getName();
        this.cnname = info.getCnname();
        this.pkName = info.getPkName();
        this.configKey = info.getConfigKey();
        this.url = info.getUrl();
        this.tablename = info.getName();
    }


    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getPkName() {
        return pkName;
    }

    public void setPkName(String pkName) {
        this.pkName = pkName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

}
