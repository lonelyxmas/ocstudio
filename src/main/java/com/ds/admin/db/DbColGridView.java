package com.ds.admin.db;

import com.ds.admin.db.action.DBColAction;
import com.ds.common.database.metadata.ColInfo;
import com.ds.enums.db.ColType;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.esd.tool.ui.enums.TagCmdsAlign;

import java.sql.Types;

@PageBar(pageCount = 100)
@RowHead(selMode = SelModeType.none, gridHandlerCaption = "删除|排序", rowHandlerWidth = "10em", rowNumbered = false)
@GridRowCmd(tagCmdsAlign = TagCmdsAlign.left, menuClass = {DBColAction.class})
@GridAnnotation(customService = {ColService.class}, customMenu = {GridMenu.Reload, GridMenu.Add}, editable = true, event = CustomGridEvent.editor)
public class DbColGridView {

    @CustomAnnotation(caption = "字段名", uid = true)
    private String name;

    @CustomAnnotation(caption = "类型")
    private ColType colType = ColType.VARCHAR;

    @CustomAnnotation(caption = "长度")
    private Integer length = 20;

    @CustomAnnotation(caption = "数字精度")
    private Integer fractions = 0;

    @CustomAnnotation(caption = "是否主键")
    private Boolean pk;

    @CustomAnnotation(caption = "是否可为空")
    private Boolean canNull = true;

    @CustomAnnotation(caption = "注解")
    private String cnname;

    @CustomAnnotation(caption = "工程名", pid = true, hidden = true)
    private String projectId;

    @CustomAnnotation(caption = "表名", pid = true, hidden = true)
    private String tablename;


    @CustomAnnotation(caption = "数据类型", hidden = true)
    private int dataType = Types.VARCHAR;


    @CustomAnnotation(caption = "连接串", hidden = true)
    private String url;

    @CustomAnnotation(caption = "数据库标识", hidden = true, pid = true)
    private String configKey;

    public DbColGridView() {

    }

    public DbColGridView(ColInfo info) {
        this.name = info.getName();
        this.canNull = info.isCanNull();
        this.cnname = info.getCnname();
        this.configKey = info.getConfigKey();
        this.dataType = info.getDataType();
        this.pk = info.getPk();
        this.fractions = info.getFractions();
        this.length = info.getLength();
        this.tablename = info.getTablename();
        this.colType = info.getColType();
        this.url = info.getUrl();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public Integer getFractions() {
        return fractions;
    }

    public void setFractions(Integer fractions) {
        this.fractions = fractions;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ColType getColType() {
        return colType;
    }

    public void setColType(ColType colType) {
        this.colType = colType;
    }

    public boolean isCanNull() {
        return canNull;
    }

    public void setCanNull(boolean canNull) {
        this.canNull = canNull;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Boolean getPk() {
        return pk;
    }

    public void setPk(Boolean pk) {
        this.pk = pk;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Boolean getCanNull() {
        return canNull;
    }

    public void setCanNull(Boolean canNull) {
        this.canNull = canNull;
    }

}
