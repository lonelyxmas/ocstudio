package com.ds.admin.db.nav;

import com.ds.common.database.metadata.ProviderConfig;
import com.ds.common.database.metadata.TableInfo;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.SelModeType;

@TreeAnnotation(heplBar = true,selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class DBTreeView extends DBNavTreeView {
    public DBTreeView() {
        super();
    }

    public DBTreeView(TableInfo tableInfo) {
        super(tableInfo);
    }

    public DBTreeView(ProviderConfig config) {
        super(config);
    }

}
