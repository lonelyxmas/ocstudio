package com.ds.admin.db.nav;

import com.ds.admin.db.ColService;
import com.ds.admin.db.ProviderService;
import com.ds.admin.db.TableService;
import com.ds.common.database.metadata.ProviderConfig;
import com.ds.common.database.metadata.TableInfo;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.web.annotation.Pid;

@TreeAnnotation(heplBar = true, lazyLoad = true)
@TabsAnnotation(singleOpen = true)
public class DBNavTreeView<T extends TreeListItem> extends TreeListItem<T> {

    @Pid
    String url;
    @Pid
    String configKey;
    @Pid
    String tablename;

    public DBNavTreeView() {

    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-coin", bindService = ProviderService.class)
    public DBNavTreeView(ProjectVersion version) {
        this.caption = "所有数据源";
        this.id = "all";
    }

    @TreeItemAnnotation(imageClass = "iconfont iconchucun", bindService = TableService.class, dynDestory = true, lazyLoad = true)
    public DBNavTreeView(ProviderConfig config) {
        this.caption = config.getConfigName() == null ? config.getConfigKey() : config.getConfigName();
        this.configKey = config.getConfigKey();
        this.url = config.getServerURL();
        this.id = config.getConfigKey() == null ? config.getConfigName() : config.getConfigKey();
    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-c-grid", bindService = ColService.class)
    public DBNavTreeView(TableInfo tableInfo) {
        this.caption = tableInfo.getName() + "(" + tableInfo.getCnname() + ")";
        this.url = tableInfo.getUrl();
        this.tablename = tableInfo.getName();
        this.configKey = tableInfo.getConfigKey();
        this.id = tableInfo.getName();

    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }
}
