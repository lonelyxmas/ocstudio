package com.ds.admin.db.nav;

import com.ds.common.util.StringUtility;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.TreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/db/nav/")
@MethodChinaName(cname = "数据库导航", imageClass = "spafont spa-icon-c-treeview")
public class DatabaseNav {

    @MethodChinaName(cname = "数据库表")
    @RequestMapping(method = RequestMethod.POST, value = "TabelNavTree")
    @APIEventAnnotation(autoRun = true)
    @TreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-grid", dynLoad = true, caption = "数据库表")
    @ResponseBody
    public TreeListResultModel<List<DBNavTreeView>> getTabelManager(String id) {
        TreeListResultModel<List<DBNavTreeView>> resultModel = new TreeListResultModel<List<DBNavTreeView>>();
        DBNavTreeView<TreeListItem> dbItem = new DBNavTreeView();
        List<DBNavTreeView> items = new ArrayList<>();
        items.add(dbItem);

        if (id != null && !id.equals("")) {
            String[] orgIdArr = StringUtility.split(id, ";");
            resultModel.setIds(Arrays.asList(orgIdArr));
        } else if (dbItem.getSub() != null && dbItem.getSub().size() > 0) {
            resultModel.setIds(Arrays.asList(new String[]{dbItem.getSub().get(0).getId()}));
        }
        resultModel.setData(items);
        return resultModel;

    }


}
