package com.ds.admin.db;

import com.ds.common.database.metadata.ProviderConfig;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.ImageAnnotation;
import com.ds.esd.custom.gallery.annotation.GalleryAnnotation;
import com.ds.esd.custom.gallery.enums.CustomGalleryEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.ComponentType;

@GalleryAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {ProviderService.class}, event = CustomGalleryEvent.onDblclick)
public class ProviderItemView {

    @CustomAnnotation(pid = true, hidden = true)
    String configKey;
    @CustomAnnotation()
    String caption;
    @CustomAnnotation()
    String javaTempId;
    @CustomAnnotation(uid = true, hidden = true)
    String id;
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String comment;

    @ImageAnnotation
    @CustomAnnotation(caption = "图片", captionField = true)
    String image = "/RAD/img/project.png";


    public ProviderItemView(ProviderConfig config) {
        this.configKey = config.getConfigKey();
        this.id = config.getConfigKey() == null ? config.getConfigName() : config.getConfigKey();
        this.comment = config.getConfigName() == null ? config.getConfigKey() : config.getConfigName();
        this.caption = "";
    }


    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getJavaTempId() {
        return javaTempId;
    }

    public void setJavaTempId(String javaTempId) {
        this.javaTempId = javaTempId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
