package com.ds.test;

import com.ds.common.database.dao.DAOException;
import com.ds.common.database.dao.DAOFactory;
import com.ds.common.database.metadata.MetadataFactory;
import com.ds.common.database.metadata.TableInfo;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.ResponsePathEnum;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.module.EUModule;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequestMapping(value = "/demo/")
public class TestService {

    @RequestMapping(value = "DynTestList")
    @DynLoadAnnotation(refClassName = "test.Search")
    @DialogAnnotation
    @ModuleAnnotation()
    @ResponseBody
    public ResultModel<EUModule> getTestList(EUModule module) {
        ResultModel resultModel = new ResultModel();
        module.getComponent().getCustomFunctions().put("test", "function(){}");
        resultModel.setData(module);
        return resultModel;
    }


    @RequestMapping(value = "TestList")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.reload})
    @ResponseBody
    public ListResultModel<List<TestView>> loadTestList() {

        ListResultModel listResultModel = new ListResultModel();
        List<TestView> list = new ArrayList<>();
        TestView testView = new TestView();
        testView.setAge(18);
        testView.setName("wenzhznag");
        list.add(testView);
        listResultModel.setData(list);
        return listResultModel;
    }

    @RequestMapping(value = "Search")
    @GridViewAnnotation
    @ModuleAnnotation
    @DynLoadAnnotation
    @ResponseBody

    public ListResultModel<List<TestView>> Search(@RequestBody TestView testView) {
        ListResultModel listResultModel = new ListResultModel();
        List<TestView> list = new ArrayList<>();
        testView = new TestView();
        testView.setAge(18);
        testView.setName("wenzhznag");
        list.add(testView);
        listResultModel.setData(list);
        return listResultModel;
    }


    @RequestMapping(value = "TestForm")
    @FormViewAnnotation()
    @ModuleAnnotation()
    @DialogAnnotation
    @APIEventAnnotation(customResponseData = ResponsePathEnum.expression, bindMenu = {CustomMenuItem.add})
    @ResponseBody
    public ResultModel<TestView> getTestForm(@RequestBody TestView testView, String projectName) {
        ResultModel resultModel = new ResultModel();
        testView.setName("ddddddddddd");
        testView.setAge(15);
        resultModel.setData(testView);
        return resultModel;
    }

    @RequestMapping(value = "SearchTestForm")
    @FormViewAnnotation()
    @ModuleAnnotation
    @ResponseBody
    public ResultModel<TestView> SerchTestForm(@RequestBody TestView testView) {
        ResultModel resultModel = new ResultModel();
        testView.setName("ddddddddddd");
        resultModel.setData(testView);
        return resultModel;
    }

    @RequestMapping(value = "SaveForm")
    @APIEventAnnotation(bindMenu = {CustomMenuItem.save}, callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    @ResponseBody
    public ResultModel<Boolean> saveForm(@RequestBody TestView testView) {
        ResultModel resultModel = new ResultModel();
        MetadataFactory factory = MetadataFactory.getInstance("fdt");
        try {
            TableInfo tableInfo = factory.getInfoByClass(TestView.class);
            DAOFactory daoFactory = new DAOFactory(tableInfo);
            if (testView.getPersonId() == null || testView.getPersonId().equals("")) {
                testView.setPersonId(UUID.randomUUID().toString());
            }

            daoFactory.getDAO().update(testView);
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return resultModel;
    }


}
