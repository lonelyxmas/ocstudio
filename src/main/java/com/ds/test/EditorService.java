package com.ds.test;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.esb.util.EsbFactory;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.api.enums.ResponsePathEnum;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.tool.ui.component.Component;
import com.ds.esd.tool.ui.component.form.ComboInputComponent;
import com.ds.esd.tool.ui.component.form.ComboInputProperties;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.module.ModuleComponent;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RequestMapping(value = "/eidtor/")
public class EditorService {
    @RequestMapping(value = "onChange")
    @APIEventAnnotation(customResponseData = ResponsePathEnum.expression, customRequestData = RequestPathEnum.form)
    @DynLoadAnnotation
    @ResponseBody
    public ListResultModel<List<Component>> onChange(@RequestBody TestView testView, ModuleComponent moduleComponent) {
        ListResultModel<List<Component>> listResultModel = new ListResultModel();
        List<Component> components = new ArrayList<>();
        List<ComboInputComponent> comboInputComponents = null;
        try {
            comboInputComponents = moduleComponent.getRealModuleComponent().findComponents(ComponentType.ComboInput, null);
            ((ComboInputProperties) comboInputComponents.get(1).getProperties()).setDisabled(true);
            ((ComboInputProperties) comboInputComponents.get(1).getProperties()).setValue(14);
            components.addAll(comboInputComponents);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        listResultModel.setData(components);
        return listResultModel;
    }

}
