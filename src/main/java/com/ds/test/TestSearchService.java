package com.ds.test;

import com.ds.common.JDSException;
import com.ds.config.ListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.form.FullClassFormComponent;
import com.ds.esd.custom.grid.FullGridComponent;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.component.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping(value = "/demo/testSearch/")
public class TestSearchService {

    @RequestMapping(value = "Search")
    //@GridViewAnnotation
    // @APIEventAnnotation(bindMenu = {CustomMenuItem.search})
    @DynLoadAnnotation()
    @ModuleAnnotation(bindService = SearchTestView.class, caption = "流程展示")
    @ResponseBody
    public ListResultModel<List<Component>> Search(@RequestBody TestView testView) {
        ListResultModel<List<Component>> listResultModel = new ListResultModel<List<Component>>();
        Map<String, Object> valueMap = JDSActionContext.getActionContext().getContext();
        EUModule module = null;
        try {
            module = ESDFacrory.getESDClient().buildDynCustomModule(FullClassFormComponent.class, valueMap, true);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        Map<String, String> test = new HashMap<>();
        test.put("fff", "ffffff");
        test.put("prjectName", "ddddd");
        module.addParams(test);
        List<Component> components = module.getTopComponents(true);


        listResultModel.setData(components);
        return listResultModel;
    }


    @RequestMapping(value = "SearchTest")
    @DynLoadAnnotation()
    @GridViewAnnotation
    @ModuleAnnotation(bindService = SearchTestView.class)
    @APIEventAnnotation(bindMenu = {CustomMenuItem.reload}, autoRun = true, customRequestData = RequestPathEnum.ctx)
    @ResponseBody
    public ListResultModel<List<SearchTestView>> searchTest(@RequestBody TestView testView, FullGridComponent component) {
        ListResultModel<List<SearchTestView>> resultModel = new ListResultModel();

        List<SearchTestView> list = new ArrayList<>();
        SearchTestView searchTestView = new SearchTestView();
        searchTestView.setAge(18);
        searchTestView.setPersonId("test111");
        searchTestView.setName("wenzhznag");
        list.add(searchTestView);

        Map<String, String> test = new HashMap<>();
        test.put("fff", "ffffff");
        test.put("projectName", "ddddd");
        test.put("name", "dddddddddd");
        component.addParams(test);

        resultModel.setData(list);

        return resultModel;
    }


}
