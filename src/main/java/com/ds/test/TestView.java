package com.ds.test;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.enums.db.ColType;
import com.ds.enums.db.DBField;
import com.ds.enums.db.DBTable;
import com.ds.esd.custom.action.CustomListAnnotation;
import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.ComboListBoxAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridRowCmd;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.component.item.CmdItem;
import com.ds.esd.tool.ui.enums.AlignType;
import com.ds.esd.tool.ui.enums.CmdButtonType;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.json.ItemSubDeserializer;
import com.ds.web.annotation.Required;

import java.util.ArrayList;
import java.util.List;

@GridRowCmd(menuClass = OtherButton.class)
@GridAnnotation(customMenu = {GridMenu.Add, GridMenu.Reload, GridMenu.Delete}, event = {CustomGridEvent.editor})
@FormAnnotation(col = 1, customService = TestService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Search})
@DBTable(tableName = "User3", configKey = "fdt", primaryKey = "personId")
public class TestView {
    @CustomAnnotation(hidden = true, uid = true)
    @DBField(dbFieldName = "PERSONID")
    String personId;


    @Required
    @CustomAnnotation(caption = "姓名", expression = "this.age+this.name")
    @DBField(dbFieldName = "NAME")
    String name;

    @ComboListBoxAnnotation
    @ComboInputAnnotation()
    @CustomListAnnotation(dynLoad = true)
    @FieldAnnotation(serviceClass = EditorService.class)
    @CustomAnnotation(caption = "性別")
    @DBField(dbFieldName = "SEX")
    SexEnum sex;

    @JSONField(serializeUsing = ItemSubDeserializer.class)
    List<CmdItem> tagCmds = new ArrayList<>();

    @ComboInputAnnotation(expression = "this.age+this.name", inputType = ComboInputType.spin)
    @CustomAnnotation(caption = "年龄" )
    @FieldAnnotation(serviceClass = EditorService.class)
    @DBField(dbFieldName = "AGE", dbType = ColType.INTEGER)
    Integer age;


    public List<CmdItem> getTagCmds() {
        CmdItem item = new CmdItem("id", "测试", "");
        item.setButtonType(CmdButtonType.button);
        CmdItem item2 = new CmdItem("getTestFormbutton", "测试2", "");
        item.setLocation(AlignType.left);
        item.setTag("row");
        item2.setTag("row");

        CmdItem item3 = new CmdItem("id3", "测试3", "");
        item3.setTag("row");
        tagCmds.add(item);
        tagCmds.add(item2);
        tagCmds.add(item3);
        return tagCmds;
    }

    public void setTagCmds(List<CmdItem> tagCmds) {
        this.tagCmds = tagCmds;
    }


    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }


}
