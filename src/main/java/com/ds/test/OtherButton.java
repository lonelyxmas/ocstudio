package com.ds.test;

import com.ds.common.database.dao.DAOException;
import com.ds.common.database.metadata.MetadataFactory;
import com.ds.common.database.metadata.TableInfo;
import com.ds.config.ResultModel;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/demo/")
public class OtherButton {

    @RequestMapping(value = "CreateTable")
    @CustomAnnotation(caption = "创建 数据库表", expression = "this.readOnly!=null")
    @APIEventAnnotation()
    public ResultModel<Boolean> getTestForm(TestView testView) throws DAOException {
        MetadataFactory factory = MetadataFactory.getInstance("fdt");
        TableInfo tableInfo = factory.getInfoByClass(TestView.class);
        factory.createTableByInfo(tableInfo);

        return new ResultModel<>();
    }
}
