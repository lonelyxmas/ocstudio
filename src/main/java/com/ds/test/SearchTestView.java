package com.ds.test;

import com.ds.esd.custom.annotation.ComboInputAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.enums.ComboInputType;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.web.annotation.Required;

@FormAnnotation(col = 1, bottombarMenu = {CustomFormMenu.Search})
public class SearchTestView {

    @CustomAnnotation(hidden = true, uid = true)
    String personId;
    @Required
    @CustomAnnotation(caption = "姓名", pid = true)
    String name;

    @CustomAnnotation(caption = "性別")
    SexEnum sex;

    @ComboInputAnnotation(inputType = ComboInputType.spin)
    @CustomAnnotation(caption = "年龄")
    Integer age;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
