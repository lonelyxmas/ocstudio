package com.ds.test.tree;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.esd.dsm.view.field.FieldFormConfig;
import com.ds.esd.custom.DataComponent;
import com.ds.esd.custom.form.FormLayoutModule;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.component.form.formlayout.FormLayoutComponent;
import com.ds.esd.tool.ui.component.form.formlayout.FormLayoutProperties;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.enums.event.enums.PanelEventEnum;

import java.util.List;
import java.util.Map;

public class ClassFormComponent<T extends FormLayoutProperties, K extends PanelEventEnum, N extends Map<String, Object>> extends FormLayoutComponent<T, K> implements DataComponent<N> {
    @JSONField(serialize = false)
    private final FormLayoutModule layoutModule;

    public ClassFormComponent(EUModule module, List<FieldFormConfig> fieldList, Map dbMap) {
        super(module.getName() + "Form", (T) new FormLayoutProperties());
        this.getProperties().setDock(Dock.fill);
        this.layoutModule = new FormLayoutModule(module, (FormLayoutComponent<FormLayoutProperties, PanelEventEnum>) this, fieldList, dbMap, null);

    }

    @Override
    @JSONField(serialize = false)
    public N getData() {
        return (N) layoutModule.getValueMap();
    }

    @Override
    public void setData(N data) {
        this.getModuleComponent().fillFormValues(data, false);
    }
}
