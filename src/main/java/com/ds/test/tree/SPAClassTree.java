package com.ds.test.tree;

import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.web.RequestMethodBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@TreeAnnotation(heplBar = true, lazyLoad = true, selMode = SelModeType.singlecheckbox, customService = SPACompoentService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class SPAClassTree extends TreeListItem {

    public SPAClassTree(String dsmId) {
        this.caption = "实体信息";
        this.setIniFold(false);
        this.addTagVar("dsmId", dsmId);
        this.setImageClass("spafont spa-icon-coin");
        this.id = "all";
        try {
            DSMFactory.getInstance().reload();
            List<ESDClass> esdClasses = DSMFactory.getInstance().getClassManager().getAllAggView();
            if (esdClasses.size() > 0) {
                for (ESDClass child : esdClasses) {
                    this.addChild(new SPAClassTree(child, dsmId));
                }
        }
        } catch (JDSException e) {
            e.printStackTrace();
        }
    }


    public SPAClassTree(ESDClass esdClass, String dsmId) {
        this.caption = esdClass.getName() + "(" + esdClass.getDesc() + ")";
        this.setEuClassName(esdClass.getClassName());
        this.setImageClass("spafont spa-icon-c-grid");
        this.setIniFold(false);
        this.tagVar = new HashMap<>();
        this.tagVar.put("className", esdClass.getClassName());
        this.tagVar.put("dsmId", dsmId);
        this.tagVar.put("serviceName", esdClass.getClassName());
        this.setIniFold(true);
        this.id = esdClass.getClassName();
        this.setSub(new ArrayList());

    }

    public SPAClassTree(EUModule module, RequestMethodBean methodBean, CustomMenuItem item, String dsmId) {
        this.caption = module.getName() + "(" + item.getName() + ")";
        this.setEuClassName(methodBean.getClassName());
        this.setImageClass(item.getImageClass());
        this.setIniFold(false);
        this.tagVar = new HashMap<>();
        this.tagVar.put("className", module.getClassName());
        this.tagVar.put("dsmId", dsmId);
        this.id = module.getClassName();

    }
}
