package com.ds.test.tree;

import com.ds.config.ListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.CustomMethodInfo;
import com.ds.esd.custom.CustomViewFactory;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.api.enums.ResponsePathEnum;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.component.Component;
import com.ds.esd.tool.ui.component.data.APICallerComponent;
import com.ds.esd.tool.ui.component.data.APICallerProperties;
import com.ds.esd.tool.ui.component.panel.PanelProperties;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.web.APIConfig;
import com.ds.web.APIConfigFactory;
import com.ds.web.RequestMethodBean;
import com.ds.web.json.JSONData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(path = "/demo/component/")
@MethodChinaName(cname = "实体管理", imageClass = "spafont spa-icon-c-gallery")
public class SPACompoentService {

    @CustomAnnotation(pid = true, hidden = true)
    String dsmId;
    @CustomAnnotation(pid = true, hidden = true)
    String projectId;


    @RequestMapping(method = RequestMethod.POST, value = "loadChild")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild, customRequestData = {RequestPathEnum.SPA_projectName})
    @ResponseBody
    public TreeListResultModel<List<SPAClassTree>> loadChild(String projectName, String id, String dsmId, @JSONData Map<String, String> tagVar) {
        TreeListResultModel<List<SPAClassTree>> result = new TreeListResultModel<>();
        List<SPAClassTree> menuTrees = new ArrayList<>();
        try {
            String serviceName = tagVar.get("serviceName");
            ESDClass esdClass = DSMFactory.getInstance().getClassManager().getAggEntityByName(serviceName, dsmId, true);
            List<CustomMethodInfo> esdMethods = new ArrayList<>();
            esdMethods.addAll(esdClass.getMethodsList());
            esdMethods.addAll(esdClass.getOtherMethodsList());
            Collections.sort(esdMethods);
            APIConfig config = APIConfigFactory.getInstance().getAPIConfig(esdClass.getClassName());
            for (CustomMethodInfo field : esdMethods) {
                RequestMethodBean methodBean = config.getMethodByName(field.getInnerMethod().getName());
                projectName = JDSActionContext.getActionContext().getParams("projectName").toString();
                if (methodBean != null) {
                    APICallerComponent component = new APICallerComponent(methodBean);
                    APICallerProperties properties = component.getProperties();
                    properties.setExpression(field.getExpression());
                    properties.setImageClass(field.getImageClass());
                    Set<CustomMenuItem> bindMenus = properties.getBindMenu();
                    ComponentType[] bindTypes = new ComponentType[]{};
                    MethodConfig methodAPIBean = CustomViewFactory.getInstance().getMethodAPIBean(methodBean.getUrl(), projectName);
                    EUModule module = CustomViewFactory.getInstance().getViewByMethod(methodAPIBean, projectName, new HashMap<>());
                    if (bindMenus != null && bindMenus.size() > 0) {
                        for (CustomMenuItem bindMenu : bindMenus) {
                            // if (checkComponentType(bindTypes, bindMenu.getBindTypes())) {
                            if (bindMenu.getMenu() != null && bindMenu.getMenu().actions().length > 0 && module != null) {
                                menuTrees.add(new SPAClassTree(module, methodBean, bindMenu, dsmId));
                            }

                            //  }
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        result.setData(menuTrees);

        return result;
    }

    public boolean checkComponentType(ComponentType[] currComTypes, ComponentType[] componentTypes) {
        if (componentTypes == null || componentTypes.length == 0) {
            return true;
        }
        for (ComponentType currComType : currComTypes) {
            for (ComponentType type : componentTypes) {
                if (type.equals(currComType)) {
                    return true;
                }
            }
        }
        return false;
    }


    @MethodChinaName(cname = "保存实体关系")
    @RequestMapping(value = {"save"}, method = {RequestMethod.POST})
    @APIEventAnnotation(isAllform = true, callback = {CustomCallBack.Close}, bindMenu = CustomMenuItem.treeSave
            , customRequestData = {RequestPathEnum.SPA_className, RequestPathEnum.RAD_selectItems}
            , customResponseData = {ResponsePathEnum.spacomponent})
    public @ResponseBody
    ListResultModel<List<Component>> save(String ESDClassTree, String className, String projectName, String[] selectItems) {
        ListResultModel<List<Component>> result = new ListResultModel<List<Component>>();
        List<Component> components = new ArrayList<>();
        try {
            EUModule euModule = ESDFacrory.getESDClient().getModule(className, projectName);
            if (selectItems != null && selectItems.length > 0) {
                String selectItem = selectItems[0];
                Component component = euModule.getComponent().findComponentByAlias(selectItem);
                EUModule tempModule = ESDFacrory.getESDClient().getModule(ESDClassTree, projectName);
                if (tempModule == null) {
                    tempModule = ESDFacrory.getESDClient().getDSMModule(ESDClassTree, JDSActionContext.getActionContext().getContext());
                }
                List<Component> componentList = tempModule.getTopComponents(true);


                if (!(component.getProperties() instanceof PanelProperties)) {
                    Component parentcomponent = component.getParent();
                    parentcomponent.removeChildren(component);
                    for (Component child : componentList) {
                        parentcomponent.addChildren(child);
                    }
                    components.add(parentcomponent);
                } else {
                    for (Component child : componentList) {
                        component.addChildren(child);
                    }
                    components.add(component);
                }


            }
            result.setData(components);
        } catch (Exception e) {
            e.printStackTrace();

        }

        return result;

    }


    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getDsmId() {
        return dsmId;
    }

    public void setDsmId(String dsmId) {
        this.dsmId = dsmId;
    }
}
