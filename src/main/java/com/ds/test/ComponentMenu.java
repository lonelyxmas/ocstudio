package com.ds.test;

import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.test.tree.SPAClassTree;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping(value = {"/test/component/"})
@MethodChinaName(cname = "工具")
@MenuBarMenu(menuType = CustomMenuType.component, caption = "组件渲染", index = 0, imageClass = "spafont spa-icon-c-tabs")
public class ComponentMenu {

    @RequestMapping(value = {"ESDClassTree"}, method = {RequestMethod.POST})
    @PopTreeViewAnnotation()
    @DialogAnnotation(width = "400")
    @ModuleAnnotation(caption = "绑定实体", dynLoad = true, dock = Dock.fill, imageClass = "fa fa-calendar-plus-o")
    @APIEventAnnotation(isAllform = true, autoRun = true, customRequestData = {RequestPathEnum.SPA_className, RequestPathEnum.RAD_selectItems})
    @ResponseBody
    public TreeListResultModel<List<SPAClassTree>> getESDTreeView(String dsmId, String className, String[] selectItems) {
        TreeListResultModel<List<SPAClassTree>> result = new TreeListResultModel<List<SPAClassTree>>();
        try {
            if (dsmId != null && dsmId.equals("")) {
                dsmId = className;
            }
            ViewInst bean = DSMFactory.getInstance().getViewManager().getViewInstById(dsmId);
            List<SPAClassTree> treeViews = new ArrayList<>();
            treeViews.add(new SPAClassTree(dsmId));
            result.setData(treeViews);
            result.setIds(bean.getViewEntityList());
        } catch (JDSException e) {
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }
}
