package com.ds.system;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.admin.temp.JavaTempNavTree;
import com.ds.dsm.website.DomainTempNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.index.annotation.IndexAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutItemAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.enums.DSMType;
import com.ds.esd.formula.manager.formula.item.FormulaTypeItem;
import com.ds.esd.tool.ui.enums.LayoutType;
import com.ds.esd.tool.ui.enums.PosType;
import com.ds.esd.util.TreePageUtil;
import com.ds.system.sys.ServiceBeanNav;
import com.ds.system.sys.SystemNav;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/system/")
@IndexAnnotation
@LayoutAnnotation(transparent = false, type = LayoutType.vertical, items = {@LayoutItemAnnotation(panelBgClr = "#3498DB", size = 28, pos = PosType.before, locked = true, moveDisplay = false, cmd = false)})
public class SystemIndex {

    @MethodChinaName(cname = "系统管理")
    @RequestMapping(method = RequestMethod.POST, value = "SystemNav")
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "xui-icon-bullet", caption = "系统管理")
    @CustomAnnotation(index = 0)
    @ResponseBody
    public ResultModel<SystemNav> getSystemNav(String projectId) {
        return new ResultModel<SystemNav>();
    }


    @MethodChinaName(cname = "公式管理")
    @RequestMapping(method = RequestMethod.POST, value = "FormulaManager")
    @NavTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-function")
    @CustomAnnotation(index = 1)
    @ResponseBody
    public TreeListResultModel<List<FormulaTypeItem>> getFormulaManager(String id) {
        TreeListResultModel<List<FormulaTypeItem>> resultModel = new TreeListResultModel<List<FormulaTypeItem>>();
        List<FormulaTypeItem> items = new ArrayList<>();
        FormulaTypeItem root = new FormulaTypeItem();
        items.add(root);
        if (id != null && !id.equals("")) {
            String[] orgIdArr = StringUtility.split(id, ";");
            resultModel.setIds(Arrays.asList(orgIdArr));
        } else {
            resultModel.setIds(Arrays.asList(new String[]{root.getFristClassItem(root).getId()}));
        }
        resultModel.setData(items);
        return resultModel;

    }


    @RequestMapping(method = RequestMethod.POST, value = "WebSiteTreeNav")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-cssbox", dynLoad = true, caption = "模板站点")
    @CustomAnnotation(index = 2)
    @ResponseBody
    public TreeListResultModel<List<DomainTempNavTree>> getDSMTempTreeNav(String id) {
        TreeListResultModel<List<DomainTempNavTree>> resultModel = new TreeListResultModel<List<DomainTempNavTree>>();
        DomainTempNavTree viewItem = new DomainTempNavTree("dsm.website.WebSiteList", null);
        List<DomainTempNavTree> items = new ArrayList<>();
        items.add(viewItem);
        if (id != null && !id.equals("")) {
            String[] orgIdArr = StringUtility.split(id, ";");
            resultModel.setIds(Arrays.asList(orgIdArr));
        } else {
            resultModel.setIds(Arrays.asList(new String[]{viewItem.getFristClassItem(viewItem).getId()}));
        }
        resultModel.setData(items);
        return resultModel;

    }


    @MethodChinaName(cname = "模板管理")
    @RequestMapping(method = RequestMethod.POST, value = "CodeTemps")
    @APIEventAnnotation(autoRun = true)
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", dynLoad = true, caption = "模板管理")
    @CustomAnnotation(index = 3)
    @ResponseBody
    public TreeListResultModel<List<JavaTempNavTree>> getTempManager(String id) {
        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel<List<JavaTempNavTree>>();

        JavaTempNavTree tempNavTree = new JavaTempNavTree("CodeTemps", "模板", DSMType.all.getImageClass());
        List<JavaTempNavTree> childs = TreePageUtil.fillObjs(Arrays.asList(DSMType.values()), JavaTempNavTree.class, Arrays.asList(DSMType.repository.getType()));
        tempNavTree.setSub(childs);
        resultModel.setData(Arrays.asList(tempNavTree));

        return resultModel;

    }

//    @RequestMapping(method = RequestMethod.POST, value = "ViewTemps")
//    @APIEventAnnotation(autoRun = true)
//    @NavTreeViewAnnotation
//    @ModuleAnnotation(imageClass = "spafont spa-icon-config", index = 4, dynLoad = true, caption = "视图配置")
//    @ResponseBody
//    public TreeListResultModel<List<JavaTempNavTree>> getViewTemps(String id) {
//        TreeListResultModel<List<JavaTempNavTree>> resultModel = new TreeListResultModel<List<JavaTempNavTree>>();
//        resultModel = TreePageUtil.getTreeList(Arrays.asList(DSMType.aggregation), JavaTempNavTree.class);
//
//        return resultModel;
//
//    }


    @MethodChinaName(cname = "服务管理")
    @RequestMapping(method = RequestMethod.POST, value = "ServiceBeanNav")
    @NavTreeViewAnnotation
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf1", caption = "服务管理")
    @CustomAnnotation(index = 4)
    @ResponseBody
    public ResultModel<ServiceBeanNav> getServiceBeanNav(String projectId) {
        return new ResultModel<ServiceBeanNav>();
    }


    @JSONField(serialize = false)
    public ESDClient getEsdClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }
}
