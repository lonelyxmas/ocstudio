package com.ds.system.sys;

import com.ds.cluster.service.ServerEventFactory;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.esb.config.EsbFlowType;
import com.ds.esb.config.manager.EsbBean;
import com.ds.esb.config.manager.EsbBeanFactory;
import com.ds.esb.config.manager.ExpressionTempBean;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.server.JDSServer;
import com.ds.system.sys.view.esbbean.LocalEsbBeanGridView;
import com.ds.system.sys.view.esbbean.RemoteEsbBeanGridView;
import com.ds.system.sys.view.service.LocalServiceGridView;
import com.ds.system.sys.view.service.MsgServiceGridView;
import com.ds.system.sys.view.service.RemoteServiceGridView;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/system/service/")
@TabsAnnotation(closeBtn = true)
public class ServiceBeanNav {


    @RequestMapping(value = {"AllLocalService"}, method = {RequestMethod.GET, RequestMethod.POST})
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj", caption = "本地服务管理")
    @GridViewAnnotation(editorPath = "LocalEsbBeanInfo", delPath = "delService")
    public @ResponseBody
    ListResultModel<List<LocalEsbBeanGridView>> getAllLocalService() {
        List<EsbBean> esbBeans = EsbBeanFactory.getInstance().getLocalBeanList();
        ListResultModel userStatusInfo = PageUtil.getDefaultPageList(esbBeans, LocalEsbBeanGridView.class);
        return userStatusInfo;
    }


    @RequestMapping(value = {"AllRemoteService"}, method = {RequestMethod.GET, RequestMethod.POST})
    @GridViewAnnotation(editorPath = "RemoteEsbBeanInfo", delPath = "delService")
    @ModuleAnnotation(imageClass = "spafont spa-icon-alignm", caption = "远程服务管理")
    public @ResponseBody
    ListResultModel<List<RemoteEsbBeanGridView>> getAllRemoteService() {
        List<EsbBean> esbBeans = EsbBeanFactory.getInstance().getRemoveBeanList();
        ListResultModel userStatusInfo = PageUtil.getDefaultPageList(esbBeans, RemoteEsbBeanGridView.class);
        return userStatusInfo;
    }

    @GridViewAnnotation(editorPath = "ServiceInfo", delPath = "delService")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-audio", caption = "所有消息事件信息")
    @RequestMapping(value = {"AllAllEvent"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<Set<MsgServiceGridView>> getAllEvent() {
        Set<ExpressionTempBean> beanSet = ServerEventFactory.getInstance().getAllRegisterEvent();
        List<ExpressionTempBean> esbBeans = Arrays.asList(beanSet.toArray(new ExpressionTempBean[beanSet.size()]));
        ListResultModel userStatusInfo = PageUtil.getDefaultPageList(esbBeans, MsgServiceGridView.class);
        return userStatusInfo;
    }


    @RequestMapping(value = {"AllBusBeanTemp"}, method = {RequestMethod.GET, RequestMethod.POST})
    @GridViewAnnotation(editorPath = "ServiceInfo", delPath = "delService")
    @ModuleAnnotation(imageClass = "spafont spa-icon-action1", caption = "获取注册服务信息")
    public @ResponseBody
    ListResultModel<List<LocalServiceGridView>> getBusBeanTemp(String flowTypes) {
        List<EsbFlowType> types = new ArrayList<EsbFlowType>();
        if (flowTypes != null) {
            if (flowTypes.equals("all")) {
                types.addAll(Arrays.asList(EsbFlowType.values()));
            } else {
                String[] flowTypeArr = StringUtility.split(flowTypes, ";");
                for (String app : flowTypeArr) {
                    types.add(EsbFlowType.fromType(app));
                }
            }


        }

        List<ExpressionTempBean> esbBeans = (List<ExpressionTempBean>) EsbBeanFactory.getInstance().getServiceBeanByFlowType(types.toArray(new EsbFlowType[]{}));
        ListResultModel userStatusInfo = PageUtil.getDefaultPageList(esbBeans, LocalServiceGridView.class);
        return userStatusInfo;
    }

    @GridViewAnnotation(editorPath = "ServiceInfo", delPath = "delService")
    @ModuleAnnotation(caption = "获取远程服务信息")
    @RequestMapping(value = {"AllRemoteClusterSevice"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<RemoteServiceGridView>> getRemoteClusterSevice() {
        ListResultModel<List<RemoteServiceGridView>> userStatusInfo = new ListResultModel<List<RemoteServiceGridView>>();
        try {
            userStatusInfo = PageUtil.getDefaultPageList(JDSServer.getInstance().getClusterSevice(), RemoteServiceGridView.class);
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }

        return userStatusInfo;
    }

}
