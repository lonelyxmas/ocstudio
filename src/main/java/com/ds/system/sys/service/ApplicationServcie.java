package com.ds.system.sys.service;

import com.ds.config.CApplication;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.NavDomain;
import com.ds.esd.dsm.domain.enums.NavDomainType;
import com.ds.server.JDSServer;
import com.ds.server.eumus.ConfigCode;
import com.ds.system.sys.view.ApplicationFormView;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@MethodChinaName(cname = "应用配置", imageClass = "spafont spa-icon-conf1")
@RequestMapping("/system/application/")
@Aggregation(type = AggregationType.customDomain, rootClass = ApplicationServcie.class)
@NavDomain(type = NavDomainType.menu)
public class ApplicationServcie {


    @MethodChinaName(cname = "编辑应用信息")
    @RequestMapping(method = RequestMethod.POST, value = "ApplicationInfo")
    @FormViewAnnotation(saveUrl = "saveApplication")
    @DialogAnnotation(width = "350", height = "300")
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf1", caption = "编辑应用信息")
    @ResponseBody
    public ResultModel<ApplicationFormView> getApplicationInfo(String code) {
        ResultModel<ApplicationFormView> resultModel = new ResultModel<ApplicationFormView>();

        try {
            CApplication cApplication = JDSServer.getClusterClient().getApplication(ConfigCode.fromType(code));
            ApplicationFormView appView = new ApplicationFormView(cApplication);
            resultModel.setData(appView);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorResultModel<ApplicationFormView>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;


    }

    @MethodChinaName(cname = "保存应用信息")
    @RequestMapping(value = {"saveApplication"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> savePerson(@RequestBody CApplication application) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();


        return userStatusInfo;

    }

    @MethodChinaName(cname = "删除应用")
    @RequestMapping(value = {"delApplication"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent})
    public @ResponseBody
    ResultModel<Boolean> delPerson(String code) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();

        return userStatusInfo;

    }

}
