package com.ds.system.sys.service;

import com.ds.cluster.ServerNode;
import com.ds.cluster.ServerNodeList;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;

import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.NavDomain;
import com.ds.esd.dsm.domain.enums.NavDomainType;
import com.ds.server.JDSServer;
import com.ds.server.eumus.ConfigCode;
import com.ds.system.sys.view.ServerNodeFormView;
import com.ds.system.sys.view.ServerNodeGridView;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@MethodChinaName(cname = "集群服务", imageClass = "spafont spa-icon-c-toolbar")
@RequestMapping("/system/node/")
@Aggregation(type=AggregationType.customDomain,rootClass = ServerNodeServcie.class)
@NavDomain(type=NavDomainType.menu)
public class ServerNodeServcie {


    @GridViewAnnotation(editorPath = "system.sys.ServerNodeInfo", delPath = "delServerNode")
    @ModuleAnnotation( caption = "获取所有节点服务")
    @RequestMapping(value = {"AllServerNodes"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<ServerNodeGridView>> getAllServerNodes(String code) {
        ServerNodeList serverNodeList = JDSServer.getClusterClient().getServerNodeListByConfigCode(ConfigCode.fromType(code));
        ListResultModel<List<ServerNodeGridView>> userStatusInfo = PageUtil.getDefaultPageList(serverNodeList.getServerNodeList(), ServerNodeGridView.class);
        return userStatusInfo;
    }


    @RequestMapping(method = RequestMethod.POST, value = "ServerNodeInfo")
    @ModuleAnnotation(caption = "编辑节点信息")
    @DialogAnnotation(width = "350", height = "300")
    @FormViewAnnotation(saveUrl = "saveServerNode")
    @ResponseBody
    public ResultModel<ServerNodeFormView> getServerNodeInfo(String id) {
        ResultModel<ServerNodeFormView> resultModel = new ResultModel<ServerNodeFormView>();

        try {
            ServerNode cApplication = JDSServer.getClusterClient().getServerNodeById(id);
            ServerNodeFormView serverNodeView = new ServerNodeFormView(cApplication);
            resultModel.setData(serverNodeView);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorResultModel<ServerNodeFormView>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;


    }

    @MethodChinaName(cname = "保存节点信息")
    @RequestMapping(value = {"saveServerNode"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> saveServerNode(@RequestBody ServerNode serverNode) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();


        return userStatusInfo;

    }

    @MethodChinaName(cname = "删除节点")
    @RequestMapping(value = {"delServerNode"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent})
    public @ResponseBody
    ResultModel<Boolean> delServerNode(String id) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();

        return userStatusInfo;

    }

}
