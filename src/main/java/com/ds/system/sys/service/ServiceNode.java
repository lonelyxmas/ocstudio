package com.ds.system.sys.service;

import com.ds.common.logging.Log;
import com.ds.common.logging.LogFactory;
import com.ds.common.util.ClassUtility;
import com.ds.esb.config.EsbFlowType;
import com.ds.esb.config.manager.EsbBeanFactory;
import com.ds.esb.config.manager.ExpressionParameter;
import com.ds.esb.config.manager.ServiceBean;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceNode {

    protected transient static final Log logger = LogFactory.getLog("DAO", ServiceNode.class);

    public String id;

    public String path;

    public boolean group = false;

    public String key;

    public String imageClass = "spafot spa-icon-c-";

    public String caption;

    public String expression;

    public List<ExpressionParameter> parameters;


    public boolean iniFold = false;

    public List<ServiceNode> sub;

    public Map<String, String> tagVar = new HashMap<String, String>();


    public ServiceNode(String parent, Method methodBean) {
        this.caption = methodBean.getName();
        this.id = parent + "." + methodBean.getName();
    }

    public ServiceNode(ServiceBean bean) {
        this.caption = bean.getId() + "(" + (bean.getName() == null ? bean.getId() : bean.getName()) + ")";
        this.id = this.getPath();
        this.expression = bean.getExpression();
        this.parameters = bean.getParams();

        try {
            Class clazz = ClassUtility.loadClass(bean.getClazz());
            Method[] methods = clazz.getDeclaredMethods();
            if (methods.length > 0) {
                this.sub = new ArrayList<>();
                for (Method methodBean : methods) {
                    sub.add(new ServiceNode("$" + bean.getId(), methodBean));
                }
            }

        } catch (ClassNotFoundException e) {
            logger.error(e);
        }

    }

    public ServiceNode(EsbFlowType type) {
        this.caption = type.getType() + "(" + type.getName() + ")";
        this.id = type.getType();
        List<? extends ServiceBean> serviceBeans = EsbBeanFactory.getInstance().getServiceBeanByFlowType(type);
        if (serviceBeans.size() > 0) {
            this.sub = new ArrayList<>();
            for (ServiceBean bean : serviceBeans) {
                sub.add(new ServiceNode(bean));
            }
        }
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public boolean isIniFold() {
        return iniFold;
    }

    public void setIniFold(boolean iniFold) {
        this.iniFold = iniFold;
    }

    public List<ServiceNode> getSub() {
        return sub;
    }

    public void setSub(List<ServiceNode> sub) {
        this.sub = sub;
    }

    public Map<String, String> getTagVar() {
        return tagVar;
    }

    public void setTagVar(Map<String, String> tagVar) {
        this.tagVar = tagVar;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public List<ExpressionParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<ExpressionParameter> parameters) {
        this.parameters = parameters;
    }

}
