package com.ds.system.sys.service;

import com.ds.cluster.ServerNode;
import com.ds.common.JDSException;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;

import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.NavDomain;
import com.ds.esd.dsm.domain.enums.NavDomainType;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.server.JDSServer;
import com.ds.server.service.SysWebManager;
import com.ds.system.sys.view.ServerNodeFormView;
import com.ds.system.sys.view.system.SystemFormView;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@MethodChinaName(cname = "子系统", imageClass = "spafont spa-icon-coin")
@RequestMapping("/system/sys/")
@Aggregation(type = AggregationType.customDomain,rootClass =SystemService.class )
@NavDomain(type = NavDomainType.menu)
public class SystemService {
    @RequestMapping(method = RequestMethod.POST, value = "SystemInfo")
    @FormViewAnnotation(saveUrl = "updateSystemInfo")
    @DialogAnnotation(width = "650", height = "350")
    @ModuleAnnotation(caption = "编辑服务信息")
    @APIEventAnnotation(bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<SystemFormView> getSystemInfo(String sysId) {
        ResultModel<SystemFormView> resultModel = new ResultModel<SystemFormView>();
        try {
            ServerNode serverNode = JDSServer.getClusterClient().getServerNodeById(sysId);
            SystemFormView serviceView = new SystemFormView(serverNode);
            resultModel.setData(serviceView);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorResultModel();
            ((ErrorResultModel<SystemFormView>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;
    }


    @MethodChinaName(cname = "更新系统服务信息")
    @RequestMapping(value = {"updateSystemInfo"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> updateSystemInfo(@RequestBody SystemFormView subSystem) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        try {
            getSysWebManager().saveSystemInfo(subSystem).get();
        } catch (JDSException e) {
            userStatusInfo = new ErrorResultModel();
            ((ErrorResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorResultModel) userStatusInfo).setErrdes(e.getMessage());
        }

        return userStatusInfo;
    }


    @GridViewAnnotation
    @MethodChinaName(cname = "获取节点信息")
    @ModuleAnnotation()
    @RequestMapping(value = {"AppServerNodeInfo"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ResultModel<ServerNodeFormView> getServerNodeInfo(String code) {
        ServerNode serverNode = JDSServer.getClusterClient().getServerNodeById(code);
        ResultModel<ServerNodeFormView> userStatusInfo = new ResultModel<ServerNodeFormView>();
        userStatusInfo.setData(new ServerNodeFormView(serverNode));
        return userStatusInfo;
    }


    SysWebManager getSysWebManager() {
        return (SysWebManager) EsbUtil.parExpression("$SysWebManager");
    }

}
