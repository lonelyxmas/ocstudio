package com.ds.system.sys.service;

import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.manager.EsbBean;
import com.ds.esb.config.manager.EsbBeanFactory;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.NavDomain;
import com.ds.esd.dsm.domain.enums.NavDomainType;
import com.ds.system.sys.view.esbbean.LocalEsbBeanFormView;
import com.ds.system.sys.view.esbbean.RemoteEsbBeanFormView;
import com.ds.system.sys.view.service.LocalServiceFormView;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@MethodChinaName(cname = "服务接口", imageClass = "spafont spa-icon-settingprj")
@RequestMapping("/system/service/")
@Aggregation(type = AggregationType.customDomain,rootClass =ServiceServcie.class )
@NavDomain(type = NavDomainType.menu)
public class ServiceServcie {

    @RequestMapping(method = RequestMethod.POST, value = "LocalServiceInfo")
    @DialogAnnotation( width = "600", height = "480")
    @FormViewAnnotation(caption = "编辑服务信息", saveUrl = "saveService")
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj")
    @ResponseBody
    public ResultModel<LocalServiceFormView> getLocalServiceInfo(String id) {
        ResultModel<LocalServiceFormView> resultModel = new ResultModel<LocalServiceFormView>();
        try {
            ServiceBean serviceBean = EsbBeanFactory.getInstance().getEsbBeanById(id);
            LocalServiceFormView serviceView = new LocalServiceFormView(serviceBean);
            resultModel.setData(serviceView);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorResultModel<LocalServiceFormView>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;


    }

    @MethodChinaName(cname = "本地服务信息")
    @RequestMapping(method = RequestMethod.POST, value = "LocalEsbBeanInfo")
    @DialogAnnotation(width = "550", height = "400")
    @ModuleAnnotation(caption = "编辑服务信息", imageClass = "spafont spa-icon-alignm")
    @FormViewAnnotation(saveUrl = "saveService")
    @ResponseBody
    public ResultModel<LocalEsbBeanFormView> getLocalEsbBeanInfo(String id) {
        ResultModel<LocalEsbBeanFormView> resultModel = new ResultModel<LocalEsbBeanFormView>();

        try {
            EsbBean esbBean = EsbBeanFactory.getInstance().getTopEsbBeanById(id);
            LocalEsbBeanFormView serviceView = new LocalEsbBeanFormView(esbBean);
            resultModel.setData(serviceView);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorResultModel<LocalEsbBeanFormView>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;


    }


    @MethodChinaName(cname = "编辑服务信息")
    @RequestMapping(method = RequestMethod.POST, value = "RemoteEsbBeanInfo")
    @FormViewAnnotation(saveUrl = "saveService")
    @DialogAnnotation(width = "550", height = "400")
    @ModuleAnnotation(caption = "编辑服务信息", imageClass = "xui-icon-upload")
    @ResponseBody
    public ResultModel<RemoteEsbBeanFormView> getRemoteEsbBeanInfo(String id) {
        ResultModel<RemoteEsbBeanFormView> resultModel = new ResultModel<RemoteEsbBeanFormView>();

        try {
            EsbBean esbBean = EsbBeanFactory.getInstance().getTopEsbBeanById(id);
            RemoteEsbBeanFormView serviceView = new RemoteEsbBeanFormView(esbBean);
            resultModel.setData(serviceView);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorResultModel<RemoteEsbBeanFormView>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;


    }

    @MethodChinaName(cname = "保存服务信息")
    @RequestMapping(value = {"saveService"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> savePerson(@RequestBody ServiceBean service) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        return userStatusInfo;

    }

    @MethodChinaName(cname = "删除服务")
    @RequestMapping(value = {"delService"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent})
    public @ResponseBody
    ResultModel<Boolean> delService(String id) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();

        return userStatusInfo;

    }

}
