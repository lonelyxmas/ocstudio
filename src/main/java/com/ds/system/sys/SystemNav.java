package com.ds.system.sys;

import com.ds.cluster.ServerNode;
import com.ds.cluster.ServerNodeList;
import com.ds.common.JDSException;
import com.ds.config.CApplication;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.jds.core.esb.EsbUtil;
import com.ds.server.JDSServer;
import com.ds.server.SubSystem;
import com.ds.server.eumus.ConfigCode;
import com.ds.server.eumus.SystemType;
import com.ds.server.service.SysWebManager;
import com.ds.system.sys.view.ApplicationGridView;
import com.ds.system.sys.view.ServerNodeGridView;
import com.ds.system.sys.view.system.DevSystemGridView;
import com.ds.system.sys.view.system.SystemGridView;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/system/sys/")
@TabsAnnotation(closeBtn = true)
public class SystemNav {

    @GridViewAnnotation()
    @ModuleAnnotation(imageClass = "spafont spa-icon-coin", caption = "获取所有系统")
    @RequestMapping(value = "AllSystem", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<SystemGridView>> getAllSystem() {
        ListResultModel<List<SystemGridView>> userStatusInfo = new ListResultModel<List<SystemGridView>>();
        try {
            List<ServerNode> serverNodes = JDSServer.getInstance().getClusterClient().getAllServer();
            List<ServerNode> sysNodes = new ArrayList<>();
            for (ServerNode node : serverNodes) {
                SubSystem subSystem = JDSServer.getInstance().getClusterClient().getSystem(node.getId());
                if (subSystem != null && subSystem.getType() != null && !subSystem.getType().equals(SystemType.dev)) {
                    sysNodes.add(node);
                }
            }
            userStatusInfo = PageUtil.getDefaultPageList(sysNodes, SystemGridView.class);
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }
        return userStatusInfo;
    }


    @GridViewAnnotation()
    @MethodChinaName(cname = "租户管理")
    @ModuleAnnotation(imageClass = "bpmfont bpmgongzuoliu2")
    @RequestMapping(value = "AllDevSystem", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<DevSystemGridView>> getAllDevSystem() {
        ListResultModel<List<DevSystemGridView>> userStatusInfo = new ListResultModel<List<DevSystemGridView>>();
        try {
            List<SubSystem> systems = getSysWebManager().getAllSAASSystemInfo().get();
            List<SubSystem> sysNodes = new ArrayList<>();
            for (SubSystem subSystem : systems) {
                if (subSystem != null && subSystem.getType() != null) {
                    sysNodes.add(subSystem);
                }
            }
            userStatusInfo = PageUtil.getDefaultPageList(sysNodes, DevSystemGridView.class);
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }
        return userStatusInfo;
    }

    @MethodChinaName(cname = "获取所有应用配置")
    @GridViewAnnotation(editorPath = "system.application.ApplicationInfo", addPath = "system.application.ApplicationInfo")
    @ModuleAnnotation(imageClass = "spafont spa-icon-conf1")
    @RequestMapping(value = {"AllApplications"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<ApplicationGridView>> getApplications() {
        List<CApplication> applications = JDSServer.getClusterClient().getApplications();
        ListResultModel userStatusInfo = PageUtil.getDefaultPageList(applications, ApplicationGridView.class);
        return userStatusInfo;
    }


    @GridViewAnnotation(editorPath = "system.node.AppServerNodeInfo", addPath = "system.node.ApplicationInfo")
    @ModuleAnnotation(imageClass = "spafont spa-icon-c-toolbar")
    @MethodChinaName(cname = "获取所有节点服务")
    @RequestMapping(value = {"AllServerNodes"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<ServerNodeGridView>> getServerNodes(String code) {
        ServerNodeList serverNodeList = JDSServer.getClusterClient().getServerNodeListByConfigCode(ConfigCode.fromType(code));
        ListResultModel<List<ServerNodeGridView>> userStatusInfo = PageUtil.getDefaultPageList(serverNodeList.getServerNodeList(), ServerNodeGridView.class);
        return userStatusInfo;
    }

    SysWebManager getSysWebManager() {
        return (SysWebManager) EsbUtil.parExpression("$SysWebManager");
    }
}
