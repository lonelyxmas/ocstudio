package com.ds.system.sys.view.system;

import com.ds.cluster.ServerNode;
import com.ds.cluster.service.ServerEventFactory;
import com.ds.common.JDSException;
import com.ds.common.util.CnToSpell;
import com.ds.esb.config.TokenType;
import com.ds.esb.config.manager.ExpressionTempBean;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.esd.custom.annotation.ComboPopAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.FileUploadAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.org.OrgManager;
import com.ds.org.OrgNotFoundException;
import com.ds.org.Person;
import com.ds.org.PersonNotFoundException;
import com.ds.server.JDSServer;
import com.ds.server.OrgManagerFactory;
import com.ds.server.SubSystem;
import com.ds.server.eumus.ConfigCode;
import com.ds.server.eumus.SystemStatus;
import com.ds.server.eumus.SystemType;
import com.ds.system.sys.service.DevSystemService;
import com.ds.web.annotation.Required;

import java.util.List;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add}, event = CustomGridEvent.editor, customService = DevSystemService.class)
public class DevSystemGridView implements SubSystem {


    @ComboPopAnnotation(src = "AddOrg2Sys")
    @CustomAnnotation(caption = "租户名称")
    @Required
    String orgname;
    @Required
    @CustomAnnotation(caption = "系统名称")
    String name;
    @ComboPopAnnotation(src = "AddPerson2Sys")
    @Required
    @CustomAnnotation(caption = "管理员")
    String personname;

    @Required
    @CustomAnnotation(caption = "英文名称")
    String enname;


    @FieldAnnotation(rowHeight = "50", colSpan = -1)
    @CustomAnnotation(caption = "发布地址")
    String url;
    @Required
    @CustomAnnotation(caption = "配置选项")
    ConfigCode configname;

    @CustomAnnotation(caption = "状态", readonly = true)
    SystemStatus status;
    @Required
    @CustomAnnotation(caption = "系统类型")
    SystemType type;


    @CustomAnnotation(hidden = true)
    String orgId;

    @ComboPopAnnotation(src = "AddSpace2Sys")
    @Required
    @CustomAnnotation(caption = "空间地址")
    private String vfsPath;


    @CustomAnnotation(hidden = true)
    String adminId;

    @CustomAnnotation(caption = "访问类型")
    TokenType tokenType;

    @FileUploadAnnotation(src = "/system/AttachUPLoad")

    @FieldAnnotation(componentType = ComponentType.FileUpload, colSpan = -1)
    @CustomAnnotation( caption = "略缩图")
    String icon;


    @CustomAnnotation(hidden = true)
    String personid;

    @CustomAnnotation(uid = true, hidden = true)
    String sysId;


    @CustomAnnotation(hidden = true)
    Integer serialindex = 0;

    @CustomAnnotation(hidden = true)
    String repeatEventKey = "";


    public DevSystemGridView() {

    }

    public DevSystemGridView(SubSystem subSystem) {
        this.name = subSystem.getName();
        this.enname = subSystem.getEnname();
        this.configname = subSystem.getConfigname() == null ? ConfigCode.app : subSystem.getConfigname();
        this.icon = subSystem.getIcon();
        this.orgId = subSystem.getOrgId();
        this.vfsPath = subSystem.getVfsPath();
        this.tokenType = subSystem.getTokenType() == null ? this.tokenType = TokenType.user : subSystem.getTokenType();
        this.adminId = subSystem.getAdminId();
        this.type = subSystem.getType() == null ? SystemType.dev : subSystem.getType();
        this.url = subSystem.getUrl() == null ? "http://" + enname + ".itjds.net" : subSystem.getUrl();
        this.sysId = subSystem.getSysId();
        if (this.name != null) {
            this.enname = CnToSpell.getFirstSpell(this.name);
        }

        OrgManager orgManager = OrgManagerFactory.getOrgManager(this.configname);
        try {
            if (this.orgId != null) {
                this.orgname = orgManager.getOrgByID(orgId).getName();
                this.personname = orgManager.getPersonByID(this.adminId).getName();
            }

        } catch (OrgNotFoundException e) {
            e.printStackTrace();
        } catch (PersonNotFoundException e) {
            e.printStackTrace();
        }


    }

    public DevSystemGridView(ServerNode node) throws JDSException {
        SubSystem subSystem = JDSServer.getInstance().getClusterClient().getSystem(node.getId());
        if (subSystem == null) {
            throw new JDSException("subSystem is null systemId is[" + node.getId() + "] systemName is[" + node.getName() + "]");
        }
        OrgManager orgManager = OrgManagerFactory.getOrgManager(subSystem.getConfigname());
        status = JDSServer.getClusterClient().getSystemStatus(node.getId());
        this.name = subSystem.getName();
        this.enname = subSystem.getEnname();
        this.configname = subSystem.getConfigname();
        this.icon = subSystem.getIcon();
        this.orgId = subSystem.getOrgId();
        this.vfsPath = subSystem.getVfsPath();
        this.tokenType = subSystem.getTokenType();
        this.adminId = subSystem.getAdminId();
        this.type = subSystem.getType();

        if (orgManager.getTopOrgs(subSystem.getSysId()).size() > 0) {
            this.orgname = orgManager.getTopOrgs(subSystem.getSysId()).get(0).getName();
        }
        try {
            if (subSystem.getAdminId() != null && orgManager.getPersonByID(subSystem.getAdminId()) != null) {
                Person person = orgManager.getPersonByID(subSystem.getAdminId());
                this.personname = person.getName();
            }
        } catch (PersonNotFoundException e) {
            e.printStackTrace();
        }

        ServerEventFactory factory = ServerEventFactory.getInstance();
        List<ExpressionTempBean> serviceBeans = factory.getRegisterEventByCode(node.getId());

        for (ServiceBean serviceBean : serviceBeans) {
            repeatEventKey = repeatEventKey + serviceBean.getName() + ",";
        }
        if (repeatEventKey.endsWith(",")) {
            repeatEventKey = repeatEventKey.substring(0, repeatEventKey.length() - 1);
        }
        this.type = subSystem.getType();
        this.url = subSystem.getUrl();
        this.sysId = subSystem.getSysId();
    }

    public String getRepeatEventKey() {
        return repeatEventKey;
    }

    public void setRepeatEventKey(String repeatEventKey) {
        this.repeatEventKey = repeatEventKey;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getPersonname() {
        return personname;
    }

    public void setPersonname(String personname) {
        this.personname = personname;
    }

    public String getEnname() {
        return enname;
    }

    public void setEnname(String enname) {
        this.enname = enname;
    }

    public String getIcon() {
        return icon;
    }

    @Override
    public String getOrgId() {
        return orgId;
    }


    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    @Override
    public void setSerialindex(Integer serialindex) {

        this.serialindex = serialindex;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }


    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public void setVfsPath(String vfsPath) {
        this.vfsPath = vfsPath;
    }

    @Override
    public void setOrgId(String orgId) {

        this.orgId = orgId;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SystemStatus getStatus() {
        return status;
    }

    public void setStatus(SystemStatus status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getVfsPath() {
        return vfsPath;
    }

    @Override
    public Integer getSerialindex() {
        return serialindex;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public ConfigCode getConfigname() {
        return configname;
    }

    @Override
    public TokenType getTokenType() {
        return tokenType;
    }

    public void setConfigname(ConfigCode configname) {
        this.configname = configname;
    }


    public SystemType getType() {
        return type;
    }

    @Override
    public String getAdminId() {
        return adminId;
    }

    public void setType(SystemType type) {
        this.type = type;
    }
}
