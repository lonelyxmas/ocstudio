package com.ds.system.sys.view.service;

import com.ds.common.util.ClassUtility;
import com.ds.esb.config.EsbFlowType;
import com.ds.esb.config.manager.EsbBeanFactory;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.system.sys.service.ServiceNode;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@PageBar
@GridAnnotation(customService = LocalService.class, customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, event = CustomGridEvent.editor)
public class LocalServiceGridView {
    @CustomAnnotation(caption = "名称")
    public String caption;

    @CustomAnnotation(caption = "表达式")
    public String expression;

    @CustomAnnotation(caption = "UID", uid = true)
    public String id;

    @CustomAnnotation(caption = "路径", hidden = true)
    public String path;

    @CustomAnnotation(caption = "是否", hidden = true)
    public boolean group = false;

    @CustomAnnotation(caption = "标识")
    public String key;

    @CustomAnnotation(caption = "图标", hidden = true)
    public String imageClass = "spafot spa-icon-c-";

    @CustomAnnotation(caption = "是否展开", hidden = true)
    public boolean iniFold = false;

    public List<ServiceNode> sub;


    public LocalServiceGridView(String parent, Method methodBean) {
        this.caption = methodBean.getName();
        this.id = parent + "." + methodBean.getName();
    }

    public LocalServiceGridView(ServiceBean bean) {
        this.caption = bean.getId() + "(" + (bean.getName() == null ? bean.getId() : bean.getName()) + ")";
        this.id = this.getPath();
        this.expression = bean.getExpression();

        try {
            Class clazz = ClassUtility.loadClass(bean.getClazz());
            Method[] methods = clazz.getDeclaredMethods();
            if (methods.length > 0) {
                this.sub = new ArrayList<>();
                for (Method methodBean : methods) {
                    sub.add(new ServiceNode("$" + bean.getId(), methodBean));
                }
            }

        } catch (ClassNotFoundException e) {
            // logger.error(e);
        }

    }

    public LocalServiceGridView(EsbFlowType type) {
        this.caption = type.getType() + "(" + type.getName() + ")";
        this.id = type.getType();
        List<? extends ServiceBean> serviceBeans = EsbBeanFactory.getInstance().getServiceBeanByFlowType(type);
        if (serviceBeans.size() > 0) {
            this.sub = new ArrayList<>();
            for (ServiceBean bean : serviceBeans) {
                sub.add(new ServiceNode(bean));
            }
        }
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public boolean isIniFold() {
        return iniFold;
    }

    public void setIniFold(boolean iniFold) {
        this.iniFold = iniFold;
    }

    public List<ServiceNode> getSub() {
        return sub;
    }

    public void setSub(List<ServiceNode> sub) {
        this.sub = sub;
    }


    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }


}
