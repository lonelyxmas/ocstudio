package com.ds.system.sys.view;

import com.ds.common.JDSException;
import com.ds.common.org.CtOrgAdminManager;
import com.ds.common.query.Operator;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.DeparmentFService;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.org.Org;
import com.ds.org.query.OrgCondition;
import com.ds.org.query.OrgConditionKey;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@TreeAnnotation(heplBar = true, caption = "选择部门", bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, selMode = SelModeType.singlecheckbox, customService = {DeparmentFService.class})
public class DeparmentTopTree extends TreeListItem {
    @CustomAnnotation(pid = true)
    String pattern = "";
    @CustomAnnotation(pid = true)
    String sysId;


    public DeparmentTopTree(String pattern) throws JDSException {
        imageClass = "bpmfont bpm-gongzuoliu-moxing";
        this.caption = "组织机构";

        this.id = "allOrg";
        OrgCondition condition = new OrgCondition(OrgConditionKey.ORG_PARENTID, Operator.EQUALS, CtOrgAdminManager.OrgRootId);
        List<Org> orgs = CtOrgAdminManager.getInstance().findOrgs(condition).get();

        for (Org org : orgs) {
            if (checkChild(pattern, org)) {
                this.addChild(new DeparmentTopTree(pattern, org));
            }
        }
    }

    public DeparmentTopTree(String pattern, Org org) {
        this.caption = org.getName();
        this.setId(org.getOrgId());
        this.setIniFold(false);
        this.tagVar = new HashMap<>();
        this.imageClass = "spafont spa-icon-c-treeview";
        this.tagVar.put("orgId", org.getOrgId());
        if (org.getChildrenList() != null && org.getChildrenList().size() > 0) {
            for (Org childorg : org.getChildrenList()) {
                if (checkChild(pattern, childorg)) {
                    this.addChild(new DeparmentTopTree(pattern, childorg));
                }

            }
        }

    }

    private boolean checkChild(String pattern, Org org) {
        if (pattern(pattern, org)) {
            return true;
        }
        List<Org> orgs = org.getChildrenRecursivelyList();
        for (Org childOrg : orgs) {
            if (pattern(pattern, childOrg)) {
                return true;
            }
        }
        return false;
    }

    private boolean pattern(String pattern, Org org) {

        if (pattern != null && !pattern.equals("")) {
            Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher namematcher = p.matcher(org.getName());
            if (namematcher.find()) {
                return true;
            }
        } else {
            return true;
        }

        return false;

    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
