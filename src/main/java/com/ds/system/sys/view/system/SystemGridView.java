package com.ds.system.sys.view.system;

import com.ds.cluster.ServerNode;
import com.ds.cluster.service.ServerEventFactory;
import com.ds.common.JDSException;
import com.ds.esb.config.TokenType;
import com.ds.esb.config.manager.ExpressionTempBean;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FileUploadAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.org.OrgManager;
import com.ds.server.JDSServer;
import com.ds.server.OrgManagerFactory;
import com.ds.server.SubSystem;
import com.ds.server.eumus.ConfigCode;
import com.ds.server.eumus.SystemStatus;
import com.ds.server.eumus.SystemType;
import com.ds.system.sys.service.SystemService;

import java.util.List;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add}, event = CustomGridEvent.editor,customService = SystemService.class)
public class SystemGridView implements SubSystem {


    
    @CustomAnnotation(caption = "系统名称")
    String name;


    @CustomAnnotation(caption = "英文名称")
    String enname;


    @CustomAnnotation(caption = "系统类型")
    SystemType type;


    @CustomAnnotation(caption = "配置选项")
    ConfigCode configname;

    @CustomAnnotation(hidden = true)
    private String vfsPath;


    @CustomAnnotation(caption = "状态", readonly = true)
    SystemStatus status;

    @CustomAnnotation(caption = "访问类型")
    TokenType tokenType;


    @CustomAnnotation(caption = "URL")
    String url;

    @CustomAnnotation(caption = "订阅消息")
    String repeatEventKey = "";

    @CustomAnnotation(hidden = true)
    String adminId;

    @FileUploadAnnotation(src = "/custom/system/AttachUPLoad")
    @CustomAnnotation(caption = "略缩图")
    String icon;

    @CustomAnnotation(uid = true, hidden = true)
    String sysId;

    @CustomAnnotation(hidden = true)
    String orgId;


    @CustomAnnotation(hidden = true)
    Integer serialindex = 0;


    public SystemGridView() {

    }

    public SystemGridView(ServerNode node) throws JDSException {
        SubSystem subSystem = JDSServer.getInstance().getClusterClient().getSystem(node.getId());
        if (subSystem == null) {
            throw new JDSException("subSystem is null systemId is[" + node.getId() + "] systemName is[" + node.getName() + "]");
        }
        OrgManager orgManager = OrgManagerFactory.getOrgManager(subSystem.getConfigname());
        status = JDSServer.getClusterClient().getSystemStatus(node.getId());
        this.name = subSystem.getName();
        this.enname = subSystem.getEnname();
        this.configname = subSystem.getConfigname();
        this.icon = subSystem.getIcon();
        this.orgId = subSystem.getOrgId();
        this.vfsPath = subSystem.getVfsPath();
        this.tokenType = subSystem.getTokenType();
        this.adminId = subSystem.getAdminId();
        this.type = subSystem.getType();


        ServerEventFactory factory = ServerEventFactory.getInstance();
        List<ExpressionTempBean> serviceBeans = factory.getRegisterEventByCode(node.getId());

        for (ServiceBean serviceBean : serviceBeans) {
            repeatEventKey = repeatEventKey + serviceBean.getName() + ",";
        }
        if (repeatEventKey.endsWith(",")) {
            repeatEventKey = repeatEventKey.substring(0, repeatEventKey.length() - 1);
        }
        this.type = subSystem.getType();
        this.url = subSystem.getUrl();
        this.sysId = subSystem.getSysId();
    }

    public String getRepeatEventKey() {
        return repeatEventKey;
    }

    public void setRepeatEventKey(String repeatEventKey) {
        this.repeatEventKey = repeatEventKey;
    }


    public String getEnname() {
        return enname;
    }

    public void setEnname(String enname) {
        this.enname = enname;
    }

    public String getIcon() {
        return icon;
    }

    @Override
    public String getOrgId() {
        return orgId;
    }


    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    @Override
    public void setSerialindex(Integer serialindex) {
        this.serialindex = serialindex;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public void setVfsPath(String vfsPath) {
        this.vfsPath = vfsPath;
    }

    @Override
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SystemStatus getStatus() {
        return status;
    }

    public void setStatus(SystemStatus status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getVfsPath() {
        return vfsPath;
    }

    @Override
    public Integer getSerialindex() {
        return serialindex;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ConfigCode getConfigname() {
        return configname;
    }

    @Override
    public TokenType getTokenType() {
        return tokenType;
    }

    public void setConfigname(ConfigCode configname) {
        this.configname = configname;
    }

    public SystemType getType() {
        return type;
    }

    @Override
    public String getAdminId() {
        return adminId;
    }

    public void setType(SystemType type) {
        this.type = type;
    }
}
