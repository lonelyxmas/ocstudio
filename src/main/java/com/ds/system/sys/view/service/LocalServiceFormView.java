package com.ds.system.sys.view.service;

import com.ds.common.util.ClassUtility;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.EsbFlowType;
import com.ds.esb.config.manager.EsbBeanFactory;
import com.ds.esb.config.manager.ExpressionParameter;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.esd.custom.annotation.ComboModuleAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.enums.AppendType;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.system.sys.service.ServiceNode;
import com.ds.web.annotation.Required;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@BottomBarMenu
@FormAnnotation(col = 2, customService = LocalService.class, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class LocalServiceFormView {
    @CustomAnnotation(caption = "名称")
    public String caption;

    @CustomAnnotation(caption = "表达式")
    public String expression;

    @CustomAnnotation(caption = "UID", uid = true)
    public String id;

    @CustomAnnotation(caption = "路径", hidden = true)
    public String path;

    @CustomAnnotation(caption = "是否", hidden = true)
    public boolean group = false;

    @Required
    @CustomAnnotation(caption = "标识")
    public String key;

    @CustomAnnotation(caption = "图标", hidden = true)
    public String imageClass = "spafot spa-icon-c-";

    @CustomAnnotation(caption = "是否展开", hidden = true)
    public boolean iniFold = false;

    @MethodChinaName(cname = "公式参数")
    @ComboModuleAnnotation(append = AppendType.ref)
    @CustomAnnotation()
    @FieldAnnotation(colSpan = -1, rowHeight = "300")
    public List<ExpressionParameter> parameters;


    public List<ExpressionParameter> getParameters() {
        return parameters;
    }


    public void setParameters(List<ExpressionParameter> parameters) {
        this.parameters = parameters;
    }


    public LocalServiceFormView(String parent, Method methodBean) {
        this.caption = methodBean.getName();
        this.id = parent + "." + methodBean.getName();
    }

    public LocalServiceFormView(ServiceBean bean) {
        this.caption = bean.getId() + "(" + (bean.getName() == null ? bean.getId() : bean.getName()) + ")";
        this.id = this.getPath();
        this.expression = bean.getExpression();
        this.parameters = bean.getParams();

    }

    public LocalServiceFormView(EsbFlowType type) {
        this.caption = type.getType() + "(" + type.getName() + ")";
        this.id = type.getType();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public boolean isIniFold() {
        return iniFold;
    }

    public void setIniFold(boolean iniFold) {
        this.iniFold = iniFold;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }


}
