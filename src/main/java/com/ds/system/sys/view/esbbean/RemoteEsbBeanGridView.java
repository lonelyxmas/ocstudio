package com.ds.system.sys.view.esbbean;

import com.ds.esb.config.ContextType;
import com.ds.esb.config.EsbBeanType;
import com.ds.esb.config.TokenType;
import com.ds.esb.config.manager.EsbBean;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.GridColItemAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.server.SubSystem;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, event = CustomGridEvent.editor)
public class RemoteEsbBeanGridView {

    @CustomAnnotation(caption = "名称")
    private String cnname;

    @CustomAnnotation(caption = "标识", uid = true)
    private String id;

    @CustomAnnotation(caption = "服务标识", hidden = true)
    private String serverKey;


    @CustomAnnotation(caption = "访问安全")
    private TokenType tokenType = TokenType.guest;

    @CustomAnnotation(caption = "生命周期")
    private ContextType type;

    @CustomAnnotation(caption = "用户名")
    private String username;

    @GridColItemAnnotation(width = "20em")
    @CustomAnnotation(caption = "配置地址")
    private String path;
    @GridColItemAnnotation(width = "20em")
    @CustomAnnotation(caption = "服务地址")
    private String serverUrl;

    @CustomAnnotation(caption = "描述")
    private String desc;


    @CustomAnnotation(caption = "服务类型", hidden = true)
    private EsbBeanType esbtype = EsbBeanType.Local;


    public RemoteEsbBeanGridView(EsbBean bean) {
        this.id = bean.getId();
        this.cnname = bean.getCnname();
        this.tokenType = bean.getTokenType();
        this.esbtype = bean.getEsbtype();
        this.serverUrl = bean.getServerUrl();
        if (serverUrl != null && !serverUrl.equals("")) {
            this.esbtype = EsbBeanType.Cluster;
        }

        this.desc = bean.getDesc();
        this.serverKey = bean.getServerKey();
        this.type = bean.getType();
        this.path = bean.getPath();
        this.username = bean.getUsername();

    }

    public RemoteEsbBeanGridView(SubSystem system) {
        this.id = system.getEnname();
        this.cnname = system.getName();
        this.tokenType = system.getTokenType();
        this.esbtype = EsbBeanType.Cluster;
        this.serverUrl = system.getUrl();
        this.desc = system.getVfsPath();
        this.serverKey = system.getEnname();
        this.type = ContextType.Server;
        this.path = system.getUrl();
        this.username = system.getAdminId();


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public ContextType getType() {
        return type;
    }

    public void setType(ContextType type) {
        this.type = type;
    }

    public EsbBeanType getEsbtype() {
        return esbtype;
    }

    public void setEsbtype(EsbBeanType esbtype) {
        this.esbtype = esbtype;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getServerKey() {
        return serverKey;
    }

    public void setServerKey(String serverKey) {
        this.serverKey = serverKey;
    }


}