package com.ds.system.sys.view;

import com.ds.common.util.CnToSpell;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.formula.service.PersonFService;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.org.Org;
import com.ds.org.Person;
import com.ds.server.OrgManagerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@TreeAnnotation(heplBar = true, caption = "选择人员", bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, selMode = SelModeType.singlecheckbox, customService = {PersonFService.class})
public class PersonPopTree extends TreeListItem {


    @CustomAnnotation(pid = true)
    String pattern = "";
    @CustomAnnotation(pid = true)
    String sysId;


    public PersonPopTree(String pattern) {
        this.imageClass = "spafont spa-icon-c-treeview";
        this.caption = "组织机构";
        this.id = "allOrg";
        List<Org> orgs = OrgManagerFactory.getOrgManager().getTopOrgs();
        for (Org org : orgs) {
            this.addChild(new PersonPopTree(pattern, org));
        }
    }

    private boolean pattern(String pattern, Person person) {
        if (pattern != null && !pattern.equals("")) {
            Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher namematcher = p.matcher(person.getName());
            Matcher fieldmatcher = p.matcher(CnToSpell.getFullSpell(person.getName()));
            if (namematcher.find() || fieldmatcher.find()) {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    public PersonPopTree(String pattern, Org org) {
        this.caption = org.getName();
        this.setId(org.getOrgId());
        imageClass = "bpmfont bpm-gongzuoliu-moxing";
        this.setIniFold(false);
        this.tagVar = new HashMap<>();
        this.tagVar.put("orgId", org.getOrgId());
        if (org.getChildrenList() != null && org.getChildrenList().size() > 0) {
            for (Org childorg : org.getChildrenList()) {
                this.addChild(new PersonPopTree(pattern, childorg));
            }
        }

        if (org.getPersonList() != null && org.getPersonList().size() > 0) {
            for (Person person : org.getPersonList()) {
                if (pattern(pattern, person)) {
                    this.addChild(new PersonPopTree(person));
                }

            }
        }
    }

    public PersonPopTree(Person person) {
        this.caption = person.getName();
        this.setId(person.getID());
        this.setIniFold(false);
        this.setImageClass("bpmfont bpmgongzuoliu");
        this.tagVar = new HashMap<>();
        this.tagVar.put("personId", person.getID());
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
