package com.ds.system.sys.view;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.cluster.ServerNode;
import com.ds.common.JDSException;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.ImageAnnotation;
import com.ds.esd.custom.gallery.annotation.GalleryAnnotation;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.server.JDSServer;
import com.ds.server.SubSystem;

@GalleryAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete})
public class SystemItemView {


    @CustomAnnotation()
    String caption;

    @CustomAnnotation(hidden = true)
    String sysId;

    @CustomAnnotation(uid = true, hidden = true)
    String id;
    @CustomAnnotation(caption = "模板名称", captionField = true)
    String comment;

    @ImageAnnotation
    @CustomAnnotation(caption = "图片", captionField = true)
    String image = "/RAD/img/project.png";


    public SystemItemView() {

    }

    public SystemItemView(SubSystem system) {
        this.comment = system.getName();
        this.sysId = system.getSysId();
        this.image = system.getIcon();
        this.id = system.getSysId();
        this.caption = "";


    }


    public SystemItemView(ServerNode node) throws JDSException {
        SubSystem system = JDSServer.getInstance().getClusterClient().getSystem(node.getId());
        if (system == null) {
            throw new JDSException("subSystem is null systemId is[" + node.getId() + "] systemName is[" + node.getName() + "]");
        }
        this.comment = system.getName();
        this.sysId = system.getSysId();
        this.image = system.getIcon();
        this.id = system.getSysId();
        this.caption = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
