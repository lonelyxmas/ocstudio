package com.ds.system.sys.view.service;

import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.manager.EsbBeanFactory;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/system/service/local/")
@TabsAnnotation(closeBtn = true)
public class LocalService {


    @RequestMapping(method = RequestMethod.POST, value = "LocalServiceInfo")
    @DialogAnnotation(width = "600", height = "480")
    @FormViewAnnotation(caption = "编辑服务信息")
    @ModuleAnnotation(imageClass = "spafont spa-icon-settingprj")
    @APIEventAnnotation(bindMenu = CustomMenuItem.editor)
    @ResponseBody
    public ResultModel<LocalServiceFormView> getLocalServiceInfo(String id) {
        ResultModel<LocalServiceFormView> resultModel = new ResultModel<LocalServiceFormView>();
        try {
            ServiceBean serviceBean = EsbBeanFactory.getInstance().getEsbBeanById(id);
            LocalServiceFormView serviceView = new LocalServiceFormView(serviceBean);
            resultModel.setData(serviceView);
        } catch (Exception e) {
            e.printStackTrace();
            resultModel = new ErrorListResultModel();
            ((ErrorResultModel<LocalServiceFormView>) resultModel).setErrdes(e.getMessage());
        }
        return resultModel;


    }


    @MethodChinaName(cname = "保存服务信息")
    @RequestMapping(value = {"saveService"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close})
    public @ResponseBody
    ResultModel<Boolean> savePerson(@RequestBody LocalServiceFormView service) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();
        return userStatusInfo;

    }

    @MethodChinaName(cname = "删除服务")
    @RequestMapping(value = {"delService"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(bindMenu = CustomMenuItem.delete, callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent})
    public @ResponseBody
    ResultModel<Boolean> delService(String id) {
        ResultModel<Boolean> userStatusInfo = new ResultModel<Boolean>();

        return userStatusInfo;

    }
}
