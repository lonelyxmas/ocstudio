package com.ds.system.sys.view;

import com.ds.cluster.ServerNode;
import com.ds.cluster.service.ServerEventFactory;
import com.ds.common.JDSException;
import com.ds.esb.config.manager.ExpressionTempBean;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.org.OrgManager;
import com.ds.server.JDSServer;
import com.ds.server.OrgManagerFactory;
import com.ds.server.SubSystem;
import com.ds.server.eumus.SystemNodeType;
import com.ds.server.eumus.SystemStatus;
import com.ds.system.sys.view.system.XUISystem;

import java.util.List;

@PageBar
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add}, event = CustomGridEvent.editor)
public class ServerNodeGridView {


    @CustomAnnotation(caption = "标识", uid = true, hidden = true)
    String id;
    @CustomAnnotation(caption = "节点名称")
    String name;

    @CustomAnnotation(caption = "最大连接数")
    Integer maxconnection = 2000;

    @CustomAnnotation(caption = "最小连接数")
    Integer minconnection = 200;

    @CustomAnnotation(caption = "超时时间")
    String timeout = "60";

    @CustomAnnotation(caption = "URL")
    String url = "";

    @CustomAnnotation(caption = "表达式")
    String userexpression;

    @CustomAnnotation(caption = "类型")
    SystemNodeType type;
    @CustomAnnotation(hidden = true)
    String gwmserver;

    @CustomAnnotation(caption = "订阅消息")
    String repeatEventKey = "";

    @CustomAnnotation(hidden = true)
    public SystemStatus status;

    @CustomAnnotation(hidden = true)
    public int currCount;

    @CustomAnnotation(hidden = true)
    public int maxCounts;

    @CustomAnnotation(hidden = true)
    public Integer checkTimes = 0;


    public ServerNodeGridView(ServerNode node) {
        SubSystem subSystem = null;
        try {
            subSystem = JDSServer.getInstance().getClusterClient().getSystem(node.getId());

        } catch (JDSException e) {
            e.printStackTrace();
        }
        try {
            XUISystem xuisubSystem = new XUISystem(node);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        String systemId = subSystem.getSysId();

        OrgManager orgManager = OrgManagerFactory.getOrgManager(subSystem.getConfigname());
        status = JDSServer.getClusterClient().getSystemStatus(systemId);
        this.id = node.getId();
        this.name = node.getName();

        if (node.getMaxconnection() != null) {
            this.maxconnection = Integer.valueOf(node.getMaxconnection());
        }
        if (node.getMinconnection() != null) {
            this.minconnection = Integer.valueOf(node.getMinconnection());
        }
        this.timeout = node.getTimeout();
        this.url = node.getUrl();
        this.userexpression = node.getUserexpression();
        this.type = node.getType();

        this.checkTimes = node.getCheckTimes();
        ServerEventFactory factory = ServerEventFactory.getInstance();
        List<ExpressionTempBean> serviceBeans = factory.getRegisterEventByCode(node.getId());

        for (ServiceBean serviceBean : serviceBeans) {
            repeatEventKey = repeatEventKey + serviceBean.getId() + ",";
        }
        if (repeatEventKey.endsWith(",")) {
            repeatEventKey = repeatEventKey.substring(0, repeatEventKey.length() - 1);
        }

    }

    public String getRepeatEventKey() {
        return repeatEventKey;
    }

    public void setRepeatEventKey(String repeatEventKey) {
        this.repeatEventKey = repeatEventKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxconnection() {
        return maxconnection;
    }

    public void setMaxconnection(Integer maxconnection) {
        this.maxconnection = maxconnection;
    }

    public Integer getMinconnection() {
        return minconnection;
    }

    public void setMinconnection(Integer minconnection) {
        this.minconnection = minconnection;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserexpression() {
        return userexpression;
    }

    public void setUserexpression(String userexpression) {
        this.userexpression = userexpression;
    }

    public SystemNodeType getType() {
        return type;
    }

    public void setType(SystemNodeType type) {
        this.type = type;
    }

    public String getGwmserver() {
        return gwmserver;
    }

    public void setGwmserver(String gwmserver) {
        this.gwmserver = gwmserver;
    }

    public SystemStatus getStatus() {
        return status;
    }

    public void setStatus(SystemStatus status) {
        this.status = status;
    }

    public int getCurrCount() {
        return currCount;
    }

    public void setCurrCount(int currCount) {
        this.currCount = currCount;
    }

    public int getMaxCounts() {
        return maxCounts;
    }

    public void setMaxCounts(int maxCounts) {
        this.maxCounts = maxCounts;
    }

    public Integer getCheckTimes() {
        return checkTimes;
    }

    public void setCheckTimes(Integer checkTimes) {
        this.checkTimes = checkTimes;
    }

    @Override
    public String toString() {
        return "id=" + id + ";serverurl" + this.url;
    }


}
