package com.ds.system.sys.view;

import com.ds.cluster.ServerNode;
import com.ds.cluster.ServerNodeList;
import com.ds.config.CApplication;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.server.JDSServer;
import com.ds.server.eumus.ConfigCode;
import com.ds.server.eumus.SystemStatus;

import java.util.List;

@BottomBarMenu
@FormAnnotation(col = 1,bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close})
public class ApplicationFormView {


    @CustomAnnotation(caption = "标识", uid = true)
    private String code;
    @CustomAnnotation(caption = "名称")
    private String name;
    @CustomAnnotation(caption = "部署节点")
    private String nodeIds = "";
    @CustomAnnotation(caption = "在线节点")
    private String nolineNodeIds = "";
    @CustomAnnotation(caption = "连接Session")
    private String connectionHandle;
    @CustomAnnotation(caption = "认证方式")
    private String jdsService;
    @CustomAnnotation(caption = "配置文件路径")
    private String configPath;

    @CustomAnnotation(caption = "用户表达式", hidden = true)
    private String userexpression;
    @CustomAnnotation(caption = "集群管理器", hidden = true)
    private String clusterManagerClass;


    public ApplicationFormView(CApplication application) {
        ServerNodeList node = JDSServer.getClusterClient().getServerNodeListByConfigCode(ConfigCode.fromType(application.getConfigCode()));
        this.code = application.getSysId();
        this.name = application.getName();
        List<ServerNode> servernodes = node.getServerNodeList();

        for (ServerNode subserverNode : servernodes) {
            String nodeId = subserverNode.getId();
            nodeIds = nodeIds + subserverNode.getId() + " ";
            if (JDSServer.getClusterClient().getSystemStatus(subserverNode.getId()).equals(SystemStatus.ONLINE)) {
                nolineNodeIds = nolineNodeIds + nodeId + " ";
            }
        }

        this.configPath = application.getConfigPath();
        this.connectionHandle = application.getConnectionHandle() != null ? application.getConnectionHandle().getImplementation() : "";
        this.jdsService = application.getJdsService() != null ? application.getJdsService().getImplementation() : "";
        this.userexpression = node.getUserexpression();
        this.clusterManagerClass = node.getClusterManagerClass();


    }

    public String getConfigPath() {
        return configPath;
    }

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }


    public String getNolineNodeIds() {
        return nolineNodeIds;
    }

    public void setNolineNodeIds(String nolineNodeIds) {
        this.nolineNodeIds = nolineNodeIds;
    }

    public String getNodeIds() {
        return nodeIds;
    }

    public void setNodeIds(String nodeIds) {
        this.nodeIds = nodeIds;
    }


    public String getUserexpression() {
        return userexpression;
    }

    public void setUserexpression(String userexpression) {
        this.userexpression = userexpression;
    }

    public String getClusterManagerClass() {
        return clusterManagerClass;
    }

    public void setClusterManagerClass(String clusterManagerClass) {
        this.clusterManagerClass = clusterManagerClass;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getConnectionHandle() {
        return connectionHandle;
    }

    public void setConnectionHandle(String connectionHandle) {
        this.connectionHandle = connectionHandle;
    }

    public String getJdsService() {
        return jdsService;
    }

    public void setJdsService(String jdsService) {
        this.jdsService = jdsService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
