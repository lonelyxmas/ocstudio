package com.ds.system.sys.node;

import com.ds.common.util.ClassUtility;
import com.ds.esb.config.manager.ServiceBean;
import com.ds.esd.tool.ui.component.list.TreeListItem;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class ServiceItem extends TreeListItem {


    public ServiceItem(String parent, Method methodBean) {
        this.caption = methodBean.getName();
        this.id = parent + "." + methodBean.getName();
    }

    public ServiceItem(ServiceBean bean) {
        this.caption = bean.getId() + "(" + (bean.getName() == null ? bean.getId() : bean.getName()) + ")";
        this.id = bean.getPath();

        this.tagVar = new HashMap<>();
        this.tagVar.put("parentId", bean.getId());
        this.expression = bean.getExpression();
        try {
            Class clazz = ClassUtility.loadClass(bean.getClazz());
            Method[] methods = clazz.getDeclaredMethods();
            if (methods.length > 0) {
                this.sub = new ArrayList<>();
                for (Method methodBean : methods) {
                    sub.add(new ServiceItem("$" + bean.getId(), methodBean));
                }
            }

        } catch (ClassNotFoundException e) {

        }


    }
}