package com.ds.system.topbar;

import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.ListAnnotation;
import com.ds.esd.custom.annotation.SVGPaperAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;
import com.ds.esd.tool.ui.component.UIItem;
import com.ds.esd.tool.ui.component.svg.SVGPathComponent;
import com.ds.esd.tool.ui.component.svg.comb.path.PathAttr;
import com.ds.esd.tool.ui.component.svg.comb.path.PathProperties;
import com.ds.esd.tool.ui.enums.SelModeType;
import org.checkerframework.checker.guieffect.qual.UI;

import java.util.List;

public class DynSvg {



    @ListAnnotation(selMode = SelModeType.none)
    @UIAnnotation(left ="0em",top ="1.5em",width ="9.85em",height = "auto")
    @FieldAnnotation(dirtyMark = false)
    List<UIItem>  dymMenu;


    @SVGPaperAnnotation()
    SVGPathComponent svgPath;


    public List<UIItem> getDymMenu() {
        return dymMenu;
    }

    public void setDymMenu(List<UIItem> dymMenu) {
        this.dymMenu = dymMenu;
    }

    public SVGPathComponent getSvgPath() {
        SVGPathComponent<PathProperties> pathComponent = new SVGPathComponent("svgPath");
        PathProperties pathProperties = pathComponent.getProperties();
        PathAttr pathAttr = new PathAttr();
        pathAttr.setPath("M,21,21L,28,1L,51,21");
        pathAttr.setFill("#ffffff");
        pathAttr.setStroke("#B6B6B6");
        pathProperties.setAttr(pathAttr);
        return pathComponent;
    }

    public void setSvgPath(SVGPathComponent svgPath) {
        this.svgPath = svgPath;
    }
}
