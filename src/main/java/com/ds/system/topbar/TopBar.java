package com.ds.system.topbar;

import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.*;
import com.ds.esd.tool.ui.enums.HAlignType;
import com.ds.esd.tool.ui.enums.OverflowType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/system/")
@MethodChinaName(cname = "topbar")
public class TopBar {


    @UIAnnotation(left = "0.7em")
    @ImageAnnotation(width = "3.6em", height = "3.6em")
    String logo = "/img/staticjds.gif";

    @LabelAnnotation(hAlign = HAlignType.center, fontColor = "#FFFFFF", fontSize = "18px")
    @UIAnnotation(left = "4.5em", width = "3.8em", height = "1.2em")
    String logoName = "CodeBee";

    @UIAnnotation(left = "10.6em", top = "0.75em", width = "3em", height = "2.2em")
    @IconAnnotation(imageClass = "spafont spa-icon-login", iconFontSize = "2em")
    String userIcon;


    @UIAnnotation(top = "0.68em", width = "16em", height = "5em", right = "0em")
    @GalleryViewAnnotation()
    TreeListResultModel<List<TopBarMenu>> galleryMenu;


    @DivAnnotation()
    @ContainerAnnotation(hoverPop = "dynSvg")
    @UIAnnotation(left = "13.5em", top = "0.75em", height = "2.5em", width = "8.75em")
    UserConfig userConfig;

    @UIAnnotation(left = "13.6em", top = "3em", zindex = 1002)
    @SVGPaperAnnotation(height = "1.375em", width = "11.25em", graphicZIndex = 2, overflow = OverflowType.visible)
    DynSvg dynSvg;

    public DynSvg getDynSvg() {
        return dynSvg;
    }

    public void setDynSvgMenu(DynSvg dynSvg) {
        this.dynSvg = dynSvg;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLogoName() {
        return logoName;
    }

    public void setLogoName(String logoName) {
        this.logoName = logoName;
    }

    public UserConfig getUserConfig() {
        return userConfig;
    }

    public void setUserConfig(UserConfig userConfig) {
        this.userConfig = userConfig;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public TreeListResultModel<List<TopBarMenu>> getGalleryMenu() {
        return galleryMenu;
    }

    public void setGalleryMenu(TreeListResultModel<List<TopBarMenu>> galleryMenu) {
        this.galleryMenu = galleryMenu;
    }

}
