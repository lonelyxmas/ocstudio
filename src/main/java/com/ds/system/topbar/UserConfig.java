package com.ds.system.topbar;

import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.LabelAnnotation;
import com.ds.esd.custom.annotation.UIAnnotation;

public class UserConfig {

    @UIAnnotation(position = "static")
    @FieldAnnotation(tabindex = 3)
    @LabelAnnotation(fontColor = "#FFFFFF")
    String userName;


    @UIAnnotation(position = "static")
    @FieldAnnotation(tabindex = 4)
    @LabelAnnotation(imageClass = "xui-icon-sort-checked", fontColor = "#FFFFFF")
    String toggleIcon;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToggleIcon() {
        return toggleIcon;
    }

    public void setToggleIcon(String toggleIcon) {
        this.toggleIcon = toggleIcon;
    }
}
