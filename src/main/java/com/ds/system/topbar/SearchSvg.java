package com.ds.system.topbar;

import com.ds.esd.custom.annotation.*;
import com.ds.esd.tool.ui.component.block.BlockComponent;
import com.ds.esd.tool.ui.component.block.BlockProperties;
import com.ds.esd.tool.ui.component.svg.SVGPathComponent;
import com.ds.esd.tool.ui.component.svg.comb.path.PathAttr;
import com.ds.esd.tool.ui.component.svg.comb.path.PathProperties;
import com.ds.esd.tool.ui.enums.BorderType;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.enums.LabelPos;
import com.ds.esd.tool.ui.enums.event.enums.UIEventEnum;

public class SearchSvg {

    @UIAnnotation(left = "0.625em", top = "1.875em", width = "14.875em", zindex = 1002)
    @Label(labelSize = "8em", labelPos = LabelPos.none)
    @ComboInputAnnotation(caption = "查询", commandBtn = "xui-icon-search")
    String search;

    @SVGPaperAnnotation
    SVGPathComponent svgPath;

    @UIAnnotation(left = "0em", top = "1.25em", width = "16.9em", height = "3.1em", zindex = 0, shadows = true)
    @BlockAnnotation(borderType = BorderType.flat, background = "#FFFFFF", dock = Dock.none)
    BlockComponent block;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public SVGPathComponent getSvgPath() {
        SVGPathComponent<PathProperties> svgPath = new SVGPathComponent();
        PathProperties pathProperties = svgPath.getProperties();
        pathProperties.setSvgTag("Shapes:Triangle");
        PathAttr pathAttr = new PathAttr();
        pathAttr.setPath("M,140,21L,133,1L,110,21");
        pathAttr.setFill("#ffffff");
        pathAttr.setStroke("#B6B6B6");
        pathProperties.setAttr(pathAttr);

        return svgPath;
    }

    public void setBlock(BlockComponent block) {
        this.block = block;
    }

    public BlockComponent getBlock() {
        BlockComponent<BlockProperties, UIEventEnum> blockComponent = new BlockComponent();
        BlockProperties blockProperties = blockComponent.getProperties();
        blockProperties.setLeft("0em");
        blockProperties.setDock(Dock.none);
        blockProperties.setTop("1.25em");
        blockProperties.setWidth("16.9em");
        blockProperties.setHeight("3.1em");
        blockProperties.setzIndex(0);
        blockProperties.setShadow(true);
        blockProperties.setBorderType(BorderType.flat);
        blockProperties.setBackground("#FFFFFF");
        return blockComponent;
    }

    public void setSvgPath(SVGPathComponent svgPath) {
        this.svgPath = svgPath;
    }
}
