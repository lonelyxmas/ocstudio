package com.ds.system.topbar;

import com.ds.esd.custom.annotation.toolbar.GalleryFieldAnnotation;
import com.ds.esd.custom.gallery.annotation.GalleryAnnotation;
import com.ds.esd.tool.ui.component.item.GalleryItem;
import com.ds.esd.tool.ui.enums.BorderType;
import com.ds.esd.tool.ui.enums.SelModeType;

@GalleryAnnotation(selMode = SelModeType.none,borderType = BorderType.none)
public class TopBarMenu extends GalleryItem {

    public TopBarMenu(){

    }

}
