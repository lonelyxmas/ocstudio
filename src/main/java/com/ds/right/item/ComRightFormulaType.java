package com.ds.right.item;


import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.right.component.service.*;


public enum ComRightFormulaType implements TreeItem {


    ComponentRight(ItemFormulaService.class, "組件权限", "spafont spa-icon-module", true, false, false),

    UIComponentRight(UIFormulaService.class, "UI组件", "spafont spa-icon-frame", true, false, false),

    FormComponentRight(FormFormulaService.class, "表单之间", "spafont spa-icon-c-hiddeninput", true, false, false),

    BarComponentRight(BarFormulaService.class, "工具栏", "spafont spa-icon-c-buttonviews", true, false, false),

    ActionComponentRight(ActionFormulaService.class, "动作授权", "spafont spa-icon-c-imagebutton", true, false, false),

    TabComponentRight(TabFormulaService.class, "Tab项授权", "spafont spa-icon-c-treebar", true, false, false);


    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    ComRightFormulaType(Class bindClass, String name, String imageClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {

        this.name = name;
        this.bindClass = bindClass;
        this.imageClass = imageClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;
    }


    public String getName() {
        return name;
    }

    @Override
    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String getImageClass() {
        return imageClass;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public static ComRightFormulaType fromName(String name) {
        for (ComRightFormulaType type : ComRightFormulaType.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    @Override
    public String getType() {
        return name();
    }

}
