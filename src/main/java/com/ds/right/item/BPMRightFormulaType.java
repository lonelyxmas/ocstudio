package com.ds.right.item;


import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.right.org.InsteadSignFormulaService;
import com.ds.right.org.PerformFormulaService;
import com.ds.right.org.ReadFormulaService;


public enum BPMRightFormulaType implements TreeItem {


    ReaderSelectedID(ReadFormulaService.class, "读者组授权", "bpmfont bpmgongzuoliu", true, false, false),

    PerformerSelectedID(PerformFormulaService.class, "办理组", "bpmfont bpmyuxiandengjibanli", true, false, false),

    InsteadSignSelectedID(InsteadSignFormulaService.class, "代办组", "bpmfont bpmgongzuoliujilu", true, false, false);


    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    BPMRightFormulaType(Class bindClass, String name, String imageClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {

        this.name = name;
        this.bindClass = bindClass;
        this.imageClass = imageClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;
    }


    public String getName() {
        return name;
    }

    @Override
    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String getImageClass() {
        return imageClass;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public static BPMRightFormulaType fromName(String name) {
        for (BPMRightFormulaType type : BPMRightFormulaType.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    @Override
    public String getType() {
        return name();
    }

}
