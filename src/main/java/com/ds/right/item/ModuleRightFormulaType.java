package com.ds.right.item;


import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.right.component.service.*;
import com.ds.right.module.MenuFormulaService;
import com.ds.right.module.ModuleFormulaService;


public enum ModuleRightFormulaType implements TreeItem {


    ModuleRight(ModuleFormulaService.class, "模块授权", "spafont spa-icon-c-iconslist", true, false, false),

    MenuRight(MenuFormulaService.class, "菜单授权", "spafont spa-icon-c-menu", true, false, false);

    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    ModuleRightFormulaType(Class bindClass, String name, String imageClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {

        this.name = name;
        this.bindClass = bindClass;
        this.imageClass = imageClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;
    }


    public String getName() {
        return name;
    }

    @Override
    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String getImageClass() {
        return imageClass;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public static ModuleRightFormulaType fromName(String name) {
        for (ModuleRightFormulaType type : ModuleRightFormulaType.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    @Override
    public String getType() {
        return name();
    }

}
