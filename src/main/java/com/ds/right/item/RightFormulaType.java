package com.ds.right.item;


import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.right.component.service.*;
import com.ds.right.module.MenuFormulaService;
import com.ds.right.module.ModuleFormulaService;
import com.ds.right.org.InsteadSignFormulaService;
import com.ds.right.org.PerformFormulaService;
import com.ds.right.org.ReadFormulaService;


public enum RightFormulaType implements TreeItem {


    ComponentRight(RightType.comright, ItemFormulaService.class, "組件权限", "spafont spa-icon-module", true, false, false),

    UIComponentRight(RightType.comright, UIFormulaService.class, "UI组件", "spafont spa-icon-frame", true, false, false),

    FormComponentRight(RightType.comright, FormFormulaService.class, "表单之间", "spafont spa-icon-c-hiddeninput", true, false, false),

    BarComponentRight(RightType.comright, BarFormulaService.class, "工具栏", "spafont spa-icon-c-buttonviews", true, false, false),

    ActionComponentRight(RightType.comright, ActionFormulaService.class, "动作授权", "spafont spa-icon-c-imagebutton", true, false, false),

    TabComponentRight(RightType.comright, TabFormulaService.class, "Tab项授权", "spafont spa-icon-c-treebar", true, false, false),

    ModuleRight(RightType.moduleright, ModuleFormulaService.class, "模块授权", "spafont spa-icon-c-iconslist", true, false, false),

    MenuRight(RightType.moduleright, MenuFormulaService.class, "菜单授权", "spafont spa-icon-c-menu", true, false, false),

    ReaderSelectedID(RightType.bpmright, ReadFormulaService.class, "读者组授权", "bpmfont bpmgongzuoliu", true, false, false),

    PerformerSelectedID(RightType.bpmright, PerformFormulaService.class, "办理组", "bpmfont bpmyuxiandengjibanli", true, false, false),

    InsteadSignSelectedID(RightType.bpmright, InsteadSignFormulaService.class, "代办组", "bpmfont bpmgongzuoliujilu", true, false, false);


    private RightType rightType;
    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    RightFormulaType(RightType rightType, Class bindClass, String name, String imageClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.rightType = rightType;
        this.name = name;
        this.bindClass = bindClass;
        this.imageClass = imageClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;
    }

    public RightType getRightType() {
        return rightType;
    }

    public String getName() {
        return name;
    }

    public void setRightType(RightType rightType) {
        this.rightType = rightType;
    }

    @Override
    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String getImageClass() {
        return imageClass;
    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public static RightFormulaType fromName(String name) {
        for (RightFormulaType type : RightFormulaType.values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return null;
    }

    @Override
    public String getType() {
        return name();
    }

}
