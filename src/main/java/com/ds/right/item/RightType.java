package com.ds.right.item;

import com.ds.esd.custom.tree.enums.TreeItem;
import com.ds.right.component.service.FormFormulaService;
import com.ds.right.module.MenuFormulaService;
import com.ds.right.org.BPMTreeRightIndex;

public enum RightType implements TreeItem {

    comright("组件权限", "bpmfont bpmgongzuoliuzhutiguizeweihuguanli", FormFormulaService.class, true, false, false),

    moduleright("模块权限", "bpmfont bpmgongzuoliuxitongpeizhi", MenuFormulaService.class, true, false, false),

    bpmright("流程授权", "spafont spa-icon-app", BPMTreeRightIndex.class, true, false, false);

    private final String name;
    private final Class bindClass;
    private final String imageClass;

    private final boolean iniFold;

    private final boolean dynDestory;

    private final boolean dynLoad;


    RightType(String name, String imageClass, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;
        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }

    public static RightType fromType(String typeName) {
        for (RightType type : RightType.values()) {
            if (type.getType().equals(typeName)) {
                return type;
            }
        }
        return moduleright;
    }

}
