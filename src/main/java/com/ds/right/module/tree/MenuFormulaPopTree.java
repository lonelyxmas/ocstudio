package com.ds.right.module.tree;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.right.component.service.FormFormulaService;
import com.ds.right.module.MenuFormulaService;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {MenuFormulaService.class})
@PopTreeAnnotation(caption = "添加表达式")
public class MenuFormulaPopTree extends TreeListItem {


    @Pid
    private String formulaInstId;

    @Pid
    private String path;

    @Pid
    public FormulaType formulaType;

    @Uid
    public String selectId;

    @TreeItemAnnotation(bindService = FormFormulaService.class)
    public MenuFormulaPopTree(FormulaType formulaType, String path) {
        this.caption = formulaType.getName();
        this.id = formulaType.getType();
        this.imageClass = formulaType.getImageClass();
        this.formulaType = formulaType;
        this.path = path;
    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-function")
    public MenuFormulaPopTree(ParticipantSelect select, String path) {
        this.caption = select.getSelectName();
        this.id = select.getParticipantSelectId();
        this.tips = select.getFormula();
        this.selectId = select.getParticipantSelectId();
        this.formulaType = select.getFormulaType();
        this.path = path;


    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public FormulaType getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }

    public String getSelectId() {
        return selectId;
    }

    public void setSelectId(String selectId) {
        this.selectId = selectId;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }
}
