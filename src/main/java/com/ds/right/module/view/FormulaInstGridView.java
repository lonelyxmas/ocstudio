package com.ds.right.module.view;

import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.TextEditorAnnotation;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.project.config.formula.FormulaInst;
import com.ds.esd.tool.ui.enums.SelModeType;

@PageBar
@RowHead(selMode = SelModeType.multibycheckbox)
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, event = CustomGridEvent.editor)
public class FormulaInstGridView {


    @CustomAnnotation(uid = true, hidden = true)
    private String formulaInstId;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectName;
    @CustomAnnotation(pid = true, hidden = true)
    String participantSelectId;

    @MethodChinaName(cname = "名称")
    private String name;

    @CustomAnnotation(caption = "类型", readonly = true)
    private FormulaType formulaType;

    @MethodChinaName(cname = "描述")
    @TextEditorAnnotation

    private String selectDesc;

    @MethodChinaName(cname = "公式")
    private String expression;


    public FormulaInstGridView() {

    }

    public FormulaInstGridView(FormulaInst formula) {
        this.formulaInstId = formula.getFormulaInstId();
        this.projectName = formula.getProjectName();
        this.formulaType = formula.getFormulaType();
        this.participantSelectId = formula.getParticipantSelectId();
        this.name = formula.getName();
        this.selectDesc = formula.getSelectDesc();
        this.expression = formula.getExpression();

    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParticipantSelectId() {
        return participantSelectId;
    }

    public void setParticipantSelectId(String participantSelectId) {
        this.participantSelectId = participantSelectId;
    }

    public FormulaType getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }


    public String getSelectDesc() {
        return selectDesc;
    }

    public void setSelectDesc(String selectDesc) {
        this.selectDesc = selectDesc;
    }
}
