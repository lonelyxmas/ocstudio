package com.ds.right.component.tree;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.enums.attribute.Attributetype;
import com.ds.esb.config.formula.EngineType;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;

import java.util.HashMap;
import java.util.List;

@BottomBarMenu
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox,bottombarMenu = {CustomFormMenu.Close, CustomFormMenu.Save}, caption = "添加表达式")
@PopTreeAnnotation()
public class FormulaPopTree extends TreeListItem {


    @CustomAnnotation(pid = true, hidden = true)
    private String formulaInstId;


    public FormulaPopTree() {
        this.caption = "公式类型";
        this.setId("allFormulaType");
        this.setIniFold(false);
        EngineType[] types = EngineType.values();
        for (EngineType type : types) {
            FormulaPopTree engineType = new FormulaPopTree(type);
            if (engineType.getSub() != null && engineType.getSub().size() > 0) {
                this.addChild(engineType);
            }

        }
    }

    public FormulaPopTree(Attributetype baseType) {
        this.caption = baseType.getName();
        this.setId(baseType.getType());
        this.setIniFold(false);
        this.setImageClass(baseType.getImageClass());
        FormulaType[] formulaTypes = FormulaType.values();
        for (FormulaType formulaType : formulaTypes) {
            if (formulaType.getBaseType().equals(baseType)) {
                FormulaPopTree formulaTypeItem = new FormulaPopTree(formulaType);
                this.addChild(formulaTypeItem);
            }
        }
    }

    public FormulaPopTree(FormulaType formulaType) {
        this.caption = formulaType.getName();
        this.setId(formulaType.getType());
        this.setIniFold(false);
        this.setImageClass(formulaType.getImageClass());
        try {
            List<ParticipantSelect> selects = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulas(formulaType);
            for (ParticipantSelect select : selects) {
                this.addChild(new FormulaPopTree(select));
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }


        this.setEuClassName("admin.formula.Formulas");
        this.tagVar = new HashMap<>();
        this.tagVar.put("formulaType", formulaType.getType());

    }

    public FormulaPopTree(ParticipantSelect select) {
        this.caption = select.getSelectName();
        this.id = select.getParticipantSelectId();
        this.tips = select.getFormula();
        this.tagVar = new HashMap<>();
        this.setImageClass("spafont spa-icon-function");
        this.tagVar.put("formulaType", select.getFormulaType().getType());
        this.tagVar.put("selectId", select.getParticipantSelectId());

    }

    public FormulaPopTree(EngineType engineType) {
        this.caption = engineType.getName();
        this.setId(engineType.getType());
        this.setImageClass(engineType.getImageClass());
        this.setIniFold(false);
        Attributetype[] attributetypes = Attributetype.values();
        for (Attributetype attributetype : attributetypes) {
            if (attributetype.getEngineType().equals(engineType)) {
                FormulaPopTree formulaTypeItem = new FormulaPopTree(attributetype);
                if (formulaTypeItem.getSub() != null && formulaTypeItem.getSub().size() > 0) {
                    this.addChild(formulaTypeItem);
                }
            }
        }

    }


    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }
}
