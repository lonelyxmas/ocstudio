package com.ds.right.component.tree;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.enums.attribute.Attributetype;
import com.ds.esb.config.formula.EngineType;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.right.component.service.BarFormulaService;
import com.ds.right.component.service.CustomComponentService;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

import java.util.HashMap;
import java.util.List;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {CustomComponentService.class})
@PopTreeAnnotation(caption = "添加表达式")
public class ComponentFormulaPopTree extends TreeListItem {

    @Pid
    private String formulaInstId;

    @Pid
    private String currClassName;

    @Pid
    FormulaType formulaType;

    @Uid
    public String selectId;


    @TreeItemAnnotation(bindService = BarFormulaService.class)
    public ComponentFormulaPopTree(FormulaType formulaType, String currClassName) {
        this.caption = formulaType.getName();
        this.currClassName = currClassName;
        this.id = formulaType.getType();
        this.formulaType = formulaType;
        this.imageClass = formulaType.getImageClass();

    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-function")
    public ComponentFormulaPopTree(ParticipantSelect select, String currClassName) {
        this.caption = select.getSelectName();
        this.id = select.getParticipantSelectId();
        this.tips = select.getFormula();
        this.currClassName = currClassName;
        this.formulaType = select.getFormulaType();
        this.selectId = select.getParticipantSelectId();

    }

    public ComponentFormulaPopTree(EngineType engineType) {
        this.caption = engineType.getName();
        this.setId(engineType.getType());
        this.setImageClass(engineType.getImageClass());
        this.setIniFold(false);
        Attributetype[] attributetypes = Attributetype.values();
        for (Attributetype attributetype : attributetypes) {
            if (attributetype.getEngineType().equals(engineType)) {
                FormulaPopTree formulaTypeItem = new FormulaPopTree(attributetype);
                if (formulaTypeItem.getSub() != null && formulaTypeItem.getSub().size() > 0) {
                    this.addChild(formulaTypeItem);
                }
            }
        }

    }

    public FormulaType getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }

    public String getSelectId() {
        return selectId;
    }

    public void setSelectId(String selectId) {
        this.selectId = selectId;
    }

    public String getCurrClassName() {
        return currClassName;
    }

    public void setCurrClassName(String currClassName) {
        this.currClassName = currClassName;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }
}
