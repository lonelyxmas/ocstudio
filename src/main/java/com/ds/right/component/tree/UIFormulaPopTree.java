package com.ds.right.component.tree;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.right.component.service.UIFormulaService;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {UIFormulaService.class})
@PopTreeAnnotation(caption = "添加表达式")
public class UIFormulaPopTree extends TreeListItem {


    @CustomAnnotation(pid = true, hidden = true)
    private String formulaInstId;

    @CustomAnnotation(pid = true, hidden = true)
    private String currClassName;

    @Pid
    public FormulaType formulaType;

    @Uid
    public String selectId;

    @TreeItemAnnotation(bindService = UIFormulaService.class)
    public UIFormulaPopTree(FormulaType formulaType, String currClassName) {
        this.caption = formulaType.getName();
        this.currClassName = currClassName;
        this.id = formulaType.getType();
        this.formulaType = formulaType;
        this.imageClass = formulaType.getImageClass();

    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-function")
    public UIFormulaPopTree(ParticipantSelect select, String currClassName) {
        this.caption = select.getSelectName();
        this.id = select.getParticipantSelectId();
        this.tips = select.getFormula();
        this.currClassName = currClassName;
        this.formulaType = select.getFormulaType();
        this.selectId = select.getParticipantSelectId();

    }

    public FormulaType getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }

    public String getSelectId() {
        return selectId;
    }

    public void setSelectId(String selectId) {
        this.selectId = selectId;
    }

    public String getCurrClassName() {
        return currClassName;
    }

    public void setCurrClassName(String currClassName) {
        this.currClassName = currClassName;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }
}
