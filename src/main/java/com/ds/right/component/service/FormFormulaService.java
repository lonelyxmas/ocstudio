package com.ds.right.component.service;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.OrgDomain;
import com.ds.esd.dsm.domain.enums.OrgDomainType;
import com.ds.esd.project.config.formula.FormulaInst;
import com.ds.esd.project.config.formula.ModuleFormulaInst;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.util.TreePageUtil;
import com.ds.right.component.tree.BarFormulaPopTree;
import com.ds.right.component.tree.FormFormulaPopTree;
import com.ds.right.component.tree.ItemFormulaPopTree;
import com.ds.right.component.view.ModuleFormGridView;
import com.ds.right.component.view.ModuleFormulaInstNav;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(path = "/esd/right/module/form/")
@MethodChinaName(cname = "表单项授权", imageClass = "spafont spa-icon-astext")
@Aggregation(rootClass = FormFormulaService.class)
@OrgDomain(type = OrgDomainType.right)
public class FormFormulaService {
    @RequestMapping(method = RequestMethod.POST, value = "Index")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "xui-uicmd-cmdbox", caption = "表单项授权")
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.reload, CustomMenuItem.index, CustomMenuItem.treeNodeEditor}, customRequestData = {RequestPathEnum.SPA_className})
    @ResponseBody
    public ListResultModel<List<ModuleFormGridView>> getFormulaInstList(String projectId, String currClassName) {
        ListResultModel<List<ModuleFormGridView>> resultModel = new ListResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectById(projectId);
            EUModule euModule = ESDFacrory.getESDClient().getModule(currClassName, project.getActiveProjectVersion().getVersionName());
            Map<String, ModuleFormulaInst> instMap = euModule.getComponent().getFormulas();

            List<FormulaInst> instList = new ArrayList<>();

            for (FormulaInst inst : instMap.values()) {
                if (inst != null && inst.getFormulaType() != null && inst.getFormulaType().equals(FormulaType.FormComponentRight)) {
                    instList.add(inst);
                }
            }


            resultModel = PageUtil.getDefaultPageList(instList, ModuleFormGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }



    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @RequestMapping("loadChildItem")
    @ResponseBody
    public TreeListResultModel<List<ItemFormulaPopTree>> loadChildItem(String currClassName, FormulaType formulaType) {
        TreeListResultModel<List<ItemFormulaPopTree>> model = new TreeListResultModel<>();
        try {
            List<ParticipantSelect> selects = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulas(formulaType);
            model = TreePageUtil.getTreeList(selects, ItemFormulaPopTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return model;
    }


    @PopTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, caption = "添加表达式",  imageClass = "spafont spa-icon-function")
    @APIEventAnnotation(bindMenu = CustomMenuItem.add, customRequestData = {RequestPathEnum.SPA_className})
    @RequestMapping("SelectFormula")
    @DialogAnnotation(width = "300", height = "450")
    @ResponseBody
    public TreeListResultModel<List<FormFormulaPopTree>> getFormulaTree(String currClassName) {
        TreeListResultModel<List<FormFormulaPopTree>> model = new TreeListResultModel<>();
         model = TreePageUtil.getTreeList(Arrays.asList(FormulaType.FormComponentRight), FormFormulaPopTree.class);
        return model;
    }


    @MethodChinaName(cname = "选择 表达式信息")
    @RequestMapping(value = {"addFormula"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.treeSave}, customRequestData = {RequestPathEnum.SPA_className})
    public @ResponseBody
    ResultModel<Boolean> addFormulaInst(String SelectFormulaTree, String currClassName, String projectId) {
        ResultModel<Boolean> resultModel = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectById(projectId);
            EUModule euModule = ESDFacrory.getESDClient().getModule(currClassName, project.getActiveProjectVersion().getVersionName());
            Map<String, ModuleFormulaInst> instMap = euModule.getComponent().getFormulas();

            String[] formulaIdArr = StringUtility.split(SelectFormulaTree, ";");
            for (String id : formulaIdArr) {
                ParticipantSelect select = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulaById(id);
                ModuleFormulaInst inst = new ModuleFormulaInst();
                inst.setExpression(select.getFormula());
                inst.setFormulaType(select.getFormulaType());
                inst.setClassName(currClassName);
                inst.setParticipantSelectId(select.getParticipantSelectId());
                inst.setName(select.getSelectName());
                inst.setSelectDesc(select.getSelectDesc());
                inst.setFormulaInstId(UUID.randomUUID().toString());
                instMap.put(inst.getFormulaInstId(), inst);
            }

            ESDFacrory.getESDClient().saveModule(euModule);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "保存表达式信息")
    @RequestMapping(value = {"saveFormula"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.formSave})
    public @ResponseBody
    ResultModel<Boolean> saveFormulaInst(@RequestBody ModuleFormulaInst inst) {
        ResultModel<Boolean> resultModel = new ResultModel<>();
        try {
            Project project = ESDFacrory.getESDClient().getProjectByName(inst.getProjectName());
            EUModule euModule = ESDFacrory.getESDClient().getModule(inst.getClassName(), project.getActiveProjectVersion().getVersionName());
            Map<String, ModuleFormulaInst> instMap = euModule.getComponent().getFormulas();
            instMap.remove("");
            instMap.put(inst.getFormulaInstId(), inst);
            ESDFacrory.getESDClient().saveModule(euModule);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FormulaInstInfo")
    @NavGroupViewAnnotation
    @DialogAnnotation
    @ModuleAnnotation(caption = "添加参数", imageClass = "spafont spa-icon-function")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
    public @ResponseBody
    ResultModel<ModuleFormulaInstNav> getFormulaInstInfo(String formulaInstId, String projectName) {
        ResultModel<ModuleFormulaInstNav> model = new ResultModel<ModuleFormulaInstNav>();
        return model;
    }

    @MethodChinaName(cname = "删除表达式")
    @RequestMapping(value = {"delFormulaInst"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent}, bindMenu = {CustomMenuItem.delete}, customRequestData = {RequestPathEnum.SPA_className})
    public @ResponseBody
    ResultModel<Boolean> delFormulaInst(String formulaInstId, String projectName, String currClassName) {
        ResultModel<Boolean> resultModel = new ResultModel<>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(currClassName, version.getVersionName());
            Map<String, ModuleFormulaInst> instMap = euModule.getComponent().getFormulas();
            String[] formulaInstIdArr = StringUtility.split(formulaInstId, ";");
            for (String id : formulaInstIdArr) {
                instMap.remove(id);
            }
            ESDFacrory.getESDClient().saveModule(euModule);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
