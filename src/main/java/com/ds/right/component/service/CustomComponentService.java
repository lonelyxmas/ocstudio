package com.ds.right.component.service;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.bpm.BPMFactory;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.project.config.formula.ModuleFormulaInst;
import com.ds.esd.tool.module.EUModule;
import com.ds.right.component.view.ModuleFormulaInstNav;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping(path = "/esd/right/module/")
@MethodChinaName(cname = "模块授权", imageClass = "spafont spa-icon-action1")
public class CustomComponentService {


    @MethodChinaName(cname = "选择 表达式信息")
    @RequestMapping(value = {"addFormula"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.treeSave}, customRequestData = {RequestPathEnum.SPA_className})
    public @ResponseBody
    ResultModel<Boolean> addFormulaInst(String SelectFormulaTree, String currClassName, String projectName) {
        ResultModel<Boolean> resultModel = new ResultModel<>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(currClassName,version.getVersionName());
            Map<String, ModuleFormulaInst> instMap = euModule.getComponent().getFormulas();

            String[] formulaIdArr = StringUtility.split(SelectFormulaTree, ";");
            for (String id : formulaIdArr) {
                ParticipantSelect select = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulaById(id);
                ModuleFormulaInst inst = new ModuleFormulaInst();
                inst.setExpression(select.getFormula());
                inst.setFormulaType(select.getFormulaType());
                inst.setClassName(currClassName);
                inst.setParticipantSelectId(select.getParticipantSelectId());
                inst.setName(select.getSelectName());
                inst.setSelectDesc(select.getSelectDesc());
                inst.setFormulaInstId(UUID.randomUUID().toString());
                instMap.put(inst.getFormulaInstId(), inst);
            }

            ESDFacrory.getESDClient().saveModule(euModule);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "保存表达式信息")
    @RequestMapping(value = {"saveFormula"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.formSave})
    public @ResponseBody
    ResultModel<Boolean> saveFormulaInst(@RequestBody ModuleFormulaInst inst) {
        ResultModel<Boolean> resultModel = new ResultModel<>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(inst.getProjectName());
            EUModule euModule = ESDFacrory.getESDClient().getModule(inst.getClassName(), version.getVersionName());
            Map<String, ModuleFormulaInst> instMap = euModule.getComponent().getFormulas();
            instMap.remove("");
            instMap.put(inst.getFormulaInstId(), inst);
            ESDFacrory.getESDClient().saveModule(euModule);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FormulaInstInfo")
    @NavGroupViewAnnotation
    @ModuleAnnotation(caption = "添加参数", imageClass = "spafont spa-icon-function")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
    public @ResponseBody
    ResultModel<ModuleFormulaInstNav> getFormulaInstInfo(String formulaInstId) {
        ResultModel<ModuleFormulaInstNav> model = new ResultModel<ModuleFormulaInstNav>();
        return model;
    }

    @MethodChinaName(cname = "删除表达式")
    @RequestMapping(value = {"delFormulaInst"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent}, bindMenu = {CustomMenuItem.delete}, customRequestData = {RequestPathEnum.SPA_className})
    public @ResponseBody
    ResultModel<Boolean> delFormulaInst(String formulaInstId, String projectName, String currClassName) {
        ResultModel<Boolean> resultModel = new ResultModel<>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            EUModule euModule = ESDFacrory.getESDClient().getModule(currClassName,version.getVersionName());
            Map<String, ModuleFormulaInst> instMap = euModule.getComponent().getFormulas();
            String[] formulaInstIdArr = StringUtility.split(formulaInstId, ";");
            for (String id : formulaInstIdArr) {
                instMap.remove(id);
            }
            ESDFacrory.getESDClient().saveModule(euModule);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
