package com.ds.right.component.view;

import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.CodeEditorAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.TextEditorAnnotation;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.grid.annotation.GridAnnotation;
import com.ds.esd.custom.grid.annotation.PageBar;
import com.ds.esd.custom.grid.annotation.RowHead;
import com.ds.esd.custom.grid.enums.CustomGridEvent;
import com.ds.esd.custom.grid.enums.GridMenu;
import com.ds.esd.custom.toolbar.CodeEditorTools;
import com.ds.esd.project.config.formula.ModuleFormulaInst;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.right.component.service.FormFormulaService;
@PageBar
@RowHead(selMode = SelModeType.multibycheckbox)
@GridAnnotation(customMenu = {GridMenu.Reload, GridMenu.Add, GridMenu.Delete}, customService = {FormFormulaService.class}, event = CustomGridEvent.editor)
public class ModuleFormGridView {
    @CustomAnnotation(uid = true, hidden = true)
    private String formulaInstId;
    @CustomAnnotation(pid = true, hidden = true)
    private String projectName;

    @CustomAnnotation(pid = true, hidden = true)
    private String className;

    @CustomAnnotation(pid = true, hidden = true)
    String participantSelectId;
    @MethodChinaName(cname = "名称")

    private String name;

    @MethodChinaName(cname = "公式")
    @CodeEditorAnnotation
    @ToolBarMenu(menuClasses = CodeEditorTools.class)

    private String expression;

    @CustomAnnotation(caption = "类型",hidden = true)
    private FormulaType formulaType;

    @MethodChinaName(cname = "描述")
    @CustomAnnotation(hidden = true)
    private String selectDesc;


    public ModuleFormGridView() {

    }

    public ModuleFormGridView(ModuleFormulaInst formula) {
        this.formulaInstId = formula.getFormulaInstId();
        this.projectName = formula.getProjectName();
        this.participantSelectId = formula.getParticipantSelectId();
        this.name = formula.getName();
        this.className = formula.getClassName();
        this.expression = formula.getExpression();
        this.selectDesc = formula.getSelectDesc();
        this.formulaType = formula.getFormulaType();
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParticipantSelectId() {
        return participantSelectId;
    }

    public void setParticipantSelectId(String participantSelectId) {
        this.participantSelectId = participantSelectId;
    }


    public FormulaType getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }

    public String getSelectDesc() {
        return selectDesc;
    }

    public void setSelectDesc(String selectDesc) {
        this.selectDesc = selectDesc;
    }
}
