package com.ds.right.component.view;

import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.CodeEditorAnnotation;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.FieldAnnotation;
import com.ds.esd.custom.annotation.TextEditorAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.annotation.toolbar.ToolBarMenu;
import com.ds.esd.custom.form.annotation.FormAnnotation;
import com.ds.esd.custom.toolbar.CodeEditorTools;
import com.ds.esd.project.config.formula.ModuleFormulaInst;
import com.ds.web.annotation.Required;

@BottomBarMenu
@FormAnnotation()
public class ModuleFormInstView {
    @CustomAnnotation(uid = true, hidden = true)
    public String formulaInstId;
    @CustomAnnotation(pid = true, hidden = true)
    public String projectName;

    @CustomAnnotation(pid = true, hidden = true)
    public String className;

    @CustomAnnotation(pid = true, hidden = true)
    public String participantSelectId;

    @MethodChinaName(cname = "显示名称")
    @Required
    public String name;

    @CustomAnnotation(caption = "类型", readonly = true)
    public FormulaType formulaType;

    @MethodChinaName(cname = "描述")
    @TextEditorAnnotation
    @CustomAnnotation( readonly = true)
    @FieldAnnotation( rowHeight = "25", colSpan = -1)
    public String selectDesc;

    @MethodChinaName(cname = "公式")
    @CodeEditorAnnotation
    @ToolBarMenu(menuClasses = CodeEditorTools.class)
    @FieldAnnotation(rowHeight = "60", colSpan = -1, colWidth = "200")
    @CustomAnnotation( readonly = true)
    public String expression;


    public ModuleFormInstView() {

    }

    public ModuleFormInstView(ModuleFormulaInst formula) {
        this.formulaInstId = formula.getFormulaInstId();
        this.projectName = formula.getProjectName();
        this.participantSelectId = formula.getParticipantSelectId();
        this.name = formula.getName();
        this.className = formula.getClassName();
        this.selectDesc = formula.getSelectDesc();
        this.formulaType = formula.getFormulaType();
        this.expression = formula.getExpression();

    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }

    public String getParticipantSelectId() {
        return participantSelectId;
    }

    public void setParticipantSelectId(String participantSelectId) {
        this.participantSelectId = participantSelectId;
    }

    public FormulaType getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }


    public String getSelectDesc() {
        return selectDesc;
    }

    public void setSelectDesc(String selectDesc) {
        this.selectDesc = selectDesc;
    }
}
