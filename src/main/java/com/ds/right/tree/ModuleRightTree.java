package com.ds.right.tree;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.right.item.ComRightFormulaType;
import com.ds.right.item.ModuleRightFormulaType;
import com.ds.right.item.RightType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation
public class ModuleRightTree extends TreeListItem {

    @Pid
    RightType rightType;
    @Pid
    String projectName;
    @Pid
    String path;
    @Pid
    ModuleRightFormulaType rightFormulaType;


    @TreeItemAnnotation(customItems = ModuleRightFormulaType.class)
    public ModuleRightTree(ModuleRightFormulaType rightFormulaType, RightType rightType, String path, String projectName) {
        this.rightFormulaType = rightFormulaType;
        this.caption = rightFormulaType.getName();
        this.imageClass = rightFormulaType.getImageClass();
        this.id = rightFormulaType.getType();
        this.rightType = rightType;
        this.path = path;
        this.projectName = projectName;

    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public RightType getRightType() {
        return rightType;
    }

    public void setRightType(RightType rightType) {
        this.rightType = rightType;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public ModuleRightFormulaType getRightFormulaType() {
        return rightFormulaType;
    }

    public void setRightFormulaType(ModuleRightFormulaType rightFormulaType) {
        this.rightFormulaType = rightFormulaType;
    }
}
