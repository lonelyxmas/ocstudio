package com.ds.right.tree;

import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.right.item.BPMRightFormulaType;
import com.ds.right.item.RightFormulaType;
import com.ds.right.item.RightType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation
public class BPMRightTree extends TreeListItem {

    @Pid
    RightType rightType;
    @Pid
    String projectName;
    @Pid
    String activityDefId;
    @Pid
    String processDefId;
    @Pid
    BPMRightFormulaType rightFormulaType;


    @TreeItemAnnotation(customItems = BPMRightFormulaType.class)
    public BPMRightTree(BPMRightFormulaType rightFormulaType, RightType rightType, String activityDefId, String processDefId, String projectName) {
        this.rightFormulaType = rightFormulaType;
        this.caption = rightFormulaType.getName();
        this.imageClass = rightFormulaType.getImageClass();
        this.id = rightFormulaType.getType();
        this.rightType = rightType;
        this.activityDefId = activityDefId;
        this.processDefId = processDefId;
        this.projectName = projectName;

    }

    public String getActivityDefId() {
        return activityDefId;
    }

    public void setActivityDefId(String activityDefId) {
        this.activityDefId = activityDefId;
    }

    public String getProcessDefId() {
        return processDefId;
    }

    public void setProcessDefId(String processDefId) {
        this.processDefId = processDefId;
    }

    public RightType getRightType() {
        return rightType;
    }

    public void setRightType(RightType rightType) {
        this.rightType = rightType;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public BPMRightFormulaType getRightFormulaType() {
        return rightFormulaType;
    }

    public void setRightFormulaType(BPMRightFormulaType rightFormulaType) {
        this.rightFormulaType = rightFormulaType;
    }
}
