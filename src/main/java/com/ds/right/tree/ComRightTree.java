package com.ds.right.tree;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.right.item.ComRightFormulaType;
import com.ds.right.item.RightType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(singleOpen = true)
@TreeAnnotation
public class ComRightTree extends TreeListItem {

    @Pid
    RightType rightType;
    @Pid
    String projectName;
    @Pid
    String currClassName;
    @Pid
    ComRightFormulaType rightFormulaType;


    @TreeItemAnnotation(customItems = ComRightFormulaType.class)
    public ComRightTree(ComRightFormulaType rightFormulaType, RightType rightType, String currClassName, String projectName) {
        this.rightFormulaType = rightFormulaType;
        this.caption = rightFormulaType.getName();
        this.imageClass = rightFormulaType.getImageClass();
        this.id = rightFormulaType.getType();
        this.rightType = rightType;
        this.currClassName = currClassName;
        this.projectName = projectName;

    }

    public String getCurrClassName() {
        return currClassName;
    }

    public void setCurrClassName(String currClassName) {
        this.currClassName = currClassName;
    }

    public RightType getRightType() {
        return rightType;
    }

    public void setRightType(RightType rightType) {
        this.rightType = rightType;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public ComRightFormulaType getRightFormulaType() {
        return rightFormulaType;
    }

    public void setRightFormulaType(ComRightFormulaType rightFormulaType) {
        this.rightFormulaType = rightFormulaType;
    }
}
