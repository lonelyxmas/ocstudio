package com.ds.right.org;

import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.OrgDomain;
import com.ds.esd.dsm.domain.enums.OrgDomainType;
import com.ds.esd.util.TreePageUtil;
import com.ds.right.item.BPMRightFormulaType;
import com.ds.right.tree.BPMRightTree;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/esd/right/bpm/")
@MethodChinaName(cname = "流程授权", imageClass = "bpmfont bpmgongzuoliu")
@Aggregation( rootClass = BPMTreeRightIndex.class)
@OrgDomain(type = OrgDomainType.right)
public class BPMTreeRightIndex {

    @MethodChinaName(cname = "流程授权")
    @RequestMapping(method = RequestMethod.POST, value = "BPMRightNav")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "600", height = "450")
    @ModuleAnnotation(dynLoad = true, imageClass = "bpmfont bpmgongzuoliu", caption = "流程授权")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<BPMRightTree>> getOrgRightNav(String activityDefId, String processDefId, String projectName) {
        return TreePageUtil.getTreeList(Arrays.asList(BPMRightFormulaType.values()), BPMRightTree.class);
    }

}
