package com.ds.right.org;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.common.JDSException;
import com.ds.common.util.StringUtility;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esb.config.formula.FormulaType;

import com.ds.esd.bpm.BPMFactory;

import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.Project;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.GridViewAnnotation;
import com.ds.esd.custom.annotation.PopTreeViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;

import com.ds.esd.dsm.domain.annotation.OrgDomain;
import com.ds.esd.dsm.domain.enums.OrgDomainType;
import com.ds.esd.project.config.ProjectConfig;
import com.ds.esd.project.config.formula.FormulaInst;
import com.ds.esd.util.TreePageUtil;
import com.ds.right.module.view.FormulaInstNav;
import com.ds.right.org.tree.InsteadSignFormulaPopTree;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(path = "/esd/right/org/insteadsign/")
@MethodChinaName(cname = "代签人授权", imageClass = "bpmfont bpmgongzuoliujilu")
@Aggregation(rootClass =InsteadSignFormulaService.class )
@OrgDomain(type=OrgDomainType.right)
public class InsteadSignFormulaService {
    @RequestMapping(method = RequestMethod.POST, value = "Index")
    @GridViewAnnotation
    @ModuleAnnotation(imageClass = "bpmfont bpmgongzuoliujilu",  caption = "代签人授权")
    @APIEventAnnotation(autoRun = true, bindMenu = {CustomMenuItem.reload,CustomMenuItem.index, CustomMenuItem.treeNodeEditor})
    @ResponseBody
    public ListResultModel<List<InsteadSignGridView>> getFormulaInstList(String projectName) {
        ListResultModel<List<InsteadSignGridView>> resultModel = new ListResultModel<>();
        try {
            Project config = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> instMap = config.getFormulas();
            List<FormulaInst> instList = new ArrayList<>();
            for (FormulaInst inst : instMap.values()) {
                if (inst != null && inst.getFormulaType() != null && inst.getFormulaType().equals(FormulaType.InsteadSignSelectedID)) {
                    instList.add(inst);
                }
            }
            resultModel = PageUtil.getDefaultPageList(instList, InsteadSignGridView.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;

    }




    @PopTreeViewAnnotation()
    @ModuleAnnotation(dynLoad = true )
    @APIEventAnnotation(bindMenu = CustomMenuItem.add)
    @RequestMapping("SelectFormula")
    @DialogAnnotation(width = "300", height = "450")
    @ResponseBody
    public TreeListResultModel<List<InsteadSignFormulaPopTree>> getFormulaTree(String activityDefId,String processDefId) {
        TreeListResultModel<List<InsteadSignFormulaPopTree>> model = new TreeListResultModel<>();
        model = TreePageUtil.getTreeList(Arrays.asList(FormulaType.InsteadSignSelectedID), InsteadSignFormulaPopTree.class);
        return model;
    }


    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @RequestMapping("loadChildItem")
    @ResponseBody
    public TreeListResultModel<List<InsteadSignFormulaPopTree>> loadChildItem(String activityDefId,String processDefId, FormulaType formulaType) {
        TreeListResultModel<List<InsteadSignFormulaPopTree>> model = new TreeListResultModel<>();
        try {
            List<ParticipantSelect> selects = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulas(formulaType);
            model = TreePageUtil.getTreeList(selects, InsteadSignFormulaPopTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return model;
    }



    @MethodChinaName(cname = "选择 表达式信息")
    @RequestMapping(value = {"addFormula"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.treeSave})
    public @ResponseBody
    ResultModel<Boolean> addFormulaInst(String SelectFormulaTree, String projectName) {
        ResultModel<Boolean> resultModel = new ResultModel<>();
        try {
            Project config = ESDFacrory.getESDClient().getProjectByName(projectName);
            String[] formulaIdArr = StringUtility.split(SelectFormulaTree, ";");
            for (String id : formulaIdArr) {
                ParticipantSelect select = BPMFactory.getInstance(ESDFacrory.getESDClient().getSpace()).getFormulaById(id);
                FormulaInst inst = new FormulaInst();
                inst.setExpression(select.getFormula());
                inst.setFormulaType(select.getFormulaType());
                inst.setParticipantSelectId(select.getParticipantSelectId());
                inst.setName(select.getSelectName());
                inst.setSelectDesc(select.getSelectDesc());
                inst.setFormulaInstId(UUID.randomUUID().toString());
                config.getFormulas().put(inst.getFormulaInstId(), inst);
                ESDFacrory.getESDClient().updateFormulaConfig(config.getId(),inst);
            }



        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @MethodChinaName(cname = "保存表达式信息")
    @RequestMapping(value = {"saveFormula"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.formSave})
    public @ResponseBody
    ResultModel<Boolean> saveFormulaInst(@RequestBody FormulaInst inst) {
        ResultModel<Boolean> resultModel = new ResultModel<>();
        try {
            Project config = ESDFacrory.getESDClient().getProjectVersionByName(inst.getProjectName()).getProject();
            ESDFacrory.getESDClient().updateFormulaConfig(config.getId(),inst);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FormulaInstInfo")
    @NavGroupViewAnnotation()
    @DialogAnnotation
    @ModuleAnnotation(imageClass =  "获取表达式信息")
    @APIEventAnnotation(callback = {CustomCallBack.ReloadParent, CustomCallBack.Close}, bindMenu = {CustomMenuItem.editor})
    public @ResponseBody
    ResultModel<FormulaInstNav> getFormulaInstInfo(String formulaInstId) {
        ResultModel<FormulaInstNav> model = new ResultModel<FormulaInstNav>();
        return model;
    }

    @MethodChinaName(cname = "删除表达式")
    @RequestMapping(value = {"delFormulaInst"}, method = {RequestMethod.GET, RequestMethod.POST})
    @APIEventAnnotation(callback = {CustomCallBack.Reload, CustomCallBack.ReloadParent}, bindMenu = {CustomMenuItem.delete})
    public @ResponseBody
    ResultModel<Boolean> delFormulaInst(String formulaInstId, String projectName) {
        ResultModel<Boolean> resultModel = new ResultModel<>();
        try {
            Project config = ESDFacrory.getESDClient().getProjectByName(projectName);
            Map<String, FormulaInst> instMap = config.getFormulas();
            String[] formulaInstIdArr = StringUtility.split(formulaInstId, ";");
            for (String id : formulaInstIdArr) {
                instMap.remove(id);
                ESDFacrory.getESDClient().deleteFormulaConfig(config.getId(),id);
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }


}
