package com.ds.right.org.tree;

import com.ds.bpm.formula.ParticipantSelect;
import com.ds.esb.config.formula.FormulaType;
import com.ds.esd.custom.annotation.PopTreeAnnotation;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.toolbar.BottomBarMenu;
import com.ds.esd.custom.form.enums.CustomFormMenu;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.SelModeType;
import com.ds.right.org.PerformFormulaService;
import com.ds.web.annotation.Pid;
import com.ds.web.annotation.Uid;

@BottomBarMenu()
@TreeAnnotation(heplBar = true, selMode = SelModeType.multibycheckbox, bottombarMenu = {CustomFormMenu.Save, CustomFormMenu.Close}, customService = {PerformFormulaService.class})
@PopTreeAnnotation(caption = "添加表达式")
public class PerformFormulaPopTree extends TreeListItem {


    @Pid
    public String formulaInstId;
    @Pid
    public String activityDefId;
    @Pid
    public String processDefId;
    @Pid
    public FormulaType formulaType;
    @Uid
    public String selectId;


    @TreeItemAnnotation(bindService = PerformFormulaService.class)
    public PerformFormulaPopTree(FormulaType formulaType, String activityDefId, String processDefId) {
        this.caption = formulaType.getName();
        this.id = formulaType.getType();
        this.imageClass = formulaType.getImageClass();
        this.formulaType = formulaType;
        this.activityDefId = activityDefId;
        this.processDefId = processDefId;
    }

    @TreeItemAnnotation(imageClass = "spafont spa-icon-function")
    public PerformFormulaPopTree(ParticipantSelect select, String activityDefId, String processDefId) {
        this.caption = select.getSelectName();
        this.id = select.getParticipantSelectId();
        this.tips = select.getFormula();
        this.selectId = select.getParticipantSelectId();
        this.formulaType = select.getFormulaType();
        this.activityDefId = activityDefId;
        this.processDefId = processDefId;


    }

    public String getActivityDefId() {
        return activityDefId;
    }

    public void setActivityDefId(String activityDefId) {
        this.activityDefId = activityDefId;
    }

    public String getProcessDefId() {
        return processDefId;
    }

    public void setProcessDefId(String processDefId) {
        this.processDefId = processDefId;
    }

    public FormulaType getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(FormulaType formulaType) {
        this.formulaType = formulaType;
    }

    public String getSelectId() {
        return selectId;
    }

    public void setSelectId(String selectId) {
        this.selectId = selectId;
    }

    public String getFormulaInstId() {
        return formulaInstId;
    }

    public void setFormulaInstId(String formulaInstId) {
        this.formulaInstId = formulaInstId;
    }
}
