package com.ds.right;

import com.ds.config.TreeListResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.DialogAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.domain.annotation.OrgDomain;
import com.ds.esd.dsm.domain.enums.OrgDomainType;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.util.TreePageUtil;
import com.ds.right.item.BPMRightFormulaType;
import com.ds.right.item.ComRightFormulaType;
import com.ds.right.item.ModuleRightFormulaType;
import com.ds.right.tree.BPMRightTree;
import com.ds.right.tree.ComRightTree;
import com.ds.right.tree.ModuleRightTree;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/esd/right/")
@MethodChinaName(cname = "权限管理")
@MenuBarMenu(menuType = CustomMenuType.menubar, caption = "权限管理", index = 6, imageClass = "bpmfont bpmgongzuoliu")
//插件显示位置
@Aggregation( rootClass = RightIndex.class)//声明为Domin域
@OrgDomain(type = OrgDomainType.right)//构建当前域环境
public class RightIndex {

    @MethodChinaName(cname = "组件授权")
    @RequestMapping(method = RequestMethod.POST, value = "ComRight")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "750", height = "600")
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-astext", caption = "组件授权")
    @CustomAnnotation(imageClass = "spafont spa-icon-astext")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName, RequestPathEnum.SPA_className})
    @ResponseBody
    public TreeListResultModel<List<ComRightTree>> getComRight(String id, String projectName, String currClassName) {
        return TreePageUtil.getTreeList(Arrays.asList(ComRightFormulaType.values()), ComRightTree.class);
    }

    @MethodChinaName(cname = "模块授权")
    @RequestMapping(method = RequestMethod.POST, value = "ModuleRight")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "750", height = "600")
    @CustomAnnotation(imageClass = "spafont spa-icon-moveforward")
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-moveforward", caption = "模块授权")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName, RequestPathEnum.SPA_className})
    @ResponseBody
    public TreeListResultModel<List<ModuleRightTree>> getModuleRight(String id, String projectName, String className) {
        return TreePageUtil.getTreeList(Arrays.asList(ModuleRightFormulaType.values()), ModuleRightTree.class);
    }

    @MethodChinaName(cname = "流程授权")
    @RequestMapping(method = RequestMethod.POST, value = "BpmRight")
    @NavTreeViewAnnotation
    @DialogAnnotation(width = "750", height = "600")
    @CustomAnnotation(imageClass = "bpmfont bpmgongzuoliuzhutiguizeweihuguanli")
    @ModuleAnnotation( dynLoad = true, imageClass = "bpmfont bpmgongzuoliuzhutiguizeweihuguanli", caption = "流程授权")
    @APIEventAnnotation(customRequestData = {RequestPathEnum.SPA_projectName, RequestPathEnum.SPA_className})
    @ResponseBody
    public TreeListResultModel<List<BPMRightTree>> getBpmRight(String id, String projectName, String className) {
        return TreePageUtil.getTreeList(Arrays.asList(BPMRightFormulaType.values()), BPMRightTree.class);
    }

}
