package com.ds.debug.bean;

import com.ds.debug.meun.FileContextMenuAction;
import com.ds.debug.meun.PreviewTopBar;
import com.ds.debug.meun.TopPackageContextMenuAction;
import com.ds.debug.service.*;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.ESDClass;
import com.ds.esd.custom.annotation.TreeAnnotation;
import com.ds.esd.custom.annotation.TreeItemAnnotation;
import com.ds.esd.custom.annotation.nav.TabsAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.annotation.toolbar.RightContextMenu;
import com.ds.esd.custom.bean.CustomViewBean;
import com.ds.esd.custom.bean.enums.PanelType;
import com.ds.esd.custom.layout.annotation.LayoutAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutItemAnnotation;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.enums.PosType;
import com.ds.web.annotation.Pid;

@TabsAnnotation(closeBtn = true)
@MenuBarMenu(menuClasses = PreviewTopBar.class)
@TreeAnnotation(caption = "菜单树",
        customService = ProjectTreeService.class,
        lazyLoad = true, heplBar = true)
@LayoutAnnotation(items = {
        @LayoutItemAnnotation(pos = PosType.before, size = 260, max = 1000)
})
public class ConfigModuleTree extends TreeListItem {


    @Pid
    String parentId;
    @Pid
    String projectName;
    @Pid
    String packageName;
    @Pid
    String euPackageName;
    @Pid
    PanelType panelType;
    @Pid
    String viewClassName;
    @Pid
    String sourceClassName;
    @Pid
    String sourceMethodName;
    @Pid
    String viewInstId;
    @Pid
    String domainId;

    @Pid
    String javaClassName;

    @Pid
    String methodName;

    @TreeItemAnnotation(bindService = ProjectTreeService.class, imageClass = "spafont spa-icon-alignh")
    public ConfigModuleTree(ProjectVersion version, String euPackageName, String domainId, String viewInstId) {
        this.caption = version.getProject().getProjectName();
        this.id = version.getVersionId();
        this.euPackageName = euPackageName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;
    }


    @RightContextMenu(menuClass = TopPackageContextMenuAction.class)
    @TreeItemAnnotation(bindService = TopPackageTreeService.class, dynDestory = true, iniFold = true, deepSearch = true, lazyLoad = true)
    public ConfigModuleTree(TopPackage topPackage, String euPackageName, String domainId, String viewInstId) {
        EUPackage euPackage = topPackage.getEuPackage();
        this.caption = euPackage.getDesc();
        this.imageClass = euPackage.getImageClass();
        this.packageName = euPackage.getPackageName();
        this.id = euPackage.getFolder().getID();
        this.parentId = euPackage.getId();
        this.domainId = domainId;
        this.euPackageName = euPackageName;
        this.viewInstId = viewInstId;

    }

    @RightContextMenu(menuClass = TopPackageContextMenuAction.class)
    @TreeItemAnnotation(bindService = PackageTreeService.class, imageClass = "spafont spa-icon-package", deepSearch = true, iniFold = true, dynDestory = true, lazyLoad = true)
    public ConfigModuleTree(EUPackage euPackage, String euPackageName, String domainId, String viewInstId) {
        this.imageClass = euPackage.getImageClass();
        this.packageName = euPackage.getPackageName();
        this.caption = euPackage.getDesc().indexOf(packageName) > -1 ? euPackage.getDesc() : packageName + "(" + euPackage.getDesc() + ")";
        this.id = euPackage.getFolder().getID();
        this.parentId = euPackage.getId();
        this.euPackageName = euPackageName;
        this.domainId = domainId;
        this.viewInstId = viewInstId;

    }

    @TreeItemAnnotation(bindService = JavaBeanService.class, imageClass = "spafont spa-icon-json-file", iniFold = true, dynDestory = true, lazyLoad = true)
    public ConfigModuleTree(JavaSrcBean javaSrcBean, String domainId, String viewInstId) {
        this.id = javaSrcBean.getClassName();
        this.domainId = domainId;
        this.javaClassName = javaSrcBean.getClassName();
        this.caption = javaClassName.substring(javaClassName.lastIndexOf(".") + 1);
        this.sourceClassName = javaSrcBean.getSourceClassName();
        this.viewInstId = viewInstId;
    }


    @RightContextMenu(menuClass = FileContextMenuAction.class)
    @TreeItemAnnotation(bindService = ModuleTreeService.class, imageClass = "spafont spa-icon-page", iniFold = true, dynDestory = true, lazyLoad = true)
    public ConfigModuleTree(ESDClass esdClass, String domainId, String viewInstId) {
        this.id = esdClass.getClassName();
        this.domainId = domainId;
        this.caption = esdClass.getDesc() + "(" + esdClass.getClassName() + ")";
        this.sourceClassName = esdClass.getClassName();
        this.viewInstId = viewInstId;
    }


    @TreeItemAnnotation(bindService = ModuleMethodService.class, imageClass = "spafont spa-icon-page")
    public ConfigModuleTree(CustomViewBean customView, String methodName, String sourceClassName, String domainId, String viewInstId) {
        this.methodName = customView.getMethodName();
        if (customView.getCaption() != null && !customView.getCaption().equals("")) {
            this.caption = customView.getCaption() + "(" + customView.getName() + ")";
        } else {
            this.caption = methodName + "(" + customView.getName() + ")-" + customView.getModuleViewType().getName();
        }
        this.domainId = domainId;
        this.viewInstId = viewInstId;
        this.panelType = customView.getMethodConfig().getModuleBean().getPanelType();
        this.sourceClassName = sourceClassName;
        this.viewClassName = customView.getViewClassName();
        this.sourceMethodName = methodName;
        this.id = sourceClassName.replace(".", "_") + "_" + methodName;
        this.imageClass = customView.getImageClass();
        this.tips = customView.getCaption() + "(" + customView.getModuleViewType().getName() + ")<br/>";
        this.tips = tips + methodName + "(" + customView.getName() + ")";
    }

    @TreeItemAnnotation(customItems = ConfigModuleNavItem.class)
    public ConfigModuleTree(ConfigModuleNavItem apiNavItem, String domainId, String sourceClassName, String packageName, String viewInstId) {
        this.imageClass = apiNavItem.getImageClass();
        this.packageName = packageName;
        this.sourceClassName = sourceClassName;
        this.caption = apiNavItem.getName();
        this.viewInstId = viewInstId;
        this.id = sourceClassName + "_" + apiNavItem.getType();
        this.domainId = domainId;

    }

    public String getSourceMethodName() {
        return sourceMethodName;
    }

    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethodName = sourceMethodName;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public void setJavaClassName(String javaClassName) {
        this.javaClassName = javaClassName;
    }

    public String getViewInstId() {
        return viewInstId;
    }

    public void setViewInstId(String viewInstId) {
        this.viewInstId = viewInstId;
    }

    public String getEuPackageName() {
        return euPackageName;
    }

    public void setEuPackageName(String euPackageName) {
        this.euPackageName = euPackageName;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public PanelType getPanelType() {
        return panelType;
    }

    public void setPanelType(PanelType panelType) {
        this.panelType = panelType;
    }

    public String getViewClassName() {
        return viewClassName;
    }

    public void setViewClassName(String viewClassName) {
        this.viewClassName = viewClassName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
}
