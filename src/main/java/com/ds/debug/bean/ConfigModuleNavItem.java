package com.ds.debug.bean;

import com.ds.debug.service.JavaSrcListService;
import com.ds.debug.service.MethodTreeService;
import com.ds.debug.service.ModuleViewService;
import com.ds.dsm.aggregation.config.entity.tree.AggEntityMethodNavService;
import com.ds.dsm.aggregation.config.entity.tree.field.AggEntityFieldsService;
import com.ds.esd.custom.tree.enums.TreeItem;

public enum ConfigModuleNavItem implements TreeItem {
    ViewConfig("视图配置", "spafont spa-icon-rendermode", ModuleViewService.class, false, true, true),
    MethodConfig("视图路由", "spafont spa-icon-c-webapi", MethodTreeService.class, false, true, true),
    CustomMethodConfig("领域事件", "spafont spa-icon-conf", AggEntityMethodNavService.class, false, true, true),
    JavaSrc("Java源码", "spafont spa-icon-json-file", JavaSrcListService.class, true, true, true),
    FieldsConfig("实体子项", "spafont spa-icon-c-comboinput", AggEntityFieldsService.class, true, true, true);

    private final String name;
    private final Class bindClass;
    private final String imageClass;
    private final boolean iniFold;
    private final boolean dynDestory;
    private final boolean dynLoad;

    ConfigModuleNavItem(String name, String imageClass, Class bindClass, Boolean iniFold, Boolean dynLoad, Boolean dynDestory) {
        this.name = name;
        this.imageClass = imageClass;
        this.bindClass = bindClass;

        this.iniFold = iniFold;
        this.dynLoad = dynLoad;
        this.dynDestory = dynDestory;

    }

    @Override
    public boolean isIniFold() {
        return iniFold;
    }

    @Override
    public boolean isDynDestory() {
        return dynDestory;
    }

    @Override
    public boolean isDynLoad() {
        return dynLoad;
    }

    public Class getBindClass() {
        return bindClass;
    }

    @Override
    public String toString() {
        return name();
    }

    @Override
    public String getType() {
        return name();
    }

    @Override
    public String getName() {
        return name;
    }

    public String getImageClass() {
        return imageClass;
    }
}
