package com.ds.debug.bean;

import com.ds.esd.tool.module.EUPackage;

public class TopPackage {

    private final EUPackage euPackage;


    public TopPackage(EUPackage euPackage) {
        this.euPackage = euPackage;
    }

    public EUPackage getEuPackage() {
        return euPackage;
    }
}
