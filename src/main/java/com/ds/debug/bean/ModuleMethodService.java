package com.ds.debug.bean;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/dsm/config/module/")
@MethodChinaName(cname = "子模块", imageClass = "spafont spa-icon-c-comboinput")
public class ModuleMethodService {


    @RequestMapping(method = RequestMethod.POST, value = "loadChildModule")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> loadChildModule(String viewInstId, String domainId, String sourceClassName, String methodName, String sourceMethodName, String groupId) {
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<List<ViewConfigTree>>();
        List<Object> obj = new ArrayList<>();
        ApiClassConfig customESDClassAPIBean = null;
        try {
            customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            MethodConfig methodAPIBean = customESDClassAPIBean.getMethodByName(methodName);
            obj.add(methodAPIBean);
            if (methodAPIBean.getView() != null) {
                obj.add(methodAPIBean.getView());
            }
            resultModel = TreePageUtil.getTreeList(obj, ViewConfigTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

}
