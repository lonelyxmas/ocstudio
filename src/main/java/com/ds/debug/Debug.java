package com.ds.debug;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.common.JDSException;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/Debug/")
@MethodChinaName(cname = "预览")
public class Debug {


    public Debug() {

    }

//    @RequestMapping(method = RequestMethod.POST, value = "Index")
//    @DynLoadAnnotation
//    @ModuleAnnotation(caption = "调试")
//    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.index)
//    @ResponseBody
//    public ListResultModel<List<Component>> index(String packageName, String spaceName, String projectId) {
//        ListResultModel<List<Component>> result = new ListResultModel<List<Component>>();
//        try {
//            Map<String, Object> valueMap = JDSActionContext.getActionContext().getContext();
//            valueMap.put("projectId", projectId);
//            List<Component> components = new ArrayList<>();
//            if (packageName == null || packageName.equals("")) {
//                packageName = "com.ds";
//                valueMap.put("packageName", packageName);
//            }
//            if (spaceName == null || spaceName.equals("")) {
//                spaceName = "test";
//                valueMap.put("spaceName", spaceName);
//            }
//
//            valueMap.put("packageName", packageName);
//            valueMap.put("spaceName", spaceName);
//            ESDFacrory.getESDClient().buildCustomModule(projectId, packageName, spaceName, valueMap, null);
//            EUModule module = this.getEsdClient().buildDynCustomModule(FullDebugGalleryComponent.class, valueMap, true);
//            result.setData(module.getTopComponents(true));
//        } catch (JDSException e) {
//            e.printStackTrace();
//            result = new ErrorListResultModel<>();
//            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
//            ((ErrorListResultModel) result).setErrdes(e.getMessage());
//        }
//        return result;
//    }

    @JSONField(serialize = false)
    public ESDClient getEsdClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }

}
