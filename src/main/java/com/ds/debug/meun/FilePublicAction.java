package com.ds.debug.meun;

import com.ds.common.JDSException;
import com.ds.context.JDSActionContext;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.tool.module.EUModule;
import com.ds.web.RemoteConnectionManager;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import com.ds.web.json.JSONData;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


@Controller
@RequestMapping(value = {"/dsm/config/debug/file/"})
@Aggregation(type=AggregationType.menu,rootClass =FilePublicAction.class )
public class FilePublicAction {


    @MethodChinaName(cname = "发布本地")
    @RequestMapping(method = RequestMethod.POST, value = "publicLocal")
    @CustomAnnotation(imageClass = "iconfont iconbug")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview)
    @ResponseBody
    public void publicLocal(String projectName,  String className ) {

        if (projectName != null && className != null) {
            RemoteConnectionManager.getConntctionService(projectName.toString()).execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        EUModule euModule= ESDFacrory.getESDClient().getModule(className,projectName);
                        ESDFacrory.getESDClient().publicLocal(projectName.toString(), euModule, null, true);
                    } catch (JDSException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @MethodChinaName(cname = "发布远程")
    @RequestMapping(method = RequestMethod.POST, value = "publicRemote")
    @CustomAnnotation(imageClass = "iconfont iconicon_fabu")
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview)
    @ResponseBody
    public void publicRemote(String projectName, @JSONData Map<String, String> tagVar) {
        String className = tagVar.get("euClassName");
        if (projectName != null && className != null) {
            RemoteConnectionManager.getConntctionService(projectName.toString()).execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        ESDFacrory.getESDClient().publicRemote(projectName.toString(), className.toString(), null, false);
                    } catch (JDSException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
