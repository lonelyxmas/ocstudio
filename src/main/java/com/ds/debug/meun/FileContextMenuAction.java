package com.ds.debug.meun;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.debug.bean.ConfigModuleTree;
import com.ds.editor.ESDEditor;
import com.ds.editor.cmd.ESDChrome;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.CustomViewFactory;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.toolbar.MenuBarMenu;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.api.enums.CustomCallBack;
import com.ds.esd.custom.api.enums.RequestPathEnum;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.aggregation.DomainInst;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.tool.enums.FileType;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.ui.enums.ComponentType;
import com.ds.esd.tool.ui.module.ModuleComponent;
import com.ds.web.annotation.Aggregation;
import com.ds.web.annotation.AggregationType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;


@Controller
@RequestMapping(value = {"/dsm/config/toppackage/file/"})
@Aggregation(type = AggregationType.menu, rootClass = FileContextMenuAction.class)
@MenuBarMenu(menuType = CustomMenuType.preview, caption = "菜单")
public class FileContextMenuAction {


    @MethodChinaName(cname = "编译")
    @RequestMapping(method = RequestMethod.POST, value = "dsmBuild")
    @CustomAnnotation(imageClass = "spafont spa-icon-moveforward", index = 1)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> dsmBuild(String id, String projectName, String domainId, String parentId, String packageName, String className) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();
        try {
            Map<String, ?> valueMap = JDSActionContext.getActionContext().getContext();
            result.setIds(Arrays.asList(new String[]{parentId}));
            if (className != null) {
                ESDFacrory.getESDClient().rebuildCustomModule(className, projectName, valueMap);
            } else if (packageName != null) {
                ESDFacrory.getESDClient().buildPackage(projectName, packageName, domainId, valueMap, this.getCurrChromeDriver());
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }

    @MethodChinaName(cname = "重置")
    @RequestMapping(method = RequestMethod.POST, value = "reSet")
    @CustomAnnotation(imageClass = "fa fa-lg fa-close", index = 2)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> reSet(String id, String projectName, String domainId, String sourceClassName) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();
        ESDChrome chrome = getCurrChromeDriver();
        try {
            List<MethodConfig> allViewConfigs = new ArrayList<>();
            DomainInst domainInst = DSMFactory.getInstance().getAggregationManager().getDomainInstById(domainId);
            ApiClassConfig apiClassConfig = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            allViewConfigs.addAll(apiClassConfig.getAllMethods());
            List<MethodConfig> configs = apiClassConfig.getAllProxyMethods();
            Set<String> esdClassNames = new HashSet<>();
            esdClassNames.add(sourceClassName);
            for (MethodConfig methodConfig : configs) {
                if (methodConfig.getViewClassName() != null) {
                    esdClassNames.add(methodConfig.getViewClassName());
                }
            }
            DSMFactory.getInstance().getAggregationManager().delAggEntity(domainId, esdClassNames, true);
            DSMFactory.getInstance().getAggregationManager().deleteApiClassConfig(sourceClassName, domainId, true);
            for (MethodConfig methodAPIBean : allViewConfigs) {
                if (methodAPIBean.isModule()) {
                    EUModule module = ESDFacrory.getESDClient().getModule(methodAPIBean.getUrl(), domainInst.getProjectVersionName());
                    if (module != null) {
                        List<ModuleComponent> moduleComponents = module.getComponent().findComponents(ComponentType.Module, null);
                        for (ModuleComponent moduleComponent : moduleComponents) {
                            ESDFacrory.getESDClient().delModule(moduleComponent.getEuModule());
                        }
                        ESDFacrory.getESDClient().delModule(module);
                    }
                    CustomViewFactory.getInstance().buildView(methodAPIBean, domainInst.getProjectVersionName(), (Map<String, ?>) JDSActionContext.getActionContext().getContext(), true);
                }
            }
            EUPackage euPackage = ESDFacrory.getESDClient().getPackageByPath(projectName, apiClassConfig.getUrl());
            if (euPackage != null) {
                result.setIds(Arrays.asList(euPackage.getId()));
            }

        } catch (Exception e) {
            e.printStackTrace();
            chrome.printError(e.getMessage());
        }


        return result;
    }

    @MethodChinaName(cname = "刷新")
    @RequestMapping(method = RequestMethod.POST, value = "reLoad")
    @CustomAnnotation(imageClass = "fa fa-lg fa-circle-o-notch", index = 0)
    @APIEventAnnotation(customRequestData = RequestPathEnum.treeview, callback = CustomCallBack.TreeReloadNode)
    public @ResponseBody
    TreeListResultModel<List<ConfigModuleTree>> reLoad(String id, String projectName, String parentId, FileType type, String packageName) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<List<ConfigModuleTree>>();
        result.setIds(Arrays.asList(new String[]{parentId}));
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "FilePublicAction")
    @CustomAnnotation(index = 3, imageClass = "iconfont iconbug")
    @MenuBarMenu(menuType = CustomMenuType.sub, caption = "发布预览")
    public FilePublicAction getFilePublicAction() {
        return new FilePublicAction();
    }


    public ESDChrome getCurrChromeDriver() {
        Object handleId = JDSActionContext.getActionContext().getParams("handleId");
        ChromeDriver chrome = null;
        if (handleId != null) {
            chrome = ESDEditor.getInstance().getChromeDriverById(handleId.toString());
        }
        if (chrome == null) {
            chrome = ESDEditor.getInstance().getCurrChromeDriver();
        }
        return new ESDChrome(chrome);
    }


}
