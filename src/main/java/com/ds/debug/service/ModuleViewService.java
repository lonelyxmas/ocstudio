package com.ds.debug.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.dsm.view.config.tree.ViewConfigTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/config/package/childview/")
public class ModuleViewService {


    @RequestMapping(method = RequestMethod.POST, value = "loadChildView")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ViewConfigTree>> loadChildView(String viewInstId, String domainId, String sourceClassName) {
        TreeListResultModel<List<ViewConfigTree>> resultModel = new TreeListResultModel<List<ViewConfigTree>>();
        try {

            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            List<MethodConfig> customMethods = customESDClassAPIBean.getAllViewMethods();
            List<Object> customViews = new ArrayList<>();
            for (MethodConfig methodAPIBean : customMethods) {
                if (methodAPIBean.getView() != null) {
                    customViews.add(methodAPIBean.getView());
                }
            }
            resultModel = TreePageUtil.getTreeList(customViews, ViewConfigTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

}
