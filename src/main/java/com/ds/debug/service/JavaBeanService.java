package com.ds.debug.service;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.dsm.editor.view.JavaViewEditor;
import com.ds.esd.custom.annotation.FormViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.view.ViewInst;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dsm/config/javasrc/")
public class JavaBeanService {


    @RequestMapping(method = RequestMethod.POST, value = "ViewJavaEditor")
    @ModuleAnnotation(caption = "JAVA树", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @FormViewAnnotation()
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.treeNodeEditor)
    @ResponseBody
    public ResultModel<JavaViewEditor> getViewJavaEditor(String javaClassName, String viewInstId, String domainId, String sourceClassName, String methodName) {
        ResultModel<JavaViewEditor> result = new ResultModel<>();
        JavaSrcBean srcBean = null;
        try {
            if (viewInstId == null || viewInstId.equals("")) {
                viewInstId = domainId;
            }
            ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);
            srcBean = viewInst.getJavaSrcByClassName(javaClassName);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        JavaViewEditor editor = new JavaViewEditor(srcBean);
        result.setData(editor);
        return result;
    }


}
