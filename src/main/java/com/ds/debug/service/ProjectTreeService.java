package com.ds.debug.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.debug.bean.ConfigModuleTree;
import com.ds.debug.bean.TopPackage;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("/dsm/config/")
public class ProjectTreeService {
    @RequestMapping(method = RequestMethod.POST, value = "loadTree")
    @APIEventAnnotation(bindMenu = CustomMenuItem.treeReload)
    @ResponseBody
    public TreeListResultModel<List<ConfigModuleTree>> loadTree(String projectName, String id, String euPackageName, String parentId, String domainId) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            result = TreePageUtil.getTreeList(Arrays.asList(version), ConfigModuleTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(method = RequestMethod.POST, value = "loadChild")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ConfigModuleTree>> loadChild(String projectName, String euPackageName) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<>();
        List<TopPackage> childs = new ArrayList<>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            List<EUPackage> packages = ESDFacrory.getESDClient().getTopPackages(version.getVersionName());
            for (EUPackage euPackage : packages) {
                if (euPackageName == null || euPackageName.equals("") || euPackageName.startsWith(euPackage.getPackageName())) {
                    childs.add(new TopPackage(euPackage));
                }
            }
            Collections.sort(childs, new Comparator<TopPackage>() {
                public int compare(TopPackage o1, TopPackage o2) {
                    return o1.getEuPackage().getPackageName().compareTo(o2.getEuPackage().getPackageName());
                }
            });

            result = TreePageUtil.getTreeList(childs, ConfigModuleTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }

        return result;
    }

//
}
