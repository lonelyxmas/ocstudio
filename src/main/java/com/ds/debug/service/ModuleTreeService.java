package com.ds.debug.service;

import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.debug.bean.ConfigModuleNavItem;
import com.ds.debug.bean.ConfigModuleTree;
import com.ds.dsm.view.config.nav.ViewEntityNav;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.custom.annotation.nav.NavGroupViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/dsm/config/entity/")
public class ModuleTreeService {

    @RequestMapping(method = RequestMethod.POST, value = "loadChild")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ConfigModuleTree>> loadChild(String sourceClassName, String viewInstId, String domainId) {
        TreeListResultModel<List<ConfigModuleTree>> resultModel = new TreeListResultModel<List<ConfigModuleTree>>();
        resultModel = TreePageUtil.getTreeList(Arrays.asList(ConfigModuleNavItem.values()), ConfigModuleTree.class);
        return resultModel;
    }

    @APIEventAnnotation(bindMenu = CustomMenuItem.treeNodeEditor)
    @MethodChinaName(cname = "视图集合信息")
    @RequestMapping(method = RequestMethod.POST, value = "AggEntityConfig")
    @NavGroupViewAnnotation
    @ModuleAnnotation(dock = Dock.fill, caption = "视图集合信息", imageClass = "spafont spa-icon-c-grid")
    @ResponseBody
    public ResultModel<ViewEntityNav> getAggEntityConfig(String viewInstId, String domainId, String sourceClassName) {
        ResultModel<ViewEntityNav> result = new ResultModel<ViewEntityNav>();
        return result;
    }

}
