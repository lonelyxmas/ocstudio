package com.ds.debug.service;

import com.ds.common.JDSException;
import com.ds.config.TreeListResultModel;
import com.ds.debug.bean.ConfigModuleTree;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.bean.ApiClassConfig;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.dsm.DSMFactory;
import com.ds.esd.dsm.java.JavaSrcBean;
import com.ds.esd.dsm.view.ViewInst;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dsm/config/javasrc/")
public class JavaSrcListService {


    @RequestMapping(method = RequestMethod.POST, value = "loadJava")
    @APIEventAnnotation(bindMenu = CustomMenuItem.loadChild)
    @ResponseBody
    public TreeListResultModel<List<ConfigModuleTree>> loadChildView(String viewInstId, String domainId, String sourceClassName) {
        TreeListResultModel<List<ConfigModuleTree>> resultModel = new TreeListResultModel<List<ConfigModuleTree>>();
        try {
            if (viewInstId == null || viewInstId.equals("")) {
                viewInstId = domainId;
            }
            ViewInst viewInst = DSMFactory.getInstance().getViewManager().getViewInstById(viewInstId);

            List<JavaSrcBean> javaSrcBeans = new ArrayList<>();
            ApiClassConfig customESDClassAPIBean = DSMFactory.getInstance().getAggregationManager().getApiClassConfig(sourceClassName, domainId);
            List<MethodConfig> customMethods = customESDClassAPIBean.getAllMethods();
            for (MethodConfig methodAPIBean : customMethods) {
                if (methodAPIBean.getView() != null) {
                    javaSrcBeans.add(viewInst.getJavaSrcBeanByMethod(methodAPIBean));
                }
            }
            resultModel = TreePageUtil.getTreeList(javaSrcBeans, ConfigModuleTree.class);

        } catch (JDSException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

}
