package com.ds.debug;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.annotation.index.IndexViewAnnotation;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/Debug/")
@MethodChinaName(cname = "预览")
public class DebugMain {


    public DebugMain() {

    }


    @MethodChinaName(cname = "首页")
    @RequestMapping(method = RequestMethod.POST, value = "Index")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.index)
    @IndexViewAnnotation
    @ModuleAnnotation(caption = "首页")
    @ResponseBody
    public ResultModel<DebugIndex> index(String projectId) {
        ResultModel<DebugIndex> result = new ResultModel<DebugIndex>();
        return result;
    }


    @JSONField(serialize = false)
    public ESDClient getEsdClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }

}
