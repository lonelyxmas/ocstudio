package com.ds.debug.component;

import com.ds.common.JDSException;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.CustomModuleComponent;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.enums.CustomViewType;
import com.ds.esd.custom.nav.gallery.index.IndxeGalleryComponent;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.ui.component.item.GalleryNavItem;
import com.ds.esd.tool.ui.component.panel.GalleryProperties;
import com.ds.esd.tool.ui.enums.event.enums.GalleryEventEnum;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FullIndexGalleryComponent<M extends IndxeGalleryComponent<GalleryProperties<GalleryNavItem>, GalleryEventEnum, List<GalleryNavItem>>> extends CustomModuleComponent<M> {

    public FullIndexGalleryComponent() {
        super();
    }

    public FullIndexGalleryComponent(EUModule module, MethodConfig apiBean, Map<String, Object> valueMap) throws JDSException, ClassNotFoundException {
        super(module, apiBean, valueMap);
        EUModule[] modules = (EUModule[]) valueMap.get("NavModules");
        String packageNames = (String) valueMap.get("spaceName");
        if (packageNames != null && !packageNames.equals("")) {
            EUPackage euPackage = ESDFacrory.getESDClient().getPackageByPath(module.getProjectVersion().getVersionName(), packageNames);
            Set<EUModule> navModules = euPackage.listRealModules(CustomViewType.nav);
            Set<EUModule> navs = new HashSet<>();
            if (navModules.size() == 0) {
                navModules = euPackage.listRealModules(CustomViewType.listModule);
            }
            for (EUModule navModule : navModules) {
                if (!navModule.getRealClassName().equals(this.euModule.getRealClassName())) {
                    navs.add(navModule);
                }
            }
            modules = navs.toArray(new EUModule[]{});
        }
        M currComponent = (M) new IndxeGalleryComponent(euModule, modules);
        this.setCurrComponent(currComponent);
        this.addChildLayoutNav(currComponent);
    }

}
