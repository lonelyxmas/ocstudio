package com.ds.debug.component;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.common.JDSException;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.bean.MethodConfig;
import com.ds.esd.custom.bean.nav.gallery.NavGalleryViewBean;
import com.ds.esd.custom.nav.gallery.GalleryModuleComponent;
import com.ds.esd.custom.nav.gallery.index.IndxeGalleryComponent;
import com.ds.esd.custom.toolbar.dynbar.DynBar;
import com.ds.esd.custom.toolbar.dynbar.MenuDynBar;
import com.ds.esd.editor.enums.CustomMenuType;
import com.ds.esd.editor.extmenu.PluginsFactory;
import com.ds.esd.editor.extmenu.RADTopMenu;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.module.EUPackage;
import com.ds.esd.tool.ui.component.data.APICallerComponent;
import com.ds.esd.tool.ui.component.event.Action;
import com.ds.esd.tool.ui.component.event.Event;
import com.ds.esd.tool.ui.component.item.GalleryItem;
import com.ds.esd.tool.ui.component.list.TreeListItem;
import com.ds.esd.tool.ui.component.navigation.MenuBarComponent;
import com.ds.esd.tool.ui.component.navigation.MenuBarProperties;
import com.ds.esd.tool.ui.component.panel.GalleryComponent;
import com.ds.esd.tool.ui.component.panel.GalleryProperties;
import com.ds.esd.tool.ui.enums.Dock;
import com.ds.esd.tool.ui.enums.event.enums.GalleryEventEnum;
import com.ds.esd.tool.ui.enums.event.enums.MenuEventEnum;
import com.ds.esd.tool.ui.module.DSMProperties;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FullDebugGalleryComponent<M extends GalleryComponent<GalleryProperties<? extends GalleryItem>, GalleryEventEnum, List<? extends GalleryItem>>> extends GalleryModuleComponent<M> {

    public FullDebugGalleryComponent() {
        super();
    }

    public FullDebugGalleryComponent(EUModule module, MethodConfig apiBean, Map<String, Object> valueMap) throws JDSException, ClassNotFoundException {
        super(module, apiBean, valueMap);
        EUModule[] modules = (EUModule[]) valueMap.get("NavModules");
        String spaceName = (String) valueMap.get("spaceName");
        if (spaceName != null && !spaceName.equals("")) {
            EUPackage euPackage = ESDFacrory.getESDClient().getPackageByPath(module.getProjectVersion().getVersionName(), spaceName);
            Set<EUModule> navModules = euPackage.listModules();
            Set<EUModule> navs = new HashSet<>();
            for (EUModule navModule : navModules) {
                if (!navModule.getRealClassName().equals(this.euModule.getRealClassName())) {
                    navs.add(navModule);
                }
            }
            modules = navs.toArray(new EUModule[]{});
        }
        M currComponent = (M) new IndxeGalleryComponent(euModule, modules);
        this.addChildLayoutNav(getToolbars());
        this.setCurrComponent(currComponent);
        this.addChildNav(currComponent, (NavGalleryViewBean) apiBean.getView());
    }

    @JSONField(serialize = false)
    public MenuBarComponent getToolbars() {

        MenuBarComponent<MenuBarProperties, MenuEventEnum> component = new MenuBarComponent();
        MenuBarProperties barProperties = new MenuBarProperties();
        try {
            String domainId=null;
            DSMProperties dsmProperties=this.getProperties().getDsmProperties();
            if (dsmProperties!=null){
                domainId=dsmProperties.getDomainId();
            }

            List<DynBar> menus = PluginsFactory.getInstance().getAllViewBar(CustomMenuType.RADDebug, domainId);

            for (DynBar viewBar : menus) {
                RADTopMenu<MenuBarProperties, MenuEventEnum> topMenu = (RADTopMenu<MenuBarProperties, MenuEventEnum>) viewBar;

                TreeListItem baritem = new TreeListItem(viewBar.getId(), ((MenuDynBar) viewBar).getCaption(), ((MenuDynBar) viewBar).getImageClass());
                List<TreeListItem> items = topMenu.getProperties().getItems();
                for (TreeListItem citem : items) {
                    baritem.addChild(citem);
                }

                List<APICallerComponent> apiComponentNodes = topMenu.getApis();
                for (APICallerComponent apiComponentNode : apiComponentNodes) {
                    this.addChildren(apiComponentNode);
                }
                Event event = topMenu.getEvents().get(MenuEventEnum.onMenuSelected);
                for (Action action : event.getActions()) {
                    component.addEvent(MenuEventEnum.onMenuSelected, action);
                }

                barProperties.addItem(baritem);
            }
        } catch (JDSException e) {
            e.printStackTrace();
        }

        barProperties.setDock(Dock.top);
//
//        Map<String, Object> styleMap = new HashMap<>();
//
//        styleMap.put("background-color", "#8FBC8F");
//
//        com.ds.esd.tool.ui.component.CS cs = component.getCS();
//        if (cs == null) {
//            cs = new com.ds.esd.tool.ui.component.CS();
//        }
//        cs.setITEMS(styleMap);
//        component.setCS(cs);

        component.setProperties(barProperties);
        return component;
    }

}
