package com.ds.debug;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.context.JDSActionContext;
import com.ds.debug.component.FullIndexGalleryComponent;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDClient;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.custom.api.annotation.APIEventAnnotation;
import com.ds.esd.custom.enums.CustomMenuItem;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.module.EUModule;
import com.ds.esd.tool.ui.component.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/debugproject/")
@MethodChinaName(cname = "预览")
public class Preview {


    public Preview() {

    }

    @RequestMapping(method = RequestMethod.POST, value = "Index")
    @DynLoadAnnotation
    @ModuleAnnotation(caption = "预览")
    @APIEventAnnotation(autoRun = true, bindMenu = CustomMenuItem.index)
    @ResponseBody
    public ListResultModel<List<Component>> index(String packageName, String spaceName, String projectId) {
        ListResultModel<List<Component>> result = new ListResultModel<List<Component>>();
        try {
            Map<String, Object> valueMap = JDSActionContext.getActionContext().getContext();
            valueMap.put("projectId", projectId);
            List<Component> components = new ArrayList<>();
            if (packageName == null) {
                packageName = "com.ds";
            }
            if (spaceName == null || packageName.equals("")) {
                spaceName = "test";
            }
            valueMap.put("packageName", packageName);
            valueMap.put("spaceName", spaceName);
            ESDFacrory.getESDClient().buildCustomModule(projectId, packageName, spaceName, valueMap, null);
            EUModule module = this.getEsdClient().buildDynCustomModule(FullIndexGalleryComponent.class, valueMap, true);
            result.setData(module.getTopComponents(true));
        } catch (JDSException e) {
            e.printStackTrace();
            result = new ErrorListResultModel<>();
            ((ErrorListResultModel) result).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @JSONField(serialize = false)
    public ESDClient getEsdClient() throws JDSException {

        ESDClient client = ESDFacrory.getESDClient();

        return client;
    }

}
