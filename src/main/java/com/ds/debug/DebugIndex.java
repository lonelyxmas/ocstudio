package com.ds.debug;

import com.ds.common.JDSException;
import com.ds.config.ResultModel;
import com.ds.config.TreeListResultModel;
import com.ds.debug.bean.ConfigModuleTree;
import com.ds.dsm.editor.JavaMetaView;
import com.ds.dsm.nav.DSMNavItems;
import com.ds.dsm.nav.DSMNavTree;
import com.ds.enums.db.MethodChinaName;
import com.ds.esd.client.ESDFacrory;
import com.ds.esd.client.ProjectVersion;
import com.ds.esd.custom.annotation.CustomAnnotation;
import com.ds.esd.custom.annotation.nav.NavButtonViewsViewAnnotation;
import com.ds.esd.custom.annotation.nav.NavTreeViewAnnotation;
import com.ds.esd.custom.index.annotation.IndexAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutAnnotation;
import com.ds.esd.custom.layout.annotation.LayoutItemAnnotation;
import com.ds.esd.custom.module.annotation.DynLoadAnnotation;
import com.ds.esd.custom.module.annotation.ModuleAnnotation;
import com.ds.esd.tool.ui.enums.LayoutType;
import com.ds.esd.tool.ui.enums.PosType;
import com.ds.esd.util.TreePageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/Debug/")
@MethodChinaName(cname = "预览")
@IndexAnnotation
@LayoutAnnotation(transparent = false, type = LayoutType.vertical, items = {@LayoutItemAnnotation(panelBgClr = "#3498DB", size = 28, pos = PosType.before, locked = true, moveDisplay = false, cmd = false)})
public class DebugIndex {

    public DebugIndex() {

    }


    @RequestMapping(method = RequestMethod.POST, value = "ModuleTree")
    @ModuleAnnotation(dynLoad = true, caption = "运行配置", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @NavTreeViewAnnotation()
    @CustomAnnotation(index = 0)
    @ResponseBody
    public TreeListResultModel<List<ConfigModuleTree>> getModuleTree(String projectName, String id, String euPackageName) {
        TreeListResultModel<List<ConfigModuleTree>> result = new TreeListResultModel<>();
        try {
            ProjectVersion version = ESDFacrory.getESDClient().getProjectVersionByName(projectName);
            result = TreePageUtil.getTreeList(Arrays.asList(version), ConfigModuleTree.class);
        } catch (JDSException e) {
            e.printStackTrace();
        }
        return result;
    }


    @MethodChinaName(cname = "建模工厂")
    @RequestMapping(method = RequestMethod.POST, value = "DSMInstNavTree")
    @NavTreeViewAnnotation
    @ModuleAnnotation(dynLoad = true, imageClass = "spafont spa-icon-conf", caption = "建模工厂")
    @CustomAnnotation(index = 1)
    @ResponseBody
    public TreeListResultModel<List<DSMNavTree>> getDSMInstNavTree(String projectVersionName) {
        return TreePageUtil.getTreeList(Arrays.asList(DSMNavItems.values()), DSMNavTree.class);

    }


    @RequestMapping(method = RequestMethod.POST, value = "BuildTree")
    @ModuleAnnotation(cache = false, caption = "源码工程", imageClass = "bpmfont bpmgongzuoliuxitongpeizhi")
    @NavButtonViewsViewAnnotation()
    @DynLoadAnnotation
    @ResponseBody
    public ResultModel<List<JavaMetaView>> getBuildTree(String projectVersionName) {
        ResultModel<List<JavaMetaView>> result = new ResultModel<List<JavaMetaView>>();
        return result;
    }


}
