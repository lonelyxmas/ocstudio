package com.ds.iot.home;

import com.ds.iot.Area;
import com.ds.iot.Place;

import java.util.ArrayList;
import java.util.List;

public class XUIPlace {

    public String id;

    public String path;

    public boolean disabled = false;

    public boolean group = false;

    public boolean draggable = true;

    public String cls = "xui.Module";

    public String imageClass = "iconfont iconiframetianjia";

    public String caption;

    List<XUIPlace> sub;


    public XUIPlace(Place place) {
        this(place, false);
    }

    public XUIPlace(Place place, boolean hasArea) {
        this.id = place.getPlaceid();
        this.caption = place.getName();
        List<Place> childPlaces = place.getChilders();
        if (childPlaces != null) {
            for (Place childplace : childPlaces) {
                this.addSub(new XUIPlace(childplace, hasArea));
            }
        }
//
//        if (place.getChilders() != null && place.getChilders().size() > 0) {
//            this.setDisabled(true);
//        }

        if (hasArea) {
            this.setDisabled(true);
            List<Area> areas = place.getAreas();
            for (Area area : areas) {
                this.addSub(new XUIPlace(area));
            }
        }

    }

    public XUIPlace(Area area) {
        this.id = area.getAreaid();
        this.caption = area.getName();
        imageClass = "iconfont iconyuanquyunwei";
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public void addSub(XUIPlace subNode) {
        if (sub == null) {
            sub = new ArrayList<XUIPlace>();
        }
        sub.add(subNode);
    }

    public List<XUIPlace> getSub() {
        return sub;
    }

    public void setSub(List<XUIPlace> sub) {
        this.sub = sub;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
