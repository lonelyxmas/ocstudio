package com.ds.iot.log;

import com.ds.common.JDSException;
import com.ds.common.util.DateUtility;
import com.ds.iot.Area;
import com.ds.iot.DeviceEndPoint;
import com.ds.iot.ZNode;
import com.ds.iot.ct.CtIotFactory;
import com.ds.msg.SensorMsg;

import java.util.Date;

public class XUISensorLog {
    String value;
    String msgId;
    String datetime;
    String areaname;
    String htmlValue;
    String sensorId;
    String sensorName;

    public XUISensorLog() {

    }

    public XUISensorLog(SensorMsg msg) {

        this.msgId = msg.getId();
        this.sensorId = msg.getSensorId();


        if (msg.getEventTime() != null &&msg.getEventTime() >0) {
            this.setDatetime(DateUtility.formatDate(new Date(msg.getEventTime() ), "yyyy-MM-dd HH:mm:ss"));
        } else {
            this.setDatetime(DateUtility.formatDate(new Date(msg.getReceiveTime()), "yyyy-MM-dd HH:mm:ss"));
        }

        this.setHtmlValue(msg.getTitle());
        try {
            ZNode zode = CtIotFactory.getCtIotService().getZNodeById(sensorId);
            if (zode != null) {
                DeviceEndPoint endPoint = zode.getEndPoint();
                this.sensorId = endPoint.getIeeeaddress();
                Area area = null;
                try {
                    area = CtIotFactory.getCtIotService().getAreaById(endPoint.getDevice().getAreaid());
                } catch (JDSException e) {
                    e.printStackTrace();
                }
                if (area != null) {
                    this.areaname = area.getName();
                }
                this.sensorName = (endPoint.getName() == null || endPoint.getName().equals("")) ? endPoint.getSensortype().getName() : endPoint.getName();
            } else {
                this.sensorId = "0000000000";
                this.sensorName = "已删除设备";
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }
    }


    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getAreaname() {
        return areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }


    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public String getHtmlValue() {
        return htmlValue;
    }

    public void setHtmlValue(String htmlValue) {
        this.htmlValue = htmlValue;
    }


    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
