package com.ds.iot.device;

import com.ds.common.JDSException;
import com.ds.iot.Area;
import com.ds.iot.Device;
import com.ds.iot.Place;
import com.ds.iot.ct.CtIotFactory;
import com.ds.iot.enums.DeviceStatus;


public class XUIDevice {


    private final String deviceId;

    private String account;

    private String name;

    private String factory;

    private String subsyscode;

    private String placename;

    private String areaname;


    private String placeId;

    private String personname;

    private String personId;

    private String batch;

    private DeviceStatus deviceStatus = DeviceStatus.OFFLINE;

    private String ieee;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public XUIDevice(Device device) {
        this.account = device.getBindingaccount();
        this.factory = device.getFactory();
        this.subsyscode = device.getSubsyscode();
        this.placeId = device.getPlaceid();
        this.deviceId = device.getDeviceid();
        if (device.getPlaceid() != null) {
            try {
                Place place = CtIotFactory.getCtIotService().getPlaceById(device.getRootDevice().getPlaceid());
                if (place != null) {
                    this.placename = place.getName();
                }
            } catch (JDSException e) {
                e.printStackTrace();
            }
        } else {
            placename = "未绑定";
        }

        if (device.getAreaid() != null) {
            try {
                Area area = CtIotFactory.getCtIotService().getAreaById(device.getAreaid());
                if (area != null) {
                    this.areaname = area.getName();
                }
            } catch (JDSException e) {
                e.printStackTrace();
            }
        } else {
            areaname = "未绑定";
        }

        if (device.getDeviceEndPoints().size() > 0) {
            name = device.getDeviceEndPoints().get(0).getName();

        } else {
            name = device.getSensortype().getName();
        }

        this.batch = device.getBatch();
        this.ieee = device.getSerialno();
        deviceStatus = device.getStates();
        this.subsyscode = device.getSubsyscode();


    }

    public String getAreaname() {
        return areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getPlacename() {
        return placename;
    }

    public void setPlacename(String placename) {
        this.placename = placename;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getPersonname() {
        return personname;
    }

    public void setPersonname(String personname) {
        this.personname = personname;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }


    public DeviceStatus getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(DeviceStatus deviceStatus) {
        this.deviceStatus = deviceStatus;
    }


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public String getSubsyscode() {
        return subsyscode;
    }

    public void setSubsyscode(String subsyscode) {
        this.subsyscode = subsyscode;
    }


    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }


    public String getIeee() {
        return ieee;
    }

    public void setIeee(String ieee) {
        this.ieee = ieee;
    }


}
