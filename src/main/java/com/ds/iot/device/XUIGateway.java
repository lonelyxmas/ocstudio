package com.ds.iot.device;

import com.ds.common.JDSException;
import com.ds.engine.ConnectInfo;
import com.ds.engine.JDSSessionHandle;
import com.ds.iot.Device;
import com.ds.iot.Place;
import com.ds.iot.ct.CtIotFactory;
import com.ds.iot.enums.DeviceStatus;
import com.ds.org.Person;
import com.ds.org.PersonNotFoundException;
import com.ds.server.JDSServer;
import com.ds.server.OrgManagerFactory;

import java.util.Date;
import java.util.Set;


public class XUIGateway {


    private final String deviceId;
    private String account;

    private String name;

    private String factory;

    private String subsyscode;

    private String placename;


    private String placeId;

    public String getDeviceId() {
        return deviceId;
    }

    private String personname;

    private String personId;

    private Integer childsize = 0;

    private String batch;

    private DeviceStatus deviceStatus = DeviceStatus.OFFLINE;

    private String gwieee;

    private Date loginTime;

    private String sessionId;


    private String onlineNode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public XUIGateway(Device device) {
        this.account = device.getBindingaccount();
        this.factory = device.getFactory();
        this.subsyscode = device.getSubsyscode();
        this.placeId = device.getPlaceid();
        this.deviceId = device.getDeviceid();
        if (device.getPlaceid() != null) {
            try {
                Place place = CtIotFactory.getCtIotService().getPlaceById(device.getPlaceid());
                if (place != null) {
                    this.placename = place.getName();
                }
            } catch (JDSException e) {
                e.printStackTrace();
            }
        }


        if (device.getDeviceEndPoints().size() > 0) {
            name = device.getDeviceEndPoints().get(0).getName();

        } else {
            name = device.getSensortype().getName();
        }

        if (device.getAppaccount() != null) {
            try {
                this.personname = OrgManagerFactory.getOrgManager().getPersonByAccount(device.getAppaccount()).getName();
            } catch (PersonNotFoundException e) {
                e.printStackTrace();
            }
        }

        this.batch = device.getBatch();
        this.gwieee = device.getSerialno();
        //deviceStatus = device.getStates();
        this.childsize = device.getChildDevices().size();
        this.subsyscode = device.getSubsyscode();

        try {
            Person person = OrgManagerFactory.getOrgManager().getPersonByAccount(account);
            if (person != null) {
                ConnectInfo con = new ConnectInfo(person.getID(), person.getAccount(), person.getPassword());
                Set<JDSSessionHandle> handles = JDSServer.getInstance().getSessionHandleList(con);
                if (handles.size() > 0) {
                    for (JDSSessionHandle handle : handles) {
                        String sysCode = JDSServer.getSessionhandleSystemCodeCache().get(handle);
                        if (sysCode != null) {
                            loginTime = new Date(JDSServer.getInstance().getConnectTimeCache().get(handle));
                            deviceStatus = DeviceStatus.ONLINE;
                            onlineNode = sysCode;
                            sessionId = handle.getSessionID();
                        }
                    }
                } else {

                    if (deviceStatus.equals(DeviceStatus.ONLINE)) {
                        deviceStatus = DeviceStatus.OFFLINE;
                    }

                }
            } else {
                deviceStatus = DeviceStatus.DELETE;
            }


        } catch (JDSException e) {
            e.printStackTrace();
        } catch (PersonNotFoundException e) {
            e.printStackTrace();
        }

    }

    public String getPlacename() {
        return placename;
    }

    public void setPlacename(String placename) {
        this.placename = placename;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getPersonname() {
        return personname;
    }

    public void setPersonname(String personname) {
        this.personname = personname;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public Integer getChildsize() {
        return childsize;
    }

    public void setChildsize(Integer childsize) {
        this.childsize = childsize;
    }

    public DeviceStatus getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(DeviceStatus deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public String getOnlineNode() {
        return onlineNode;
    }

    public void setOnlineNode(String onlineNode) {
        this.onlineNode = onlineNode;
    }


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public String getSubsyscode() {
        return subsyscode;
    }

    public void setSubsyscode(String subsyscode) {
        this.subsyscode = subsyscode;
    }


    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }


    public String getGwieee() {
        return gwieee;
    }

    public void setGwieee(String ieee) {
        this.gwieee = gwieee;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }


}
