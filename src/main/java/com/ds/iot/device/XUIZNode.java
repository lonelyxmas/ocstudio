package com.ds.iot.device;

import com.ds.common.JDSException;
import com.ds.context.JDSActionContext;
import com.ds.iot.*;
import com.ds.iot.ct.CtIotFactory;
import com.ds.iot.enums.DeviceDataTypeKey;
import com.ds.iot.enums.DeviceStatus;
import com.ds.iot.json.SensorInfo;

import java.util.Map;


public class XUIZNode {


    String znodeId;

    String ieee;

    String alias;

    String personName;

    String placename;

    String gatewayid;

    String gatewayname;

    String typename;

    String areaname;

    DeviceStatus status;

    String htmlValue;

    String icon;

    String color;



    public XUIZNode(ZNode znode) {
        SensorInfo sensorInfo=new  SensorInfo(znode);
        DeviceEndPoint endPoint= null;
        try {
            endPoint = CtIotFactory.getCtIotService().getEndPointByIeee(sensorInfo.getSerialno());
        } catch (JDSException e) {
            e.printStackTrace();
        }

        Map<DeviceDataTypeKey,String> value = endPoint.getCurrvalue();
        JDSActionContext.getActionContext().getContext().put("value", value);
        Device device = endPoint.getDevice();
        this.alias = sensorInfo.getAlias();
        this.status = endPoint.getDevice().getStates();
        this.ieee = endPoint.getIeeeaddress();
        Sensortype sensortype = endPoint.getSensortype();
        this.typename = sensortype.getName();
        this.gatewayid=sensorInfo.getGatewayid();
        this.gatewayname=sensorInfo.getGatewayname();
        this.znodeId=sensorInfo.getId();


        Place place = null;

        if (place != null) {
            this.placename = place.getName();

        }

        try {
           if (endPoint.getDevice().getAreaid()!=null){
               Area area  = CtIotFactory.getCtIotService().getAreaById(endPoint.getDevice().getAreaid());
               this.areaname = area.getName();
            }

        } catch (JDSException e) {
            e.printStackTrace();
        }


        this.htmlValue = sensortype.getHtmltemp();
        if (endPoint.getCurrvalue().get("imgUrl") == null) {
            this.icon = sensortype.getIcontemp();
        } else {
            this.icon = endPoint.getCurrvalue().get("imgUrl").toString();
        }

        this.color = sensortype.getColor();
          if (value.get(DeviceDataTypeKey.Status)!= null && !value.get(DeviceDataTypeKey.Status).toString().equals("0") && !value.get(DeviceDataTypeKey.Status).toString().equals("1")) {
            value.put(DeviceDataTypeKey.Status, this.getStatus().getType());

        }

        JDSActionContext.getActionContext().getContext().remove("value");
    }
    public String getZnodeId() {
        return znodeId;
    }

    public void setZnodeId(String znodeId) {
        this.znodeId = znodeId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getGatewayid() {
        return gatewayid;
    }

    public void setGatewayid(String gatewayid) {
        this.gatewayid = gatewayid;
    }

    public String getGatewayname() {
        return gatewayname;
    }

    public void setGatewayname(String gatewayname) {
        this.gatewayname = gatewayname;
    }


    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }


    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }


    public String getAreaname() {
        return areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }


    public String getPlacename() {
        return placename;
    }

    public void setPlacename(String placename) {
        this.placename = placename;
    }



    public String getHtmlValue() {
        return htmlValue;
    }

    public void setHtmlValue(String htmlValue) {
        this.htmlValue = htmlValue;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(DeviceStatus status) {
        this.status = status;
    }

    public String getIeee() {
        return ieee;
    }

    public void setIeee(String ieee) {
        this.ieee = ieee;
    }



}
