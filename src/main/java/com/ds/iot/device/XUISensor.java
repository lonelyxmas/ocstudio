package com.ds.iot.device;

import com.alibaba.fastjson.JSONObject;
import com.ds.common.JDSException;
import com.ds.context.JDSActionContext;
import com.ds.iot.*;
import com.ds.iot.ct.CtIotFactory;
import com.ds.iot.enums.DeviceDataTypeKey;
import com.ds.iot.enums.DeviceStatus;

import java.util.Map;


public class XUISensor {


    String ieee;

    String alias;

    String factory;

    String lqi;

    String batch;

    String battery;

    String typename;

    String areaname;

    DeviceStatus status;

    String placename;

    String placeId;

    String areaId;

    String currValue;

    String htmlValue;

    String icon;

    String color;


    private String hadeviceid;// HA属性

    private String nwkAddress;// 短地址

    private String ep;//

    private String profileid;


    public XUISensor(DeviceEndPoint endPoint) {

        Map<DeviceDataTypeKey, String> value = endPoint.getCurrvalue();
        JDSActionContext.getActionContext().getContext().put("value", value);
        this.hadeviceid = endPoint.getHadeviceid();
        this.nwkAddress = endPoint.getNwkAddress();
        this.ep = endPoint.getEp();
        this.profileid = endPoint.getProfileid();
        this.placeId = endPoint.getDevice().getRootDevice().getPlaceid();


        Device device = endPoint.getDevice();

        this.alias = endPoint.getName();
        this.areaId=endPoint.getDevice().getAreaid();
        this.status = endPoint.getDevice().getStates();
        this.ieee = endPoint.getIeeeaddress();

        this.factory = device.getFactory();
        this.batch = device.getBatch();
        Sensortype sensortype = endPoint.getSensortype();
        this.typename = sensortype.getName();

        this.battery = device.getBattery();//value.get(DeviceDataTypeKey.battery);


        // 门锁处理简化业务

        Place place = null;

        if (place != null) {
            this.placename = place.getName();

        }

        try {
            if (endPoint.getDevice().getAreaid() != null) {
                Area area = CtIotFactory.getCtIotService().getAreaById(endPoint.getDevice().getAreaid());
                if (area != null) {
                    this.areaname = area.getName();
                }

            }

        } catch (JDSException e) {
            e.printStackTrace();
        }


        this.htmlValue = sensortype.getHtmltemp();
        if (endPoint.getCurrvalue().get("imgUrl") == null) {
            this.icon = sensortype.getIcontemp();
        } else {
            this.icon = endPoint.getCurrvalue().get("imgUrl").toString();
        }

        this.color = sensortype.getColor();
        lqi = (value.get(DeviceDataTypeKey.lqi)==null? "0": value.get(DeviceDataTypeKey.lqi));

        if (value.get(DeviceDataTypeKey.Status) != null && !value.get(DeviceDataTypeKey.Status).toString().equals("0") && !value.get(DeviceDataTypeKey.Status).toString().equals("1")) {
            value.put(DeviceDataTypeKey.Status, this.getStatus().getType());
        }

        this.currValue = JSONObject.toJSONString(value);

        JDSActionContext.getActionContext().getContext().remove("value");
    }


    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getHadeviceid() {
        return hadeviceid;
    }

    public void setHadeviceid(String hadeviceid) {
        this.hadeviceid = hadeviceid;
    }

    public String getNwkAddress() {
        return nwkAddress;
    }

    public void setNwkAddress(String nwkAddress) {
        this.nwkAddress = nwkAddress;
    }

    public String getEp() {
        return ep;
    }

    public void setEp(String ep) {
        this.ep = ep;
    }

    public String getProfileid() {
        return profileid;
    }

    public void setProfileid(String profileid) {
        this.profileid = profileid;
    }

    public String getCurrValue() {
        return currValue;
    }

    public void setCurrValue(String currValue) {
        this.currValue = currValue;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }


    public String getAreaname() {
        return areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }


    public String getPlacename() {
        return placename;
    }

    public void setPlacename(String placename) {
        this.placename = placename;
    }


    public String getHtmlValue() {
        return htmlValue;
    }

    public void setHtmlValue(String htmlValue) {
        this.htmlValue = htmlValue;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(DeviceStatus status) {
        this.status = status;
    }

    public String getIeee() {
        return ieee;
    }

    public void setIeee(String ieee) {
        this.ieee = ieee;
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public String getLqi() {
        return lqi;
    }

    public void setLqi(String lqi) {
        this.lqi = lqi;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }


}
