package com.ds.iot.dic;

import com.ds.iot.DeviceEndPoint;
import com.ds.iot.Sensortype;


public class XUIIcon {
    String id;
    String caption;
    String imageClass;
    String iconFontSize;
    String itemWidth;
    String iconStyle;
    String itemHeight;
    boolean capDisplay=true;


    public XUIIcon(DeviceEndPoint endPoint){
        this.id=endPoint.getDeviceId();
        this.caption=endPoint.getName()==null?endPoint.getIeeeaddress():endPoint.getName()+"("+endPoint.getIeeeaddress()+")";
        this.imageClass="iconfont iconfont-8x icon"+endPoint.getSensortype().getIcon();
    }

    public XUIIcon(Sensortype sensortype){
        this.id=sensortype.getType().toString();
        this.caption=sensortype.getName();
        this.imageClass=sensortype.getIcontemp();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getIconFontSize() {
        return iconFontSize;
    }

    public void setIconFontSize(String iconFontSize) {
        this.iconFontSize = iconFontSize;
    }

    public String getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(String itemWidth) {
        this.itemWidth = itemWidth;
    }

    public String getIconStyle() {
        return iconStyle;
    }

    public void setIconStyle(String iconStyle) {
        this.iconStyle = iconStyle;
    }

    public String getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(String itemHeight) {
        this.itemHeight = itemHeight;
    }

    public boolean isCapDisplay() {
        return capDisplay;
    }

    public void setCapDisplay(boolean capDisplay) {
        this.capDisplay = capDisplay;
    }
    //


}
