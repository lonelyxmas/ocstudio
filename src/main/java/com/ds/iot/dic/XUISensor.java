package com.ds.iot.dic;

import com.alibaba.fastjson.annotation.JSONField;
import com.ds.iot.DeviceEndPoint;
import com.ds.iot.enums.DeviceDataTypeKey;

import java.util.HashMap;
import java.util.Map;


public class XUISensor {
    String id;
    @JSONField(name = "caption_")
    String caption;
    String imageClass;
    String iconFontSize;
    String itemWidth;
    String iconStyle;


    String itemHeight;

    public Map<DeviceDataTypeKey, String> getTag() {
        return tag;
    }

    public void setTag(Map<DeviceDataTypeKey, String> tag) {
        this.tag = tag;
    }

    String value;
    boolean capDisplay = true;

    Map<DeviceDataTypeKey, String> tag = new HashMap<DeviceDataTypeKey, String>();


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public XUISensor(DeviceEndPoint endPoint) {
        this.id = endPoint.getIeeeaddress();
        this.value = endPoint.getIeeeaddress();
        tag = endPoint.getCurrvalue();
        tag.put(DeviceDataTypeKey.battery, endPoint.getDevice().getBattery());
        this.caption = endPoint.getIeeeaddress();
        // this.caption= this.caption=endPoint.getName()==null?endPoint.getIeeeaddress():endPoint.getName()+"("+endPoint.getIeeeaddress()+")";
        //  this.imageClass="iconfont iconfont-8x icon"+endPoint.getSensortype().getIcon();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getIconFontSize() {
        return iconFontSize;
    }

    public void setIconFontSize(String iconFontSize) {
        this.iconFontSize = iconFontSize;
    }

    public String getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(String itemWidth) {
        this.itemWidth = itemWidth;
    }

    public String getIconStyle() {
        return iconStyle;
    }

    public void setIconStyle(String iconStyle) {
        this.iconStyle = iconStyle;
    }

    public String getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(String itemHeight) {
        this.itemHeight = itemHeight;
    }

    public boolean isCapDisplay() {
        return capDisplay;
    }

    public void setCapDisplay(boolean capDisplay) {
        this.capDisplay = capDisplay;
    }
    //


}
