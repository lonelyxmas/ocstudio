package com.ds.iot.dic;

import com.alibaba.fastjson.JSONObject;
import com.ds.command.*;
import com.ds.enums.CommandEnums;

import java.util.ArrayList;
import java.util.List;

public class XUICmdTemp {

    String commandStr;

    String name;

    String type;

    String id;

    List<XUICmdTemp> sub;


    public XUICmdTemp(CommandEnums commandEnums, String gwieee) {
        this(commandEnums, gwieee, null);
    }


    public XUICmdTemp(CommandEnums commandEnums, String gwieee, OperationCommandTypeEnum operation) {
        this.id = commandEnums.getType();
        List<String> sensorid = new ArrayList<>();
        sensorid.add("00137000000000001");
        sensorid.add("00137000000000002");

        List<OperatorCommand> cmds = new ArrayList<OperatorCommand>();
        OperatorCommand cmd = new OperatorCommand();
        cmd.setSensorieee(sensorid.get(0));
        cmd.setOperation(OperationCommandParamEnums.off);
        cmds.add(cmd);

       // SensorReportCommand sensorReportCommand = new SensorReportCommand();
        //cmds.add(sensorReportCommand);


        List<OperatorCommand> gropcmds = new ArrayList<OperatorCommand>();

        MoveToLevelCommand levelCommand = new MoveToLevelCommand(OperationCommandTypeEnum.group);
        levelCommand.setValue("80");
        levelCommand.setGroupname("newlight");
        levelCommand.setGroupid("1B");

        OperatorCommand operatorCommand = new OperatorCommand(OperationCommandTypeEnum.group);
        operatorCommand.setOperation(OperationCommandParamEnums.on);
        gropcmds.add(levelCommand);
        gropcmds.add(operatorCommand);

        List<OperatorCommand> scenecmds = new ArrayList<OperatorCommand>();

        OperatorCommand sceneCommand = new OperatorCommand(OperationCommandTypeEnum.scene);
        sceneCommand.setSceneid("1C");
        sceneCommand.setScenename("newscene");
        operatorCommand.setOperation(OperationCommandParamEnums.off);
        scenecmds.add(sceneCommand);


        this.name = commandEnums.getType() + "(" + commandEnums.getName() + ")";
        try {

            Command command  = commandEnums.getCommand().newInstance();
            if (command instanceof SensorCommand) {
                ((SensorCommand) command).setSensorieee("00137000000000001");
            }

            if (command instanceof GroupCommand) {
                ((GroupCommand) command).setGroupid("0A");
                ((GroupCommand) command).setGroupname("light");

            }

            if (command instanceof SceneCommand) {
                ((SceneCommand) command).setSceneid("0A");
                ((SceneCommand) command).setScenename("openall");
            }

            if (command instanceof CreateGroupCommand) {
                ((CreateGroupCommand) command).setSensorieees(sensorid);
            }

            if (command instanceof CreateSceneCommand) {

                ((CreateSceneCommand) command).setCmds(cmds);
                ((CreateSceneCommand) command).setGroupCmds(gropcmds);
                ((CreateSceneCommand) command).setSceneCmds(scenecmds);

            }


            command.setGatewayieee(gwieee);
            this.commandStr = JSONObject.toJSONString(command);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public String getCommandStr() {
        return commandStr;
    }

    public void setCommandStr(String commandStr) {
        this.commandStr = commandStr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<XUICmdTemp> getSub() {
        return sub;
    }

    public void setSub(List<XUICmdTemp> sub) {
        this.sub = sub;
    }

}
