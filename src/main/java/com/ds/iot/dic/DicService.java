package com.ds.iot.dic;

import com.ds.command.*;
import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.engine.ConnectInfo;
import com.ds.enums.CommandEnums;
import com.ds.iot.Device;
import com.ds.iot.DeviceEndPoint;
import com.ds.iot.Sensortype;
import com.ds.iot.ct.CtIotFactory;
import com.ds.server.JDSServer;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/iot/dic")
public class DicService {

    @RequestMapping(value = {"/getDicSensorType"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIIcon>> getAllSensorType() {
        ListResultModel<List<XUIIcon>> userStatusInfo = new ListResultModel<List<XUIIcon>>();
        try {
            List<Sensortype> sensortypes = CtIotFactory.getCtIotService().getAllSensorTypes();
            userStatusInfo = PageUtil.getDefaultPageList(sensortypes, XUIIcon.class);
        } catch (
                JDSException e)

        {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }
        return userStatusInfo;
    }


    @RequestMapping(value = {"/getAllLockSensor"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUISensor>> getAllLockSensor(String gwieee) {
        ListResultModel<List<XUISensor>> userStatusInfo = new ListResultModel<List<XUISensor>>();
        try {
            List<Device> devices = CtIotFactory.getCtIotService().getDeviceByIeee(gwieee).getChildDevices();
            List<DeviceEndPoint> deviceEndPoints = new ArrayList<DeviceEndPoint>();
            for (Device device : devices) {
                List<DeviceEndPoint> endPoints = device.getDeviceEndPoints();
                for (DeviceEndPoint deviceEndPoint : endPoints) {
                    if (deviceEndPoint.getSensortype().getType() == 30) {
                        deviceEndPoints.add(deviceEndPoint);
                    }
                }
            }
            userStatusInfo = PageUtil.getDefaultPageList(deviceEndPoints, XUISensor.class);
        } catch (
                JDSException e)

        {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }
        return userStatusInfo;
    }


    @RequestMapping(value = {"/getAllOnLineGateway"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIIcon>> getAllOnLineGateway() {
        ListResultModel<List<XUIIcon>> userStatusInfo = new ListResultModel<List<XUIIcon>>();
        try {
            List<XUIIcon> modules = new ArrayList<XUIIcon>();
            List<DeviceEndPoint> deviceEndPoints = new ArrayList<DeviceEndPoint>();
            List<ConnectInfo> sessionList = JDSServer.getInstance().getAllConnectInfo();
            for (ConnectInfo connectInfo : sessionList) {
                if (connectInfo == null) {
                    continue;
                }
                if (connectInfo.getLoginName().length() > 35) {
                    Device device = CtIotFactory.getCtIotService().getDeviceById(connectInfo.getLoginName());
                    if (device != null) {
                        List<DeviceEndPoint> endpoints = device.getDeviceEndPoints();
                        for (DeviceEndPoint endpoint : endpoints) {
                            XUIIcon module = new XUIIcon(endpoint);
                            modules.add(module);
                        }
                    }
                }

            }
            userStatusInfo.setSize(sessionList.size());
            userStatusInfo.setData(modules);
        } catch (
                JDSException e)

        {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }


        return userStatusInfo;
    }


    @RequestMapping(value = {"/getCommandTmp"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUICmdTemp>> getCommandTmp(String gwieee, String filter) {

        ListResultModel<List<XUICmdTemp>> userStatusInfo = new ListResultModel<List<XUICmdTemp>>();
        List<XUICmdTemp> cmdTemps = new ArrayList<XUICmdTemp>();
        CommandEnums[] commandEnums = CommandEnums.values();
        for (CommandEnums commandEnum : commandEnums) {

            if (filter!=null){
                if (filter.equals("gateway")) {
                    if (!SensorCommand.class.isAssignableFrom(commandEnum.getCommand())
                            && !NetCommand.class.isAssignableFrom(commandEnum.getCommand())
                            && !ADCommand.class.isAssignableFrom(commandEnum.getCommand())){
                        XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee);
                        cmdTemps.add(cmdTemp);
                    }

                } else if (filter.equals("lock")) {
                    if (PasswordCommand.class.isAssignableFrom(commandEnum.getCommand())) {
                        XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee);
                        cmdTemps.add(cmdTemp);
                    }
                } else if (filter.equals("network")) {
                    if (NetCommand.class.isAssignableFrom(commandEnum.getCommand())
                        && !ADCommand.class.isAssignableFrom(commandEnum.getCommand())){
                        XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee);
                        cmdTemps.add(cmdTemp);
                    }
                } else if (filter.equals("ad")) {
                    if (ADCommand.class.isAssignableFrom(commandEnum.getCommand())) {
                        XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee);
                        cmdTemps.add(cmdTemp);
                    }
                } else if (filter.equals("group")) {
                    if (GroupCommand.class.isAssignableFrom(commandEnum.getCommand())) {
                        XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee,OperationCommandTypeEnum.group);
                        cmdTemps.add(cmdTemp);
                    }
                } else if (filter.equals("scene")) {
                    if (SceneCommand.class.isAssignableFrom(commandEnum.getCommand())) {
                        XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee, OperationCommandTypeEnum.scene);
                        cmdTemps.add(cmdTemp);
                    }
                } else if (filter.equals("sensor")) {
                    if (SensorCommand.class.isAssignableFrom(commandEnum.getCommand())
                            && !ADCommand.class.isAssignableFrom(commandEnum.getCommand())
                            && !NetCommand.class.isAssignableFrom(commandEnum.getCommand())
                            && !PasswordCommand.class.isAssignableFrom(commandEnum.getCommand())) {
                        XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee);
                        cmdTemps.add(cmdTemp);
                    }
                }else {
                    XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee);
                    cmdTemps.add(cmdTemp);
                }
            }else {
                XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee, OperationCommandTypeEnum.group);
                cmdTemps.add(cmdTemp);
            }
        }


        userStatusInfo.setData(cmdTemps);
        return userStatusInfo;
    }

    @RequestMapping(value = {"/getPassWordTmp"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUICmdTemp>> getPassWordTmp(String gwieee) {

        ListResultModel<List<XUICmdTemp>> userStatusInfo = new ListResultModel<List<XUICmdTemp>>();
        List<XUICmdTemp> cmdTemps = new ArrayList<XUICmdTemp>();
        CommandEnums[] commandEnums = CommandEnums.values();
        for (CommandEnums commandEnum : commandEnums) {
            if (PasswordCommand.class.isAssignableFrom(commandEnum.getCommand())) {
                XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee, OperationCommandTypeEnum.group);
                cmdTemps.add(cmdTemp);
            }

        }
        userStatusInfo.setData(cmdTemps);
        return userStatusInfo;
    }


    @RequestMapping(value = {"/getCommandSensorTmp"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUICmdTemp>> getCommandSensorTmp(String gwieee) {

        ListResultModel<List<XUICmdTemp>> userStatusInfo = new ListResultModel<List<XUICmdTemp>>();
        List<XUICmdTemp> cmdTemps = new ArrayList<XUICmdTemp>();
        CommandEnums[] commandEnums = CommandEnums.values();
        for (CommandEnums commandEnum : commandEnums) {
            if (SensorCommand.class.isAssignableFrom(commandEnum.getCommand())
                    && !PasswordCommand.class.isAssignableFrom(commandEnum.getCommand())) {
                XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee, OperationCommandTypeEnum.group);
                cmdTemps.add(cmdTemp);
            }
        }
        userStatusInfo.setData(cmdTemps);
        return userStatusInfo;
    }


    @RequestMapping(value = {"/getGatewaySensorTmp"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUICmdTemp>> getGatewaySensorTmp(String gwieee) {

        ListResultModel<List<XUICmdTemp>> userStatusInfo = new ListResultModel<List<XUICmdTemp>>();
        List<XUICmdTemp> cmdTemps = new ArrayList<XUICmdTemp>();
        CommandEnums[] commandEnums = CommandEnums.values();
        for (CommandEnums commandEnum : commandEnums) {
            if (!SensorCommand.class.isAssignableFrom(commandEnum.getCommand())) {
                XUICmdTemp cmdTemp = new XUICmdTemp(commandEnum, gwieee, OperationCommandTypeEnum.group);
                cmdTemps.add(cmdTemp);
            }
        }
        userStatusInfo.setData(cmdTemps);
        return userStatusInfo;
    }


    @RequestMapping(value = {"/getAllSensors"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIIcon>> getAllSensors(String gwieee) {
        List<DeviceEndPoint> deviceEndPoints = new ArrayList<DeviceEndPoint>();
        ListResultModel<List<XUIIcon>> userStatusInfo = new ListResultModel<List<XUIIcon>>();
        List<XUIIcon> modules = new ArrayList<XUIIcon>();
        Device device = null;
        try {
            device = CtIotFactory.getCtIotService().getDeviceByIeee(gwieee);
            List<Device> childdevices = device.getChildDevices();
            for (Device childdevice : childdevices) {
                List<DeviceEndPoint> endpoints = childdevice.getDeviceEndPoints();
                for (DeviceEndPoint endpoint : endpoints) {
                    XUIIcon module = new XUIIcon(endpoint);
                    modules.add(module);
                }
            }
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }
        userStatusInfo.setSize(modules.size());
        userStatusInfo.setData(modules);
        return userStatusInfo;
    }


}
