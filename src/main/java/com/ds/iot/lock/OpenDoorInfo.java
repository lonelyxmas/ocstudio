package iot.lock;

import com.alibaba.fastjson.JSONObject;
import com.ds.common.util.DateUtility;
import com.ds.iot.AppLockPassword;
import com.ds.msg.LogMsg;

import java.util.Date;

public class OpenDoorInfo {
    String msgId;

    String event;

    String phone;

    String passId;

    String sensorieee;

    String gwieee;

    String type;

    String time;


    public OpenDoorInfo(LogMsg msg) {
        AppLockPassword password = JSONObject.parseObject(msg.getBody(), AppLockPassword.class);
        this.msgId = msg.getId();
        this.event = msg.getTitle();

        if (password.getPassId() > 850) {
            this.type = "离线密码";
        } else {
            this.type = "在线密码";
        }
        this.passId = password.getPassId().toString();

        this.sensorieee = msg.getSensorId();
        this.gwieee = msg.getGatewayId();

        this.phone = password.getPhone();
        this.time = DateUtility.formatDate(new Date(msg.getEventTime() == null ? msg.getReceiveTime() : msg.getEventTime()), "yyyy-MM-dd HH:mm:ss");

    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassId() {
        return passId;
    }

    public void setPassId(String passId) {
        this.passId = passId;
    }

    public String getSensorieee() {
        return sensorieee;
    }

    public void setSensorieee(String sensorieee) {
        this.sensorieee = sensorieee;
    }

    public String getGwieee() {
        return gwieee;
    }

    public void setGwieee(String gwieee) {
        this.gwieee = gwieee;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
