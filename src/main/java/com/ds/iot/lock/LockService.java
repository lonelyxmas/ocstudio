package com.ds.iot.lock;

import com.ds.common.query.Condition;
import com.ds.common.query.JLuceneIndex;
import com.ds.common.query.JoinOperator;
import com.ds.common.query.Operator;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ListResultModel;
import com.ds.iot.log.XUISensorLog;
import com.ds.msg.LogMsg;
import com.ds.msg.MsgClient;
import com.ds.msg.MsgFactroy;
import com.ds.org.query.MsgConditionKey;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/iot/lock")
public class LockService {


    @RequestMapping(value = {"/getOpenDoorLog"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<iot.lock.OpenDoorInfo>> getOpenDoorLog(String ieee, String phone) {
        ListResultModel<List<iot.lock.OpenDoorInfo>> userStatusInfo = new ListResultModel<List<iot.lock.OpenDoorInfo>>();
        try {
            List<XUISensorLog> msgs = new ArrayList<XUISensorLog>();
            Condition condition = new Condition(MsgConditionKey.MSG_TYPE, Operator.EQUALS, "LOG");
            if (phone != null && !phone.equals("")) {
                Condition<MsgConditionKey, JLuceneIndex> gwcondition = new Condition(MsgConditionKey.MSG_BODY, Operator.LIKE, "%" + phone + "%");
                condition.addCondition(gwcondition, JoinOperator.JOIN_AND);
            }
            Condition<MsgConditionKey, JLuceneIndex> sensorcondition = new Condition(MsgConditionKey.MSG_ACTIVITYINSTID, Operator.EQUALS, ieee);
            condition.addCondition(sensorcondition, JoinOperator.JOIN_AND);

            List<String> events = new ArrayList<String>();
            events.add("OpenDoor");
            //vents.add("AddPassword");

            Condition<MsgConditionKey, JLuceneIndex> eventcondition = new Condition(MsgConditionKey.MSG_EVENT, Operator.IN, events);
            condition.addCondition(eventcondition, JoinOperator.JOIN_AND);
            MsgClient<LogMsg> client = MsgFactroy.getInstance().getClient(null, LogMsg.class);
            ListResultModel<List<LogMsg>> commandMsgs = client.getMsgList(condition);
            userStatusInfo = PageUtil.changPageList(commandMsgs, iot.lock.OpenDoorInfo.class);

        } catch (Exception e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }
        return userStatusInfo;


    }

    @RequestMapping(value = {"/getLockPasswordHistory"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<LockPasswordHistory>> getLockPasswordHistory(String ieee, String phone) {
        ListResultModel<List<LockPasswordHistory>> userStatusInfo = new ListResultModel<List<LockPasswordHistory>>();
        try {
            List<XUISensorLog> msgs = new ArrayList<XUISensorLog>();
            Condition condition = new Condition(MsgConditionKey.MSG_TYPE, Operator.EQUALS, "LOG");
            if (phone != null && !phone.equals("")) {
                Condition<MsgConditionKey, JLuceneIndex> gwcondition = new Condition(MsgConditionKey.MSG_BODY, Operator.LIKE, "%" + phone + "%");
                condition.addCondition(gwcondition, JoinOperator.JOIN_AND);
            }
            Condition<MsgConditionKey, JLuceneIndex> sensorcondition = new Condition(MsgConditionKey.MSG_ACTIVITYINSTID, Operator.EQUALS, ieee);
            condition.addCondition(sensorcondition, JoinOperator.JOIN_AND);
            List<String> events = new ArrayList<String>();
            events.add("AddPassword");
            events.add("AddPYPassword");
            events.add("getOfflinePwd");
            Condition<MsgConditionKey, JLuceneIndex> eventcondition = new Condition(MsgConditionKey.MSG_EVENT, Operator.IN, events);
            condition.addCondition(eventcondition, JoinOperator.JOIN_AND);
            MsgClient<LogMsg> client = MsgFactroy.getInstance().getClient(null, LogMsg.class);
            ListResultModel<List<LogMsg>> commandMsgs = client.getMsgList(condition);
            userStatusInfo = PageUtil.changPageList(commandMsgs, LockPasswordHistory.class);
        } catch (Exception e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }
        return userStatusInfo;


    }


}
