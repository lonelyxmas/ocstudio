package com.ds.iot.lock;

import com.alibaba.fastjson.JSONObject;
import com.ds.command.PasswordCommand;
import com.ds.common.util.DateUtility;
import com.ds.enums.CommandEnums;
import com.ds.msg.LogMsg;

import java.util.Date;

public class LockPasswordHistory {
    String msgId;

    String event;

    String phone;

    String sensorieee;

    String gwieee;

    String type;

    String time;

    String seed;

    String passwordType;
    String modeId;
    Integer passId;
    String password;
    String startTime;
    String endTime;


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSeed() {
        return seed;
    }

    public void setSeed(String seed) {
        this.seed = seed;
    }

    public String getPasswordType() {
        return passwordType;
    }

    public void setPasswordType(String passwordType) {
        this.passwordType = passwordType;
    }

    public String getModeId() {
        return modeId;
    }

    public void setModeId(String modeId) {
        this.modeId = modeId;
    }

    public Integer getPassId() {
        return passId;
    }

    public void setPassId(Integer passId) {
        this.passId = passId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public LockPasswordHistory(LogMsg msg) {

        this.msgId = msg.getId();
        this.event = msg.getTitle();
        this.type = msg.getType();
        this.sensorieee = msg.getSensorId();
        this.gwieee = msg.getGatewayId();
        PasswordCommand password = JSONObject.parseObject(msg.getBody(), PasswordCommand.class);
        this.seed=password.getSeed();
        this.modeId=password.getModeId();
        this.phone = password.getPhone();
        this.passId=password.getPassId();
        this.password=password.getPassVal1();
        if (password.getCommand()!=null && password.getCommand().equals(CommandEnums.AddPassword)){
            this.passwordType="在线密码";
           this.startTime = DateUtility.formatDate(new Date(password.getStartTime()*1000), "yyyy-MM-dd HH:mm:ss");
            this.endTime = DateUtility.formatDate(new Date(password.getEndTime()*1000), "yyyy-MM-dd HH:mm:ss");

        }else{
            this.passwordType="离线密码";
            this.startTime =  DateUtility.formatDate(new Date(password.getStartTime()), "yyyy-MM-dd HH:mm:ss");
            this.endTime ="长期";
            this.password="动态获取";

        }



        this.time = DateUtility.formatDate(new Date(msg.getEventTime() == null ? msg.getReceiveTime() : msg.getEventTime()), "yyyy-MM-dd HH:mm:ss");

    }


    public String getSensorieee() {
        return sensorieee;
    }

    public void setSensorieee(String sensorieee) {
        this.sensorieee = sensorieee;
    }

    public String getGwieee() {
        return gwieee;
    }

    public void setGwieee(String gwieee) {
        this.gwieee = gwieee;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
