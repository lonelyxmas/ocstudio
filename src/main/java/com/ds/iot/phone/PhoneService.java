package com.ds.iot.phone;

import com.ds.command.Command;
import com.ds.common.JDSException;
import com.ds.config.ErrorListResultModel;
import com.ds.config.ErrorResultModel;
import com.ds.config.ListResultModel;
import com.ds.config.ResultModel;
import com.ds.home.client.CommandClient;
import com.ds.iot.DeviceEndPoint;
import com.ds.iot.Place;
import com.ds.iot.ZNode;
import com.ds.iot.ct.CtIotFactory;
import com.ds.iot.device.XUIHAParam;
import com.ds.iot.device.XUIZNode;
import com.ds.iot.enums.DeviceDataTypeKey;
import com.ds.iot.enums.ZNodeZType;
import com.ds.iot.home.XUIAppSensor;
import com.ds.web.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/app/phone")
public class PhoneService {

    @RequestMapping(value = {"/getZnodes"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIZNode>> getZnodes(String ieee, String type) {
        ListResultModel<List<XUIZNode>> userStatusInfo = new ListResultModel<List<XUIZNode>>();
        List<ZNode> typeznodes = new ArrayList<ZNode>();
        try {
            if (ieee != null && !ieee.equals("")) {
                DeviceEndPoint deviceEndPoint = CtIotFactory.getCtIotService().getEndPointByIeee(ieee);
                List<ZNode> znodes = deviceEndPoint.getAllZNodes();
                for (ZNode zNode : typeznodes) {
                    if (zNode.getZtype().equals(ZNodeZType.fromType(type))) {
                        typeznodes.add(zNode);
                    }
                }
                userStatusInfo = PageUtil.getDefaultPageList(typeznodes, XUIZNode.class);
            }
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }


        return userStatusInfo;
    }

    @RequestMapping(value = {"/lightCmd"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ResultModel<Command> lightCmd(String id) {
        ResultModel<Command> result = new ResultModel<Command>();
        try {
            ZNode znode = CtIotFactory.getCtIotService().getZNodeById(id);
            CommandClient client = CtIotFactory.getCommandClient(znode.getEndPoint().getDevice().getRootDevice().getSerialno());
            String iotStatus = znode.getEndPoint().getCurrvalue().get(DeviceDataTypeKey.StateOnOff);
            Command command = null;
            if (client != null) {
                if (iotStatus.equals("0")) {
                    command = client.sendOnOutLetCommand(znode.getEndPoint().getIeeeaddress(), true).get();
                } else {
                    command = client.sendOnOutLetCommand(znode.getEndPoint().getIeeeaddress(), false).get();
                }
                result.setData(command);
            } else {
                result = new ErrorResultModel();
                ((ErrorResultModel) result).setErrcode(JDSException.APPLICATIONNOTFOUNDERROR);
                ((ErrorResultModel) result).setErrdes("网关离线！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = new ErrorResultModel();
            ((ErrorResultModel) result).setErrcode(JDSException.APPLICATIONNOTFOUNDERROR);
            ((ErrorResultModel) result).setErrdes(e.getMessage());
        }
        return result;
    }

    @RequestMapping(value = {"/getValue"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ResultModel<Map> getValue(String ieee) {
        ResultModel<Map> userStatusInfo = new ResultModel<Map>();
        try {
            DeviceEndPoint ep = CtIotFactory.getCtIotService().getEndPointByIeee(ieee);
            userStatusInfo.setData(ep.getCurrvalue());
        } catch (JDSException e) {
            userStatusInfo = new ErrorResultModel<Map>();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }

        return userStatusInfo;
    }


    @RequestMapping(value = {"/getHAParams"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIHAParam>> getHAParams(String ieee) {
        ListResultModel<List<XUIHAParam>> userStatusInfo = new ListResultModel<List<XUIHAParam>>();
        try {
            List<XUIHAParam> modules = new ArrayList<XUIHAParam>();
            DeviceEndPoint ep = CtIotFactory.getCtIotService().getEndPointByIeee(ieee);
            Iterator it = ep.getCurrvalue().keySet().iterator();
            while (it.hasNext()) {
                String key = it.next().toString();
                DeviceDataTypeKey dkey = DeviceDataTypeKey.fromType(key);
                XUIHAParam xuihaParam = new XUIHAParam();
                xuihaParam.setDesc(dkey.getName());
                xuihaParam.setKey(dkey.getType());
                Object value = ep.getCurrvalue().get(dkey);
                if (value != null) {
                    xuihaParam.setValue(value.toString());
                } else {
                    xuihaParam.setValue("");
                }
                xuihaParam.setType(dkey.getParamType().getName());
                modules.add(xuihaParam);
            }
            userStatusInfo.setSize(modules.size());
            userStatusInfo.setData(modules);
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel<>();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());
        }


        return userStatusInfo;
    }


    @RequestMapping(value = {"/getDesktopSensor"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIAppSensor>> getDesktopSensor(String placeId) {
        ListResultModel<List<XUIAppSensor>> userStatusInfo = new ListResultModel<List<XUIAppSensor>>();

        try {
            Place place = CtIotFactory.getCtIotService().getPlaceById(placeId);
            List<ZNode> nodes = new ArrayList<ZNode>();
            for (ZNode zNode : place.getGateways()) {
                nodes.addAll(zNode.getChildNodeList());
            }
            // place.getGateways().get(0).getChannel()
            // userStatusInfo = PageUtil.getDefaultPageList( place.getSensors(), XUIAppSensor.class);
            userStatusInfo = PageUtil.getDefaultPageList(nodes, XUIAppSensor.class);
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());


        }

        return userStatusInfo;
    }


    @RequestMapping(value = {"/getLightSensor"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUILightSensor>> getLightSensor(String placeId) {
        ListResultModel<List<XUILightSensor>> userStatusInfo = new ListResultModel<List<XUILightSensor>>();

        try {
            Place place = CtIotFactory.getCtIotService().getPlaceById(placeId);
            List<ZNode> nodes = new ArrayList<ZNode>();


            for (ZNode zNode : place.getGateways()) {
                List<ZNode> znodes = zNode.getChildNodeList();
                for (ZNode cNode : znodes) {
                    if (cNode != null) {
                        ZNodeZType type = cNode.getZtype();
                        if (type.equals(ZNodeZType.Light) || type.equals(ZNodeZType.Level) || type.equals(ZNodeZType.Power) || type.equals(ZNodeZType.IRControl)) {
                            nodes.add(cNode);
                        }
                    }
                }
            }
            userStatusInfo = PageUtil.getDefaultPageList(nodes, XUILightSensor.class);
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());


        }

        return userStatusInfo;
    }

    @RequestMapping(value = {"/getScenes"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIAppSensor>> getScenes(String placeId) {
        ListResultModel<List<XUIAppSensor>> userStatusInfo = new ListResultModel<List<XUIAppSensor>>();

        try {
            Place place = CtIotFactory.getCtIotService().getPlaceById(placeId);
            List<ZNode> nodes = new ArrayList<ZNode>();

            for (ZNode zNode : place.getGateways()) {
                List<ZNode> znodes = zNode.getChildNodeList();
                for (ZNode cNode : znodes) {
                    ZNodeZType type = cNode.getZtype();
                    if (type.equals(ZNodeZType.Scene)) {
                        nodes.add(cNode);
                    }
                }
            }
            // place.getGateways().get(0).getChannel()
            // userStatusInfo = PageUtil.getDefaultPageList( place.getSensors(), XUIAppSensor.class);
            userStatusInfo = PageUtil.getDefaultPageList(nodes, XUIAppSensor.class);
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());


        }

        return userStatusInfo;
    }

    @RequestMapping(value = {"/getSensors"}, method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody
    ListResultModel<List<XUIAppSensor>> getSensors(String placeId) {
        ListResultModel<List<XUIAppSensor>> userStatusInfo = new ListResultModel<List<XUIAppSensor>>();

        try {
            Place place = CtIotFactory.getCtIotService().getPlaceById(placeId);
            List<ZNode> nodes = new ArrayList<ZNode>();

            for (ZNode zNode : place.getGateways()) {
                List<ZNode> znodes = zNode.getChildNodeList();
                for (ZNode cNode : znodes) {
                    if (cNode != null) {
                        ZNodeZType type = cNode.getZtype();
                        if (type.equals(ZNodeZType.Sensor) || type.equals(ZNodeZType.Zone)) {
                            nodes.add(cNode);
                        }
                    }
                }
            }
            userStatusInfo = PageUtil.getDefaultPageList(nodes, XUIAppSensor.class);
        } catch (JDSException e) {
            userStatusInfo = new ErrorListResultModel();
            ((ErrorListResultModel) userStatusInfo).setErrcode(e.getErrorCode());
            ((ErrorListResultModel) userStatusInfo).setErrdes(e.getMessage());


        }

        return userStatusInfo;
    }


}
