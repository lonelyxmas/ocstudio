package com.ds.iot.phone;

import com.ds.context.JDSActionContext;
import com.ds.iot.DeviceEndPoint;
import com.ds.iot.ZNode;
import com.ds.iot.enums.DeviceDataTypeKey;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class XUILightSensor {
    String id;
    String caption;
    String imageClass;
    String iconFontSize;
    String itemWidth;
    String iconStyle;
    String iotStatus = "on";
    String icon;
    String flagClass;


    String itemHeight;
    String url;
    String flagText;


    Map<DeviceDataTypeKey, String> values;
    String comment;

    boolean capDisplay = true;

    public Map getValues() {
        return values;
    }

    public void setValues(Map values) {
        this.values = values;
    }

    public XUILightSensor(ZNode znode) {
        this.id = znode.getZnodeid();
        this.caption = "";
        this.iotStatus = znode.getEndPoint().getCurrvalue().get(DeviceDataTypeKey.StateOnOff);
        if (iotStatus == null) {
            iotStatus = "none";
        } else if (iotStatus.equals("0")) {
            iotStatus = "off";
        } else {
            iotStatus = "on";
        }


        this.values = znode.getEndPoint().getCurrvalue();
        this.url = znode.getEndPoint().getSensortype().getDatalisturl();
        String defaultIcon = znode.getEndPoint().getSensortype().getIcon();
        if (defaultIcon != null && !defaultIcon.equals("")) {
            icon = defaultIcon;
        } else {
            this.imageClass = znode.getEndPoint().getSensortype().getIcontemp();
        }


        String htmltemp = znode.getEndPoint().getSensortype().getHtmltemp();
        if (htmltemp != null || !htmltemp.equals("")) {
            HashMap<String, String> hashValue = new HashMap<String, String>();
            Set<DeviceDataTypeKey> keys = values.keySet();
            for (DeviceDataTypeKey key : keys) {
                hashValue.put(key.toString(), values.get(key));
            }
            JDSActionContext.getActionContext().getContext().put("value", hashValue);
            caption = JDSActionContext.getActionContext().Par(htmltemp, String.class);
            JDSActionContext.getActionContext().getContext().remove("value");
        }


        switch (znode.getStatus()) {
            case ONLINE:
                flagClass = "fa fa-sliders";
                break;
            case OFFLINE:
                this.caption = "";
                flagClass = "fa fa-assistive-listening-systems";
                break;
            case FAULT:
                this.caption = "";
                flagClass = "fa fa-wrench";
                break;
            case DISABLE:
                flagClass = "fa fa-warning";
                break;
            default:
                break;
        }
        comment = znode.getEndPoint().getName();

    }


    public XUILightSensor(DeviceEndPoint endPoint) {
        this.id = endPoint.getIeeeaddress();
        this.caption = endPoint.getDevice().getSerialno();
        String defaultIcon = endPoint.getSensortype().getIcon();
        if (defaultIcon != null && !defaultIcon.equals("")) {
            icon = defaultIcon;
        } else {
            this.imageClass = endPoint.getSensortype().getIcontemp();
        }
        comment = endPoint.getName();
    }

    public String getFlagText() {
        return flagText;
    }

    public void setFlagText(String flagText) {
        this.flagText = flagText;
    }

    public String getFlagClass() {
        return flagClass;
    }

    public void setFlagClass(String flagClass) {
        this.flagClass = flagClass;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getIconFontSize() {
        return iconFontSize;
    }

    public void setIconFontSize(String iconFontSize) {
        this.iconFontSize = iconFontSize;
    }

    public String getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(String itemWidth) {
        this.itemWidth = itemWidth;
    }

    public String getIconStyle() {
        return iconStyle;
    }

    public void setIconStyle(String iconStyle) {
        this.iconStyle = iconStyle;
    }

    public String getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(String itemHeight) {
        this.itemHeight = itemHeight;
    }

    public boolean isCapDisplay() {
        return capDisplay;
    }

    public void setCapDisplay(boolean capDisplay) {
        this.capDisplay = capDisplay;
    }
    //

    public String getIotStatus() {
        return iotStatus;
    }

    public void setIotStatus(String iotStatus) {
        this.iotStatus = iotStatus;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}
