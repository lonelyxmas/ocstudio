package com.ds;

import com.ds.config.UserBean;
import com.ds.editor.ESDEditor;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

public class ESDStudio {

    public static final String PRODUCT_NAME = "ESD EditorTools";

    public static final String PRODUCT_VERSION = "V2.0b";

    public static final String PRODUCT_COPYRIGHT = "Copyright(c)2004 - 2023 itjds.net, All Rights Reserved";


    public static void main(String[] args) throws IOException {
        System.out.println("************************************************");
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                SystemTray tray = SystemTray.getSystemTray();
                tray.remove(ESDEditor.getInstance().getTi());
                ESDEditor.getInstance().closeAllPage();
            }
        }));

        try {
            System.out.println("************************************************");
            System.out.println("----- 欢迎使用 JDS ESD 开发工具");
            System.out.println(PRODUCT_NAME + " - " + PRODUCT_VERSION);
            System.out.println(PRODUCT_COPYRIGHT);
            System.out.println("************************************************");
            System.out.println("-------- ESDTools Initialization ---------");
            System.out.println("************************************************");
            System.out.println("- Start Connect Server - " + UserBean.getInstance().getServerUrl() + "*");
            System.out.println("- Connent JDSServer UserName    [" + UserBean.getInstance().getUsername() + "]*");
            ESDEditor.getInstance().login(false, UserBean.getInstance());


        } catch (Exception e1) {
            e1.printStackTrace();

        }


    }

    public static int main(String[] args, InputStream in, PrintStream out) {
        try {
            main(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
