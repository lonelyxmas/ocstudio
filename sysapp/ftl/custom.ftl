<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta name="viewport"
          content="user-scalable=no, initial-scale=0.5, minimum-scale=0.5, maximum-scale=2.0,width=device-width, height=device-height"/>
    <meta http-equiv="no-cache">
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="content-type" content="no-cache, must-revalidate"/>
    <meta http-equiv="expires" content="-1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="/RAD//css/default.css"/>
    <link rel="stylesheet" type="text/css" href="/xui/iconfont/iconfont.css"/>
    <link rel="stylesheet" type="text/css" href="/RAD/css/control-themes.css"/>
    <link rel="stylesheet" type="text/css" href="/plugins/formlayout/handsontable.full.min.css"/>
    <link rel="stylesheet" type="text/css" href="/xui/bpm/bpmfont.css"/>
    <link type="text/css" href="/RAD/release/1.0/fontawesome/font-awesome.min.css" rel="stylesheet">
    <link type="text/css" href="/xui/appearance/webflat/theme.css" rel="stylesheet">
    <#list $CssFills as item>
        <link rel="stylesheet" type="text/css" href="/root/${item.path}"/>
    </#list>

    <script type="text/javascript" src="/RAD/xui.js"></script>
    <script type="text/javascript" src="/xui/Module.js"></script>
    <script type="text/javascript" src="/xui/XML.js"></script>
    <script type="text/javascript" src="/xui/XMLRPC.js"></script>
    <script type="text/javascript" src="/xui/SOAP.js"></script>
    <script type="text/javascript" src="/xui/ModuleFactory.js"></script>
    <script type="text/javascript" src="/xui/Debugger.js"></script>
    <script type="text/javascript" src="/xui/Date.js"></script>
    <script type="text/javascript" src="/xui/UI.js"></script>
    <script type="text/javascript" src="/xui/UI/Image.js"></script>
    <script type="text/javascript" src="/xui/UI/Flash.js"></script>
    <script type="text/javascript" src="/xui/UI/Audio.js"></script>
    <script type="text/javascript" src="/xui/UI/FileUpload.js"></script>
    <script type="text/javascript" src="/xui/UI/Video.js"></script>
    <script type="text/javascript" src="/xui/UI/Resizer.js"></script>
    <script type="text/javascript" src="/xui/UI/Block.js"></script>
    <script type="text/javascript" src="/xui/UI/Label.js"></script>
    <script type="text/javascript" src="/xui/UI/ProgressBar.js"></script>
    <script type="text/javascript" src="/xui/UI/Slider.js"></script>
    <script type="text/javascript" src="/xui/UI/Input.js"></script>
    <script type="text/javascript" src="/xui/UI/CheckBox.js"></script>
    <script type="text/javascript" src="/xui/UI/HiddenInput.js"></script>
    <script type="text/javascript" src="/xui/UI/RichEditor.js"></script>
    <script type="text/javascript" src="/xui/UI/ComboInput.js"></script>
    <script type="text/javascript" src="/xui/UI/ColorPicker.js"></script>
    <script type="text/javascript" src="/xui/UI/DatePicker.js"></script>
    <script type="text/javascript" src="/xui/UI/TimePicker.js"></script>
    <script type="text/javascript" src="/xui/UI/List.js"></script>
    <script type="text/javascript" src="/xui/UI/Gallery.js"></script>
    <script type="text/javascript" src="/xui/UI/Panel.js"></script>
    <script type="text/javascript" src="/xui/UI/Group.js"></script>
    <script type="text/javascript" src="/xui/UI/PageBar.js"></script>
    <script type="text/javascript" src="/xui/UI/Tabs.js"></script>
    <script type="text/javascript" src="/xui/UI/Stacks.js"></script>
    <script type="text/javascript" src="/xui/UI/ButtonViews.js"></script>
    <script type="text/javascript" src="/xui/UI/RadioBox.js"></script>
    <script type="text/javascript" src="/xui/UI/StatusButtons.js"></script>
    <script type="text/javascript" src="/xui/UI/TreeBar.js"></script>
    <script type="text/javascript" src="/xui/UI/TreeView.js"></script>
    <script type="text/javascript" src="/xui/UI/PopMenu.js"></script>
    <script type="text/javascript" src="/xui/UI/MenuBar.js"></script>
    <script type="text/javascript" src="/xui/UI/ToolBar.js"></script>
    <script type="text/javascript" src="/xui/UI/Layout.js"></script>
    <script type="text/javascript" src="/xui/UI/TreeGrid.js"></script>
    <script type="text/javascript" src="/xui/UI/Dialog.js"></script>
    <script type="text/javascript" src="/xui/UI/FormLayout.js"></script>
    <script type="text/javascript" src="/xui/UI/FoldingTabs.js"></script>
    <script type="text/javascript" src="/xui/UI/FoldingList.js"></script>
    <script type="text/javascript" src="/xui/UI/Opinion.js"></script>
    <script type="text/javascript" src="/xui/ThirdParty/raphael.js"></script>
    <script type="text/javascript" src="/xui/svg.js"></script>
    <script type="text/javascript" src="/xui/UI/SVGPaper.js"></script>
    <script type="text/javascript" src="/xui/UI/FusionChartsXT.js"></script>
    <script type="text/javascript" src="/xui/UI/ECharts.js"></script>
    <script type="text/javascript" src="/xui/Coder.js"></script>
    <script type="text/javascript" src="/xui/Module/JSONEditor.js"></script>




    <title>${projectName}调试界面</title>
</head>


<body>
<div id="loading" style="position:fixed;width:100%;text-align:center;">
    <img id="loadingimg" alt="Loading..." title="Loading..." src="/RAD/img/loading.gif"/>
</div>
</body>


<script type="text/javascript">
    if (/#.*touch\=(1|true)/.test(location.href)) {
        window.xui_ini = {fakeTouch: 1};
        document.body.className += " xui-cursor-touch";
    }
</script>

<script type="text/javascript">

    var args = xui.getUrlParams(),
            onEnd = function () {
                this.setData(args);
                xui('loading').remove();
                this.initData();
                this._fireEvent('afterShow');
            };
    xui.launch('${className}', onEnd, 'cn', args && args.theme || 'default');


    $E = xui.execExpression;
    $BPD = {
        open: function () {
            $E('$BPD.open()')
        },
        close: function () {
            $E('$BPD.close()')
        }
    }
    $ESD = {
        export: function (url, params) {
            var paramArr = params | {};
            if (url) {
                paramArr.url = url;
            }
            $E('$ESD.export()', paramArr)
        },
        reload: function (packageName, params) {
            var paramArr = params || {projectName: "${projectName}"};
            if (packageName) {
                paramArr.packageName = packageName;
                paramArr.handleId = window.handleId;
            }
            $E('$ESD.reload()', paramArr)
        },
        open: function (url, params) {
            var paramArr = params | {};
            if (url) {
                paramArr.url = url;
            }
            $E('$ESD.open()', paramArr)
        },
        quit: function () {
            $E('$BPD.quit()')
        },
        logout: function () {
            $E('$BPD.logout()')
        }
    }
</script>
</html>