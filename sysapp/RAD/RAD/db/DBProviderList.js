
xui.Class('RAD.db.DBProviderList', 'xui.Module',{
    Instance:{
        initialize : function(){
        },
        Dependencies:[],
        Required:[],
        properties : {
            "path":"form/myspace/versionspace/projectManager/0/RAD/db/DBProviderList.cls",
            "personId":"devdev",
            "personName":"devdev",
            "projectName":"projectManager"
        },
        events:{},
        functions:{},
        iniComponents : function(){
            // [[Code created by JDSEasy RAD Studio
            var host=this, children=[], properties={}, append=function(child){children.push(child.get(0));};
            xui.merge(properties, this.properties);

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"api_4")
                    .setName("api_4")
                    .setAutoRun(true)
                    .setQueryURL("/admin/getDbConfigList")
                    .setQueryMethod("POST")
                    .setRequestDataSource([
                        {
                            "type":"pagebar",
                            "name":"xui_ui_pagebar8",
                            "path":""
                        }
                    ])
                    .setResponseDataTarget([
                        {
                            "type":"treegrid",
                            "name":"xui_ui_treegrid20",
                            "path":"data"
                        },
                        {
                            "type":"pagebar",
                            "name":"xui_ui_pagebar8",
                            "path":"size"
                        }
                    ])
                    .setResponseCallback([ ])
            );

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"api_17")
                    .setRequestDataSource([
                        {
                            "type":"treegrid",
                            "name":"xui_ui_treegrid20",
                            "path":""
                        }
                    ])
                    .setResponseDataTarget([ ])
                    .setResponseCallback([ ])
                    .setQueryURL("/admin/removeDBConfig")
                    .setQueryMethod("POST")
                    .onData({
                        "actions":[
                            {
                                "args":[
                                    "{page.api_4.invoke()}"
                                ],
                                "desc":"动作 1",
                                "method":"invoke",
                                "redirection":"other:callback:call",
                                "target":"api_4",
                                "type":"control"
                            }
                        ]
                    })
            );

            append(
                xui.create("xui.UI.Dialog")
                    .setHost(host,"xui_ui_dialog8")
                    .setLeft("1.6666666666666667em")
                    .setTop("2.5em")
                    .setWidth("55.833333333333336em")
                    .setHeight("34.166666666666664em")
                    .setCaption("数据源维护")
                    .setImageClass("fa fa-database")
            );

            host.xui_ui_dialog8.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block52")
                    .setDesc("主框架")
                    .setDock("fill")
                    .setLeft("0em")
                    .setTop("0em")
            );

            host.xui_ui_block52.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block53")
                    .setDock("fill")
                    .setLeft("20em")
                    .setTop("20em")
            );

            host.xui_ui_block53.append(
                xui.create("xui.UI.TreeGrid")
                    .setHost(host,"xui_ui_treegrid20")
                    .setName("personlist")
                    .setLeft("0em")
                    .setTop("0em")
                    .setSelMode("multibycheckbox")
                    .setRowNumbered(true)
                    .setHeader([
                        {
                            "caption":"驱动标识",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"configKey",
                            "readonly":true,
                            "type":"label",
                            "width":"8em"
                        },
                        {
                            "caption":"用户名",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"username",
                            "readonly":true,
                            "type":"label",
                            "width":"12em"
                        },
                        {
                            "caption":"数据库驱动",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"driver",
                            "readonly":true,
                            "type":"label",
                            "width":"12em"
                        },
                        {
                            "caption":"最大链接",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":true,
                            "id":"maxConnections",
                            "readonly":true,
                            "type":"label",
                            "width":"8em"
                        },
                        {
                            "caption":"最小链接",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"minConnections",
                            "readonly":true,
                            "type":"label",
                            "width":"8em"
                        },
                        {
                            "caption":"链接串",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":true,
                            "hidden":false,
                            "id":"serverURL",
                            "readonly":true,
                            "type":"label",
                            "width":"12em"
                        }
                    ])
                    .setUidColumn("configKey")
                    .setTagCmds([
                        {
                            "caption":"删除",
                            "hidden":false,
                            "id":"del",
                            "itemClass":"fa fa-minus-square",
                            "location":"right",
                            "pos":"row"
                        }
                    ])
                    .setValue("")
                    .onCmd({
                        "actions":[
                            {
                                "args":[
                                    "{page.api_17.invoke()}"
                                ],
                                "conditions":[
                                    {
                                        "symbol":"=",
                                        "right":"del",
                                        "left":"{args[2]}"
                                    }
                                ],
                                "desc":"动作 1",
                                "koFlag":"_DI_fail",
                                "method":"invoke",
                                "okFlag":"_DI_succeed",
                                "redirection":"other:callback:call",
                                "return":false,
                                "target":"api_17",
                                "type":"control"
                            }
                        ]
                    })
                    .onDblclickRow({
                        "actions":[
                            {
                                "args":[
                                    "{page.show2()}",
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    "{args[1]}",
                                    "{page}"
                                ],
                                "desc":"动作 2",
                                "method":"show2",
                                "redirection":"page",
                                "target":"RAD.db.DBConfig",
                                "type":"page"
                            }
                        ]
                    })
            );

            host.xui_ui_block53.append(
                xui.create("xui.UI.ToolBar")
                    .setHost(host,"xui_ui_toolbar36")
                    .setName("persontoolbar")
                    .setItems([
                        {
                            "caption":"grp1",
                            "hidden":false,
                            "id":"grp1",
                            "sub":[
                                {
                                    "caption":"添加",
                                    "hidden":false,
                                    "iconFontSize":"",
                                    "id":"add",
                                    "imageClass":"fa fa-plus-square",
                                    "position":"absolute"
                                },
                                {
                                    "caption":"删除",
                                    "hidden":true,
                                    "iconFontSize":"",
                                    "id":"delete",
                                    "imageClass":"fa fa-remove",
                                    "position":"absolute"
                                },
                                {
                                    "caption":"刷新",
                                    "hidden":false,
                                    "iconFontSize":"",
                                    "id":"reload",
                                    "imageClass":"fa fa-refresh",
                                    "position":"absolute"
                                }
                            ]
                        }
                    ])
                    .setLeft("Infinityem")
                    .setTop("3.3333333333333335em")
                    .onClick({
                        "actions":[
                            {
                                "args":[
                                    "{page.api_4.invoke()}"
                                ],
                                "conditions":[
                                    {
                                        "symbol":"=",
                                        "right":"reload",
                                        "left":"{args[1].id}"
                                    }
                                ],
                                "desc":"动作 3",
                                "koFlag":"_DI_fail",
                                "method":"invoke",
                                "okFlag":"_DI_succeed",
                                "redirection":"other:callback:call",
                                "return":false,
                                "target":"api_4",
                                "type":"control"
                            },
                            {
                                "args":[
                                    "{page.show2()}",
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    "{page}"
                                ],
                                "conditions":[
                                    {
                                        "symbol":"=",
                                        "right":"add",
                                        "left":"{args[5]}"
                                    }
                                ],
                                "desc":"动作 2",
                                "method":"show2",
                                "redirection":"page",
                                "target":"RAD.db.DBConfig",
                                "type":"page"
                            }
                        ]
                    })
            );

            host.xui_ui_block53.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block54")
                    .setDock("bottom")
                    .setLeft("15em")
                    .setTop("45em")
                    .setHeight("2.8333333333333335em")
            );

            host.xui_ui_block54.append(
                xui.create("xui.UI.PageBar")
                    .setHost(host,"xui_ui_pagebar8")
                    .setName("personpagebar")
                    .setLeft("20.833333333333332em")
                    .setTop("0.8333333333333334em")
                    .setCaption("页码自定义")
            );

            return children;
            // ]]Code created by JDSEasy RAD Studio
        },

        customAppend : function(parent, subId, left, top){
            return false;
        }
    } ,
    Static:{
        "designViewConf":{
            "height":1024,
            "mobileFrame":false,
            "width":1280
        }
    }
});