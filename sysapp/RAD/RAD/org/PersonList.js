
xui.Class('RAD.org.PersonList', 'xui.Module',{
    Instance:{
        initialize : function(){
        },
        Dependencies:[],
        Required:[],
        properties : {
            "path":"form/myspace/versionspace/projectManager/0/RAD/org/PersonList.cls",
            "projectName":"projectManager"
        },
        events:{},
        functions:{},
        iniComponents : function(){
            // [[Code created by JDSEasy RAD Studio
            var host=this, children=[], properties={}, append=function(child){children.push(child.get(0));};
            xui.merge(properties, this.properties);

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"loadperson")
                    .setName("loadperson")
                    .setAutoRun(true)
                    .setQueryURL("/admin/org/getDevPersons")
                    .setQueryMethod("POST")
                    .setRequestDataSource([
                        {
                            "name":"xui_ui_pagebar14",
                            "path":"",
                            "type":"pagebar"
                        },
                        {
                            "name":"xui_ui_block61",
                            "path":"",
                            "type":"form"
                        }
                    ])
                    .setResponseDataTarget([
                        {
                            "name":"xui_ui_treegrid28",
                            "path":"data",
                            "type":"treegrid"
                        },
                        {
                            "name":"xui_ui_pagebar14",
                            "path":"size",
                            "type":"pagebar"
                        }
                    ])
                    .setResponseCallback([ ])
            );

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"removeDevPersons")
                    .setName("removeDevPersons")
                    .setQueryURL("/admin/org/removeDevPersons")
                    .setQueryMethod("POST")
                    .setRequestDataSource([
                        {
                            "name":"xui_ui_treegrid28",
                            "path":"",
                            "type":"treegrid"
                        },
                        {
                            "name":"xui_ui_block61",
                            "path":"",
                            "type":"form"
                        }
                    ])
                    .setResponseDataTarget([ ])
                    .setResponseCallback([ ])
                    .onData({
                        "actions":[
                            {
                                "args":[ ],
                                "desc":"动作 1",
                                "koFlag":"_DI_fail",
                                "method":"invoke",
                                "okFlag":"_DI_succeed",
                                "target":"loadperson",
                                "type":"control"
                            }
                        ]
                    })
            );

            append(
                xui.create("xui.UI.Dialog")
                    .setHost(host,"xui_ui_dialog11")
                    .setLeft("3.3333333333333335em")
                    .setTop("8.333333333333334em")
                    .setWidth("68.33333333333333em")
                    .setHeight("40em")
                    .setCaption("团队权限")
                    .setImageClass("fa fa-address-book")
            );

            host.xui_ui_dialog11.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block61")
                    .setDock("fill")
                    .setLeft("15.833333333333334em")
                    .setTop("6.666666666666667em")
                    .setBorderType("inset")
            );

            host.xui_ui_block61.append(
                xui.create("xui.UI.Layout")
                    .setHost(host,"xui_ui_layout36")
                    .setItems([
                        {
                            "cmd":true,
                            "folded":false,
                            "id":"before",
                            "locked":false,
                            "min":10,
                            "pos":"before",
                            "size":200,
                            "hidden":false
                        },
                        {
                            "id":"main",
                            "min":10,
                            "size":80
                        }
                    ])
                    .setLeft("0em")
                    .setTop("0em")
                    .setType("horizontal")
            );

            host.xui_ui_layout36.append(
                xui.create("xui.UI.TreeView")
                    .setHost(host,"groupTree")
                    .setName("groupTree")
                    .setItems([
                        {
                            "caption":"工作组",
                            "hidden":false,
                            "id":"all",
                            "imageClass":"fa fa-address-book",
                            "iniFold":false,
                            "sub":[
                                {
                                    "caption":"开发组",
                                    "hidden":false,
                                    "id":"dev",
                                    "imageClass":"fa fa-code",
                                    "iniFold":false
                                },
                                {
                                    "caption":"运维组",
                                    "hidden":false,
                                    "id":"ops",
                                    "imageClass":"fa fa-cogs",
                                    "iniFold":false
                                },
                                {
                                    "caption":"测试组",
                                    "hidden":false,
                                    "id":"test",
                                    "imageClass":"fa fa-bug",
                                    "iniFold":false
                                }
                            ]
                        }
                    ])
                    .setLeft("0em")
                    .setTop("0em")
                    .setValue("dev")
                    .onChange({
                        "actions":[
                            {
                                "args":[
                                    "{page.group.setUIValue()}",
                                    null,
                                    null,
                                    "{args[2]}"
                                ],
                                "desc":"reload",
                                "method":"setUIValue",
                                "redirection":"other:callback:call",
                                "target":"group",
                                "type":"control"
                            },
                            {
                                "args":[ ],
                                "desc":"动作 2",
                                "koFlag":"_DI_fail",
                                "method":"invoke",
                                "okFlag":"_DI_succeed",
                                "target":"loadperson",
                                "type":"control"
                            }
                        ]
                    }),
                "before"
            );

            host.xui_ui_layout36.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block63")
                    .setDock("fill")
                    .setLeft("20.833333333333332em")
                    .setTop("20.833333333333332em"),
                "main"
            );

            host.xui_ui_block63.append(
                xui.create("xui.UI.HiddenInput")
                    .setHost(host,"projectName")
                    .setName("projectName")
                    .setValue("")
            );

            host.xui_ui_block63.append(
                xui.create("xui.UI.HiddenInput")
                    .setHost(host,"group")
                    .setName("group")
                    .setValue("dev")
            );

            host.xui_ui_block63.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block64")
                    .setDock("bottom")
                    .setLeft("15em")
                    .setTop("45em")
                    .setHeight("2.8333333333333335em")
            );

            host.xui_ui_block64.append(
                xui.create("xui.UI.PageBar")
                    .setHost(host,"xui_ui_pagebar14")
                    .setName("personpagebar")
                    .setLeft("20.833333333333332em")
                    .setTop("0.8333333333333334em")
                    .setCaption("页码自定义")
            );

            host.xui_ui_block63.append(
                xui.create("xui.UI.ToolBar")
                    .setHost(host,"xui_ui_toolbar45")
                    .setName("persontoolbar")
                    .setItems([
                        {
                            "caption":"grp1",
                            "hidden":false,
                            "id":"grp1",
                            "sub":[
                                {
                                    "caption":"添加",
                                    "hidden":false,
                                    "iconFontSize":"",
                                    "id":"add",
                                    "imageClass":"fa fa-plus-square",
                                    "position":"absolute"
                                },
                                {
                                    "caption":"删除",
                                    "hidden":true,
                                    "iconFontSize":"",
                                    "id":"delete",
                                    "imageClass":"fa fa-remove",
                                    "position":"absolute"
                                },
                                {
                                    "caption":"刷新",
                                    "hidden":false,
                                    "iconFontSize":"",
                                    "id":"reload",
                                    "imageClass":"fa fa-refresh",
                                    "position":"absolute"
                                }
                            ]
                        }
                    ])
                    .setLeft("Infinityem")
                    .setTop("3.3333333333333335em")
                    .onClick({
                        "actions":[
                            {
                                "args":[ ],
                                "conditions":[
                                    {
                                        "symbol":"=",
                                        "right":"reload",
                                        "left":"{args[1].id}"
                                    }
                                ],
                                "desc":"动作 3",
                                "koFlag":"_DI_fail",
                                "method":"invoke",
                                "okFlag":"_DI_succeed",
                                "return":false,
                                "target":"loadperson",
                                "type":"control"
                            },
                            {
                                "args":[
                                    "{page.show2()}",
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    "{page.getData()}",
                                    "{page}"
                                ],
                                "conditions":[
                                    {
                                        "symbol":"=",
                                        "right":"add",
                                        "left":"{args[5]}"
                                    }
                                ],
                                "desc":"动作 2",
                                "method":"show2",
                                "redirection":"page",
                                "target":"RAD.org.PersonTree",
                                "type":"page"
                            }
                        ]
                    })
            );

            host.xui_ui_block63.append(
                xui.create("xui.UI.TreeGrid")
                    .setHost(host,"xui_ui_treegrid28")
                    .setName("personlist")
                    .setLeft("0em")
                    .setTop("0em")
                    .setSelMode("multibycheckbox")
                    .setAltRowsBg(true)
                    .setRowNumbered(true)
                    .setHeader([
                        {
                            "caption":"编号",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":true,
                            "id":"iD",
                            "readonly":true,
                            "type":"label",
                            "width":"8em"
                        },
                        {
                            "caption":"登录用户名",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"account",
                            "readonly":true,
                            "type":"label",
                            "width":"12em"
                        },
                        {
                            "caption":"姓名",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"name",
                            "readonly":true,
                            "type":"label",
                            "width":"12em"
                        },
                        {
                            "caption":"手机号码",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"mobile",
                            "readonly":true,
                            "type":"label",
                            "width":"12em"
                        },
                        {
                            "caption":"邮箱",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":true,
                            "hidden":false,
                            "id":"email",
                            "readonly":true,
                            "type":"label",
                            "width":"8em"
                        },
                        {
                            "caption":"orgId",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":true,
                            "id":"orgId",
                            "readonly":true,
                            "type":"label",
                            "width":"8em"
                        }
                    ])
                    .setUidColumn("iD")
                    .setTagCmds([
                        {
                            "caption":"删除",
                            "hidden":false,
                            "id":"del",
                            "itemClass":"fa fa-minus-square",
                            "location":"right",
                            "pos":"row"
                        }
                    ])
                    .setValue("")
                    .onCmd({
                        "actions":[
                            {
                                "args":[ ],
                                "conditions":[
                                    {
                                        "symbol":"=",
                                        "right":"del",
                                        "left":"{args[2]}"
                                    }
                                ],
                                "desc":"动作 1",
                                "koFlag":"_DI_fail",
                                "method":"invoke",
                                "okFlag":"_DI_succeed",
                                "return":false,
                                "target":"removeDevPersons",
                                "type":"control"
                            }
                        ]
                    })
                    .onDblclickRow([
                        {
                            "args":[
                                "{page.show2()}",
                                null,
                                null,
                                null,
                                null,
                                null,
                                "{args[1]}",
                                "{page}"
                            ],
                            "desc":"动作 2",
                            "method":"show2",
                            "redirection":"page",
                            "target":"RAD.org.PersonInfo",
                            "type":undefined
                        }
                    ])
            );

            return children;
            // ]]Code created by JDSEasy RAD Studio
        },

        customAppend : function(parent, subId, left, top){
            return false;
        }
    } ,
    Static:{
        "designViewConf":{
            "height":1024,
            "mobileFrame":false,
            "width":1280
        }
    }
});