
xui.Class('RAD.server.RemoteServerList', 'xui.Module',{
    Instance:{
        initialize : function(){ },
        Dependencies:[],
        Required:[
            "RAD.server.RemoteServerConfig"
        ],
        properties : {
            "path":"form/myspace/versionspace/projectManager/0/RAD/server/RemoteServerList.cls",
            "projectName":"projectManager"
        },
        events:{},
        functions:{},
        iniComponents : function(){
            // [[Code created by JDSEasy RAD Studio
            var host=this, children=[], properties={}, append=function(child){children.push(child.get(0));};
            xui.merge(properties, this.properties);

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"clearHosts")
                    .setName("clearHosts")
                    .setQueryURL("/admin/proxyhost/clearServers")
                    .setRequestDataSource([
                        {
                            "name":"xui_ui_block29",
                            "path":"",
                            "type":"form"
                        }
                    ])
                    .setResponseDataTarget([ ])
                    .setResponseCallback([ ])
                    .onData([
                        {
                            "args":[
                                "{page.initData()}"
                            ],
                            "desc":"动作 1",
                            "method":"initData",
                            "redirection":"page",
                            "target":"RAD.server.RemoteServerList",
                            "type":"page"
                        }
                    ])
            );

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"api_12")
                    .setName("api_12")
                    .setAutoRun(true)
                    .setQueryURL("/admin/proxyhost/getRemoteServerList")
                    .setRequestDataSource([
                        {
                            "name":"xui_ui_pagebar7",
                            "path":"",
                            "type":"pagebar"
                        }
                    ])
                    .setResponseDataTarget([
                        {
                            "name":"xui_ui_treegrid14",
                            "path":"data",
                            "type":"treegrid"
                        },
                        {
                            "name":"xui_ui_pagebar7",
                            "path":"size",
                            "type":"pagebar"
                        }
                    ])
                    .setResponseCallback([ ])
            );

            append(
                xui.create("xui.APICaller")
                    .setHost(host,"delete")
                    .setName("delete")
                    .setQueryURL("/admin/proxyhost/deleteRemoteServer")
                    .setRequestDataSource([
                        {
                            "name":"xui_ui_treegrid14",
                            "path":"",
                            "type":"treegrid"
                        }
                    ])
                    .setResponseDataTarget([ ])
                    .setResponseCallback([ ])
                    .onData([
                        {
                            "args":[
                                "{page.initData()}"
                            ],
                            "desc":"动作 1",
                            "method":"initData",
                            "redirection":"page",
                            "target":"RAD.server.RemoteServerList",
                            "type":"page"
                        }
                    ])
            );

            append(
                xui.create("xui.UI.Dialog")
                    .setHost(host,"xui_ui_dialog18")
                    .setLeft("5em")
                    .setTop("1.6666666666666667em")
                    .setWidth("59.166666666666664em")
                    .setHeight("37.5em")
                    .setCaption("远程服务器配置")
                    .setImageClass("xui-icon-dragmove")
            );

            host.xui_ui_dialog18.append(
                xui.create("xui.UI.Block")
                    .setHost(host,"xui_ui_block29")
                    .setName("mainPanel")
                    .setDock("fill")
                    .setLeft("23.333333333333332em")
                    .setTop("9em")
            );

            host.xui_ui_block29.append(
                xui.create("xui.UI.Div")
                    .setHost(host,"xui_ui_div87")
                    .setName("pagediv")
                    .setDock("bottom")
                    .setLeft("19.375em")
                    .setTop("35.625em")
                    .setHeight("2.25em")
            );

            host.xui_ui_div87.append(
                xui.create("xui.UI.PageBar")
                    .setHost(host,"xui_ui_pagebar7")
                    .setName("pagebar")
                    .setLeft("12.5em")
                    .setTop("0.625em")
                    .setCaption("翻页")
            );

            host.xui_ui_block29.append(
                xui.create("xui.UI.ToolBar")
                    .setHost(host,"xui_ui_toolbar26")
                    .setName("toolbar")
                    .setItems([
                        {
                            "caption":"common",
                            "hidden":false,
                            "id":"common",
                            "sub":[
                                {
                                    "caption":"添加",
                                    "hidden":false,
                                    "iconFontSize":"",
                                    "id":"add",
                                    "imageClass":"fa fa-lg fa-calendar-plus-o",
                                    "position":"absolute"
                                },
                                {
                                    "caption":"刷新",
                                    "hidden":false,
                                    "iconFontSize":"",
                                    "id":"reload",
                                    "imageClass":"fa fa-lg fa-circle-o-notch",
                                    "position":"absolute"
                                },
                                {
                                    "caption":"清空",
                                    "hidden":false,
                                    "iconFontSize":"",
                                    "id":"clear",
                                    "imageClass":"fa fa-lg fa-close",
                                    "position":"absolute"
                                }
                            ]
                        }
                    ])
                    .setLeft("Infinityem")
                    .setTop("2.0833333333333335em")
                    .onClick([
                        {
                            "args":[
                                "{page.show2()}",
                                null,
                                null,
                                null,
                                null,
                                null,
                                "{page.getData()}",
                                "{page}"
                            ],
                            "className":"RAD.server.RemoteServerConfig",
                            "conditions":[
                                {
                                    "symbol":"=",
                                    "right":"add",
                                    "left":"{args[5]}"
                                }
                            ],
                            "desc":"动作 3",
                            "method":"show2",
                            "redirection":"page",
                            "return":false,
                            "target":"RAD.server.RemoteServerConfig",
                            "type":"page"
                        },
                        {
                            "args":[
                                "{page.clearHosts.invoke()}"
                            ],
                            "conditions":[
                                {
                                    "symbol":"=",
                                    "right":"clear",
                                    "left":"{args[1].id}"
                                }
                            ],
                            "desc":"清空",
                            "koFlag":"_DI_fail",
                            "method":"invoke",
                            "okFlag":"_DI_succeed",
                            "redirection":"other:callback:call",
                            "return":false,
                            "target":"clearHosts",
                            "type":"control"
                        },
                        {
                            "args":[
                                "{page.api_12.invoke()}"
                            ],
                            "conditions":[
                                {
                                    "symbol":"=",
                                    "right":"reload",
                                    "left":"{args[1].id}"
                                }
                            ],
                            "desc":"刷新",
                            "koFlag":"_DI_fail",
                            "method":"invoke",
                            "okFlag":"_DI_succeed",
                            "redirection":"other:callback:call",
                            "return":false,
                            "target":"api_12",
                            "type":"control"
                        }
                    ])
            );

            host.xui_ui_block29.append(
                xui.create("xui.UI.TreeGrid")
                    .setHost(host,"xui_ui_treegrid14")
                    .setName("grid")
                    .setLeft("0em")
                    .setTop("0em")
                    .setRowNumbered(true)
                    .setHeader([
                        {
                            "caption":"名称",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"name",
                            "readonly":true,
                            "type":"label",
                            "width":"12em"
                        },
                        {
                            "caption":"用户名",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":false,
                            "id":"userName",
                            "readonly":true,
                            "type":"label",
                            "width":"16em"
                        },
                        {
                            "caption":"URL",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":true,
                            "hidden":false,
                            "id":"url",
                            "readonly":true,
                            "type":"label",
                            "width":"12em"
                        },
                        {
                            "caption":"ID",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":true,
                            "id":"serverId",
                            "readonly":true,
                            "type":"label",
                            "width":"8em"
                        },
                        {
                            "caption":"密码",
                            "colResizer":true,
                            "editable":false,
                            "flexSize":false,
                            "hidden":true,
                            "id":"password",
                            "readonly":true,
                            "type":"label",
                            "width":"20em"
                        }
                    ])
                    .setUidColumn("serverId")
                    .setTagCmds([
                        {
                            "hidden":false,
                            "id":"delete",
                            "itemClass":"fa fa-lg fa-close",
                            "location":"left",
                            "pos":"row",
                            "tips":"删除"
                        }
                    ])
                    .setValue("")
                    .onCmd([
                        {
                            "args":[
                                "{page.delete.invoke()}"
                            ],
                            "desc":"动作 1",
                            "method":"invoke",
                            "redirection":"other:callback:call",
                            "target":"delete",
                            "type":"control"
                        }
                    ])
                    .onDblclickRow([
                        {
                            "args":[
                                "{page.show2()}",
                                null,
                                null,
                                null,
                                null,
                                null,
                                "{args[1]}",
                                "{page}"
                            ],
                            "className":"RAD.server.RemoteServerConfig",
                            "desc":"动作 1",
                            "method":"show2",
                            "redirection":"page",
                            "return":false,
                            "target":"RAD.server.RemoteServerConfig",
                            "type":"page"
                        }
                    ])
            );

            return children;
            // ]]Code created by JDSEasy RAD Studio
        },

        customAppend : function(parent, subId, left, top){
            return false;
        }  } ,
    Static:{
        "designViewConf":{
            "height":1024,
            "mobileFrame":false,
            "width":1280
        }
    }



});