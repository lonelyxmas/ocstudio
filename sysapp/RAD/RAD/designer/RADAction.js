new function () {
    if (!window.RAD) window.RAD = {};

    delFiele=function(){
        xui.Ajax(CONF.delFile, {
            paths: id,
            projectName: SPA.curProjectName
        }, function (txt) {
            var obj = txt;
            if (obj && !obj.error && obj.requestStatus != -1) {
                if (tree1) tree1.removeItems(arr);

                var items = tab.getItems(), b = [];
                xui.arr.each(items, function (o) {
                    if (xui.str.startWith(o.id, id))
                        b.push(o.id);
                }, null, true);
                tab.removeItems(b);

                if (id == ns.curProjectPath) {
                    ns._closeproject(null, true);
                }

                if (tree1 && !tree1.getUIValue())
                    ns._updateFileToolbar(null, 'prj');

            } else
                xui.message(xui.get(obj, ['error', 'errdes']) || obj || 'no response!');
        }, null, null, {method: 'POST'}).start();
    },


    _addfile= function (pid, pathadd, name, type, openit) {
        var ns = this,
            tb1 = ns.treebarPrj,
            a = name.split('.'),
            imageClass = a[1] ? CONF.getImageTag(a[1]) : null,
            item = {
                id: pathadd,
                name: name,
                caption: name,
                imageClass: imageClass,
                value: pathadd,
                sub: type == '/' ? [] : null
            },
            items = [item];

        if (type !== '/' && openit !== false)
            ns._openfile(item);

        if (tb1 && tb1.getItemByItemId(pid))
            tb1.insertItems(items, pid);

        tb1.selectItem(item.id);

    };
}