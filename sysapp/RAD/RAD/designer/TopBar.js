xui.Class('RAD', 'xui.Module', {
    Constructor: function () {
        var ns = this,
            o = xui.message;
        arguments.callee.upper.apply(ns, arguments);
        SPA = ns;
        ns.spath = null;
        ns.tpath = null;
        ns.curProjectPath = null;
        ns.curProjectName = null;
        ns.currentPage = null;
        ns.currentClassName = null;
        ns.currentRootCls = null;
        ns.curProjectConfig = null;
        ns.ViewMenuBar = {};
        ns.topMenuItems = xui.clone(CONF.SPATomMenu());
        ns.componentMenuItems = [];
        ns.toolbarItems = xui.clone(CONF.toolbarItems);

        ns.Message = [];
        xui.message = function (content) {
            if (!ns.Message)
                return;
            if (ns.Message.length > 20) ns.Message.pop();

            content = xui.stringify(content);

            if (/</.test(content))
                while (content != (content = content.replace(/<[^<>]*>/g, ""))) ;

            ns.Message.unshift({
                id: xui.id(),
                caption: content.length > 50 ? content.substr(0, 50) + '...' : content,
                tips: content,
                imageClass: 'xuicon xui-uicmd-info'
            });

            ns.toolbar.updateItem('info', {label: content.length > 50 ? content.substr(0, 50) + '...' : content});
            o.apply(null, xui.toArr(arguments));
        };

        xui('body').css({height: '100%', overflow: 'hidden'});
    },
    Instance: {
        fe: function (evtName, evtParams) {
            if (this.events[evtName]) return this.fireEvent(evtName, evtParams);
        },

        events: {
            beforeCreated: function () {
                xui.Dom.setCover('<img src="/RAD/img/loading.gif" border="0"/><div>Created</div>');
                var ns = this;


            },
            onLoadBaseClass: function (module, threadid, key) {
                xui.Dom.setCover('<img src="/RAD/img/loading.gif" border="0"/><div>' + 'Load Base Class: ' + key + '</div>');
            },
            onReady: function (page) {
                var host = this;
                host.menubar.setItems(host.topMenuItems);
                host.menubar.selectItem(host.topMenuItems[0].id)
                this.toolbar.setItems(this.toolbarItems);
                host.prjtabs.append(
                    (new xui.UI.ToolBar())
                        .setHost(host, "projecttool")
                        .setDisplay("none")
                        .setHandler(false)
                        .onClick("_projecttool_onclick")
                        .setCustomStyle({
                            "ITEMS": {
                                "border-width": "0 0 1px 0"
                            },
                        })
                    , "prj");

                host.prjtabs.append(
                    (new xui.UI.TreeView())
                        .setHost(host, "treebarPrj")
                        .setDynDestory(true)
                        .setOptBtn('xui-uicmd-opt')
                        .setDropKeys("xui.builder.pfjm")
                        .setDragKey("xui.builder.pfjm")
                        .onContextmenu("_treebarprj_oncontextmenu")
                        .onDblclick("_treebarprj_ondblclick")
                        .onGetContent("_treebarprj_onGetContent")
                        .onShowOptions("_treebarprj_onshowopt")
                        .onClick("_treebarprj_onclick")
                        .onItemSelected("_treebarprj_onitemselected")
                        .afterFold("_treebarprj_fold")
                        .onDropTest("__ondroptest")
                        .onDrop("__ondrop")
                        .beforeDrop("__beforedrop")
                        .setCustomStyle({
                            "RULER": {
                                "font-size": "1.5em"
                            }
                        })
                    , "prj");

                xui.ModuleFactory.setProfile(CONF.ModuleFactoryProfile);

                if (this.projecttool)
                    this.projecttool.setItems(CONF.projecttool);


                if (CONF.customUI) CONF.customUI(this);
            },
            onRender: function (page) {
                var ns = this;
                //use customApperance
                page.$infoList = new xui.UI.List({width: 400}).setCustomStyle('ITEM', 'border-bottom:dashed 1px gray');

                ns.openProject();
                xui.win.onKeypress(function (pro, e) {
                    var key = xui.Event.getKey(e),
                        ctrl = key.ctrlKey;
                    if (ctrl && k == 's') {
                        ns._menubar_onclick(ns.menubar.get(0), null, {id: "save"});
                        return false;
                    }
                });
            }
        },
        openProject: function () {
            var ns = this;
            if (currProjectName) {
                CONF.openProject(currProjectName, ns._openproject, ns._openclsfolder, function () {
                }, ns);
            } else {
                xui.ModuleFactory.getModule('prjSel', function () {
                    this.setProperties({
                        onOK1: ns._openproject,
                        onOK2: ns._openclsfolder,
                        namespace: ns
                    });
                    this.show(xui('body'));
                });
            }

        },

        showFirstPage: function () {
            var ns = this;
            xui.showModule("RAD.ProjectPro", function () {
                this.setProperties({
                    onOK1: ns._openproject,
                    onOK2: ns._openclsfolder,
                    namespace: ns
                });
            }, null, null, true);
        },
        _getpagename: function (name) {
            return name == "index.cls" ? xui.adjustRes('$(RAD.pm.Start Page)', true) :
                xui.str.endWith(name, '.cls') ? (name.replace(".cls", "") + " " + xui.adjustRes('$RAD.pm.Page', true)) :
                    name;
        },


        _addfile: function (pid, pathadd, name, type, openit) {
            var ns = this,
                tb1 = ns.treebarPrj,

                a = name.split('.'),
                imageClass = a[1] ? CONF.getImageTag(a[1]) : null,
                item = {
                    id: pathadd,
                    name: name,
                    caption: name,
                    imageClass: imageClass,
                    value: pathadd,
                    sub: type == '/' ? [] : null
                },
                items = [item];

            if (type !== '/' && openit !== false)
                ns._openfile(item);

            if (tb1 && tb1.getItemByItemId(pid))
                tb1.insertItems(items, pid);

            tb1.selectItem(item.id);

        },
        _delfile: function (id) {
            var ns = this,
                tree1 = this.treebarPrj,

                tab = this.tabsMain,
                arr = id.split(';'),
                a = [];
            xui.arr.each(arr, function (o, i) {
                a[i] = o;
            });


            xui.Ajax(CONF.delFile, {
                paths: id,
                projectName: SPA.curProjectName
            }, function (txt) {
                var obj = txt;
                if (obj && !obj.error && obj.requestStatus != -1) {
                    if (tree1) tree1.removeItems(arr);

                    var items = tab.getItems(), b = [];
                    xui.arr.each(items, function (o) {
                        if (xui.str.startWith(o.id, id))
                            b.push(o.id);
                    }, null, true);
                    tab.removeItems(b);

                    if (id == ns.curProjectPath) {
                        ns._closeproject(null, true);
                    }

                    if (tree1 && !tree1.getUIValue())
                        ns._updateFileToolbar(null, 'prj');

                } else
                    xui.message(xui.get(obj, ['error', 'errdes']) || obj || 'no response!');
            }, null, null, {method: 'POST'}).start();
        },

        _projecttool_onclick: function (profile, titem, group, e, src) {
            this._fileaction(null, titem.id, src, 'prj');

        },

        _fileaction: function (tbItem, action, src, type) {
            var ns = this,
                ins1 = ns.treebarPrj,
                ins = ins1;
            if (!this.curProjectPath) {
                xui.message(xui.getRes('RAD.ps.noprj'));
                return;
            }
            if (!tbItem) {
                var sv = ins.getUIValue();
                if (sv) tbItem = ins.getItemByItemId(sv);
            }
            if (!tbItem) {
                if (action != "del2" && action != "new2") {
                    var items = ins.getItems();
                    if (items[0]) {
                        tbItem = items[0];
                    }
                    if (!tbItem) {
                        return;
                    }
                }
            }

            switch (action) {
                case 'editastext':
                    ns._openfile(tbItem, src, false, true, true)
                    break;
                case 'explore':
                    CONF.showInExplore(tbItem ? tbItem.id : ns.curProjectPath);
                    break;
                case 'newFolder':
                    xui.showModule("RAD.AddFile", function () {
                        this.setProperties({
                            fileName: name,
                            parentPath: tbItem.id,
                            typeCaption: '$RAD.esdmenu.newFolder',
                            type: '/',
                            forcedir: true,
                            tailTag: ""
                        });
                        this.setEvents({
                            onOK: function (item) {
                                try {
                                    ns.dialog.hide();
                                } catch (e) {
                                }
                                item.sub = true;
                                item.id = item.location;
                                item.value = item.location
                                var insOther = ins1;
                                if (insOther && (tbItem = insOther.getItemByItemId(tbItem.id)))
                                    insOther.insertItems([item], tbItem.id, true);
                            }
                        });
                    }, null, null, true);
                    break;
                case 'newClass':
                    xui.showModule("RAD.AddFile", function () {
                        this.setProperties({
                            fileName: 'Page',
                            parentPath: tbItem.id,
                            typeCaption: '.cls',
                            type: 'class',
                            forcedir: true,
                            tailTag: ".cls"
                        });
                        this.setEvents({
                            onOK: function (item) {
                                try {
                                    ns.dialog.hide();
                                } catch (e) {

                                }

                                var insOther = ins1;
                                if (insOther && (tbItem = insOther.getItemByItemId(tbItem.id))) {
                                    item.caption = item.name ? item.name : item.caption;
                                    insOther.insertItems([item], tbItem.id, true);
                                }
                                item.value = item.id;
                                ns._openfile(item, item.path, false, true, true)

                            }
                        });
                    }, null, null, true);
                    break;
                case 'upload':
                    var pPath = tbItem.id;
                    var uploadPath = CONF.uploadPath;
                    var params = {
                        uploadUrl: CONF.upload,
                        projectName: SPA.curProjectName,
                        uploadpath: pPath
                    }

                    var dio = xui.create("xui.UI.Dialog")
                        .setLeft("8.333333333333334em")
                        .setTop("5.833333333333333em")
                        .setWidth("40.833333333333336em")
                        .setHeight("30em")
                        .setIframeAutoLoad(uploadPath + "?uploadUrl=" + CONF.upload + "&projectName=" + SPA.curProjectName + "&uploadpath=" + pPath)
                        .setCaption("$RAD.addfile.iUpload")
                        .setMaxBtn(false)
                        .beforeClose(function () {
                            //window['postMessage'] = [];
                            //     xui.message(xui.getRes('RAD.tool2.refreshOK'));
                            // }, ns, true);
                            // ns.fireEvent('onOK',[pPath, "", "", false],ns);
                        })
                        .setMaxBtn(false);

                    if (window['postMessage']) {
                        self._msgcb = function (e) {
                            try {
                                e = e.data;
                                e = xui.unserialize(e);
                                if (e.data) {
                                    var item = e.data;
                                    var insOther = ins1;
                                    if (insOther && (tbItem = insOther.getItemByItemId(tbItem.id))) {
                                        var a = item.name.split('.');
                                        a = a.length == 1 ? "" : (a[a.length - 1].toLowerCase());
                                        item.imageClass = CONF.getImageTag(a);
                                        item.value = item.location;
                                        insOther.insertItems([item], tbItem.id, true);
                                    }
                                }
                            } catch (e) {

                            }

                        };
                        if (window.addEventListener) window.addEventListener('message', self._msgcb, false);
                        else window.attachEvent('onmessage', self._msgcb);
                    }


                    dio.show();
                    break;
                case  'import'   :
                    xui.showModule("RAD.ProjectTree", function () {
                        this.setData({
                            tpath: tbItem.id
                        });
                        this.setEvents({
                            onOK: function (sub) {
                                var insOther = ins1;
                                xui.each(sub, function (item) {
                                    if (insOther && (tbItem = insOther.getItemByItemId(tbItem.id)))
                                        insOther.insertItems([item], tbItem.id, false);
                                })
                            }
                        });
                        this.initData();
                        this._fireEvent('afterShow');
                    }, null, null, true);

                    break;
                case  'importModule'   :
                    xui.showModule("RAD.ProjectModuleTree", function () {
                        this.setData({
                            tpath: tbItem.id

                        });
                        this.setEvents({
                            onOK: function (sub) {
                                var insOther = ins1;
                                xui.each(sub, function (item) {
                                    if (insOther && (tbItem = insOther.getItemByItemId(tbItem.id)))
                                        insOther.insertItems([item], tbItem.id, false);
                                })
                            }
                        });
                        this.initData();
                        this._fireEvent('afterShow');
                    }, null, null, true);

                    break;


                case
                'paste2'
                :

                    xui.showModule("RAD.AddFile", function () {
                        this.setProperties({
                            fileName: 'Page',
                            parentPath: tbItem.id,
                            typeCaption: '.cls',
                            type: 'class',
                            forcedir: true,
                            tailTag: ".cls"
                        });
                        this.setEvents({
                            onOK: function (item) {
                                try {
                                    ns.dialog.hide();
                                } catch (e) {

                                }
                                var insOther = ins1;
                                if (insOther && (tbItem = insOther.getItemByItemId(tbItem.id))) {
                                    item.caption = item.name ? item.name : item.caption;
                                    insOther.insertItems([item], tbItem.id, true);
                                }
                                item.value = item.id;
                                ns._openfile(item, item.path, false, true, true)

                            }
                        });
                    }, null, null, true);
                    break;
                case
                'paste'
                :
                    xui.Ajax(CONF.copy, {
                        spath: ns.spath,
                        forcedir: true,
                        tpath: tbItem.id,
                        projectName: SPA.curProjectName
                    }, function (txt) {
                        var obj = txt;
                        if (obj && !obj.error && obj.requestStatus != -1) {

                            var data = obj.data;
                            item = {
                                id: data.location,
                                name: data.name,
                                caption: data.name,
                                className: data.className,
                                imageClass: data.imageClass,
                                value: data.location
                            };

                            if (xui.str.endWith(data.id, ".cls")) {

                                var insOther = ins1;
                                if (insOther && (tbItem = insOther.getItemByItemId(tbItem.id)))
                                    insOther.insertItems([item], tbItem.id);
                                ns._openfile(item, item.path, false, true, true)
                            } else if (xui.str.endWith(data.id, "/")) {
                                item.sub = true;
                                var insOther = ins1;
                                if (insOther && (tbItem = insOther.getItemByItemId(tbItem.id)))
                                    insOther.insertItems([item], tbItem.id);

                            } else {
                                var insOther = ins1;
                                if (insOther && (tbItem = insOther.getItemByItemId(tbItem.id)))
                                    insOther.insertItems([item], tbItem.id);
                            }

                            ns.spath = null;
                        } else
                            xui.message(xui.get(obj, ['error', 'errdes']) || obj || 'no response!');
                    }, function (obj) {
                        xui.message(xui.get(obj, ['error', 'errdes']) || obj || 'no response!');
                    }, null, {method: 'POST'}).start();

                    break;

                case
                'copy2'
                :
                    ns.spath = tbItem.id;

                    break;

                case
                'rename'
                :

                    var name = tbItem.name, tail = "";
                    if (!tbItem.sub) {
                        var a = name.split("."),
                            tail = "." + a.pop();
                        name = a.join(".");
                    }

                    var dlg = xui.prompt(xui.getRes("RAD.designer.Rename"), xui.getRes("RAD.Specify the new name") + (tail ? ("(" + tail + ")") : ""), name, function (nn) {

                        if (nn) nn = xui.str.trim(nn);
                        nn += tail;
                        if (!CONF.fileNames.test(nn)) {
                            xui.message(xui.getRes("RAD.addfile.filenameformat"));
                            return false;
                        } else {
                            var nid = tbItem._pid;
                            if (!xui.str.endWith(nid, "/")) {
                                nid = nid + "/";
                            }
                            var oid = tbItem.id,
                                nid = nid + nn,
                                name = nn,
                                imageClass;

                            xui.Ajax(CONF.reName, {
                                path: oid,
                                projectName: SPA.curProjectName,
                                newName: name
                            }, function (txt) {
                                var obj = txt;
                                if (obj && !obj.error && obj.requestStatus != -1) {
                                    var data = obj.data;
                                    var item = {
                                        id: data.location,
                                        name: data.name,
                                        caption: data.name,
                                        className: data.className,
                                        imageClass: data.imageClass,
                                        path: data.location,
                                        value: data.location
                                    };


                                    if (tbItem.sub) {
                                        // modify the project name
                                        if (SPA.curProjectPath == tbItem.id) {
                                            SPA.curProjectPath = nid;
                                            SPA.curProjectName = name;
                                        }

                                        // remove allsub first
                                        var arr = [],
                                            fun = function (sub) {
                                                var f = fun;
                                                if (xui.isArr(sub))
                                                    xui.arr.each(sub, function (o, i) {
                                                        arr.push(o.id);
                                                        if (o.sub) f(o.sub);
                                                    });
                                                f = null;
                                            };
                                        fun(tbItem.sub);
                                        ins.removeItems(arr);

                                        arr = [];
                                        var insOther = type == 'prj' ? ins2 : ins1;
                                        var itt = insOther.getItemByItemId(tbItem.id);
                                        if (itt && itt.sub && itt.sub.length) {
                                            fun(itt.sub);
                                            insOther.removeItems(arr);
                                        }

                                        if (ins1) ins1.updateItem(oid, {
                                            id: nid,
                                            value: nid,
                                            loacation: nid,
                                            name: name,
                                            caption: name,
                                            _inited: false,
                                            sub: true
                                        });
                                        ins2.updateItem(oid, {
                                            id: nid,
                                            value: nid,
                                            loacation: nid,
                                            name: name,
                                            caption: ns._getpagename(name),
                                            _inited: false,
                                            sub: true
                                        });

                                        // remove all modified tabs
                                        var items = ns.tabsMain.getItems(), b = [];
                                        xui.arr.each(items, function (o) {
                                            if (!ins.getSubIdByItemId(o.id))
                                                b.push(o.id);
                                        }, null, true);

                                        if (b && b.length)
                                            ns.tabsMain.removeItems(b);
                                    } else {
                                        item.imageClass = CONF.getImageTag(nid);
                                        // udpate file name
                                        if (ins1) ins1.updateItem(oid, xui.clone(item));
                                        ins2.updateItem(oid, xui.clone(item));
                                        // update tabs
                                        var aitem = ns.tabsMain.getItemByItemId(oid);
                                        if (aitem) {
                                            ns.closeFile(oid)
                                            ns._openfile(item, item.path, false, true, true)
                                        }
                                    }
                                    xui.message(xui.getRes("$RAD.designer.msg.renamefileok"));
                                } else
                                    xui.message(xui.get(obj, ['error', 'errdes']) || obj || 'no response!');
                            }, null, null, {method: 'POST'}).start();
                        }
                    });
                    dlg._$input.setMultiLines(false);//.setHeight(30);
                    dlg = null;
                    break;
                case

                'del2'
                :
                    if (tbItem) {
                        xui.confirm(xui.getRes('RAD.delfile.confirmdel'), xui.getRes('RAD.delfile.confirmdel2', "1"), function () {
                            ns._delfile(tbItem.id);
                        });
                    } else {
                        xui.ModuleFactory.getModule('delFile', function () {
                            this.host = ns;
                            this.setProperties({
                                fromRegion: xui(src).cssRegion(true),
                                parent: ns,
                                onOK: ns._delfile
                            });
                            this.show(xui('body'));
                        });
                    }
                    break;
                case   'refresh':
                    CONF.openProject(ns.curProjectName, ns._openproject, null, function () {
                        xui.message(xui.getRes('RAD.tool2.refreshOK'));
                    }, ns, true);
                    break;
            }

        }
        ,
        _dirtyWarn: function (callback) {
            var ns = this, dirty, tb = this.tabsMain, items = tb.getItems();
            xui.arr.each(items, function (o) {
                if (o._dirty) return !(dirty = true);
            });
            if (dirty)
                xui.UI.Dialog.confirm(xui.getRes('RAD.notsave'), xui.getRes('RAD.notsave3'), callback);
            else
                callback();
        }
        ,
        _setproject: function () {
            // xui.showModule("RAD.ProjectSetting");
            // xui.showModule("RAD.project")
            xui.getModule("RAD.project.ProjectInfo", function () {
                this.setData({projectName: SPA.curProjectName});
                this.show();
                this.initData();
                this._fireEvent('afterShow');
            });
            //  break;
        }
        ,
        _closeproject: function (callback, deleted) {
            var ns = this, dirty, tb = this.tabsMain, items = tb.getItems(),
                tree1 = this.treebarPrj;

            xui.arr.each(items, function (o) {
                if (o && o._dirty) return !(dirty = true);
            });
            var fun = function () {
                ns._resetPrjEnv();

                // save status first
                var hash = CONF.getStatus(),
                    prjs = hash.projects || (hash.projects = []),
                    index = xui.arr.subIndexOf(prjs, 'id', ns.curProjectPath);
                if (index != -1) xui.arr.removeFrom(prjs, index);
                if (!deleted) {
                    prjs.push({
                        caption: ns.curProjectName,
                        id: ns.curProjectPath,
                        file: tb.getUIValue(),
                        files: tb.getItems('min')
                    });
                    if (prjs.length >= 10) prjs.shift();
                }
                CONF.saveStatus(hash);

                tb.clearItems();
                if (tree1) tree1.clearItems();


                xui.Class.__gc("Module");
                xui.$cache.clsByURI = {};
                xui.$cache.fetching = {};

                ns.curProjectPath = ns.curProjectName = ns.currentPage = ns.currentRootCls = ns.curProjectConfig = null;


                if (typeof callback == 'function') callback();
                if (ns.projecttool)
                    ns.projecttool.setDock('none').setDisplay('none');
                if (tree1) tree1.adjustDock(true);

                if (ns._toolbox) {
                    ns._toolbox.clearContent();
                }

                if (ns._servicebox) {
                    ns._servicebox.clearContent();
                }

                window["/"] = undefined;

                // clear project
                document.title = document.title.replace(/\s*\[.*\]\s*$/, '');
            };

            if (dirty)
                xui.UI.Dialog.confirm(xui.getRes('RAD.notsave'), xui.getRes('RAD.notsave2'), fun);
            else
                fun();
        }
        ,
        _tabsmain_beforepageclose: function (profile, item, src) {
            if (item._dirty) {
                xui.UI.Dialog.confirm(xui.getRes('RAD.notsave'), xui.getRes('RAD.notsave2'), function () {
                    profile.boxing().removeItems(item.id);
                });
                return false;
            }
        }
        ,
        _tabsmain_afterpageclose: function (profile, item, src) {
            if (item.$obj) item.$obj.destroy();
            SPA.currentPage = "";
            SPA.currentClassName = "";
        }
        ,
        _tabsmain_beforeValueUpdated: function (profile, ov, nv) {
            var item = this.tabsMain.getItemByItemId(ov), t;
            if (!item) return;
            if (t = item.$obj) {
                if (t.checkEditorCode && t.checkEditorCode() === false) return false;
            }
        }
        ,
        _tabmain_onitemselected: function (profile, item, e, src) {
            if (item._inn) {
                profile.boxing().setDisabled(true);
                profile.boxing().append(item._inn, item.id);
                delete item._inn;
            }
            xui.tryF(function () {
                if (item.$obj) item.$obj.activate();
            });
            SPA.currentPage = item.id;
            SPA.currentClassName = item.className;
        }
        ,
        _treebarprj_oncontextmenu: function (profile, e, src, item) {
            if (item) {
                this._treebarprj_onshowopt(profile, item, e, src);
                return false;
            }
        }

        ,
        _treebarprj_onshowopt: function (profile, item, e, src) {
            var ns = this, ins = profile.boxing(), items;
            if (item) {
                items = [
                    {id: 'del2', imageClass: 'xuicon xui-icon-minus', caption: '$RAD.tool2.del2'},
                    {id: 'split', type: 'split'}
                ];
                if (item && !item.sub) {
                    items.push({
                        id: 'editastext',
                        imageClass: 'spafont spa-icon-astext',
                        caption: '$RAD.tool2.editastext'
                    });
                }
                items.push({id: 'refresh', imageClass: 'xuicon xui-refresh', caption: '$RAD.tool2.refresh'});
                items.unshift({id: 'rename', imageClass: 'spafont spa-icon-rename', caption: '$RAD.tool2.modify'});

                if (item.sub) {

                    items.unshift({id: 'upload', imageClass: 'fa fa-file-o', caption: '$RAD.esdmenu.upload'});
                    if (xui.str.startWith(item.id, 'Module')) {
                        items.unshift({
                            id: 'importModule',
                            imageClass: 'fa fa-file-o',
                            caption: '$RAD.esdmenu.importModule'
                        });
                    } else {
                        items.unshift({id: 'import', imageClass: 'fa fa-file-o', caption: '$RAD.esdmenu.import'});
                    }
                    items.unshift({id: 'newFolder', imageClass: 'fa fa-folder-o', caption: '$RAD.esdmenu.newFolder'});
                    items.unshift({
                        id: 'newClass',
                        imageClass: 'xuicon xui-uicmd-add',
                        caption: '$RAD.esdmenu.newClass'
                    });
                    items.unshift({
                        id: 'copy2',
                        imageClass: 'spafont spa-icon-copy',
                        caption: '',
                        caption: '$RAD.tool2.copy2'
                    });

                    if (ns.spath) {
                        items.unshift({
                            id: 'paste',
                            imageClass: 'spafont spa-icon-paste',
                            caption: '$RAD.esdment.paste',
                            tip: '$RAD.esdment.paste'
                        });
                    }
                } else {

                    items.unshift({
                        id: 'copy2',
                        imageClass: 'spafont spa-icon-copy',
                        caption: '',
                        caption: '$RAD.tool2.copy2'
                    });
                }
            } else {
                items = [
                    {id: 'refresh', imageClass: 'xuicon xui-refresh', caption: '$RAD.tool2.refresh'},
                    {id: 'new', imageClass: 'xuicon xui-uicmd-add', caption: '$RAD.tool2.new2'},
                    {id: 'del', imageClass: 'xuicon xui-icon-minus', caption: '$RAD.tool2.del2'}
                ];
            }
            var pop = new xui.UI.PopMenu({
                items: items
            });
            pop.onMenuSelected(function (prf, popitem, src) {
                ns._fileaction(item, popitem.id,
                    item ? ins.getSubNodeByItemId('BAR', item.id) : null,
                    "prj");
                pop.destroy();
            });
            pop.pop(xui.Event.getPos(e));
            return false;
        }
        ,
        _treebarprj_onGetContent: function (profile, item, callback) {
            var ns = this;
            xui.Ajax(CONF.openFolderService, {path: item.id, projectName: SPA.curProjectName}, function (txt) {
                var obj = txt;
                if (obj && !obj.error) {
                    isxuipage = false;
                    var root = ns.buildFileItems(item.id, obj.data.files || obj.data, isxuipage ? function (o) {
                        return !(o.type && !xui.str.endWith(o.name, ".js") && !xui.str.endWith(o.name, ".cls") && !xui.str.endWith(o.name, ".html") && !xui.str.endWith(o.name, ".vv"));
                    } : null, isxuipage);
                    callback(root.sub);
                } else xui.message(xui.get(obj, ['error', 'errdes']) || obj || 'no response!');
            }, null, null, {method: 'POST'}).start();


        }
        ,
        _treebarprj_ondblclick: function (profile, item, e, src) {
            if (!item.sub) {
                this._openfile(item, src)
            }
        }
        ,
        openFile: function (path) {
            this._openfile({
                id: path,
                value: path,
                name: path.split('/').pop()
            });
        }
        ,
        closeFile: function (path) {

            var ns = this,
                tb = ns.tabsMain;
            if (tb.getItemByItemId(path)) {
                tb.removeItems([path]);
            }
        }
        ,

        getSelected: function () {
            return this._getSelected();
        },

        _openfile: function (item, src, stopseleted, dontshowerror, forceAsText) {
            if (!item.id || item.sub) return;

            var ns = this,
                tb1 = ns.treebarPrj,
                className = item.className;
            if (className && xui.str.endWith(className, ".cls")) {
                className = className.replace(".cls", "");
            }
            arr = item.name.split('.'),
                type = arr[arr.length - 1],
                isText = false;

            if (!forceAsText) {
                if (!isText) {
                    if (!CONF.getRenderMode(type) && type.indexOf("VVVERSION") == -1) {
                        CONF.openFile(item.id);
                        return;
                    }
                }
            }

            //item.value
            var value = item.value;
            if (!value) {
                value = item._pid + '/' + item.name;
            }

            var arr = value.split('/'),
                filename = arr[arr.length - 1],
                caption = xui.str.startWith(value, SPA.curProjectPath + "/" + ns.currentRootCls + "/js/") ? ns._getpagename(filename) : filename,
                filearr = filename.split('.'),
                filetype = filearr[filearr.length - 1],
                imageClass = CONF.getImageTag(filetype);

            var tb = ns.tabsMain,
                pro = tb.reBoxing().cssRegion(true);
            if (tb.getItemByItemId(value) && !tb._dirty) {
                tb.fireItemClickEvent(value);
            } else {

                var funOK = function (txt) {
                        if (txt.error) {
                            return;
                        }
                        if (!txt.data || !txt.data.content) {
                            return;
                        }


                        var filecon = txt.data.content;
                        if (xui.isSet(filecon)) {
                            var item = {
                                    id: value, tips: value,
                                    caption: caption,
                                    closeBtn: true,
                                    className: className,
                                    imageClass: imageClass,
                                    panelBgImg: '/RAD/img/loading.gif',
                                    panelBgImgPos: "center center",
                                    panelBgImgRepeat: "no-repeat"//,
                                    //newText:filecon,
                                    //text:filecon
                                },
                                items = tb.getItems(),
                                itemid = item.id,
                                callback = function (pagprofile, b) {
                                    tb.markItemCaption(pagprofile.properties.keyId, b);
                                };

                            if (!tb.getItemByItemId(value)) {
                                tb.insertItems([item], items.length ? items[items.length - 1].id : null);
                            }

                            if (filetype != 'js' && filetype != 'cls') {
                                xui.ModuleFactory.newModule('RAD.PageEditor', function () {
                                    var inn = this;
                                    inn.host = ns;
                                    inn.setType(filetype);
                                    inn.setProperties({
                                        keyId: itemid
                                    });
                                    inn.setEvents('onValueChanged', callback);
                                    inn.setEvents('onCommandSave', function (pagprofile) {
                                        //pagprofile.properties.keyId
                                        ns._menubar_onclick(ns.menubar.get(0), null, {id: "save"});
                                    });
                                    inn.setEvents('afterRendered', function () {
                                        tb.setDisabled(false);
                                    });

                                    inn.setValue(filecon);

                                    var item = tb.getItemByItemId(itemid);
                                    item.$obj = inn;
                                    item._inn = inn;

                                    if (!stopseleted)
                                        tb.fireItemClickEvent(value);
                                });
                            } else {
                                xui.ModuleFactory.newModule('RAD.JSEditor', function () {
                                    var inn = this;
                                    inn.buttonview.setNoHandler(true);
                                    var tb = ns.tabsMain;
                                    inn.host = ns;
                                    inn.setProperties({
                                        keyId: itemid
                                    });
                                    inn.setEvents('onValueChanged', callback);
                                    inn.setEvents('onCommandSave', function (pagprofile) {
                                        //pagprofile.properties.keyId
                                        ns._menubar_onclick(ns.menubar.get(0), null, {id: "save"});
                                    });
                                    inn.setEvents('afterRendered', function () {
                                        tb.setDisabled(true);

                                        var des = inn._designer;

                                        var item = tb.getItemByItemId(itemid);
                                        item.$obj = inn;
                                        // item._inn = inn;
                                        xui.setTimeout(function () {
                                            try {
                                                if (des.getWidgets() && des.getWidgets().length > 0) {
                                                    var jscode = des.getJSCode(des.getWidgets(true));
                                                    // des.refreshView(jscode, true);
                                                    des.markDirty(true);
                                                    des.resetCodeFromDesigner(true);
                                                    des.refreshView(jscode, true);
                                                    des.markDirty(false);
                                                    tb.markItemCaption(itemid, false, true);
                                                }
                                            } catch (e) {
                                                console.warn(e);
                                            }
                                            tb.setDisabled(false);
                                        }, 500);


                                    });
                                    var clsname = RAD.ClassTool.getClassName(filecon),
                                        pclsname = RAD.ClassTool.getClassName(filecon, true);

                                    if (clsname && pclsname && !forceAsText) {
                                        inn.setDftPage('design');
                                    } else {
                                        inn.setDftPage('code');
                                    }
                                    inn.setValue(filecon);

                                    var item = tb.getItemByItemId(itemid);
                                    item.$obj = inn;
                                    item._inn = inn;

                                    if (!stopseleted)
                                        tb.fireItemClickEvent(value);
                                });
                            }
                            // try to select
                            if (tb1)
                                tb1.setUIValue(value);

                        }
                    },
                    funFail = function (msg) {
                        if (!dontshowerror)
                            xui.message(msg);
                    };

                xui.Ajax(CONF.getFileContent, {
                    path: value,
                    className: className,
                    projectName: SPA.curProjectName
                }, funOK, funFail, {method: 'POST'}).start();


            }
        }

        ,
        iniComponents: function () {
            // [[Code created by EU RAD Studio
            var host = this, ns = this, children = [], append = function (child) {
                children.push(child.get(0));
            };

            append(
                (new xui.UI.MenuBar())
                    .setHost(host, "menubar")
                    .setDockOrder("1")
                    .setAutoShowTime(0)
                    .setHandler(false)
                    .setValue('file')
                    .onMenuBtnClick("_menubar_onMenuBtnClick")
                    .onMenuSelected("_menubar_onclick")
                    .setCustomStyle({
                        "BORDER": {
                            "background-color": "#3498DB",
                            "border": "0px"
                        },
                        "LIST": {
                            "padding": "0px"
                        },
                        "ITEM": {
                            "margin": "0 1em -0.125em 1em",
                            "padding-left": "1em",
                            "padding-right": "1em"
                        },
                        "ITEMS": {
                            "color": "#f6f6f6",
                            "background-color": "#3498DB"
                        }
                    })
            );


            append(
                xui.create("xui.UI.Div")
                    .setHost(host, "topbar")
                    .setDock("top")
                    .setLeft("21.6em")
                    .setHeight("3.8em")
                    .setZIndex(10)
                    .setCustomStyle({
                        "KEY": {
                            "background-color": "#3498DB"
                        }
                    })
            );

            host.topbar.append(
                xui.create("xui.UI.Div")
                    .setHost(host, "xui_ui_div22")
                    .setHoverPop("xui_ui_svgpaper1")
                    .setLeft("12.878787878787879em")
                    .setTop("0.9375em")
                    .setWidth("8.75em")
                    .setHeight("2.5em")
            );

            host.xui_ui_div22.append(
                xui.create("xui.UI.Span")
                    .setHost(host, "xui_ui_space")
                    .setWidth("1em")
                    .setTabindex(2)
                    .setPosition("static")
            );

            host.xui_ui_div22.append(
                xui.create("xui.UI.Label")
                    .setHost(host, "xui_ui_label3")
                    .setTabindex(3)
                    .setPosition("static")
                    .setCaption("管理员")
                    .setCustomStyle({
                        "KEY": {
                            "color": "#FFFFFF"
                        }
                    })
            );

            append(
                xui.create("xui.UI.SVGPaper")
                    .setHost(host, "xui_ui_svgpaper1")
                    .setLeft("4.375em")
                    .setTop("3.75em")
                    .setWidth("11.25em")
                    .setHeight("1.375em")
                    .setZIndex(1002)
                    .setOverflow("visible")
                    .setGraphicZIndex(2)
            );

            host.xui_ui_svgpaper1.append(
                xui.create("xui.UI.List")
                    .setHost(host, "xui_ui_list8")
                    .setDirtyMark(false)
                    .setItems(CONF.teamBar)
                    .setLeft("0.625em")
                    .setTop("1.25em")
                    .setWidth("9.84848484848485em")
                    .setHeight("auto")
                    .setSelMode("none")
                    .setValue("")
            );

            host.xui_ui_svgpaper1.append(
                xui.create("xui.svg.path")
                    .setHost(host, "xui_svg_path1")
                    .setSvgTag("Shapes:Triangle")
                    .setAttr({
                        "path": "M,21,21L,28,1L,51,21",
                        "fill": "#ffffff",
                        "stroke": "#B6B6B6"
                    })
            );

            host.xui_ui_div22.append(
                xui.create("xui.UI.Label")
                    .setHost(host, "xui_ui_label4")
                    .setLeft("0em")
                    .setTabindex(4)
                    .setPosition("static")
                    .setCaption("")
                    .setImageClass("xui-icon-sort-checked")
                    .setCustomStyle({
                        "KEY": {
                            "color": "#FFFFFF"
                        }
                    })
            );

            host.topbar.append(
                xui.create("xui.UI.Icon")
                    .setHost(host, "xui_ui_icon9")
                    .setLeft("10.606060606060607em")
                    .setTop("0.7575757575757576em")
                    .setWidth("3.0303030303030303em")
                    .setHeight("2.272727272727273em")
                    .setImageClass("spafont spa-icon-login")
                    .setIconFontSize("2em")
            );

            host.topbar.append(
                xui.create("xui.UI.Gallery")
                    .setHost(host, "xui_ui_gallery3")
                    .setItems(CONF.navBar)
                    .setTop("0.6875em")
                    .setWidth("16em")
                    .setHeight("5em")
                    .setRight("0em")
                    .setSelMode("none")
                    .setBorderType("none")
                    .setIconOnly(true)
                    .setValue("")
                    .onClick({
                        "actions": [
                            {
                                "args": [
                                    "{page.xui_ui_svgpaper10.popUp()}"
                                ],
                                "conditions": [
                                    {
                                        "symbol": "=",
                                        "right": "a",
                                        "conditionId": "a_equal_{args[1].id}",
                                        "left": "{args[1].id}"
                                    }
                                ],
                                "desc": "Action1",
                                "method": "popUp",
                                "redirection": "other:callback:call",
                                "target": "xui_ui_svgpaper10",
                                "type": "control"
                            }
                        ]
                    })
                    .setCustomStyle({
                        "FLAG": {
                            "margin": "-12px -48px 0px 0px",
                            "font-size": "8px"
                        },
                        "ICON": {
                            "font-size": "3em"
                        },
                        "ITEM": {
                            "background-color": "transparent"
                        },
                        "ITEMS": {
                            "background-color": "transparent",
                            "overflow": "hidden"
                        }
                    })
            );

            host.topbar.append(
                xui.create("xui.UI.Label")
                    .setHost(host, "xui_ui_label28")
                    .setLeft("4.545454545454546em")
                    .setWidth("3.8636363636363638em")
                    .setHeight("1.2121212121212122em")
                    .setCaption("CodeBee")
                    .setHAlign("center")
                    .setFontColor("#FFFFFF")
                    .setFontSize("1.5em")
            );

            host.topbar.append(
                xui.create("xui.UI.Image")
                    .setHost(host, "xui_ui_image4")
                    .setLeft("0.7575757575757576em")
                    .setTop("0.5em")
                    .setWidth("2.621212121212121em")
                    .setHeight("2.636363636363638em")
                    .setSrc("/RAD/img/staticjds.gif")
                    .setCustomStyle({
                        "KEY": {
                            "opacity": 0.75
                        }
                    })
            );


            append(
                (new xui.UI.ButtonViews())
                    .setHost(host, "mainLayout")
                    .setBarLocation("right")
                    .setBarSize("7em")
                    .setSideBarStatus("fold")
                    .setItems([{
                        "id": "xuimain",
                        "caption": "页面编辑",

                        "imageClass": "spafont spa-icon-page"
                    }, {
                        "id": "java",
                        "caption": "JAVA编辑",
                        "imageClass": "spafont spa-icon-hash"
                    },
                        {
                            "id": "dsm",
                            "caption": "领域建模",
                            "imageClass": "iconfont iconchangjingguanli"
                        }
                    ])
                    .onItemSelected(function (profile, item) {
                        if (item.id == 'java' && !item.className) {
                            var className = 'java.BuildTree';
                            xui.showModule2('java.BuildTree', "mainLayout", "java", {
                                projectVersionName: currProjectName,
                                projectName: currProjectName
                            });
                            item.className = className;
                        } else if (item.id == 'dsm' && !item.className) {
                            var className = 'dsm.nav.DSMInstNavTree';
                            xui.showModule2(className, "mainLayout", "dsm", {
                                projectVersionName: currProjectName,
                                projectName: currProjectName
                            });
                            item.className = className;
                        }

                    })
                    .setOverflow("none")
                    .setValue("xuimain")
                , "main"
            )


            host.mainLayout.append(
                (new xui.UI.Block())
                    .setHost(host, "mainPanel")
                    .setDock('fill')
                , "xuimain"
            );

            host.mainPanel.append(
                (new xui.UI.Layout())
                    .setHost(host, "layout")
                    .setItems([{
                        "id": "before",
                        "pos": "main",
                        "locked": false,
                        "size": 260,
                        "min": 50,
                        "max": 800,
                        "cmd": true,
                        "hide": false,
                        "folded": false,
                        "hidden": false
                    }, {
                        "id": "main",
                        "min": 10
                    }])
                    .setCustomStyle({
                        "MOVE": {
                            "background-color": "transparent"
                        }
                    })
                    .setType("horizontal")
            );


            host.layout.append(
                (new xui.UI.ButtonViews())
                    .setHost(host, "prjtabs")
                    .setNoHandler(true)
                    .setItems([{
                        "id": "prj",
                        "caption": "$RAD.toolbox.prj",
                        "imageClass": "spafont spa-icon-project"
                    }])
                    .setOverflow("none")
                    .setValue("prj")
                , "before");


            host.layout.append(
                (new xui.UI.Tabs())
                    .setHost(host, "tabsMain")
                    .beforeUIValueSet("_tabsmain_beforeValueUpdated")
                    .beforePageClose("_tabsmain_beforepageclose")
                    .afterPageClose("_tabsmain_afterpageclose")
                    .onItemSelected("_tabmain_onitemselected")
                    .setCustomStyle({
                        "PANEL": "overflow:hidden;"
                    })
                , "main");

            xui.newModule("RAD.ToolBox", function () {
                this.dlg.show(host.mainPanel, null, 0, 30);
                ns._toolbox = this;
            })


            host.mainLayout.append(
                (new xui.UI.ToolBar())
                    .setHost(host, "toolbar")
                    .setDockOrder("2")
                    .onClick("_toolbar_onclick")
                    .setHandler(false)
                    .setIconFontSize("2em")
                    .setCustomStyle({
                        "GROUP": {
                            height: "90px"
                        },
                        "BOXWRAP": {
                            "display": "block",
                        },
                        "ITEMS": {
                            // "font-size": "2em",
                            "background-color": "#f6f6f6"
                        }
                    })
            );


            return children;
            // ]]Code created by ESDUI RAD Studio
        }
        ,
        _treebarprj_fold: function (profile) {
            var tree = profile.boxing();
            if (!tree.getUIValue())
                this._updateFileToolbar(null, 'prj');
        }
        ,

        _treebarprj_onitemselected: function (profile, item, e, src, type) {
            this._updateFileToolbar(item, 'prj');
        }
        ,
        _treebarprj_onclick: function (profile, item) {
            if (item.sub)
                profile.boxing().toggleNode(item.id, true);
        }
        ,
        _ontvcmd: function (profile, item, cmdkey) {
            var w = xui.get(SPA.curProjectConfig, ['$DevEnv', 'designViewConf', 'width']) || 1280,
                h = xui.get(SPA.curProjectConfig, ['$DevEnv', 'designViewConf', 'height']) || 1024,
                touch = xui.get(SPA.curProjectConfig, ['$DevEnv', 'designViewConf', 'touchDevice']) || 0;


            CONF.debugApp("/debug/" + SPA.curProjectName + "/", xui.getClassName(item.id), {
                rand: xui.rand(),
                touch: touch,
                width: w,
                height: h,
                cls: xui.getClassName(item.id),
                theme: xui.getTheme(),
                lang: xui.getLang()
            }, true);
        }
        ,
        _treebarprj_onitemselected2: function (profile, item, e, src, type) {
            this._updateFileToolbar(item, 'cls');
        }
        ,
        _updateFileToolbar: function (item, type) {
            var ns = this, opt = {hidden: !item || !!item.sub},
                tool = ns.projecttool;
            tool.updateItem('rename', opt);
            tool.updateItem('copy2', opt);
            tool.updateItem('editastext', opt);
        }
        ,
        _toolbar_onclick: function (profile, titem, group, e, src) {
            var ns = this;
            if (titem.sub && titem.sub.length > 0) {
                var popMenu = new xui.UI.PopMenu({
                        items: titem.sub
                    }, {
                        onMenuSelected: function (p, item) {
                            ns._toolbar_onclick(p, item, group, src);
                        }
                    })
                ;
                popMenu.setCustomStyle({
                    "ICON": "font-size:1.2em;",
                    "ITEM": "background-color:#F6F6F6;"

                })
                popMenu.pop(src);

            } else if (xui.str.startWith(titem.id, "xui.") && titem.draggable) {
                this._addComponent(titem);
                return;
            } else {
                this._menubar_onclick(null, null, titem, src, false);
            }
        }
        ,
        buildFileItems: function (path, obj, filter, isxuipage) {
            obj = obj.files || obj;
            //root
            var ns = this,
                names = path.split('/'),
                name = names[names.length - 1],
                imagePos, imageClass, className

            hash = {
                '*': {
                    id: path,
                    name: name,
                    caption: name,
                    imageClass: 'fa fa-bank',
                    value: path,
                    sub: [],
                    iniFold: false
                }
            };

            //sort to appropriate order
            obj.sort(function (x, y) {
                return x.layer < y.layer ? 1 : x.layer == y.layer ? (
                    x.type > y.type ? 1 : x.type == y.type ? (
                        x.name == "index.cls" ? -1 :
                            y.name == "index.cls" ? 1 :
                                x.location > y.location ? 1 :
                                    -1
                    ) : -1
                ) : -1;
            });
            //add sub
            xui.arr.each(obj, function (o) {
                // filter
                if (filter && filter(o) === false) return;

                if (!o.type)
                    imagePos = undefined;
                else {
                    var a = o.name.split('.');
                    a = a.length == 1 ? "" : (a[a.length - 1].toLowerCase());
                    imageClass = CONF.getImageTag(a, isxuipage);
                }
                hash[o.id] = {
                    id: o.location,
                    name: o.name,
                    caption: isxuipage ? ns._getpagename(o.name) : o.name,
                    imageClass: o.imageClass,
                    className: o.className,
                    value: o.location,
                    iniFold: o.iniFold,
                    sub: o.sub
                };
                if (!o.type) {
                    hash[o.id].sub = true;
                    hash[o.id].tips = "$(RAD.designer.Click to expand) " + o.name;
                } else {
                    hash[o.id].tips = "$(RAD.designer.Double click to open) " + o.name;
                }
                if (isxuipage && o.type)
                    hash[o.id].tagCmds = [{
                        id: "preview",
                        tips: "$RAD.JSEditor.Preview",
                        type: "text",
                        sub: true,
                        caption: '',
                        itemClass: "spafont spa-icon-preview"
                    }];
                if (!o.pid) {
                    o.pid = "*";
                }
                hash[o.pid].sub.push(hash[o.id]);
            });

            return hash['*'];
        }
        ,
        _resetPrjEnv: function () {
            var ns = this,
                dftHash = {
                    'background-image': 'url(' + xui.getPath('/RAD/img/designer/', 'bg.gif') + ')',
                    'background-position': 'left top',
                    'background-repeat': 'repeat',
                    "background-color": '',
                    "background-attachment": ''
                };
            //Page Appearance
            ns.tabsMain.setSandboxTheme("");
            xui.CSS.addStyleSheet(xui.UI.buildCSSText({
                '.xui-designer-root .xui-designer-canvas': dftHash
            }), 'xui.UI.design', null, true);

            // Font Icons CDN
            xui('head').querySelectorAll('link[id^="xui_app_fscdn-"]').remove(true);

            //Element Style
            xui.CSS.setStyleRules(".xui-desinger-canvas .xui-custom", null, true);

            //mobile frame
            try {
                xui.each(RAD.Designer.getAllInstance(), function (page) {
                    if (page.canvas)
                        page.setMobileFrame(false);
                });
            } catch (e) {

            }
            //Default Prop
            xui.UI.__resetDftProp_in_Desinger = null;
            xui.arr.each(xui.Class._all, function (cls) {
                if (/^xui\.UI\./.test(cls)) {
                    if (cls = xui.get(window, cls.split('.'))) {
                        cls.__resetDftProp_in_Desinger = null;
                    }
                }
            });
        },


        refreshProjectConfig: function (setting, callback) {
            var ns = this,
                path = "xuiconf.js",
                txt, json, arr, reg,
                dftHash = {
                    'background-image': 'url(' + xui.getPath('/RAD/img/designer/', 'bg.gif') + ')',
                    'background-position': 'left top',
                    'background-repeat': 'repeat',
                    "background-color": '',
                    "background-attachment": ''
                },
                cover = function (scr) {
                    scr = xui.replace(scr, [
                        [/\/(\\[\/\\]|[^*\/])(\\.|[^\/\n\\])*\/[gim]*/, '$0'], //regexp
                        [/"(\\.|[^"\\])*"/, '$0'], //""
                        [/'(\\.|[^'\\])*'/, '$0'], //''
                        [/\/\*[^*]*\*+([^\/][^*]*\*+)*\//, '']
                    ]);
                    // Page Appearance
                    arr = scr.split(/\s*\/\/ \[\[Page Appearance\s*\n|\n\s*\/\/ \]\]Page Appearance\s*/);
                    if (arr.length == 3) {
                        txt = arr[1];
                        json = null;

                        try {
                            reg = /^\s*xui\.ini\.\$PageAppearance\s*\=\s*/;
                            if (reg.test(txt)) {
                                txt = xui.replace(txt, [
                                    [/\/(\\[\/\\]|[^*\/])(\\.|[^\/\n\\])*\/[gim]*/, '$0'],
                                    [/"(\\.|[^"\\])*"/, '$0'],
                                    [/'(\\.|[^'\\])*'/, '$0'],
                                    [reg, ''],
                                    [/;\s*$/, ''],
                                    [/\/\/[^\n]*/, '']
                                ]);
                                json = xui.unserialize(txt);
                            }
                        } catch (e) {
                            xui.alert(e + "<br\><br\>File => " + path + "<br\><br\>at : xui.ini.$PageAppearance = ...");
                            return;
                        }
                        if (typeof json === "object" && json !== null) {
                            ns.curProjectConfig.$PageAppearance = json;

                            if (json.theme) {
                                ns.tabsMain.setSandboxTheme(json.theme, true, '[class~="xui-designer-inner-control"]');
                            }
                            if (json.background && xui.isHash(json.background)) {
                                var hash = xui.clone(dftHash);
                                xui.each(json.background, function (o, i) {
                                    hash[i] = xui.adjustRes(o);
                                });

                                xui.CSS.addStyleSheet(xui.UI.buildCSSText({
                                    '.xui-designer-root .xui-designer-canvas': hash
                                }), 'xui.UI.design', null, true);
                            }
                        }
                    }


                    // Font Icons CDN
                    arr = scr.split(/\s*\/\/ \[\[Font Icons CDN\s*\n|\n\s*\/\/ \]\]Font Icons CDN\s*/);
                    if (arr.length == 3) {
                        txt = arr[1];
                        json = null;

                        try {
                            reg = /^\s*xui\.ini\.\$FontIconsCDN\s*\=\s*/;
                            if (reg.test(txt)) {
                                txt = xui.replace(txt, [
                                    [/\/(\\[\/\\]|[^*\/])(\\.|[^\/\n\\])*\/[gim]*/, '$0'],
                                    [/"(\\.|[^"\\])*"/, '$0'],
                                    [/'(\\.|[^'\\])*'/, '$0'],
                                    [reg, ''],
                                    [/;\s*$/, ''],
                                    [/\/\/[^\n]*/, '']
                                ]);
                                json = xui.unserialize(txt);
                            }
                        } catch (e) {
                            xui.alert(e + "<br\><br\>File => " + path + "<br\><br\>at : xui.ini.$FontIconsCDN = ...");
                            return;
                        }
                        if (typeof json === "object" && json !== null) {
                            ns.curProjectConfig.$FontIconsCDN = json;
                            // for CDN font icons
                            xui.each(json, function (o, i) {
                                if (o.href) {
                                    var attr = {crossorigin: 'anonymous'};
                                    xui.merge(attr, o, function (v, j) {
                                        return j !== 'href'
                                    });
                                    xui.CSS.includeLink(xui.adjustRes(o.href), 'xui_app_fscdn-' + i, false, attr);
                                }
                            });
                        }
                    }

                    // Element Style
                    arr = scr.split(/\s*\/\/ \[\[Element Style\s*\n|\n\s*\/\/ \]\]Element Style\s*/);
                    if (arr.length == 3) {
                        txt = arr[1];
                        json = null;

                        try {
                            reg = /^\s*xui\.ini\.\$ElementStyle\s*\=\s*/;
                            if (reg.test(txt)) {
                                txt = xui.replace(txt, [
                                    [/\/(\\[\/\\]|[^*\/])(\\.|[^\/\n\\])*\/[gim]*/, '$0'],
                                    [/"(\\.|[^"\\])*"/, '$0'],
                                    [/'(\\.|[^'\\])*'/, '$0'],
                                    [reg, ''],
                                    [/;\s*$/, ''],
                                    [/\/\/[^\n]*/, '']
                                ]);
                                json = xui.unserialize(txt);
                            }
                        } catch (e) {
                            xui.alert(e + "<br\><br\>File => " + path + "<br\><br\>at : xui.ini.$ElementStyle = ...");
                            return;
                        }
                        if (typeof json === "object" && json !== null) {
                            ns.curProjectConfig.$ElementStyle = json;
                            xui.CSS.setStyleRules(".xui-desinger-canvas .xui-custom", json, true);
                        }
                    }

                    // Default Prop
                    arr = scr.split(/\s*\/\/ \[\[Default Prop\s*\n|\n\s*\/\/ \]\]Default Prop\s*/);
                    if (arr.length == 3) {
                        txt = arr[1];
                        json = null;

                        try {
                            reg = /^\s*xui\.ini\.\$DefaultProp\s*\=\s*/;
                            if (reg.test(txt)) {
                                txt = xui.replace(txt, [
                                    [/\/(\\[\/\\]|[^*\/])(\\.|[^\/\n\\])*\/[gim]*/, '$0'],
                                    [/"(\\.|[^"\\])*"/, '$0'],
                                    [/'(\\.|[^'\\])*'/, '$0'],
                                    [reg, ''],
                                    [/;\s*$/, ''],
                                    [/\/\/[^\n]*/, '']
                                ]);
                                json = xui.unserialize(txt);
                            }
                        } catch (e) {
                            xui.alert(e + "<br\><br\>File => " + path + "<br\><br\>at : xui.ini.$DefaultProp = ...");
                            return;
                        }
                        if (typeof json === "object" && json !== null) {
                            ns.curProjectConfig.$DefaultProp = json;

                            var allp = {}, ctl;
                            xui.each(json, function (v, k) {
                                if (/^xui\.UI\./.test(k) && xui.isHash(v) && (ctl = xui.get(window, k.split('.')))) {
                                    ctl.__resetDftProp_in_Desinger = v;
                                } else {
                                    allp[k] = v;
                                }
                            });
                            if (!xui.isEmpty(allp)) {
                                xui.UI.__resetDftProp_in_Desinger = allp;
                            }
                        }
                    }

                    // Develop Env Setting
                    arr = scr.split(/\s*\/\/ \[\[Develop Env Setting\s*\n|\n\s*\/\/ \]\]Develop Env Setting\s*/);
                    if (arr.length == 3) {
                        txt = arr[1];
                        json = null;

                        try {
                            reg = /^\s*xui\.ini\.\$DevEnv\s*\=\s*/;
                            if (reg.test(txt)) {
                                txt = xui.replace(txt, [
                                    [/\/(\\[\/\\]|[^*\/])(\\.|[^\/\n\\])*\/[gim]*/, '$0'],
                                    [/"(\\.|[^"\\])*"/, '$0'],
                                    [/'(\\.|[^'\\])*'/, '$0'],
                                    [reg, ''],
                                    [/;\s*$/, ''],
                                    [/\/\/[^\n]*/, '']
                                ]);
                                json = xui.unserialize(txt);
                            }
                        } catch (e) {
                            xui.alert(e + "<br\><br\>File => " + path + "<br\><br\>at : xui.ini.$DevEnv = ...");
                            return;
                        }
                        // cover
                        if (typeof json === "object" && json !== null) {
                            ns.curProjectConfig.$DevEnv = json;

                            //view size
                            var size = {}, mf, su;
                            if ((size.width = xui.get(json, ['designViewConf', 'width'])) &&
                                (size.height = xui.get(json, ['designViewConf', 'height']))
                            ) {
                                xui.each(RAD.Designer.getAllInstance(), function (page) {
                                    if (page.canvas) {
                                        page.setViewSize(size);
                                        // for themes: classic webflat
                                        if (page._cls) {
                                            xui.asyRun(function () {
                                                if (page._cls) page.refreshView(page._cls);
                                            }, 100);
                                        }
                                    }
                                });
                            }

                            if (mf = xui.get(json, ['designViewConf', 'touchDevice'])) {
                                xui.each(RAD.Designer.getAllInstance(), function (page) {
                                    if (page.canvas)
                                        page.setMobileFrame(mf);
                                });
                            }
                            if (su = xui.get(json, ['SpaceUnit'])) {
                                xui.each(RAD.Designer.getAllInstance(), function (page) {
                                    if (page.canvas) {
                                        // for themes: classic webflat
                                        if (page._cls) {
                                            xui.asyRun(function () {
                                                if (page._cls) page.refreshView(page._cls);
                                            }, 100);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    // Global Functions
                    arr = scr.split(/\s*\/\/ \[\[Global Functions\s*\n|\n\s*\/\/ \]\]Global Functions\s*/);
                    if (arr.length == 3) {
                        txt = arr[1];
                        json = null;

                        try {
                            reg = /^\s*xui\.\$cache\.functions\s*\=\s*/;
                            if (reg.test(txt)) {
                                txt = xui.replace(txt, [
                                    [/\/(\\[\/\\]|[^*\/])(\\.|[^\/\n\\])*\/[gim]*/, '$0'],
                                    [/"(\\.|[^"\\])*"/, '$0'],
                                    [/'(\\.|[^'\\])*'/, '$0'],
                                    [reg, ''],
                                    [/;\s*$/, ''],
                                    [/\/\/[^\n]*/, '']
                                ]);
                                json = xui.unserialize(txt);
                            }
                        } catch (e) {
                            xui.alert(e + "<br\><br\>File => " + path + "<br\><br\>at : xui.$cache.functions = ...");
                            return;
                        }

                        // cover
                        if (typeof json === "object" && json !== null) {
                            ns.curProjectConfig.$GlobalFunctions = json;
                        }
                    }

                    // Global Data
                    arr = scr.split(/\s*\/\/ \[\[Global Data\s*\n|\n\s*\/\/ \]\]Global Data\s*/);
                    if (arr.length == 3) {
                        txt = arr[1];
                        json = null;

                        try {
                            reg = /^\s*xui\.\$cache\.data\s*\=\s*/;
                            if (reg.test(txt)) {
                                txt = xui.replace(txt, [
                                    [/\/(\\[\/\\]|[^*\/])(\\.|[^\/\n\\])*\/[gim]*/, '$0'],
                                    [/"(\\.|[^"\\])*"/, '$0'],
                                    [/'(\\.|[^'\\])*'/, '$0'],
                                    [reg, ''],
                                    [/;\s*$/, ''],
                                    [/\/\/[^\n]*/, '']
                                ]);
                                json = xui.unserialize(txt);
                            }
                        } catch (e) {
                            xui.alert(e + "<br\><br\>File => " + path + "<br\><br\>at : xui.$cache.data = ...");
                            return;
                        }

                        // cover
                        if (typeof json === "object" && json !== null) {
                            ns.curProjectConfig.$GlobalData = json;
                        }
                    }


                    // Constant Data
                    arr = scr.split(/\s*\/\/ \[\[Constant Data\s*\n|\n\s*\/\/ \]\]Constant Data\s*/);
                    if (arr.length == 3) {
                        txt = arr[1];
                        json = null;

                        try {
                            reg = /^\s*xui\.constant\s*\=\s*/;
                            if (reg.test(txt)) {
                                txt = xui.replace(txt, [
                                    [/\/(\\[\/\\]|[^*\/])(\\.|[^\/\n\\])*\/[gim]*/, '$0'],
                                    [/"(\\.|[^"\\])*"/, '$0'],
                                    [/'(\\.|[^'\\])*'/, '$0'],
                                    [reg, ''],
                                    [/;\s*$/, ''],
                                    [/\/\/[^\n]*/, '']
                                ]);
                                json = xui.unserialize(txt);
                            }
                        } catch (e) {
                            xui.alert(e + "<br\><br\>File => " + path + "<br\><br\>at : xui.constant = ...");
                            return;
                        }

                        // cover
                        if (typeof json === "object" && json !== null) {
                            ns.curProjectConfig.$ConstantData = json;
                        }
                    }
                };

            // cached in memory
            ns.curProjectConfig = {};

            if (xui.isSet(setting)) {
                ns._resetPrjEnv();
                cover(setting || "");
                xui.tryF(callback);
            } else {
                // get file sync

                xui.Ajax(CONF.getProjectConfigService, {projectName: SPA.curProjectName}, function (scr) {
                    ns._resetPrjEnv();
                    cover(scr || "");
                    xui.tryF(callback);
                    //
                }, function (e) {

                    ns._resetPrjEnv();
                    xui.tryF(callback);
                    xui.alert("Missing file : '" + path + "'.");
                }, null, {rspType: "text", method: 'POST'}).start();
            }
            return ns.curProjectConfig;
        }
        ,
        _openproject: function (pm, obj, nomainjs, currentRootCls) {
            var ns = this;
            pm = pm.replace(/\\/g, "/");
            window["/"] = CONF.getPrjPath(pm, true) + "/";
            window["/"] = CONF.adjustProtocol(window["/"]);

            ns.currentRootCls = currentRootCls || "App";
            ns.curProjectPath = pm;

            while (xui.str.endWith(pm, "/")) {
                pm = pm.substr(0, pm.length - 1);
            }

            ns.curProjectName = pm.substr(pm.lastIndexOf("/") + 1, pm.length);
            var projectName = pm.replace("VVVERSION", "(V") + ")";

            var root = ns.buildFileItems(projectName, obj),
                tb = ns.treebarPrj,
                tabs = ns.tabsMain;

            if (tb) tb.clearItems();
            if (tb) tb.insertItems([root]);

            ns.tabsMain.setDisabled(false);

            var hash = CONF.getStatus(),
                prjs = hash.projects,
                index = xui.arr.subIndexOf(prjs || [], 'id', ns.curProjectPath),
                files = index != -1 ? prjs[index].files : [],
                file = index != -1 ? prjs[index].file : null;
            if (!files) files = [];
            if (!file && files[0]) file = files[0];
            if (!file && !nomainjs) {
                file = ns.currentRootCls + "/index.cls";
                if (xui.arr.subIndexOf(files, file) == -1) {
                    files.push(file);
                }
            }

            ns.refreshProjectConfig(null, function () {
                // add them to tabs
                if (files && files.length) {
                    xui.arr.each(files, function (o, i) {
                        i = o.substr(o.lastIndexOf("/") + 1, o.length);
                        var item = {
                            id: o,
                            value: o,
                            name: i,
                            className: o.replaceAll('/', '.'),
                            caption: ns._getpagename(i)
                        };

                        // open the 'file'
                        if (item.caption.indexOf('VVVERSION') == -1) {
                            ns._openfile(item, null, o != file, true);
                        }
                    });


                }
                // select one to expand
                if (file && tb) {
                    tb.setUIValue(null);
                    tb.setUIValue(file, true);
                }
            });

            if (ns.projecttool)
                ns.projecttool.setDock('none').setDisplay('').setDock('top');


            if (!nomainjs && ns._toolbox) {
                ns._toolbox.fillContent();
            }


            // add project path here
            document.title = document.title.replace(/\s*\[.*\]\s*$/, '') + " [" + pm + "]";
        }
        ,

        _checkEvent: function (events) {
            var ns = this;

            var subevents = {};
            xui.each(events, function (event, key, events) {

                var eventMap = events[key];
                if (eventMap) {
                    if (xui.isArr(eventMap)) {
                        aAction = [];
                        xui.each(eventMap, function (action) {
                            if (typeof(action) == "string") {
                                aAction.push({script: action, desc: '函数'});
                            } else if (xui.isHash(action)) {
                                aAction.push(action);
                            }
                        });
                        subevents[key] = {actions: aAction};
                    } else if (typeof(eventMap) == "string") {
                        subevents[key] = {actions: [{script: eventMap, desc: '函数'}]}
                    } else if (!eventMap.actions) {
                        delete  events[key];
                        var neweventMap = {actions: eventMap};
                        subevents[key] = neweventMap;
                    } else {
                        var subAction = [];
                        var actions = events[key].actions;
                        if (typeof(actions) == "string") {
                            subAction.push({script: actions.toString(), desc: '函数'})
                        } else {
                            xui.each(actions, function (action, k) {
                                if (!xui.isHash(action)) {
                                    subAction.push({script: action.toString(), desc: '函数'});
                                } else {
                                    var args = [];
                                    xui.each(action.args, function (arg, i) {
                                            if (typeof(arg) == "function") {
                                                args.push({script: arg.toString(), desc: '函数'});
                                            } else {
                                                args.push(arg);
                                            }

                                        }
                                    );
                                    if (!action.type) {
                                        action.type = 'page';
                                    }
                                    action.args = args;
                                    subAction.push(action);
                                }

                            });
                            subevents[key] = events[key];
                            subevents[key].actions = subAction;
                        }

                    }
                }
            });
            return subevents;
        },

        _getDesigner: function () {
            var tb = this.tabsMain,
                id = tb.getUIValue(),
                o = tb.getItemByItemId(id);
            return o.$obj._designer;
        }

        ,
        _saveFile: function (o, collections) {
            var ns = this, tb = this.tabsMain;
            if (o._dirty) {
                var newText = o.$obj.getValue(), isModule, cur, shouldbe;
                if (false === newText)
                    return false;

                if (xui.str.startWith(o.id, SPA.curProjectPath + "/" + ns.currentRootCls + "/") || (isModule = xui.str.startWith(o.id, SPA.curProjectPath + "/Module/js/"))) {
                    cur = RAD.ClassTool.getClassName(newText);
                    var currpath = o.id;
                    if (currpath.indexOf(SPA.curProjectPath) > -1) {
                        currpath = currpath.replace(SPA.curProjectPath + '/', '');
                    }
                    shouldbe = xui.getClassName(currpath);
                    if (cur != shouldbe) {
                        xui.pop(xui.getRes("RAD.designer.The class name in '$0' should be '$1', but it's '$2'", o.id, shouldbe, cur));
                        shouldbe = cur = null;
                    }
                }

                var paras = {
                    projectName: SPA.curProjectName,
                    hashCode: xui.id(),
                    curProjectPath: SPA.curProjectPath,
                    path: o.id,
                    fileType: 'EUFile',
                    content: newText,
                    jscontent: newText
                };
                var designer = o.$obj._designer;

                if (designer && designer.getWidgets() && designer.getWidgets().length > 0) {
                    var code = designer.getJSCode(designer.getWidgets());
                    var com = new xui.Module();
                    com.iniComponents = new Function([], code);
                    var hash = {}, t;
                    xui.each(designer.getNames(), function (prf) {
                        var parentName = prf.parent ? prf.parent.alias : 'this';
                        if (!prf.moduleClass) {
                            t = hash[prf.alias] = prf.serialize(false, false, false);
                            if (hash[prf.alias].events) {
                                hash[prf.alias].events = ns._checkEvent(hash[prf.alias].events);
                            }
                            hash[prf.alias].host = parentName;
                            if (prf.childrenId) {
                                hash[prf.alias].target = prf.childrenId;
                            }
                        } else {
                            var module = prf.host;
                            module.properties.className = prf.moduleClass;
                            if (!hash[module.alias]) {
                                t = hash[module.alias] = module.serialize(false, false, false);
                                if (hash[module.alias].events) {
                                    hash[module.alias].events = ns._checkEvent(hash[module.alias].events);
                                }
                                hash[module.alias].host = parentName;
                                if (prf.childrenId) {
                                    hash[module.alias].target = prf.childrenId;
                                }

                                hash[module.alias].className = prf.moduleClass;
                                hash[module.alias].key = 'xui.Module'
                            }


                        }

                        //delete t['xui.Module'];
                    });


                    //删除错误定义
                    if (designer._cls.Instance.functions) {
                        xui.each(designer._cls.Instance.functions, function (modulefunction) {
                            if (modulefunction.actions) {
                                var iactions = [];
                                xui.each(modulefunction.actions, function (action) {
                                    if (xui.isFun(action)) {
                                        iactions.push(
                                            {script: action.toString(), desc: '函数'}
                                        )
                                    } else if (action && xui.isObj(action)) {
                                        if (!action.type) {
                                            delete action.type;
                                        }
                                        iactions.push(action)
                                    }
                                    ;
                                });
                                modulefunction.actions = iactions;
                            }
                        })

                    }
                    ;

                    //初始化事件

                    designer._cls.Instance.events = ns._checkEvent(designer._cls.Instance.events);
                    var modulefunctions = {};

                    xui.each(designer._cls.Instance, function (action, key) {
                        var syskeys = ['iniComponents', 'initialize', 'customAppend'];
                        if (typeof(action) == 'function') {
                            if (xui.arr.indexOf(syskeys, key) == -1) {
                                modulefunctions[key] = action.toString();
                            }
                        }
                    });

                    var moduleVar = {};

                    xui.each(designer._cls.Instance, function (action, key) {
                        var syskeys = ['Dependencies', 'Required', 'properties', 'events', 'functions'];
                        if (typeof(action) != 'function') {
                            if (xui.arr.indexOf(syskeys, key) == -1) {
                                moduleVar[key] = xui.serialize(action);
                                ;
                            }
                        }
                    });

                    //初始化
                    var customAppend = designer._cls.Instance.customAppend, customAppendStr;
                    if (customAppend && typeof(customAppend) == 'function') {
                        customAppendStr = customAppend.toString();
                    }


                    var deffunctions = {};
                    if (!designer._cls.Instance.functions && xui.isHash(designer._cls.Instance.functions)) {
                        deffunctions = designer._cls.Instance.functions;
                    }

                    // var staticConfig = {
                    //     designViewConf: designer.$curViewSize || {},
                    //     viewStyles: designer._cls.Static?.viewStyles || {}
                    // };


                    var xuicode = {
                            components: hash,
                            'Static': designer._cls.Static || {},
                            events: ns._checkEvent(designer._cls.Instance.events),
                            customFunctions: modulefunctions,
                            moduleVar: moduleVar,
                            customAppend: customAppendStr,
                            dependencies:
                            designer._cls.Instance.dependencies || [],
                            properties:
                            designer._cls.Instance.properties || {},
                            required:
                            designer._cls.Instance.Required || []
                        }
                    ;


                    var code = xui.Coder.formatText(xui.stringify(xuicode), 'js');
                    paras.className = designer._className;
                    paras.content = code
                    paras.fileType = 'EUClass';
                    com.destroy();
                }


                if (CONF.grid_showPropBinder) {
                    var init = xui.get(RAD.ClassTool.getClassStruct(newText), ["sub", "Instance", "sub", "iniComponents"]);
                    if (init) {
                        var module = new xui.Module();
                        module.iniComponents = xui.unserialize(init.code);
                        paras.propKeys = module.getPropBinderKeys();
                    }
                }
                ;
                var onSuccess = function (txt) {
                    var obj = txt;
                    if (obj && !obj.error && obj.data && obj.requestStatus != -1) {
                        o.$obj.setValue(newText);
                        tb.markItemCaption(o.id, false, true);
                        if (isModule && shouldbe) {
                            var m = xui.SC(shouldbe);
                            if (m) {
                                // refresh Class first
                                m.refresh(newText);
                                // refresh  instances
                                var ins = m.getAllInstance();
                                if (!xui.isEmpty(ins)) {
                                    xui.each(ins, function (mi) {
                                        mi.refresh();
                                    });
                                }
                            }
                        }

                        if (o.id == SPA.curProjectPath + "/xuiconf.js") {
                            ns.refreshProjectConfig(newText);
                        }
                        xui.message('  save success!   ');
                    } else
                        xui.message(xui.get(obj, ['error', 'errdes']) || obj || 'no response!');
                };
                var onFail = function (txt) {
                    xui.message(txt);
                };
                collections.push({id: o.id, paras: paras, onSuccess: onSuccess, onFail: onFail});

            }
            return collections;
        }

        ,
        _ann_onclick: function () {
            xui.ModuleFactory.getModule('about', function () {
                this.dialog.show(null, true);
            });
        },

        _getSelected: function () {
            var sel = this._getDesigner().tempSelected,
                out;
            if (sel && sel.length) {
                out = [];
                xui.arr.each(sel, function (o) {
                    out.push(xui.getObject(o));
                });
            }
            return out;
        },

        _updateComponent: function (item) {
            var designer = this._getDesigner(), currNode = designer.getByAlias(item.alias, true), ns = this;
            currNode = currNode || designer.canvas;
            if (!currNode.removeChildren) {
                currNode.boxing().removeChildren(true, true, true);
            } else {
                currNode.removeChildren(true, true, true);
            }

            if (item.children) {
                xui.each(item.children, function (o) {
                    ns._addComponent(o);
                })
            }

            var dirty = designer._dirty;
            designer.refreshView(designer.getJSCode(designer.getWidgets(true)), true);
            if (dirty) designer.markDirty();
        }

        ,
        _addComponent: function (item) {
            var designer = this._getDesigner();
            var selectNode = designer.getByCacheId(this.tempSelected);
            var parent;
            if (selectNode.isEmpty()) {
                selectNode = xui.getObject(designer.tempSelected[0]);
                if (selectNode) {
                    selectNode = selectNode.boxing();
                    parent = selectNode || designer.canvas;
                } else {
                    parent = designer.canvas;
                }
            } else {
                selectNode = selectNode.boxing();
            }


            var obj = item, children = item.children || item.sub;
            if (!children || children.length == 0) {
                obj = {children: [item]}
            }

            if (xui.str.startWith(item.key, 'xui.svg.')) {
                var components = xui.addChild(obj, parent, designer.$host, designer.$host);

                var redo = function () {
                        var components = xui.addChild(obj, parent, designer.$host, designer.$host);
                        var go = components[0].get(0);
                        designer._designable(go);
                        designer.selectWidget([go.$xid]);
                    },
                    undo = function () {
                        var ins = designer.getByAlias(item.alias);
                        ins.destroy(true, true);
                    }

                designer.markDirty(undo, redo, "add commponents");


            } else if (xui.str.startWith(item.key, 'xui.UI.')) {
                var components = xui.addChild(obj, parent, designer.$host, designer.$host);
                var go = components[0].get(0), xid = go.$xid;
                designer._designable(go);
                designer.selectWidget([xid]);

                var redo = function () {
                        var components = xui.addChild(obj, parent, designer.$host, designer.$host);
                        var go = components[0].get(0);
                        designer._designable(go);
                        designer.selectWidget([go.$xid]);
                    },
                    undo = function () {
                        var ins = designer.getByAlias(item.alias);
                        ins.destroy(true, true);
                    }

                designer.markDirty(undo, redo, "add commponents");


            } else {
                var citem = item;
                var target = xui.create(citem.key)
                    .setHost(designer.$host, citem.alias)
                    .setAlias(citem.alias)
                    .setEvents(citem.events)
                    .setProperties(citem.properties);
                if (designer.$host) {
                    designer.$host[citem.alias] = target
                }

                designer.iconlist.insertItems([{
                    'id': target.get(0).$xid,
                    'imgWidth': '16',
                    'imgHeight': '16',
                    'tips': designer._bldTips(target, designer.$moduleInnerMode ? "" : CONF.enable_codeEdtor ? ("$(RAD.designer.Dblclick to edit the code)") : "")
                    , 'imgStyle': item.imgStyle,
                    'imageClass': item.imageClass
                }]);
                designer.iconlist.setUIValue(target.get(0).$xid);
                if (!children || children.length == 0) {
                    obj = {children: [item]}
                }
                var go = target.get(0), xid = go.$xid;
                designer._designable(go);
                designer.selectWidget([go.$xid]);
                xui.addChild(obj, parent, designer.$host, designer.$host);
                var redo = function () {
                        var target = xui.create(citem.key)
                            .setHost(designer.$host, citem.alias)
                            .setAlias(citem.alias)
                            .setEvents(citem.events)
                            .setProperties(citem.properties);
                        if (designer.$host) {
                            designer.$host[citem.alias] = target
                        }

                        designer.iconlist.insertItems([{
                            'id': xid,
                            'imgWidth': '16',
                            'imgHeight': '16',
                            'tips': designer._bldTips(target, designer.$moduleInnerMode ? "" : CONF.enable_codeEdtor ? ("$(RAD.designer.Dblclick to edit the code)") : "")
                            , 'imgStyle': item.imgStyle,
                            'imageClass': item.imageClass
                        }]);
                        designer.iconlist.setUIValue(xid);
                        if (!children || children.length == 0) {
                            obj = {children: [item]}
                        }
                        designer._designable(go);
                        designer.selectWidget([xid]);
                    },
                    undo = function () {
                        designer.iconlist.removeItems([xid]);
                        var ins = designer.getByAlias(citem.alias);
                        ins.destroy(true, true);
                    }
                designer.markDirty(undo, redo, "add commponents");
            }
        },
        _loadPluginsMenu: function (server) {
            var ns = this;
            var callback = function (data) {
                var menus = [];
                var allactions = [];

                //初始化API
                if (data && data.apis) {
                    xui.each(data.apis, function (citem) {
                        xui.create(citem.key)
                            .setAlias(citem.alias)
                            .setHost(ns, citem.alias)
                            .setEvents(xui.checkEvents(citem.events))
                            .setProperties(citem.properties);
                    });
                }
                var events = ns._checkEvent(SPA.toolbar.getEvents())

                xui.each(data.events, function (event, key) {
                    var toolbarAction = events[key].actions;
                    xui.each(event.actions, function (action) {
                        toolbarAction.push(action);
                    })

                })
                SPA.toolbar.setEvents(xui.checkEvents(events));

                // scope, hash[i], i, hash

                //SPA.toolbar.setEvents(data.events);

                if (data.properties && data.properties.items && data.properties.items.length > 0) {
                    menus.push(data.properties.items[0]);
                }
                SPA.toolbar.setItems(CONF.chaneWidget(data.properties.items))
            };


            var params = server.params;
            xui.Ajax(server.baseUrl, params, function (txt) {
                var obj = txt;
                if (obj && !obj.error) {
                    callback(obj.data);
                }
            }, null, null, null, {method: 'POST', asy: true}).start();


        },

        _loadBarMenu: function (server) {
            var ns = this;
            var callback = function (data) {
                var menus = [];

                if (data && data.apis) {
                    xui.each(data.apis, function (citem) {
                        xui.create(citem.key)
                            .setAlias(citem.alias)
                            .setHost(ns, citem.alias)
                            .setEvents(xui.checkEvents(citem.events))
                            .setProperties(citem.properties);
                    });
                }

                var datas = data[0].sub || data.sub || data;
                SPA.toolbar.setItems(CONF.chaneWidget(datas))
            }

            var params = server.params;
            xui.Ajax(server.baseUrl, params, function (txt) {
                var obj = txt;
                if (obj && !obj.error) {
                    callback(obj.data);
                }
            }, null, null, null, {method: 'POST', asy: true}).start();


        }

        ,
        _menubar_onMenuBtnClick: function (profile, item, src) {
            var ns = this;
            ns.currSrc = src;
            if (item.toolbar && item.toolbar.length > 0) {
                this.toolbar.setItems(item.toolbar)
            }
            if (item.server && item.server.baseUrl) {
                this._loadBarMenu(item.server, src);
            } else if (item.plugins && item.plugins.baseUrl) {
                this._loadPluginsMenu(item.plugins, src);
            }

            var item = ns.mainLayout.getSelectedItem();

            if (item.id != 'xuimain') {
                ns.mainLayout.selectItem('xuimain')
            }

        }
        ,
        _menubar_onclick: function (profile, popPro, item, src, newinstance) {
            var ns = this;
            if (!ns._prjserial) ns._prjserial = 1;

            if (xui.str.startWith(item.id, "xui.") && item.draggable) {
                this._addComponent(item);
                return;
            }
            if (item.root) {
                if (!ns.ViewMenuBar[item.id]) {
                    xui.Ajax("/radplugins/getActions", {viewBarId: item.id}, function (txt) {
                        var obj = txt;
                        if (item.apis) {
                            xui.each(item.apis, function (item) {
                                xui.create(item.key)
                                    .setAlias(item.alias)
                                    .setHost(ns, item.alias)
                                    .setEvents(xui.checkEvents(item.events))
                                    .setProperties(item.properties);
                            });

                        }
                        ns.ViewMenuBar[item.id] = xui.create(item.key)
                            .setAlias(item.alias)
                            .setHost(ns, item.alias)
                            .setEvents(xui.checkEvents(item.events))
                            .setProperties(item.properties);
                        ns.ViewMenuBar[item.id].pop(src);
                    }, function (obj) {
                        xui.message(xui.get(obj, ['error', 'errdes']) || obj || 'no response!');
                    }, null, null, {method: 'post'}).start();

                } else {
                    ns.ViewMenuBar[item.id].pop(src);
                }
            }


            if (item.menuType || item.root) {
                xui.ModuleFactory.getModule(item.className, function () {
                    var tagVar = item.tagVar;
                    tagVar.className = SPA.currentClassName;
                    this.setData(tagVar);
                    this.show(xui('body'));
                    this.initData();
                    this._fireEvent('afterShow');
                });
                return;
            }


            switch (item.id) {
                case 'setproject':
                    if (!this.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    this._setproject();
                    break;
                case 'about':
                    xui.ModuleFactory.getModule('about', function () {
                        this.show(null, null, null, null);
                    });
                    break;
                case 'version':
                    var tb = ns.tabsMain,
                        id = tb.getUIValue(),
                        o = tb.getItemByItemId(id),
                        hash = {path: o.id};
                    xui.ModuleFactory.getModule('RAD.VersionManager', function () {
                            this.show(null, null, null, null, hash);
                        }
                    );

                    break;
                case 'remoteServer':
                    $ESD.exportRemoteServer();
                    break;

                case 'localServer':
                    $ESD.exportLocalServer();
                    break;
                case 'buildServer':
                    $ESD.download();
                    break;

                case 'startServer':
                    $ESD.startDebugServer();
                    break;
                case 'stopServer':
                    $ESD.stopDebugServer();
                    break;

                case 'exit':
                    $ESD.quit();
                    break;

                case 'pullCode':
                    $ESD.pull();
                    break;

                case 'pushCode':
                    $ESD.push();
                    break;
                case 'clearAll':
                    $ESD.clearAll();
                    break;
                case 'openBPD':
                    $BPD.open();
                    break;

                case 'customDebug':
                    $ESD.customDebug();
                    break;
                case 'activeBranch':
                    break;


                case 'openBranch':
                    // var callback = function () {
                    xui.ModuleFactory.getModule('prjVersionSel', function () {
                        this.setProperties({
                            fromRegion: xui(src).cssRegion(true),
                            onOK1: ns._openproject,
                            onOK2: ns._openclsfolder,
                            projectName: SPA.curProjectName,
                            namespace: ns
                        });
                        this.show(xui('body'));
                    });

                    break;

                case 'newProject':

                    xui.ModuleFactory.getModule('prjPro', function () {
                        this.setProperties({
                            fromRegion: xui(src).cssRegion(true),
                            onOK1: ns._openproject,
                            onOK2: ns._openclsfolder,
                            namespace: ns
                        });
                        this.show(xui('body'));
                    });

                    break;


                case 'openProject':
                    xui.ModuleFactory.getModule('prjSel', function () {
                        this.setProperties({
                            fromRegion: xui(src).cssRegion(true),
                            onOK1: ns._openproject,
                            onOK2: ns._openclsfolder,
                            namespace: ns
                        });
                        this.show(xui('body'));
                    });
                    break;
                case 'newfile':
                    if (!this.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    var tab = this.tabsMain, type = tab.getUIValue();
                    this._fileaction(null, "new2", src, this.tabsMain.getUIValue());
                    break;

                case 'upload':
                    if (!this.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    var tab = this.tabsMain, type = tab.getUIValue();
                    this._fileaction(null, "upload", src, this.tabsMain.getUIValue());
                    break;
                case 'newFolder':
                    if (!this.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    var tab = this.tabsMain, type = tab.getUIValue();
                    this._fileaction(null, "newFolder", src, this.tabsMain.getUIValue());
                    break;

                case 'newClass':
                    if (!this.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    var tab = this.tabsMain, type = tab.getUIValue();
                    this._fileaction(null, "newClass", src, this.tabsMain.getUIValue());
                    break;
                case 'closeall':
                    if (!this.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    var tb = this.tabsMain,
                        count = 0,
                        items = tb.getItems(),
                        hash = {},
                        collections = [],
                        oneMoudleRefreshed;
                    tb.clearItems();
                    // xui.arr.each(items, function (o) {
                    //   tb.removeItem(o.id)
                    // });
                    break;

                case 'save':
                    if (!this.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    var tb = this.tabsMain,
                        id = tb.getUIValue(),
                        ns = this,
                        o = tb.getItemByItemId(id),
                        hash = {},
                        collections = [];
                    this._saveFile(o, collections);
                    xui.resetRun("$RAD-save-last", function () {
                        CONF.saveFiles(collections);
                    });
                    break;

                case 'saveall':
                    if (!this.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    var tb = this.tabsMain,
                        count = 0,
                        items = tb.getItems(),
                        hash = {},
                        ns = this,
                        collections = [],
                        oneMoudleRefreshed;
                    xui.arr.each(items, function (o) {
                        ns._saveFile(o, collections);
                        //  collections.push({id: o.id, paras: paras, onSuccess: onSuccess, onFail: onFail});
                    });

                    CONF.saveFiles(collections);

                    break;
                case 'ec':
                    if (!this.$dropmenulang || this.$dropmenulang.isDestroyed()) {
                        this.$dropmenulang = new xui.UI.PopMenu({
                            items: [
                                {id: 'en', caption: xui.wrapRes('RAD.en')},
                                {id: 'cn', caption: xui.wrapRes('RAD.cn')}

                            ]
                        }, {
                            onMenuSelected: function (p, item) {
                                var lang = item.id;
                                if (xui.getLang() != lang) {
                                    var fun = function () {
                                        xui.setLang(lang, null, function (c, l) {
                                            if (c == 0) {
                                                xui.include("xui.Locale.en.doc", CONF.getAPIPath() + "Locale/en.js", function () {
                                                    if (!xui.Locale[lang].doc)
                                                        xui.Locale[lang].doc = xui.Locale.en.doc;
                                                });
                                            }
                                            // ajust all popmenu

                                            try {
                                                xui.UI.MenuBar.getAll().clearPopCache();
                                                xui.UI.PopMenu.getAll().adjustSize();
                                            } catch (e) {

                                            }

                                        });
                                        var hash = CONF.getStatus();
                                        xui.merge(hash, {
                                            "lang": xui.getLang()
                                        }, 'all');
                                        CONF.saveStatus(hash);
                                    };
                                    //use no-exist id
                                    xui.include("xui.Locale." + lang + ".doc", CONF.getAPIPath() + "Locale/" + lang + ".js", fun, fun);
                                }
                            }
                        });
                    }
                    this.$dropmenulang.pop(src);
                    break;
                case 'theme':
                    var m;
                    if (!(m = this.$dropmenutheme) || m.isDestroyed()) {
                        m = this.$dropmenutheme = new xui.UI.PopMenu();
                        m.onMenuSelected(function (p, item) {
                            if (xui.getTheme() != item.id) {
                                xui.setTheme(item.id, true, function () {
                                    xui.UI.MenuBar.getAll().clearPopCache();
                                    xui.UI.PopMenu.getAll().adjustSize();
                                    // ns.toolbar.updateItem('ec',{'caption':xui.getRes('RAD.'+item.id)});
                                    var hash = CONF.getStatus();
                                    xui.merge(hash, {
                                        "theme": xui.getTheme()
                                    }, 'all');
                                    CONF.saveStatus(hash);
                                }, null, ':not(.xui-designer-inner-control)');
                            }
                        });
                    }
                    var callback = function () {
                        m.pop(src);
                    };

                    if (m.getTag()) {
                        callback();
                    } else {
                        var items = [];
                        xui.arr.each(CONF.designer_themes, function (o) {
                            if (o) items.push({"id": o, "caption": "$RAD.builder.themes." + o});
                        });
                        m.setItems(items);
                        m.setTag("Loaded");
                        callback(items);
                    }
                    break;
                case 'info':
                    if (!this.Message.length)
                        return;
                    var list = this.$infoList, node = list.reBoxing();
                    list.setItems(xui.copy(this.Message));
                    node.popToTop(src, 4);
                    var unFun = function () {
                        node.hide();
                        //unhook
                        xui.Event.keyboardHook('esc');
                    };
                    //for on blur disappear
                    node.setBlurTrigger(list.get(0).$xid, unFun);
                    //for esc
                    xui.Event.keyboardHook('esc', 0, 0, 0, unFun);

                    break;
                case 'debug':
                    if (!ns.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    ns._dirtyWarn(function () {
                        var w = xui.get(SPA.curProjectConfig, ['$DevEnv', 'designViewConf', 'width']) || 800,
                            h = xui.get(SPA.curProjectConfig, ['$DevEnv', 'designViewConf', 'height']) || 600,
                            touch = xui.get(SPA.curProjectConfig, ['$DevEnv', 'designViewConf', 'touchDevice']) || 0;

                        CONF.debugApp("/debug/" + SPA.curProjectName + "/", xui.getClassName(SPA.currentPage.replace(SPA.curProjectPath + "/", "")) + ".cls", {
                            'rand': xui.rand(),
                            'width': w,
                            'height': h,
                            'theme': xui.get(SPA, ['curProjectConfig', '$PageAppearance', 'theme']) || '',
                            'ficdn': xui.get(SPA, ['curProjectConfig', '$FontIconsCDN']) || '',
                            'lang': xui.getLang()
                        }, newinstance !== false);

                    });
                    break;

                case 'release':

                    if (!this.$deploymenu) {
                        this.$deploymenu = new xui.UI.PopMenu({
                            items: [
                                {id: 'remoteServer', caption: '$(RAD.esdmenu.remoteServer)'},
                                {id: 'localServer', caption: '$(RAD.esdmenu.localServer)'},
                            ]
                        }, {
                            onMenuSelected: function (p, item) {
                                ns._menubar_onclick(ns.menubar.get(0), null, item, src);
                            }
                        });
                    }
                    this.$deploymenu.pop(src);
                    break;

                case 'saveasweb': {
                    if (!ns.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    xui.showModule("RAD.CustomPackage", function () {
                        this.initData();
                        this._fireEvent('afterShow');
                        this.setEvents("onOK", function (lib, appearance, locale) {
                            var hash = {
                                key: CONF.requestKey,
                                curProjectPath: SPA.curProjectPath,
                                paras: {
                                    action: 'saveasweb',
                                    ver: 'advanced',
                                    path: ns.curProjectPath,
                                    theme: xui.getTheme(),
                                    lang: xui.getLang(),
                                    files_lib: lib,
                                    files_appearance: appearance,
                                    files_locale: locale
                                }
                            };
                            CONF.saveAS(hash);
                        });
                    });
                }
                    break;
                case 'servicetester':
                    xui.getModule('RAD.ServiceTester', function () {
                        this.init();
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;
                case 'command':
                    xui.Debugger.log('Ready');
                    break;

                case 'localicon':
                    xui.getModule("RAD.SelFontAwesome", function () {
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;
                case 'orgManager':
                    xui.getModule("RAD.org.Main", function () {
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;


                case 'formulatool':
                    xui.ModuleFactory.getModule('esd.right.TabIndex', function () {
                        this.setData({baseType: 'RIGHT'});
                        this.show(xui('body'));
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;


                case 'imgmanager':
                    xui.getModule("RAD.ImageSelector", function () {
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;


                case 'imgimport':
                    xui.getModule("RAD.resource.ImageTree", function () {
                        this.setEvents("onDestroy", function (lib, appearance, locale) {
                            //ns._reload();
                            xui.alert("添加成功，请刷新浏览器后查看！");

                        });
                        this.setData({projectName: SPA.curProjectName});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;

                case 'serviceManager':
                    var designer = this._getDesigner();
                    xui.getModule("RAD.api.WebAPIManager", function () {
                        this.setData({projectName: SPA.curProjectName, className: SPA.currentClassName});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                        this.setDataToEditor(designer.$host, designer._cls, designer);

                    });
                    break;


                case 'expressionEditor':
                    var designer = this._getDesigner();
                    xui.getModule("RAD.expression.ExpressionTemp", function () {
                        this.setData({projectName: SPA.curProjectName, className: SPA.currentClassName});

                        this.setDataToEditor(designer.$host, designer._cls, {url: '', queryArgs: {expression: ''}});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;


                 case 'serviceImport':
                    xui.getModule("RAD.api.APITree", function () {
                        this.setData({projectName: SPA.curProjectName});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;


                case 'DBProviderList':
                    xui.getModule("RAD.db.DBProviderList", function () {
                        this.setData({projectName: SPA.curProjectName});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;


                case 'dbmodule':
                    $ESD.createDBModule();
                    break;
                case 'dbimport':
                    xui.getModule("RAD.db.project.DataBaseConfigList", function () {
                        this.setData({projectName: SPA.curProjectName});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;

                case 'configLocalServer':
                    xui.getModule("RAD.server.ESDServerList", function () {
                        this.setData({projectName: SPA.curProjectName});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;

                case 'configRemoteServer':
                    xui.getModule("RAD.server.RemoteServerList", function () {
                        this.setData({projectName: SPA.curProjectName});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;


                case 'configProxy':
                    xui.getModule("RAD.server.ProxyHostList", function () {
                        this.setData({projectName: SPA.curProjectName});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');
                    });
                    break;
                case 'teamRight':
                    xui.getModule("RAD.org.PersonList", function () {
                        this.setData({projectName: SPA.curProjectName});
                        this.show();
                        this.initData();
                        this._fireEvent('afterShow');

                    });
                    break;


                case 'iconfont.cn':
                    xui.openOtherWin("http://www.iconfont.cn");
                    break;

                case 'editorTheme':
                    xui.ModuleFactory.newModule('RAD.EditorTheme', function () {
                        this.show();
                        this.initData();
                        this.setEvents("onOK", function (theme, fs, wrap) {
                            var obj = CONF.getStatus();
                            obj.editorTheme = theme;
                            obj.editorFontSize = fs;
                            obj.lineWrapping = wrap;
                            CONF.saveStatus(obj);


                            if (ns.curProjectPath) {
                                var tb = ns.tabsMain,
                                    items = tb.getItems();
                                xui.arr.each(items, function (o) {
                                    if (o = o.$obj)
                                        o.resetEditorStyle(theme, fs, wrap);
                                });
                            }
                        });
                    });
                    break;
                case 'editxml':
                    if (!ns.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    break;
                case 'editcfg':
                    if (!ns.curProjectPath) {
                        xui.message(xui.getRes('RAD.ps.noprj'));
                        return;
                    }
                    break;
                case 'movews':
                    var f = function () {
                        xui.ModuleFactory.getModule('RAD.SwitchWorkspace', function () {
                            this.init();
                            this.show();
                            this.initData();
                            this._fireEvent('afterShow');
                        })
                    };
                    if (this.curProjectPath) {
                        this._closeproject(f);
                    } else
                        f();
                    break;

                case 'download':
                    CONF.openURL(CONF.path_download);
                    break;
                case 'viewbtn':
                    xui.showModule2('action.action.CodeTemps', null, null, {
                        projectVersionName: currProjectName,
                        projectName: currProjectName
                    });

                    break;


                //
                // default:
                //     xui.message(xui.getRes('RAD.soon'));
            }
        }
        ,
        __ondroptest: function (profile, e, src, dragKey, dragData, item) {
            var id = xui(src).id(),
                rn = profile.getRootNode(),
                oitem = profile.getItemByDom(dragData.domId);
            if (item) {
                // stop no sub item, stop sibling adding
                if (!item.sub || profile.getKey(src) != profile.keys["TOGGLE"]) {
                    return false;
                }
            }
            // stop root
            else {
                return false;
            }
        }
        ,
        __ondroptest2: function (profile, e, src, dragKey, dragData, item) {
            var id = xui(src).id(),
                rn = profile.getRootNode(),
                oitem = profile.getItemByDom(dragData.domId);
            if (item) {
                // stop no sub item, stop sibling adding
                if (!item.sub || profile.getKey(src) != profile.keys["TOGGLE"]) {
                    return false;
                }
            }
            // stop root
            else {
//                return false;
            }
        }
        ,
        __beforedrop: function (profile, e, src, dragKey, dragData, item) {
            var ns = this,
                ins = profile.boxing(),
                oitem = profile.getItemByDom(dragData.domId);
            if (!oitem) return;

            var oid = oitem.id,
                caption = oitem.name,
                nid = (item ? item.id : (ns.curProjectPath + "/" + ns.currentRootCls + "/js"));

            xui.Ajax(CONF.copy, {
                spath: oid,
                tpath: nid,
                packageName: ns.packageName
            }, function (txt) {
                var obj = txt;
                if (obj && !obj.error && obj.requestStatus != -1 && obj.data.path) {
                    var pathadd = obj.data.path,
                        name = pathadd.substr(pathadd.lastIndexOf("/") + 1);
                    item = {
                        id: pathadd,
                        name: name,
                        caption: name,
                        value: pathadd
                    };
                    item = xui.clone(item);
                    if (type == 'cls') {
                        item.caption = ns._getpagename(name);
                    }
                    if (type == 'prj') {
                        item.caption = ns._getpagename(name);
                    }
                    var insOther = type == 'prj' ? ins1 : ins2;
                    if (insOther && (tbItem = insOther.getItemByItemId(tbItem.id)))
                        insOther.insertItems([item], tbItem.id);
                    ns.spath = null;
                } else
                    xui.message(xui.get(obj, ['error', 'errdes']) || obj || 'no response!');
            }, function (obj) {
                xui.message(xui.get(obj, ['error', 'errdes']) || obj || 'no response!');
            }, null, null, {method: 'POST'}).start();

            return false;
        }
    }
})
;

